<?xml version='1.0'?>

<xmas:sensor xmlns:xmas="http://xdaq.web.cern.ch/xdaq/xsd/2006/xmas-10">

  <!-- TCDS common monitoring. -->
  <xmas:monitor xmlns:xlink="http://www.w3.org/1999/xlink">
    <xmas:flash xlink:type="locator" xlink:href="http://${SLIM_SERVICE_HOST}:${SLIM_DIRECTORY_SERVICE_PORT}/directory/flash/tcds_common.flash#urn:xdaq-flashlist:tcds_common" />
    <xmas:sampler tag="unit" type="urn:xmas-sampler:instant" />
  </xmas:monitor>

  <!-- CPM-specific - grandmaster lumi section table. -->
  <xmas:monitor xmlns:xlink="http://www.w3.org/1999/xlink">
    <xmas:flash xlink:type="locator" xlink:href="http://${SLIM_SERVICE_HOST}:${SLIM_DIRECTORY_SERVICE_PORT}/directory/flash/tcds_cpm_grandmaster.flash#urn:xdaq-flashlist:tcds_cpm_grandmaster" />
    <xmas:sampler tag="unit" type="urn:xmas-sampler:instant" />
  </xmas:monitor>

  <!-- CPM-specific - lumi section and lumi nibble storage. -->
  <xmas:monitor xmlns:xlink="http://www.w3.org/1999/xlink">
    <xmas:flash xlink:type="locator" xlink:href="http://${SLIM_SERVICE_HOST}:${SLIM_DIRECTORY_SERVICE_PORT}/directory/flash/tcds_cpm_sections.flash#urn:xdaq-flashlist:tcds_cpm_sections" />
    <xmas:sampler tag="unit" type="urn:xmas-sampler:instant" />
  </xmas:monitor>
  <xmas:monitor xmlns:xlink="http://www.w3.org/1999/xlink">
    <xmas:flash xlink:type="locator" xlink:href="http://${SLIM_SERVICE_HOST}:${SLIM_DIRECTORY_SERVICE_PORT}/directory/flash/tcds_cpm_nibbles.flash#urn:xdaq-flashlist:tcds_cpm_nibbles" />
    <!-- NOTE: In order to avoid strange effects (e.g.,
         https://its.cern.ch/jira/browse/CMSOS-35) the hashkey here
         has to match the ones in the slash2g configuration file. -->
    <xmas:sampler tag="unit" type="urn:xmas-sampler:hash" every="PT2S" hashkey="service,nibble_number" clear="true" />
  </xmas:monitor>

  <!-- CPM-specific - trigger counts. -->
  <xmas:monitor xmlns:xlink="http://www.w3.org/1999/xlink">
    <xmas:flash xlink:type="locator" xlink:href="http://${SLIM_SERVICE_HOST}:${SLIM_DIRECTORY_SERVICE_PORT}/directory/flash/tcds_cpm_counts.flash#urn:xdaq-flashlist:tcds_cpm_counts" />
    <xmas:sampler tag="unit" type="urn:xmas-sampler:instant" />
  </xmas:monitor>

  <!-- CPM-specific - trigger rates. -->
  <!-- - Updated once per lumi section for storage purposes. -->
  <xmas:monitor xmlns:xlink="http://www.w3.org/1999/xlink">
    <xmas:flash xlink:type="locator" xlink:href="http://${SLIM_SERVICE_HOST}:${SLIM_DIRECTORY_SERVICE_PORT}/directory/flash/tcds_cpm_rates.flash#urn:xdaq-flashlist:tcds_cpm_rates" />
    <xmas:sampler tag="unit" type="urn:xmas-sampler:instant" />
  </xmas:monitor>
  <!-- - Updated at approximately 1 Hz for monitoring pusposes. -->
  <xmas:monitor xmlns:xlink="http://www.w3.org/1999/xlink">
    <xmas:flash xlink:type="locator" xlink:href="http://${SLIM_SERVICE_HOST}:${SLIM_DIRECTORY_SERVICE_PORT}/directory/flash/tcds_cpm_rates_1hz.flash#urn:xdaq-flashlist:tcds_cpm_rates_1hz" />
    <xmas:sampler tag="unit" type="urn:xmas-sampler:instant" />
  </xmas:monitor>

  <!-- CPM-specific - deadtimes. -->
  <!-- - Updated once per lumi section for storage purposes. -->
  <xmas:monitor xmlns:xlink="http://www.w3.org/1999/xlink">
    <xmas:flash xlink:type="locator" xlink:href="http://${SLIM_SERVICE_HOST}:${SLIM_DIRECTORY_SERVICE_PORT}/directory/flash/tcds_cpm_deadtimes.flash#urn:xdaq-flashlist:tcds_cpm_deadtimes" />
    <xmas:sampler tag="unit" type="urn:xmas-sampler:instant" />
  </xmas:monitor>
  <!-- - Updated at approximately 1 Hz for monitoring pusposes. -->
  <xmas:monitor xmlns:xlink="http://www.w3.org/1999/xlink">
    <xmas:flash xlink:type="locator" xlink:href="http://${SLIM_SERVICE_HOST}:${SLIM_DIRECTORY_SERVICE_PORT}/directory/flash/tcds_cpm_deadtimes_1hz.flash#urn:xdaq-flashlist:tcds_cpm_deadtimes_1hz" />
    <xmas:sampler tag="unit" type="urn:xmas-sampler:instant" />
  </xmas:monitor>

  <!-- PM-specific - B-go counts. -->
  <xmas:monitor xmlns:xlink="http://www.w3.org/1999/xlink">
    <xmas:flash xlink:type="locator" xlink:href="http://${SLIM_SERVICE_HOST}:${SLIM_DIRECTORY_SERVICE_PORT}/directory/flash/tcds_pm_bgo_counts.flash#urn:xdaq-flashlist:tcds_pm_bgo_counts" />
    <xmas:sampler tag="unit" type="urn:xmas-sampler:instant" />
  </xmas:monitor>

  <!-- PM-specific - Sequence counts. -->
  <xmas:monitor xmlns:xlink="http://www.w3.org/1999/xlink">
    <xmas:flash xlink:type="locator" xlink:href="http://${SLIM_SERVICE_HOST}:${SLIM_DIRECTORY_SERVICE_PORT}/directory/flash/tcds_pm_sequence_counts.flash#urn:xdaq-flashlist:tcds_pm_sequence_counts" />
    <xmas:sampler tag="unit" type="urn:xmas-sampler:instant" />
  </xmas:monitor>

  <!-- PM-specific - Action counts. -->
  <xmas:monitor xmlns:xlink="http://www.w3.org/1999/xlink">
    <xmas:flash xlink:type="locator" xlink:href="http://${SLIM_SERVICE_HOST}:${SLIM_DIRECTORY_SERVICE_PORT}/directory/flash/tcds_pm_action_counts.flash#urn:xdaq-flashlist:tcds_pm_action_counts" />
    <xmas:sampler tag="unit" type="urn:xmas-sampler:instant" />
  </xmas:monitor>

  <!-- PM-specific - TTS channels. -->
  <xmas:monitor xmlns:xlink="http://www.w3.org/1999/xlink">
    <xmas:flash xlink:type="locator" xlink:href="http://${SLIM_SERVICE_HOST}:${SLIM_DIRECTORY_SERVICE_PORT}/directory/flash/tcds_pm_tts_channel.flash#urn:xdaq-flashlist:tcds_pm_tts_channel" />
    <xmas:sampler tag="unit" type="urn:xmas-sampler:instant" />
  </xmas:monitor>

</xmas:sensor>
