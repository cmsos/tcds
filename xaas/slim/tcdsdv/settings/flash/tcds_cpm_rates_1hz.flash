<?xml version='1.0'?>

<!--
This is basically a copy of the tcds_cpm_rates flashlist. However, it
is updated at approximately 1 Hz, instead of once per lumi
section. This means that the former flashlist should be used for XMAS
storage, and this flashlist should be used for monitoring only. And
even then: beware of fluctuations.
-->

<xmas:flash
    xmlns:xmas="http://xdaq.web.cern.ch/xdaq/xsd/2006/xmas-10"
    id="urn:xdaq-flashlist:tcds_cpm_rates_1hz"
    version="1"
    key="fill_number,run_number,section_number">

  <!-- ++++++++++++++++++++++++++++++ -->
  <!-- Common. -->
  <!-- ++++++++++++++++++++++++++++++ -->

  <!-- The application context: the host name and the port number of
       the XDAQ executive. -->
  <xmas:item
      name="context"
      function="context()"
      type="string" />

  <!-- The local-id of the application in the executive. -->
  <xmas:item
      name="lid"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="$id"
      type="string" />

  <!-- The name of the service provided by the XDAQ application. -->
  <xmas:item
      name="service"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="$service"
      type="string" />

  <!-- ++++++++++++++++++++++++++++++ -->
  <!-- CPM-specific. -->
  <!-- ++++++++++++++++++++++++++++++ -->

  <!-- NOTE: The database column names cannot be longer than 30
       characters, hence the messing-about with the parameter
       names. -->

  <!-- Book keeping information. -->
  <xmas:item
      name="fill_number"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      type="unsigned int 32" />
  <xmas:item
      name="run_number"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      type="unsigned int 32" />
  <xmas:item
      name="section_number"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      type="unsigned int 32" />
  <!-- NOTE: The num_nibbles means something slightly different here
       from what it means in the tcds-cpm-rates flashlist. In this
       case it shows the number of nibbles in the sliding window used
       to get the current values. -->
  <xmas:item
      name="num_nibbles"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      type="unsigned int 32" />

  <!-- Trigger rates. -->
  <xmas:item
      name="trg_rate_total"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.plain.trigger_rate_total"
      type="double" />
  <xmas:item
      name="trg_rate_tt0"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.plain.trigger_rate_trigtype0"
      type="double" />
  <xmas:item
      name="trg_rate_tt1"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.plain.trigger_rate_trigtype1"
      type="double" />
  <xmas:item
      name="trg_rate_tt2"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.plain.trigger_rate_trigtype2"
      type="double" />
  <xmas:item
      name="trg_rate_tt3"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.plain.trigger_rate_trigtype3"
      type="double" />
  <xmas:item
      name="trg_rate_tt4"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.plain.trigger_rate_trigtype4"
      type="double" />
  <xmas:item
      name="trg_rate_tt5"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.plain.trigger_rate_trigtype5"
      type="double" />
  <xmas:item
      name="trg_rate_tt6"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.plain.trigger_rate_trigtype6"
      type="double" />
  <xmas:item
      name="trg_rate_tt7"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.plain.trigger_rate_trigtype7"
      type="double" />
  <xmas:item
      name="trg_rate_tt8"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.plain.trigger_rate_trigtype8"
      type="double" />
  <xmas:item
      name="trg_rate_tt9"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.plain.trigger_rate_trigtype9"
      type="double" />
  <xmas:item
      name="trg_rate_tt10"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.plain.trigger_rate_trigtype10"
      type="double" />
  <xmas:item
      name="trg_rate_tt11"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.plain.trigger_rate_trigtype11"
      type="double" />
  <xmas:item
      name="trg_rate_tt12"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.plain.trigger_rate_trigtype12"
      type="double" />
  <xmas:item
      name="trg_rate_tt13"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.plain.trigger_rate_trigtype13"
      type="double" />
  <xmas:item
      name="trg_rate_tt14"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.plain.trigger_rate_trigtype14"
      type="double" />
  <xmas:item
      name="trg_rate_tt15"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.plain.trigger_rate_trigtype15"
      type="double" />

  <xmas:item
      name="trg_rate_beamactive_total"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.beamactive.trigger_rate_total"
      type="double" />
  <xmas:item
      name="trg_rate_beamactive_tt0"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.beamactive.trigger_rate_trigtype0"
      type="double" />
  <xmas:item
      name="trg_rate_beamactive_tt1"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.beamactive.trigger_rate_trigtype1"
      type="double" />
  <xmas:item
      name="trg_rate_beamactive_tt2"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.beamactive.trigger_rate_trigtype2"
      type="double" />
  <xmas:item
      name="trg_rate_beamactive_tt3"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.beamactive.trigger_rate_trigtype3"
      type="double" />
  <xmas:item
      name="trg_rate_beamactive_tt4"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.beamactive.trigger_rate_trigtype4"
      type="double" />
  <xmas:item
      name="trg_rate_beamactive_tt5"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.beamactive.trigger_rate_trigtype5"
      type="double" />
  <xmas:item
      name="trg_rate_beamactive_tt6"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.beamactive.trigger_rate_trigtype6"
      type="double" />
  <xmas:item
      name="trg_rate_beamactive_tt7"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.beamactive.trigger_rate_trigtype7"
      type="double" />
  <xmas:item
      name="trg_rate_beamactive_tt8"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.beamactive.trigger_rate_trigtype8"
      type="double" />
  <xmas:item
      name="trg_rate_beamactive_tt9"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.beamactive.trigger_rate_trigtype9"
      type="double" />
  <xmas:item
      name="trg_rate_beamactive_tt10"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.beamactive.trigger_rate_trigtype10"
      type="double" />
  <xmas:item
      name="trg_rate_beamactive_tt11"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.beamactive.trigger_rate_trigtype11"
      type="double" />
  <xmas:item
      name="trg_rate_beamactive_tt12"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.beamactive.trigger_rate_trigtype12"
      type="double" />
  <xmas:item
      name="trg_rate_beamactive_tt13"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.beamactive.trigger_rate_trigtype13"
      type="double" />
  <xmas:item
      name="trg_rate_beamactive_tt14"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.beamactive.trigger_rate_trigtype14"
      type="double" />
  <xmas:item
      name="trg_rate_beamactive_tt15"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.beamactive.trigger_rate_trigtype15"
      type="double" />

  <!-- Suppressed-trigger rates. -->
  <xmas:item
      name="sup_trg_rate_total"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.beamactive.suppressed_trigger_rate_total"
      type="double" />
  <xmas:item
      name="sup_trg_rate_tt0"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.beamactive.suppressed_trigger_rate_trigtype0"
      type="double" />
  <xmas:item
      name="sup_trg_rate_tt1"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.beamactive.suppressed_trigger_rate_trigtype1"
      type="double" />
  <xmas:item
      name="sup_trg_rate_tt2"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.beamactive.suppressed_trigger_rate_trigtype2"
      type="double" />
  <xmas:item
      name="sup_trg_rate_tt3"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.beamactive.suppressed_trigger_rate_trigtype3"
      type="double" />
  <xmas:item
      name="sup_trg_rate_tt4"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.beamactive.suppressed_trigger_rate_trigtype4"
      type="double" />
  <xmas:item
      name="sup_trg_rate_tt5"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.beamactive.suppressed_trigger_rate_trigtype5"
      type="double" />
  <xmas:item
      name="sup_trg_rate_tt6"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.beamactive.suppressed_trigger_rate_trigtype6"
      type="double" />
  <xmas:item
      name="sup_trg_rate_tt7"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.beamactive.suppressed_trigger_rate_trigtype7"
      type="double" />
  <xmas:item
      name="sup_trg_rate_tt8"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.beamactive.suppressed_trigger_rate_trigtype8"
      type="double" />
  <xmas:item
      name="sup_trg_rate_tt9"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.beamactive.suppressed_trigger_rate_trigtype9"
      type="double" />
  <xmas:item
      name="sup_trg_rate_tt10"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.beamactive.suppressed_trigger_rate_trigtype10"
      type="double" />
  <xmas:item
      name="sup_trg_rate_tt11"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.beamactive.suppressed_trigger_rate_trigtype11"
      type="double" />
  <xmas:item
      name="sup_trg_rate_tt12"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.beamactive.suppressed_trigger_rate_trigtype12"
      type="double" />
  <xmas:item
      name="sup_trg_rate_tt13"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.beamactive.suppressed_trigger_rate_trigtype13"
      type="double" />
  <xmas:item
      name="sup_trg_rate_tt14"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.beamactive.suppressed_trigger_rate_trigtype14"
      type="double" />
  <xmas:item
      name="sup_trg_rate_tt15"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.beamactive.suppressed_trigger_rate_trigtype15"
      type="double" />

  <xmas:item
      name="sup_trg_rate_beamactive_total"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.beamactive.suppressed_trigger_rate_total"
      type="double" />
  <xmas:item
      name="sup_trg_rate_beamactive_tt0"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.beamactive.suppressed_trigger_rate_trigtype0"
      type="double" />
  <xmas:item
      name="sup_trg_rate_beamactive_tt1"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.beamactive.suppressed_trigger_rate_trigtype1"
      type="double" />
  <xmas:item
      name="sup_trg_rate_beamactive_tt2"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.beamactive.suppressed_trigger_rate_trigtype2"
      type="double" />
  <xmas:item
      name="sup_trg_rate_beamactive_tt3"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.beamactive.suppressed_trigger_rate_trigtype3"
      type="double" />
  <xmas:item
      name="sup_trg_rate_beamactive_tt4"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.beamactive.suppressed_trigger_rate_trigtype4"
      type="double" />
  <xmas:item
      name="sup_trg_rate_beamactive_tt5"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.beamactive.suppressed_trigger_rate_trigtype5"
      type="double" />
  <xmas:item
      name="sup_trg_rate_beamactive_tt6"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.beamactive.suppressed_trigger_rate_trigtype6"
      type="double" />
  <xmas:item
      name="sup_trg_rate_beamactive_tt7"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.beamactive.suppressed_trigger_rate_trigtype7"
      type="double" />
  <xmas:item
      name="sup_trg_rate_beamactive_tt8"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.beamactive.suppressed_trigger_rate_trigtype8"
      type="double" />
  <xmas:item
      name="sup_trg_rate_beamactive_tt9"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.beamactive.suppressed_trigger_rate_trigtype9"
      type="double" />
  <xmas:item
      name="sup_trg_rate_beamactive_tt10"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.beamactive.suppressed_trigger_rate_trigtype10"
      type="double" />
  <xmas:item
      name="sup_trg_rate_beamactive_tt11"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.beamactive.suppressed_trigger_rate_trigtype11"
      type="double" />
  <xmas:item
      name="sup_trg_rate_beamactive_tt12"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.beamactive.suppressed_trigger_rate_trigtype12"
      type="double" />
  <xmas:item
      name="sup_trg_rate_beamactive_tt13"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.beamactive.suppressed_trigger_rate_trigtype13"
      type="double" />
  <xmas:item
      name="sup_trg_rate_beamactive_tt14"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.beamactive.suppressed_trigger_rate_trigtype14"
      type="double" />
  <xmas:item
      name="sup_trg_rate_beamactive_tt15"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.beamactive.suppressed_trigger_rate_trigtype15"
      type="double" />

</xmas:flash>
