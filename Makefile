BUILD_HOME:=$(shell pwd)

ifndef BUILD_SUPPORT
  BUILD_SUPPORT=build
endif
export BUILD_SUPPORT

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfAutoconf.rules
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfDefs.$(XDAQ_OS)

# As a transition measure between XDAQ14 (gcc 4.8, C++98) and XDAQ15
# (gcc 7.2, C++14), let's build in some wiggle room.
# Useful choices should be one of:
# - XDAQ_TARGET_XDAQ14
# - XDAQ_TARGET_XDAQ15
ifndef XDAQ_TARGET
  export XDAQ_TARGET=XDAQ_TARGET_XDAQ14
endif

# export BUILD_VERSION = alpha

PROJECT_NAME=tcds

# NOTE: The build order here is important. Packages that build
# libraries used by other packages need to be built first.
ifndef PACKAGES
  PACKAGES= \
      tcds/addresstables \
      tcds/apve \
      tcds/bobr \
      tcds/brildaqchecker \
      tcds/cntrl \
      tcds/cpm \
      tcds/deadwood \
      tcds/exception \
      tcds/freqmon \
      tcds/hardwood \
      tcds/hwlayer \
      tcds/hwlayertca \
      tcds/hwlayervme \
      tcds/hwutilstca \
      tcds/hwutilsvme \
      tcds/ici \
      tcds/lpm \
      tcds/phasemon \
      tcds/pi \
      tcds/pm \
      tcds/pytcds \
      tcds/pyvme \
      tcds/rf2ttc \
      tcds/rfdippub \
      tcds/rfrxd \
      tcds/utils \
      xaas/slim/tcds40/zone \
      xaas/slim/tcds40/settings \
      xaas/slim/tcds40/service \
      xaas/slim/tcds40/client \
      xaas/slim/tcds40/extension \
      xaas/slim/tcds904/zone \
      xaas/slim/tcds904/settings \
      xaas/slim/tcds904/service \
      xaas/slim/tcds904/client \
      xaas/slim/tcds904/extension \
      xaas/slim/tcdsd3v/zone \
      xaas/slim/tcdsd3v/settings \
      xaas/slim/tcdsd3v/service \
      xaas/slim/tcdsd3v/client \
      xaas/slim/tcdsd3v/extension \
      xaas/slim/tcdsdv/zone \
      xaas/slim/tcdsdv/settings \
      xaas/slim/tcdsdv/service \
      xaas/slim/tcdsdv/client \
      xaas/slim/tcdsdv/extension \
      xaas/slim/tcdslab/zone \
      xaas/slim/tcdslab/settings \
      xaas/slim/tcdslab/service \
      xaas/slim/tcdslab/client \
      xaas/slim/tcdslab/addon \
      xaas/slim/tcdslab/extension \
      xaas/slim/tcdsp5/zone \
      xaas/slim/tcdsp5/settings \
      xaas/slim/tcdsp5/service \
      xaas/slim/tcdsp5/client \
      xaas/slim/tcdsp5/addon \
      xaas/slim/tcdsp5/extension
endif
export PACKAGES

Project=$(PROJECT_NAME)
Packages=$(PACKAGES)

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/Makefile.rules
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfRPM.rules
