#include "tcds/hwutilstca/HwIDInfoSpaceHandlerTCA.h"

#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/WebServer.h"

tcds::hwutilstca::HwIDInfoSpaceHandlerTCA::HwIDInfoSpaceHandlerTCA(xdaq::Application& xdaqApp,
                                                                   tcds::utils::InfoSpaceUpdater* updater) :
  InfoSpaceHandler(xdaqApp, "tcds-hw-id", updater)
{
  // General board and firmware information.
  createString("board_id",
               tcds::utils::InfoSpaceHandler::kUninitializedString,
               "",
               tcds::utils::InfoSpaceItem::PROCESS);
  createString("system_id",
               tcds::utils::InfoSpaceHandler::kUninitializedString,
               "",
               tcds::utils::InfoSpaceItem::PROCESS);
  createString("system_firmware_version",
               tcds::utils::InfoSpaceHandler::kUninitializedString,
               "",
               tcds::utils::InfoSpaceItem::PROCESS);
  createString("user_firmware_date",
               tcds::utils::InfoSpaceHandler::kUninitializedString,
               "",
               tcds::utils::InfoSpaceItem::PROCESS);
  createString("user_firmware_version",
               tcds::utils::InfoSpaceHandler::kUninitializedString,
               "",
               tcds::utils::InfoSpaceItem::PROCESS);
  createString("system_firmware_date",
               tcds::utils::InfoSpaceHandler::kUninitializedString,
               "",
               tcds::utils::InfoSpaceItem::PROCESS);
  createString("eui48",
               tcds::utils::InfoSpaceHandler::kUninitializedString,
               "",
               tcds::utils::InfoSpaceItem::PROCESS);
  createString("mac_address",
               tcds::utils::InfoSpaceHandler::kUninitializedString,
               "",
               tcds::utils::InfoSpaceItem::PROCESS);
}

tcds::hwutilstca::HwIDInfoSpaceHandlerTCA::~HwIDInfoSpaceHandlerTCA()
{
}

void
tcds::hwutilstca::HwIDInfoSpaceHandlerTCA::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  // General board and firmware information.
  monitor.newItemSet("itemset-hw-id");
  monitor.addItem("itemset-hw-id",
                  "board_id",
                  "Board id",
                  this);
  monitor.addItem("itemset-hw-id",
                  "system_id",
                  "System ID",
                  this);
  monitor.addItem("itemset-hw-id",
                  "system_firmware_version",
                  "System firmware version",
                  this);
  monitor.addItem("itemset-hw-id",
                  "system_firmware_date",
                  "System firmware date",
                  this);
  monitor.addItem("itemset-hw-id",
                  "user_firmware_version",
                  "User firmware version",
                  this);
  monitor.addItem("itemset-hw-id",
                  "user_firmware_date",
                  "User firmware date",
                  this);
  monitor.addItem("itemset-hw-id",
                  "eui48",
                  "EUI-48",
                  this);
  monitor.addItem("itemset-hw-id",
                  "mac_address",
                  "MAC address",
                  this);
}

void
tcds::hwutilstca::HwIDInfoSpaceHandlerTCA::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                         tcds::utils::Monitor& monitor,
                                                                         std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "Hardware ID" : forceTabName;

  webServer.registerTab(tabName,
                        "Hardware identifiers",
                        3);
  webServer.registerTable("Hardware identifiers",
                          "General hardware identifiers",
                          monitor,
                          "itemset-hw-id",
                          tabName);
}
