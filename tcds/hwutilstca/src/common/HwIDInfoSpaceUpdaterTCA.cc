#include "tcds/hwutilstca/HwIDInfoSpaceUpdaterTCA.h"

#include <string>

#include "tcds/hwlayertca/TCADeviceBase.h"
#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/InfoSpaceItem.h"

tcds::hwutilstca::HwIDInfoSpaceUpdaterTCA::HwIDInfoSpaceUpdaterTCA(tcds::utils::XDAQAppBase& xdaqApp,
                                                                   tcds::hwlayertca::TCADeviceBase const& hw) :
  tcds::utils::HwInfoSpaceUpdaterBase(xdaqApp, hw)
{
}

tcds::hwutilstca::HwIDInfoSpaceUpdaterTCA::~HwIDInfoSpaceUpdaterTCA()
{
}

bool
tcds::hwutilstca::HwIDInfoSpaceUpdaterTCA::updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                                               tcds::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  bool updated = false;
  tcds::hwlayertca::TCADeviceBase const& hw = getHw();
  if (hw.isReadyForUse())
    {
      std::string name = item.name();
      tcds::utils::InfoSpaceItem::UpdateType updateType = item.updateType();
      if (updateType == tcds::utils::InfoSpaceItem::PROCESS)
        {
          // The 'PROCESS' update type means that there is something
          // special to the variable. Figure out what to do based on the
          // variable name.
          if (name == "eui48")
            {
              std::string const newVal = hw.readEUI48();
              infoSpaceHandler->setString(name, newVal);
              updated = true;
            }
          else if (name == "mac_address")
            {
              std::string const newVal = hw.readMACAddress();
              infoSpaceHandler->setString(name, newVal);
              updated = true;
            }
          else if (name == "board_id")
            {
              std::string const newVal = hw.readBoardId();
              infoSpaceHandler->setString(name, newVal);
              updated = true;
            }
          else if (name == "system_id")
            {
              std::string const newVal = hw.readSystemId();
              infoSpaceHandler->setString(name, newVal);
              updated = true;
            }
          else if (name == "system_firmware_version")
            {
              std::string const newVal = hw.readSystemFirmwareVersion();
              infoSpaceHandler->setString(name, newVal);
              updated = true;
            }
          else if (name == "user_firmware_version")
            {
              std::string const newVal = hw.readUserFirmwareVersion();
              infoSpaceHandler->setString(name, newVal);
              updated = true;
            }
          else if (name == "system_firmware_date")
            {
              std::string const newVal = hw.readSystemFirmwareDate();
              infoSpaceHandler->setString(name, newVal);
              updated = true;
            }
          else if (name == "user_firmware_date")
            {
              std::string const newVal = hw.readUserFirmwareDate();
              infoSpaceHandler->setString(name, newVal);
              updated = true;
            }
        }
      if (!updated)
        {
          updated = tcds::utils::HwInfoSpaceUpdaterBase::updateInfoSpaceItem(item, infoSpaceHandler);
        }
    }

  if (updated)
    {
      item.setValid();
    }
  else
    {
      item.setInvalid();
    }

  return updated;
}

tcds::hwlayertca::TCADeviceBase const&
tcds::hwutilstca::HwIDInfoSpaceUpdaterTCA::getHw() const
{
  return dynamic_cast<tcds::hwlayertca::TCADeviceBase const&>(tcds::utils::HwInfoSpaceUpdaterBase::getHw());
}
