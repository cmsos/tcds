#include "tcds/hwutilstca/SFPDataPointCollection.h"

#include <cmath>

tcds::hwutilstca::SFPDataPointCollection::SFPDataPointCollection()

{
}

void
tcds::hwutilstca::SFPDataPointCollection::addDataPoint(tcds::hwutilstca::SFPDataPoint const& dataPoint)
{
  dataPoints_.push_back(dataPoint);
}

tcds::hwutilstca::SFPDataPoint
tcds::hwutilstca::SFPDataPointCollection::back() const
{
  return dataPoints_.back();
}

bool
tcds::hwutilstca::SFPDataPointCollection::modAbs() const
{
  // NOTE: The assumption is that only compatible points have been
  // added to this collection, so the values for all entries are the
  // same.
  bool const res = dataPoints_.at(0).modAbs();
  return res;
}

bool
tcds::hwutilstca::SFPDataPointCollection::txDisable() const
{
  // NOTE: The assumption is that only compatible points have been
  // added to this collection, so the values for all entries are the
  // same.
  bool const res = dataPoints_.at(0).txDisable();
  return res;
}

bool
tcds::hwutilstca::SFPDataPointCollection::txFault() const
{
  // NOTE: The assumption is that only compatible points have been
  // added to this collection, so the values for all entries are the
  // same.
  bool const res = dataPoints_.at(0).txFault();
  return res;
}

bool
tcds::hwutilstca::SFPDataPointCollection::rxLOS() const
{
  // NOTE: The assumption is that only compatible points have been
  // added to this collection, so the values for all entries are the
  // same.
  bool const res = dataPoints_.at(0).rxLOS();
  return res;
}

std::string
tcds::hwutilstca::SFPDataPointCollection::vendorName() const
{
  // NOTE: The assumption is that only compatible points have been
  // added to this collection, so the values for all entries are the
  // same.
  std::string const res = dataPoints_.at(0).vendorName();
  return res;
}

std::string
tcds::hwutilstca::SFPDataPointCollection::vendorPartNumber() const
{
  // NOTE: The assumption is that only compatible points have been
  // added to this collection, so the values for all entries are the
  // same.
  std::string const res = dataPoints_.at(0).vendorPartNumber();
  return res;
}

std::string
tcds::hwutilstca::SFPDataPointCollection::vendorRevision() const
{
  // NOTE: The assumption is that only compatible points have been
  // added to this collection, so the values for all entries are the
  // same.
  std::string const res = dataPoints_.at(0).vendorRevision();
  return res;
}

std::string
tcds::hwutilstca::SFPDataPointCollection::vendorSerialNumber() const
{
  // NOTE: The assumption is that only compatible points have been
  // added to this collection, so the values for all entries are the
  // same.
  std::string const res = dataPoints_.at(0).vendorSerialNumber();
  return res;
}

float
tcds::hwutilstca::SFPDataPointCollection::temperature() const
{
  // Just the average of the values of the individual points.
  float sum = 0.;
  for (std::vector<tcds::hwutilstca::SFPDataPoint>::const_iterator i = dataPoints_.begin();
       i != dataPoints_.end();
       ++i)
    {
      sum += i->temperature();
    }
  float const res = sum / dataPoints_.size();
  return res;
}

float
tcds::hwutilstca::SFPDataPointCollection::vcc() const
{
  // Just the average of the values of the individual points.
  float sum = 0.;
  for (std::vector<tcds::hwutilstca::SFPDataPoint>::const_iterator i = dataPoints_.begin();
       i != dataPoints_.end();
       ++i)
    {
      sum += i->vcc();
    }
  float const res = sum / dataPoints_.size();
  return res;
}

float
tcds::hwutilstca::SFPDataPointCollection::txBiasCurrent() const
{
  // Just the average of the values of the individual points.
  float sum = 0.;
  for (std::vector<tcds::hwutilstca::SFPDataPoint>::const_iterator i = dataPoints_.begin();
       i != dataPoints_.end();
       ++i)
    {
      sum += i->txBiasCurrent();
    }
  float const res = sum / dataPoints_.size();
  return res;
}

float
tcds::hwutilstca::SFPDataPointCollection::txPower() const
{
  // Just the average of the values of the individual points.
  float sum = 0.;
  for (std::vector<tcds::hwutilstca::SFPDataPoint>::const_iterator i = dataPoints_.begin();
       i != dataPoints_.end();
       ++i)
    {
      sum += i->txPower();
    }
  float const res = sum / dataPoints_.size();
  return res;
}

float
tcds::hwutilstca::SFPDataPointCollection::rxPower() const
{
  // Just the average of the values of the individual points.
  float sum = 0.;
  for (std::vector<tcds::hwutilstca::SFPDataPoint>::const_iterator i = dataPoints_.begin();
       i != dataPoints_.end();
       ++i)
    {
      sum += i->rxPower();
    }
  float const res = sum / dataPoints_.size();
  return res;
}

size_t
tcds::hwutilstca::SFPDataPointCollection::size() const
{
  return dataPoints_.size();
}

void
tcds::hwutilstca::SFPDataPointCollection::clear()
{
  dataPoints_.clear();
}

bool
tcds::hwutilstca::SFPDataPointCollection::differsFrom(SFPDataPointCollection const& other,
                                                      float const perc) const
{
  bool res = false;

  // If any of the measurements change by more than the specified
  // percentage, then yes.
  if ((percDiff(temperature(), other.temperature()) >= perc)
      || (percDiff(vcc(), other.vcc()) >= perc)
      || (percDiff(txBiasCurrent(), other.txBiasCurrent()) >= perc)
      || (percDiff(txPower(), other.txPower()) >= perc)
      || (percDiff(rxPower(), other.rxPower()) >= perc))
    {
      res = true;
    }

  return res;
}

float
tcds::hwutilstca::SFPDataPointCollection::percDiff(float const self, float const other)
{
  float res = 0.;
  if (self != 0.)
    {
      res = 100. * fabs(self - other) / self;
    }
  else
    {
      if (other != self)
        {
          res = 100.;
        }
    }
  return res;
}
