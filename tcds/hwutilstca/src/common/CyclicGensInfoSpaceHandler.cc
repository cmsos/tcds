#include "tcds/hwutilstca/CyclicGensInfoSpaceHandler.h"

#include <stdint.h>

#include "toolbox/string.h"

#include "tcds/hwutilstca/Definitions.h"
#include "tcds/hwutilstca/Utils.h"
#include "tcds/utils/Definitions.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/Utils.h"
#include "tcds/utils/WebServer.h"

tcds::hwutilstca::CyclicGensInfoSpaceHandler::CyclicGensInfoSpaceHandler(xdaq::Application& xdaqApp,
                                                                         tcds::utils::InfoSpaceUpdater* updater,
                                                                         unsigned int const numCyclicGens) :
  InfoSpaceHandler(xdaqApp, "tcds-cyclic-gens", updater),
  numCyclicGens_(numCyclicGens)
{
  for (unsigned int genNum = 0; genNum < numCyclicGens_; ++genNum)
    {
      std::string const baseName = toolbox::toString("cyclic_generator%d", genNum);
      createUInt32(baseName + ".configuration.mode",
                   0,
                   "",
                   tcds::utils::InfoSpaceItem::PROCESS);
      createBool(baseName + ".configuration.enabled",
                 false,
                 "true/false");
      createBool(baseName + ".configuration.permanent",
                 false,
                 "true/false");
      createUInt32(baseName + ".configuration.start_bx");
      createUInt32(baseName + ".configuration.prescale");
      createUInt32(baseName + ".configuration.initial_prescale");
      createUInt32(baseName + ".configuration.postscale");
      createBool(baseName + ".configuration.repeat_cycle",
                 false,
                 "true/false");
      createUInt32(baseName + ".configuration.pause");
      createUInt32(baseName + ".configuration.sync_mode",
                   0,
                   "",
                   tcds::utils::InfoSpaceItem::PROCESS);
      createUInt32(baseName + ".configuration.behaviour_in_system_pause");

      // NOTE: The following registers are only used internally by
      // this InfoSpaceHandler. They do not show up in the items on
      // the HyperDAQ page.
      createUInt32(baseName + ".configuration.trigger_word.id");
      createUInt32(baseName + ".configuration.init_bgo_id");
    }

}

tcds::hwutilstca::CyclicGensInfoSpaceHandler::~CyclicGensInfoSpaceHandler()
{
}

void
tcds::hwutilstca::CyclicGensInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  for (unsigned int genNum = 0; genNum < numCyclicGens_; ++genNum)
    {
      std::string itemSetName = toolbox::toString("itemset-cyclic-generator%d", genNum);
      monitor.newItemSet(itemSetName);

      std::string const baseName = toolbox::toString("cyclic_generator%d", genNum);
      monitor.addItem(itemSetName,
                      baseName + ".configuration.mode",
                      "Mode",
                      this,
                      "The firing mode of the cyclic generator. E.g., L1A, B-go, B-go sequence.");
      monitor.addItem(itemSetName,
                      baseName + ".configuration.enabled",
                      "Enabled",
                      this,
                      "If true: the generator runs. If false: the generator does not run.");
      monitor.addItem(itemSetName,
                      baseName + ".configuration.permanent",
                      "Permanent",
                      this,
                      "Permanent cyclic generators always run. Non-permenent cyclic generators are started by a B-go Start and stopped by a B-go Stop.");
      monitor.addItem(itemSetName,
                      baseName + ".configuration.start_bx",
                      "Firing BX",
                      this,
                      "The firing bunch-crossing of this cyclic generator.");
      monitor.addItem(itemSetName,
                      baseName + ".configuration.prescale",
                      "Prescale",
                      this,
                      "If set to N <> 0: only every N-th orbit will advance the cyclic generator.");
      monitor.addItem(itemSetName,
                      baseName + ".configuration.initial_prescale",
                      "Initial prescale value",
                      this,
                      "The starting value of the prescale counter. If set to 0: the first firing occurs in the orbit in which the cyclic generator starts.");
      monitor.addItem(itemSetName,
                      baseName + ".configuration.postscale",
                      "Postscale",
                      this,
                      "If set to N <> 0: the generator will fire N times and then either pause and restart, or stop.");
      monitor.addItem(itemSetName,
                      baseName + ".configuration.repeat_cycle",
                      "Repeat cycle",
                      this,
                      "If true: after processing 'postscale' firings, the generator will pause for 'pause' orbits and repeat its cycle.");
      monitor.addItem(itemSetName,
                      baseName + ".configuration.pause",
                      "Pause",
                      this,
                      "The number of orbits to pause before repeating the cyclic generator sequence (if postscale is non-zero).");
      monitor.addItem(itemSetName,
                      baseName + ".configuration.sync_mode",
                      "Synchronisation mode",
                      this,
                      "Cyclic generators can be free-running, or synchronised to a certain B-go.");
      monitor.addItem(itemSetName,
                      baseName + ".configuration.behaviour_in_system_pause",
                      "Behaviour during system pause",
                      this,
                      "During system pause, cyclic generators can either freeze completely, or run and cancel their output.");
    }
}

void
tcds::hwutilstca::CyclicGensInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                            tcds::utils::Monitor& monitor,
                                                                            std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "Cyclic generators" : forceTabName;

  webServer.registerTab(tabName,
                        "Info on the cyclic generators",
                        4);

  for (unsigned int genNum = 0; genNum < numCyclicGens_; ++genNum)
    {
      std::string const name = toolbox::toString("Cyclic generator %d", genNum);
      webServer.registerTable(name,
                              "",
                              monitor,
                              toolbox::toString("itemset-cyclic-generator%d", genNum),
                              tabName);
    }
}

std::string
tcds::hwutilstca::CyclicGensInfoSpaceHandler::formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const
{
  std::string res = tcds::utils::escapeAsJSONString(tcds::utils::InfoSpaceHandler::kInvalidItemString);
  if (item->isValid())
    {
      std::string name = item->name();
      if (toolbox::endsWith(name, ".mode"))
        {
          uint32_t const value = getUInt32(name);
          tcds::definitions::CYCLIC_GEN_MODE const valueEnum =
            static_cast<tcds::definitions::CYCLIC_GEN_MODE>(value);
          std::string tmp = "";
          if ((valueEnum == tcds::definitions::CYCLIC_GEN_BGO) ||
              (valueEnum == tcds::definitions::CYCLIC_GEN_SEQUENCE))
            {
              std::string const baseName = name.substr(0, name.size() - std::string(".mode").size());
              uint32_t const id = getUInt32(baseName + ".trigger_word.id");
              if (valueEnum == tcds::definitions::CYCLIC_GEN_BGO)
                {
                  std::string const bgoNameString =
                    tcds::utils::formatBgoNameString(static_cast<tcds::definitions::BGO_NUM>(id));
                  tmp = "fire " + bgoNameString;
                }
              else if (valueEnum == tcds::definitions::CYCLIC_GEN_SEQUENCE)
                {
                  std::string const sequenceNameString =
                    tcds::utils::formatSequenceNameString(static_cast<tcds::definitions::SEQUENCE_NUM>(id));
                  tmp = "fire " + sequenceNameString;
                }
            }
          else
            {
              tmp = tcds::hwutilstca::cyclicGenModeToString(valueEnum);
            }
          res = tcds::utils::escapeAsJSONString(tmp);
        }
      else if (toolbox::endsWith(name, ".sync_mode"))
        {
          uint32_t const value = getUInt32(name);
          tcds::definitions::CYCLIC_GEN_SYNC_MODE const valueEnum =
            static_cast<tcds::definitions::CYCLIC_GEN_SYNC_MODE>(value);
          std::string tmp = "";
          if (valueEnum == tcds::definitions::CYCLIC_GEN_SYNC_BGO)
            {
              std::string const baseName = name.substr(0, name.size() - std::string(".sync_mode").size());
              uint32_t const id = getUInt32(baseName + ".init_bgo_id");
                  std::string const bgoNameString =
                    tcds::utils::formatBgoNameString(static_cast<tcds::definitions::BGO_NUM>(id));
                  tmp = "aligned to " + bgoNameString;
            }
          else
            {
              tmp = tcds::hwutilstca::cyclicGenSyncModeToString(valueEnum);
            }
          res = tcds::utils::escapeAsJSONString(tmp);
        }
      else if (toolbox::endsWith(name, ".behaviour_in_system_pause"))
        {
          uint32_t const value = getUInt32(name);
          tcds::definitions::CYCLIC_GEN_PAUSE_BEHAVIOUR const valueEnum =
            static_cast<tcds::definitions::CYCLIC_GEN_PAUSE_BEHAVIOUR>(value);
          std::string const tmp = tcds::hwutilstca::cyclicGenPauseBehaviourToString(valueEnum);
          res = tcds::utils::escapeAsJSONString(tmp);
        }
      else
        {
          // For everything else simply call the generic formatter.
          res = InfoSpaceHandler::formatItem(item);
        }
    }
  return res;
}
