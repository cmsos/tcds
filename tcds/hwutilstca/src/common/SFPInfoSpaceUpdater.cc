#include "tcds/hwutilstca/SFPInfoSpaceUpdater.h"

#include "toolbox/TimeInterval.h"
#include "toolbox/string.h"
#include "xcept/Exception.h"

#include "tcds/exception/Exception.h"
#include "tcds/hwlayertca/Definitions.h"
#include "tcds/hwlayertca/I2CPlayer.h"
#include "tcds/hwutilstca/SFPDataPoint.h"
#include "tcds/utils/ConfigurationInfoSpaceHandler.h"
#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/XDAQAppBase.h"

namespace toolbox {
  namespace exception {
  class Exception;
  }
}

tcds::hwutilstca::SFPInfoSpaceUpdater::SFPInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                                           tcds::hwlayertca::TCADeviceBase const& hw) :
  tcds::utils::HwInfoSpaceUpdaterBase(xdaqApp, hw)
{
  tcds::utils::ConfigurationInfoSpaceHandler const& cfgInfoSpace =
    getOwnerApplication().getConfigurationInfoSpaceHandler();

  numAvg_ = cfgInfoSpace.getUInt32("sfpMonitorNumAvg");

  deadbandPercent_ = cfgInfoSpace.getFloat("sfpMonitorDeadBandPercent");
  std::string const tmp = cfgInfoSpace.getString("sfpMonitorDeadBandOverrideTime");
  try
    {
      deadbandOverrideTime_.fromString(tmp);
    }
  catch (toolbox::exception::Exception& err)
    {
      std::string const msg =
        toolbox::toString("Failed to interpret '%s' as dead-band override interval.",
                          tmp.c_str());
      XCEPT_RAISE(tcds::exception::ValueError, msg.c_str());
    }
}

void
tcds::hwutilstca::SFPInfoSpaceUpdater::updateInfoSpaceImpl(tcds::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  std::string const sfpType = infoSpaceHandler->getString("sfp_source_type");
  uint32_t const sfpId = infoSpaceHandler->getUInt32("sfp_source_id_number");

  toolbox::TimeVal const timeNow = toolbox::TimeVal::gettimeofday();
  tcds::hwlayertca::TCADeviceBase const& hw = getHw();
  if (hw.isHwConnected())
    {
      if (!i2cPlayerP_.get())
        {
          i2cPlayerP_ = std::unique_ptr<tcds::hwlayertca::I2CPlayer>(new tcds::hwlayertca::I2CPlayer(hw));
        }
      else
        {
          i2cPlayerP_->update();
        }

      // Figure out which SFP (and on which FMC) we're talking about
      // here.
      short fmcNum = 0;
      short sfpNum = 0;

      switch (sfpId)
        {
        case tcds::definitions::SFP_NUMBER_L12_SFP1:
          fmcNum = tcds::definitions::FMC_NUMBER_L12;
          sfpNum = 0;
          break;
        case tcds::definitions::SFP_NUMBER_L12_SFP2:
          fmcNum = tcds::definitions::FMC_NUMBER_L12;
          sfpNum = 1;
          break;
        case tcds::definitions::SFP_NUMBER_L12_SFP3:
          fmcNum = tcds::definitions::FMC_NUMBER_L12;
          sfpNum = 2;
          break;
        case tcds::definitions::SFP_NUMBER_L12_SFP4:
          fmcNum = tcds::definitions::FMC_NUMBER_L12;
          sfpNum = 3;
          break;
        case tcds::definitions::SFP_NUMBER_L12_SFP5:
          fmcNum = tcds::definitions::FMC_NUMBER_L12;
          sfpNum = 4;
          break;
        case tcds::definitions::SFP_NUMBER_L12_SFP6:
          fmcNum = tcds::definitions::FMC_NUMBER_L12;
          sfpNum = 5;
          break;
        case tcds::definitions::SFP_NUMBER_L12_SFP7:
          fmcNum = tcds::definitions::FMC_NUMBER_L12;
          sfpNum = 6;
          break;
        case tcds::definitions::SFP_NUMBER_L12_SFP8:
          fmcNum = tcds::definitions::FMC_NUMBER_L12;
          sfpNum = 7;
          break;
        case tcds::definitions::SFP_NUMBER_L8_SFP1:
          fmcNum = tcds::definitions::FMC_NUMBER_L8;
          sfpNum = 0;
          break;
        case tcds::definitions::SFP_NUMBER_L8_SFP2:
          fmcNum = tcds::definitions::FMC_NUMBER_L8;
          sfpNum = 1;
          break;
        case tcds::definitions::SFP_NUMBER_L8_SFP3:
          fmcNum = tcds::definitions::FMC_NUMBER_L8;
          sfpNum = 2;
          break;
        case tcds::definitions::SFP_NUMBER_L8_SFP4:
          fmcNum = tcds::definitions::FMC_NUMBER_L8;
          sfpNum = 3;
          break;
        case tcds::definitions::SFP_NUMBER_L8_SFP5:
          fmcNum = tcds::definitions::FMC_NUMBER_L8;
          sfpNum = 4;
          break;
        case tcds::definitions::SFP_NUMBER_L8_SFP6:
          fmcNum = tcds::definitions::FMC_NUMBER_L8;
          sfpNum = 5;
          break;
        case tcds::definitions::SFP_NUMBER_L8_SFP7:
          fmcNum = tcds::definitions::FMC_NUMBER_L8;
          sfpNum = 6;
          break;
        case tcds::definitions::SFP_NUMBER_L8_SFP8:
          fmcNum = tcds::definitions::FMC_NUMBER_L8;
          sfpNum = 7;
          break;
        default:
          XCEPT_RAISE(tcds::exception::ValueError,
                      toolbox::toString("Trying to update monitoring info for an unknow SFP ID: %d",
                                        sfpId));
          break;
        }

      // Now go and find all information related to this SFP.
      bool const modAbs = !i2cPlayerP_->isModulePresent(fmcNum, sfpNum);
      bool const txDisable = i2cPlayerP_->isModuleDisabled(fmcNum, sfpNum);
      bool const txFault = i2cPlayerP_->isTXFault(fmcNum, sfpNum);
      bool const rxLOS = i2cPlayerP_->isRXLOS(fmcNum, sfpNum);

      std::string sfpVendorName = "";
      std::string sfpVendorPartNumber = "";
      std::string sfpVendorRevision = "";
      std::string sfpVendorSerialNumber = "";
      float sfpTemperature = 0.;
      float sfpVcc = 0.;
      float sfpTXBiasCurrent = 0.;
      float sfpTXPower = 0.;
      float sfpRXPower = 0.;
      if (!modAbs)
        {
          sfpVendorName = i2cPlayerP_->getSFPVendorName(fmcNum, sfpNum);
          sfpVendorPartNumber = i2cPlayerP_->getSFPVendorPartNumber(fmcNum, sfpNum);
          sfpVendorRevision = i2cPlayerP_->getSFPVendorRevision(fmcNum, sfpNum);
          sfpVendorSerialNumber = i2cPlayerP_->getSFPVendorSerialNumber(fmcNum, sfpNum);
          sfpTemperature = i2cPlayerP_->getSFPTemperature(fmcNum, sfpNum);
          sfpVcc = i2cPlayerP_->getSFPVcc(fmcNum, sfpNum);
          sfpTXBiasCurrent = i2cPlayerP_->getSFPTXBiasCurrent(fmcNum, sfpNum);
          sfpTXPower = i2cPlayerP_->getSFPTXPower(fmcNum, sfpNum);
          sfpRXPower = i2cPlayerP_->getSFPRXPower(fmcNum, sfpNum);
        }

      // Now we have to see if things have changed sufficiently since
      // last time to warrant storing the data and starting a new
      // collection.
      tcds::hwutilstca::SFPDataPoint const
        dataPointCurr(modAbs, txDisable, txFault, rxLOS,
                      sfpVendorName, sfpVendorPartNumber, sfpVendorRevision, sfpVendorSerialNumber,
                      sfpTemperature, sfpVcc, sfpTXBiasCurrent, sfpTXPower, sfpRXPower);

      std::pair<std::string, uint32_t> const key = std::make_pair(sfpType, sfpId);
      tcds::hwutilstca::SFPDataPointCollection& dataPoints = dataPoints_[key];
      tcds::hwutilstca::SFPDataPointCollection& dataPointsPrev = dataPointsPrev_[key];
      toolbox::TimeVal& timePreviousUpdate = updateTimestamps_[key];
      bool& firstConnection = firstConnection_[key];
      if (dataPoints.size() != 0)
        {
          tcds::hwutilstca::SFPDataPoint const& dataPointPrev = dataPoints.back();

          // See if the current point can be added to what we already have
          // or not.
          if (!dataPointCurr.isCompatible(dataPointPrev))
            {
              // Top-level monitoring conditions have changed, or the
              // device has changed since we last looked, so let's
              // store what we have and start over with a clean slate.

              // Store what we have.
              store(infoSpaceHandler);

              // Start over based on our current data point.
              startOver(infoSpaceHandler, timeNow);
            }
        }
      // else
      {
        // It looks as if nothing has changed, or we cleaned up
        // what we had, so let's add the current data point to
        // our collection.
        dataPoints.addDataPoint(dataPointCurr);
        updateValues(infoSpaceHandler);

        // Now check if there is reason to store and start over
        // based on normal conditions.
        if ((dataPoints.size() == numAvg_) || firstConnection)
          {
            // We have reached our target number of points to
            // average over. So we potentially have a new set of
            // values to store.
            if (firstConnection
                || (dataPoints.differsFrom(dataPointsPrev, deadbandPercent_))
                || (toolbox::TimeInterval(timeNow - timePreviousUpdate) >= deadbandOverrideTime_))
              {
                store(infoSpaceHandler);
                startOver(infoSpaceHandler, timeNow);
              }
            else
              {
                dataPoints.clear();
              }
          }
        firstConnection = false;
      }
      infoSpaceHandler->setValid();
    }
  else
    {
      std::pair<std::string, uint32_t> const key = std::make_pair(sfpType, sfpId);
      tcds::hwutilstca::SFPDataPointCollection& dataPoints = dataPoints_[key];
      if (dataPoints.size())
        {
          store(infoSpaceHandler);
          startOver(infoSpaceHandler, timeNow);
        }

      infoSpaceHandler->setInvalid();

      bool& firstConnection = firstConnection_[key];
      firstConnection = true;
    }
}

tcds::hwlayertca::TCADeviceBase const&
tcds::hwutilstca::SFPInfoSpaceUpdater::getHw() const
{
  return static_cast<tcds::hwlayertca::TCADeviceBase const&>(tcds::utils::HwInfoSpaceUpdaterBase::getHw());
}

void
tcds::hwutilstca::SFPInfoSpaceUpdater::updateValues(tcds::utils::InfoSpaceHandler* const infoSpaceHandler) const
{
  std::string const sfpType = infoSpaceHandler->getString("sfp_source_type");
  uint32_t const sfpId = infoSpaceHandler->getUInt32("sfp_source_id_number");

  std::pair<std::string, uint32_t> const key = std::make_pair(sfpType, sfpId);
  tcds::hwutilstca::SFPDataPointCollection const dataPoints = dataPoints_.at(key);

  infoSpaceHandler->setBool("sfp_source_mod_abs", dataPoints.modAbs());
  infoSpaceHandler->setBool("sfp_source_tx_disable", dataPoints.txDisable());
  infoSpaceHandler->setBool("sfp_source_tx_fault", dataPoints.txFault());
  infoSpaceHandler->setBool("sfp_source_rx_los", dataPoints.rxLOS());

  infoSpaceHandler->setString("sfp_source_vendor_name", dataPoints.vendorName());
  infoSpaceHandler->setString("sfp_source_vendor_part_number", dataPoints.vendorPartNumber());
  infoSpaceHandler->setString("sfp_source_vendor_revision", dataPoints.vendorRevision());
  infoSpaceHandler->setString("sfp_source_vendor_serial_number", dataPoints.vendorSerialNumber());

  infoSpaceHandler->setUInt32("num_average", dataPoints.size());

  infoSpaceHandler->setFloat("sfp_source_temperature", dataPoints.temperature());
  infoSpaceHandler->setFloat("sfp_source_vcc", dataPoints.vcc());
  infoSpaceHandler->setFloat("sfp_source_tx_bias_current", dataPoints.txBiasCurrent());
  infoSpaceHandler->setFloat("sfp_source_tx_power", dataPoints.txPower());
  infoSpaceHandler->setFloat("sfp_source_rx_power", dataPoints.rxPower());
}

void
tcds::hwutilstca::SFPInfoSpaceUpdater::store(tcds::utils::InfoSpaceHandler* const infoSpaceHandler) const
{
  infoSpaceHandler->writeInfoSpace();
}

void
tcds::hwutilstca::SFPInfoSpaceUpdater::startOver(tcds::utils::InfoSpaceHandler* const infoSpaceHandler,
                                                 toolbox::TimeVal const& timeNow)
{
  std::string const sfpType = infoSpaceHandler->getString("sfp_source_type");
  uint32_t const sfpId = infoSpaceHandler->getUInt32("sfp_source_id_number");

  std::pair<std::string, uint32_t> const key = std::make_pair(sfpType, sfpId);
  tcds::hwutilstca::SFPDataPointCollection& dataPoints = dataPoints_.at(key);
  tcds::hwutilstca::SFPDataPointCollection& dataPointsPrev = dataPointsPrev_.at(key);
  toolbox::TimeVal& timePreviousUpdate = updateTimestamps_.at(key);

  dataPointsPrev = dataPoints;
  dataPoints.clear();
  timePreviousUpdate = timeNow;
}
