#include "tcds/hwutilstca/SFPInfoSpaceHandlerBase.h"

#include "tcds/hwutilstca/SFPInfoSpaceUpdater.h"
#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/XDAQAppBase.h"

tcds::hwutilstca::SFPInfoSpaceHandlerBase::SFPInfoSpaceHandlerBase(tcds::utils::XDAQAppBase& xdaqApp,
                                                                   tcds::hwutilstca::SFPInfoSpaceUpdater* updater) :
  MultiInfoSpaceHandler(xdaqApp, updater)
{
}

tcds::hwutilstca::SFPInfoSpaceHandlerBase::~SFPInfoSpaceHandlerBase()
{
  // Cleanup of InfoSpaceHandlers happens in the MultiInfoSpaceHandler
  // base class.
}

void
tcds::hwutilstca::SFPInfoSpaceHandlerBase::createSFPChannel(tcds::utils::InfoSpaceHandler* const handler,
                                                            std::string const sourceType,
                                                            unsigned int const idNumber,
                                                            std::string const name)
{
  // An identifier string indicating the source type of this channel.
  handler->createString("sfp_source_type",
                        sourceType,
                        "",
                        tcds::utils::InfoSpaceItem::NOUPDATE);

  // The source ID number of the source of this TTS channel.
  handler->createUInt32("sfp_source_id_number",
                        idNumber,
                        "",
                        tcds::utils::InfoSpaceItem::NOUPDATE);

  // Module presence/absence.
  handler->createBool("sfp_source_mod_abs",
                      true,
                      "false/true",
                      tcds::utils::InfoSpaceItem::NOUPDATE);

  // Module disable.
  handler->createBool("sfp_source_tx_disable",
                      true,
                      "true/false",
                      tcds::utils::InfoSpaceItem::NOUPDATE);

  // TX fault.
  handler->createBool("sfp_source_tx_fault",
                      true,
                      "true/false",
                      tcds::utils::InfoSpaceItem::NOUPDATE);

  // RX LOS.
  handler->createBool("sfp_source_rx_los",
                      true,
                      "true/false",
                      tcds::utils::InfoSpaceItem::NOUPDATE);

  // Vendor name (16 char).
  handler->createString("sfp_source_vendor_name",
                        "",
                        "",
                        tcds::utils::InfoSpaceItem::NOUPDATE);

  // Vendor part number (16 char).
  handler->createString("sfp_source_vendor_part_number",
                        "",
                        "",
                        tcds::utils::InfoSpaceItem::NOUPDATE);

  // Vendor revision number (4 char).
  handler->createString("sfp_source_vendor_revision",
                        "",
                        "",
                        tcds::utils::InfoSpaceItem::NOUPDATE);

  // Vendor serial number (16 char).
  handler->createString("sfp_source_vendor_serial_number",
                        "",
                        "",
                        tcds::utils::InfoSpaceItem::NOUPDATE);

  // The rest of the values are averages over (relatively noisy)
  // measurement series. This variable tells us how many single
  // measurements went into the current data point.
  handler->createUInt32("num_average",
                        0,
                        "",
                        tcds::utils::InfoSpaceItem::NOUPDATE);

  // SFP temperature.
  handler->createFloat("sfp_source_temperature",
                       0.,
                       "%.1f",
                       tcds::utils::InfoSpaceItem::NOUPDATE);

  // SFP Vcc.
  handler->createFloat("sfp_source_vcc",
                       0.,
                       "%.1f",
                       tcds::utils::InfoSpaceItem::NOUPDATE);

  // SFP TX bias current.
  handler->createFloat("sfp_source_tx_bias_current",
                       0.,
                       "%.1f",
                       tcds::utils::InfoSpaceItem::NOUPDATE);
  // SFP TX power.
  handler->createFloat("sfp_source_tx_power",
                       0.,
                       "%.2f",
                       tcds::utils::InfoSpaceItem::NOUPDATE);

  // SFP RX power.
  handler->createFloat("sfp_source_rx_power",
                       0.,
                       "%.2f",
                       tcds::utils::InfoSpaceItem::NOUPDATE);
}

void
tcds::hwutilstca::SFPInfoSpaceHandlerBase::registerSFPChannel(std::string const& name,
                                                              std::string const& itemSetName,
                                                              tcds::utils::Monitor& monitor)
{
  // For every SFP we want to track:
  // - Some form of manufacturer and/or model ID:
  //   - Vendor name (16 char).
  //   - Vendor part number (16 char).
  //   - Vendor revision number (4 char).
  //   - Vendor serial number (16 char).
  // - Status/diagnostic information:
  //   - Module absent or not.
  //   - TX disabled or not.
  //   - RX LOS or not.
  //   - TX fault or not.
  //   - Temperature.
  //   - Vcc.
  //   - TX bias current.
  //   - TX power.
  //   - RX power.

  monitor.newItemSet(itemSetName);

  monitor.addItem(itemSetName,
                  "sfp_source_mod_abs",
                  "Module present",
                  infoSpaceHandlers_.at(name));
  monitor.addItem(itemSetName,
                  "sfp_source_tx_disable",
                  "Module disabled",
                  infoSpaceHandlers_.at(name));
  monitor.addItem(itemSetName,
                  "sfp_source_tx_fault",
                  "Transmit fault",
                  infoSpaceHandlers_.at(name));
  monitor.addItem(itemSetName,
                  "sfp_source_rx_los",
                  "Input signal lost",
                  infoSpaceHandlers_.at(name));

  monitor.addItem(itemSetName,
                  "sfp_source_vendor_name",
                  "Vendor name",
                  infoSpaceHandlers_.at(name));
  monitor.addItem(itemSetName,
                  "sfp_source_vendor_part_number",
                  "Vendor part number",
                  infoSpaceHandlers_.at(name));
  monitor.addItem(itemSetName,
                  "sfp_source_vendor_revision",
                  "Vendor revision",
                  infoSpaceHandlers_.at(name));
  monitor.addItem(itemSetName,
                  "sfp_source_vendor_serial_number",
                  "Vendor serial number",
                  infoSpaceHandlers_.at(name));

  monitor.addItem(itemSetName,
                  "sfp_source_temperature",
                  "SFP temperature (C)",
                  infoSpaceHandlers_.at(name));
  monitor.addItem(itemSetName,
                  "sfp_source_vcc",
                  "SFP Vcc (V)",
                  infoSpaceHandlers_.at(name));
  monitor.addItem(itemSetName,
                  "sfp_source_tx_bias_current",
                  "SFP TX bias current (uA)",
                  infoSpaceHandlers_.at(name));
  monitor.addItem(itemSetName,
                  "sfp_source_tx_power",
                  "SFP TX power (mW)",
                  infoSpaceHandlers_.at(name));
  monitor.addItem(itemSetName,
                  "sfp_source_rx_power",
                  "SFP RX power (mW)",
                  infoSpaceHandlers_.at(name));
}
