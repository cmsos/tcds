#include "tcds/hwutilstca/CyclicGensInfoSpaceUpdater.h"

#include <stdint.h>
#include <string>

#include "toolbox/string.h"

#include "tcds/hwlayertca/TCADeviceBase.h"
#include "tcds/hwutilstca/Definitions.h"
#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/InfoSpaceItem.h"

tcds::hwutilstca::CyclicGensInfoSpaceUpdater::CyclicGensInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                                                         tcds::hwlayertca::TCADeviceBase const& hw) :
  tcds::utils::HwInfoSpaceUpdaterBase(xdaqApp, hw)
{
}

tcds::hwutilstca::CyclicGensInfoSpaceUpdater::~CyclicGensInfoSpaceUpdater()
{
}

bool
tcds::hwutilstca::CyclicGensInfoSpaceUpdater::updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                                                  tcds::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  bool updated = false;
  tcds::hwlayertca::TCADeviceBase const& hw = getHw();
  if (hw.isReadyForUse())
    {
      std::string name = item.name();
      tcds::utils::InfoSpaceItem::UpdateType updateType = item.updateType();
      if (updateType == tcds::utils::InfoSpaceItem::PROCESS)
        {
          // The 'PROCESS' update type means that there is something
          // special to the variable. Figure out what to do based on the
          // variable name.
          if (toolbox::endsWith(name, ".configuration.mode"))
            {
              // TODO TODO TODO
              // This is a wee bit hacky. Would be nice to clean this
              // up a little.
              std::string const baseName = name.substr(0, name.size() - std::string(".mode").size());
              bool const isL1A = (hw.readRegister(baseName + ".trigger_word.l1a") == 0x1);
              bool const isBgo = (hw.readRegister(baseName + ".trigger_word.bgo_command") == 0x1);
              bool const isSequence = (hw.readRegister(baseName + ".trigger_word.bgo_sequence_command") == 0x1);
              tcds::definitions::CYCLIC_GEN_MODE cyclicGenMode = tcds::definitions::CYCLIC_GEN_OFF;
              if ((!!isL1A + !!isBgo + !!isSequence) > 1)
                {
                  cyclicGenMode = tcds::definitions::CYCLIC_GEN_MISCONFIGURED;
                }
              else
                {
                  if (isL1A)
                    {
                      cyclicGenMode = tcds::definitions::CYCLIC_GEN_L1A;
                    }
                  else if (isBgo)
                    {
                  cyclicGenMode = tcds::definitions::CYCLIC_GEN_BGO;
                    }
                  else if (isSequence)
                    {
                      cyclicGenMode = tcds::definitions::CYCLIC_GEN_SEQUENCE;
                    }
                }
              uint32_t const newVal = cyclicGenMode;
              // TODO TODO TODO end
              infoSpaceHandler->setUInt32(name, newVal);
              updated = true;
            }
          else if (toolbox::endsWith(name, ".configuration.sync_mode"))
            {
              // TODO TODO TODO
              // This is a wee bit hacky. Would be nice to clean this
              // up a little.
              std::string const baseName = name.substr(0, name.size() - std::string(".sync_mode").size());
              bool const isBgo = (hw.readRegister(baseName + ".init_on_bgo") == 0x1);
              tcds::definitions::CYCLIC_GEN_SYNC_MODE cyclicGenSyncMode = tcds::definitions::CYCLIC_GEN_SYNC_FREE;
              if (isBgo)
                {
                  cyclicGenSyncMode = tcds::definitions::CYCLIC_GEN_SYNC_BGO;
                }
              uint32_t const newVal = cyclicGenSyncMode;
              // TODO TODO TODO end
              infoSpaceHandler->setUInt32(name, newVal);
              updated = true;
            }
        }
      if (!updated)
        {
          updated = tcds::utils::HwInfoSpaceUpdaterBase::updateInfoSpaceItem(item, infoSpaceHandler);
        }
    }

  if (updated)
    {
      item.setValid();
    }
  else
    {
      item.setInvalid();
    }

  return updated;
}

tcds::hwlayertca::TCADeviceBase const&
tcds::hwutilstca::CyclicGensInfoSpaceUpdater::getHw() const
{
  return dynamic_cast<tcds::hwlayertca::TCADeviceBase const&>(tcds::utils::HwInfoSpaceUpdaterBase::getHw());
}
