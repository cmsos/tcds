#include "tcds/hwutilstca/version.h"

#include "config/version.h"
#include "hyperdaq/version.h"
#include "toolbox/version.h"
#include "xcept/version.h"
#include "xdaq/version.h"
#include "xdata/version.h"
#include "xgi/version.h"

#include "tcds/exception/version.h"
#include "tcds/hwlayer/version.h"
#include "tcds/hwlayertca/version.h"
#include "tcds/utils/version.h"

GETPACKAGEINFO(tcds::hwutilstca)

void
tcds::hwutilstca::checkPackageDependencies()
{
  CHECKDEPENDENCY(config);
  CHECKDEPENDENCY(hyperdaq);
  CHECKDEPENDENCY(toolbox);
  CHECKDEPENDENCY(xcept);
  CHECKDEPENDENCY(xdaq);
  CHECKDEPENDENCY(xdata);
  CHECKDEPENDENCY(xgi);

  CHECKDEPENDENCY(tcds::exception);
  CHECKDEPENDENCY(tcds::hwlayer);
  CHECKDEPENDENCY(tcds::hwlayertca);
  CHECKDEPENDENCY(tcds::utils);
}

std::set<std::string, std::less<std::string> >
tcds::hwutilstca::getPackageDependencies()
{
  std::set<std::string, std::less<std::string> > dependencies;

  ADDDEPENDENCY(dependencies, config);
  ADDDEPENDENCY(dependencies, hyperdaq);
  ADDDEPENDENCY(dependencies, toolbox);
  ADDDEPENDENCY(dependencies, xcept);
  ADDDEPENDENCY(dependencies, xdaq);
  ADDDEPENDENCY(dependencies, xdata);
  ADDDEPENDENCY(dependencies, xgi);

  ADDDEPENDENCY(dependencies, tcds::exception);
  ADDDEPENDENCY(dependencies, tcds::hwlayer);
  ADDDEPENDENCY(dependencies, tcds::hwlayertca);
  ADDDEPENDENCY(dependencies, tcds::utils);

  return dependencies;
}
