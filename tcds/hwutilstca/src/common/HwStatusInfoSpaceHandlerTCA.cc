#include "tcds/hwutilstca/HwStatusInfoSpaceHandlerTCA.h"

#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/WebServer.h"

tcds::hwutilstca::HwStatusInfoSpaceHandlerTCA::HwStatusInfoSpaceHandlerTCA(xdaq::Application& xdaqApp,
                                                                           std::string const& name,
                                                                           tcds::utils::InfoSpaceUpdater* updater) :
  InfoSpaceHandler(xdaqApp, name, updater)
{
  // Hardware status.
  createBool("ttc_clock_stable", false, "true/false", tcds::utils::InfoSpaceItem::PROCESS);
  createBool("ttc_clock_up", false, "true/false", tcds::utils::InfoSpaceItem::PROCESS);

  // Phase monitoring of the 40 MHz and 160 MHz clocks.
  // NOTE: There are two versions of these variables: one in ps for
  // live view in the application, one in s for database storage.
  createDouble("ttc_phase_mon.meas_40.measurement_value",
               0,
               "%.2f",
               tcds::utils::InfoSpaceItem::PROCESS);
  createDouble("ttc_phase_mon.meas_160.measurement_value",
               0,
               "%.2f",
               tcds::utils::InfoSpaceItem::PROCESS);
  // The below are the database versions, updated together with the
  // above ones.
  createDouble("ttc_phase_mon.meas_40.measurement_value_for_db",
               0,
               "",
               tcds::utils::InfoSpaceItem::NOUPDATE);
  createDouble("ttc_phase_mon.meas_160.measurement_value_for_db",
               0,
               "",
               tcds::utils::InfoSpaceItem::NOUPDATE);
}

tcds::hwutilstca::HwStatusInfoSpaceHandlerTCA::~HwStatusInfoSpaceHandlerTCA()
{
}

void
tcds::hwutilstca::HwStatusInfoSpaceHandlerTCA::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  std::string const itemSetName = "itemset-hw-status";

  // Hardware status.
  monitor.newItemSet(itemSetName);
  monitor.addItem(itemSetName,
                  "ttc_clock_stable",
                  "TTC clock stable",
                  this);
  monitor.addItem(itemSetName,
                  "ttc_clock_up",
                  "TTC clock PLL locked",
                  this);

  // Phase monitoring of the 40 MHz and 160 MHz clocks.
  monitor.addItem(itemSetName,
                  "ttc_phase_mon.meas_40.measurement_value",
                  "Phase indicator of 40 MHz clock (ps)",
                  this,
                  "The relative phase between the backplane 40 MHz clock and the PLL output 40 MHz clock.");
  monitor.addItem(itemSetName,
                  "ttc_phase_mon.meas_160.measurement_value",
                  "Phase indicator of 160 MHz clock (ps)",
                  this,
                  "The relative phase between the backplane 40 MHz clock and the PLL output 160 MHz clock.");
}

void
tcds::hwutilstca::HwStatusInfoSpaceHandlerTCA::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                             tcds::utils::Monitor& monitor,
                                                                             std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "Hardware status" : forceTabName;

  webServer.registerTab(tabName,
                        "Hardware information",
                        3);
  webServer.registerTable("Hardware status info",
                          "Hardware status info",
                          monitor,
                          "itemset-hw-status",
                          tabName);
}
