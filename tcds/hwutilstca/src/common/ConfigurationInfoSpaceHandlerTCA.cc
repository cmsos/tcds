#include "tcds/hwutilstca/ConfigurationInfoSpaceHandlerTCA.h"

#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Monitor.h"

tcds::hwutilstca::ConfigurationInfoSpaceHandlerTCA::ConfigurationInfoSpaceHandlerTCA(xdaq::Application& xdaqApp,
                                                                                     std::string const& name) :
  tcds::utils::ConfigurationInfoSpaceHandler(xdaqApp, name)
{
  // IPbus connection parameters.

  // The name of the IPbus connections file.
  createString("ipbusConnectionsFile",
               "${XDAQ_SETUP_ROOT}/${XDAQ_ZONE}/etc/tcds_connections.xml",
               "",
               tcds::utils::InfoSpaceItem::NOUPDATE,
               true);

  // The name of the IPbus connection in the above file.
  createString("ipbusConnection",
               "dummy",
               "",
               tcds::utils::InfoSpaceItem::NOUPDATE,
               true);
}

void
tcds::hwutilstca::ConfigurationInfoSpaceHandlerTCA::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  ConfigurationInfoSpaceHandler::registerItemSetsWithMonitor(monitor);

  std::string const itemSetName = "Application configuration";
  monitor.addItem(itemSetName,
                  "ipbusConnectionsFile",
                  "IPbus connections file",
                  this);
  monitor.addItem(itemSetName,
                  "ipbusConnection",
                  "IPbus connection name",
                  this);
}
