#include "tcds/hwutilstca/SFPDataPoint.h"

tcds::hwutilstca::SFPDataPoint::SFPDataPoint(bool const modAbs,
                                             bool const txDisable,
                                             bool const txFault,
                                             bool const rxLOS,
                                             std::string const& vendorName,
                                             std::string const& vendorPartNumber,
                                             std::string const& vendorRevision,
                                             std::string const& vendorSerialNumber,
                                             float const sfpTemperature,
                                             float const sfpVcc,
                                             float const sfpTXBiasCurrent,
                                             float const sfpTXPower,
                                             float const sfpRXPower) :
  modAbs_(modAbs),
  txDisable_(txDisable),
  txFault_(txFault),
  rxLOS_(rxLOS),
  vendorName_(vendorName),
  vendorPartNumber_(vendorPartNumber),
  vendorRevision_(vendorRevision),
  vendorSerialNumber_(vendorSerialNumber),
  sfpTemperature_(sfpTemperature),
  sfpVcc_(sfpVcc),
  sfpTXBiasCurrent_(sfpTXBiasCurrent),
  sfpTXPower_(sfpTXPower),
  sfpRXPower_(sfpRXPower)
{
}

bool
tcds::hwutilstca::SFPDataPoint::modAbs() const
{
  return modAbs_;
}

bool
tcds::hwutilstca::SFPDataPoint::txDisable() const
{
  return txDisable_;
}

bool
tcds::hwutilstca::SFPDataPoint::txFault() const
{
  return txFault_;
}

bool
tcds::hwutilstca::SFPDataPoint::rxLOS() const
{
  return rxLOS_;
}

std::string
tcds::hwutilstca::SFPDataPoint::vendorName() const
{
  return vendorName_;
}

std::string
tcds::hwutilstca::SFPDataPoint::vendorPartNumber() const
{
  return vendorPartNumber_;
}

std::string
tcds::hwutilstca::SFPDataPoint::vendorRevision() const
{
  return vendorRevision_;
}

std::string
tcds::hwutilstca::SFPDataPoint::vendorSerialNumber() const
{
  return vendorSerialNumber_;
}

float
tcds::hwutilstca::SFPDataPoint::temperature() const
{
  return sfpTemperature_;
}

float
tcds::hwutilstca::SFPDataPoint::vcc() const
{
  return sfpVcc_;
}

float
tcds::hwutilstca::SFPDataPoint::txBiasCurrent() const
{
  return sfpTXBiasCurrent_;
}

float
tcds::hwutilstca::SFPDataPoint::txPower() const
{
  return sfpTXPower_;
}

float
tcds::hwutilstca::SFPDataPoint::rxPower() const
{
  return sfpRXPower_;
}

bool
tcds::hwutilstca::SFPDataPoint::isCompatible(SFPDataPoint const& other) const
{
  bool const res = !haveConditionsChanged(other) && isSameDevice(other);
  return res;
}

bool
tcds::hwutilstca::SFPDataPoint::haveConditionsChanged(SFPDataPoint const& other) const
{
  bool res = false;

  // If the top-level flags change, then yes.
  if ((modAbs_ != other.modAbs_)
      || (txDisable_ != other.txDisable_)
      || (txFault_ != other.txFault_)
      || (rxLOS_ != other.rxLOS_))
    {
      res = true;
    }

  return res;
}

bool
tcds::hwutilstca::SFPDataPoint::isSameDevice(SFPDataPoint const& other) const
{
  bool res = false;

  // If any of the identification strings change, then yes. (This must
  // mean that someone really quickly exchanged an SFP.)
  if ((vendorName_ == other.vendorName_)
      || (vendorPartNumber_ == other.vendorPartNumber_)
      || (vendorRevision_ == other.vendorRevision_)
      || (vendorSerialNumber_ == other.vendorSerialNumber_))
    {
      res = true;
    }

  return res;
}
