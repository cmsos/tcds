#include "tcds/hwutilstca/SFPMonitoringConfigHelpers.h"

#include <string>

#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Monitor.h"

void
tcds::hwutilstca::SFPMonitoringConfigCreateItems(tcds::utils::InfoSpaceHandler* const handler)
{
  // Create the SFP monitoring configuration parameters.

  // - The number of measurements to average over.
  handler->createUInt32("sfpMonitorNumAvg",
                        100,
                        "",
                        tcds::utils::InfoSpaceItem::NOUPDATE,
                        true);

  // - The dead-band variation in percent.
  handler->createFloat("sfpMonitorDeadBandPercent",
                       1.,
                       "",
                       tcds::utils::InfoSpaceItem::NOUPDATE,
                       true);

  // - The dead-band override/time-out.
  handler->createString("sfpMonitorDeadBandOverrideTime",
                        "PT1H",
                        "",
                        tcds::utils::InfoSpaceItem::NOUPDATE,
                        true);
}

void
tcds::hwutilstca::SFPMonitoringRegisterItemsWithMonitor(tcds::utils::InfoSpaceHandler* const handler,
                                                        tcds::utils::Monitor& monitor)
{
  std::string const itemSetName = "Application configuration";

  // - The number of measurements to average over.
  monitor.addItem(itemSetName,
                  "sfpMonitorNumAvg",
                  "SFP monitoring: measurements per data point",
                  handler,
                  "These data are intrinsically noisy,"
                  " so a number of single measurements is averaged"
                  " for each actual data point.");

  // - The dead-band variation in percent.
  monitor.addItem(itemSetName,
                  "sfpMonitorDeadBandPercent",
                  "SFP monitoring: dead-band (%)",
                  handler,
                  "The data point only updates if the value changes"
                  " by at least this percentage.");

  // - The dead-band override/time-out.
  monitor.addItem(itemSetName,
                  "sfpMonitorDeadBandOverrideTime",
                  "SFP monitoring: dead-band override time interval",
                  handler,
                  "If the data point does not vary enough to exit the dead-band"
                  " it will be updated after this interval anyway.");
}
