#ifndef _tcds_hwutilstca_SFPInfoSpaceHandlerBase_h_
#define _tcds_hwutilstca_SFPInfoSpaceHandlerBase_h_

#include <string>

#include "tcds/utils/MultiInfoSpaceHandler.h"

namespace tcds {
  namespace utils {
    class Monitor;
    class InfoSpaceHandler;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace hwutilstca {

    class SFPInfoSpaceUpdater;

    class SFPInfoSpaceHandlerBase : public tcds::utils::MultiInfoSpaceHandler
    {

    public:
      virtual ~SFPInfoSpaceHandlerBase();

    protected:
      SFPInfoSpaceHandlerBase(tcds::utils::XDAQAppBase& xdaqApp,
                              tcds::hwutilstca::SFPInfoSpaceUpdater* updater);

      void createSFPChannel(tcds::utils::InfoSpaceHandler* const handler,
                            std::string const sourceType,
                            unsigned int const idNumber,
                            std::string const name);
      void registerSFPChannel(std::string const& name,
                              std::string const& itemSetName,
                              tcds::utils::Monitor& monitor);

    };

  } // namespace hwutilstca
} // namespace tcds

#endif // _tcds_hwutilstca_SFPInfoSpaceHandlerBase_h_
