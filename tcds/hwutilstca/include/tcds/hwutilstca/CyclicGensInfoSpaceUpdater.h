#ifndef _tcds_hwutilstca_CyclicGensInfoSpaceUpdater_h_
#define _tcds_hwutilstca_CyclicGensInfoSpaceUpdater_h_

#include "tcds/utils/HwInfoSpaceUpdaterBase.h"

namespace tcds {
  namespace hwlayertca {
    class TCADeviceBase;
  }
}

namespace tcds {
  namespace utils {
    class InfoSpaceHandler;
    class InfoSpaceItem;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace hwutilstca {

    class CyclicGensInfoSpaceUpdater : public tcds::utils::HwInfoSpaceUpdaterBase
    {

    public:
      CyclicGensInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                 tcds::hwlayertca::TCADeviceBase const& hw);
      virtual ~CyclicGensInfoSpaceUpdater();

      virtual bool updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                       tcds::utils::InfoSpaceHandler* const infoSpaceHandler);

    private:
      tcds::hwlayertca::TCADeviceBase const& getHw() const;

    };

  } // namespace hwutilstca
} // namespace tcds

#endif // _tcds_hwutilstca_CyclicGensInfoSpaceUpdater_h_
