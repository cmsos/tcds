#ifndef _tcds_hwutilstca_SFPDataPoint_h_
#define _tcds_hwutilstca_SFPDataPoint_h_

#include <string>

namespace tcds {
  namespace hwutilstca {

    class SFPDataPoint
    {
    public:
      SFPDataPoint(bool const modAbs,
                   bool const txDisable,
                   bool const txFault,
                   bool const rxLOS,
                   std::string const& vendorName,
                   std::string const& vendorPartNumber,
                   std::string const& vendorRevision,
                   std::string const& vendorSerialNumber,
                   float const sfpTemperature,
                   float const sfpVcc,
                   float const sfpTXBiasCurrent,
                   float const sfpTXPower,
                   float const sfpRXPower);

      bool modAbs() const;
      bool txDisable() const;
      bool txFault() const;
      bool rxLOS() const;

      std::string vendorName() const;
      std::string vendorPartNumber() const;
      std::string vendorRevision() const;
      std::string vendorSerialNumber() const;

      float temperature() const;
      float vcc() const;
      float txBiasCurrent() const;
      float txPower() const;
      float rxPower() const;

      bool isCompatible(SFPDataPoint const& other) const;
      bool haveConditionsChanged(SFPDataPoint const& other) const;
      bool isSameDevice(SFPDataPoint const& other) const;

    private:
      bool modAbs_;
      bool txDisable_;
      bool txFault_;
      bool rxLOS_;
      std::string vendorName_;
      std::string vendorPartNumber_;
      std::string vendorRevision_;
      std::string vendorSerialNumber_;
      float sfpTemperature_;
      float sfpVcc_;
      float sfpTXBiasCurrent_;
      float sfpTXPower_;
      float sfpRXPower_;
    };

  } // namespace hwutilstca
} // namespace tcds

#endif // _tcds_pi_SFPDataPoint_h_
