#ifndef _tcds_hwutilstca_SFPInfoSpaceUpdater_h_
#define _tcds_hwutilstca_SFPInfoSpaceUpdater_h_

#include <map>
#include <memory>
#include <stddef.h>
#include <stdint.h>
#include <string>
#include <utility>

#include "toolbox/TimeInterval.h"
#include "toolbox/TimeVal.h"

#include "tcds/hwlayertca/I2CPlayer.h"
#include "tcds/hwlayertca/TCADeviceBase.h"
#include "tcds/hwutilstca/SFPDataPointCollection.h"
#include "tcds/utils/HwInfoSpaceUpdaterBase.h"

namespace tcds {
  namespace utils {
    class InfoSpaceHandler;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace hwutilstca {

    class SFPInfoSpaceUpdater : public tcds::utils::HwInfoSpaceUpdaterBase
    {

    public:
      SFPInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                          tcds::hwlayertca::TCADeviceBase const& hw);

    protected:
      virtual void updateInfoSpaceImpl(tcds::utils::InfoSpaceHandler* const infoSpaceHandler);

    private:
      std::unique_ptr<tcds::hwlayertca::I2CPlayer> i2cPlayerP_;

      // Several parameters determining:
      // - How many single measurements to average into each data
      //   point.
      size_t numAvg_;
      // - The dead-band (in percent) applied to data points before
      //   updating.
      float deadbandPercent_;
      // - The dead-band override time-out. If the value does not exit
      //   the dead-band within this period, the data point is updated
      //   anyway.
      toolbox::TimeInterval deadbandOverrideTime_;

      // Our memory of the previous data points we took.
      // NOTE: Since each infospace updater can be used for multiple
      // SFPs, this information needs to be indexed by SFP.
      std::map<std::pair<std::string, uint32_t>, tcds::hwutilstca::SFPDataPointCollection> dataPoints_;
      std::map<std::pair<std::string, uint32_t>, tcds::hwutilstca::SFPDataPointCollection> dataPointsPrev_;
      std::map<std::pair<std::string, uint32_t>, toolbox::TimeVal> updateTimestamps_;

      // This variable is used to keep track of hardware
      // (dis)connections. It helps us provide a preliminary
      // single-measurement data point every time we (re)connect to
      // the hardware. (As opposed to waiting for a full found of
      // averaging.)
      std::map<std::pair<std::string, uint32_t>, bool> firstConnection_;

      tcds::hwlayertca::TCADeviceBase const& getHw() const;

      void updateValues(tcds::utils::InfoSpaceHandler* const infoSpaceHandler) const;
      void store(tcds::utils::InfoSpaceHandler* const infoSpaceHandler) const;
      void startOver(tcds::utils::InfoSpaceHandler* const infoSpaceHandler,
                     toolbox::TimeVal const& timeNow);
    };

  } // namespace hwutilstca
} // namespace tcds

#endif // _tcds_pi_SFPInfoSpaceUpdater_h_
