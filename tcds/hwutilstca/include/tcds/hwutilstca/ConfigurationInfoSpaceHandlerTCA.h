#ifndef _tcds_hwutilstca_ConfigurationInfoSpaceHandlerTCA_h_
#define _tcds_hwutilstca_ConfigurationInfoSpaceHandlerTCA_h_

#include <string>

#include "tcds/utils/ConfigurationInfoSpaceHandler.h"

namespace xdaq {
  class Application;
}

namespace tcds {
  namespace utils {
    class Monitor;
  }
}

namespace tcds {
  namespace hwutilstca {

    class ConfigurationInfoSpaceHandlerTCA :
      public tcds::utils::ConfigurationInfoSpaceHandler
    {

    public:
      ConfigurationInfoSpaceHandlerTCA(xdaq::Application& xdaqApp,
                                       std::string const& name="tcds-application-config-tca");

    protected:
      virtual void registerItemSetsWithMonitor(tcds::utils::Monitor& monitor);

    };

  } // namespace hwutilstca
} // namespace tcds

#endif // _tcds_hwutilstca_ConfigurationInfoSpaceHandlerTCA_h_
