#ifndef _tcds_hwutilstca_Utils_h_
#define _tcds_hwutilstca_Utils_h_

#include <string>

#include "tcds/hwutilstca/Definitions.h"

namespace tcds {
  namespace hwlayertca {
    class TCADeviceBase;
  }
}

namespace tcds {
  namespace utils {
    class ConfigurationInfoSpaceHandler;
  }
}

namespace tcds {
  namespace hwutilstca {

    void tcaDeviceHwConnectImpl(tcds::utils::ConfigurationInfoSpaceHandler const& cfgInfoSpace,
                                tcds::hwlayertca::TCADeviceBase& hw);

    // Mapping of cyclic generator operating modes to strings.
    std::string cyclicGenModeToString(tcds::definitions::CYCLIC_GEN_MODE const mode);

    // Mapping of cyclic generator synchronisation modes to strings.
    std::string cyclicGenSyncModeToString(tcds::definitions::CYCLIC_GEN_SYNC_MODE const mode);

    // Mapping of cyclic generator pause behaviours to strings.
    std::string cyclicGenPauseBehaviourToString(tcds::definitions::CYCLIC_GEN_PAUSE_BEHAVIOUR const mode);

  } // namespace hwutilstca
} // namespace tcds

#endif // _tcds_hwutilstca_Utils_h_
