#ifndef _tcds_hwutilstca_HwStatusInfoSpaceUpdaterTCA_h_
#define _tcds_hwutilstca_HwStatusInfoSpaceUpdaterTCA_h_

#include "tcds/utils/HwInfoSpaceUpdaterBase.h"

namespace tcds {
  namespace hwlayertca {
    class TCADeviceBase;
  }
}

namespace tcds {
  namespace utils {
    class InfoSpaceHandler;
    class InfoSpaceItem;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace hwutilstca {

    // BUG BUG BUG
    // Maybe this should be called HwStatusInfoSpaceUpdaterTCABase(?).
    // BUG BUG BUG end
    class HwStatusInfoSpaceUpdaterTCA : public tcds::utils::HwInfoSpaceUpdaterBase
    {

    public:
      virtual ~HwStatusInfoSpaceUpdaterTCA();

      virtual bool updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                       tcds::utils::InfoSpaceHandler* const infoSpaceHandler);

    protected:
      HwStatusInfoSpaceUpdaterTCA(tcds::utils::XDAQAppBase& xdaqApp,
                                  tcds::hwlayertca::TCADeviceBase const& hw);

    private:
      tcds::hwlayertca::TCADeviceBase const& getHw() const;

    };

  } // namespace hwutilstca
} // namespace tcds

#endif // _tcds_hwutilstca_HwStatusInfoSpaceUpdaterTCA_h_
