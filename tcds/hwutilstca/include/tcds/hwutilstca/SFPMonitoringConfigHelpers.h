#ifndef _tcds_hwutilstca_SFPMonitoringConfigHelpers_h_
#define _tcds_hwutilstca_SFPMonitoringConfigHelpers_h_

namespace tcds {
  namespace utils {
    class InfoSpaceHandler;
    class Monitor;
  }
}

namespace tcds {
  namespace hwutilstca {

    void SFPMonitoringConfigCreateItems(tcds::utils::InfoSpaceHandler* const handler);
    void SFPMonitoringRegisterItemsWithMonitor(tcds::utils::InfoSpaceHandler* const handler,
                                               tcds::utils::Monitor& monitor);

  } // namespace hwutilstca
} // namespace tcds

#endif // _tcds_hwutilstca_SFPMonitoringConfigHelpers_h_
