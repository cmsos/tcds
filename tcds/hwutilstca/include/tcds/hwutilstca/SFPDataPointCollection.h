#ifndef _tcds_hwutilstca_SFPDataPointCollection_h_
#define _tcds_hwutilstca_SFPDataPointCollection_h_

#include <cstddef>
#include <string>
#include <vector>

#include "tcds/hwutilstca/SFPDataPoint.h"

namespace tcds {
  namespace hwutilstca {

    class SFPDataPointCollection
    {

    public:
      SFPDataPointCollection();

      void addDataPoint(tcds::hwutilstca::SFPDataPoint const& dataPoint);
      tcds::hwutilstca::SFPDataPoint back() const;

      bool modAbs() const;
      bool txDisable() const;
      bool txFault() const;
      bool rxLOS() const;

      std::string vendorName() const;
      std::string vendorPartNumber() const;
      std::string vendorRevision() const;
      std::string vendorSerialNumber() const;

      float temperature() const;
      float vcc() const;
      float txBiasCurrent() const;
      float txPower() const;
      float rxPower() const;

      size_t size() const;
      void clear();

      bool differsFrom(SFPDataPointCollection const& other, float const perc) const;

    private:
      std::vector<tcds::hwutilstca::SFPDataPoint> dataPoints_;

      static float percDiff(float const self, float const other);

    };

  } // namespace hwutilstca
} // namespace tcds

#endif // _tcds_hwutilstca_SFPDataPointCollection_h_
