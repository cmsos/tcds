#include "tcds/hwlayervme/VMEDeviceBase.h"

#include <sstream>
#include <utility>

#include "tcds/hwlayervme/Utils.h"

tcds::hwlayervme::VMEDeviceBase::VMEDeviceBase()
{
}

tcds::hwlayervme::VMEDeviceBase::~VMEDeviceBase()
{
}

bool
tcds::hwlayervme::VMEDeviceBase::isReadyForUseImpl() const
{
  return isHwConnected();
}

void
tcds::hwlayervme::VMEDeviceBase::hwConnect(std::string const& addressTableFileName,
                                           int const unitNumber,
                                           int const chainNumber,
                                           uint32_t const slotNumber)
{
  hwConnectImpl(addressTableFileName, unitNumber, chainNumber, slotNumber);
}

void
tcds::hwlayervme::VMEDeviceBase::hwConnectImpl(std::string const& addressTableFileName,
                                               int const unitNumber,
                                               int const chainNumber,
                                               uint32_t const slotNumber)
{
  hwDevice_.hwConnect(addressTableFileName, unitNumber, chainNumber, getBaseAddress(slotNumber));
}

void
tcds::hwlayervme::VMEDeviceBase::hwRelease()
{
  hwReleaseImpl();
}

void
tcds::hwlayervme::VMEDeviceBase::hwReleaseImpl()
{
  hwDevice_.hwRelease();
}

tcds::hwlayer::RegDumpVec
tcds::hwlayervme::VMEDeviceBase::dumpRegisterContentsImpl() const
{
  return hwDevice_.dumpRegisterContents();
}

bool
tcds::hwlayervme::VMEDeviceBase::isHwConnected() const
{
  return hwDevice_.isHwConnected();
}

std::string
tcds::hwlayervme::VMEDeviceBase::readBoardID() const
{
  return readBoardIDImpl();
}

std::string
tcds::hwlayervme::VMEDeviceBase::readBoardIDImpl() const
{
  uint32_t const tmpVal = hwDevice_.readRegister("board_id");
  BOARD_ID_TYPE tmp = static_cast<BOARD_ID_TYPE>(tmpVal);
  return boardTypeToString(tmp);
}

std::string
tcds::hwlayervme::VMEDeviceBase::readFirmwareVersion() const
{
  return readFirmwareVersionImpl();
}

std::string
tcds::hwlayervme::VMEDeviceBase::readFirmwareVersionImpl() const
{
  uint32_t const tmpVal = hwDevice_.readRegister("firmware_id");
  std::stringstream res;
  res << tmpVal;
  return res.str();
}

std::vector<std::string>
tcds::hwlayervme::VMEDeviceBase::getRegisterNamesImpl() const
{
  std::vector<std::string> res = hwDevice_.getRegisterNames();
  return res;
}

tcds::hwlayer::RegisterInfo::RegInfoVec
tcds::hwlayervme::VMEDeviceBase::getRegisterInfosImpl() const
{
  tcds::hwlayer::RegisterInfo::RegInfoVec res = hwDevice_.getRegisterInfos();
  return res;
}

uint32_t
tcds::hwlayervme::VMEDeviceBase::readRegisterImpl(std::string const& regName) const
{
  return hwDevice_.readRegister(regName);
}

void
tcds::hwlayervme::VMEDeviceBase::writeRegisterImpl(std::string const& regName,
                                                   uint32_t const regVal) const
{
  hwDevice_.writeRegister(regName, regVal);
}

std::vector<uint32_t>
tcds::hwlayervme::VMEDeviceBase::readBlockImpl(std::string const& regName,
                                               uint32_t const nWords) const
{
  std::vector<uint32_t> const res = hwDevice_.readBlock(regName, nWords);
  return res;
}

std::vector<uint32_t>
tcds::hwlayervme::VMEDeviceBase::readBlockOffsetImpl(std::string const& regName,
                                                     uint32_t const nWords,
                                                     uint32_t const offset) const
{
  std::vector<uint32_t> const res = hwDevice_.readBlockOffset(regName, nWords, offset);
  return res;
}

void
tcds::hwlayervme::VMEDeviceBase::writeBlockImpl(std::string const& regName,
                                                std::vector<uint32_t> const& regVals) const
{
  hwDevice_.writeBlock(regName, regVals);
}

uint32_t
tcds::hwlayervme::VMEDeviceBase::getBlockSizeImpl(std::string const& regName) const
{
  return hwDevice_.getBlockSize(regName);
}

void
tcds::hwlayervme::VMEDeviceBase::writeHardwareConfigurationImpl(tcds::hwlayer::ConfigurationProcessor::RegValVec const& cfg) const
{
  for (tcds::hwlayer::ConfigurationProcessor::RegValVec::const_iterator it = cfg.begin();
       it != cfg.end();
       ++it)
    {
      // NOTE: Some care is required here. For single-word writes the
      // appropriate mask (specified in the address table) is
      // applied. Block writes don't know about masks.
      if (it->second.size() == 1)
        {
          writeRegister(it->first, it->second.at(0));
        }
      else
        {
          writeBlock(it->first, it->second);
        }
    }
}

tcds::hwlayer::DeviceBase::RegContentsVec
tcds::hwlayervme::VMEDeviceBase::readHardwareConfigurationImpl(tcds::hwlayer::RegisterInfo::RegInfoVec const& regInfos) const
{
  tcds::hwlayer::DeviceBase::RegContentsVec res;
  for (tcds::hwlayer::RegisterInfo::RegInfoVec::const_iterator regInfo = regInfos.begin();
       regInfo != regInfos.end();
       ++regInfo)
    {
      std::vector<uint32_t> regVals;
      // NOTE: Some care is required here. For single-word reads the
      // appropriate mask (specified in the address table) is
      // applied. Block reads don't know about masks.
      uint32_t const size = getBlockSize(regInfo->name());
      if (size == 1)
        {
          regVals.push_back(readRegister(regInfo->name()));
        }
      else
        {
          regVals = readBlock(regInfo->name());
        }

      res.push_back(std::make_pair(*regInfo, regVals));
    }

  return res;
}
