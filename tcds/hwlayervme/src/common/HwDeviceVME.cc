#include "tcds/hwlayervme/HwDeviceVME.h"

#include <cassert>
#include <tr1/unordered_map>
#include <utility>

#include "hal/AddressTableItem.hh"
#include "hal/CAENLinuxBusAdapter.hh"
#include "hal/GeneralHardwareAddress.hh"
#include "hal/HardwareDeviceInterface.hh"
#include "hal/VMEAddressTable.hh"
#include "hal/VMEAddressTableASCIIReader.hh"
#include "hal/VMEDevice.hh"
#include "toolbox/string.h"
#include "xcept/Exception.h"

#include "tcds/exception/Exception.h"
#include "tcds/utils/Utils.h"

namespace HAL {
  template <class _Key> struct HalHash;
}

tcds::hwlayervme::HwDeviceVME::HwDeviceVME()
{
}

tcds::hwlayervme::HwDeviceVME::~HwDeviceVME()
{
}

void
tcds::hwlayervme::HwDeviceVME::hwConnect(std::string const& addressTableFileName,
                                         int const unitNumber,
                                         int const chainNumber,
                                         uint32_t const baseAddress)
{
  std::unique_ptr<HAL::VMEAddressTable> addrTableTmpP;
  try
    {
      // Read in the address table.
      std::string const tmp = tcds::utils::expandPathName(addressTableFileName);
      HAL::VMEAddressTableASCIIReader addressTableReader(tmp);
      addrTableTmpP = std::unique_ptr<HAL::VMEAddressTable>(new HAL::VMEAddressTable("dummy", addressTableReader));
    }
  catch (xcept::Exception const& err)
    {
      std::string const msgBase =
        "Could not read address table";
      std::string msg = toolbox::toString("%s: %s.", msgBase.c_str(), err.what());
      XCEPT_RAISE(tcds::exception::ConfigurationProblem, msg);
    }
  addrTableP_ = std::move(addrTableTmpP);

  std::unique_ptr<HAL::CAENLinuxBusAdapter> busAdapterTmpP;
  try
    {
      busAdapterTmpP = std::unique_ptr<HAL::CAENLinuxBusAdapter>(new HAL::CAENLinuxBusAdapter(HAL::CAENLinuxBusAdapter::V2718,
                                                                                            unitNumber,
                                                                                            chainNumber,
                                                                                            HAL::CAENLinuxBusAdapter::A3818));
    }
  catch (xcept::Exception const& err)
    {
      std::string const msgBase =
        "Could not access CAEN VME bridge";
      std::string msg = toolbox::toString("%s: %s.", msgBase.c_str(), err.what());
      XCEPT_RAISE(tcds::exception::HardwareProblem, msg);
    }
  busAdapterP_ = std::move(busAdapterTmpP);

  std::unique_ptr<HAL::VMEDevice> hwPTmp;
  try
    {
      hwPTmp = std::unique_ptr<HAL::VMEDevice>(new HAL::VMEDevice(*addrTableP_, *busAdapterP_, baseAddress));
    }
  catch (xcept::Exception const& err)
    {
      std::string const msgBase =
        "Could not connect to the hardware";
      std::string msg = toolbox::toString("%s: %s.", msgBase.c_str(), err.what());
      XCEPT_RAISE(tcds::exception::HardwareProblem, msg);
    }

  hwP_ = std::move(hwPTmp);
}

void
tcds::hwlayervme::HwDeviceVME::hwRelease()
{
  hwP_.reset();
  addrTableP_.reset();
  busAdapterP_.reset();
}

bool
tcds::hwlayervme::HwDeviceVME::isHwConnected() const
{
  return (hwP_.get() != 0);
}

uint32_t
tcds::hwlayervme::HwDeviceVME::readRegister(std::string const& regName) const
{
  return readRegister(regName, 0);
}

uint32_t
tcds::hwlayervme::HwDeviceVME::readRegister(std::string const& regName,
                                            uint32_t const offset) const
{
  HAL::VMEDevice& hw = getHwDevice();
  uint32_t res;
  try
    {
      // NOTE: The '4 *' is because of the 8-bit -> 32-bit transition.
      hw.read(regName, &res, 4 * offset);
    }
  catch (xcept::Exception const& err)
    {
      std::string msgBase = toolbox::toString("Could not read register '%s'", regName.c_str());
      std::string msg = toolbox::toString("%s: %s.", msgBase.c_str(), err.what());
      XCEPT_RAISE(tcds::exception::HardwareProblem, toolbox::toString("%s.", msg.c_str()));
    }
  return res;
}

void
tcds::hwlayervme::HwDeviceVME::writeRegister(std::string const& regName,
                                             uint32_t const regVal) const
{
  HAL::VMEDevice& hw = getHwDevice();
  try
    {
      hw.write(regName, regVal);
    }
  catch (xcept::Exception const& err)
    {
      std::string msgBase = toolbox::toString("Could not write register '%s'", regName.c_str());
      std::string msg = toolbox::toString("%s: %s.", msgBase.c_str(), err.what());
      XCEPT_RAISE(tcds::exception::HardwareProblem, toolbox::toString("%s.", msg.c_str()));
    }
}

std::vector<uint32_t>
tcds::hwlayervme::HwDeviceVME::readBlock(std::string const& regName,
                                         uint32_t const nWords) const
{
  return readBlock(regName, nWords, 0, false);
}

std::vector<uint32_t>
tcds::hwlayervme::HwDeviceVME::readBlock(std::string const& regName,
                                         bool const fakeBlockTransfer) const
{
  return readBlock(regName, getBlockSize(regName), 0, fakeBlockTransfer);
}

std::vector<uint32_t>
tcds::hwlayervme::HwDeviceVME::readBlock(std::string const& regName,
                                         uint32_t const nWords,
                                         uint32_t const offset,
                                         bool const fakeBlockTransfer) const
{
 // NOTE: Some hardware, like the BOBR, does not do VME block
 // transfers. So hence the slightly clumsy approach with 'fake block
 // transfers' here.
  // NOTE: The HAL requires that we specify sizes in bytes, not in
  // words.
  uint32_t const numBytes = nWords;
  // ASSERT ASSERT ASSERT
  // So far no one has implemented 'half-uint32_t' VME reads...
  assert ((numBytes % 4) == 0);
  // ASSERT ASSERT ASSERT end
  HAL::VMEDevice& hw = getHwDevice();
  std::vector<uint32_t> res;

  try
    {
      if (!fakeBlockTransfer)
        {
          std::vector<char> tmp(numBytes, 0);
          hw.readBlock(regName, numBytes, &tmp.at(0), HAL::HAL_DO_INCREMENT, offset);
          uint32_t* const tmpPtr = reinterpret_cast<uint32_t*>(&tmp.at(0));
          // NOTE: The '4 *' is because of the 8-bit -> 32-bit
          // transition.
          res = std::vector<uint32_t>(tmpPtr, tmpPtr + (4 * numBytes));
        }
      else
        {
          for (uint32_t i=0; i != numBytes; ++i)
            {
              std::vector<uint32_t> tmp(numBytes, 0);
              hw.read(regName, &(tmp.at(i)), offset + i);
              res = tmp;
            }
        }
    }
  catch (xcept::Exception const& err)
    {
      std::string msgBase = toolbox::toString("Could not read block '%s'", regName.c_str());
      std::string msg = toolbox::toString("%s: %s.", msgBase.c_str(), err.what());
      XCEPT_RAISE(tcds::exception::HardwareProblem, toolbox::toString("%s.", msg.c_str()));
    }

  return res;
}

std::vector<uint32_t>
tcds::hwlayervme::HwDeviceVME::readBlockOffset(std::string const& regName,
                                               uint32_t const nWords,
                                               uint32_t const offset) const
{
  return readBlock(regName, nWords, offset, false);
}

void
tcds::hwlayervme::HwDeviceVME::writeBlock(std::string const& regName,
                                          std::vector<uint32_t> const regVal) const
{
  std::vector<char> tmp;
  uint32_t const nBytes = regVal.size() * 4;
  char const* const tmpPtr0 = reinterpret_cast<char const*>(&regVal.at(0));
  char* const tmpPtr1 = const_cast<char*>(tmpPtr0);
  HAL::VMEDevice& hw = getHwDevice();
  try
    {
      hw.writeBlock(regName, nBytes, tmpPtr1);
    }
  catch (xcept::Exception const& err)
    {
      std::string msgBase = toolbox::toString("Could not write block '%s'", regName.c_str());
      std::string msg = toolbox::toString("%s: %s.", msgBase.c_str(), err.what());
      XCEPT_RAISE(tcds::exception::HardwareProblem, toolbox::toString("%s.", msg.c_str()));
    }
}

uint32_t
tcds::hwlayervme::HwDeviceVME::getBlockSize(std::string const& regName) const
{
  // This is a bit of a tricky corner of code. In order to do block
  // transfers one needs to know how large a block is available for
  // transfer. The HAL address tables, however, do not store this
  // information. The default behaviour for this method is to return
  // the width of the AddressTableItem. (For block transfers the
  // number of bytes to transfer has to be a multiple of the
  // dataWidth.) For each 'proper block' a hard-coded mapping from
  // regName to number of bytes has to be included in the code.
  return addrTableP_->getGeneralHardwareAddress(regName).getDataWidth();
}

uint32_t
tcds::hwlayervme::HwDeviceVME::getMask(std::string const& regName) const
{
  return addrTableP_->getMask(regName);
}

std::vector<std::string>
tcds::hwlayervme::HwDeviceVME::getRegisterNames() const
{
  typedef std::tr1::unordered_map<std::string, HAL::AddressTableItem*, HAL::HalHash<std::string> > halmap;
  std::vector<std::string> res;
  for (halmap::const_iterator i = addrTableP_->getItemListBegin();
       i != addrTableP_->getItemListEnd();
       ++i)
    {
      res.push_back(i->first);
    }
  return res;
}

tcds::hwlayer::RegisterInfo::RegInfoVec
tcds::hwlayervme::HwDeviceVME::getRegisterInfos() const
{
  std::vector<std::string> const regNames = getRegisterNames();
  tcds::hwlayer::RegisterInfo::RegInfoVec regInfos;
  for (std::vector<std::string>::const_iterator regName = regNames.begin();
       regName != regNames.end();
       ++regName)
    {
      HAL::AddressTableItem const& node = addrTableP_->checkItem(*regName);
      bool const isReadable = node.isReadable();
      bool const isWritable = node.isWritable();
      tcds::hwlayer::RegisterInfo regInfo(*regName, isReadable, isWritable);
      regInfos.push_back(regInfo);
    }
  return regInfos;
}

tcds::hwlayer::RegDumpVec
tcds::hwlayervme::HwDeviceVME::dumpRegisterContents() const
{
  tcds::hwlayer::RegDumpVec res;

  tcds::hwlayer::RegisterInfo::RegInfoVec regInfos = getRegisterInfos();
  for (tcds::hwlayer::RegisterInfo::RegInfoVec::const_iterator regInfo = regInfos.begin();
       regInfo != regInfos.end();
       ++regInfo)
    {
      if (regInfo->isReadable())
        {
          std::vector<uint32_t> regVals;
          // NOTE: Some care is required here. For single-word reads
          // the appropriate mask (specified in the address table) is
          // applied. Block reads don't know about masks.
          uint32_t const size = getBlockSize(regInfo->name());
          if (size == 1)
            {
              regVals.push_back(readRegister(regInfo->name()));
            }
          else
            {
              regVals = readBlock(regInfo->name());
            }
          res.push_back(std::make_pair(regInfo->name(), regVals));
        }
    }

  return res;
}

HAL::VMEDevice&
tcds::hwlayervme::HwDeviceVME::getHwDevice() const
{
  if (!isHwConnected())
    {
      std::string msg = "Trying to control the hardware, "
        "but the XDAQ control application is not (yet) connected "
        "to the hardware. (Unless something is very wrong, "
        "this simply means you have to 'Configure' first.)";
      XCEPT_RAISE(tcds::exception::SoftwareProblem, msg);
    }
  else
    {
      HAL::VMEDevice& hw = static_cast<HAL::VMEDevice&>(*hwP_);
      return hw;
    }
}
