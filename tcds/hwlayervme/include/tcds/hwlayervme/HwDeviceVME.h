#ifndef _tcds_hwlayervme_HwDeviceVME_h_
#define _tcds_hwlayervme_HwDeviceVME_h_

#include <memory>
#include <stdint.h>
#include <string>
#include <vector>

#include "tcds/hwlayer/IHwDevice.h"
#include "tcds/hwlayer/RegisterInfo.h"

namespace HAL {
  class CAENLinuxBusAdapter;
  class VMEAddressTable;
  class VMEDevice;
}

namespace tcds {
  namespace hwlayervme {

    /**
     * Hardware access class for VME devices.
     */
    class HwDeviceVME : public tcds::hwlayer::IHwDevice
    {

    public:
      HwDeviceVME();
      virtual ~HwDeviceVME();

      void hwConnect(std::string const& addressTableFileName,
                     int const unitNumber,
                     int const chainNumber,
                     uint32_t const baseAddress);
      void hwRelease();

      bool isHwConnected() const;

      uint32_t readRegister(std::string const& regName) const;
      uint32_t readRegister(std::string const& regName,
                            uint32_t const offset) const;
      void writeRegister(std::string const& regName,
                         uint32_t const regVal) const;
      std::vector<uint32_t> readBlock(std::string const& regName,
                                      uint32_t const nWords) const;
      std::vector<uint32_t> readBlock(std::string const& regName,
                                      bool const fakeBlockTransfer=false) const;
      std::vector<uint32_t> readBlock(std::string const& regName,
                                      uint32_t const nWords,
                                      uint32_t const offset,
                                      bool const fakeBlockTransfer) const;
      std::vector<uint32_t> readBlockOffset(std::string const& regName,
                                            uint32_t const nWords,
                                            uint32_t const offset) const;
      void writeBlock(std::string const& regName,
                      std::vector<uint32_t> const regVal) const;
      uint32_t getBlockSize(std::string const& regName) const;
      uint32_t getMask(std::string const& regName) const;

      std::vector<std::string> getRegisterNames() const;
      tcds::hwlayer::RegisterInfo::RegInfoVec getRegisterInfos() const;

      virtual tcds::hwlayer::RegDumpVec dumpRegisterContents() const;

    protected:
      HAL::VMEDevice& getHwDevice() const;

    private:
      std::unique_ptr<HAL::VMEAddressTable> addrTableP_;
      std::unique_ptr<HAL::CAENLinuxBusAdapter> busAdapterP_;
      std::unique_ptr<HAL::VMEDevice> hwP_;

    };

  } // namespace hwlayervme
} // namespace tcds

#endif // _tcds_hwlayervme_HwDeviceVME_h_
