#ifndef _tcds_hwlayervme_Utils_h_
#define _tcds_hwlayervme_Utils_h_

#include <string>

namespace tcds {
  namespace hwlayervme {

    enum BOARD_ID_TYPE {
      BOARD_ID_RF2TTC=0x16b,
      BOARD_ID_RFRXD=0x16c
    };

    std::string boardTypeToString(BOARD_ID_TYPE const boardType);

  } // namespace hwlayervme
} // namespace tcds

#endif // _tcds_hwlayervme_Utils_h_
