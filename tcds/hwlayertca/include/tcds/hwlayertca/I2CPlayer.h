#ifndef _tcds_hwlayertca_I2CPlayer_h_
#define _tcds_hwlayertca_I2CPlayer_h_

#include <cstddef>
#include <stdint.h>
#include <string>
#include <vector>

namespace tcds {
  namespace hwlayertca {

    class TCADeviceBase;

    class I2CPlayer
    {

    public:

      I2CPlayer(tcds::hwlayertca::TCADeviceBase const& device);

      void enable() const;
      void disable() const;
      void update();
      void verify() const;
      void program() const;

      bool isModulePresent(short const fmcNum, short const sfpNum) const;
      bool isModuleDisabled(short const fmcNum, short const sfpNum) const;
      bool isTXFault(short const fmcNum, short const sfpNum) const;
      bool isRXLOS(short const fmcNum, short const sfpNum) const;

      std::string getSFPVendorName(short const fmcNum, short const sfpNum) const;
      std::string getSFPVendorPartNumber(short const fmcNum, short const sfpNum) const;
      std::string getSFPVendorRevision(short const fmcNum, short const sfpNum) const;
      std::string getSFPVendorSerialNumber(short const fmcNum, short const sfpNum) const;

      float getSFPTemperature(short const fmcNum, short const sfpNum) const;
      float getSFPVcc(short const fmcNum, short const sfpNum) const;
      float getSFPRXPower(short const fmcNum, short const sfpNum) const;
      float getSFPTXPower(short const fmcNum, short const sfpNum) const;
      float getSFPTXBiasCurrent(short const fmcNum, short const sfpNum) const;

    private:

      // Let's hide a little helper as sub-class.
      class I2CPlayerRXValue
      {
      public:
        I2CPlayerRXValue(uint32_t rxData) :
          rxData_(rxData) {};
        int extractDataValue() const
        {
          float res = -1.;
          if (isDone() && !isError())
            {
              if (is16Bit())
                {
                  res = int(rxData_ & uint32_t(0xffff));
                }
              else
                {
                  res = int(rxData_ & uint32_t(0x00ff));
                }
            }
          return res;
        };
        bool isDone() const {return ((rxData_ & uint32_t(0x04000000)) != 0);}
        bool isError() const {return ((rxData_ & uint32_t(0x08000000)) != 0);}
      private:
        uint32_t rxData_;
        bool is16Bit() const {return ((rxData_ & uint32_t(0x02000000)) != 0);}
      };

      // There are 2 FMC.
      static short const kNumFMCs = 2;

      // There are 8 I2C-player general info TX (and thus RX) entries
      // for each FMC.
      static size_t const kNumEntriesPerFMC = 8;

      // These are the offsets inside the general SFP info block.
      static size_t const kOffsetModAbs = 1;
      static size_t const kOffsetDisable = 3;
      static size_t const kOffsetTXFault = 5;
      static size_t const kOffsetRXLOS = 7;

      // There are 58 I2C-player TX (and thus RX) entries for each SFP.
      static short const kNumEntriesPerSFP = 58;
      // There are 8 SFP slots possible on each FMC, even though not
      // all have been implemented.
      static short const kNumSFPsPerFMC = 8;

      // These are the 'per-SFP' offsets inside the detailed
      // diagnostics block.
      // NOTE: The first offset of 1 is due to the 'switch MUX'
      // command.
      static size_t const kOffsetVendorName = 1;
      static size_t const kSizeVendorName = 16;
      static size_t const kOffsetVendorPartNumber = kOffsetVendorName + kSizeVendorName;
      static size_t const kSizeVendorPartNumber = 16;
      static size_t const kOffsetVendorRevision = kOffsetVendorPartNumber + kSizeVendorPartNumber;
      static size_t const kSizeVendorRevision = 4;
      static size_t const kOffsetVendorSerialNumber = kOffsetVendorRevision + kSizeVendorRevision;
      static size_t const kSizeVendorSerialNumber = 16;
      static size_t const kOffsetTemperature = kOffsetVendorSerialNumber + kSizeVendorSerialNumber;
      static size_t const kSizeTemperature = 1;
      static size_t const kOffsetVcc = kOffsetTemperature + kSizeTemperature;
      static size_t const kSizeVcc = 1;

      static size_t const kOffsetTXBiasCurrent = kOffsetVcc + kSizeVcc;
      static size_t const kSizeTXBiasCurrent = 1;

      static size_t const kOffsetTXPower = kOffsetTXBiasCurrent + kSizeTXBiasCurrent;
      static size_t const kSizeTXPower = 1;

      static size_t const kOffsetRXPower = kOffsetTXPower + kSizeTXPower;
      static size_t const kSizeRXPower = 1;

      tcds::hwlayertca::TCADeviceBase const& device_;

      std::vector<uint32_t> baseTXData_;
      size_t baseTXDataOffset_;

      std::vector<uint32_t> rawTXData_;
      std::vector<uint32_t> rawRXData_;

      std::vector<uint32_t> baseTXData() const;

      size_t calcIndexGeneralInfo(short const fmcNum,
                                  size_t const itemOffset) const;
      size_t calcIndexPerSFPInfo(short const fmcNum,
                                 short const sfpNum,
                                 size_t const itemOffset) const;

      std::vector<uint32_t> extractRXData(size_t const indexFirst,
                                          size_t const numEntries) const;
      std::string convertRXDataToString(std::vector<uint32_t> const rxData) const;
      float convertRXDataToTemperature(std::vector<uint32_t> const rxData) const;
      float convertRXDataToVcc(std::vector<uint32_t> const rxData) const;
      float convertRXDataToRXPower(std::vector<uint32_t> const rxData) const;
      float convertRXDataToTXPower(std::vector<uint32_t> const rxData) const;
      float convertRXDataToTXBiasCurrent(std::vector<uint32_t> const rxData) const;

    };

  } // namespace hwlayertca
} // namespace tcds

#endif // _tcds_hwlayertca_I2CPlayer_h_
