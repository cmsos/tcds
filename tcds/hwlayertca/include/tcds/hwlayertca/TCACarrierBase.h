#ifndef _tcds_hwlayertca_TCACarrierBase_h_
#define _tcds_hwlayertca_TCACarrierBase_h_

#include <string>
#include <vector>

namespace tcds {
  namespace hwlayertca {

    class HwDeviceTCA;

    /**
     * Abstract base class for (u)TCA FMC carrier implementations.
     *
     * At the moment specialized into three concrete FMC carrier
     * classes (for the corresponding harware boards):
     * - TCACarrierAMC13 (which is slightly fake)
     * - TCACarrierFC7
     * - TCACarrierGLIB
     *
     * @note
     * This is really an implementation of a 'uTCA carrier board with
     * one or two TCDS FMCs.'
     */
    class TCACarrierBase
    {

    public:
      enum CLOCK_SOURCE {CLOCK_SOURCE_FCLKA, CLOCK_SOURCE_FMC, CLOCK_SOURCE_XTAL};

      virtual ~TCACarrierBase();

      /**
       * Load the desired firmware image. Choices are 'golden' or
       * 'user'.
       */
      void loadFirmwareImage(std::string const& imageName) const;

      /**
       * Board power-up sequence.
       */
      void powerUp() const;

      void selectClockSource(CLOCK_SOURCE const clkSrc) const;
      void resetPLL() const;

      void enableSFPs() const;
      void disableSFPs() const;

      /**
         Read the 48-bit unique identifier of the board.
       */
      std::string readEUI48() const;

      /**
         Read the MAC address of the board.
       */
      std::string readMACAddress() const;

      /**
       * Read the carrier type from the board.
       */
      std::string readCarrierType() const;

      /**
       * Get the list of FMC names.
       */
      std::vector<std::string> fmcNames() const;

      /**
       * Check if a certain FMC (specified by name) is present or not.
       */
      bool isFMCPresent(std::string const& fmcName) const;

      /**
       * Check if a certain FMC (specified by name) reports its
       * power-good flag set.
       */
      bool isFMCPowerGood(std::string const& fmcName) const;

    protected:
      /**
       * @note
       * Protected constructor since this is an abstract base class.
       */
      TCACarrierBase(HwDeviceTCA& device);

      virtual std::string registerNamePrefix() const = 0;

      virtual std::vector<std::string> fmcNamesImpl() const = 0;

      /**
       * Check if a certain FMC (specified by name) is present or not.
       */
      virtual bool isFMCPresentImpl(std::string const& fmcName) const;

      /**
       * Check if a certain FMC (specified by name) reports its
       * power-good flag set.
       */
      virtual bool isFMCPowerGoodImpl(std::string const& fmcName) const;

      virtual void loadFirmwareImageImpl(std::string const& imageName) const = 0;

      virtual void powerUpImpl() const = 0;

      virtual void selectClockSourceImpl(CLOCK_SOURCE const clkSrc) const = 0;

      virtual void enableSFPsImpl() const = 0;
      virtual void disableSFPsImpl() const = 0;

      /**
         The expected carrier type identification string.
      */
      virtual std::string expectedCarrierType() const = 0;

      HwDeviceTCA& hwDevice_;

    };

  } // namespace hwlayertca
} // namespace tcds

#endif // _tcds_hwlayertca_TCACarrierBase_h_
