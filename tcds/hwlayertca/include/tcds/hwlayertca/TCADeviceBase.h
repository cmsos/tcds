#ifndef _tcds_hwlayertca_TCADeviceBase_h_
#define _tcds_hwlayertca_TCADeviceBase_h_

#include <functional>
#include <memory>
#include <stddef.h>
#include <stdint.h>
#include <string>
#include <vector>

#include "tcds/hwlayer/ConfigurationProcessor.h"
#include "tcds/hwlayer/DeviceBase.h"
#include "tcds/hwlayer/IHwDevice.h"
#include "tcds/hwlayer/RegisterInfo.h"
#include "tcds/hwlayertca/TCACarrierBase.h"
#include "tcds/hwlayertca/HwDeviceTCA.h"

namespace tcds {
  namespace hwlayertca {

    /**
     * Abstract base class for all (u)TCA hardware devices.
     */
    class TCADeviceBase : public tcds::hwlayer::DeviceBase
    {

      friend class BootstrapHelper;

      //--------------------------------------------------

      struct RegNameMatchesA :
        public std::unary_function<std::string&, bool>
        {
        RegNameMatchesA(std::string const& prefix) :
          prefix_(prefix), prefixLen_(prefix.size()) {};
          bool operator() (std::string const& regName) const
          {
            return (regName.compare(0, prefixLen_, prefix_) == 0);
          }
        private:
          std::string const prefix_;
          size_t const prefixLen_;
        };

      struct RegNameMatchesB :
        public std::unary_function<tcds::hwlayer::RegisterInfo const&, bool>
        {
        RegNameMatchesB(std::string const& prefix) :
          prefix_(prefix), prefixLen_(prefix.size()) {};
          bool operator() (tcds::hwlayer::RegisterInfo const& regInfo) const
          {
            return (regInfo.name().compare(0, prefixLen_, prefix_) == 0);
          }
        private:
          std::string const prefix_;
          size_t const prefixLen_;
        };

      struct RegNameUnprefixer
        {
        RegNameUnprefixer(std::string const& prefix) :
          prefix_(prefix), prefixLen_(prefix.size()) {};
          std::string operator() (std::string const& regName) const
          {
            return regName.substr(prefixLen_);
          }
          tcds::hwlayer::RegisterInfo operator() (tcds::hwlayer::RegisterInfo const& regInfo) const
          {
            std::string const name = regInfo.name().substr(prefixLen_);
            tcds::hwlayer::RegisterInfo res(name,
                                            regInfo.isReadable(),
                                            regInfo.isWritable());
            return res;
          }
        private:
          std::string const prefix_;
          size_t const prefixLen_;
        };

      //--------------------------------------------------

    public:
      virtual ~TCADeviceBase();

      uint32_t readRegister(std::string const& regName,
                            bool const unprefixed=false) const;
      void writeRegister(std::string const& regName,
                         uint32_t const regVal,
                         bool const unprefixed=false) const;
      std::vector<uint32_t> readBlock(std::string const& regName,
                                      bool const unprefixed=false) const;
      std::vector<uint32_t> readBlock(std::string const& regName,
                                      uint32_t const nWords,
                                      bool const unprefixed=false) const;
      void writeBlock(std::string const& regName,
                      std::vector<uint32_t> const& regVal,
                      bool const unprefixed=false) const;
      uint32_t getBlockSize(std::string const& regName,
                            bool const unprefixed=false) const;

      /**
       * Connect to the hardware. Does whatever is necessary to find
       * the hardware and setup the connection.
       */
      void hwConnect(std::string const& connectionsFileName,
                     std::string const& connectionName);

      /**
       * The inverse of hwConnect. Disconnects from the hardware,
       * releasing it for use by someone/something else.
       */
      void hwRelease();

      bool isHwConnected() const;

      void selectClockSource(tcds::hwlayertca::TCACarrierBase::CLOCK_SOURCE const clkSrc) const;
      void resetPLL() const;
      void enableSFPs() const;
      void disableSFPs() const;
      void switchAllSFPs(bool const switchOn) const;

      std::string readEUI48() const;
      std::string readMACAddress() const;
      std::string readBoardId() const;
      std::string readSystemId() const;
      std::string readSystemFirmwareVersion() const;
      std::string readUserFirmwareVersion() const;
      std::string readSystemFirmwareDate() const;
      std::string readUserFirmwareDate() const;

      bool isTTCClockUp() const;
      bool isTTCClockStable() const;
      uint32_t readTTCClockUnlockCounter() const;

      // BUG BUG BUG
      // This is a little bit cheating, since this phase monitoring is
      // really only implemented in the FC7s, not in the CPM T1/T2.
      bool isPhaseMonLocked() const;
      void enablePhaseMonitoring() const;
      double readPhaseMonitoring40MHz() const;
      double readPhaseMonitoring160MHz() const;
      // BUG BUG BUG end

      std::string regNamePrefix() const;

    protected:
      /**
       * @note
       * Protected constructor since this is an abstract base class.
       * @note
       * The TCADeviceBase class takes ownership of the TCACarrierBase
       * pointer.
       */
      TCADeviceBase(std::unique_ptr<TCACarrierBase> carrier);

      virtual bool isReadyForUseImpl() const;

      uint32_t readModifyWriteRegister(std::string const& regName,
                                       uint32_t const regVal) const;

      bool bootstrapDone() const;
      virtual bool bootstrapDoneImpl() const;

      void runBootstrap() const;
      virtual void runBootstrapImpl() const;

      /**
       * Load the desired firmware image. Choices are 'golden' or
       * 'user'.
       */
      void loadFirmwareImage(std::string const& imageName) const;

      virtual void hwConnectImpl(std::string const& connectionsFileName,
                                 std::string const& connectionName);
      virtual void hwReleaseImpl();

      virtual std::vector<std::string> getRegisterNamesImpl() const;
      virtual tcds::hwlayer::RegisterInfo::RegInfoVec getRegisterInfosImpl() const;

      virtual uint32_t readRegisterImpl(std::string const& regName) const;
      virtual uint32_t readRegisterImpl(std::string const& regName,
                                        bool const unprefixed) const;
      virtual void writeRegisterImpl(std::string const& regName,
                                     uint32_t const regVal) const;
      virtual void writeRegisterImpl(std::string const& regName,
                                     uint32_t const regVal,
                                     bool const unprefixed) const;
      virtual std::vector<uint32_t> readBlockImpl(std::string const& regName,
                                                  uint32_t const nWords) const;
      virtual std::vector<uint32_t> readBlockImpl(std::string const& regName,
                                                  uint32_t const nWords,
                                                  bool const unprefixed) const;
      virtual std::vector<uint32_t> readBlockOffsetImpl(std::string const& regName,
                                                        uint32_t const nWords,
                                                        uint32_t const offset) const;
      virtual void writeBlockImpl(std::string const& regName,
                                  std::vector<uint32_t> const& regVals) const;
      virtual void writeBlockImpl(std::string const& regName,
                                  std::vector<uint32_t> const& regVals,
                                  bool const unprefixed) const;

      virtual uint32_t readModifyWriteRegisterImpl(std::string const& regName,
                                                   uint32_t const regVal) const;

      virtual uint32_t getBlockSizeImpl(std::string const& regName) const;
      virtual uint32_t getBlockSizeImpl(std::string const& regName,
                                        bool const unprefixed) const;

      virtual void writeHardwareConfigurationImpl(tcds::hwlayer::ConfigurationProcessor::RegValVec const& cfg) const;
      virtual tcds::hwlayer::DeviceBase::RegContentsVec readHardwareConfigurationImpl(tcds::hwlayer::RegisterInfo::RegInfoVec const& regInfos) const;

      virtual tcds::hwlayer::RegDumpVec dumpRegisterContentsImpl() const;

      virtual std::string readEUI48Impl() const;
      virtual std::string readMACAddressImpl() const;
      virtual std::string readBoardIdImpl() const;
      virtual std::string readSystemIdImpl() const;
      virtual std::string readSystemFirmwareVersionImpl() const;
      virtual std::string readUserFirmwareVersionImpl() const;
      virtual std::string readSystemFirmwareDateImpl() const;
      virtual std::string readUserFirmwareDateImpl() const;
      virtual bool isTTCClockUpImpl() const;
      virtual bool isTTCClockStableImpl() const;
      virtual uint32_t readTTCClockUnlockCounterImpl() const;

      std::string regNamePrefixed(std::string const& regName) const;
      virtual std::string regNamePrefixImpl() const;

      /*
       * @note
       * The order of hwDevice_ and hwCarrierP_ is important: the
       * former is used when instantiating the latter.
       */
      HwDeviceTCA hwDevice_;
      std::unique_ptr<TCACarrierBase> hwCarrierP_;

    private:
      std::string readFirmwareDate(std::string const& regNamePrefix) const;
      std::string readFirmwareVersion(std::string const& regNamePrefix) const;

      // The measurement duration (i.e., number of iterations) of the
      // White-Rabbit phase measurement.
      static uint32_t const kMeasDuration;
      // The calibration factor (into s) of the White-Rabbit phase
      // measurement.
      static double const kCalFactor;

    };

  } // namespace hwlayertca
} // namespace tcds

#endif // _tcds_hwlayertca_TCADeviceBase_h_
