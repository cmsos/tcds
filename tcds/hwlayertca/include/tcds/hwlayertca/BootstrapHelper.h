#ifndef _tcds_hwlayertca_BootstrapHelper_h_
#define _tcds_hwlayertca_BootstrapHelper_h_

namespace tcds {
  namespace hwlayertca {

    class TCADeviceBase;

    /**
     * Helper class to coordinate bootstrapping procedure for
     * GLIB/FC7-based systems.
     *
     * The 'bootstrap' in this case refers to the sequence of: loading
     * the user firmware image, powering up the board, switching on
     * the SFPs, etc.
     *
     * @note This is an area to tread carefully, since this acts upon
     * a whole board, touching resources shared by multiple XDAQ
     * applications.
     *
     * @note The 'accounting' register in the GLIB/FC7 system logic is
     * used to keep track of the bootstrap.

     */
    class BootstrapHelper
    {

    public:
      BootstrapHelper(tcds::hwlayertca::TCADeviceBase const& device);
      ~BootstrapHelper();

      bool bootstrapDone() const;
      void runBootstrap() const;

    private:
      tcds::hwlayertca::TCADeviceBase const& device_;

      bool bootstrapInProgress() const;

      bool claimBootstrap() const;
      void unsetBootstrapInProgress() const;

      bool setBootstrapDone() const;
      void unsetBootstrapDone() const;

    };

  } // namespace hwlayertca
} // namespace tcds

#endif // _tcds_hwlayertca_BootstrapHelper_h_
