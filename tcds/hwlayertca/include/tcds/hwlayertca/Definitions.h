#ifndef _tcds_hwlayertca_Definitions_h_
#define _tcds_hwlayertca_Definitions_h_

#include <stdint.h>

namespace tcds {
  namespace definitions {

    // The measurement duration (i.e., number of iterations) of the
    // White-Rabbit phase measurement.
    uint32_t const kWRMeasDuration = 0x10;
    // The calibration factor (into s) of the White-Rabbit phase
    // measurement.
    double const kWRCalFactor = (1. / 538) * 1.e-9;

    // L12 is the top FMC on the FC7, L8 is the bottom FMC. SFPs are
    // numbered starting at 1, from top to bottom, and from left to
    // right.
    enum FMC_NUMBER {FMC_NUMBER_L12 = 0,
                     FMC_NUMBER_L8 = 1};
    enum SFP_NUMBER {SFP_NUMBER_L12_SFP1 = 0,
                     SFP_NUMBER_L12_SFP2 = 1,
                     SFP_NUMBER_L12_SFP3 = 2,
                     SFP_NUMBER_L12_SFP4 = 3,
                     SFP_NUMBER_L12_SFP5 = 4,
                     SFP_NUMBER_L12_SFP6 = 5,
                     SFP_NUMBER_L12_SFP7 = 6,
                     SFP_NUMBER_L12_SFP8 = 7,
                     SFP_NUMBER_L8_SFP1 = 8,
                     SFP_NUMBER_L8_SFP2 = 9,
                     SFP_NUMBER_L8_SFP3 = 10,
                     SFP_NUMBER_L8_SFP4 = 11,
                     SFP_NUMBER_L8_SFP5 = 12,
                     SFP_NUMBER_L8_SFP6 = 13,
                     SFP_NUMBER_L8_SFP7 = 14,
                     SFP_NUMBER_L8_SFP8 = 15};

    enum SFP_LPM {SFP_LPM_ICI1 = SFP_NUMBER_L12_SFP1,
                  SFP_LPM_ICI2 = SFP_NUMBER_L12_SFP2,
                  SFP_LPM_ICI3 = SFP_NUMBER_L12_SFP3,
                  SFP_LPM_ICI4 = SFP_NUMBER_L12_SFP4,
                  SFP_LPM_ICI5 = SFP_NUMBER_L12_SFP5,
                  SFP_LPM_ICI6 = SFP_NUMBER_L12_SFP6,
                  SFP_LPM_ICI7 = SFP_NUMBER_L12_SFP7,
                  SFP_LPM_ICI8 = SFP_NUMBER_L12_SFP8,
                  SFP_LPM_DAQ0 = SFP_NUMBER_L8_SFP7,
                  SFP_LPM_DAQ1 = SFP_NUMBER_L8_SFP8};
    SFP_LPM const SFP_LPM_ICI_MIN = SFP_LPM_ICI1;
    SFP_LPM const SFP_LPM_ICI_MAX = SFP_LPM_ICI8;
    SFP_LPM const SFP_LPM_DAQ_MIN = SFP_LPM_DAQ0;
    SFP_LPM const SFP_LPM_DAQ_MAX = SFP_LPM_DAQ1;

    enum SFP_PI {SFP_PI_LPMPRI = SFP_NUMBER_L12_SFP5,
                 SFP_PI_LPMSEC = SFP_NUMBER_L12_SFP6,
                 SFP_PI_FED1 = SFP_NUMBER_L12_SFP7,
                 SFP_PI_FED2 = SFP_NUMBER_L12_SFP8,
                 SFP_PI_FED3 = SFP_NUMBER_L8_SFP1,
                 SFP_PI_FED4 = SFP_NUMBER_L8_SFP2,
                 SFP_PI_FED5 = SFP_NUMBER_L8_SFP3,
                 SFP_PI_FED6 = SFP_NUMBER_L8_SFP4,
                 SFP_PI_FED7 = SFP_NUMBER_L8_SFP5,
                 SFP_PI_FED8 = SFP_NUMBER_L8_SFP6,
                 SFP_PI_FED9 = SFP_NUMBER_L8_SFP7,
                 SFP_PI_FED10 = SFP_NUMBER_L8_SFP8};
    SFP_PI const SFP_PI_LPM_MIN = SFP_PI_LPMPRI;
    SFP_PI const SFP_PI_LPM_MAX = SFP_PI_LPMSEC;
    SFP_PI const SFP_PI_FED_MIN = SFP_PI_FED1;
    SFP_PI const SFP_PI_FED_MAX = SFP_PI_FED10;

    enum SFP_PHASEMON {SFP_PHASEMON_L8_1 = SFP_NUMBER_L8_SFP1,
                       SFP_PHASEMON_L8_2 = SFP_NUMBER_L8_SFP2,
                       SFP_PHASEMON_L8_3 = SFP_NUMBER_L8_SFP3,
                       SFP_PHASEMON_L8_4 = SFP_NUMBER_L8_SFP4,
                       SFP_PHASEMON_L8_5 = SFP_NUMBER_L8_SFP5,
                       SFP_PHASEMON_L8_6 = SFP_NUMBER_L8_SFP6,
                       SFP_PHASEMON_L8_7 = SFP_NUMBER_L8_SFP7,
                       SFP_PHASEMON_L8_8 = SFP_NUMBER_L8_SFP8};
    SFP_PHASEMON const SFP_PHASEMON_MIN = SFP_PHASEMON_L8_1;
    SFP_PHASEMON const SFP_PHASEMON_MAX = SFP_PHASEMON_L8_8;

    std::string const kSFP_TYPE_PHASEMON = "phasemon";
    std::string const kSFP_TYPE_LPM_ICI = "lpm_ici";
    std::string const kSFP_TYPE_LPM_DAQ = "lpm_daq";
    std::string const kSFP_TYPE_PI_LPM = "pi_lpm";
    std::string const kSFP_TYPE_PI_FED = "pi_fed";

  } // namespace definitions
} // namespace tcds

#endif // _tcds_hwlayertca_Definitions_h_
