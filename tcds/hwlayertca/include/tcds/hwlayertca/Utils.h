#ifndef _tcds_hwlayertca_Utils_h_
#define _tcds_hwlayertca_Utils_h_

#include <map>
#include <string>

#include "tcds/hwlayertca/Definitions.h"

namespace tcds {
  namespace hwlayertca {

    // Mapping of SFPs to register names for the LPM.
    std::map<tcds::definitions::SFP_LPM, std::string> sfpRegNameMapLPM();
    std::string sfpToRegName(tcds::definitions::SFP_LPM const sfp);
    std::string sfpToLongName(tcds::definitions::SFP_LPM const sfp);
    std::string sfpToShortName(tcds::definitions::SFP_LPM const sfp);

    // Mapping of SFPs to register names for the PI.
    std::map<tcds::definitions::SFP_PI, std::string> sfpRegNameMapPI();
    std::string sfpToRegName(tcds::definitions::SFP_PI const sfp);
    std::string sfpToLongName(tcds::definitions::SFP_PI const sfp);
    std::string sfpToShortName(tcds::definitions::SFP_PI const sfp);

    // Mapping of SFPs to register names for the PhaseMon.
    std::map<tcds::definitions::SFP_PHASEMON, std::string> sfpRegNameMapPhaseMon();
    std::string sfpToRegName(tcds::definitions::SFP_PHASEMON const sfp);
    std::string sfpToLongName(tcds::definitions::SFP_PHASEMON const sfp);
    std::string sfpToShortName(tcds::definitions::SFP_PHASEMON const sfp);

  } // namespace hwlayertca
} // namespace tcds

#endif // _tcds_hwlayertca_Utils_h_
