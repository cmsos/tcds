#include "tcds/hwlayertca/TCACarrierBase.h"

#include <cstdio>
#include <stdint.h>
#include <vector>

#include "xcept/Exception.h"

#include "tcds/exception/Exception.h"
#include "tcds/hwlayer/Utils.h"
#include "tcds/hwlayertca/HwDeviceTCA.h"

tcds::hwlayertca::TCACarrierBase::TCACarrierBase(HwDeviceTCA& device) :
  hwDevice_(device)
{
}

tcds::hwlayertca::TCACarrierBase::~TCACarrierBase()
{
}

std::string
tcds::hwlayertca::TCACarrierBase::readEUI48() const
{
  uint32_t tmpHi = hwDevice_.readRegister("system.eui.hi");
  uint32_t tmpLo = hwDevice_.readRegister("system.eui.lo");
  uint8_t byte0 = ((tmpHi & 0x0000ff00) >> 8);
  uint8_t byte1 = (tmpHi & 0x000000ff);
  uint8_t byte2 = ((tmpLo & 0xff000000) >> 24);
  uint8_t byte3 = ((tmpLo & 0x00ff0000) >> 16);
  uint8_t byte4 = ((tmpLo & 0x0000ff00) >> 8);
  uint8_t byte5 = (tmpLo & 0x000000ff);
  char tmp[18];
  snprintf(tmp, sizeof(tmp), "%02x:%02x:%02x:%02x:%02x:%02x",
           byte0, byte1, byte2, byte3, byte4, byte5);
  std::string res(tmp);
  return res;
}

std::string
tcds::hwlayertca::TCACarrierBase::readMACAddress() const
{
  uint32_t tmpHi = hwDevice_.readRegister("system.mac_address.hi");
  uint32_t tmpLo = hwDevice_.readRegister("system.mac_address.lo");
  uint8_t byte0 = ((tmpHi & 0x0000ff00) >> 8);
  uint8_t byte1 = (tmpHi & 0x000000ff);
  uint8_t byte2 = ((tmpLo & 0xff000000) >> 24);
  uint8_t byte3 = ((tmpLo & 0x00ff0000) >> 16);
  uint8_t byte4 = ((tmpLo & 0x0000ff00) >> 8);
  uint8_t byte5 = (tmpLo & 0x000000ff);
  char tmp[18];
  snprintf(tmp, sizeof(tmp), "%02x:%02x:%02x:%02x:%02x:%02x",
           byte0, byte1, byte2, byte3, byte4, byte5);
  std::string res(tmp);
  return res;
}

void
tcds::hwlayertca::TCACarrierBase::loadFirmwareImage(std::string const& imageName) const
{
  loadFirmwareImageImpl(imageName);
}

void
tcds::hwlayertca::TCACarrierBase::powerUp() const
{
  // First thing to do is to make sure that we are indeed connected to
  // the expected carrier type.
  std::string const carrierType = readCarrierType();
  std::string const expectedCarrierType = this->expectedCarrierType();
  if (carrierType != expectedCarrierType)
    {
      std::string const msg =
        "Expected carrier type '" +
        expectedCarrierType +
        "' but the hardware is of carrier type '" +
        carrierType +
        "'";
      XCEPT_RAISE(tcds::exception::HardwareProblem, msg.c_str());
    }

  //----------

  // Now call the customised (i.e., the actual) part of the power-up
  // sequence.
  powerUpImpl();
}

void
tcds::hwlayertca::TCACarrierBase::selectClockSource(CLOCK_SOURCE const clkSrc) const
{
  // Switch clock sources.
  selectClockSourceImpl(clkSrc);

  // Reset the TTC-clock PLL.
  resetPLL();
}

void
tcds::hwlayertca::TCACarrierBase::resetPLL() const
{
  std::string const regName = "user.ttc_clock_pll_reset";
  hwDevice_.writeRegister(regName, 0x1);
  hwDevice_.writeRegister(regName, 0x0);
  tcds::hwlayer::sleep(1);
}

void
tcds::hwlayertca::TCACarrierBase::enableSFPs() const
{
  enableSFPsImpl();
}

void
tcds::hwlayertca::TCACarrierBase::disableSFPs() const
{
  disableSFPsImpl();
}

std::string
tcds::hwlayertca::TCACarrierBase::readCarrierType() const
{
  uint32_t const tmp = hwDevice_.readRegister("system.board_id");
  std::string const res = tcds::hwlayer::uint32ToString(tmp);
  return res;
}

std::vector<std::string>
tcds::hwlayertca::TCACarrierBase::fmcNames() const
{
  return fmcNamesImpl();
}

bool
tcds::hwlayertca::TCACarrierBase::isFMCPresent(std::string const& fmcName) const
{
  return isFMCPresentImpl(fmcName);
}

bool
tcds::hwlayertca::TCACarrierBase::isFMCPresentImpl(std::string const& fmcName) const
{
  std::string const regName =
  registerNamePrefix() + ".status.fmc_" + fmcName + "_present";
  uint32_t const tmp = hwDevice_.readRegister(regName);
  bool const res = (tmp == 1);
  return res;
}

bool
tcds::hwlayertca::TCACarrierBase::isFMCPowerGood(std::string const& fmcName) const
{
  return isFMCPowerGoodImpl(fmcName);
}

bool
tcds::hwlayertca::TCACarrierBase::isFMCPowerGoodImpl(std::string const& fmcName) const
{
  std::string const regName =
    registerNamePrefix() + ".status.fmc_" + fmcName + "_power_good_m2c";
  uint32_t const tmp = hwDevice_.readRegister(regName);
  bool const res = (tmp == 1);
  return res;
}
