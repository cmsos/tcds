#include "tcds/hwlayertca/version.h"

#include "config/version.h"
#include "toolbox/version.h"
#include "xcept/version.h"

#include "tcds/exception/version.h"
#include "tcds/hwlayer/version.h"

GETPACKAGEINFO(tcds::hwlayertca)

void
tcds::hwlayertca::checkPackageDependencies()
{
  CHECKDEPENDENCY(config);
  CHECKDEPENDENCY(toolbox);
  CHECKDEPENDENCY(xcept);

  CHECKDEPENDENCY(tcds::exception);
  CHECKDEPENDENCY(tcds::hwlayer);
}

std::set<std::string, std::less<std::string> >
tcds::hwlayertca::getPackageDependencies()
{
  std::set<std::string, std::less<std::string> > dependencies;

  ADDDEPENDENCY(dependencies, config);
  ADDDEPENDENCY(dependencies, toolbox);
  ADDDEPENDENCY(dependencies, xcept);

  ADDDEPENDENCY(dependencies, tcds::exception);
  ADDDEPENDENCY(dependencies, tcds::hwlayer);

  return dependencies;
}
