#include "tcds/hwlayertca/Utils.h"

#include <algorithm>
#include <cstddef>

#include "toolbox/string.h"
#include "xcept/Exception.h"

#include "tcds/exception/Exception.h"
#include "tcds/hwlayertca/Definitions.h"

std::map<tcds::definitions::SFP_LPM, std::string>
tcds::hwlayertca::sfpRegNameMapLPM()
{
  std::map<tcds::definitions::SFP_LPM, std::string> regNames;

  regNames[tcds::definitions::SFP_LPM_ICI1] = "ici1";
  regNames[tcds::definitions::SFP_LPM_ICI2] = "ici2";
  regNames[tcds::definitions::SFP_LPM_ICI3] = "ici3";
  regNames[tcds::definitions::SFP_LPM_ICI4] = "ici4";
  regNames[tcds::definitions::SFP_LPM_ICI5] = "ici5";
  regNames[tcds::definitions::SFP_LPM_ICI6] = "ici6";
  regNames[tcds::definitions::SFP_LPM_ICI7] = "ici7";
  regNames[tcds::definitions::SFP_LPM_ICI8] = "ici8";

  regNames[tcds::definitions::SFP_LPM_DAQ0] = "daq0";
  regNames[tcds::definitions::SFP_LPM_DAQ1] = "daq1";

  return regNames;
}

std::string
tcds::hwlayertca::sfpToRegName(tcds::definitions::SFP_LPM const sfp)
{
  std::map<tcds::definitions::SFP_LPM, std::string> regNames = sfpRegNameMapLPM();
  std::map<tcds::definitions::SFP_LPM, std::string>::const_iterator i = regNames.find(sfp);
  if (i == regNames.end())
    {
      std::string const msg =
        toolbox::toString("Number %d is not a valid LPM SFP number"
                          " (for register name lookups).",
                          sfp);
      XCEPT_RAISE(tcds::exception::ValueError, msg);
    }
  return i->second;
}

std::string
tcds::hwlayertca::sfpToLongName(tcds::definitions::SFP_LPM const sfp)
{
  std::string res;

  size_t tmp;
  switch (sfp)
    {
    case tcds::definitions::SFP_LPM_ICI1:
    case tcds::definitions::SFP_LPM_ICI2:
    case tcds::definitions::SFP_LPM_ICI3:
    case tcds::definitions::SFP_LPM_ICI4:
    case tcds::definitions::SFP_LPM_ICI5:
    case tcds::definitions::SFP_LPM_ICI6:
    case tcds::definitions::SFP_LPM_ICI7:
    case tcds::definitions::SFP_LPM_ICI8:
      tmp = sfp - tcds::definitions::SFP_LPM_ICI1 + 1;
      res = toolbox::toString("ICI %d", tmp);
      break;
    case tcds::definitions::SFP_LPM_DAQ0:
      res = "DAQ0";
      break;
    case tcds::definitions::SFP_LPM_DAQ1:
      res = "DAQ1";
      break;
    default:
      std::string const msg =
        toolbox::toString("Number %d is not a valid LPM SFP number"
                          " (for register name lookups).",
                          sfp);
      XCEPT_RAISE(tcds::exception::ValueError, msg);
      break;
    }

  return res;
}

std::string
tcds::hwlayertca::sfpToShortName(tcds::definitions::SFP_LPM const sfp)
{
  std::string tmp = sfpToRegName(sfp);
  tmp.erase(std::remove(tmp.begin(), tmp.end(), '_'), tmp.end());
  return tmp;
}

std::map<tcds::definitions::SFP_PI, std::string>
tcds::hwlayertca::sfpRegNameMapPI()
{
  std::map<tcds::definitions::SFP_PI, std::string> regNames;

  regNames[tcds::definitions::SFP_PI_LPMPRI] = "lpm_pri";
  regNames[tcds::definitions::SFP_PI_LPMSEC] = "lpm_sec";

  regNames[tcds::definitions::SFP_PI_FED1] = "fed1";
  regNames[tcds::definitions::SFP_PI_FED2] = "fed2";
  regNames[tcds::definitions::SFP_PI_FED3] = "fed3";
  regNames[tcds::definitions::SFP_PI_FED4] = "fed4";
  regNames[tcds::definitions::SFP_PI_FED5] = "fed5";
  regNames[tcds::definitions::SFP_PI_FED6] = "fed6";
  regNames[tcds::definitions::SFP_PI_FED7] = "fed7";
  regNames[tcds::definitions::SFP_PI_FED8] = "fed8";
  regNames[tcds::definitions::SFP_PI_FED9] = "fed9";
  regNames[tcds::definitions::SFP_PI_FED10] = "fed10";

  return regNames;
}

std::string
tcds::hwlayertca::sfpToRegName(tcds::definitions::SFP_PI const sfp)
{
  std::map<tcds::definitions::SFP_PI, std::string> regNames = sfpRegNameMapPI();
  std::map<tcds::definitions::SFP_PI, std::string>::const_iterator i = regNames.find(sfp);
  if (i == regNames.end())
    {
      std::string const msg =
        toolbox::toString("Number %d is not a valid PI SFP number"
                          " (for register name lookups).",
                          sfp);
      XCEPT_RAISE(tcds::exception::ValueError, msg);
    }
  return i->second;
}

std::string
tcds::hwlayertca::sfpToLongName(tcds::definitions::SFP_PI const sfp)
{
  std::string res;

  size_t tmp;
  switch (sfp)
    {
    case tcds::definitions::SFP_PI_LPMPRI:
      res = "Primary LPM";
      break;
    case tcds::definitions::SFP_PI_LPMSEC:
      res = "Secondary LPM";
      break;
    case tcds::definitions::SFP_PI_FED1:
    case tcds::definitions::SFP_PI_FED2:
    case tcds::definitions::SFP_PI_FED3:
    case tcds::definitions::SFP_PI_FED4:
    case tcds::definitions::SFP_PI_FED5:
    case tcds::definitions::SFP_PI_FED6:
    case tcds::definitions::SFP_PI_FED7:
    case tcds::definitions::SFP_PI_FED8:
    case tcds::definitions::SFP_PI_FED9:
    case tcds::definitions::SFP_PI_FED10:
      tmp = sfp - tcds::definitions::SFP_PI_FED1 + 1;
      res = toolbox::toString("FED %d", tmp);
      break;
    default:
      std::string const msg =
        toolbox::toString("Number %d is not a valid PI SFP number"
                          " (for register name lookups).",
                          sfp);
      XCEPT_RAISE(tcds::exception::ValueError, msg);
      break;
    }

  return res;
}

std::string
tcds::hwlayertca::sfpToShortName(tcds::definitions::SFP_PI const sfp)
{
  std::string tmp = sfpToRegName(sfp);
  tmp.erase(std::remove(tmp.begin(), tmp.end(), '_'), tmp.end());
  return tmp;
}

std::map<tcds::definitions::SFP_PHASEMON, std::string>
tcds::hwlayertca::sfpRegNameMapPhaseMon()
{
  std::map<tcds::definitions::SFP_PHASEMON, std::string> regNames;

  regNames[tcds::definitions::SFP_PHASEMON_L8_1] = "l8_1";
  regNames[tcds::definitions::SFP_PHASEMON_L8_2] = "l8_2";
  regNames[tcds::definitions::SFP_PHASEMON_L8_3] = "l8_3";
  regNames[tcds::definitions::SFP_PHASEMON_L8_4] = "l8_4";
  regNames[tcds::definitions::SFP_PHASEMON_L8_5] = "l8_5";
  regNames[tcds::definitions::SFP_PHASEMON_L8_6] = "l8_6";
  regNames[tcds::definitions::SFP_PHASEMON_L8_7] = "l8_7";
  regNames[tcds::definitions::SFP_PHASEMON_L8_8] = "l8_8";

  return regNames;
}

std::string
tcds::hwlayertca::sfpToRegName(tcds::definitions::SFP_PHASEMON const sfp)
{
  std::map<tcds::definitions::SFP_PHASEMON, std::string> regNames = sfpRegNameMapPhaseMon();
  std::map<tcds::definitions::SFP_PHASEMON, std::string>::const_iterator i = regNames.find(sfp);
  if (i == regNames.end())
    {
      std::string const msg =
        toolbox::toString("Number %d is not a valid PhaseMon SFP number"
                          " (for register name lookups).",
                          sfp);
      XCEPT_RAISE(tcds::exception::ValueError, msg);
    }
  return i->second;
}

std::string
tcds::hwlayertca::sfpToLongName(tcds::definitions::SFP_PHASEMON const sfp)
{
  std::string res;

  size_t tmp;
  switch (sfp)
    {
    case tcds::definitions::SFP_PHASEMON_L8_1:
    case tcds::definitions::SFP_PHASEMON_L8_2:
    case tcds::definitions::SFP_PHASEMON_L8_3:
    case tcds::definitions::SFP_PHASEMON_L8_4:
    case tcds::definitions::SFP_PHASEMON_L8_5:
    case tcds::definitions::SFP_PHASEMON_L8_6:
    case tcds::definitions::SFP_PHASEMON_L8_7:
    case tcds::definitions::SFP_PHASEMON_L8_8:
      tmp = sfp - tcds::definitions::SFP_PHASEMON_L8_1 + 1;
      res = toolbox::toString("L8 SFP %d", tmp);
      break;
    default:
      std::string const msg =
        toolbox::toString("Number %d is not a valid PhaseMon SFP number"
                          " (for register name lookups).",
                          sfp);
      XCEPT_RAISE(tcds::exception::ValueError, msg);
      break;
    }

  return res;
}

std::string
tcds::hwlayertca::sfpToShortName(tcds::definitions::SFP_PHASEMON const sfp)
{
  std::string tmp = sfpToRegName(sfp);
  tmp.erase(std::remove(tmp.begin(), tmp.end(), '_'), tmp.end());
  return tmp;
}
