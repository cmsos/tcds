#include "tcds/hwlayertca/TCACarrierFC7.h"

#include <algorithm>
#include <cassert>
#include <cmath>
#include <functional>
#include <iomanip>
#include <cstddef>
#include <unistd.h>
#include <utility>

#include "toolbox/string.h"
#include "xcept/Exception.h"

#include "tcds/exception/Exception.h"
#include "tcds/hwlayertca/FC7MMCPipe.h"
#include "tcds/hwlayertca/HwDeviceTCA.h"

namespace tcds {
  namespace hwlayertca {
    std::ostream& operator<<(std::ostream& os,
                             TCACarrierFC7::VOLTAGE_VALUE const& voltage)
    {
      switch(voltage)
        {
        case TCACarrierFC7::VOLTAGE_VALUE_3V3:
          os << "3V3";
          break;
        case TCACarrierFC7::VOLTAGE_VALUE_12V:
          os << "12V";
          break;
        case TCACarrierFC7::VOLTAGE_VALUE_VADJ:
          os << "VADJ";
          break;
        default:
          os << "unknown voltage";
          break;
        }
      return os;
    }
  }
}

tcds::hwlayertca::TCACarrierFC7::TCACarrierFC7(HwDeviceTCA& device) :
  TCACarrierBase(device)
{
}

tcds::hwlayertca::TCACarrierFC7::~TCACarrierFC7()
{
}

void
tcds::hwlayertca::TCACarrierFC7::loadFirmwareImageImpl(std::string const& imageName) const
{
  std::string fileName = "";
  if (imageName == "golden")
    {
      fileName = "GoldenImage.bin";
    }
  else if (imageName == "user")
    {
      fileName = "UserImage.bin";
    }
  else
    {
      std::string const msg =
        toolbox::toString("Failed to interpret '%s' as a valid FC7 firmware image name.",
                          imageName.c_str());
      XCEPT_RAISE(tcds::exception::ValueError, msg.c_str());
    }

  FC7MMCPipe mmcPipe(hwDevice_);
  mmcPipe.loadFirmwareImage(fileName);
}

void
tcds::hwlayertca::TCACarrierFC7::powerUpImpl() const
{
  std::vector<std::string> names = fmcNames();

  // - Set the FMC VADJ.

  // NOTE: The adjustable voltage required by the TCDS FMCs is
  // determined by the fact that LVDS signals are used. Ergo: we need
  // vadj = 2V5.

  // BUG BUG BUG
  // Hard-coded numbers here.
  // The I2C address of the digitial potentiometer regulating
  // VADJ for both FMCs.
  uint32_t const kI2CAddressAD5172 = 0x2f;
  // The proper AD5172 addressing instructions for:
  // - FMC-L8 VADJ connected to B1 (channel 1).
  // - FMC-L12 VADJ connected to B2 (channel 2).
  std::map<std::string, uint32_t> potmeterInstructionsPerFMC;
  potmeterInstructionsPerFMC["l8"] = 0x0;
  potmeterInstructionsPerFMC["l12"] = 0x80;
  // The AND mask for the AD5172 instruction byte.
  // See the data sheet for details.
  uint32_t kAD5172InstANDMask = 0x88;
  // Magic number representing the correct voltage.
  uint32_t const kP2V5 = 0xff - 0x2b;
  // BUG BUG BUG end

  tcds::hwlayer::I2CAccessor i2c(hwDevice_,
                                 tcds::hwlayer::I2CAccessor::I2CMasterSystem,
                                 tcds::hwlayer::I2CAccessor::I2CBus0);
  // Set VADJ for all present FMCs.
  for (std::vector<std::string>::const_iterator it = names.begin();
       it != names.end();
       ++it)
    {
      if (isFMCPresent(*it))
        {
          uint32_t const instr = potmeterInstructionsPerFMC[*it] & kAD5172InstANDMask;
          try
            {
              i2c.writeMemoryEntry(kI2CAddressAD5172, instr, kP2V5);
            }
          catch (tcds::exception::I2CProblem const&)
            {
              // This may be nothing. If we are connected to an FC7
              // without adjustable voltage (i.e., without the digital
              // potentiometers), there is nothing to I2C to, and the
              // voltage should be okay anyway.
            }
        }
    }

  // - Check all voltages on the carrier board.
  assertFMCSupplyVoltagesAreCorrect();

  // - Enable FMC power.
  for (std::vector<std::string>::const_iterator it = names.begin();
       it != names.end();
       ++it)
    {
      if (isFMCPresent(*it))
        {
          powerUpFMC(*it);
        }
    }

  // - Set power-good flag (carrier-to-module).
  hwDevice_.writeRegister("fc7.ctrl2.fmc_power_good_c2m", 0x1);

  // - Check FMC power-good flags (module-to-carrier).
  for (std::vector<std::string>::const_iterator it = names.begin();
       it != names.end();
       ++it)
    {
      if (isFMCPresent(*it))
        {
          bool const powerGood = isFMCPowerGood(*it);
          if (!powerGood)
            {
              std::string const msg = "FMC '" + *it + "' reports power-good = False.";
              XCEPT_RAISE(tcds::exception::HardwareProblem, msg.c_str());
            }
        }
    }
}

void
tcds::hwlayertca::TCACarrierFC7::selectClockSourceImpl(tcds::hwlayertca::TCACarrierBase::CLOCK_SOURCE const clkSrc) const
{
  // This method drives the configuration of the FC7 cross-point
  // switches routing the clocks to the FPGA.

  uint32_t ttcXpointASrc = 0x0;
  switch (clkSrc)
    {
    case CLOCK_SOURCE_FCLKA:
      // Select the FCLKA clock from the uTCA backplane.
      ttcXpointASrc = 0x0;
      break;
    case CLOCK_SOURCE_FMC:
      // Select clock0 coming from FMC-L8.
      ttcXpointASrc = 0x2;
      break;
    case CLOCK_SOURCE_XTAL:
      // Select the clock coming from the on-board crystal.
      ttcXpointASrc = 0x3;
      hwDevice_.writeRegister("fc7.ctrl.osc_coax_sel", 0x1);
      break;
    default:
      // ASSERT ASSERT ASSERT
      assert (false);
      // ASSERT ASSERT ASSERT end
      break;
    }

  // Switch all inputs of the master xpoint switch to the same input.
  hwDevice_.writeRegister("fc7.ctrl.master_xpoint_s1", ttcXpointASrc);
  hwDevice_.writeRegister("fc7.ctrl.master_xpoint_s2", ttcXpointASrc);
  hwDevice_.writeRegister("fc7.ctrl.master_xpoint_s3", ttcXpointASrc);
  hwDevice_.writeRegister("fc7.ctrl.master_xpoint_s4", ttcXpointASrc);

  // Finally: configure the CDCE xpoint switch to listen to reference
  // clock 0 and use the 160 MHz clock from CDCE u1 as input. (The 240
  // MHz clock is at u0.)
  uint32_t const cdceXpointSrc = 0x1;
  hwDevice_.writeRegister("fc7.ctrl.cdce_xpoint_s2", cdceXpointSrc);
  hwDevice_.writeRegister("fc7.ctrl.cdce_xpoint_s3", cdceXpointSrc);
  hwDevice_.writeRegister("fc7.ctrl.cdce_xpoint_s4", cdceXpointSrc);
  hwDevice_.writeRegister("fc7.ctrl.cdce_refsel", 0x0);

  // Important: on the FC7 we also have to resync the CDCE after
  // switching clock sources.
  resyncCDCE();
}

void
tcds::hwlayertca::TCACarrierFC7::enableSFPsImpl() const
{
  std::vector<std::string> names = fmcNames();
  for (std::vector<std::string>::const_iterator fmcName = names.begin();
       fmcName != names.end();
       ++fmcName)
    {
      if (isFMCPresent(*fmcName))
        {
          switchFMCSFPs(*fmcName, true);
        }
    }
}

void
tcds::hwlayertca::TCACarrierFC7::disableSFPsImpl() const
{
  std::vector<std::string> names = fmcNames();
  for (std::vector<std::string>::const_iterator fmcName = names.begin();
       fmcName != names.end();
       ++fmcName)
    {
      if (isFMCPresent(*fmcName))
        {
          switchFMCSFPs(*fmcName, false);
        }
    }
}

void
tcds::hwlayertca::TCACarrierFC7::powerUpFMC(std::string const& fmcName) const
{
  std::string const regName =
    registerNamePrefix() + ".ctrl2.fmc_" + fmcName + "_power_enable";
  hwDevice_.writeRegister(regName, 0x1);
}

void
tcds::hwlayertca::TCACarrierFC7::powerDownFMC(std::string const& fmcName) const
{
  std::string const regName =
    registerNamePrefix() + ".ctrl2.fmc_" + fmcName + "_power_enable";
  hwDevice_.writeRegister(regName, 0x0);
}

void
tcds::hwlayertca::TCACarrierFC7::assertFMCSupplyVoltagesAreCorrect() const
{
  // The following are the accepted tolerances on the various FC7
  // supply voltages. Quite a bit of leeway is given since neither the
  // voltage dividers nor the ADC are very accurate. (Especially for
  // the 12 V.)
  std::map<VOLTAGE_VALUE, std::pair<float, float> > voltageSettings;
  voltageSettings[VOLTAGE_VALUE_3V3] = std::make_pair(3.3, .05);
  voltageSettings[VOLTAGE_VALUE_12V] = std::make_pair(12., .10);
  voltageSettings[VOLTAGE_VALUE_VADJ] = std::make_pair(2.5, .05);

  std::vector<std::string> names = fmcNames();
  for (std::vector<std::string>::const_iterator fmcName = names.begin();
       fmcName != names.end();
       ++fmcName)
    {
      if (isFMCPresent(*fmcName))
        {
          std::map<VOLTAGE_VALUE, double> voltages = readFMCSupplyVoltages(*fmcName);
          for (std::map<VOLTAGE_VALUE, double>::const_iterator it = voltages.begin();
               it != voltages.end();
               ++it)
            {
              VOLTAGE_VALUE voltage = it->first;
              double measuredVoltage = it->second;
              double expectedVoltage = voltageSettings[voltage].first;
              double voltageToleranceRel = voltageSettings[voltage].second;
              double voltageToleranceAbs = voltageToleranceRel * expectedVoltage;
              if ((measuredVoltage < (expectedVoltage - voltageToleranceAbs)) ||
                  (measuredVoltage > (expectedVoltage + voltageToleranceAbs)))
                {
                  std::stringstream msg;
                  msg << std::fixed << std::setprecision(3)
                      << "FMC '" << *fmcName << "' reports bad voltage for "
                      << voltage << ": "
                      << measuredVoltage
                      << " lies outside "
                      << expectedVoltage
                      << " +/- "
                      << 100. * voltageToleranceRel << "%";
                  XCEPT_RAISE(tcds::exception::HardwareProblem, msg.str());
                }
            }
        }
    }
}

void
tcds::hwlayertca::TCACarrierFC7::resyncCDCE() const
{
  // This is the time in seconds to wait for the CDCE to recover after
  // every step.
  int sleepTime(1);

  // Obtain control over the CDCE.
  hwDevice_.writeRegister("fc7.ctrl.cdce_ctrl_sel", 0x1);

  // Power-cycle the CDCE.
  hwDevice_.writeRegister("fc7.ctrl.cdce_powerup", 0x0);
  hwDevice_.writeRegister("fc7.ctrl.cdce_powerup", 0x1);
  ::sleep(sleepTime);

  // Trigger a resync of the CDCE.
  hwDevice_.writeRegister("fc7.ctrl.cdce_sync", 0x0);
  hwDevice_.writeRegister("fc7.ctrl.cdce_sync", 0x1);
  ::sleep(sleepTime);

  // Release control of the CDCE.
  hwDevice_.writeRegister("fc7.ctrl.cdce_ctrl_sel", 0x0);
}

tcds::hwlayer::I2CAccessor::I2CBus
tcds::hwlayertca::TCACarrierFC7::fmcBusSelect(std::string const& fmcName) const
{
  tcds::hwlayer::I2CAccessor::I2CBus res = tcds::hwlayer::I2CAccessor::I2CBus0;

  if (fmcName == "l8")
    {
      res = tcds::hwlayer::I2CAccessor::I2CBus1;
    }
  else if (fmcName == "l12")
    {
      res = tcds::hwlayer::I2CAccessor::I2CBus0;
    }

  return res;
}

void
tcds::hwlayertca::TCACarrierFC7::switchFMCSFPs(std::string const& fmcName, bool const switchOn) const
{
  // BUG BUG BUG
  // Hard-coded number here. May want to move this.
  uint32_t const kI2CAddressSFPStatusRegisters = 0x38;
  std::map<std::string, uint32_t> kI2CAddressMUXSFPStatusRegistersMap;
  kI2CAddressMUXSFPStatusRegistersMap["l8"] = 0x77;
  kI2CAddressMUXSFPStatusRegistersMap["l12"] = 0x74;
  // BUG BUG BUG end

  // NOTE: The bus-select value depends on the FMC being addressed.
  tcds::hwlayer::I2CAccessor::I2CBus busSelect = fmcBusSelect(fmcName);

  // NOTE: This is the MUX channels connected to the SFP TX_DIS line.
  uint8_t const channelNum = 2;

  // NOTE: The SFPs are _enabled_ by setting their TX_DISABLE lines to
  // zero.
  uint8_t stateVal = 0xff;
  if (switchOn)
    {
      stateVal = 0x00;
    }

  uint32_t const i2cAddressMUXSFPStatusRegisters = kI2CAddressMUXSFPStatusRegistersMap[fmcName];
  tcds::hwlayer::I2CAccessor i2c(hwDevice_,
                                 tcds::hwlayer::I2CAccessor::I2CMasterUser,
                                 busSelect);

  // Step 1: select the right MUX channel.
  i2c.writeRegister(i2cAddressMUXSFPStatusRegisters, (0x1 << channelNum));

  // Step 2: write the new value to the register pointed to by the
  // selected MUX channel.
  i2c.writeRegister(kI2CAddressSFPStatusRegisters, stateVal);
}

std::map<tcds::hwlayertca::TCACarrierFC7::VOLTAGE_VALUE, double>
tcds::hwlayertca::TCACarrierFC7::readFMCSupplyVoltages(std::string const& fmcName) const
{
  // BUG BUG BUG
  // Hard-coded numbers here.

  // // The I2C address of U18, the LTC2990 connected to L8_VADJ.
  // // - V2: L8_VADJ
  // // - V4: +3V3
  // uint32_t const kI2CAddressLTC2990L8A = 0x4c;
  // // The I2C address of U83, the LTC2990 connected to the 12V for FMC-L8.
  // // - V2: +12V
  // uint32_t const kI2CAddressLTC2990L8B = 0x4d;

  // // The I2C address of U73, the LTC2990 connected to L12_VADJ.
  // // - V2: L12_VADJ
  // // - V4: +3V3
  // uint32_t const kI2CAddressLTC2990L12A = 0x4e;
  // // The I2C address of U74, the LTC2990 connected to the 12V for FMC-L8.
  // // - V2: +12V
  // uint32_t const kI2CAddressLTC2990L12B = 0x4f;

  std::map<std::string, std::pair<uint32_t, uint32_t> > i2cAddressesLTC2990;
  i2cAddressesLTC2990["l8"] = std::make_pair(0x4c, 0x4d);
  i2cAddressesLTC2990["l12"] = std::make_pair(0x4e, 0x4f);
  // There is one 'Global Sync Address' to which all LTC2990s
  // respond. This allows write-only access for synchronous
  // triggering.
  uint32_t const kI2CAddressLTC2990Broadcast = 0x77;

  uint32_t const kLTC2990RegAddressStatus = 0x0;
  uint32_t const kLTC2990RegAddressCtrl = 0x1;
  uint32_t const kLTC2990RegAddressTrigger = 0x2;
  uint32_t const kLTC2990CtrlSingleAcq = 0x40;
  uint32_t const kLTC2990CtrlMeasModeAllVoltages = 0x1f;
  double const kLTCADCStep = 5. / std::pow(2, 14);
  // BUG BUG BUG end

  tcds::hwlayer::I2CAccessor i2c(hwDevice_,
                                 tcds::hwlayer::I2CAccessor::I2CMasterSystem,
                                 tcds::hwlayer::I2CAccessor::I2CBus0);

  // Configure both LTC2990 chips for single-acquisition, four-voltage
  // (single-ended) measurements.
  uint32_t const ctrl = kLTC2990CtrlSingleAcq | kLTC2990CtrlMeasModeAllVoltages;
  i2c.writeMemoryEntry(kI2CAddressLTC2990Broadcast,
                       kLTC2990RegAddressCtrl,
                       ctrl);
  // Trigger a measurement on all chips.
  i2c.writeMemoryEntry(kI2CAddressLTC2990Broadcast,
                       kLTC2990RegAddressTrigger,
                       0x1);

  // Read the status register and see if the conversion is done or not
  // (based on the 'busy' flag). (And if not, wait a bit. Until we get
  // bored.)
  // NOTE: Of course we can't use the broadcast feature for this, so
  // we'll have to poll each of the two LTC2990s independently.
  std::vector<uint32_t> tmp;
  tmp.push_back(i2cAddressesLTC2990[fmcName].first);
  tmp.push_back(i2cAddressesLTC2990[fmcName].second);
  for (std::vector<uint32_t>::const_iterator i = tmp.begin();
       i != tmp.end();
       ++i)
    {
      size_t const maxNumTries = 15;
      bool done = false;
      size_t numTries = 0;
      while (!done and (numTries < maxNumTries))
        {
          uint32_t const conversionStatus =
            i2c.readMemoryEntry(*i, kLTC2990RegAddressStatus);
          done = (conversionStatus & 0x1) == 0;
          ++numTries;
        }
      if (!done)
        {
          std::string const msg =
            "Supply voltage measurement on FMC '" + fmcName + "' timed out.";
          XCEPT_RAISE(tcds::exception::HardwareProblem, msg);
        }
    }

  // Now read the measurement results from both LTC2990s one after the
  // other.
  // - The first one (U18/U73) gives us VADJ and 3V3.
  // - The second one (U83/U74) gives us 12V.
  uint32_t i2cAddress = i2cAddressesLTC2990[fmcName].first;
  std::vector<int16_t> rawValuesA = readLTC2990RawValues(i2cAddress);
  i2cAddress = i2cAddressesLTC2990[fmcName].second;
  std::vector<int16_t> rawValuesB = readLTC2990RawValues(i2cAddress);

  // Convert from raw measurements to voltages using the values of the
  // external (i.e., resistor) voltage dividers.
  // - For all four inputs to U18/U73: (4.7 + 4.7) / 4.7 = 2.
  // - For all four inputs to U83/U74: (4.7 + 1.18) / 1.18 = 4.9831.
  double const factorA = 2. * kLTCADCStep;
  std::vector<double> valuesA(rawValuesA.size(), 0.);
  std::transform(rawValuesA.begin(), rawValuesA.end(),
                 valuesA.begin(),
                 std::bind1st(std::multiplies<double>(), factorA));
  double const factorB = (4.7 + 1.18) / 1.18 * kLTCADCStep;
  std::vector<double> valuesB(rawValuesB.size(), 0.);
  std::transform(rawValuesB.begin(), rawValuesB.end(),
                 valuesB.begin(),
                 std::bind1st(std::multiplies<double>(), factorB));

  // Now turn these measurements into a recognizable format.
  std::map<VOLTAGE_VALUE, double> res;
  res[VOLTAGE_VALUE_3V3] = valuesA.at(3);
  res[VOLTAGE_VALUE_12V] = valuesB.at(1);
  res[VOLTAGE_VALUE_VADJ] = valuesA.at(1);
  return res;
}

std::vector<int16_t>
tcds::hwlayertca::TCACarrierFC7::readLTC2990RawValues(uint32_t const i2cAddressLTC2990) const
{
  // NOTE: The method name does not give this fact away, but this
  // method should only be used for voltage/current readings, and
  // _not_ for temperature readings. (The 'special' status bits in the
  // MSB are different for temperature readings.)
  // NOTE: This method also relies on

  // BUG BUG BUG
  // Hard-coded numbers here.

  // See the LTC2990 datasheet for details on the below addresses.
  // LTC2990_REG_ADDRESS_V1_MSB = 0x6
  // LTC2990_REG_ADDRESS_V1_LSB = 0x7
  // LTC2990_REG_ADDRESS_V2_MSB = 0x8
  // LTC2990_REG_ADDRESS_V2_LSB = 0x9
  // LTC2990_REG_ADDRESS_V3_MSB = 0xa
  // LTC2990_REG_ADDRESS_V3_LSB = 0xb
  // LTC2990_REG_ADDRESS_V4_MSB = 0xc
  // LTC2990_REG_ADDRESS_V4_LSB = 0xd

  uint32_t const kLTC2990RegAddressV1MSB = 0x6;
  uint32_t const kLTC2990RegAddressV4LSB = 0xd;
  // BUG BUG BUG end

  tcds::hwlayer::I2CAccessor i2c(hwDevice_,
                                 tcds::hwlayer::I2CAccessor::I2CMasterSystem,
                                 tcds::hwlayer::I2CAccessor::I2CBus0);
  // NOTE: Watch out with the addressing. This loop works because of
  // the way the four voltage measurements are stored in the LTC2990.
  // See the LTC2990 data sheet for details.
  std::vector<uint32_t> resTmp;
  for (uint32_t registerAddress = kLTC2990RegAddressV1MSB;
       registerAddress <= kLTC2990RegAddressV4LSB;
       ++registerAddress)
    {
      i2c.writeRegister(i2cAddressLTC2990, registerAddress);
      uint32_t const tmp = i2c.readRegister(i2cAddressLTC2990);
      resTmp.push_back(tmp);
    }

  // Now glue together all LSB and MSB in the correct way.
  // NOTE: special handling for the top two bits of the MSB:
  // - the data-valid bit (MSB bit7), and
  // - the sign bit (MSB bit6).
  std::vector<int16_t> res;
  for (std::vector<uint32_t>::const_iterator it = resTmp.begin();
       it != resTmp.end();
       it += 2)
    {
      uint32_t const msb = *it;
      uint32_t const lsb = *(it + 1);
      uint32_t const dataValid = msb & 0x80;
      if (dataValid == 0x0)
        {
          std::string const msg =
            "Could not measure FC7 supply voltage. (No measurement result found.)";
          XCEPT_RAISE(tcds::exception::HardwareProblem, msg.c_str());
        }
      // NOTE: The various variable bit widths below may look a bit
      // dodgy, but based on the data itself this works out just fine
      // by construction.
      // NOTE: No need to mask out the sign bit here.
      uint32_t const part1 = (msb & 0x7f) << 8;
      uint32_t const part2 = lsb;
      uint32_t const sign = msb & 0x40;
      int16_t sum = part1 + part2;
      if (sign != 0x0)
        {
          sum = ~sum + 1;
        }
      res.push_back(sum);
    }
  return res;
}

std::string
tcds::hwlayertca::TCACarrierFC7::expectedCarrierType() const
{
  return "FC7 ";
}

std::string
tcds::hwlayertca::TCACarrierFC7::registerNamePrefix() const
{
  return "fc7";
}

std::vector<std::string>
tcds::hwlayertca::TCACarrierFC7::fmcNamesImpl() const
{
  std::vector<std::string> res;
  res.push_back("l8");
  res.push_back("l12");
  return res;
}
