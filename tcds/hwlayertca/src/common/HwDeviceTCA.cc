#include "tcds/hwlayertca/HwDeviceTCA.h"

#include <cstddef>
#include <ctime>
#include <utility>

#include "uhal/ConnectionManager.hpp"
#include "uhal/ClientInterface.hpp"
#include "uhal/definitions.hpp"
#include "uhal/HwInterface.hpp"
#include "uhal/log/exception.hpp"
#include "uhal/Node.hpp"
#include "uhal/ProtocolControlHub.hpp"
#include "uhal/utilities/bits.hpp"
#include "uhal/ValMem.hpp"

#include "toolbox/string.h"
#include "xcept/Exception.h"

#include "tcds/exception/Exception.h"
#include "tcds/hwlayer/Utils.h"

tcds::hwlayertca::HwDeviceTCA::HwDeviceTCA()
{
  // Turn up the logging level for the IPBus libraries. Otherwise they
  // a) produce too much output for convenient, and b) take a lot of
  // extra time during, for example, hardware connection and
  // configuration.
  uhal::setLogLevelTo(uhal::Error());
}

tcds::hwlayertca::HwDeviceTCA::~HwDeviceTCA()
{
}

void
tcds::hwlayertca::HwDeviceTCA::hwConnect(std::string const& connectionsFileName,
                                         std::string const& connectionName)
{
  std::unique_ptr<uhal::ConnectionManager> mgrP;
  try
    {
      std::string const tmp = toolbox::toString("file://%s", connectionsFileName.c_str());
      // Clear the address-table cache.
      uhal::ConnectionManager::clearAddressFileCache();
      // Instantiate the connection manager.
      mgrP = std::unique_ptr<uhal::ConnectionManager>(new uhal::ConnectionManager(tmp));
    }
  catch (uhal::exception::exception const& err)
    {
      std::string const msgBase =
        "Could not instantiate the uhal connection manager";
      std::string msg = toolbox::toString("%s: %s.", msgBase.c_str(), err.what());
      XCEPT_RAISE(tcds::exception::HardwareProblem, msg);
    }

  std::unique_ptr<uhal::HwInterface> hwPTmp;
  try
    {
      hwPTmp = std::unique_ptr<uhal::HwInterface>(new uhal::HwInterface(mgrP->getDevice(connectionName)));
    }
  catch (uhal::exception::exception const& err)
    {
      std::string const msgBase =
        "Could not obtain the uhal device from the connection manager";
      std::string msg = toolbox::toString("%s: %s.", msgBase.c_str(), err.what());
      XCEPT_RAISE(tcds::exception::HardwareProblem, msg);
    }

  // Perform a dummy read from the hardware to see if things really
  // work.
  // NOTE: The retry mechanism here is needed for the case where a
  // board is reloading firmware, in that case the read should work
  // after a few tries. If not, we can still throw an exception.
  bool done = false;
  size_t const maxNumTries = 15;
  time_t const singleIterationSleepTime = 1;
  size_t numTries = 0;
  tcds::hwlayer::randomSleep(1);
  while (!done and (numTries < maxNumTries))
    {
      try
        {
          hwPTmp->getNode("system.board_id").read();
          hwPTmp->dispatch();
          done = true;
        }
      catch (uhal::exception::ControlHubTargetTimeout const&)
        {
          tcds::hwlayer::sleep(singleIterationSleepTime);
        }
      catch (uhal::exception::exception const& err)
        {
          std::string msgBase = "Could not connect to the hardware";
          std::string msg = toolbox::toString("%s: %s.", msgBase.c_str(), err.what());
          XCEPT_RAISE(tcds::exception::HardwareProblem, msg);
        }
      ++numTries;
    }

  if (!done)
    {
      std::string msgBase = "Could not connect to the hardware. "
        "Connection still times out after %d tries.";
      std::string msg = toolbox::toString(msgBase.c_str(), numTries);
      XCEPT_RAISE(tcds::exception::HardwareProblem, msg);
    }

  hwP_ = std::move(hwPTmp);
}

void
tcds::hwlayertca::HwDeviceTCA::hwRelease()
{
  hwP_.reset();
}

bool
tcds::hwlayertca::HwDeviceTCA::isHwConnected() const
{
  return (hwP_.get() != 0);
}

uint32_t
tcds::hwlayertca::HwDeviceTCA::readRegister(std::string const& regName) const
{
  // LockGuard<Lock> guardedLock(lock_);
  uhal::HwInterface& hw = getHwInterface();

  // NOTE: The read(), dispatch() and value() all have to be together
  // inside the try block. Why? In case the getNode() or the read()
  // fails and the dispatch() never happens, leaving the returned
  // object as unvalidated memory.
  uint32_t res;
  try
    {
      uhal::ValWord<uint32_t> val = hw.getNode(regName).read();
      hw.dispatch();
      res = val.value();
    }
  catch (uhal::exception::exception const& err)
    {
      // NOTE: The uhal exceptions are terribly verbose, so those
      // messages are only logged and not propagated in the
      // exceptions.
      std::string msgBase = toolbox::toString("Could not read register '%s'", regName.c_str());
      std::string msg = toolbox::toString("%s: %s.", msgBase.c_str(), err.what());
      XCEPT_RAISE(tcds::exception::HardwareProblem, toolbox::toString("%s.", msg.c_str()));
    }

  return res;
}

void
tcds::hwlayertca::HwDeviceTCA::writeRegister(std::string const& regName,
                                             uint32_t const regVal) const
{
  // LockGuard<Lock> guardedLock(lock_);
  uhal::HwInterface& hw = getHwInterface();

  // NOTE: The write() and dispatch() should both be together inside
  // the try block.
  uhal::ValHeader status;
  try
    {
      status = hw.getNode(regName).write(regVal);
      hw.dispatch();
    }
  catch (uhal::exception::exception const& err)
    {
      // NOTE: The uhal exceptions are terribly verbose, so those
      // messages are only logged and not propagated in the
      // exceptions.
      std::string msgBase = toolbox::toString("Could not write register '%s'", regName.c_str());
      std::string msg = toolbox::toString("%s: %s.", msgBase.c_str(), err.what());
      XCEPT_RAISE(tcds::exception::HardwareProblem, toolbox::toString("%s.", msg.c_str()));
    }

  if (!status.valid())
    {
      std::string msgBase = toolbox::toString("Could not write register '%s'", regName.c_str());
      std::string msg = toolbox::toString("%s: %s.", msgBase.c_str(), "Return status is 'invalid'.");
      XCEPT_RAISE(tcds::exception::HardwareProblem, toolbox::toString("%s.", msg.c_str()));
    }
}

std::vector<uint32_t>
tcds::hwlayertca::HwDeviceTCA::readBlock(std::string const& regName,
                                         uint32_t const nWords) const
{
  // LockGuard<Lock> guardedLock(lock_);
  uhal::HwInterface& hw = getHwInterface();

  // NOTE: The readBlock(), dispatch() and value() all have to be together
  // inside the try block. Why? In case the getNode() or the read()
  // fails and the dispatch() never happens, leaving the returned
  // object as unvalidated memory.
  std::vector<uint32_t> res;
  try
    {
      uhal::ValVector<uint32_t> val = hw.getNode(regName).readBlock(nWords);
      hw.dispatch();
      res = val.value();
    }
  catch (uhal::exception::exception const& err)
    {
      // NOTE: The uhal exceptions are terribly verbose, so those
      // messages are only logged and not propagated in the
      // exceptions.
      std::string msgBase = toolbox::toString("Could not read block '%s'", regName.c_str());
      std::string msg = toolbox::toString("%s: %s.", msgBase.c_str(), err.what());
      XCEPT_RAISE(tcds::exception::HardwareProblem, toolbox::toString("%s.", msg.c_str()));
    }

  return res;
}

std::vector<uint32_t>
tcds::hwlayertca::HwDeviceTCA::readBlock(std::string const& regName) const
{
  return readBlock(regName, getBlockSize(regName));
}

std::vector<uint32_t>
tcds::hwlayertca::HwDeviceTCA::readBlockOffset(std::string const& regName,
                                               uint32_t const nWords,
                                               uint32_t const offset) const
{
  uhal::HwInterface& hw = getHwInterface();

  // NOTE: The readBlockOffset(), dispatch() and value() all have to
  // be together inside the try block. Why? In case the getNode() or
  // the read() fails and the dispatch() never happens, leaving the
  // returned object as unvalidated memory.
  std::vector<uint32_t> res;
  try
    {
      uhal::ValVector<uint32_t> val = hw.getNode(regName).readBlockOffset(nWords, offset);
      hw.dispatch();
      res = val.value();
    }
  catch (uhal::exception::exception const& err)
    {
      // NOTE: The uhal exceptions are terribly verbose, so those
      // messages are only logged and not propagated in the
      // exceptions.
      std::string msgBase = toolbox::toString("Could not read block '%s' (offset: 0x%x)",
                                              regName.c_str(),
                                              offset);
      std::string msg = toolbox::toString("%s: %s.", msgBase.c_str(), err.what());
      XCEPT_RAISE(tcds::exception::HardwareProblem, toolbox::toString("%s.", msg.c_str()));
    }

  return res;
}

void
tcds::hwlayertca::HwDeviceTCA::writeBlock(std::string const& regName,
                                          std::vector<uint32_t> const regVal) const
{
  // LockGuard<Lock> guardedLock(lock_);
  uhal::HwInterface& hw = getHwInterface();

  // NOTE: The writeBlock() and dispatch() should both be together
  // inside the try block.
  uhal::ValHeader status;
  try
    {
      status = hw.getNode(regName).writeBlock(regVal);
      hw.dispatch();
    }
  catch (uhal::exception::exception const& err)
    {
      // NOTE: The uhal exceptions are terribly verbose, so those
      // messages are only logged and not propagated in the
      // exceptions.
      std::string msgBase = toolbox::toString("Could not write block '%s'", regName.c_str());
      std::string msg = toolbox::toString("%s: %s.", msgBase.c_str(), err.what());
      XCEPT_RAISE(tcds::exception::HardwareProblem, toolbox::toString("%s.", msg.c_str()));
    }

  if (!status.valid())
    {
      std::string msgBase = toolbox::toString("Could not write block '%s'", regName.c_str());
      std::string msg = toolbox::toString("%s: %s.", msgBase.c_str(), "Return status is 'invalid'.");
      XCEPT_RAISE(tcds::exception::HardwareProblem, toolbox::toString("%s.", msg.c_str()));
    }
}

uint32_t
tcds::hwlayertca::HwDeviceTCA::readModifyWriteRegister(std::string const& regName,
                                                       uint32_t const regVal) const
{
  // Performs an IPbus read-modify-write-bits operation.

  uhal::HwInterface& hw = getHwInterface();
  uint32_t const address = hw.getNode(regName).getAddress();
  uint32_t const mask = hw.getNode(regName).getMask();
  uint32_t const shiftSize = uhal::utilities::TrailingRightBits(mask);
  uint32_t const bitShiftedSource = (regVal << shiftSize);

  if ((bitShiftedSource >> shiftSize) != regVal)
    {
      std::string msg = toolbox::toString("Could not (read-modify-)write register '%s'. Value to write has bits set outside the mask.", regName.c_str());
      XCEPT_RAISE(tcds::exception::RuntimeProblem, toolbox::toString("%s.", msg.c_str()));
    }

  uint32_t const overlap = (bitShiftedSource & ~mask);

 if (overlap != 0)
   {
      std::string msg = toolbox::toString("Could not (read-modify-)write register '%s'. Value to write has bits set outside the mask.", regName.c_str());
      XCEPT_RAISE(tcds::exception::RuntimeProblem, toolbox::toString("%s.", msg.c_str()));
   }

  uint32_t tmp;
  try
    {
      uhal::ValWord<uint32_t> val =
        hw.getClient().rmw_bits(address, ~mask, (bitShiftedSource & mask));
      hw.dispatch();
      tmp = val.value();
    }
  catch (uhal::exception::exception const& err)
    {
      // NOTE: The uhal exceptions are terribly verbose, so those
      // messages are only logged and not propagated in the
      // exceptions.
      std::string msgBase =
        toolbox::toString("Could not (read-modify-)write register '%s'", regName.c_str());
      std::string msg = toolbox::toString("%s: %s.", msgBase.c_str(), err.what());
      XCEPT_RAISE(tcds::exception::HardwareProblem, toolbox::toString("%s.", msg.c_str()));
    }

  uint32_t const res = ((tmp & mask) >> shiftSize);
  return res;
}

uint32_t
tcds::hwlayertca::HwDeviceTCA::getBlockSize(std::string const& regName) const
{
  try
    {
      return getHwInterface().getNode(regName).getSize();
    }
  catch (uhal::exception::exception const& err)
    {
      // NOTE: The uhal exceptions are terribly verbose, so those
      // messages are only logged and not propagated in the
      // exceptions.
      std::string msgBase = toolbox::toString("Could not get size of register '%s'", regName.c_str());
      std::string msg = toolbox::toString("%s: %s.", msgBase.c_str(), err.what());
      XCEPT_RAISE(tcds::exception::HardwareProblem, toolbox::toString("%s.", msg.c_str()));
    }
}

std::vector<std::string>
tcds::hwlayertca::HwDeviceTCA::getRegisterNames() const
{
  return getHwInterface().getNodes();
}

tcds::hwlayer::RegisterInfo::RegInfoVec
tcds::hwlayertca::HwDeviceTCA::getRegisterInfos() const
{
  uhal::HwInterface& hw = getHwInterface();
  std::vector<std::string> const regNames = getRegisterNames();
  tcds::hwlayer::RegisterInfo::RegInfoVec regInfos;
  for (std::vector<std::string>::const_iterator regName = regNames.begin();
       regName != regNames.end();
       ++regName)
    {
      uhal::Node const& node = hw.getNode(*regName);
      // In order to avoid overlaps, include only end-point (or
      // 'leaf') registers.
      if (node.getNodes().size() == 0)
        {
          uhal::defs::NodePermission permission = node.getPermission();
          bool const isReadable = ((permission == uhal::defs::READ) ||
                                   (permission == uhal::defs::READWRITE));
          bool const isWritable = ((permission == uhal::defs::WRITE) ||
                                   (permission == uhal::defs::READWRITE));
          tcds::hwlayer::RegisterInfo regInfo(*regName, isReadable, isWritable);
          regInfos.push_back(regInfo);
        }
    }
  return regInfos;
}

tcds::hwlayer::RegDumpVec
tcds::hwlayertca::HwDeviceTCA::dumpRegisterContents() const
{
  tcds::hwlayer::RegDumpVec res;

  tcds::hwlayer::RegisterInfo::RegInfoVec regInfos = getRegisterInfos();
  for (tcds::hwlayer::RegisterInfo::RegInfoVec::const_iterator regInfo = regInfos.begin();
       regInfo != regInfos.end();
       ++regInfo)
    {
      if (regInfo->isReadable())
        {
          std::vector<uint32_t> regVals;
          // NOTE: Some care is required here. For single-word reads
          // the appropriate mask (specified in the address table) is
          // applied. Block reads don't know about masks.
          uint32_t const size = getBlockSize(regInfo->name());
          if (size == 1)
            {
              regVals.push_back(readRegister(regInfo->name()));
            }
          else
            {
              regVals = readBlock(regInfo->name());
            }
          res.push_back(std::make_pair(regInfo->name(), regVals));
        }
    }

  return res;
}

uhal::HwInterface&
tcds::hwlayertca::HwDeviceTCA::getHwInterface() const
{
  if (!isHwConnected())
    {
      std::string msg = "Trying to control the hardware, "
        "but the XDAQ control application is not (yet) connected "
        "to the hardware. (Unless something is very wrong, "
        "this simply means you have to 'Configure' first.)";
      XCEPT_RAISE(tcds::exception::SoftwareProblem, msg);
    }
  else
    {
      uhal::HwInterface& hw = static_cast<uhal::HwInterface&>(*hwP_);
      return hw;
    }
}
