#include "tcds/hwlayertca/FC7MMCPipe.h"

#include <cstddef>

#include "toolbox/string.h"
#include "xcept/Exception.h"

#include "tcds/exception/Exception.h"
#include "tcds/hwlayer/Utils.h"
#include "tcds/hwlayertca/HwDeviceTCA.h"

tcds::hwlayertca::FC7MMCPipe::FC7MMCPipe(HwDeviceTCA& device) :
  hwDevice_(device),
  dataAvailableFPGAToMMC_(0),
  dataAvailableMMCToFPGA_(0),
  spaceAvailableFPGAToMMC_(0),
  spaceAvailableMMCToFPGA_(0)
{
}

tcds::hwlayertca::FC7MMCPipe::~FC7MMCPipe()
{
}

void
tcds::hwlayertca::FC7MMCPipe::loadFirmwareImage(std::string const& fileName)
{
  writeToTextSpace(fileName);
  enterSecureMode();
  send(kPipeCmdRebootFPGA);
}

uint32_t
tcds::hwlayertca::FC7MMCPipe::fifoSize() const
{
  return hwDevice_.getBlockSize("fc7.mmc_interface.fifo");
}

void
tcds::hwlayertca::FC7MMCPipe::updateCounters()
{
  uint32_t const wordCountFPGAToMMC = hwDevice_.readRegister("fc7.mmc_interface.fpga_to_mmc_counters");
  uint32_t const wordCountMMCToFPGA = hwDevice_.readRegister("fc7.mmc_interface.mmc_to_fpga_counters");
  uint32_t const pipeSize = fifoSize();

  uint32_t tmp = (((wordCountFPGAToMMC >> 16 ) & 0x0000ffff) -
                  ((wordCountFPGAToMMC >> 1) & 0x00007fff) +
                  1) % pipeSize;
  dataAvailableFPGAToMMC_ = tmp;
  spaceAvailableFPGAToMMC_ = pipeSize - dataAvailableFPGAToMMC_ - 1;

  tmp = (((wordCountMMCToFPGA >> 1) & 0x00007fff) -
         ((wordCountMMCToFPGA >> 16) & 0x0000ffff) +
         1) % pipeSize;
  dataAvailableMMCToFPGA_ = tmp;
  spaceAvailableMMCToFPGA_ = pipeSize - dataAvailableMMCToFPGA_ - 1;
}

void
tcds::hwlayertca::FC7MMCPipe::send(uint32_t const command,
                                   std::vector<uint32_t> const& data)
{
  std::vector<uint32_t> tmp;
  tmp.push_back(command);
  tmp.push_back(data.size());
  if (data.size() > 0)
    {
      tmp.insert(tmp.end(), data.begin(), data.end());
    }
  updateCounters();
  while (spaceAvailableFPGAToMMC_ < data.size())
    {
      tcds::hwlayer::nanosleep(kSleepTime);
      updateCounters();
    }
  hwDevice_.writeBlock("fc7.mmc_interface.fifo", tmp);
}

std::vector<uint32_t>
tcds::hwlayertca::FC7MMCPipe::receive()
{
  // Wait for the header to arrive.
  // NOTE: Under rare circumstances the DMA pipe connecting the FPGA
  // and the MMC can lock up. In that case the counters will never
  // reach the expected values, and everything hangs. Let's try to
  // catch that by looking at a maximum number of iterations.
  size_t const maxNumIterations = 1000;
  size_t numIterations = 0;
  updateCounters();
  while ((dataAvailableMMCToFPGA_ < 2) &&
         (numIterations < maxNumIterations))
    {
      tcds::hwlayer::nanosleep(kSleepTime);
      updateCounters();
      ++numIterations;
    }
  if (!(numIterations < maxNumIterations))
    {
      std::string const msg =
        "Problem in the FC7 MMC pipe: the communication seems to be stuck.";
      XCEPT_RAISE(tcds::exception::RuntimeProblem, msg.c_str());
    }


  std::vector<uint32_t> const tmp = hwDevice_.readBlock("fc7.mmc_interface.fifo", 2);

  std::vector<uint32_t> data;
  uint32_t const numWordsAvailable = tmp.at(1);
  updateCounters();
  if (numWordsAvailable > 0)
    {
      // Wait for the data to arrive.
      while (dataAvailableMMCToFPGA_ < numWordsAvailable)
        {
          tcds::hwlayer::nanosleep(kSleepTime);
          updateCounters();
            }
      data = hwDevice_.readBlock("fc7.mmc_interface.fifo", numWordsAvailable);
    }

  // Check if everything is alright.
  if (tmp.at(0) != 0)
    {
      std::string const msg =
        toolbox::toString("Problem in the FC7 MMC pipe: '%s'.", rawToString(data).c_str());
      XCEPT_RAISE(tcds::exception::RuntimeProblem, msg.c_str());
    }

  std::vector<uint32_t> res = data;
  return res;
}

void
tcds::hwlayertca::FC7MMCPipe::enterSecureMode()
{
  std::string const quip = "RuleBritannia";
  if (quip.size() > kTextSpaceSize)
    {
      std::string const msg =
        toolbox::toString("Size of string '%s' "
                          "exceeds maximum FC7 MMC text space size of %d.",
                          quip.c_str(), kTextSpaceSize);
      XCEPT_RAISE(tcds::exception::ValueError, msg.c_str());
    }
  std::vector<uint32_t> data = stringToRaw(quip);
  send(kPipeCmdEnterSecureMode, data);
  // Use _receive() to check for errors.
  receive();
}

void
tcds::hwlayertca::FC7MMCPipe::writeToTextSpace(std::string const& text)
{
  if (text.size() > kTextSpaceSize)
    {
      std::string const msg =
        toolbox::toString("Size of string '%s' "
                          "exceeds maximum FC7 MMC text space size of %d.",
                          text.c_str(), kTextSpaceSize);
      XCEPT_RAISE(tcds::exception::ValueError, msg.c_str());
    }

  std::vector<uint32_t> const data = stringToRaw(text);
  send(kPipeCmdSetTextSpace, data);
  // Use _receive() to check for errors.
  receive();
}

std::string
tcds::hwlayertca::FC7MMCPipe::rawToString(std::vector<uint32_t> const& raw) const
{
  std::string res;
  for (std::vector<uint32_t>::const_iterator i = raw.begin();
       i != raw.end();
       ++i)
    {
      res.push_back(static_cast<char>(((*i) >> 24) & 0xff));
      res.push_back(static_cast<char>(((*i) >> 16) & 0xff));
      res.push_back(static_cast<char>(((*i) >> 8) & 0xff));
      res.push_back(static_cast<char>((*i) & 0xff));
    }
  return res;
}

std::vector<uint32_t>
tcds::hwlayertca::FC7MMCPipe::stringToRaw(std::string const& str) const
{
  std::vector<uint32_t> res;
  size_t const len = str.size();
  size_t const numWords = len / 4;
  size_t const remainder = len % 4;
  size_t i = 0;
  for (i = 0; i != (numWords * 4); i += 4)
    {
      uint32_t const tmp0 = (static_cast<uint8_t>(str[i]) << 24) & 0xff000000;
      uint32_t const tmp1 = (static_cast<uint8_t>(str[i + 1]) << 16) & 0x00ff0000;
      uint32_t const tmp2 = (static_cast<uint8_t>(str[i + 2]) << 8) & 0x0000ff00;
      uint32_t const tmp3 = static_cast<uint8_t>(str[i + 3]) & 0x000000ff;
      uint32_t const tmp = tmp0 + tmp1 + tmp2 + tmp3;
      res.push_back(tmp);
    }
  if (remainder != 0)
    {
      uint32_t tmp0 = 0;
      uint32_t tmp1 = 0;
      uint32_t tmp2 = 0;
      uint32_t tmp3 = 0;
      if (i < len)
        {
          tmp0 = (static_cast<uint8_t>(str[i]) << 24) & 0xff000000;
        }
      if (i < (len - 1))
        {
          tmp1 = (static_cast<uint8_t>(str[i + 1]) << 16) & 0x00ff0000;
        }
      if (i < (len - 2))
        {
          tmp2 = (static_cast<uint8_t>(str[i + 2]) << 8) & 0x0000ff00;
        }
      uint32_t const tmp = tmp0 + tmp1 + tmp2 + tmp3;
      res.push_back(tmp);
    }
  return res;
}
