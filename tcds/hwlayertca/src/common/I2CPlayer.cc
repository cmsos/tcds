#include "tcds/hwlayertca/I2CPlayer.h"

#include <algorithm>
#include <cctype>
#include <cmath>
#include <iterator>
#include <sstream>

#include "toolbox/string.h"
#include "xcept/Exception.h"

#include "tcds/exception/Exception.h"
#include "tcds/hwlayertca/TCADeviceBase.h"
#include "tcds/utils/Utils.h"

tcds::hwlayertca::I2CPlayer::I2CPlayer(tcds::hwlayertca::TCADeviceBase const& device) :
  device_(device)
{
  baseTXData_ = baseTXData();
  baseTXDataOffset_ = baseTXData_.size();
  if ((baseTXDataOffset_ > 0)
      && (baseTXData_.back() == 0x0))
    {
      --baseTXDataOffset_;
    }
  update();
}

void
tcds::hwlayertca::I2CPlayer::enable() const
{
  // I2C player mode:
  // - 0: continuously looping.
  // - 1: single-shot.
  device_.writeRegister("user.i2c_player_ctrl.mode", 0x0, true);
  // Sleep time between iterations in mode 0. Specified in 32 ns ticks.
  device_.writeRegister("user.i2c_player_ctrl.sleep_time", 0xffff, true);
  device_.writeRegister("user.i2c.settings.enable", 0x1, true);
  device_.writeRegister("user.i2c_player_ctrl.enable", 0x1, true);
}

void
tcds::hwlayertca::I2CPlayer::disable() const
{
  device_.writeRegister("user.i2c_player_ctrl.enable", 0x0, true);
}

void
tcds::hwlayertca::I2CPlayer::update()
{
  rawTXData_ = device_.readBlock("user.i2c_player_tx", true);
  rawRXData_ = device_.readBlock("user.i2c_player_rx", true);
}

void
tcds::hwlayertca::I2CPlayer::verify() const
{
  std::vector<uint32_t> const i2cPlayerDataExpected = baseTXData_;

  if (!std::equal(i2cPlayerDataExpected.begin(),
                  i2cPlayerDataExpected.end(),
                  rawTXData_.begin()))
    {
      std::string const msg =
        toolbox::toString("The firmware-fixed part of the I2C player "
                          "TX memory did not match the expected values.");
      XCEPT_RAISE(tcds::exception::RuntimeProblem, msg.c_str());
    }
}

void
tcds::hwlayertca::I2CPlayer::program() const
{
  // L12 FMC (i.e., the top FMC on the FC7) lives on I2C bus 0.
  // L8 FMC (i.e., the bottom FMC on the FC7) lives on I2C bus 1.
  // There are (maximum) 8 SFPs on each FMC.

  // NOTE: In the schematics the SFPs are ordered from bottom to top,
  // and from right to left, as seen when looking at the front of the
  // board. Here we invert the order of the MUX addressing such that
  // we get the results back in top-to-bottom, and left-to-right
  // order.

  std::vector<uint32_t> i2cPlayerDataAdditional;

  // Start with the general SFP status information. I.e., the
  // information available on external I2C registers.
  for (short busNum = 0; busNum < kNumFMCs; ++busNum)
    {
      uint32_t muxAddress;
      if (busNum == 0)
        {
          muxAddress = 0x74;
        }
      else
        {
          muxAddress = 0x77;
        }
      // - Switch MUX.
      i2cPlayerDataAdditional.push_back(0x80800002 + (muxAddress << 16) + (busNum << 28));
      // - Module absence.
      i2cPlayerDataAdditional.push_back(0x80380000 + (busNum << 28));
      // - Switch MUX.
      i2cPlayerDataAdditional.push_back(0x80800004 + (muxAddress << 16) + (busNum << 28));
      // - TX disable.
      i2cPlayerDataAdditional.push_back(0x80380000 + (busNum << 28));
      // - Switch MUX.
      i2cPlayerDataAdditional.push_back(0x80800008 + (muxAddress << 16) + (busNum << 28));
      // - TX fault.
      i2cPlayerDataAdditional.push_back(0x80380000 + (busNum << 28));
      // - Switch MUX.
      i2cPlayerDataAdditional.push_back(0x80800010 + (muxAddress << 16) + (busNum << 28));
      // - RX LOS.
      i2cPlayerDataAdditional.push_back(0x80380000 + (busNum << 28));
    }

  // Now do the detailed SFP ID and diagnostic information.
  for (short busNum = 0; busNum < kNumFMCs; ++busNum)
    {
      for (short sfpNum = 7; sfpNum >= 0; --sfpNum)
        {
          // - Switch MUX.
          uint32_t muxAddress;
          if (busNum == 0)
            {
              muxAddress = 0x70;
            }
          else
            {
              muxAddress = 0x73;
            }
          i2cPlayerDataAdditional.push_back(0x80800000 + (busNum << 28) + (muxAddress << 16) + (0x1 << sfpNum));

          // - Read vendor name: slave address 0x50, memory
          //   address 0x14 - 0x23.
          for (short addr = 0x14; addr <= 0x23; ++addr)
            {
              i2cPlayerDataAdditional.push_back(0x81500000 + (busNum << 28) + (addr << 8));
            }
          // - Read vendor part number: slave address 0x50, memory
          //   address 0x28 - 0x37.
          for (short addr = 0x28; addr <= 0x37; ++addr)
            {
              i2cPlayerDataAdditional.push_back(0x81500000 + (busNum << 28) + (addr << 8));
            }
          // - Read vendor revision code: slave address 0x50,
          //   memory address 0x38 - 0x3b.
          for (short addr = 0x38; addr <= 0x3b; ++addr)
            {
              i2cPlayerDataAdditional.push_back(0x81500000 + (busNum << 28) + (addr << 8));
            }
          // - Read vendor serial number: slave address 0x50,
          //   memory address 0x44 - 0x53.
          for (short addr = 0x44; addr <= 0x53; ++addr)
            {
              i2cPlayerDataAdditional.push_back(0x81500000 + (busNum << 28) + (addr << 8));
            }
          // Read temperature: slave address 0x51, memory address
          // 0x60-0x61 (in 16-bit mode)
          i2cPlayerDataAdditional.push_back(0x82516000 + (busNum << 28));
          // Read Vcc: slave address 0x51, memory address 0x62-0x63
          // (in 16-bit mode)
          i2cPlayerDataAdditional.push_back(0x82516200 + (busNum << 28));
          // Read TX bias current: slave address 0x51, memory address
          // 0x64-0x65 (in 16-bit mode)
          i2cPlayerDataAdditional.push_back(0x82516400 + (busNum << 28));
          // Read TX power: slave address 0x51, memory address
          // 0x66-0x67 (in 16-bit mode)
          i2cPlayerDataAdditional.push_back(0x82516600 + (busNum << 28));
          // Read RX power: slave address 0x51, memory address
          // 0x68-0x69 (in 16-bit mode)
          i2cPlayerDataAdditional.push_back(0x82516800 + (busNum << 28));
        }
    }

  // Mark the end of the player TX data.
  i2cPlayerDataAdditional.push_back(0x00000000);

  std::vector<uint32_t> const i2cPlayerDataExpected = baseTXData_;
  size_t const index = baseTXDataOffset_;
  std::vector<uint32_t> i2cPlayerData = rawTXData_;
  std::vector<uint32_t>::iterator iter = i2cPlayerData.begin();
  std::advance(iter, index);
  std::copy(i2cPlayerDataAdditional.begin(),
            i2cPlayerDataAdditional.end(),
            iter);

  device_.writeBlock("user.i2c_player_tx", i2cPlayerData, true);
}

bool
tcds::hwlayertca::I2CPlayer::isModulePresent(short const fmcNum, short const sfpNum) const
{
  size_t const index = calcIndexGeneralInfo(fmcNum, kOffsetModAbs);
  uint32_t const data = extractRXData(index, 1).at(0);
  bool const modAbs = ((data & (0x1 << (7 - sfpNum))) != 0);
  bool const res = !modAbs;
  return res;
}

bool
tcds::hwlayertca::I2CPlayer::isModuleDisabled(short const fmcNum, short const sfpNum) const
{
  size_t const index = calcIndexGeneralInfo(fmcNum, kOffsetDisable);
  uint32_t const data = extractRXData(index, 1).at(0);
  bool const res = ((data & (0x1 << (7 - sfpNum))) != 0);
  return res;
}

bool
tcds::hwlayertca::I2CPlayer::isTXFault(short const fmcNum, short const sfpNum) const
{
  size_t const index = calcIndexGeneralInfo(fmcNum, kOffsetTXFault);
  uint32_t const data = extractRXData(index, 1).at(0);
  bool const res = ((data & (0x1 << (7 - sfpNum))) != 0);
  return res;
}

bool
tcds::hwlayertca::I2CPlayer::isRXLOS(short const fmcNum, short const sfpNum) const
{
  size_t const index = calcIndexGeneralInfo(fmcNum, kOffsetRXLOS);
  uint32_t const data = extractRXData(index, 1).at(0);
  bool const res = ((data & (0x1 << (7 - sfpNum))) != 0);
  return res;
}

std::string
tcds::hwlayertca::I2CPlayer::getSFPVendorName(short const fmcNum,
                                              short const sfpNum) const
{
  size_t const index = calcIndexPerSFPInfo(fmcNum, sfpNum, kOffsetVendorName);
  std::vector<uint32_t> const data = extractRXData(index, kSizeVendorName);
  std::string const res = convertRXDataToString(data);
  return res;
}

std::string
tcds::hwlayertca::I2CPlayer::getSFPVendorPartNumber(short const fmcNum,
                                                    short const sfpNum) const
{
  size_t const index = calcIndexPerSFPInfo(fmcNum, sfpNum, kOffsetVendorPartNumber);
  std::vector<uint32_t> const data = extractRXData(index, kSizeVendorPartNumber);
  std::string const res = convertRXDataToString(data);
  return res;
}

std::string
tcds::hwlayertca::I2CPlayer::getSFPVendorRevision(short const fmcNum,
                                                  short const sfpNum) const
{
  size_t const index = calcIndexPerSFPInfo(fmcNum, sfpNum, kOffsetVendorRevision);
  std::vector<uint32_t> const data = extractRXData(index, kSizeVendorRevision);
  std::string const res = convertRXDataToString(data);
  return res;
}

std::string
tcds::hwlayertca::I2CPlayer::getSFPVendorSerialNumber(short const fmcNum,
                                                      short const sfpNum) const
{
  size_t const index = calcIndexPerSFPInfo(fmcNum, sfpNum, kOffsetVendorSerialNumber);
  std::vector<uint32_t> const data = extractRXData(index, kSizeVendorSerialNumber);
  std::string const res = convertRXDataToString(data);
  return res;
}

float
tcds::hwlayertca::I2CPlayer::getSFPTemperature(short const fmcNum, short const sfpNum) const
{
  size_t const index = calcIndexPerSFPInfo(fmcNum, sfpNum, kOffsetTemperature);
  std::vector<uint32_t> const data = extractRXData(index, kSizeTemperature);
  float const res = convertRXDataToTemperature(data);
  return res;
}

float
tcds::hwlayertca::I2CPlayer::getSFPVcc(short const fmcNum, short const sfpNum) const
{
  size_t const index = calcIndexPerSFPInfo(fmcNum, sfpNum, kOffsetVcc);
  std::vector<uint32_t> const data = extractRXData(index, kSizeVcc);
  float const res = convertRXDataToVcc(data);
  return res;
}

float
tcds::hwlayertca::I2CPlayer::getSFPRXPower(short const fmcNum, short const sfpNum) const
{
  size_t const index = calcIndexPerSFPInfo(fmcNum, sfpNum, kOffsetRXPower);
  std::vector<uint32_t> const data = extractRXData(index, kSizeRXPower);
  float const res = convertRXDataToRXPower(data);
  return res;
}

float
tcds::hwlayertca::I2CPlayer::getSFPTXPower(short const fmcNum, short const sfpNum) const
{
  size_t const index = calcIndexPerSFPInfo(fmcNum, sfpNum, kOffsetTXPower);
  std::vector<uint32_t> const data = extractRXData(index, kSizeTXPower);
  float const res = convertRXDataToTXPower(data);
  return res;
}

float
tcds::hwlayertca::I2CPlayer::getSFPTXBiasCurrent(short const fmcNum, short const sfpNum) const
{
  size_t const index = calcIndexPerSFPInfo(fmcNum, sfpNum, kOffsetTXBiasCurrent);
  std::vector<uint32_t> const data = extractRXData(index, kSizeTXBiasCurrent);
  float const res = convertRXDataToTXBiasCurrent(data);
  return res;
}

std::vector<uint32_t>
tcds::hwlayertca::I2CPlayer::baseTXData() const
{
  // This is the default I2C player TX data filled in by the
  // firmware. It is used for the SFP monitoring, which is in turn
  // used by the TTS link monitoring on the PI, for example.
  std::vector<uint32_t> i2cPlayerDataExpected;

  // I2C bus 0: FMC L12 (i.e., the top FMC on the FC7).
  // - Switch MUX.
  i2cPlayerDataExpected.push_back(0x80F40002);
  // - Read mod_abs register.
  i2cPlayerDataExpected.push_back(0x80380000);

  // - Switch MUX.
  i2cPlayerDataExpected.push_back(0x80F40004);
  // - Read tx_disable register.
  i2cPlayerDataExpected.push_back(0x80380000);

  // - Switch MUX.
  i2cPlayerDataExpected.push_back(0x80F40008);
  // - Read tx_fault register.
  i2cPlayerDataExpected.push_back(0x80380000);

  // - Switch MUX.
  i2cPlayerDataExpected.push_back(0x80F40010);
  // - Read rx_los register.
  i2cPlayerDataExpected.push_back(0x80380000);

  // I2C bus 1: FMC L8 (i.e., the bottom FMC on the FC7).
  // - Switch MUX.
  i2cPlayerDataExpected.push_back(0x90F70002);
  // - Read mod_abs register.
  i2cPlayerDataExpected.push_back(0x90380000);

  // - Switch MUX.
  i2cPlayerDataExpected.push_back(0x90F70004);
  // - Read tx_disable register.
  i2cPlayerDataExpected.push_back(0x90380000);

  // - Switch MUX.
  i2cPlayerDataExpected.push_back(0x90F70008);
  // - Read tx_fault register.
  i2cPlayerDataExpected.push_back(0x90380000);

  // - Switch MUX.
  i2cPlayerDataExpected.push_back(0x90F70010);
  // - Read rx_los register.
  i2cPlayerDataExpected.push_back(0x90380000);

  // End-of-commands marker.
  i2cPlayerDataExpected.push_back(0x00000000);

  return i2cPlayerDataExpected;
}

size_t
tcds::hwlayertca::I2CPlayer::calcIndexGeneralInfo(short const fmcNum,
                                                  size_t const itemOffset) const
{
  size_t const generalInfoOffset = baseTXDataOffset_;
  size_t const fmcOffset = kNumEntriesPerFMC * fmcNum;
  size_t const index = generalInfoOffset + fmcOffset + itemOffset;
  return index;
}

size_t
tcds::hwlayertca::I2CPlayer::calcIndexPerSFPInfo(short const fmcNum,
                                                 short const sfpNum,
                                                 size_t const itemOffset) const
{
  size_t const generalInfoOffset = calcIndexGeneralInfo(0, 0) + (kNumEntriesPerFMC * kNumFMCs);
  short const numSFPs = fmcNum * kNumSFPsPerFMC + sfpNum;
  size_t const sfpOffset = numSFPs * kNumEntriesPerSFP;
  size_t const index = generalInfoOffset + sfpOffset + itemOffset;
  return index;
}

std::vector<uint32_t>
tcds::hwlayertca::I2CPlayer::extractRXData(size_t const indexFirst,
                                           size_t const numEntries) const
{
  std::vector<uint32_t>::const_iterator const first = rawRXData_.begin() + indexFirst;
  std::vector<uint32_t>::const_iterator const last = first + numEntries;
  std::vector<uint32_t> const res(first, last);
  return res;
}

std::string
tcds::hwlayertca::I2CPlayer::convertRXDataToString(std::vector<uint32_t> const rxData) const
{
  std::stringstream resTmp;
  for (std::vector<uint32_t>::const_iterator iter = rxData.begin();
       iter != rxData.end();
       ++iter)
    {
      bool const isDone = (*iter & 0x04000000) != 0;
      bool const isError = (*iter & 0x08000000) != 0;
      if (isDone && !isError)
        {
          char const thisChar = char(*iter & uint32_t(0xff));
          if (std::isprint(thisChar))
            {
              resTmp << thisChar;
            }
        }
    }
  std::string const res = tcds::utils::trimString(resTmp.str());
  return res;
}

float
tcds::hwlayertca::I2CPlayer::convertRXDataToTemperature(std::vector<uint32_t> const rxData) const
{
  // The internally measured transceiver temperature is represented as
  // a 16 bit signed twos complement value in increments of 1/256
  // degrees Celsius, yielding a total range of -128C to +128C.
  I2CPlayerRXValue const rxVal(rxData.at(0));
  uint32_t const valRaw = rxVal.extractDataValue();
  uint32_t const valRawLo = valRaw & uint32_t(0x00ff);
  uint32_t const valRawHi = (valRaw & uint32_t(0xff00)) >> 8;
  // First take the MSB.
  float res = 1. * valRawHi;
  // Then process the LSB to obtain the decimal part.
  for (size_t i = 0; i != 8; ++i)
    {
      res += ((valRawLo >> (7 - i)) & uint32_t(0x1)) * std::pow(2., -1. * (i + 1));
    }
  return res;
}

float
tcds::hwlayertca::I2CPlayer::convertRXDataToVcc(std::vector<uint32_t> const rxData) const
{
  // The supply voltage is represented as a 16-bit unsigned integer
  // with the voltage defined as the full 16-bit value (0 - 65535)
  // with LSB equal to 100 uVolt. The returned result is in Volt.
  I2CPlayerRXValue const rxVal(rxData.at(0));
  float const res = rxVal.extractDataValue() * 100.e-6;
  return res;
}

float
tcds::hwlayertca::I2CPlayer::convertRXDataToRXPower(std::vector<uint32_t> const rxData) const
{
  // The TX/RX power is represented as a 16-bit unsigned integer with
  // the power defined as the full 16-bit value (0 - 65535) with LSB
  // equal to 0.1 uW. The returned result is in mW.
  I2CPlayerRXValue const rxVal(rxData.at(0));
  float const res = rxVal.extractDataValue() * .1e-6 * 1.e3;
  return res;
}

float
tcds::hwlayertca::I2CPlayer::convertRXDataToTXPower(std::vector<uint32_t> const rxData) const
{
  // The TX/RX power is represented as a 16-bit unsigned integer with
  // the power defined as the full 16-bit value (0 - 65535) with LSB
  // equal to 0.1 uW. The returned result is in mW.
  I2CPlayerRXValue const rxVal(rxData.at(0));
  float const res = rxVal.extractDataValue() * .1e-6 * 1.e3;
  return res;
}

float
tcds::hwlayertca::I2CPlayer::convertRXDataToTXBiasCurrent(std::vector<uint32_t> const rxData) const
{
  // The TX bias current is represented as a 16-bit unsigned integer
  // with the current defined as the full 16-bit value (0 - 65535)
  // with LSB equal to 2 uA. The returned result is in uA.
  I2CPlayerRXValue const rxVal(rxData.at(0));
  float const res = rxVal.extractDataValue() * 2.e-6 * 1.e3;
  return res;
}
