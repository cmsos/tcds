#include "tcds/hwlayertca/TCACarrierGLIB.h"

#include <cassert>
#include <map>
#include <stdint.h>
#include <unistd.h>

#include "toolbox/string.h"
#include "xcept/Exception.h"

#include "tcds/exception/Exception.h"
#include "tcds/hwlayertca/HwDeviceTCA.h"

tcds::hwlayertca::TCACarrierGLIB::TCACarrierGLIB(HwDeviceTCA& device) :
  TCACarrierBase(device)
{
}

tcds::hwlayertca::TCACarrierGLIB::~TCACarrierGLIB()
{
}

void
tcds::hwlayertca::TCACarrierGLIB::loadFirmwareImageImpl(std::string const& imageName) const
{
  uint32_t flashPageNumber = 0;
  if (imageName == "golden")
    {
      flashPageNumber = 0;
    }
  else if (imageName == "user")
    {
      flashPageNumber = 2;
    }
  else
    {
      std::string const msg =
        toolbox::toString("Failed to interpret '%s' as a valid GLIB firmware image name.",
                          imageName.c_str());
      XCEPT_RAISE(tcds::exception::ValueError, msg.c_str());
    }

  hwDevice_.writeRegister("glib.ctrl2.flash_firmware_page", flashPageNumber);
  hwDevice_.writeRegister("glib.ctrl2.load_flash_firmware", 0x1);

  // Wait for the board to come back to life.
  ::sleep(15);
}

void
tcds::hwlayertca::TCACarrierGLIB::powerUpImpl() const
{
  // The GLIB does not require any power-up sequence.
  // NOTE: This is not really according to the uTCA standard.
}

void
tcds::hwlayertca::TCACarrierGLIB::selectClockSourceImpl(tcds::hwlayertca::TCACarrierBase::CLOCK_SOURCE const clkSrc) const
{
  // This method drives the configuration of the GLIB cross-point
  // switches routing the clock to the FPGA.

  uint32_t orMask = 0x0;
  switch (clkSrc)
    {
    case CLOCK_SOURCE_FCLKA:
      // Select the FCLKA clock from the uTCA backplane.
      orMask = 0x00ff0000;
      break;
    case CLOCK_SOURCE_FMC:
      // Select clock0 coming from FMC1.
      orMask = 0x00000000;
      break;
    case CLOCK_SOURCE_XTAL:
      // Select the clock coming from the on-board crystal.
      // NOTE: Strictly speaking this selects the clock either from
      // the on-board crystal or from the front-panel SMA
      // connector. The real switch between these two is made with a
      // jumper on the GLIB though.
      orMask = 0x00aa0000;
      break;
    default:
      // ASSERT ASSERT ASSERT
      assert (false);
      // ASSERT ASSERT ASSERT end
      break;
    }

  uint32_t const andMask = 0xff000fff;
  std::string const& regName = "glib.ctrl";
  uint32_t const ctrlOri = hwDevice_.readRegister(regName);
  uint32_t const ctrlNew = (ctrlOri & andMask) | orMask;
  hwDevice_.writeRegister(regName, ctrlNew);
}

void
tcds::hwlayertca::TCACarrierGLIB::enableSFPsImpl() const
{
  std::vector<std::string> names = fmcNames();
  for (std::vector<std::string>::const_iterator fmcName = names.begin();
       fmcName != names.end();
       ++fmcName)
    {
      if (isFMCPresent(*fmcName))
        {
          switchFMCSFPs(*fmcName, true);
        }
    }
}

void
tcds::hwlayertca::TCACarrierGLIB::disableSFPsImpl() const
{
  std::vector<std::string> names = fmcNames();
  for (std::vector<std::string>::const_iterator fmcName = names.begin();
       fmcName != names.end();
       ++fmcName)
    {
      if (isFMCPresent(*fmcName))
        {
          switchFMCSFPs(*fmcName, false);
        }
    }
}

tcds::hwlayer::I2CAccessor::I2CBus
tcds::hwlayertca::TCACarrierGLIB::fmcBusSelect(std::string const& fmcName) const
{
  tcds::hwlayer::I2CAccessor::I2CBus res = tcds::hwlayer::I2CAccessor::I2CBus0;

  if (fmcName == "1")
    {
      res = tcds::hwlayer::I2CAccessor::I2CBus0;
    }
  else if (fmcName == "2")
    {
      res = tcds::hwlayer::I2CAccessor::I2CBus1;
    }

  return res;
}

void
tcds::hwlayertca::TCACarrierGLIB::switchFMCSFPs(std::string const& fmcName, bool const switchOn) const
{
  // BUG BUG BUG
  // Hard-coded number here. May want to move this.
  uint32_t const kI2CAddressSFPStatusRegisters = 0x38;
  std::map<std::string, uint32_t> kI2CAddressMUXSFPStatusRegistersMap;
  kI2CAddressMUXSFPStatusRegistersMap["1"] = 0x74;
  kI2CAddressMUXSFPStatusRegistersMap["2"] = 0x77;
  // BUG BUG BUG end

  // NOTE: The bus-select value depends on the FMC being addressed.
  tcds::hwlayer::I2CAccessor::I2CBus busSelect = fmcBusSelect(fmcName);

  // NOTE: This is the MUX channels connected to the SFP TX_DIS line.
  uint8_t const channelNum = 2;

  // NOTE: The SFPs are _enabled_ by setting their TX_DISABLE lines to
  // zero.
  uint8_t stateVal = 0xff;
  if (switchOn)
    {
      stateVal = 0x00;
    }

  uint32_t const i2cAddressMUXSFPStatusRegisters = kI2CAddressMUXSFPStatusRegistersMap[fmcName];
  tcds::hwlayer::I2CAccessor i2c(hwDevice_,
                                 tcds::hwlayer::I2CAccessor::I2CMasterUser,
                                 busSelect);

  // Step 1: select the right MUX channel.
  i2c.writeRegister(i2cAddressMUXSFPStatusRegisters, (0x1 << channelNum));

  // Step 2: write the new value to the register pointed to by the
  // selected MUX channel.
  i2c.writeRegister(kI2CAddressSFPStatusRegisters, stateVal);
}

std::string
tcds::hwlayertca::TCACarrierGLIB::expectedCarrierType() const
{
  return "GLIB";
}

std::string
tcds::hwlayertca::TCACarrierGLIB::registerNamePrefix() const
{
  return "glib";
}

std::vector<std::string>
tcds::hwlayertca::TCACarrierGLIB::fmcNamesImpl() const
{
  std::vector<std::string> res;
  res.push_back("1");
  res.push_back("2");
  return res;
}
bool

tcds::hwlayertca::TCACarrierGLIB::isFMCPowerGoodImpl(std::string const& fmcName) const
{
  // The GLIB does not do power-good, really.
  return true;
}
