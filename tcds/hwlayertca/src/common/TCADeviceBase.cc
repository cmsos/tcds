#include "tcds/hwlayertca/TCADeviceBase.h"

#include <algorithm>
#include <ctime>
#include <ctype.h>
#include <iomanip>
#include <sstream>
#include <utility>

#include "toolbox/string.h"
#include "xcept/Exception.h"

#include "tcds/exception/Exception.h"
#include "tcds/hwlayer/Utils.h"

uint32_t const
tcds::hwlayertca::TCADeviceBase::kMeasDuration = 0x10;

double const
tcds::hwlayertca::TCADeviceBase::kCalFactor = (1. / 538) * 1.e-9;

tcds::hwlayertca::TCADeviceBase::TCADeviceBase(std::unique_ptr<TCACarrierBase> carrier) :
  hwCarrierP_(std::move(carrier))
{
}

tcds::hwlayertca::TCADeviceBase::~TCADeviceBase()
{
}

uint32_t
tcds::hwlayertca::TCADeviceBase::readRegister(std::string const& regName,
                                              bool const unprefixed) const
{
  return readRegisterImpl(regName, unprefixed);
}

void
tcds::hwlayertca::TCADeviceBase::writeRegister(std::string const& regName,
                                               uint32_t const regVal,
                                               bool const unprefixed) const
{
  writeRegisterImpl(regName, regVal, unprefixed);
}

std::vector<uint32_t>
tcds::hwlayertca::TCADeviceBase::readBlock(std::string const& regName,
                                           bool const unprefixed) const
{
  return readBlockImpl(regName, getBlockSize(regName, unprefixed), unprefixed);
}

std::vector<uint32_t>
tcds::hwlayertca::TCADeviceBase::readBlock(std::string const& regName,
                                           uint32_t const nWords,
                                           bool const unprefixed) const
{
  return readBlockImpl(regName, nWords, unprefixed);
}

void
tcds::hwlayertca::TCADeviceBase::writeBlock(std::string const& regName,
                                            std::vector<uint32_t> const& regVal,
                                            bool const unprefixed) const
{
  writeBlockImpl(regName, regVal, unprefixed);
}

uint32_t
tcds::hwlayertca::TCADeviceBase::getBlockSize(std::string const& regName,
                                              bool const unprefixed) const
{
  return getBlockSizeImpl(regName, unprefixed);
}

bool
tcds::hwlayertca::TCADeviceBase::isReadyForUseImpl() const
{
  return isHwConnected();
}

std::string
tcds::hwlayertca::TCADeviceBase::regNamePrefixed(std::string const& regName) const
{
  std::string regNameLocal(regName);
  std::string prefix = regNamePrefix();
  if (!prefix.empty())
    {
      regNameLocal = prefix + regName;
    }
  return regNameLocal;
}

bool
tcds::hwlayertca::TCADeviceBase::bootstrapDone() const
{
  return bootstrapDoneImpl();
}

bool
tcds::hwlayertca::TCADeviceBase::bootstrapDoneImpl() const
{
  // The default is: no bootstrap needed. So return true.
  return true;
}

void
tcds::hwlayertca::TCADeviceBase::runBootstrap() const
{
  runBootstrapImpl();
}

void
tcds::hwlayertca::TCADeviceBase::runBootstrapImpl() const
{
  // The default is: no bootstrap needed. So nothing to do.
}

void
tcds::hwlayertca::TCADeviceBase::loadFirmwareImage(std::string const& imageName) const
{
  hwCarrierP_->loadFirmwareImage(imageName);
}

void
tcds::hwlayertca::TCADeviceBase::hwConnect(std::string const& connectionsFileName,
                                           std::string const& connectionName)
{
  hwConnectImpl(connectionsFileName, connectionName);
}

void
tcds::hwlayertca::TCADeviceBase::hwConnectImpl(std::string const& connectionsFileName,
                                               std::string const& connectionName)
{
  hwDevice_.hwConnect(connectionsFileName, connectionName);

  //----------

  // If we're still on the golden firmware image, jump to the user
  // firmware image.
  std::string systemId = readSystemId();
  std::transform(systemId.begin(), systemId.end(), systemId.begin(), ::tolower);
  if (systemId == "gold")
    {
      try
        {
          loadFirmwareImage("user");
        }
      catch (tcds::exception::Exception& err)
        {
          std::string const msg =
            toolbox::toString("Failed to jump to the 'user' firmware image: '%s'.",
                              err.what());
          XCEPT_RETHROW(tcds::exception::HardwareProblem, msg, err);
        }
      time_t const afterFirmwareJumpSleepTime = 3;
      tcds::hwlayer::sleep(afterFirmwareJumpSleepTime);
      hwDevice_.hwRelease();
      hwDevice_.hwConnect(connectionsFileName, connectionName);
      std::string systemId = readSystemId();
      std::transform(systemId.begin(), systemId.end(), systemId.begin(), ::tolower);
      if (systemId == "gold")
        {
          std::string const msg = "Failed to jump to the 'user' firmware image."
            "Maybe the image is corrupted/absent?";
          XCEPT_RAISE(tcds::exception::HardwareProblem, msg);
        }
    }

  //----------

  // Now we are officially connected to the hardware. Next step is to
  // do some grunt work if no one else has done this yet for us.
  if (!bootstrapDone())
    {
      try
        {
          runBootstrap();
        }
      catch (tcds::exception::Exception& err)
        {
          std::string const msg =
            toolbox::toString("Failed to bootstrap the hardware device: '%s'.",
                              err.what());
          XCEPT_RETHROW(tcds::exception::HardwareProblem, msg, err);
        }
    }
}

void
tcds::hwlayertca::TCADeviceBase::hwRelease()
{
  hwReleaseImpl();
}

void
tcds::hwlayertca::TCADeviceBase::hwReleaseImpl()
{
  hwDevice_.hwRelease();
}

std::vector<std::string>
tcds::hwlayertca::TCADeviceBase::getRegisterNamesImpl() const
{
  std::vector<std::string> res = hwDevice_.getRegisterNames();
  res.erase(std::remove_if(res.begin(),
                           res.end(),
                           std::not1(RegNameMatchesA(regNamePrefix()))),
            res.end());
  // And now remove the prefixes on whatever is left.
  std::transform(res.begin(),
                 res.end(),
                 res.begin(),
                 RegNameUnprefixer(regNamePrefix()));
  return res;
}

tcds::hwlayer::RegisterInfo::RegInfoVec
tcds::hwlayertca::TCADeviceBase::getRegisterInfosImpl() const
{
  tcds::hwlayer::RegisterInfo::RegInfoVec res = hwDevice_.getRegisterInfos();
  // Filter out everything that does not belong to us.
  res.erase(std::remove_if(res.begin(),
                           res.end(),
                           std::not1(RegNameMatchesB(regNamePrefix()))),
            res.end());
  // And now remove the prefixes on whatever is left.
  std::transform(res.begin(),
                 res.end(),
                 res.begin(),
                 RegNameUnprefixer(regNamePrefix()));
  return res;
}

uint32_t
tcds::hwlayertca::TCADeviceBase::readRegisterImpl(std::string const& regName) const
{
  return readRegisterImpl(regName, false);
}

uint32_t
tcds::hwlayertca::TCADeviceBase::readRegisterImpl(std::string const& regName,
                                                  bool const unprefixed) const
{
  std::string const regNameTmp = unprefixed ? regName : regNamePrefixed(regName);
  return hwDevice_.readRegister(regNameTmp);
}

void
tcds::hwlayertca::TCADeviceBase::writeRegisterImpl(std::string const& regName,
                                                   uint32_t const regVal) const
{
  writeRegisterImpl(regName, regVal, false);
}

void
tcds::hwlayertca::TCADeviceBase::writeRegisterImpl(std::string const& regName,
                                                   uint32_t const regVal,
                                                   bool const unprefixed) const
{
  std::string const regNameTmp = unprefixed ? regName : regNamePrefixed(regName);
  hwDevice_.writeRegister(regNameTmp, regVal);
}

std::vector<uint32_t>
tcds::hwlayertca::TCADeviceBase::readBlockImpl(std::string const& regName,
                                               uint32_t const nWords) const
{
  return readBlockImpl(regName, nWords, false);
}

std::vector<uint32_t>
tcds::hwlayertca::TCADeviceBase::readBlockImpl(std::string const& regName,
                                               uint32_t const nWords,
                                               bool const unprefixed) const
{
  std::string const regNameTmp = unprefixed ? regName : regNamePrefixed(regName);
  return hwDevice_.readBlock(regNameTmp, nWords);
}

std::vector<uint32_t>
tcds::hwlayertca::TCADeviceBase::readBlockOffsetImpl(std::string const& regName,
                                                     uint32_t const nWords,
                                                     uint32_t const offset) const
{
  return hwDevice_.readBlockOffset(regNamePrefixed(regName), nWords, offset);
}

// std::vector<uint32_t>
// tcds::hwlayertca::TCADeviceBase::readBlock(std::string const& regName) const
// {
//   return readBlock(regName, getBlockSize(regName));
// }

void
tcds::hwlayertca::TCADeviceBase::writeBlockImpl(std::string const& regName,
                                                std::vector<uint32_t> const& regVals) const
{
  writeBlockImpl(regName, regVals, false);
}

void
tcds::hwlayertca::TCADeviceBase::writeBlockImpl(std::string const& regName,
                                                std::vector<uint32_t> const& regVals,
                                                bool const unprefixed) const
{
  std::string const regNameTmp = unprefixed ? regName : regNamePrefixed(regName);
  hwDevice_.writeBlock(regNameTmp, regVals);
}

uint32_t
tcds::hwlayertca::TCADeviceBase::readModifyWriteRegister(std::string const& regName,
                                                         uint32_t const regVal) const
{
  return readModifyWriteRegisterImpl(regName, regVal);
}

uint32_t
tcds::hwlayertca::TCADeviceBase::readModifyWriteRegisterImpl(std::string const& regName,
                                                             uint32_t const regVal) const
{
  return hwDevice_.readModifyWriteRegister(regNamePrefixed(regName), regVal);
}

uint32_t
tcds::hwlayertca::TCADeviceBase::getBlockSizeImpl(std::string const& regName) const
{
  return getBlockSize(regName, false);
}

uint32_t
tcds::hwlayertca::TCADeviceBase::getBlockSizeImpl(std::string const& regName,
                                                  bool const unprefixed) const
{
  std::string const regNameTmp = unprefixed ? regName : regNamePrefixed(regName);
  return hwDevice_.getBlockSize(regNameTmp);
}

void
tcds::hwlayertca::TCADeviceBase::writeHardwareConfigurationImpl(tcds::hwlayer::ConfigurationProcessor::RegValVec const& cfg) const
{
  for (tcds::hwlayer::ConfigurationProcessor::RegValVec::const_iterator it = cfg.begin();
       it != cfg.end();
       ++it)
    {
      // NOTE: Some care is required here. For single-word writes the
      // appropriate mask (specified in the address table) is
      // applied. Block writes don't know about masks.
      if (it->second.size() == 1)
        {
          writeRegister(it->first, it->second.at(0));
        }
      else
        {
          writeBlock(it->first, it->second);
        }
    }
}

tcds::hwlayer::DeviceBase::RegContentsVec
tcds::hwlayertca::TCADeviceBase::readHardwareConfigurationImpl(tcds::hwlayer::RegisterInfo::RegInfoVec const& regInfos) const
{
  tcds::hwlayer::DeviceBase::RegContentsVec res;
  for (tcds::hwlayer::RegisterInfo::RegInfoVec::const_iterator regInfo = regInfos.begin();
       regInfo != regInfos.end();
       ++regInfo)
    {
      std::vector<uint32_t> regVals;
      // NOTE: Some care is required here. For single-word reads the
      // appropriate mask (specified in the address table) is
      // applied. Block reads don't know about masks.
      uint32_t const size = getBlockSize(regInfo->name());
      if (size == 1)
        {
          regVals.push_back(readRegister(regInfo->name()));
        }
      else
        {
          regVals = readBlock(regInfo->name());
        }

      res.push_back(std::make_pair(*regInfo, regVals));
    }
  return res;
}

tcds::hwlayer::RegDumpVec
tcds::hwlayertca::TCADeviceBase::dumpRegisterContentsImpl() const
{
  return hwDevice_.dumpRegisterContents();
}

bool
tcds::hwlayertca::TCADeviceBase::isHwConnected() const
{
  return hwDevice_.isHwConnected();
}

void
tcds::hwlayertca::TCADeviceBase::selectClockSource(tcds::hwlayertca::TCACarrierBase::CLOCK_SOURCE const clkSrc) const
{
  hwCarrierP_->selectClockSource(clkSrc);
}

void
tcds::hwlayertca::TCADeviceBase::resetPLL() const
{
  hwCarrierP_->resetPLL();
}

void
tcds::hwlayertca::TCADeviceBase::enableSFPs() const
{
  hwCarrierP_->enableSFPs();
}

void
tcds::hwlayertca::TCADeviceBase::disableSFPs() const
{
  hwCarrierP_->disableSFPs();
}

std::string
tcds::hwlayertca::TCADeviceBase::readEUI48() const
{
  return readEUI48Impl();
}

std::string
tcds::hwlayertca::TCADeviceBase::readEUI48Impl() const
{
  return hwCarrierP_->readEUI48();
}

std::string
tcds::hwlayertca::TCADeviceBase::readMACAddress() const
{
  return readMACAddressImpl();
}

std::string
tcds::hwlayertca::TCADeviceBase::readMACAddressImpl() const
{
  return hwCarrierP_->readMACAddress();
}

std::string
tcds::hwlayertca::TCADeviceBase::readBoardId() const
{
  return readBoardIdImpl();
}

std::string
tcds::hwlayertca::TCADeviceBase::readBoardIdImpl() const
{
  // The board ID consists of four characters encoded as a single
  // 32-bit word.
  uint32_t val = hwDevice_.readRegister("system.board_id");
  std::string res = tcds::hwlayer::uint32ToString(val);
  return res;
}

std::string
tcds::hwlayertca::TCADeviceBase::readSystemId() const
{
  return readSystemIdImpl();
}

std::string
tcds::hwlayertca::TCADeviceBase::readSystemIdImpl() const
{
  // The system ID consists of four characters encoded as a single
  // 32-bit word.
  uint32_t val = hwDevice_.readRegister("user.system_id");
  std::string res = tcds::hwlayer::uint32ToString(val);
  return res;
}

std::string
tcds::hwlayertca::TCADeviceBase::readSystemFirmwareVersion() const
{
  return readSystemFirmwareVersionImpl();
}

std::string
tcds::hwlayertca::TCADeviceBase::readSystemFirmwareVersionImpl() const
{
  return readFirmwareVersion("system");
}

std::string
tcds::hwlayertca::TCADeviceBase::readUserFirmwareVersion() const
{
  return readUserFirmwareVersionImpl();
}

std::string
tcds::hwlayertca::TCADeviceBase::readUserFirmwareVersionImpl() const
{
  return readFirmwareVersion("user");
}

std::string
tcds::hwlayertca::TCADeviceBase::readSystemFirmwareDate() const
{
  return readSystemFirmwareDateImpl();
}

std::string
tcds::hwlayertca::TCADeviceBase::readSystemFirmwareDateImpl() const
{
  return readFirmwareDate("system");
}

std::string
tcds::hwlayertca::TCADeviceBase::readUserFirmwareDate() const
{
  return readUserFirmwareDateImpl();
}

std::string
tcds::hwlayertca::TCADeviceBase::readUserFirmwareDateImpl() const
{
  return readFirmwareDate("user");
}

bool
tcds::hwlayertca::TCADeviceBase::isTTCClockUp() const
{
  return isTTCClockUpImpl();
}

bool
tcds::hwlayertca::TCADeviceBase::isTTCClockUpImpl() const
{
  uint32_t const tmp = hwDevice_.readRegister("user.ttc_clock_mon.ttc_clock_up");
  return (tmp != 0x0);
}

bool
tcds::hwlayertca::TCADeviceBase::isTTCClockStable() const
{
  return isTTCClockStableImpl();
}

bool
tcds::hwlayertca::TCADeviceBase::isTTCClockStableImpl() const
{
  uint32_t const tmp = hwDevice_.readRegister("user.ttc_clock_mon.ttc_clock_stable");
  return (tmp != 0x0);
}

uint32_t
tcds::hwlayertca::TCADeviceBase::readTTCClockUnlockCounter() const
{
  return readTTCClockUnlockCounterImpl();
}

uint32_t
tcds::hwlayertca::TCADeviceBase::readTTCClockUnlockCounterImpl() const
{
  return hwDevice_.readRegister("user.ttc_clock_mon.ttc_clock_loss_counter");
}

bool
tcds::hwlayertca::TCADeviceBase::isPhaseMonLocked() const
{
  // Check that both MMCMs used by the phase measurements are locked.
  uint32_t const tmp1 = hwDevice_.readRegister("user.ttc_phase_mon.meas_common.mmcm1_locked");
  uint32_t const tmp2 = hwDevice_.readRegister("user.ttc_phase_mon.meas_common.mmcm2_locked");
  bool const isMMCM1Locked = (tmp1 == 0x1);
  bool const isMMCM2Locked = (tmp2 == 0x1);
  return (isMMCM1Locked && isMMCM2Locked);
}

void
tcds::hwlayertca::TCADeviceBase::enablePhaseMonitoring() const
{
  // Configure the measurement duration.
  // NOTE: Don't put this too high. The monitoring will expect new
  // numbers at least once per second.
  hwDevice_.writeRegister("user.ttc_phase_mon.meas_40.measurement_duration", kMeasDuration);
  hwDevice_.writeRegister("user.ttc_phase_mon.meas_160.measurement_duration", kMeasDuration);

  // Enable the measurement.
  hwDevice_.writeRegister("user.ttc_phase_mon.meas_40.measurement_enable", 0x1);
  hwDevice_.writeRegister("user.ttc_phase_mon.meas_160.measurement_enable", 0x1);

  // Reset the measurement for good measure (e.g., in case it was
  // already enabled with a different duration before).
  hwDevice_.writeRegister("user.ttc_phase_mon.meas_40.measurement_reset", 0x0);
  hwDevice_.writeRegister("user.ttc_phase_mon.meas_40.measurement_reset", 0x1);
  hwDevice_.writeRegister("user.ttc_phase_mon.meas_40.measurement_reset", 0x0);
  hwDevice_.writeRegister("user.ttc_phase_mon.meas_160.measurement_reset", 0x0);
  hwDevice_.writeRegister("user.ttc_phase_mon.meas_160.measurement_reset", 0x1);
  hwDevice_.writeRegister("user.ttc_phase_mon.meas_160.measurement_reset", 0x0);
}

double
tcds::hwlayertca::TCADeviceBase::readPhaseMonitoring40MHz() const
{
  // NOTE: Return value in s.
  uint32_t const regVal = hwDevice_.readRegister("user.ttc_phase_mon.meas_40.measurement_value");
  double const res = kCalFactor * regVal  / kMeasDuration;
  return res;
}

double
tcds::hwlayertca::TCADeviceBase::readPhaseMonitoring160MHz() const
{
  // NOTE: Return value in s.
  uint32_t const regVal = hwDevice_.readRegister("user.ttc_phase_mon.meas_160.measurement_value");
  double const res = kCalFactor * regVal  / kMeasDuration;
  return res;
}

std::string
tcds::hwlayertca::TCADeviceBase::readFirmwareVersion(std::string const& regNamePrefix) const
{
  // This returns the system/user firmware version number.
  std::stringstream res;
  std::stringstream regName;
  regName << regNamePrefix << ".firmware_id.ver_major";
  uint32_t versionMajor = hwDevice_.readRegister(regName.str());
  regName.str("");
  regName << regNamePrefix << ".firmware_id.ver_minor";
  uint32_t versionMinor = hwDevice_.readRegister(regName.str());
  regName.str("");
  regName << regNamePrefix << ".firmware_id.ver_build";
  uint32_t versionBuild = hwDevice_.readRegister(regName.str());
  res << versionMajor << "." << versionMinor << "." << versionBuild;
  return res.str();
}

std::string
tcds::hwlayertca::TCADeviceBase::readFirmwareDate(std::string const& regNamePrefix) const
{
  // This returns the system/user firmware build date.
  std::stringstream res;
  std::stringstream regName;
  regName << regNamePrefix << ".firmware_id.date_yy";
  uint32_t yy = hwDevice_.readRegister(regName.str());
  regName.str("");
  regName << regNamePrefix << ".firmware_id.date_mm";
  uint32_t mm = hwDevice_.readRegister(regName.str());
  regName.str("");
  regName << regNamePrefix << ".firmware_id.date_dd";
  uint32_t dd = hwDevice_.readRegister(regName.str());
  res << "20" << std::setfill('0') << std::setw(2) << yy
      << "-"
      << std::setw(2) << mm
      << "-"
      << std::setw(2) << dd;
  return res.str();
}

std::string
tcds::hwlayertca::TCADeviceBase::regNamePrefix() const
{
  // NOTE: The assumption is that prefixes include the trailing '.'.
  std::string prefix = regNamePrefixImpl();
  if (!prefix.empty())
    {
      if (*prefix.rbegin() != '.')
        {
          prefix.append(".");
        }
    }
  return prefix;
}

std::string
tcds::hwlayertca::TCADeviceBase::regNamePrefixImpl() const
{
  // The default is no prefix at all.
  return "";
}
