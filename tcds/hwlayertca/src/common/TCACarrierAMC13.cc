#include "tcds/hwlayertca/TCACarrierAMC13.h"

tcds::hwlayertca::TCACarrierAMC13::TCACarrierAMC13(HwDeviceTCA& device) :
  TCACarrierBase(device)
{
}

tcds::hwlayertca::TCACarrierAMC13::~TCACarrierAMC13()
{
}

void
tcds::hwlayertca::TCACarrierAMC13::loadFirmwareImageImpl(std::string const& imageName) const
{
  // The AMC13 does not do multiple firmware images. So nothing to do
  // here, really.
}

void
tcds::hwlayertca::TCACarrierAMC13::powerUpImpl() const
{
  // The AMC13 does not require any power-up sequence.
}

void
tcds::hwlayertca::TCACarrierAMC13::selectClockSourceImpl(tcds::hwlayertca::TCACarrierBase::CLOCK_SOURCE const clkSrc) const
{
  // The AMC13 'carrier' is only used as base for the CPM, and the CPM
  // clock source should not be switched.
}

void
tcds::hwlayertca::TCACarrierAMC13::enableSFPsImpl() const
{
  // The SFPs on the AMC13 are enabled by default.

  // BUG BUG BUG
  // Should really implement a positive enable action here.
  // BUG BUG BUG end
}

void
tcds::hwlayertca::TCACarrierAMC13::disableSFPsImpl() const
{
  // The SFPs on the AMC13 are enabled by default. There is a register
  // in the firmware somewhere that allows to turn them off.

  // BUG BUG BUG
  // To be implemented at some point.
  // BUG BUG BUG end
}

std::string
tcds::hwlayertca::TCACarrierAMC13::expectedCarrierType() const
{
  return "AMC13";
}

std::string
tcds::hwlayertca::TCACarrierAMC13::registerNamePrefix() const
{
  return "amc13";
}

std::vector<std::string>
tcds::hwlayertca::TCACarrierAMC13::fmcNamesImpl() const
{
  std::vector<std::string> res;
  return res;
}
bool

tcds::hwlayertca::TCACarrierAMC13::isFMCPowerGoodImpl(std::string const& fmcName) const
{
  // The AMC13 does not do power-good, since it does not have any
  // FMCs.
  return true;
}
