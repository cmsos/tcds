#include "tcds/hwlayertca/BootstrapHelper.h"

#include <cstddef>
#include <stdint.h>
#include <string>

#include "toolbox/string.h"
#include "xcept/Exception.h"

#include "tcds/exception/Exception.h"
#include "tcds/hwlayer/Utils.h"
#include "tcds/hwlayertca/I2CPlayer.h"
#include "tcds/hwlayertca/TCACarrierBase.h"
#include "tcds/hwlayertca/TCADeviceBase.h"

tcds::hwlayertca::BootstrapHelper::BootstrapHelper(tcds::hwlayertca::TCADeviceBase const& device) :
  device_(device)
{
}

tcds::hwlayertca::BootstrapHelper::~BootstrapHelper()
{
}

bool
tcds::hwlayertca::BootstrapHelper::bootstrapDone() const
{
  uint32_t const val = device_.hwDevice_.readRegister("system.accounting.bootstrap_done");
  bool const res = (val != 0x0);
  return res;
}

void
tcds::hwlayertca::BootstrapHelper::runBootstrap() const
{
  // The settle time in seconds in between the steps of the
  // bootstrapping procedure.
  unsigned int const settleTime = 1;

  if (claimBootstrap())
    {
      try
        {
          // Power-up the uTCA carrier.
          device_.hwCarrierP_->powerUp();
          tcds::hwlayer::sleep(settleTime);

          // Select the backplane clock (FCLKA).
          device_.selectClockSource(hwlayertca::TCACarrierBase::CLOCK_SOURCE_FCLKA);
          tcds::hwlayer::sleep(settleTime);

          // Enable all SFPs.
          device_.enableSFPs();
          tcds::hwlayer::sleep(settleTime);

          // Configure and enable the I2C player that runs the SFP
          // monitoring.
          tcds::hwlayertca::I2CPlayer i2cPlayer(device_);
          i2cPlayer.disable();
          i2cPlayer.verify();
          i2cPlayer.program();
          i2cPlayer.enable();

          setBootstrapDone();
          unsetBootstrapInProgress();
        }
      catch (...)
        {
          unsetBootstrapDone();
          unsetBootstrapInProgress();
          throw;
        }
    }
  else
    {
      // Some other piece of software is already doing the
      // bootstrapping. Let's wait to see if this finishes.
      size_t const secondsPerIteration = 1;
      size_t const maxNumIterations = 20;
      size_t iteration = 0;
      bool bootstrapIsInProgress = bootstrapInProgress();
      while (bootstrapIsInProgress && (iteration < maxNumIterations))
        {
          tcds::hwlayer::sleep(secondsPerIteration);
          ++iteration;
          bootstrapIsInProgress = bootstrapInProgress();
        }
      if (!bootstrapInProgress())
        {
          // Check if bootstrap finished successfully or not.
          if (!bootstrapDone())
            {
              std::string const msg =
                toolbox::toString("Some process other than this one "
                                  "was trying to bootstrap the hardware. "
                                  "Now it has finished, but it seems to have failed.");
              XCEPT_RAISE(tcds::exception::RuntimeProblem, msg.c_str());
            }
        }
      else
        {
          std::string const msg =
            toolbox::toString("Some process other than this one "
                              "is trying to bootstrap the hardware. "
                              "Tried waiting but got bored.");
          XCEPT_RAISE(tcds::exception::RuntimeProblem, msg.c_str());
        }
    }
}

bool
tcds::hwlayertca::BootstrapHelper::setBootstrapDone() const
{
  uint32_t const val = device_.hwDevice_.readModifyWriteRegister("system.accounting.bootstrap_done", 0x1);
  bool const res = (val != 0x0);
  return res;
}

void
tcds::hwlayertca::BootstrapHelper::unsetBootstrapDone() const
{
  device_.hwDevice_.writeRegister("system.accounting.bootstrap_done", 0x0);
}

bool
tcds::hwlayertca::BootstrapHelper::bootstrapInProgress() const
{
  uint32_t const val = device_.hwDevice_.readRegister("system.accounting.bootstrap_in_progress");
  bool const res = (val != 0x0);
  return res;
}

bool
tcds::hwlayertca::BootstrapHelper::claimBootstrap() const
{
  uint32_t const val = device_.hwDevice_.readModifyWriteRegister("system.accounting.bootstrap_in_progress", 0x1);
  bool const nothingInProgress = (val == 0x0);
  bool const done = bootstrapDone();
  bool const res = nothingInProgress && !done;
  return res;
}

void
tcds::hwlayertca::BootstrapHelper::unsetBootstrapInProgress() const
{
  device_.hwDevice_.writeRegister("system.accounting.bootstrap_in_progress", 0x0);
}
