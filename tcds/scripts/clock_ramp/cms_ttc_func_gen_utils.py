#!/usr/bin/env python

######################################################################
## File       : cms_ttc_func_gen_utils.py
## Author     : Jeroen Hegeman
##              jeroen.hegeman@cern.ch
## Last change: 20200505
##
## Purpose :
##   Library with Python utilities to use the function generator used
##   at P5 for the CMS TTC tests. The generator available there is an
##   Keysight/Agilent 33611A, registered with DNS alias
##   tcds-freq-gen.cms.
######################################################################

######################################################################
## TODO:
######################################################################

import socket
import time

######################################################################
## FuncGen class.
######################################################################

class FuncGen(object):
    """Kind of a base class for the function generator interface.

    The reason we need this is to allow the use of a `dummy' generator
    for debugging etc.

    """

    ##########

    def __init__(self, address, port):

        # The network address and port where we can reach the function
        # generator.
        self._address = address
        self._port = port

        self.set_offset(0.)

        # End of __init__.

    ##########

    def send_command(self, cmd, expect_response=False):
        """Send a command to the dummy function generator.

        Obviously this does nothing.

        """

        # End of send_command.
        return None

    ##########

    def set_display(self, display_text=None):
        """Show a message on the display.

        Obviously this does nothing.

        """

        # End of set_display.

    ##########

    def set_offset(self, offset):
        """Set the offset frequency.

        This offset frequency is added to the given frequency when
        setting, and subtracted from the frequency when getting.

        """

        self._offset = offset

        # End of set_offset.

    ##########

    def get_offset(self):
        "Return the set offset."

        # End of get_offset.
        return self._offset

    ##########

    # End of class FuncGen.

######################################################################
## FuncGenDummy class.
######################################################################

class FuncGenDummy(FuncGen):
    """Dummy class for testing of the CMS clock ramper offline.

    This dummy generator interface is used by the CMS clock ramper in
    dry-run mode useful for offline debugging etc.

    """

    ##########

    def __init__(self, address, port):

        FuncGen.__init__(self, address, port)

        # Since there is no `dummy hardware' we have to keep track of
        # the frequencies otherwise.
        self._frequency = None

    ##########

    def identify_yourself(self):
        "Show us who you are."

        # End of identify_yourself.
        return "dummy generator"

    ##########

    def set_frequency(self, freq):
        "Switch the dummy generator to the given frequency (in Hz)."

        self._frequency = freq

        # End of set_frequency.

    ##########

    def get_frequency(self):
        "Ask the dummy generator for the current frequency (in Hz)."

        # End of get_frequency.
        return self._frequency

    ##########

    # End of class FuncGenDummy.

######################################################################
## FuncGenAgilent33250A class.
######################################################################

class FuncGenAgilent33250A(FuncGen):
    """Interface to the CMS TTC function generator at P5.

    NOTE: This is specialised for the Agilent 33250A function
    generator used for the CMS TTC tests at P5.

    """

    ##########

    def __init__(self, address="tcds-freq-gen.cms", port=5025):

        FuncGen.__init__(self, address, port)

        # End of __init__.

    ##########

    def send_command(self, cmd, expect_response=False):
        """Send a command to the function generator.

        This sends a command to the function generator and if
        requested waits for the response and returns it. If no
        response is expected, None is returned.

        """

        response = None

        # All commands should end with `\n'.
        cmd_eol = "\n"
        if not cmd.endswith(cmd_eol):
            cmd += cmd_eol

        # Connect to the generator.
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.settimeout(1)
        s.connect((self._address, self._port))

        # Send the command.
        s.send(cmd)

        # If needed wait for a response.
        if expect_response:
            response_lines = []
            while True:
                data = s.recv(1024)
                if not data:
                    # No data left: we're done.
                    break
                response_lines.append(data)

                # All responses end with "\n" so we can also check for
                # this to see if we're done.
                if ord(data[-1]) == 10:
                    break

            response = "".join(response_lines)

        # Close the connection.
        s.close()
        time.sleep(.2)

        # End of send_command.
        return response

    ##########

    def identify_yourself(self):
        "Show us who you are."

        ident = self.send_command("*IDN?", True)

        # End of identify_yourself.
        return ident

    ##########

    def set_frequency(self, freq):
        "Switch the generator to the given frequency (in Hz)."

        # Don't forget the offset!

        self.send_command("FREQUENCY %d" % (freq + self._offset))

        # End of set_frequency.

    ##########

    def get_frequency(self):
        "Ask the generator for the current frequency (in Hz)."

        freq = self.send_command("FREQUENCY?", True)

        # End of get_frequency. Note: first convert to float, then
        # round to int since int directly does not like scientific
        # notation. (Numbers in scientific notation are not considered
        # to be ints, that's why.) And don't forget the offset!
        return round(float(freq) - self._offset)

    ##########

    def set_display(self, display_text=None):
        """Show a message on the display.

        If display_text is None the display is reset to its normal
        state.

        """

        if display_text is None:
            cmd = "DISPLAY:TEXT:CLEAR"
        else:
            cmd = "DISPLAY:TEXT '%s'" % display_text

        self.send_command(cmd)

        # End of set_display.

    ##########

    # End of class FuncGenAgilent33250A.

######################################################################
