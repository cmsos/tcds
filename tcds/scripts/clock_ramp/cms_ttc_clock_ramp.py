#!/usr/bin/env python

######################################################################
## File       : cms_ttc_clock_ramp.py
## Author     : Jeroen Hegeman
##              jeroen.hegeman@cern.ch
## Last change: 20200505
##
## Purpose :
##   Main program to run CMS `clock ramps'. This is the poor-man's
##   approach to a full clock scan until the full TTC (XDAQ)
##   monitoring is in place. Even then, this could probably be
##   recycled as front-end.  For more information please have a look
##   at the Twiki page:
##   https://twiki.cern.ch/twiki/bin/view/CMS/CMSTTCClockScans
######################################################################

######################################################################
## NOTES:
## - Ramps in the code have one-second precision for each step. This
##   is Python timing anyway, and one-second precision should be good
##   enough for any TTC ramping purposes.
######################################################################

######################################################################
## TODO:
## - Instrument all plain prints with logger output as well.
## - Should we have basic validity checking for ramps?
##   - Strictly increasing time values.
##   - Time values should start at zero.
##   - No two points with the same time value.
##   - No two points with the same frequency value(?).
## - Clean up the exception business around the ramp tweaking.
######################################################################

"""Main entry point for the CMS TTC `clock ramps'.

The clock ramps/scans are meant to find out which clock frequencies
the CMS detector electronics will take without getting into
trouble. This is relevant for the plans to keep taking `data' while
the LHC beams are a) not present or b) being injected, ramped and
conditioned. The `clock ramp' approach follows a trial-and-error
approach: use our local clock generator (Keysight function generator)
to mimick an LHC ramp and see what breaks.

"""

######################################################################

__version__ = "1.1.0"
__author__ = "Jeroen Hegeman (jhegeman@cern.ch)"

######################################################################

import sys
import os.path
import logging
import optparse
import cmd
import inspect
import socket
import time
import StringIO

# This module contains all ramp definitions.
import cms_ttc_ramp_defs

# This module contains the tools to communicate with the function
# generator.
import cms_ttc_func_gen_utils

# Debugging stuff.
try:
    import debug_hook
    import pdb
except ImportError:
    pass

######################################################################
## CMSClockRamperCmd class.
######################################################################

class CMSClockRamperCmd(cmd.Cmd):
    """Line-oriented command interpreter used by CMSClockRamper.

    This class provides the command line interpreter allowing loading,
    starting, stopping etc. of clock ramps.

    """

    ##########

    def __init__(self, cms_clock_ramper,
                 completekey="Tab",
                 stdin=sys.stdin, stdout=sys.stdout):

        cmd.Cmd.__init__(self, completekey, stdin, stdout)

        # We have to keep track of our `parent' to be able to call its
        # methods in response to commands.
        self._clock_ramper = cms_clock_ramper

        self.prompt = "cms ttc $ "
        sep_line = "=" * 60
        self.intro = sep_line + \
                     "\n" \
                     "Welcome to the CMS TTC clock ramper.\n" \
                     "Type `help' for more information," \
                     "`exit' to quit." \
                     "\n" \
                     + sep_line

    ##########

    # Of course we need a way to stop the interpreter. These are two
    # aliases that both reach the same goal.
    def do_quit(self, dummy):
        return True
    do_exit = do_quit

    def help_quit(self):
        print "Exit the CMS TTC clock ramper."
    help_exit = help_quit

    ##########

    # This method makes sure the app quit when reading input from file
    # given as a command line argument.
    do_EOF = do_quit

    ##########

    def do_list(self, dummy):
        "List all known ramps by name."

        # Ok, let's do this by looking in the cms_ttc_ramp_defs module
        # for all classes that inherit from RampInfo.
        known_ramp_classes = {}
        for attr_name in dir(cms_ttc_ramp_defs):
            attr = getattr(cms_ttc_ramp_defs, attr_name)
            if inspect.isclass(attr) and \
                   attr_name != "RampInfo" and \
                   issubclass(attr, cms_ttc_ramp_defs.RampInfo):
                known_ramp_classes[attr_name] = attr

        # Now let's show the user what we found.
        print "List of known ramps:"
        class_names = known_ramp_classes.keys()
        class_names.sort()
        for class_name in class_names:
            tmp_class = known_ramp_classes[class_name]()
            print "  `%s': %s" % (tmp_class._name, tmp_class._desc)

        # End of do_list.

    def help_list(self):

        print "List all known ramps by name."

        # End of help_list.

    ##########

    def do_load(self, ramp_name):
        "Load a given ramp (specified by name)."

        if len(ramp_name) < 1:
            print "Load which ramp? -> " \
                  "Please use `load \"RAMP_NAME\"."
            return

        # If the ramp name is embedded in quotes, get rid of those.
        ramp_name = ramp_name.strip("`'\"")

        msg = "Loading ramp `%s'..." % ramp_name
        print msg
        self._clock_ramper._logger.info(msg)

        try:
            new_ramp = getattr(cms_ttc_ramp_defs, \
                               "Ramp%s" % ramp_name)()
            self._clock_ramper._ramp = new_ramp
        except AttributeError:
            print "No such ramp exists (that I know of), sorry."

        self._clock_ramper._ramp_status = None
        # End of do_load.

    def help_load(self):

        print "Load a ramp."
        print "Syntax: `load \"RAMP_NAME\"'."

        # End of help_load.

    ##########

    def do_jump(self, new_pos):
        "Jump to new ramp position."

        if self._clock_ramper._ramp is None:
            print "No ramp loaded, please do that first."
        else:
            try:
                position = int(new_pos)
            except ValueError:
                print "Jump position `%s' does not look " \
                      "like an integer value." % \
                      new_pos
            else:
                if position < 0:
                    print "Negative positions don't work"
                elif position >= len(self._clock_ramper._ramp._ramp_info):
                    print "Cannot jump beyond end of ramp."
                    print "Ramp is only %d points long." % \
                          len(self._clock_ramper._ramp._ramp_info)
                self._clock_ramper._ramp_position = position
                self._clock_ramper._ramp_status = "jumped"

        # End of do_jump.

    def help_jump(self):

        print "Usage: jump pos"
        print "Jump to ramp position 'pos'."

        # End of help_jump.

    ##########

    def do_invert(self, dummy):
        "Invert the currently loaded ramp."

        if self._clock_ramper._ramp is None:
            print "No ramp loaded, please do that first."
        else:
            # Remember to update the status flag to indicate that
            # things are no longer ready to run.
            self._clock_ramper._ramp_status = "modified"
            self._clock_ramper._ramp.invert()

        # End of do_invert.

    def help_invert(self):

        print "Invert the currently loaded ramp."

        # End of help_invert.

    ##########

    def do_interpolate(self, npoints):
        """Interpolate the currently loaded ramp.

        This adds npoints points in between each two points of the
        current ramp.

        """

        if self._clock_ramper._ramp is None:
            print "No ramp loaded, please do that first."
        else:
            if len(npoints) < 1:
                npoints = "1"
            try:
                npoints = int(npoints)
            except ValueError:
                print "`%s' does not look like an integer value." % \
                      npoints
            else:
                # Remember to update the status flag to indicate that
                # things are no longer ready to run.
                self._clock_ramper._ramp_status = "modified"
                self._clock_ramper._ramp.interpolate(npoints)

        # End of do_interpolate.

    def help_interpolate(self):

        print """Interpolate the current ramp. I.e. add npoints points (linearly
interpolated in both time and frequency) between each two points of
the current ramp."""
        print "Usage: interpolate npoints"

        # End of help_interpolate.

    ##########

    def do_sanitize(self, dummy):
        """Sanitize the currently loaded ramp."""

        if self._clock_ramper._ramp is None:
            print "No ramp loaded, please do that first."
        else:
            # Remember to update the status flag to indicate that
            # things are no longer ready to run.
            self._clock_ramper._ramp_status = "modified"
            self._clock_ramper._ramp.sanitize()

        # End of do_sanitize.

    def help_sanitize(self):

        print "Sanitize the currently loaded ramp, i.e.,"
        print "limit time and frequency steps to meaningful values."

        # End of help_sanitize.

    ##########

    def do_shift(self, freq_shift):
        """Move the current ramp up or down in frequency.

        Of course moving things in time does not do anything.

        """

        if self._clock_ramper._ramp is None:
            print "No ramp loaded, please do that first."
        else:
            try:
                freq_shift_val = float(freq_shift)
            except ValueError:
                print "`%s' Does not look like a numeric value." % \
                      freq_shift
            else:
                # Remember to update the status flag to indicate that
                # things are no longer ready to run.
                self._clock_ramper._ramp_status = "modified"
                self._clock_ramper._ramp.shift(freq_shift_val)

        # End of do_shift.

    def help_shift(self):

        print "Move the current ramp up or down in frequency. " \
              "(Of course moving things in time does not " \
              "do anything.)"

        # End of help_shift.

    ##########

    def do_scale(self, scale_arg):
        """Scale the current ramp in either time or frequency.

        """

        scale_args = scale_arg.strip().split()
        if len(scale_args) < 2:
            print "Scaling requires two arguments."
            return

        if self._clock_ramper._ramp is None:
            print "No ramp loaded, please do that first."
        else:
            scale_val = None
            scale_axis = scale_args[0]
            try:
                scale_val = float(scale_args[1])
            except ValueError:
                # Hmmm, let's see if it's a string that evaluates to a
                # number.
                try:
                    scale_val = eval(scale_args[1], {"__builtins__":None},{})
                except:
                    print "`%s' Does not look like a numeric value." % \
                          scale_args[1]

            if not scale_val is None:
                try:
                    self._clock_ramper._ramp.scale(scale_axis,
                                                   scale_val)
                except ValueError:
                    print "Unknown scale axis `%s'" % scale_axis
                # Remember to update the status flag to indicate that
                # things are no longer ready to run.
                self._clock_ramper._ramp_status = "modified"

        # End of do_scale.

    def help_scale(self):

        print "Scale the current ramp along the time " \
              "or frequency axis."
        print "Usage: scale {time|frequency} factor"

        # End of help_scale.

    ##########

    def do_print(self, args):
        "Show the currently loaded ramp."

        args = args.strip()

        verbose = False
        if len(args) > 0:
            if args != "all":
                print "Sorry, but I don't understand `%s'" % \
                      args
            else:
                verbose = True

        if self._clock_ramper._ramp is None:
            print "No ramp loaded, please do that first."
        else:
            if verbose:
                self._clock_ramper._ramp.dump()
            else:
                print self._clock_ramper._ramp
            if not self._clock_ramper._ramp_position is None:
                print "Current position is point #%d" % \
                      self._clock_ramper._ramp_position

        # End of do_print.

    def help_print(self):

        print "Print information on the currently loaded ramp."
        print "Usage: print [all]"

        # End of help_print.

    ##########

    def do_init(self, arg):
        """Initialise the ramp: set the clock to the start frequency.

        NOTE: depending on the argument (none or `middle') this
        starting frequency is the first value or the middle frequency.

        """

        # Check that a ramp is loaded.
        if self._clock_ramper._ramp is None:
            print "No ramp loaded, please do that first."
        else:
            arg_tmp = arg.lower()
            if not arg_tmp is None and \
               not arg_tmp in ["", "middle"]:
                print "Invalid argument to init: `%s'" % \
                      arg
            else:
                # Initialise the function generator.
                self._clock_ramper.init_gen()
                # Set the first frequency.
                self._clock_ramper.init_ramp(arg_tmp)

        # Set the status flag to initialised.
        if arg is None or arg == "":
            self._clock_ramper._ramp_status = "initialised"
        else:
            self._clock_ramper._ramp_status = "initialised_%s" % arg_tmp
##        # Set the ramp position to None.
##        self._clock_ramper._ramp_position = None

        # End of do_init.

    def help_init(self):

        print "Initialise the ramp. This initialises the clock " \
              "generator and sets it to the starting frequency. " \
              "Use `init middle' to initialise to the ramp middle " \
              "value instead of the first value."

        # End of help_init.

    ##########

    def do_step(self, dummy):
        """Perform a single step of the ramp."""

        do_step = False

        if self._clock_ramper._ramp is None:
            print "No ramp loaded, please do that first."

        # Check that everything is initialised.
        elif self._clock_ramper._ramp_status != "initialised":
            if self._clock_ramper._ramp_status is None:
                print "Ramp not initialised, please do that first."
            elif self._clock_ramper._ramp_status == "modified":
                print "Ramp not initialised (or ramp modified since " \
                      "initialisation), please do that first."
            elif self._clock_ramper._ramp_status == "jumped":
                print "Please re-init after jumping to a new position"
            elif self._clock_ramper._ramp_status == "ramping":
                print "Continuing ramp..."
                do_step = True

        else:
            print "Stepping ramp..."
            do_step = True

        if do_step:
            self._clock_ramper._perform_step()

        # End of do_step().

    def help_step(self):

        print "Perform a single step of the current ramp " \
              "from the current position."

        # End of help_step().

    ##########

    def do_ramp(self, dummy):
        "Perform the clock ramp."

        do_ramp = False

        if self._clock_ramper._ramp is None:
            print "No ramp loaded, please do that first."

        # Check that everything is initialised.
        elif self._clock_ramper._ramp_status != "initialised":
            if self._clock_ramper._ramp_status is None:
                print "Ramp not initialised, please do that first."
            elif self._clock_ramper._ramp_status == "initialised_middle":
                print "Ramp was initialised to the middle frequency. " \
                      "Please re-init to the starting frequency first."
            elif self._clock_ramper._ramp_status == "modified":
                print "Ramp not initialised (or ramp modified since " \
                      "initialisation), please do that first."
            elif self._clock_ramper._ramp_status == "jumped":
                print "Please re-init after jumping to a new position"
            elif self._clock_ramper._ramp_status == "ramping":
                print "Continuing ramp..."
                do_ramp = True
            else:
                print "WARNING: Something strange is happening!"

        else:
            print "Starting ramp..."
            do_ramp = True

        if do_ramp:
            self._clock_ramper._perform_ramp()

        # End of do_ramp.

    def help_ramp(self):

        print "Start the frequency ramp. (Everything should be " \
              "initialised first.)"

        # End of help_ramp.

    ##########

    # TODO TODO TODO
    # Method became `obsolete.'
##    def do_continue(self, dummy):
##        """Continue an aborted/paused ramp.

##        Hitting CTRL-C during a ramp will halt the ramp and return to
##        the interpreter promtp. Use continue to continue where the
##        ramp was left.

##        """

##        # Check that we're in a state from where we can continue the
##        # ramp.
##        if self._clock_ramper._ramp_status != "ramping" or \
##           self._clock_ramper._ramp_position is None:
##            print "No ramp to continue(?)"
##        else:
##            self._clock_ramper._perform_ramp()

##        # End of do_continue.
    # TODO TODO TODO

    def do_continue(self, dummy):
        """Continue an aborted/paused ramp.

        Hitting CTRL-C during a ramp will halt the ramp and return to
        the interpreter promtp. Use continue to continue where the
        ramp was left.

        """

        self.do_ramp(0)

        # End of do_continue.

    def help_continue(self):

        print "Continue an aborted/paused ramp. " \
              "(I.e. CTRL-C was hit during ramping.)"

        # End of help_continue.

    ##########

    # End of class CMSClockRamperCmd.

######################################################################
## CMSClockRamper class.
######################################################################

class CMSClockRamper(object):
    """Class to perform CMS TTC clock ramps.

    More documentation obviously to follow. Please check the inline
    comments and the above mentioned Twiki page in the meantime.

    """

    ##########

    def __init__(self, cmd_line_opts=sys.argv[1:]):
        "Initialise class and process command line options."

        self._version = __version__

        # When offline (i.e. the function generator cannot be reached)
        # or just to be safe one can run in dry-run mode. In that case
        # the following flag is set.
        self._dry_run = False

        # We could read commands from an input commands file. If we're
        # doing so, the name of the file is stored in this variable.
        self._commands_file_name = None

        self._func_gen = None
        self._ramp = None
        self._ramp_status = None
        # The following variable is used to keep track of our position
        # in the ramp in case it is aborted and needs to be restarted.
        self._ramp_position = None

        # This is where we (think we) can find the function generator
        # at P5.
        self._func_gen_address = "tcds-freq-gen.cms"
        self._func_gen_port = 5025

        # This is the offset of the Keysight/Agilent generator: zero
        # since it's hooked up to the GPS-based reference clock.
        self._func_gen_offset = 0.

        # Store command line options for later use.
        if cmd_line_opts is None:
            cmd_line_opts = sys.argv[1:]
        self.cmd_line_opts = cmd_line_opts

        # Set up the logger.
        # Note: since we're using a command line interpreter we can't
        # log to the console -> log to file and (at startup) tell the
        # user where the logging info is going.
        script_name = os.path.normpath(sys.argv[0])
        log_file_name = script_name
        if log_file_name.endswith(".py"):
            log_file_name = log_file_name.replace(".py", ".log")
        else:
            log_file_name += ".log"
        self._log_file_name = log_file_name
        log_handler = logging.FileHandler(self._log_file_name)
        # NOTE: This is an empty formatter, the real one is set by the
        # call to _set_output_level below.
        log_formatter = logging.Formatter("")
        log_handler.setFormatter(log_formatter)
        logger = logging.getLogger()
        logger.name = "main"
        logger.addHandler(log_handler)
        self._logger = logger
        self._set_output_level("NORMAL")

        self.parse_cmd_line_options(cmd_line_opts)

        # Instantiate the command line interpreter.
        if self._commands_file_name is None:
            self._interpreter = CMSClockRamperCmd(self)
        else:
            # Copy everything that does not start with a `#' (i.e.e
            # commented lines) from the file into a temporary
            # file-like object.
            try:
                in_file = file(self._commands_file_name, "r")
                lines_from_file = in_file.readlines()
                lines_from_file = [line.strip() for line in lines_from_file]
                lines_from_file = [line for line in lines_from_file \
                                   if not line.startswith("#")]
                lines_from_file = [line for line in lines_from_file \
                                   if len(line) > 0]
                # NOTE: We have to append an empty line, since the
                # interpreter cuts off the last character and
                # otherwise we end up with an invalid command on the
                # last line...
                lines_from_file.append("")
                commands_from_file = StringIO.StringIO("\n".join(lines_from_file))
                in_file.close()
            except IOError:
                self._logger.fatal("Could not read from file `%s'" % \
                                   self._commands_file_name)
                raise
            # Now use the cleaned up list of commands as input.
            self._interpreter = CMSClockRamperCmd(self,
                                                  stdin=commands_from_file)
            self._interpreter.use_rawinput = False
            # Switch off command repetition for empty lines. Otherwise
            # an inadvertent occurrence of an empty line in the file
            # will have not-so-pleasant side-effects.
            def new_emptyline():
                pass
            self._interpreter.emptyline = new_emptyline

        sep_line = "=" * 60
        msg_lines = []
        msg_lines.append(sep_line)
        msg_lines.append("  Initialised successfully")
        msg_lines.append("  Logging to file `%s'" % \
                         self._log_file_name)
        msg_lines.append("  Using function generator at %s:%d" % \
                         (self._func_gen_address, self._func_gen_port))
        if self._dry_run:
            msg_lines.append("  !!! Running in dry-run mode !!!")
        msg_lines.append(sep_line)

        msg = "\n".join(msg_lines)
        print msg
        self._logger.info("-" * 60)
        self._logger.info("%s starting" % sys.argv[0])
        for msg_line in msg_lines:
            self._logger.info(msg_line)

        # End of __init__.

    ##########

    def cleanup(self):
        """Clean up after ourselves.

        This is a safe(r) replacement of __del__.

        """

        # Reset the display of the generator.
        if not self._func_gen is None:
            try:
                self._func_gen.set_display(None)
            except socket.error:
                # We don't really care at this point...
                pass

        # Say goodbye to the user.
        sep_line = "=" * 60
        msg_lines = []
        msg_lines.append(sep_line)
        msg_lines.append("  All done. Shutting down.")
        msg_lines.append(sep_line)

        msg = "\n".join(msg_lines)
        print msg
        for msg_line in msg_lines:
            self._logger.info(msg_line)
        self._logger.info("%s stopping" % sys.argv[0])
        self._logger.info("-" * 60)
        logging.shutdown()

        # End of cleanup.

    ##########

    def _set_output_level(self, output_level="NORMAL"):
        """Adjust the level of output generated.

        Choices are:
          - normal  : default level of output
          - quiet   : less output than the default
          - verbose : some additional information
          - debug   : lots more information, may be overwhelming

        NOTE: The debug option is a bit special in the sense that it
              also modifies the output format.

        """

        # NOTE: These levels are hooked up to the ones used in the
        #       logging module.
        output_levels = {
            "NORMAL"  : logging.INFO,
            "QUIET"   : logging.WARNING,
            "DEBUG"   : logging.DEBUG
            }

        output_level = output_level.upper()

        try:
            # Update the logger.
            self._log_level = output_levels[output_level]
            self._logger.setLevel(self._log_level)
            if output_level == "DEBUG":
                log_formatter = logging.Formatter("%(asctime)s - " \
                                                  "@%(filename)s:%(lineno)d " \
                                                  "%(message)s")
            else:
                log_formatter = logging.Formatter("%(asctime)s - " \
                                                  "[%(levelname)8s] " \
                                                  "%(message)s")
            self._logger.handlers[0].setFormatter(log_formatter)
        except KeyError:
            # Show a complaint
            self._logger.fatal("Unknown output level `%s'" % ouput_level)
            # and re-raise an exception.
            raise Exception

        # End of _set_output_level.

    ##########

    def option_handler_debug(self, option, opt_str, value, parser):
        """Switch to debug mode.

        This both increases the amount of output generated, as well as
        changes the format used (more detailed information is given).

        """

        self._set_output_level("DEBUG")

        # End of option_handler_debug.

    ##########

    def option_handler_quiet(self, option, opt_str, value, parser):
        """Switch to quiet mode.

        By default the CMSClockRamper is pretty verbose, this flag
        suppresses most of the output.

        """

        self._set_output_level("QUIET")

        # End of option_handler_quiet.

    ##########

    def option_handler_dry_run(self, option, opt_str, value, parser):
        """Run in dry-run mode.

        In dry-run mode no attempts are made to actually connect to
        the function generator. This can be useful to setup a test or
        to debug code offline (i.e. when the function generator cannot
        be reached).

        """

        self._dry_run = True

        # End of option_handler_dry_run.

    ##########

    def option_handler_commands_file_name(self, option, opt_str,
                                          value, parser):
        """Specifies the name of the input file to read commands from.

        This also checks the existence of the file before going
        further.

        """

        file_name = value

        # Let's see if we can open this file.
        try:
            tmp = file(file_name, "r")
            tmp.close()
        except IOError:
            msg = "Could not read file `%s'" % file_name
            self._logger.fatal(msg)
            raise SystemExit, msg

        self._commands_file_name = file_name

        # End of option_handler_commands_file_name.

    ##########

    def option_handler_func_gen_address(self, option, opt_str,
                                        value, parser):
        """Set the network address of the function generator.

        This parses the command line option setting the network
        address where to find the function generator at P5.

        Format: `address:port'.

        """

        if len(value) < 1:
            msg = "Please specify a network address " \
                  "for the function generator."
            self._logger.fatal(msg)
            raise optparse.OptionValueError, msg

        # Clean up and split the input.
        value = value.strip()
        value_split = value.split(":")

        if len(value_split) > 2:
            msg = "Sorry, but `%s' does not look like a " \
                  "network address." % value
            self._logger.fatal(msg)
            raise optparse.OptionValueError, msg

        self._func_gen_address = value_split[0]
        if len(value_split) > 1:
            self._func_gen_port = int(value_split[1])

        # End of option_handler_func_gen_address.

    ##########

    def option_handler_func_gen_offset(self, option, opt_str,
                                       value, parse):
        """Set the frequency offset used by the function generator.

        For details please see the FuncGen class.

        """

        pass

        # End of option_handler_func_gen_offset.

    ##########

    def parse_cmd_line_options(self, cmd_line_options):

        # Set up the command line options.
        parser = optparse.OptionParser(version="%s %s" % \
                                       ("%prog", self._version))
        self._option_parser = parser

        # The debug switch.
        parser.add_option("-d", "--debug",
                          help="Switch on debug mode",
                          action="callback",
                          callback=self.option_handler_debug)

        # The quiet switch.
        parser.add_option("-q", "--quiet",
                          help="Switch on quiet mode",
                          action="callback",
                          callback=self.option_handler_quiet)

        # The flag to make sure we're running in dry-run mode.
        parser.add_option("", "--dry-run",
                          help="Perform a dry-run (i.e. without " \
                          "trying to contact the function generator",
                          action="callback",
                          callback=self.option_handler_dry_run)

        # Use this flag to tell us to read commands from the given
        # file and then quite.
        parser.add_option("", "--commands-file",
                          help="Process commands from this " \
                          "file and quit",
                          action="callback",
                          callback=self.option_handler_commands_file_name,
                          type="string",
                          metavar="FILE")

        # The option to set the network address of the function
        # generator.
        parser.add_option("-g", "--generator",
                          help="Network address of " \
                          "the function generator",
                          action="callback",
                          callback=self.option_handler_func_gen_address,
                          type="string",
                          metavar="ADDRESS[:PORT]")

        # Some trickery with the options. Why? Well, since these
        # options change the output level immediately from the option
        # handlers, the results differ depending on where they are on
        # the command line. Let's just make sure they are at the
        # front.
        # NOTE: Not very efficient or sophisticated, but it works and
        # it only has to be done once anyway.
        for i in ["-d", "--debug",
                  "-q", "--quiet"]:
            if i in self.cmd_line_opts:
                self.cmd_line_opts.remove(i)
                self.cmd_line_opts.insert(0, i)

        # Everything is set up, now parse what we were given.
        (self._options, self._args) = parser.parse_args()

        # End of parse_cmd_line_options.

    ##########

    def setup_func_gen(self):

        if self._func_gen is None:
            address = self._func_gen_address
            port = self._func_gen_port
            if self._dry_run:
                func_gen = cms_ttc_func_gen_utils. \
                           FuncGenDummy(address, port)
            else:
                func_gen = cms_ttc_func_gen_utils. \
                           FuncGenAgilent33250A(address, port)

            func_gen.set_offset(self._func_gen_offset)

            gen_id = func_gen.identify_yourself()
            offset = func_gen.get_offset()

            msg_lines = []
            msg_lines.append("Generator setup successful")
            msg_lines.append("  id: `%s'" % gen_id.strip())
            msg_lines.append("  offset set to %.1f Hz" % offset)
            msg = "\n".join(msg_lines)
            print msg
            for msg_line in msg_lines:
                self._logger.info(msg_line)

            self._func_gen = func_gen

        # End of setup_func_gen.

    ##########

    def set_func_gen_for_ttc(self):
        """Perform basic generator settings for use with TTC.

        """

        # DEBUG DEBUG DEBUG
        # This should already have been setup.
        assert not self._func_gen is None, \
               "ERROR Please call setup_func_gen() " \
               "before set_func_gen_for_ttc()"
        # DEBUG DEBUG DEBUG end

        func_gen = self._func_gen

        # Setup the function generator to provide us with a square
        # wave around 0 V with an amplitude of 500 mV into 50 Ohm.
        cmd_lines = []
        cmd_lines.append("VOLTAGE 1.0 Vpp")
        cmd_lines.append("VOLTAGE:OFFSET 0.0")
        cmd_lines.append("FUNCTION SQUARE")
        cmd_lines.append("FUNCTION:SQUARE:DCYCLE 50")
        cmd_lines.append("OUTPUT:LOAD 50")
        func_gen.send_command("; ".join(cmd_lines))
        # NOTE: Always start out with a `TTC-safe' frequency: the
        # nominal proton frequency.
        func_gen.set_frequency(cms_ttc_ramp_defs.freq_lhc_proton_hi)

        print "Function generator setup with TTC square wave settings"
        self._func_gen.set_display("Generator is setup for " \
                                   "CMS TTC clock ramp")

        # End of set_func_gen_for_ttc.

    ##########

    def init_gen(self):

        # Setup the function generator: make sure we know where it is
        # etc.
        self.setup_func_gen()

        # Initialise the generator using the setting for the current
        # ramp.
        self.set_func_gen_for_ttc()

        # End of init_gen.

    ##########

    def init_ramp(self, arg=None):

        # When we arrive here, a ramp should have been loaded and a
        # generator setup.
        # DEBUG DEBUG DEBUG
        assert not self._func_gen is None, \
               "ERROR A generator should have been setup !!!"
        assert not self._ramp is None, \
               "ERROR A ramp should have been loaded !!!"
        assert (arg is None) or (arg in ["", "middle"])
        # DEBUG DEBUG DEBUG end

        if arg is None or arg == "":
            # Switch the generator to the initial frequency for the
            # ramp.

            # NOTE: The initial frequency may not be the first value
            # in the array, e.g. if we jumped to a position somewhere.
            if self._ramp_position is None:
                initial_freq = self._ramp._ramp_info[0][1]
            else:
                initial_freq = self._ramp._ramp_info[self._ramp_position][1]
            arg_str = "initial"
        elif arg == "middle":
            # Switch the generator to the ramp's middle frequency.
            initial_freq = .5 * (self._ramp._ramp_info[0][1] + \
                                 self._ramp._ramp_info[-1][1])
            arg_str = "ramp middle"
        else:
            assert False, "We should never get here!"

        self._func_gen.set_frequency(initial_freq)

        print "Frequency set to %s value: %.2f Hz" % \
              (arg_str, initial_freq)

        # End of init_ramp.

    ##########

    def _perform_ramp(self):
        """This contains the main mechanics for a clock ramp.

        This method is called from _do_ramp in the command
        interpreter.

        """

        if self._ramp_status == "initialised":
            if self._ramp_position is None:
                self._ramp_position = 0

        # Update the status flag.
        self._ramp_status = "ramping"
        self._func_gen.set_display("CMS TTC clock ramp in progress...")

        # Loop over all frequency points. Remember that the first
        # one should already be set up by the time we reach this
        # point.
        msg_lines = []
        current_freq_from_gen = self. \
                                _func_gen.get_frequency()
        current_freq_from_ramp = self. \
                                _ramp._ramp_info \
                                [self._ramp_position][1]

        msg_lines.append("Current frequency is %.2f Hz" % \
                         current_freq_from_gen)
        if abs(current_freq_from_gen - \
               current_freq_from_ramp) > 1.:
            msg_lines.append("!!! Something went wrong !!!")
            msg_lines.append("!!! According to the ramp " \
                             "the frequency should be %.2f Hz" % \
                             current_freq_from_ramp)
        msg = "\n".join(msg_lines)
        print msg
        for msg_line in msg_lines:
            self._logger.info(msg_line)

        time_start = time.time()
        current_time = self._ramp._ramp_info[self._ramp_position][0]
        last_time = current_time
        for (next_time, next_freq) in \
                self._ramp._ramp_info[self._ramp_position + 1:]:

            msg = "Next frequency is %.2f Hz in %.1f seconds" % \
                  (next_freq, next_time - last_time)
            print msg
            self._logger.info(msg)

            # NOTE: Of course the precision of this is a bit
            # limited, but it should be close enough for our
            # purposes.
            time_now = time.time()
            while (time_now < (time_start + (next_time - current_time))):
                time.sleep(.1)
                time_now = time.time()
            # Now set the new frequency.
            msg = "  setting frequency to %.2f Hz" % next_freq
            print msg
            self._logger.info(msg)
            self._func_gen.set_frequency(next_freq)
            self._ramp_position += 1

            # current_time = next_time
            last_time = next_time

        # Update the status flag.
        self._ramp_status = "ramp finished"
        # And reset the ramp position.
        self._ramp_position = None

        # End of ramp.
        self._func_gen.set_display("CMS TTC clock ramp finished")
        msg_lines = []
        msg_lines.append("Ramp done")
        current_freq_from_gen = self. \
                                _func_gen.get_frequency()
        current_freq_from_ramp = self. \
                                _ramp._ramp_info[-1][1]
        msg_lines.append("Frequency now set to %.2f Hz" % \
                         self. \
                         _func_gen.get_frequency())
        if abs(current_freq_from_gen - \
               current_freq_from_ramp) < 1.:
            msg_lines.append("(Which agrees with the ramp " \
                             "end frequency)")
        else:
            msg_lines.append("!!! Something went wrong " \
                             "with the ramp !!!")
            msg_lines.append("!!! According to the ramp " \
                             "the frequency should be %.2f Hz" % \
                             current_freq_from_ramp)
        msg = "\n".join(msg_lines)
        print msg
        for msg_line in msg_lines:
            self._logger.info(msg_line)

    # End of _perform_ramp.

    ##########

    def _perform_step(self):

        # NOTE: This is not very nice code. Basically copy-paste from
        # _perform_ramp(). May be factorized and prettified if you
        # have time ;-).

        if self._ramp_status == "initialised":
            if self._ramp_position is None:
                self._ramp_position = 0

        # Update the status flag.
        self._ramp_status = "ramping"
        self._func_gen.set_display("CMS TTC clock ramp in progress...")

        msg_lines = []
        current_freq_from_gen = self. \
                                _func_gen.get_frequency()
        current_freq_from_ramp = self. \
                                _ramp._ramp_info \
                                [self._ramp_position][1]

        msg_lines.append("Current frequency is %.2f Hz" % \
                         current_freq_from_gen)
        if abs(current_freq_from_gen - \
               current_freq_from_ramp) > 1.:
            msg_lines.append("!!! Something went wrong !!!")
            msg_lines.append("!!! According to the ramp " \
                             "the frequency should be %.2f Hz" % \
                             current_freq_from_ramp)
        msg = "\n".join(msg_lines)
        print msg
        for msg_line in msg_lines:
            self._logger.info(msg_line)

        time_start = time.time()
        current_time = self._ramp._ramp_info[self._ramp_position][0]
        for (next_time, next_freq) in \
                self._ramp._ramp_info[self._ramp_position + 1:
                                      self._ramp_position + 2]:

            msg = "Next frequency is %.2f Hz in %.1f seconds" % \
                  (next_freq, next_time - current_time)
            print msg
            self._logger.info(msg)

            # NOTE: Of course the precision of this is a bit
            # limited, but it should be close enough for our
            # purposes.
            while (time.time() < (time_start + next_time)):
                time.sleep(.1)
            # Now set the new frequency.
            msg = "  setting frequency to %.2f Hz" % next_freq
            print msg
            self._logger.info(msg)
            self._func_gen.set_frequency(next_freq)
            self._ramp_position += 1

            current_time = next_time

        # If this was the last step of the ramp, do book keeping.
        if self._ramp_position == len(self._ramp._ramp_info) - 1:
            # Update the status flag.
            self._ramp_status = "ramp finished"
            # And reset the ramp position.
            self._ramp_position = None

            # End of ramp.
            self._func_gen.set_display("CMS TTC clock ramp finished")
            msg_lines = []
            msg_lines.append("Ramp done")
            current_freq_from_gen = self. \
                                    _func_gen.get_frequency()
            current_freq_from_ramp = self. \
                                     _ramp._ramp_info[-1][1]
            msg_lines.append("Frequency now set to %.2f Hz" % \
                             self. \
                             _func_gen.get_frequency())
            if abs(current_freq_from_gen - \
                   current_freq_from_ramp) < 1.:
                msg_lines.append("(Which agrees with the ramp " \
                                 "end frequency)")
            else:
                msg_lines.append("!!! Something went wrong " \
                                 "with the ramp !!!")
                msg_lines.append("!!! According to the ramp " \
                                 "the frequency should be %.2f Hz" % \
                                 current_freq_from_ramp)

        msg = "\n".join(msg_lines)
        print msg
        for msg_line in msg_lines:
            self._logger.info(msg_line)

        # End of _perform_step().

    ##########

    def _log_exception(self, err):

        if not isinstance(err, KeyboardInterrupt):
            self._logger.fatal("!" * 60)
            self._logger.fatal(str(err))
            import traceback
            # traceback_string = traceback.format_exc()
            (etype, value, tb) = sys.exc_info()
            traceback_string = ''.join(traceback. \
                                       format_exception(etype, value,
                                                        tb, None))
            for line in traceback_string.split("\n"):
                self._logger.fatal(line)
            self._logger.fatal("!" * 60)

        # End of log_exception.

    ##########

    def run(self):
        "Main entry point of the CMS clock scanner."

        exit_code = 0

        try:
            first_round = True
            intro = None
            while True:
                try:

                    if first_round:
                        first_round = False
                    self._interpreter.cmdloop(intro)
                    break

                except (socket.error, socket.herror, \
                        socket.gaierror, socket.timeout), err:
                    msg = "A connection problem occurred!"
                    if isinstance(err, socket.gaierror):
                        # This one is especially common: `nodename nor
                        # servname provided, or not known.' This
                        # probably means you're not on the .CMS
                        # network.
                        if err[0] == 8:
                            msg = "Could not find the function " \
                                  "generator. This probably means " \
                                  "you're not on the .CMS network.\n" \
                                  "(But you can check in the log file: " \
                                  "`%s'.)" % self._log_file_name
                    elif isinstance(err, socket.herror):
                        msg = "Something is wrong with the address " \
                              "of the function generator (?)."
                    elif isinstance(err, socket.timeout):
                        msg = "Connection timed out trying to " \
                              "reach the function generator.\n" \
                              "Sometimes the generator needs to " \
                              "wake up...\nPlease try again if you " \
                              "have not connected in a while."
                    elif isinstance(err, socket.error):
                        # Hmmm, network address found but connection
                        # refused. Most likely cause is that someone
                        # else is already connected. Actually this
                        # also happens if you try to (re)connect
                        # really fast multiple times in a row.
                        if err[0] == 111:
                            msg = "Connection to the function generator " \
                                  "was refused. Could someone else " \
                                  "be connected?."
                    print msg
                    self._logger.error(msg)
                    self._logger.error("See below for the " \
                                       "original stack trace.")
                    self._log_exception(err)
                    intro = ""
                    self._interpreter.lastcmd = ""
                except KeyboardInterrupt:
                    # Not sure if we want to really catch this one. At
                    # least it will abort the scan in progress if
                    # CTRL-C is hit.
                    self._logger.info("!!! Keyboard interrupt " \
                                      "aborted operation !!!")
                    intro = "Aborting... Please use `quit' to exit " \
                            "or `continue' to continue."
                    self._interpreter.lastcmd = ""
                except Exception, err:
                    self._log_exception(err)
                    intro = "An exception occurred. Please have " \
                            "a look at the log file: `%s'" % \
                            self._log_file_name
                    self._interpreter.lastcmd = ""
                    # if isinstance(err, SystemExit):
                    #     self._logger.fatal(err.code)
                    # elif not isinstance(err, KeyboardInterrupt):
                    # if not isinstance(err, KeyboardInterrupt):
                    #     self._logger.fatal("!" * 60)
                    #     self._logger.fatal(str(err))
                    #     import traceback
                    #     traceback_string = traceback.format_exc()
                    #     for line in traceback_string.split("\n"):
                    #         self._logger.fatal(line)
                    #     self._logger.fatal("!" * 60)
                        #exit_code = 1

        finally:
            self.cleanup()

        # End of run.
        return exit_code

    # End of CMSClockScanner.

######################################################################
## Main entry point.
######################################################################

if __name__ == "__main__":
    "Main entry point for clock ramps."

    CMSClockRamper().run()

    # Done.

######################################################################
