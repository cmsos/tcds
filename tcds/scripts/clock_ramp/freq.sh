#!/bin/bash

ADDRESS="TCDS-FREQ-GEN.cms"
PORT="5025"

if [ $# -lt 1 ]; then
    echo "Current frequency: `(echo FREQUENCY?; sleep 1) | nc ${ADDRESS} ${PORT}` Hz"
else
    echo "Setting frequency to ${1} Hz"
    (echo FREQUENCY ${1}; sleep 1) | nc ${ADDRESS} ${PORT}
fi
