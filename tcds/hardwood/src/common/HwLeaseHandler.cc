#include "tcds/hardwood/HwLeaseHandler.h"

#include "toolbox/TimeVal.h"
#include "toolbox/net/UUID.h"
#include "toolbox/string.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerFactory.h"
#include "xcept/Exception.h"
#include "xdaq/Application.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationDescriptor.h"
#include "xoap/MessageReference.h"

#include "tcds/hardwood/exception/Exception.h"
#include "tcds/hardwood/SOAPUtils.h"

tcds::hardwood::HwLeaseHandler::HwLeaseHandler(xdaq::Application* const xdaqApp,
                                               xdaq::ApplicationDescriptor const* const target,
                                               std::string const& sessionId,
                                               toolbox::TimeInterval const& interval,
                                               toolbox::exception::Listener* listener) :
  xdaqAppP_(xdaqApp),
  targetP_(target),
  sessionId_(sessionId),
  interval_(interval),
  timerName_(toolbox::toString("HwLeaseRenewalTimer_uuid%s",
                               xdaqAppP_->getApplicationDescriptor()->getUUID().toString().c_str())),
  timerP_(toolbox::task::getTimerFactory()->createTimer(timerName_))
{
  if (listener != 0)
    {
      timerP_->addExceptionListener(listener);
    }
  toolbox::TimeVal start = toolbox::TimeVal::gettimeofday();
  timerP_->scheduleAtFixedRate(start, this, interval_, 0, "");
}

tcds::hardwood::HwLeaseHandler::~HwLeaseHandler()
{
  toolbox::task::getTimerFactory()->removeTimer(timerName_);
}

void
tcds::hardwood::HwLeaseHandler::timeExpired(toolbox::task::TimerEvent& event)
{
  renewHardwareLease();
}

void
tcds::hardwood::HwLeaseHandler::renewHardwareLease()
{
  xoap::MessageReference cmd =
    tcds::hardwood::soap::createSimpleSOAPCommand("RenewHardwareLease", sessionId_);
  executeSOAPCommand(cmd);
}

xoap::MessageReference
tcds::hardwood::HwLeaseHandler::executeSOAPCommand(xoap::MessageReference& cmd)
{
  try
    {
      xoap::MessageReference reply = sendSOAP(cmd);
      if (tcds::hardwood::soap::hasFault(reply))
        {
          std::string msg("");
          std::string const faultString =
            tcds::hardwood::soap::extractFaultString(reply);
          if (tcds::hardwood::soap::hasFaultDetail(reply))
            {
              std::string const faultDetail =
                tcds::hardwood::soap::extractFaultDetail(reply);
              msg =
                toolbox::toString("Received a SOAP fault as reply: '%s: %s'.",
                                  faultString.c_str(),
                                  faultDetail.c_str());
            }
          else
            {
              msg =
                toolbox::toString("Received a SOAP fault as reply: '%s'.",
                                  faultString.c_str());
            }
          XCEPT_RAISE(tcds::hardwood::exception::RuntimeError, msg);
        }
      else
        {
          return reply;
        }
    }
  catch (xcept::Exception& err)
    {
      std::string const msg = toolbox::toString("Problem executing SOAP command: '%s'.",
                                                err.what());
      XCEPT_RETHROW(tcds::hardwood::exception::RuntimeError,
                    msg,
                    err);
    }
}

xoap::MessageReference
tcds::hardwood::HwLeaseHandler::sendSOAP(xoap::MessageReference& msg)
{
  xoap::MessageReference const reply =
    xdaqAppP_->getApplicationContext()->postSOAP(msg,
                                                 *xdaqAppP_->getApplicationDescriptor(),
                                                 *targetP_);
  return reply;
}
