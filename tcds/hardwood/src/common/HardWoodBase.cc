#include "tcds/hardwood/HardWoodBase.h"

#include <cassert>
#include <list>

#include "log4cplus/loggingmacros.h"
#include "toolbox/TimeInterval.h"
#include "toolbox/string.h"
#include "xcept/Exception.h"
#include "xdaq/Application.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/Zone.h"
#include "xdata/InfoSpace.h"
#include "xoap/SOAPElement.h"
#include "xoap/filter/MessageFilter.h"

#include "tcds/hardwood/exception/Exception.h"
#include "tcds/hardwood/HwLeaseHandler.h"
#include "tcds/hardwood/SOAPUtils.h"

tcds::hardwood::HardWoodBase::HardWoodBase(xdaq::Application* const xdaqApp) :
  logger_(xdaqApp->getApplicationLogger()),
  xdaqAppP_(xdaqApp),
  sessionId_("dummy_session"),
  tcdsAppClassName_(""),
  tcdsAppInstance_(0),
  hwLeaseRenewalInterval_("PT10S")
{
  // Registration of the InfoSpace variables.
  xdaqAppP_->getApplicationInfoSpace()->fireItemAvailable("tcdsAppClassName", &tcdsAppClassName_);
  xdaqAppP_->getApplicationInfoSpace()->fireItemAvailable("tcdsAppInstance", &tcdsAppInstance_);
  xdaqAppP_->getApplicationInfoSpace()->fireItemAvailable("sessionId", &sessionId_);
  xdaqAppP_->getApplicationInfoSpace()->fireItemAvailable("hardwareLeaseRenewalInterval",
                                                          &hwLeaseRenewalInterval_);
}

tcds::hardwood::HardWoodBase::~HardWoodBase()
{
}

std::string
tcds::hardwood::HardWoodBase::tcdsAppClassName()
{
  return tcdsAppClassName_.toString();
}

unsigned int
tcds::hardwood::HardWoodBase::tcdsAppInstance()
{
  return (unsigned int)(tcdsAppInstance_);
}

std::string
tcds::hardwood::HardWoodBase::sessionId()
{
  return sessionId_.toString();
}

std::string
tcds::hardwood::HardWoodBase::hwLeaseRenewalInterval()
{
  return hwLeaseRenewalInterval_.toString();
}

std::string
tcds::hardwood::HardWoodBase::tcdsQueryFSMState()
{
  xoap::MessageReference cmd =
    tcds::hardwood::soap::createParameterGet(tcdsAppClassName(),
                                             "stateName", "xsd:string");
  xoap::MessageReference reply = executeSOAPCommand(cmd);
  // NOTE: This is a bit rough-and-ready. It is assumed that this will
  // work, if so far nothing has thrown any exceptions.
  xoap::filter::MessageFilter filter("//p:stateName");
  // ASSERT ASSERT ASSERT
  assert (filter.match(reply));
  // ASSERT ASSERT ASSERT end
  std::list<xoap::SOAPElement> elements = filter.extract(reply);
  // ASSERT ASSERT ASSERT
  assert (elements.size() == 1);
  // ASSERT ASSERT ASSERT end
  std::string const stateName = elements.front().getTextContent();
  return stateName;
}

std::string
tcds::hardwood::HardWoodBase::tcdsQueryHwLeaseOwnerId()
{
  xoap::MessageReference cmd =
    tcds::hardwood::soap::createParameterGet(tcdsAppClassName(),
                                             "hwLeaseOwnerId", "xsd:string");
  xoap::MessageReference reply = executeSOAPCommand(cmd);
  // NOTE: This is a bit rough-and-ready. It is assumed that this will
  // work, if so far nothing has thrown any exceptions.
  xoap::filter::MessageFilter filter("//p:hwLeaseOwnerId");
  // ASSERT ASSERT ASSERT
  assert (filter.match(reply));
  // ASSERT ASSERT ASSERT end
  std::list<xoap::SOAPElement> elements = filter.extract(reply);
  // ASSERT ASSERT ASSERT
  assert (elements.size() == 1);
  // ASSERT ASSERT ASSERT end
  std::string const hwLeaseOwnerId = elements.front().getTextContent();
  return hwLeaseOwnerId;
}

void
tcds::hardwood::HardWoodBase::tcdsConfigure(std::string const& hwCfgString)
{
  xoap::MessageReference cmd =
    tcds::hardwood::soap::createConfigureSOAPCommand(sessionId(),
                                                     hwCfgString);
  executeSOAPCommand(cmd);

  toolbox::TimeInterval interval;
  interval.fromString(hwLeaseRenewalInterval_);
  hwLeaseHandlerP_ =
    std::unique_ptr<tcds::hardwood::HwLeaseHandler>(new HwLeaseHandler(xdaqAppP_,
                                                                       getDestinationDescriptor(),
                                                                       sessionId(),
                                                                       interval,
                                                                       this));
}

void
tcds::hardwood::HardWoodBase::tcdsEnable(unsigned int const runNumber)
{
  xoap::MessageReference cmd =
    tcds::hardwood::soap::createEnableSOAPCommand(sessionId(),
                                                  runNumber);
  executeSOAPCommand(cmd);
}

void
tcds::hardwood::HardWoodBase::tcdsPause()
{
  xoap::MessageReference cmd =
    tcds::hardwood::soap::createSimpleSOAPCommand("Pause", sessionId());
  executeSOAPCommand(cmd);
}

void
tcds::hardwood::HardWoodBase::tcdsResume()
{
  xoap::MessageReference cmd =
    tcds::hardwood::soap::createSimpleSOAPCommand("Resume", sessionId());
  executeSOAPCommand(cmd);
}

void
tcds::hardwood::HardWoodBase::tcdsStop()
{
  xoap::MessageReference cmd =
    tcds::hardwood::soap::createSimpleSOAPCommand("Stop", sessionId());
  executeSOAPCommand(cmd);
}

void
tcds::hardwood::HardWoodBase::tcdsHalt()
{
  xoap::MessageReference cmd =
    tcds::hardwood::soap::createSimpleSOAPCommand("Halt", sessionId());
  executeSOAPCommand(cmd);
  hwLeaseHandlerP_.reset();
}

void
tcds::hardwood::HardWoodBase::tcdsTTCResync()
{
  xoap::MessageReference cmd =
    tcds::hardwood::soap::createSimpleSOAPCommand("TTCResync", sessionId());
  executeSOAPCommand(cmd);
}

void
tcds::hardwood::HardWoodBase::tcdsTTCHardReset()
{
  xoap::MessageReference cmd =
    tcds::hardwood::soap::createSimpleSOAPCommand("TTCHardReset", sessionId());
  executeSOAPCommand(cmd);
}

void
tcds::hardwood::HardWoodBase::tcdsRenewHardwareLease()
{
  xoap::MessageReference cmd =
    tcds::hardwood::soap::createSimpleSOAPCommand("RenewHardwareLease", sessionId());
  executeSOAPCommand(cmd);
}

xoap::MessageReference
tcds::hardwood::HardWoodBase::executeSOAPCommand(xoap::MessageReference& cmd)
{
  try
    {
      xoap::MessageReference reply = sendSOAP(cmd);
      if (tcds::hardwood::soap::hasFault(reply))
        {
          std::string msg("");
          std::string const faultString =
            tcds::hardwood::soap::extractFaultString(reply);
          if (tcds::hardwood::soap::hasFaultDetail(reply))
            {
              std::string const faultDetail =
                tcds::hardwood::soap::extractFaultDetail(reply);
              msg =
                toolbox::toString("Received a SOAP fault as reply: '%s: %s'.",
                                  faultString.c_str(),
                                  faultDetail.c_str());
            }
          else
            {
              msg =
                toolbox::toString("Received a SOAP fault as reply: '%s'.",
                                  faultString.c_str());
            }
          XCEPT_RAISE(tcds::hardwood::exception::RuntimeError, msg);
        }
      else
        {
          return reply;
        }
    }
  catch (xcept::Exception& err)
    {
      std::string const msg = toolbox::toString("Problem executing SOAP command: '%s'.",
                                                err.what());
      XCEPT_RETHROW(tcds::hardwood::exception::RuntimeError,
                    msg,
                    err);
    }
}

xoap::MessageReference
tcds::hardwood::HardWoodBase::sendSOAP(xoap::MessageReference& msg)
{
  xdaq::ApplicationDescriptor const* destination = getDestinationDescriptor();
  xoap::MessageReference const reply = postSOAP(msg, *destination);
  return reply;
}

xoap::MessageReference
tcds::hardwood::HardWoodBase::postSOAP(xoap::MessageReference& msg,
                                       xdaq::ApplicationDescriptor const& destination)
{
  xoap::MessageReference const reply =
    xdaqAppP_->getApplicationContext()->postSOAP(msg,
                                                 *xdaqAppP_->getApplicationDescriptor(),
                                                 destination);
  return reply;
}

xdaq::ApplicationDescriptor const*
tcds::hardwood::HardWoodBase::getDestinationDescriptor() const
{
  if (tcdsAppClassName_ == "")
    {
      std::string const msg = "No value set for the tcdsAppClassName parameter.";
      LOG4CPLUS_ERROR(logger_, msg);
      XCEPT_RAISE(tcds::hardwood::exception::ValueError, msg);
    }

  xdaq::ApplicationDescriptor const* desc =
    xdaqAppP_->getApplicationContext()->getDefaultZone()->getApplicationDescriptor(tcdsAppClassName_,
                                                                                   tcdsAppInstance_);
  return desc;
}

void
tcds::hardwood::HardWoodBase::onException(xcept::Exception& err)
{
  std::string const msg =
    toolbox::toString("An error occurred in the hardware lease renewal thread : '%s'.",
                      err.what());
  LOG4CPLUS_ERROR(logger_, "HardWoodBase");
  LOG4CPLUS_ERROR(logger_, msg.c_str());
}
