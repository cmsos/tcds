#include "tcds/hardwood/HardWood.h"

#include <ostream>
#include <string>
#include <vector>

#include "log4cplus/loggingmacros.h"
#include "toolbox/net/URN.h"
#include "toolbox/string.h"
#include "xcept/Exception.h"
#include "xdaq/ApplicationDescriptor.h"
#include "xdaq/ContextDescriptor.h"
#include "xdaq/exception/ApplicationInstantiationFailed.h"
#include "xdata/InfoSpace.h"
#include "xdata/InfoSpaceFactory.h"
#include "xgi/Output.h"
#include "xgi/framework/Method.h"

XDAQ_INSTANTIATOR_IMPL(tcds::hardwood::HardWood)

tcds::hardwood::HardWood::HardWood(xdaq::ApplicationStub* stub)
try
  :
    xdaq::Application(stub),
    xgi::framework::UIManager(this),
    tcds::hardwood::HardWoodBase(this),
    hwCfgString_("# Dummy hardware configuration string"),
    runNumber_(42),
    state_("uninitialised"),
    hwLeaseOwnerId_("uninitialised"),
    statusMsg_("-"),
    logger_(getApplicationLogger())
  {
    // Set the application icon.
    std::string const iconFileName = "/tcds/hardwood/images/hardwood_icon.png";
    editApplicationDescriptor()->setAttribute("icon", iconFileName);

    toolbox::net::URN const urn = createQualifiedInfoSpace("hardwood");
    xdata::InfoSpace* const infoSpace = xdata::getInfoSpaceFactory()->get(urn.toString());

    // Registration of the InfoSpace variables.
    getApplicationInfoSpace()->fireItemAvailable("stateName",
                                                 &state_);
    getApplicationInfoSpace()->fireItemAvailable("problemDescription",
                                                 &statusMsg_);
    getApplicationInfoSpace()->fireItemAvailable("hardwareConfigurationString",
                                                 &hwCfgString_);
    getApplicationInfoSpace()->fireItemAvailable("hwLeaseOwnerId",
                                                 &hwLeaseOwnerId_);
    getApplicationInfoSpace()->fireItemAvailable("runNumber",
                                                 &runNumber_);
    infoSpace->fireItemAvailable("stateName",
                                 &state_);
    infoSpace->fireItemAvailable("status",
                                 &statusMsg_);
    infoSpace->fireItemAvailable("hardwareConfigurationString",
                                 &hwCfgString_);
    infoSpace->fireItemAvailable("hwLeaseOwnerId",
                                 &hwLeaseOwnerId_);
    infoSpace->fireItemAvailable("runNumber",
                                 &runNumber_);

    // Binding of the main HyperDAQ page.
    xgi::framework::deferredbind(this, this, &tcds::hardwood::HardWood::mainPage, "Default");

    // Binding of the remote queries.
    xgi::framework::deferredbind(this, this, &tcds::hardwood::HardWood::queryFSMState, "QueryFSMState");
    xgi::framework::deferredbind(this, this, &tcds::hardwood::HardWood::queryHwLeaseOwner, "QueryHardwareLeaseOwnerId");

    // Binding of the various web entry points to the state machine
    // transitions.
    xgi::framework::deferredbind(this, this, &tcds::hardwood::HardWood::configure, "Configure");
    xgi::framework::deferredbind(this, this, &tcds::hardwood::HardWood::enable, "Enable");
    xgi::framework::deferredbind(this, this, &tcds::hardwood::HardWood::pause, "Pause");
    xgi::framework::deferredbind(this, this, &tcds::hardwood::HardWood::resume, "Resume");
    xgi::framework::deferredbind(this, this, &tcds::hardwood::HardWood::stop, "Stop");
    xgi::framework::deferredbind(this, this, &tcds::hardwood::HardWood::halt, "Halt");
    xgi::framework::deferredbind(this, this, &tcds::hardwood::HardWood::ttcResync, "TTCResync");
    xgi::framework::deferredbind(this, this, &tcds::hardwood::HardWood::ttcHardReset, "TTCHardReset");
  }
catch (xcept::Exception const& err)
  {
    std::string const msgBase = "Something went wrong instantiating your HardWood";
    std::string const msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.what());
    XCEPT_RAISE(xdaq::exception::ApplicationInstantiationFailed, msg.c_str());
  }

tcds::hardwood::HardWood::~HardWood()
{
}

void
tcds::hardwood::HardWood::mainPage(xgi::Input* in, xgi::Output* out)
{
  // Local parameters (i.e., of this XDAQ application).
  *out << "<h1>Local information</h1>";
  if (statusMsg_ != "-")
    {
      *out << "<h2>" << statusMsg_.toString() << "</h2>";
    }
  *out << "<p>"
       << "<b>Connected to TCDS application:</b> "
       << "class '" << tcdsAppClassName() << "'"
       << ", instance number " << tcdsAppInstance()
       << "</p>";
  *out << "<p><b>Session id:</b> '" << sessionId() << "'</p>";
  *out << "<p><b>Hardware lease renewal interval:</b> "
       << hwLeaseRenewalInterval()
       << "</p>";
  *out << "<p><b>Run number:</b> '" << runNumber_.toString() << "'</p>";
  *out << "<p><b>Hardware configuration string:</b> '"
       << hwCfgString_.toString() << "'</p>";

  //----------

  // Remote parameters (i.e., of the remote TCDS control application).
  *out << "<h1>Remote information</h1>";
  *out << "<p><b>State:</b> " << state_.toString() << "</p>";
  std::string const hwLeaseOwnerId(hwLeaseOwnerId_.toString());
  std::string tmpStr("");
  if (hwLeaseOwnerId == "")
    {
      tmpStr = " (hardware not leased/lease expired)";
    }
  *out << "<p><b>Hardware lease owner id:</b> "
       << "'" << hwLeaseOwnerId << "'" << tmpStr
       << "</p>";

  //----------

  // A list of all available (remote) SOAP commands.
  *out << "<h1>SOAP commands</h1>";

  std::string const url(getApplicationDescriptor()->getContextDescriptor()->getURL());
  std::string const urn(getApplicationDescriptor()->getURN());

  std::vector<std::string> commands;
  commands.push_back("Configure");
  commands.push_back("Enable");
  commands.push_back("Pause");
  commands.push_back("Resume");
  commands.push_back("Stop");
  commands.push_back("Halt");
  commands.push_back("TTCResync");
  commands.push_back("TTCHardReset");

  *out << "<ul>";
  *out << "<li><a href=\""
       << url << "/" << urn << "/QueryFSMState"
       << "\">Query FSM state</a></li>";
  *out << "<li><a href=\""
       << url << "/" << urn << "/QueryHardwareLeaseOwnerId"
       << "\">Query hardware lease owner</a></li>";
  *out << "</ul>";

  *out << "<ul>";
  for (std::vector<std::string>::const_iterator cmd = commands.begin();
       cmd != commands.end();
       ++cmd)
    {
      *out << "<li><a href=\""
           << url << "/" << urn << "/" << *cmd
           << "\">" << *cmd << "</a></li>";
    }
  *out << "</ul>";
}

void
tcds::hardwood::HardWood::redirect(xgi::Input* in, xgi::Output* out)
{
  // Redirect back to the default page.
  *out << "<script language=\"javascript\">"
       << "window.location=\"Default\""
       << "</script>";
}

void
tcds::hardwood::HardWood::queryFSMState(xgi::Input* in, xgi::Output* out)
{
  statusMsg_ = "-";
  try
    {
      state_ = tcdsQueryFSMState();
    }
  catch (xcept::Exception& err)
    {
      std::string const msg = toolbox::toString("An error occured: '%s'.",
                                                err.what());
      LOG4CPLUS_ERROR(logger_, msg);
      statusMsg_ = msg;
    }
  redirect(in, out);
}

void
tcds::hardwood::HardWood::queryHwLeaseOwner(xgi::Input* in, xgi::Output* out)
{
  statusMsg_ = "-";
  try
    {
      hwLeaseOwnerId_ = tcdsQueryHwLeaseOwnerId();
    }
  catch (xcept::Exception& err)
    {
      std::string const msg = toolbox::toString("An error occured: '%s'.",
                                                err.what());
      LOG4CPLUS_ERROR(logger_, msg);
      statusMsg_ = msg;
    }
  redirect(in, out);
}

void
tcds::hardwood::HardWood::configure(xgi::Input* in, xgi::Output* out)
{
  statusMsg_ = "-";
  try
    {
      tcdsConfigure(hwCfgString_.toString());
    }
  catch (xcept::Exception& err)
    {
      std::string const msg = toolbox::toString("An error occured: '%s'.",
                                                err.what());
      LOG4CPLUS_ERROR(logger_, msg);
      statusMsg_ = msg;
    }
  redirect(in, out);
}

void
tcds::hardwood::HardWood::enable(xgi::Input* in, xgi::Output* out)
{
  statusMsg_ = "-";
  try
    {
      tcdsEnable((unsigned int)(runNumber_));
    }
  catch (xcept::Exception& err)
    {
      std::string const msg = toolbox::toString("An error occured: '%s'.",
                                                err.what());
      LOG4CPLUS_ERROR(logger_, msg);
      statusMsg_ = msg;
    }
  redirect(in, out);
}

void
tcds::hardwood::HardWood::pause(xgi::Input* in, xgi::Output* out)
{
  statusMsg_ = "-";
  try
    {
      tcdsPause();
    }
  catch (xcept::Exception& err)
    {
      std::string const msg = toolbox::toString("An error occured: '%s'.",
                                                err.what());
      LOG4CPLUS_ERROR(logger_, msg);
      statusMsg_ = msg;
    }
  redirect(in, out);
}

void
tcds::hardwood::HardWood::resume(xgi::Input* in, xgi::Output* out)
{
  statusMsg_ = "-";
  try
    {
      tcdsResume();
    }
  catch (xcept::Exception& err)
    {
      std::string const msg = toolbox::toString("An error occured: '%s'.",
                                                err.what());
      LOG4CPLUS_ERROR(logger_, msg);
      statusMsg_ = msg;
    }
  redirect(in, out);
}

void
tcds::hardwood::HardWood::stop(xgi::Input* in, xgi::Output* out)
{
  statusMsg_ = "-";
  try
    {
      tcdsStop();
    }
  catch (xcept::Exception& err)
    {
      std::string const msg = toolbox::toString("An error occured: '%s'.",
                                                err.what());
      LOG4CPLUS_ERROR(logger_, msg);
      statusMsg_ = msg;
    }
  redirect(in, out);
}

void
tcds::hardwood::HardWood::halt(xgi::Input* in, xgi::Output* out)
{
  statusMsg_ = "-";
  try
    {
      tcdsHalt();
    }
  catch (xcept::Exception& err)
    {
      std::string const msg = toolbox::toString("An error occured: '%s'.",
                                                err.what());
      LOG4CPLUS_ERROR(logger_, msg);
      statusMsg_ = msg;
    }
  redirect(in, out);
}

void
tcds::hardwood::HardWood::ttcResync(xgi::Input* in, xgi::Output* out)
{
  statusMsg_ = "-";
  try
    {
      tcdsTTCResync();
    }
  catch (xcept::Exception& err)
    {
      std::string const msg = toolbox::toString("An error occured: '%s'.",
                                                err.what());
      LOG4CPLUS_ERROR(logger_, msg);
      statusMsg_ = msg;
    }
  redirect(in, out);
}

void
tcds::hardwood::HardWood::ttcHardReset(xgi::Input* in, xgi::Output* out)
{
  statusMsg_ = "-";
  try
    {
      tcdsTTCHardReset();
    }
  catch (xcept::Exception& err)
    {
      std::string const msg = toolbox::toString("An error occured: '%s'.",
                                                err.what());
      LOG4CPLUS_ERROR(logger_, msg);
      statusMsg_ = msg;
    }
  redirect(in, out);
}

void
tcds::hardwood::HardWood::onException(xcept::Exception& err)
{
  std::string const msg =
    toolbox::toString("An error occurred in the hardware lease renewal thread : '%s'.",
                      err.what());
  statusMsg_ = msg;
  LOG4CPLUS_ERROR(logger_, "HardWood");
  LOG4CPLUS_ERROR(logger_, msg.c_str());
}
