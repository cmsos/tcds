#include "tcds/hardwood/SOAPUtils.h"

#include <inttypes.h>

#include "toolbox/string.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPBody.h"
#include "xoap/SOAPElement.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPFault.h"
#include "xoap/SOAPName.h"
#include "xoap/SOAPPart.h"

xoap::MessageReference
tcds::hardwood::soap::createSimpleSOAPCommand(std::string const& command,
                                              std::string const& actionRequestorId)
{
  xoap::MessageReference msg = xoap::createMessage();
  xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();
  envelope.addNamespaceDeclaration("xsi",
                                   "http://www.w3.org/2001/XMLSchema-instance");
  envelope.addNamespaceDeclaration("xsd",
                                   "http://www.w3.org/2001/XMLSchema");
  xoap::SOAPBody body = envelope.getBody();
  xoap::SOAPName commandName = envelope.createName(command,
                                                   "xdaq",
                                                   "urn:xdaq-soap:3.0");
  xoap::SOAPElement commandElement = body.addBodyElement(commandName);

  if (actionRequestorId.size() > 0)
    {
      xoap::SOAPName reqIdName = envelope.createName("actionRequestorId",
                                                     "xdaq",
                                                     "dummy");
      commandElement.addAttribute(reqIdName, actionRequestorId);
    }
  return msg;
}

xoap::MessageReference
tcds::hardwood::soap::createConfigureSOAPCommand(std::string const& actionRequestorId,
                                                 std::string const& hwCfgString)
{
  xoap::MessageReference msg = createSimpleSOAPCommand("Configure",
                                                       actionRequestorId);
  xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();
  xoap::SOAPBody body = envelope.getBody();
  xoap::SOAPElement commandElement = body.getChildElements().at(0);
  xoap::SOAPName hwCfgName = envelope.createName("hardwareConfigurationString",
                                                 "xdaq",
                                                 "dummy");
  xoap::SOAPElement hwCfgElement = commandElement.addChildElement(hwCfgName);
  xoap::SOAPName typeName = envelope.createName("type",
                                                "xsi",
                                                "http://www.w3.org/2001/XMLSchema-instance");
  hwCfgElement.addAttribute(typeName, "xsd:string");
  hwCfgElement.setTextContent(hwCfgString);
  return msg;
}

xoap::MessageReference
tcds::hardwood::soap::createEnableSOAPCommand(std::string const& actionRequestorId,
                                              unsigned int const runNumber)
{
  xoap::MessageReference msg = createSimpleSOAPCommand("Enable",
                                                       actionRequestorId);
  xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();
  xoap::SOAPBody body = envelope.getBody();
  xoap::SOAPElement commandElement = body.getChildElements().at(0);
  xoap::SOAPName runNumberName = envelope.createName("runNumber",
                                                     "xdaq",
                                                     "dummy");
  xoap::SOAPElement runNumberElement = commandElement.addChildElement(runNumberName);
  xoap::SOAPName typeName = envelope.createName("type",
                                                "xsi",
                                                "http://www.w3.org/2001/XMLSchema-instance");
  runNumberElement.addAttribute(typeName, "xsd:unsignedInt");
  runNumberElement.setTextContent(toolbox::toString("%" PRIu32, runNumber));
  return msg;
}

xoap::MessageReference
tcds::hardwood::soap::createParameterGet(std::string const& className,
                                         std::string const& parName,
                                         std::string const& parType)
{
  std::string const applicationNameSpace =
    toolbox::toString("urn:xdaq-application:%s", className.c_str());
  xoap::MessageReference msg = xoap::createMessage();
  xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();
  envelope.addNamespaceDeclaration("xsi",
                                   "http://www.w3.org/2001/XMLSchema-instance");
  envelope.addNamespaceDeclaration("xsd",
                                   "http://www.w3.org/2001/XMLSchema");
  envelope.addNamespaceDeclaration("soapenc",
                                   "http://schemas.xmlsoap.org/soap/encoding/");
  xoap::SOAPBody body = envelope.getBody();
  xoap::SOAPName commandName = envelope.createName("ParameterGet",
                                                   "xdaq",
                                                   "urn:xdaq-soap:3.0");
  xoap::SOAPElement commandElement = body.addBodyElement(commandName);
  xoap::SOAPName propertiesName = envelope.createName("properties",
                                                      "p",
                                                      applicationNameSpace);
  xoap::SOAPElement propertiesElement = commandElement.addChildElement(propertiesName);
  xoap::SOAPName propertiesTypeName = envelope.createName("type",
                                                          "xsi",
                                                          "http://www.w3.org/2001/XMLSchema-instance");
  propertiesElement.addAttribute(propertiesTypeName, "soapenc:Struct");

  xoap::SOAPName propertyName = envelope.createName(parName, "p", applicationNameSpace);
  xoap::SOAPElement propertyElement = propertiesElement.addChildElement(propertyName);
  xoap::SOAPName propertyTypeName = envelope.createName("type",
                                                        "xsi",
                                                        "http://www.w3.org/2001/XMLSchema-instance");
  propertyElement.addAttribute(propertyTypeName, parType);
  return msg;
}

bool
tcds::hardwood::soap::hasFault(xoap::MessageReference const& msg)
{
  return msg->getSOAPPart().getEnvelope().getBody().hasFault();
}

bool
tcds::hardwood::soap::hasFaultDetail(xoap::MessageReference const& msg)
{
  bool res = false;
  if (tcds::hardwood::soap::hasFault(msg))
    {
      res = msg->getSOAPPart().getEnvelope().getBody().getFault().hasDetail();
    }
  return res;
}

std::string
tcds::hardwood::soap::extractFaultString(xoap::MessageReference const& msg)
{
  std::string res("");
  if (tcds::hardwood::soap::hasFault(msg))
    {
      xoap::SOAPFault fault = msg->getSOAPPart().getEnvelope().getBody().getFault();
      res = fault.getFaultString();
    }
  return res;
}

std::string
tcds::hardwood::soap::extractFaultDetail(xoap::MessageReference const& msg)
{
  std::string res("");
  if (tcds::hardwood::soap::hasFaultDetail(msg))
    {
      xoap::SOAPFault fault = msg->getSOAPPart().getEnvelope().getBody().getFault();
      res = fault.getDetail().getTextContent();
    }
  return res;
}
