#ifndef _tcds_hardwood_SOAPUtils_h
#define _tcds_hardwood_SOAPUtils_h

#include <string>

#include "xoap/MessageReference.h"

namespace tcds {
  namespace hardwood {

    namespace soap {

      xoap::MessageReference createSimpleSOAPCommand(std::string const& command,
                                                     std::string const& actionRequestorId="");
      xoap::MessageReference createConfigureSOAPCommand(std::string const& actionRequestorId,
                                                        std::string const& hwCfgString);
      xoap::MessageReference createEnableSOAPCommand(std::string const& actionRequestorId,
                                                     unsigned int const runNumber);

      xoap::MessageReference createParameterGet(std::string const& className,
                                                std::string const& parName,
                                                std::string const& parType);

      bool hasFault(xoap::MessageReference const& msg);
      bool hasFaultDetail(xoap::MessageReference const& msg);
      std::string extractFaultString(xoap::MessageReference const& msg);
      std::string extractFaultDetail(xoap::MessageReference const& msg);

    } // namespace soap

  } // namespace hardwood
} // namespace tcds

#endif // _tcds_hardwood_SOAPUtils_h
