#ifndef _tcds_hardwood_HardWoodBase_h_
#define _tcds_hardwood_HardWoodBase_h_

#include <memory>
#include <string>

#include "toolbox/exception/Listener.h"
#include "xdata/String.h"
#include "xdata/UnsignedInteger.h"
#include "xoap/MessageReference.h"

namespace log4cplus {
  class Logger;
}

namespace xcept {
  class Exception;
}

namespace xdaq {
  class Application;
  class ApplicationDescriptor;
}

namespace tcds {
  namespace hardwood {

    class HwLeaseHandler;

    class HardWoodBase : public toolbox::exception::Listener
    {

    protected:
      HardWoodBase(xdaq::Application* const xdaqApp);
      HardWoodBase(tcds::hardwood::HardWoodBase const&) = delete;
      ~HardWoodBase();

      std::string tcdsAppClassName();
      unsigned int tcdsAppInstance();
      std::string sessionId();
      std::string hwLeaseRenewalInterval();

      std::string tcdsQueryFSMState();
      std::string tcdsQueryHwLeaseOwnerId();

      void tcdsConfigure(std::string const& hwCfgString);
      void tcdsEnable(unsigned int const runNumber);
      void tcdsPause();
      void tcdsResume();
      void tcdsStop();
      void tcdsHalt();
      void tcdsTTCResync();
      void tcdsTTCHardReset();

      void tcdsRenewHardwareLease();

      xoap::MessageReference executeSOAPCommand(xoap::MessageReference& cmd);

      virtual void onException(xcept::Exception& err);

      log4cplus::Logger& logger_;

    private:
      xoap::MessageReference sendSOAP(xoap::MessageReference& msg);
      xoap::MessageReference postSOAP(xoap::MessageReference& msg,
                                      xdaq::ApplicationDescriptor const& destination);
      xdaq::ApplicationDescriptor const* getDestinationDescriptor() const;

      bool isTimerActive() const;

      xdaq::Application* xdaqAppP_;
      xdata::String sessionId_;

      // These two parameters are used to find the TCDS application to
      // talk to in the XDAQ configuration (.xml) file.
      xdata::String tcdsAppClassName_;
      xdata::UnsignedInteger tcdsAppInstance_;

      // A pointer to the automatic hardware-lease-renewal thingy.
      std::unique_ptr<tcds::hardwood::HwLeaseHandler> hwLeaseHandlerP_;
      xdata::String hwLeaseRenewalInterval_;

    };

  } // namespace hardwood
} // namespace tcds

#endif // _tcds_hardwood_HardWoodBase_h_
