#ifndef _tcds_hardwood_exception_Exception_h_
#define _tcds_hardwood_exception_Exception_h_

#include <string>

#include "xcept/Exception.h"

namespace tcds {
  namespace hardwood {
    namespace exception {

      class Exception : public xcept::Exception
      {
      public:
        Exception(std::string name,
                  std::string message,
                  std::string module,
                  int line,
                  std::string function);
        Exception(std::string name,
                  std::string message,
                  std::string module,
                  int line,
                  std::string function,
                  xcept::Exception& err);
      };

    } // namespace exception
  } // namespace hardwood
} // namespace tcds

// Mimick XCEPT_DEFINE_EXCEPTION from xcept/Exception.h.
#define HARDWOOD_DEFINE_EXCEPTION(EXCEPTION_NAME)                                      \
  namespace tcds {                                                                     \
    namespace hardwood {                                                               \
      namespace exception {                                                            \
        class EXCEPTION_NAME : public tcds::hardwood::exception::Exception             \
        {                                                                              \
        public :                                                                       \
        EXCEPTION_NAME(std::string name,                                               \
                       std::string message,                                            \
                       std::string module,                                             \
                       int line,                                                       \
                       std::string function) :                                         \
          tcds::hardwood::exception::Exception(name, message, module, line, function)  \
            {};                                                                        \
      EXCEPTION_NAME(std::string name,                                                 \
                     std::string message,                                              \
                     std::string module,                                               \
                     int line,                                                         \
                     std::string function,                                             \
                     xcept::Exception& err) :                                          \
      tcds::hardwood::exception::Exception(name, message, module, line, function, err) \
        {};                                                                            \
        };                                                                             \
      }                                                                                \
    }                                                                                  \
  }                                                                                    \

HARDWOOD_DEFINE_EXCEPTION(RuntimeError)
HARDWOOD_DEFINE_EXCEPTION(ValueError)

#endif // _tcds_exception_hardwood_Exception_h_
