#ifndef _tcds_hardwood_HardWood_h_
#define _tcds_hardwood_HardWood_h_

#include "xdaq/Application.h"
#include "xdata/String.h"
#include "xdata/UnsignedInteger32.h"
#include "xgi/framework/UIManager.h"

#include "tcds/hardwood/HardWoodBase.h"

namespace log4cplus {
  class Logger;
}

namespace xcept {
  class Exception;
}

namespace xdaq {
  class ApplicationStub;
}

namespace xgi {
  class Input;
  class Output;
}

namespace tcds {
  namespace hardwood {

    class HardWood :
      public xdaq::Application,
      public xgi::framework::UIManager,
      public tcds::hardwood::HardWoodBase
    {

    public:
      XDAQ_INSTANTIATOR();

      HardWood(xdaq::ApplicationStub* stub);
      virtual ~HardWood();

    private:
      void mainPage(xgi::Input* in, xgi::Output* out);
      void redirect(xgi::Input* in, xgi::Output* out);

      void queryFSMState(xgi::Input* in, xgi::Output* out);
      void queryHwLeaseOwner(xgi::Input* in, xgi::Output* out);

      void configure(xgi::Input* in, xgi::Output* out);
      void enable(xgi::Input* in, xgi::Output* out);
      void pause(xgi::Input* in, xgi::Output* out);
      void resume(xgi::Input* in, xgi::Output* out);
      void stop(xgi::Input* in, xgi::Output* out);
      void halt(xgi::Input* in, xgi::Output* out);
      void ttcResync(xgi::Input* in, xgi::Output* out);
      void ttcHardReset(xgi::Input* in, xgi::Output* out);

      virtual void onException(xcept::Exception& err);

      xdata::String hwCfgString_;
      xdata::UnsignedInteger32 runNumber_;

      xdata::String state_;
      xdata::String hwLeaseOwnerId_;

      xdata::String statusMsg_;
      log4cplus::Logger& logger_;

    };

  } // namespace hardwood
} // namespace tcds

#endif // _tcds_hardwood_HardWood_h_
