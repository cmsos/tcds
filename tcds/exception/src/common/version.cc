#include "tcds/exception/version.h"

#include "config/version.h"
#include "xcept/version.h"

GETPACKAGEINFO(tcds::exception)

void
tcds::exception::checkPackageDependencies()
{
  CHECKDEPENDENCY(config);
  CHECKDEPENDENCY(xcept);
}

std::set<std::string, std::less<std::string> >
tcds::exception::getPackageDependencies()
{
  std::set<std::string, std::less<std::string> > dependencies;

  ADDDEPENDENCY(dependencies, config);
  ADDDEPENDENCY(dependencies, xcept);

  return dependencies;
}
