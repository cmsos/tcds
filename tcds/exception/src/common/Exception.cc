#include "tcds/exception/Exception.h"

tcds::exception::Exception::Exception(std::string name,
                                      std::string message,
                                      std::string module,
                                      int line,
                                      std::string function) :
  xcept::Exception(name, message, module, line, function)
{
}

tcds::exception::Exception::Exception(std::string name,
                                      std::string message,
                                      std::string module,
                                      int line,
                                      std::string function,
                                      xcept::Exception const& err) :
  xcept::Exception(name, message, module, line, function, err)
{
}
