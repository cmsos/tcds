#ifndef _tcds_exception_Exception_h_
#define _tcds_exception_Exception_h_

#include <string>

#include "xcept/Exception.h"

// A little helper to save us some typing.
#define TCDS_DEFINE_ALARM(ALARM_NAME) \
  TCDS_DEFINE_EXCEPTION(ALARM_NAME)

namespace tcds {
  namespace exception {

    class Exception : public xcept::Exception
    {
    public:
    Exception(std::string name,
              std::string message,
              std::string module,
              int line,
              std::string function);
    Exception(std::string name,
              std::string message,
              std::string module,
              int line,
              std::string function,
              xcept::Exception const& err);
    };

  } // namespace exception
} // namespace tcds

// Mimick XCEPT_DEFINE_EXCEPTION from xcept/Exception.h.
#define TCDS_DEFINE_EXCEPTION(EXCEPTION_NAME)                                  \
  namespace tcds {                                                             \
    namespace exception {                                                      \
      class EXCEPTION_NAME : public tcds::exception::Exception                 \
      {                                                                        \
      public :                                                                 \
      EXCEPTION_NAME(std::string name,                                         \
                     std::string message,                                      \
                     std::string module,                                       \
                     int line,                                                 \
                     std::string function) :                                   \
        tcds::exception::Exception(name, message, module, line, function)      \
          {};                                                                  \
      EXCEPTION_NAME(std::string name,                                         \
                     std::string message,                                      \
                     std::string module,                                       \
                     int line,                                                 \
                     std::string function,                                     \
                     xcept::Exception const& err) :                            \
        tcds::exception::Exception(name, message, module, line, function, err) \
          {};                                                                  \
      };                                                                       \
    }                                                                          \
  }                                                                            \

// The TCDS exceptions.
TCDS_DEFINE_EXCEPTION(BRILDAQProblem)
TCDS_DEFINE_EXCEPTION(ConfigurationParseProblem)
TCDS_DEFINE_EXCEPTION(ConfigurationProblem)
TCDS_DEFINE_EXCEPTION(ConfigurationValidationProblem)
TCDS_DEFINE_EXCEPTION(FSMTransitionProblem)
TCDS_DEFINE_EXCEPTION(HardwareProblem)
TCDS_DEFINE_EXCEPTION(HwLeaseRenewalProblem)
TCDS_DEFINE_EXCEPTION(HTTPProblem)
TCDS_DEFINE_EXCEPTION(I2CBusBusy)
TCDS_DEFINE_EXCEPTION(I2CProblem)
TCDS_DEFINE_EXCEPTION(NibbleDAQProblem)
TCDS_DEFINE_EXCEPTION(MonitoringProblem)
TCDS_DEFINE_EXCEPTION(RCMSNotificationError)
TCDS_DEFINE_EXCEPTION(RuntimeProblem)
TCDS_DEFINE_EXCEPTION(SOAPFormatProblem)
TCDS_DEFINE_EXCEPTION(SOAPCommandProblem)
TCDS_DEFINE_EXCEPTION(SoftwareProblem)
TCDS_DEFINE_EXCEPTION(TTCClockProblem)
TCDS_DEFINE_EXCEPTION(TTCOrbitProblem)
TCDS_DEFINE_EXCEPTION(TTCStreamProblem)
TCDS_DEFINE_EXCEPTION(ValueError)
TCDS_DEFINE_EXCEPTION(XDataProblem)

// The TCDS alarms.
TCDS_DEFINE_ALARM(BRILDAQFailureAlarm)
TCDS_DEFINE_ALARM(FSMDriverFailureAlarm)
TCDS_DEFINE_ALARM(FreqMonFailureAlarm)
TCDS_DEFINE_ALARM(MonitoringFailureAlarm)
TCDS_DEFINE_ALARM(NibbleDAQFailureAlarm)
TCDS_DEFINE_ALARM(PIDAQFailureAlarm)
TCDS_DEFINE_ALARM(PhaseMonFailureAlarm)
TCDS_DEFINE_ALARM(RFInfoDAQFailureAlarm)

#endif // _tcds_exception_Exception_h_
