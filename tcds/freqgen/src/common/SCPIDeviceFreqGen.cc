#include "tcds/freqgen/SCPIDeviceFreqGen.h"

#include <sstream>
#include <string>

#include "toolbox/string.h"

#include "tcds/hwlayerscpi/HwDeviceSCPI.h"

tcds::freqgen::SCPIDeviceFreqGen::SCPIDeviceFreqGen()
{
}

tcds::freqgen::SCPIDeviceFreqGen::~SCPIDeviceFreqGen()
{
}

double
tcds::freqgen::SCPIDeviceFreqGen::readFreq(tcds::definitions::FREQ_SINK const sinkId) const
{
  std::string const cmd = toolbox::toString("SOURCE%d:FREQUENCY?", sinkId);
  std::string const rawReply = hwDevice_.execCommand(cmd);
  double res;
  std::stringstream(rawReply) >> res;
  return res;
}

tcds::definitions::REF_SOURCE
tcds::freqgen::SCPIDeviceFreqGen::getReferenceSource() const
{
  tcds::definitions::REF_SOURCE res = tcds::definitions::REF_SOURCE_UNKNOWN;

  std::string const cmd = "ROSCILLATOR:SOURCE:CURRENT?";
  std::string const rawReply = hwDevice_.execCommand(cmd);

  if (rawReply == "INT")
    {
      res = tcds::definitions::REF_SOURCE_INTERNAL;
    }
  else if (rawReply == "EXT")
    {
      res = tcds::definitions::REF_SOURCE_EXTERNAL_10MHZ;
    }
  else
    {
      res = tcds::definitions::REF_SOURCE_UNKNOWN;
    }

  return res;
}
