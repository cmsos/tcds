#include "tcds/freqgen/FreqGen.h"

#include <string>
#include <vector>

#include "toolbox/string.h"
#include "xcept/Exception.h"
#include "xdaq/exception/ApplicationInstantiationFailed.h"

#include "tcds/exception/Exception.h"
#include "tcds/freqgen/ConfigurationInfoSpaceHandler.h"
#include "tcds/freqgen/FreqGenInfoSpaceHandler.h"
#include "tcds/freqgen/FreqGenInfoSpaceUpdater.h"
#include "tcds/hwlayer/DeviceBase.h"
#include "tcds/hwlayerscpi/SCPIDeviceBase.h"
#include "tcds/hwutilsscpi/HwIDInfoSpaceHandlerSCPI.h"
#include "tcds/hwutilsscpi/HwIDInfoSpaceUpdaterSCPI.h"
#include "tcds/hwutilsscpi/HwStatusInfoSpaceHandlerSCPI.h"
#include "tcds/hwutilsscpi/HwStatusInfoSpaceUpdaterSCPI.h"
#include "tcds/hwutilsscpi/Utils.h"

XDAQ_INSTANTIATOR_IMPL(tcds::freqgen::FreqGen)

tcds::freqgen::FreqGen::FreqGen(xdaq::ApplicationStub* stub)
try
  :
  tcds::utils::XDAQAppWithFSMAutomatic(stub, std::unique_ptr<tcds::hwlayer::DeviceBase>(new tcds::freqgen::SCPIDeviceFreqGen()))
    {
      // Create the InfoSpace holding all configuration information.
      cfgInfoSpaceP_ =
        std::unique_ptr<tcds::freqgen::ConfigurationInfoSpaceHandler>(new tcds::freqgen::ConfigurationInfoSpaceHandler(*this));
    }
catch (tcds::exception::Exception const& err)
  {
    std::string msgBase = "Something went wrong instantiating the FreqGen application";
    std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.what());
    XCEPT_RAISE(xdaq::exception::ApplicationInstantiationFailed, msg.c_str());
  }

tcds::freqgen::FreqGen::~FreqGen()
{
}

void
tcds::freqgen::FreqGen::setupInfoSpaces()
{
  // Make sure the configuration settings are up-to-date.
  cfgInfoSpaceP_->readInfoSpace();

  appStateInfoSpace_.setString("hwLeaseOwnerId", "n/a");

  // Instantiate all hardware-dependent InfoSpaceHandlers and
  // InfoSpaceUpdaters.
  freqgenInfoSpaceUpdaterP_ = std::unique_ptr<tcds::freqgen::FreqGenInfoSpaceUpdater>(new tcds::freqgen::FreqGenInfoSpaceUpdater(*this, getHw()));
  freqgenInfoSpaceP_ =
    std::unique_ptr<tcds::freqgen::FreqGenInfoSpaceHandler>(new tcds::freqgen::FreqGenInfoSpaceHandler(*this, freqgenInfoSpaceUpdaterP_.get()));
  hwIDInfoSpaceUpdaterP_ = std::unique_ptr<tcds::hwutilsscpi::HwIDInfoSpaceUpdaterSCPI>(new tcds::hwutilsscpi::HwIDInfoSpaceUpdaterSCPI(*this, getHw()));
  hwIDInfoSpaceP_ =
    std::unique_ptr<tcds::hwutilsscpi::HwIDInfoSpaceHandlerSCPI>(new tcds::hwutilsscpi::HwIDInfoSpaceHandlerSCPI(*this, hwIDInfoSpaceUpdaterP_.get()));
  hwStatusInfoSpaceUpdaterP_ = std::unique_ptr<tcds::hwutilsscpi::HwStatusInfoSpaceUpdaterSCPI>(new tcds::hwutilsscpi::HwStatusInfoSpaceUpdaterSCPI(*this, getHw()));
  hwStatusInfoSpaceP_ =
    std::unique_ptr<tcds::hwutilsscpi::HwStatusInfoSpaceHandlerSCPI>(new tcds::hwutilsscpi::HwStatusInfoSpaceHandlerSCPI(*this, hwStatusInfoSpaceUpdaterP_.get()));

  // Register all InfoSpaceItems with the Monitor.
  cfgInfoSpaceP_->registerItemSets(monitor_, webServer_);
  appStateInfoSpace_.registerItemSets(monitor_, webServer_);
  hwIDInfoSpaceP_->registerItemSets(monitor_, webServer_);
  hwStatusInfoSpaceP_->registerItemSets(monitor_, webServer_);
  freqgenInfoSpaceP_->registerItemSets(monitor_, webServer_);
}

tcds::freqgen::SCPIDeviceFreqGen&
tcds::freqgen::FreqGen::getHw() const
{
  return static_cast<tcds::freqgen::SCPIDeviceFreqGen&>(*hwP_.get());
}

void
tcds::freqgen::FreqGen::hwConnectImpl()
{
  // Connect to the hardware.
  tcds::hwutilsscpi::scpiDeviceHwConnectImpl(*cfgInfoSpaceP_, getHw());
}

void
tcds::freqgen::FreqGen::hwReleaseImpl()
{
  getHw().hwRelease();
}

void
tcds::freqgen::FreqGen::hwConfigureImpl()
{
  // NOTE: The following is the output from the '*LRN?' SCPI command
  // run on the device after configuring it in the desired state. It
  // could be read from a text file, of course, but this is not
  // expected (nor supposed) to change very often.
  std::vector<std::string> commands;
  commands.push_back("*RST");
  commands.push_back(":SOUR1:FUNC SQU");
  commands.push_back(":SOUR1:FREQ +4.007900000000000E+07");
  commands.push_back(":OUTP1:LOAD +5.000000000000000E+01");
  commands.push_back(":SOUR1:VOLT +5.0000000000000E-01");
  commands.push_back(":SOUR1:VOLT:OFFS +0.0000000000000E+00");
  commands.push_back(":SOUR1:FREQ:MODE CW");
  commands.push_back(":SOUR1:VOLT:LIM:HIGH +5.000000000000000E+00");
  commands.push_back(":SOUR1:VOLT:LIM:LOW -5.000000000000000E+00");
  commands.push_back(":SOUR1:VOLT:LIM:STAT 0");
  commands.push_back(":SOUR1:VOLT:UNIT VPP");
  commands.push_back(":SOUR1:VOLT:HIGH +2.500000000000000E-01");
  commands.push_back(":SOUR1:VOLT:LOW -2.500000000000000E-01");
  commands.push_back(":SOUR1:VOLT:OFFS +0.0000000000000E+00");
  commands.push_back(":SOUR1:VOLT:RANG:AUTO 1");
  commands.push_back(":SOUR1:FUNC:NOIS:BAND +1.000000000000000E+06");
  commands.push_back(":SOUR1:FUNC:PRBS:DATA PN7");
  commands.push_back(":SOUR1:FUNC:PRBS:BRAT +1.000000000000000E+03");
  commands.push_back(":SOUR1:FUNC:PULS:DCYC +1.000000000000000E+01");
  commands.push_back(":SOUR1:FUNC:PULS:PER +2.495100000000000E-08");
  commands.push_back(":SOUR1:FUNC:PULS:TRAN:LEAD +4.000000000000000E-09");
  commands.push_back(":SOUR1:FUNC:PULS:TRAN:TRA +4.000000000000000E-09");
  commands.push_back(":SOUR1:FUNC:PULS:WIDT +1.000000000000000E-04");
  commands.push_back(":SOUR1:FUNC:RAMP:SYMM +1.000000000000000E+02");
  commands.push_back(":SOUR1:FUNC:SQU:DCYC +5.000000000000000E+01");
  commands.push_back(":SOUR1:FUNC:SQU:PER +2.495100000000000E-08");
  commands.push_back(":SOUR1:FUNC:ARB \"INT:\\BUILTIN\\EXP_RISE.ARB\"");
  commands.push_back(":SOUR1:FUNC:ARB:FILT STEP");
  commands.push_back(":SOUR1:FUNC:ARB:SRAT +3.200000000000000E+03");
  commands.push_back(":SOUR1:FUNC:ARB:ADV SRAT");
  commands.push_back(":SOUR1:FUNC:USER EXP_RISE");
  commands.push_back(":SOUR1:AM:INT:FREQ +1.000000000000000E+02");
  commands.push_back(":SOUR1:AM:INT:FUNC SIN");
  commands.push_back(":SOUR1:AM:SOUR INT");
  commands.push_back(":SOUR1:AM:STAT 0");
  commands.push_back(":SOUR1:AM:DEPT +1.000000000000000E+02");
  commands.push_back(":SOUR1:AM:DSSC 0");
  commands.push_back(":SOUR1:FM:INT:FREQ +1.000000000000000E+01");
  commands.push_back(":SOUR1:FM:INT:FUNC SIN");
  commands.push_back(":SOUR1:FM:SOUR INT");
  commands.push_back(":SOUR1:FM:STAT 0");
  commands.push_back(":SOUR1:FM:DEV +1.000000000000000E+02");
  commands.push_back(":SOUR1:FSK:INT:RATE +1.000000000000000E+01");
  commands.push_back(":SOUR1:FSK:SOUR INT");
  commands.push_back(":SOUR1:FSK:STAT 0");
  commands.push_back(":SOUR1:FSK:FREQ +1.000000000000000E+02");
  commands.push_back(":SOUR1:PM:INT:FREQ +1.000000000000000E+01");
  commands.push_back(":SOUR1:PM:INT:FUNC NOIS");
  commands.push_back(":SOUR1:PM:SOUR INT");
  commands.push_back(":SOUR1:PM:STAT 0");
  commands.push_back(":SOUR1:PM:DEV +1.800000000000000E+02");
  commands.push_back(":SOUR1:BPSK:INT:RATE +1.000000000000000E+01");
  commands.push_back(":SOUR1:BPSK:SOUR INT");
  commands.push_back(":SOUR1:BPSK:STAT 0");
  commands.push_back(":SOUR1:BPSK:PHAS +1.800000000000000E+02");
  commands.push_back(":SOUR1:PWM:INT:FREQ +1.000000000000000E+01");
  commands.push_back(":SOUR1:PWM:INT:FUNC SIN");
  commands.push_back(":SOUR1:PWM:SOUR INT");
  commands.push_back(":SOUR1:PWM:STAT 0");
  commands.push_back(":SOUR1:PWM:DEV +1.000000000000000E-05");
  commands.push_back(":SOUR1:PWM:DCYC +1.000000000000000E+00");
  commands.push_back(":SOUR1:SUM:SOUR INT");
  commands.push_back(":SOUR1:SUM:AMPL +1.000000000000000E-01");
  commands.push_back(":SOUR1:SUM:INT:FREQ +1.000000000000000E+02");
  commands.push_back(":SOUR1:SUM:INT:FUNC SIN");
  commands.push_back(":SOUR1:SUM:STAT 0");
  commands.push_back(":SOUR1:PHAS:ADJUST +0.0000000000000E+00");
  commands.push_back(":SOUR1:ROSC:SOUR:AUTO ON");
  commands.push_back(":SOUR1:BURS:GATe:POL NORM");
  commands.push_back(":SOUR1:BURS:MODE TRIG");
  commands.push_back(":SOUR1:BURS:NCYC +1.000000000000000E+00");
  commands.push_back(":SOUR1:BURS:STAT 0");
  commands.push_back(":SOUR1:BURS:INT:PER +1.000000000000000E-02");
  commands.push_back(":SOUR1:BURS:PHAS +0.0000000000000E+00");
  commands.push_back(":SOUR1:BURS:INT:RATE +1.000000000000000E+02");
  commands.push_back(":SOUR1:BURS:SOUR INT");
  commands.push_back(":SOUR1:MARK:CYCL +2.000000000000000E+00");
  commands.push_back(":SOUR1:FREQ:CENT +5.500000000000000E+02");
  commands.push_back(":SOUR1:FREQ:SPAN +9.000000000000000E+02");
  commands.push_back(":SOUR1:FREQ:STAR +1.000000000000000E+02");
  commands.push_back(":SOUR1:FREQ:STOP +1.000000000000000E+03");
  commands.push_back(":SOUR1:MARK:STAT 0");
  commands.push_back(":SOUR1:MARK:FREQ +5.000000000000000E+02");
  commands.push_back(":SOUR1:SWE:HTIM +0.000000000000000E+00");
  commands.push_back(":SOUR1:SWE:RTIM +0.000000000000000E+00");
  commands.push_back(":SOUR1:SWE:SPAC LIN");
  commands.push_back(":SOUR1:SWE:STAT 0");
  commands.push_back(":SOUR1:SWE:TIME +1.000000000000000E+00");
  commands.push_back(":FORM:BORD NORM");
  commands.push_back(":UNIT:ANGL DEG");
  commands.push_back(":DISP:TEXT \"\"");
  commands.push_back(":HCOP:SDUM:DATA:FORM PNG");
  commands.push_back(":DISP 1");
  commands.push_back(":OUTP:SYNC:STAT 0");
  commands.push_back(":OUTP:SYNC:SOUR CH1");
  commands.push_back(":OUTP:SYNC:POL NORM");
  commands.push_back(":OUTP:SYNC:MODE NORM");
  commands.push_back(":OUTP:MODE NORM");
  commands.push_back(":OUTP:TRIG:SOUR CH1");
  commands.push_back(":OUTP:TRIG:SLOP NEG");
  commands.push_back(":OUTP:TRIG 0");
  commands.push_back(":OUTP1 1");
  commands.push_back(":OUTP1:POL NORM");
  commands.push_back(":SYST:BEEP:STAT 0");
  commands.push_back(":TRIG1:SOUR IMM");
  commands.push_back(":TRIG1:SLOP POS");
  commands.push_back("*ESE +0");
  commands.push_back("*PSC 1");
  commands.push_back("*SRE +0");

  tcds::hwlayerscpi::SCPIDeviceBase const& hw = getHw();
  hw.reset();
  for (std::vector<std::string>::const_iterator it = commands.begin();
       it != commands.end();
       ++it)
    {
      hw.execCommand(*it);
    }
}
