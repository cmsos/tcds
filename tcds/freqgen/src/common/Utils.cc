#include "tcds/freqgen/Utils.h"

#include "toolbox/string.h"
#include "xcept/Exception.h"

#include "tcds/exception/Exception.h"
#include "tcds/freqgen/Definitions.h"

std::map<tcds::definitions::FREQ_SINK, std::string>
tcds::freqgen::freqSinkNameMap()
{
  std::map<tcds::definitions::FREQ_SINK, std::string> names;

  names[tcds::definitions::FREQ_SINK_OUTPUT1] = "Output1";

  return names;
}

std::string
tcds::freqgen::freqChannelToName(tcds::definitions::FREQ_SINK const channel)
{
  std::map<tcds::definitions::FREQ_SINK, std::string> names = freqSinkNameMap();
  std::map<tcds::definitions::FREQ_SINK, std::string>::const_iterator i = names.find(channel);
  if (i == names.end())
    {
      XCEPT_RAISE(tcds::exception::ValueError,
                  toolbox::toString("Number %d is not a valid frequency channel (for output name lookups).",
                                    channel));
    }
  return i->second;
}
