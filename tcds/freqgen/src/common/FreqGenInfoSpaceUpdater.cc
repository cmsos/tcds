#include "tcds/freqgen/FreqGenInfoSpaceUpdater.h"

#include <stdint.h>
#include <string>

#include "toolbox/string.h"
#include "xcept/Exception.h"

#include "tcds/exception/Exception.h"
#include "tcds/freqgen/Definitions.h"
#include "tcds/freqgen/SCPIDeviceFreqGen.h"
#include "tcds/utils/InfoSpaceHandler.h"

tcds::freqgen::FreqGenInfoSpaceUpdater::FreqGenInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                                                tcds::freqgen::SCPIDeviceFreqGen const& hw) :
  tcds::utils::HwInfoSpaceUpdaterBase(xdaqApp, hw)
{
}

void
tcds::freqgen::FreqGenInfoSpaceUpdater::updateInfoSpaceImpl(tcds::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  uint32_t const sinkId = infoSpaceHandler->getUInt32("freq_sink_id_number");

  tcds::freqgen::SCPIDeviceFreqGen const& hw = getHw();
  if (hw.isHwConnected())
    {
      switch (sinkId)
        {
        case tcds::definitions::FREQ_SINK_OUTPUT1:
          break;
        default:
          XCEPT_RAISE(tcds::exception::ValueError,
                      toolbox::toString("Trying to update monitoring info "
                                        "for an unknown frequency generator "
                                        "output ID: %d",
                                        sinkId));
          break;
        }

      // Get an updated frequency value from the hardware.
      tcds::definitions::FREQ_SINK const sinkIdEnum =
        static_cast<tcds::definitions::FREQ_SINK>(sinkId);
      double const valueCurr = hw.readFreq(sinkIdEnum);
      infoSpaceHandler->setDouble("freq_sink_value", valueCurr);
      infoSpaceHandler->writeInfoSpace();
      infoSpaceHandler->setValid();
    }
  else
    {
      infoSpaceHandler->setInvalid();
    }
}

tcds::freqgen::SCPIDeviceFreqGen const&
tcds::freqgen::FreqGenInfoSpaceUpdater::getHw() const
{
  return static_cast<tcds::freqgen::SCPIDeviceFreqGen const&>(tcds::utils::HwInfoSpaceUpdaterBase::getHw());
}
