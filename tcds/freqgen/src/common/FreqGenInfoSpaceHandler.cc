#include "tcds/freqgen/FreqGenInfoSpaceHandler.h"

#include "toolbox/string.h"

#include "tcds/freqgen/Utils.h"
#include "tcds/utils/ConfigurationInfoSpaceHandler.h"
#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/WebServer.h"
#include "tcds/utils/XDAQAppBase.h"

namespace xdaq {
  class Application;
}

tcds::freqgen::FreqGenInfoSpaceHandler::FreqGenInfoSpaceHandler(xdaq::Application& xdaqApp,
                                                                tcds::utils::InfoSpaceUpdater* updater) :
  tcds::utils::MultiInfoSpaceHandler(xdaqApp, updater)
{
  tcds::utils::ConfigurationInfoSpaceHandler const& cfgInfoSpace =
    getOwnerApplication().getConfigurationInfoSpaceHandler();

  // Pre-build all output labels.
  for (unsigned int sink = tcds::definitions::FREQ_SINK_MIN;
       sink <= tcds::definitions::FREQ_SINK_MAX;
       ++sink)
    {
      tcds::definitions::FREQ_SINK const channel = static_cast<tcds::definitions::FREQ_SINK>(sink);
      std::string const name = freqChannelToName(channel);
      std::string const label = cfgInfoSpace.getString("label" + name);
      outputNames_[channel] = name;
      outputLabels_[channel] = label;
    }

  //----------

  // All individual frequency values.
  for (unsigned int sink = tcds::definitions::FREQ_SINK_MIN;
       sink <= tcds::definitions::FREQ_SINK_MAX;
       ++sink)
    {
      tcds::definitions::FREQ_SINK const channel = static_cast<tcds::definitions::FREQ_SINK>(sink);
      std::string const outputName = outputNames_[channel];
      std::string const name = outputName;

      tcds::utils::InfoSpaceHandler* handler =
        new tcds::utils::InfoSpaceHandler(xdaqApp,
                                          toolbox::toString("tcds-freqgen-data-%s",
                                          outputName.c_str()),
                                          updater);
      infoSpaceHandlers_[name] = handler;
      createFreqChannel(handler,
                        outputLabels_[channel],
                        sink);
    }
}

tcds::freqgen::FreqGenInfoSpaceHandler::~FreqGenInfoSpaceHandler()
{
}

tcds::utils::XDAQAppBase&
tcds::freqgen::FreqGenInfoSpaceHandler::getOwnerApplication() const
{
  return static_cast<tcds::utils::XDAQAppBase&>(tcds::utils::MultiInfoSpaceHandler::getOwnerApplication());
}

void
tcds::freqgen::FreqGenInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  // All individual frequency values.
  std::string const itemSetName = "itemset-freqgen-data";
  monitor.newItemSet(itemSetName);
  for (unsigned int sink = tcds::definitions::FREQ_SINK_MIN;
       sink <= tcds::definitions::FREQ_SINK_MAX;
       ++sink)
    {
      tcds::definitions::FREQ_SINK const channel = static_cast<tcds::definitions::FREQ_SINK>(sink);
      std::string const outputName = outputNames_[channel];
      std::string const name = outputName;

      monitor.addItem(itemSetName,
                      "freq_sink_value",
                      outputLabels_[channel],
                      infoSpaceHandlers_.at(name));
    }
}

void
tcds::freqgen::FreqGenInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                      tcds::utils::Monitor& monitor,
                                                                      std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "Frequencies" : forceTabName;

  webServer.registerTab(tabName,
                        "Configured clock frequencies",
                        2);
  //----------

  // All individual frequency values.
  std::string itemSetName = "Configured frequency values";
  webServer.registerTable("Configured frequency",
                          "Configured clock frequency (MHz)",
                          monitor,
                          "itemset-freqgen-data",
                          tabName,
                          3);
}

void
tcds::freqgen::FreqGenInfoSpaceHandler::createFreqChannel(tcds::utils::InfoSpaceHandler* const handler,
                                                          std::string const sinkLabel,
                                                          unsigned int const idNumber)
{
  // A human-readable label for this channel.
  handler->createString("freq_sink_label",
                        sinkLabel,
                        "",
                        tcds::utils::InfoSpaceItem::NOUPDATE);

  // The sink number of this channel.
  handler->createUInt32("freq_sink_id_number",
                        idNumber,
                        "",
                        tcds::utils::InfoSpaceItem::NOUPDATE);

  // The configured value for this channel.
  handler->createDouble("freq_sink_value",
                        0.,
                        "freq_MHz",
                        tcds::utils::InfoSpaceItem::PROCESS);
}
