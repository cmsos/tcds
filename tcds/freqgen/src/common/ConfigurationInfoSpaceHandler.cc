#include "tcds/freqgen/ConfigurationInfoSpaceHandler.h"

#include "tcds/freqgen/Definitions.h"
#include "tcds/freqgen/Utils.h"

tcds::freqgen::ConfigurationInfoSpaceHandler::ConfigurationInfoSpaceHandler(xdaq::Application& xdaqApp) :
  tcds::hwutilsscpi::ConfigurationInfoSpaceHandlerSCPI(xdaqApp)
{
  for (unsigned int sink = tcds::definitions::FREQ_SINK_MIN;
       sink <= tcds::definitions::FREQ_SINK_MAX;
       ++sink)
    {
      tcds::definitions::FREQ_SINK const channel = static_cast<tcds::definitions::FREQ_SINK>(sink);
      std::string const name = freqChannelToName(channel);
      std::string const labelName = "label" + name;
      createString(labelName,
                   "Unknown",
                   "",
                   tcds::utils::InfoSpaceItem::NOUPDATE,
                   true);
    }
}

void
tcds::freqgen::ConfigurationInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  tcds::hwutilsscpi::ConfigurationInfoSpaceHandlerSCPI::registerItemSetsWithMonitor(monitor);
}

void
tcds::freqgen::ConfigurationInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                            tcds::utils::Monitor& monitor,
                                                                            std::string const& forceTabName)
{
  tcds::hwutilsscpi::ConfigurationInfoSpaceHandlerSCPI::registerItemSetsWithWebServer(webServer, monitor, forceTabName);
}
