#ifndef _tcds_freqgen_Utils_h_
#define _tcds_freqgen_Utils_h_

#include <map>
#include <string>

#include "tcds/freqgen/Definitions.h"

namespace tcds {
  namespace freqgen {

    // Mapping of frequency channels to names.
    std::map<tcds::definitions::FREQ_SINK, std::string> freqSinkNameMap();
    std::string freqChannelToName(tcds::definitions::FREQ_SINK const channel);

  } // namespace freqgen
} // namespace tcds

#endif // _tcds_freqgen_Utils_h_
