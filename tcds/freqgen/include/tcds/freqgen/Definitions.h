#ifndef _tcds_freqgen_Definitions_h_
#define _tcds_freqgen_Definitions_h_

#include <stdint.h>
#include <string>

namespace tcds {
  namespace definitions {

    enum FREQ_SINK {FREQ_SINK_OUTPUT1=1};
    FREQ_SINK const FREQ_SINK_MIN = FREQ_SINK_OUTPUT1;
    FREQ_SINK const FREQ_SINK_MAX = FREQ_SINK_OUTPUT1;

  } // namespace definitions
} // namespace tcds

#endif // _tcds_freqgen_Definitions_h_
