#ifndef _tcds_freqgen_FreqGenInfoSpaceHandler_h_
#define _tcds_freqgen_FreqGenInfoSpaceHandler_h_

#include <map>
#include <string>

#include "tcds/freqgen/Definitions.h"
#include "tcds/utils/MultiInfoSpaceHandler.h"

namespace xdaq {
  class Application;
}

namespace tcds {
  namespace utils {
    class InfoSpaceHandler;
    class InfoSpaceUpdater;
    class Monitor;
    class WebServer;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace freqgen {

    class FreqGenInfoSpaceHandler : public tcds::utils::MultiInfoSpaceHandler
    {

    public:
      FreqGenInfoSpaceHandler(xdaq::Application& xdaqApp,
                              tcds::utils::InfoSpaceUpdater* updater);
      virtual ~FreqGenInfoSpaceHandler();

      tcds::utils::XDAQAppBase& getOwnerApplication() const;

    protected:
      virtual void registerItemSetsWithMonitor(tcds::utils::Monitor& monitor);
      virtual void registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                 tcds::utils::Monitor& monitor,
                                                 std::string const& forceTabName="");

    private:
      std::map<tcds::definitions::FREQ_SINK, std::string> outputNames_;
      std::map<tcds::definitions::FREQ_SINK, std::string> outputLabels_;

      void createFreqChannel(tcds::utils::InfoSpaceHandler* const handler,
                             std::string const sinkLabel,
                             unsigned int const idNumber);

    };

  } // namespace freqgen
} // namespace tcds

#endif // _tcds_freqgen_FreqGenInfoSpaceHandler_h_
