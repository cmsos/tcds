#ifndef _tcds_freqgen_SCPIDeviceFreqGen_h_
#define _tcds_freqgen_SCPIDeviceFreqGen_h_

#include "tcds/freqgen/Definitions.h"
#include "tcds/hwlayerscpi/Definitions.h"
#include "tcds/hwlayerscpi/SCPIDeviceBase.h"

namespace tcds {
  namespace freqgen {

    class SCPIDeviceFreqGen : public tcds::hwlayerscpi::SCPIDeviceBase
    {

    public:
      SCPIDeviceFreqGen();
      virtual ~SCPIDeviceFreqGen();

      virtual tcds::definitions::REF_SOURCE getReferenceSource() const;
      double readFreq(tcds::definitions::FREQ_SINK const sinkId) const;

    };

  } // namespace freqgen
} // namespace tcds

#endif // _tcds_freqgen_SCPIDeviceFreqGen_h_
