#ifndef _tcds_freqgen_FreqGenInfoSpaceUpdater_h_
#define _tcds_freqgen_FreqGenInfoSpaceUpdater_h_

#include "tcds/utils/HwInfoSpaceUpdaterBase.h"

namespace tcds {
  namespace utils {
    class InfoSpaceHandler;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace freqgen {

    class SCPIDeviceFreqGen;

    class FreqGenInfoSpaceUpdater : public tcds::utils::HwInfoSpaceUpdaterBase
    {

    public:
      FreqGenInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                              tcds::freqgen::SCPIDeviceFreqGen const& hw);

    protected:
      virtual void updateInfoSpaceImpl(tcds::utils::InfoSpaceHandler* const infoSpaceHandler);

    private:
      tcds::freqgen::SCPIDeviceFreqGen const& getHw() const;
    };

  } // namespace freqgen
} // namespace tcds

#endif // _tcds_pi_FreqGenInfoSpaceUpdater_h_
