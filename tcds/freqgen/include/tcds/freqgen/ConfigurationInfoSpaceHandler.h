#ifndef _tcds_freqgen_ConfigurationInfoSpaceHandler_h_
#define _tcds_freqgen_ConfigurationInfoSpaceHandler_h_

#include <string>

#include "tcds/hwutilsscpi/ConfigurationInfoSpaceHandlerSCPI.h"

namespace xdaq {
  class Application;
}

namespace tcds {
  namespace utils {
    class Monitor;
    class WebServer;
  }
}

namespace tcds {
  namespace freqgen {

    class ConfigurationInfoSpaceHandler :
      public tcds::hwutilsscpi::ConfigurationInfoSpaceHandlerSCPI
    {

    public:
      ConfigurationInfoSpaceHandler(xdaq::Application& xdaqApp);

    protected:
      virtual void registerItemSetsWithMonitor(tcds::utils::Monitor& monitor);
      virtual void registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                 tcds::utils::Monitor& monitor,
                                                 std::string const& forceTabName="");

    };

  } // namespace freqgen
} // namespace tcds

#endif // _tcds_freqgen_ConfigurationInfoSpaceHandler_h_
