#ifndef _tcds_freqgen_FreqGen_h_
#define _tcds_freqgen_FreqGen_h_

#include <memory>

#include "xdaq/Application.h"

#include "tcds/freqgen/SCPIDeviceFreqGen.h"
#include "tcds/utils/XDAQAppWithFSMAutomatic.h"

namespace xdaq {
  class ApplicationStub;
}

namespace tcds {
  namespace hwutilsscpi {
    class HwIDInfoSpaceHandlerSCPI;
    class HwIDInfoSpaceUpdaterSCPI;
    class HwStatusInfoSpaceHandlerSCPI;
    class HwStatusInfoSpaceUpdaterSCPI;
  }
}

namespace tcds {
  namespace freqgen {

    class FreqGenInfoSpaceHandler;
    class FreqGenInfoSpaceUpdater;

    class FreqGen : public tcds::utils::XDAQAppWithFSMAutomatic
    {

    public:
      XDAQ_INSTANTIATOR();

      FreqGen(xdaq::ApplicationStub* stub);
      virtual ~FreqGen();

    protected:
      virtual void setupInfoSpaces();

      virtual SCPIDeviceFreqGen& getHw() const;

      virtual void hwConnectImpl();
      virtual void hwReleaseImpl();
      virtual void hwConfigureImpl();

    private:
      // Various InfoSpaces and their InfoSpaceUpdaters.
      std::unique_ptr<tcds::freqgen::FreqGenInfoSpaceUpdater> freqgenInfoSpaceUpdaterP_;
      std::unique_ptr<tcds::freqgen::FreqGenInfoSpaceHandler> freqgenInfoSpaceP_;
      std::unique_ptr<tcds::hwutilsscpi::HwIDInfoSpaceUpdaterSCPI> hwIDInfoSpaceUpdaterP_;
      std::unique_ptr<tcds::hwutilsscpi::HwIDInfoSpaceHandlerSCPI> hwIDInfoSpaceP_;
      std::unique_ptr<tcds::hwutilsscpi::HwStatusInfoSpaceUpdaterSCPI> hwStatusInfoSpaceUpdaterP_;
      std::unique_ptr<tcds::hwutilsscpi::HwStatusInfoSpaceHandlerSCPI> hwStatusInfoSpaceP_;

    };

  } // namespace freqgen
} // namespace tcds

#endif // _tcds_freqgen_FreqGen_h_
