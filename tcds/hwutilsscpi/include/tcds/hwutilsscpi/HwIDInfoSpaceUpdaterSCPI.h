#ifndef _tcds_hwutilsscpi_HwIDInfoSpaceUpdaterSCPI_h_
#define _tcds_hwutilsscpi_HwIDInfoSpaceUpdaterSCPI_h_

#include "tcds/utils/HwInfoSpaceUpdaterBase.h"

namespace tcds {
  namespace hwlayerscpi {
    class SCPIDeviceBase;
  }
}

namespace tcds {
  namespace utils {
    class InfoSpaceHandler;
    class InfoSpaceItem;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace hwutilsscpi {

    class HwIDInfoSpaceUpdaterSCPI : public tcds::utils::HwInfoSpaceUpdaterBase
    {

    public:
      HwIDInfoSpaceUpdaterSCPI(tcds::utils::XDAQAppBase& xdaqApp,
                               tcds::hwlayerscpi::SCPIDeviceBase const& hw);
      virtual ~HwIDInfoSpaceUpdaterSCPI();

      virtual bool updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                       tcds::utils::InfoSpaceHandler* const infoSpaceHandler);

    private:
      tcds::hwlayerscpi::SCPIDeviceBase const& getHw() const;

    };

  } // namespace hwutilsscpi
} // namespace tcds

#endif // _tcds_hwutilsscpi_HwIDInfoSpaceUpdaterSCPI_h_
