#ifndef _tcds_hwutilsscpi_ConfigurationInfoSpaceHandlerSCPI_h_
#define _tcds_hwutilsscpi_ConfigurationInfoSpaceHandlerSCPI_h_

#include <string>

#include "tcds/utils/ConfigurationInfoSpaceHandler.h"

namespace xdaq {
  class Application;
}

namespace tcds {
  namespace utils {
    class Monitor;
    class WebServer;
  }
}

namespace tcds {
  namespace hwutilsscpi {

    class ConfigurationInfoSpaceHandlerSCPI :
      public tcds::utils::ConfigurationInfoSpaceHandler
    {

    public:
      ConfigurationInfoSpaceHandlerSCPI(xdaq::Application& xdaqApp);

    protected:
      virtual void registerItemSetsWithMonitor(tcds::utils::Monitor& monitor);
      virtual void registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                 tcds::utils::Monitor& monitor,
                                                 std::string const& forceTabName="");

    };

  } // namespace hwutilsscpi
} // namespace tcds

#endif // _tcds_hwutilsscpi_ConfigurationInfoSpaceHandlerSCPI_h_
