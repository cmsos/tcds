#ifndef _tcds_hwutilsscpi_HwIDInfoSpaceHandlerSCPI_h_
#define _tcds_hwutilsscpi_HwIDInfoSpaceHandlerSCPI_h_

#include <string>

#include "tcds/utils/InfoSpaceHandler.h"

namespace xdaq {
  class Application;
}

namespace tcds {
  namespace utils {
    class InfoSpaceUpdater;
    class Monitor;
    class WebServer;
  }
}

namespace tcds {
  namespace hwutilsscpi {

    class HwIDInfoSpaceHandlerSCPI : public tcds::utils::InfoSpaceHandler
    {

    public:
      HwIDInfoSpaceHandlerSCPI(xdaq::Application& xdaqApp,
                               tcds::utils::InfoSpaceUpdater* updater);
      virtual ~HwIDInfoSpaceHandlerSCPI();

    protected:
      virtual void registerItemSetsWithMonitor(tcds::utils::Monitor& monitor);
      virtual void registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                 tcds::utils::Monitor& monitor,
                                                 std::string const& forceTabName="");

    };

  } // namespace hwutilsscpi
} // namespace tcds

#endif // _tcds_hwutilsscpi_HwIDInfoSpaceHandlerSCPI_h_
