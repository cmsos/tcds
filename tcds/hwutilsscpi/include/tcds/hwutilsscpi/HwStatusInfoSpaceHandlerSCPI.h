#ifndef _tcds_hwutilsscpi_HwStatusInfoSpaceHandlerSCPI_h_
#define _tcds_hwutilsscpi_HwStatusInfoSpaceHandlerSCPI_h_

#include <string>

#include "tcds/utils/InfoSpaceHandler.h"

namespace xdaq {
  class Application;
}

namespace tcds {
  namespace utils {
    class InfoSpaceUpdater;
    class Monitor;
    class WebServer;
  }
}

namespace tcds {
  namespace hwutilsscpi {

    class HwStatusInfoSpaceHandlerSCPI : public tcds::utils::InfoSpaceHandler
    {

    public:
      HwStatusInfoSpaceHandlerSCPI(xdaq::Application& xdaqApp,
                                   tcds::utils::InfoSpaceUpdater* updater);
      virtual ~HwStatusInfoSpaceHandlerSCPI();

    protected:
      virtual void registerItemSetsWithMonitor(tcds::utils::Monitor& monitor);
      virtual void registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                 tcds::utils::Monitor& monitor,
                                                 std::string const& forceTabName="");

      virtual std::string formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const;

    };

  } // namespace hwutilsscpi
} // namespace tcds

#endif // _tcds_hwutilsscpi_HwStatusInfoSpaceHandler_h_
