#ifndef _tcds_hwutilsscpi_Utils_h_
#define _tcds_hwutilsscpi_Utils_h_

namespace tcds {
  namespace hwlayerscpi {
    class SCPIDeviceBase;
  }
}

namespace tcds {
  namespace utils {
    class ConfigurationInfoSpaceHandler;
  }
}

namespace tcds {
  namespace hwutilsscpi {

    void scpiDeviceHwConnectImpl(tcds::utils::ConfigurationInfoSpaceHandler const& cfgInfoSpace,
                                 tcds::hwlayerscpi::SCPIDeviceBase& hw);

  } // namespace hwutilsscpi
} // namespace tcds

#endif // _tcds_hwutilsscpi_Utils_h_
