#include "tcds/hwutilsscpi/version.h"

#include "config/version.h"
#include "hyperdaq/version.h"
#include "toolbox/version.h"
#include "xcept/version.h"
#include "xdaq/version.h"
#include "xdata/version.h"
#include "xgi/version.h"

#include "tcds/hwlayer/version.h"
#include "tcds/hwlayerscpi/version.h"
#include "tcds/utils/version.h"

GETPACKAGEINFO(tcds::hwutilsscpi)

void
tcds::hwutilsscpi::checkPackageDependencies()
{
  CHECKDEPENDENCY(config);
  CHECKDEPENDENCY(hyperdaq);
  CHECKDEPENDENCY(toolbox);
  CHECKDEPENDENCY(xcept);
  CHECKDEPENDENCY(xdaq);
  CHECKDEPENDENCY(xdata);
  CHECKDEPENDENCY(xgi);

  CHECKDEPENDENCY(tcds::hwlayer);
  CHECKDEPENDENCY(tcds::hwlayerscpi);
  CHECKDEPENDENCY(tcds::utils);
}

std::set<std::string, std::less<std::string> >
tcds::hwutilsscpi::getPackageDependencies()
{
  std::set<std::string, std::less<std::string> > dependencies;

  ADDDEPENDENCY(dependencies, config);
  ADDDEPENDENCY(dependencies, hyperdaq);
  ADDDEPENDENCY(dependencies, toolbox);
  ADDDEPENDENCY(dependencies, xcept);
  ADDDEPENDENCY(dependencies, xdaq);
  ADDDEPENDENCY(dependencies, xdata);
  ADDDEPENDENCY(dependencies, xgi);

  ADDDEPENDENCY(dependencies, tcds::hwlayer);
  ADDDEPENDENCY(dependencies, tcds::hwlayerscpi);
  ADDDEPENDENCY(dependencies, tcds::utils);

  return dependencies;
}
