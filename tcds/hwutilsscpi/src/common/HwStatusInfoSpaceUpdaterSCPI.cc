#include "tcds/hwutilsscpi/HwStatusInfoSpaceUpdaterSCPI.h"

#include <stdint.h>
#include <string>

#include "tcds/hwlayerscpi/SCPIDeviceBase.h"
#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/InfoSpaceItem.h"

tcds::hwutilsscpi::HwStatusInfoSpaceUpdaterSCPI::HwStatusInfoSpaceUpdaterSCPI(tcds::utils::XDAQAppBase& xdaqApp,
                                                                              tcds::hwlayerscpi::SCPIDeviceBase const& hw) :
  tcds::utils::HwInfoSpaceUpdaterBase(xdaqApp, hw)
{
}

tcds::hwutilsscpi::HwStatusInfoSpaceUpdaterSCPI::~HwStatusInfoSpaceUpdaterSCPI()
{
}

bool
tcds::hwutilsscpi::HwStatusInfoSpaceUpdaterSCPI::updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                                                     tcds::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  bool updated = false;
  tcds::hwlayerscpi::SCPIDeviceBase const& hw = getHw();
  if (hw.isHwConnected())
    {
      std::string name = item.name();
      tcds::utils::InfoSpaceItem::UpdateType updateType = item.updateType();
      if (updateType == tcds::utils::InfoSpaceItem::PROCESS)
        {
          // The 'PROCESS' update type means that there is something
          // special to the variable. Figure out what to do based on
          // the variable name.
          if (name == "status_byte")
            {
              uint32_t const newVal = hw.readStatusByte();
              infoSpaceHandler->setUInt32(name, newVal);
              updated = true;
            }
          else if (name == "latest_error")
            {
              std::string const newVal = hw.readLatestError();
              infoSpaceHandler->setString(name, newVal);
              updated = true;
            }
          else if (name == "ref_clk_src")
            {
              uint32_t const newVal = hw.getReferenceSource();
              infoSpaceHandler->setUInt32(name, newVal);
              updated = true;
            }
        }
      if (!updated)
        {
          updated = tcds::utils::HwInfoSpaceUpdaterBase::updateInfoSpaceItem(item, infoSpaceHandler);
        }
    }

  if (updated)
    {
      item.setValid();
    }
  else
    {
      item.setInvalid();
    }

  return updated;
}

tcds::hwlayerscpi::SCPIDeviceBase const&
tcds::hwutilsscpi::HwStatusInfoSpaceUpdaterSCPI::getHw() const
{
  return dynamic_cast<tcds::hwlayerscpi::SCPIDeviceBase const&>(tcds::utils::HwInfoSpaceUpdaterBase::getHw());
}
