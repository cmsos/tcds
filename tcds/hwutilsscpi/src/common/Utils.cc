#include "tcds/hwutilsscpi/Utils.h"

#include <stdint.h>
#include <string>

#include "tcds/hwlayerscpi/SCPIDeviceBase.h"
#include "tcds/utils/ConfigurationInfoSpaceHandler.h"

void
tcds::hwutilsscpi::scpiDeviceHwConnectImpl(tcds::utils::ConfigurationInfoSpaceHandler const& cfgInfoSpace,
                                           tcds::hwlayerscpi::SCPIDeviceBase& hw)
{
  std::string const hostName = cfgInfoSpace.getString("hostName");
  uint32_t const portNumber = cfgInfoSpace.getUInt32("portNumber");

  hw.hwConnect(hostName, portNumber);
  hw.reset();
  hw.clearStatus();
}
