#include "tcds/hwutilsscpi/HwStatusInfoSpaceHandlerSCPI.h"

#include <stdint.h>

#include "tcds/hwlayerscpi/Definitions.h"
#include "tcds/hwlayerscpi/Utils.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/Utils.h"
#include "tcds/utils/WebServer.h"

tcds::hwutilsscpi::HwStatusInfoSpaceHandlerSCPI::HwStatusInfoSpaceHandlerSCPI(xdaq::Application& xdaqApp,
                                                                              tcds::utils::InfoSpaceUpdater* updater) :
  InfoSpaceHandler(xdaqApp, "tcds-hw-status", updater)
{
  // Overall status byte.
  createUInt32("status_byte",
               0,
               "",
               tcds::utils::InfoSpaceItem::PROCESS);

  // Latest system error.
  createString("latest_error",
               "",
               "",
               tcds::utils::InfoSpaceItem::PROCESS);

  // Reference clock source.
  createUInt32("ref_clk_src",
               tcds::definitions::REF_SOURCE_UNKNOWN,
               "",
               tcds::utils::InfoSpaceItem::PROCESS);
}

tcds::hwutilsscpi::HwStatusInfoSpaceHandlerSCPI::~HwStatusInfoSpaceHandlerSCPI()
{
}

void
tcds::hwutilsscpi::HwStatusInfoSpaceHandlerSCPI::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  std::string itemSetName = "itemset-hw-status";
  monitor.newItemSet(itemSetName);

  // Overall status byte.
  monitor.addItem(itemSetName,
                  "status_byte",
                  "Status byte",
                  this,
                  "This is the result of the '*STB?' command.");

  // Latest system error.
  monitor.addItem(itemSetName,
                  "latest_error",
                  "Latest system error",
                  this,
                  "This is the result of the 'SYSTEM:ERROR?' command.");

  // Reference clock source.
  monitor.addItem(itemSetName,
                  "ref_clk_src",
                  "Reference clock",
                  this,
                  "Source of the reference clock signal (if applicable)");
}

void
tcds::hwutilsscpi::HwStatusInfoSpaceHandlerSCPI::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                               tcds::utils::Monitor& monitor,
                                                                               std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "Hardware status" : forceTabName;

  webServer.registerTab(tabName,
                        "Hardware information",
                        3);

  webServer.registerTable("Status",
                          "Status info",
                          monitor,
                          "itemset-hw-status",
                          tabName);
}

std::string
tcds::hwutilsscpi::HwStatusInfoSpaceHandlerSCPI::formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const
{
  std::string res = tcds::utils::escapeAsJSONString(tcds::utils::InfoSpaceHandler::kInvalidItemString);
  if (item->isValid())
    {
      std::string name = item->name();
      if (name == "status_byte")
        {
          uint32_t const value = getUInt32(name);
          res = tcds::utils::escapeAsJSONString(tcds::hwlayerscpi::statusByteToString(value));
        }
      else if (name == "ref_clk_src")
        {
          uint32_t const value = getUInt32(name);
          tcds::definitions::REF_SOURCE const valueEnum =
            static_cast<tcds::definitions::REF_SOURCE>(value);
          res = tcds::utils::escapeAsJSONString(tcds::hwlayerscpi::refClkSrcToString(valueEnum));
        }
      else
        {
          // For everything else simply call the generic formatter.
          res = InfoSpaceHandler::formatItem(item);
        }
    }
  return res;
}
