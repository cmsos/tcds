#include "tcds/hwutilsscpi/HwIDInfoSpaceUpdaterSCPI.h"

#include <string>

#include "tcds/hwlayerscpi/SCPIDeviceBase.h"
#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/InfoSpaceItem.h"

tcds::hwutilsscpi::HwIDInfoSpaceUpdaterSCPI::HwIDInfoSpaceUpdaterSCPI(tcds::utils::XDAQAppBase& xdaqApp,
                                                                      tcds::hwlayerscpi::SCPIDeviceBase const& hw) :
  tcds::utils::HwInfoSpaceUpdaterBase(xdaqApp, hw)
{
}

tcds::hwutilsscpi::HwIDInfoSpaceUpdaterSCPI::~HwIDInfoSpaceUpdaterSCPI()
{
}

bool
tcds::hwutilsscpi::HwIDInfoSpaceUpdaterSCPI::updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                                                 tcds::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  bool updated = false;
  tcds::hwlayerscpi::SCPIDeviceBase const& hw = getHw();
  if (hw.isHwConnected())
    {
      std::string name = item.name();
      tcds::utils::InfoSpaceItem::UpdateType updateType = item.updateType();
      if (updateType == tcds::utils::InfoSpaceItem::PROCESS)
        {
          // The 'PROCESS' update type means that there is something
          // special to the variable. Figure out what to do based on
          // the variable name.
          if (name == "device_id")
            {
              std::string const newVal = hw.readDeviceID();
              infoSpaceHandler->setString(name, newVal);
              updated = true;
            }
          else if (name == "device_options")
            {
              std::string const newVal = hw.readDeviceOptions();
              infoSpaceHandler->setString(name, newVal);
              updated = true;
            }
        }
      if (!updated)
        {
          tcds::utils::HwInfoSpaceUpdaterBase::updateInfoSpaceItem(item, infoSpaceHandler);
        }
    }

  if (updated)
    {
      item.setValid();
    }
  else
    {
      item.setInvalid();
    }

  return updated;
}

tcds::hwlayerscpi::SCPIDeviceBase const&
tcds::hwutilsscpi::HwIDInfoSpaceUpdaterSCPI::getHw() const
{
  return dynamic_cast<tcds::hwlayerscpi::SCPIDeviceBase const&>(tcds::utils::HwInfoSpaceUpdaterBase::getHw());
}
