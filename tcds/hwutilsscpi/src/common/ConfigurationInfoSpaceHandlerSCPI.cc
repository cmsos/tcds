#include "tcds/hwutilsscpi/ConfigurationInfoSpaceHandlerSCPI.h"

#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/WebServer.h"

tcds::hwutilsscpi::ConfigurationInfoSpaceHandlerSCPI::ConfigurationInfoSpaceHandlerSCPI(xdaq::Application& xdaqApp) :
  tcds::utils::ConfigurationInfoSpaceHandler(xdaqApp)
{
  // Where to find the SCPI device (via telnet).
  createString("hostName",
               "freqmeter1.cms",
               "",
               tcds::utils::InfoSpaceItem::NOUPDATE,
               true);
  createUInt32("portNumber",
               4001,
               "",
               tcds::utils::InfoSpaceItem::NOUPDATE,
               true);
}

void
tcds::hwutilsscpi::ConfigurationInfoSpaceHandlerSCPI::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  std::string itemSetName = "Application configuration";
  monitor.newItemSet(itemSetName);
  monitor.addItem(itemSetName,
                  "monitoringInterval",
                  "Monitoring update interval",
                  this);
  monitor.addItem(itemSetName,
                  "hostName",
                  "Host name of the SCPI device",
                  this);
  monitor.addItem(itemSetName,
                  "portNumber",
                  "Port number of the SCPI device",
                  this,
                  " NOTE: This should be the socket port number,"
                  " and not the telnet port number.");
}

void
tcds::hwutilsscpi::ConfigurationInfoSpaceHandlerSCPI::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                                    tcds::utils::Monitor& monitor,
                                                                                    std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "Configuration" : forceTabName;

  webServer.registerTab(tabName,
                        "Configuration parameters",
                        2);
  webServer.registerTable("Application configuration",
                          "Application configuration parameters",
                          monitor,
                          "Application configuration",
                          tabName);
}
