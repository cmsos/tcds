#include "tcds/hwutilsscpi/HwIDInfoSpaceHandlerSCPI.h"

#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/WebServer.h"

tcds::hwutilsscpi::HwIDInfoSpaceHandlerSCPI::HwIDInfoSpaceHandlerSCPI(xdaq::Application& xdaqApp,
                                                                      tcds::utils::InfoSpaceUpdater* updater) :
  InfoSpaceHandler(xdaqApp, "tcds-hw-id", updater)
{
  // Device ID.
  createString("device_id",
               tcds::utils::InfoSpaceHandler::kUninitializedString,
               "",
               tcds::utils::InfoSpaceItem::PROCESS);
  // Device options.
  createString("device_options",
               tcds::utils::InfoSpaceHandler::kUninitializedString,
               "",
               tcds::utils::InfoSpaceItem::PROCESS);
}

tcds::hwutilsscpi::HwIDInfoSpaceHandlerSCPI::~HwIDInfoSpaceHandlerSCPI()
{
}

void
tcds::hwutilsscpi::HwIDInfoSpaceHandlerSCPI::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  std::string itemSetName = "itemset-hw-id";
  monitor.newItemSet(itemSetName);

  // Device ID.
  monitor.addItem(itemSetName,
                  "device_id",
                  "Device ID",
                  this,
                  "This is the result of the '*IDN?' command.");
  // Device options.
  monitor.addItem(itemSetName,
                  "device_options",
                  "Device options",
                  this,
                  "This is the result of the '*OPT?' command.");
}

void
tcds::hwutilsscpi::HwIDInfoSpaceHandlerSCPI::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                           tcds::utils::Monitor& monitor,
                                                                           std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "Hardware ID" : forceTabName;

  webServer.registerTab(tabName,
                        "Hardware identifiers",
                        3);
  webServer.registerTable("Hardware identifiers",
                          "Hardware identifiers",
                          monitor,
                          "itemset-hw-id",
                          tabName);
}
