#ifndef _tcds_hwutilsvme_ConfigurationInfoSpaceHandlerVME_h_
#define _tcds_hwutilsvme_ConfigurationInfoSpaceHandlerVME_h_

#include <string>

#include "tcds/utils/ConfigurationInfoSpaceHandler.h"

namespace xdaq {
  class Application;
}

namespace tcds {
  namespace utils {
    class Monitor;
  }
}

namespace tcds {
  namespace hwutilsvme {

    class ConfigurationInfoSpaceHandlerVME :
      public tcds::utils::ConfigurationInfoSpaceHandler
    {

    public:
      ConfigurationInfoSpaceHandlerVME(xdaq::Application& xdaqApp,
                                       std::string const& name="tcds-application-config-vme");

    protected:
      virtual void registerItemSetsWithMonitor(tcds::utils::Monitor& monitor);

    };

  } // namespace hwutilsvme
} // namespace tcds

#endif // _tcds_hwutilsvme_ConfigurationInfoSpaceHandlerVME_h_
