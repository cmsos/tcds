#ifndef _tcds_hwutilsvme_HwIDInfoSpaceHandlerVME_h_
#define _tcds_hwutilsvme_HwIDInfoSpaceHandlerVME_h_

#include <string>

#include "tcds/utils/InfoSpaceHandler.h"

namespace xdaq {
  class Application;
}

namespace tcds {
  namespace utils {
    class InfoSpaceUpdater;
    class Monitor;
    class WebServer;
  }
}

namespace tcds {
  namespace hwutilsvme {

    class HwIDInfoSpaceHandlerVME : public tcds::utils::InfoSpaceHandler
    {

    public:
      HwIDInfoSpaceHandlerVME(xdaq::Application& xdaqApp,
                              tcds::utils::InfoSpaceUpdater* updater);
      virtual ~HwIDInfoSpaceHandlerVME();

    protected:
      virtual void registerItemSetsWithMonitor(tcds::utils::Monitor& monitor);
      virtual void registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                 tcds::utils::Monitor& monitor,
                                                 std::string const& forceTabName="");

    };

  } // namespace hwutilsvme
} // namespace tcds

#endif // _tcds_hwutilsvme_HwIDInfoSpaceHandlerVME_h_
