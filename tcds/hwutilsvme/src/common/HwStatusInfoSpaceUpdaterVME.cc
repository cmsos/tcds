#include "tcds/hwutilsvme/HwStatusInfoSpaceUpdaterVME.h"

#include "tcds/hwlayervme/VMEDeviceBase.h"
#include "tcds/utils/InfoSpaceItem.h"

tcds::hwutilsvme::HwStatusInfoSpaceUpdaterVME::HwStatusInfoSpaceUpdaterVME(tcds::utils::XDAQAppBase& xdaqApp,
                                                                           tcds::hwlayervme::VMEDeviceBase const& hw) :
  tcds::utils::HwInfoSpaceUpdaterBase(xdaqApp, hw)
{
}

tcds::hwutilsvme::HwStatusInfoSpaceUpdaterVME::~HwStatusInfoSpaceUpdaterVME()
{
}

bool
tcds::hwutilsvme::HwStatusInfoSpaceUpdaterVME::updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                                                   tcds::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  bool updated = false;
  tcds::hwlayervme::VMEDeviceBase const& hw = getHw();
  if (hw.isReadyForUse())
    {
      // std::string name = item.name();
      // tcds::utils::InfoSpaceItem::UpdateType updateType = item.updateType();
      // if (updateType == tcds::utils::InfoSpaceItem::PROCESS)
      //   {
      //     // The 'PROCESS' update type means that there is something
      //     // special to the variable. Figure out what to do based on the
      //     // variable name.
      //     if (name == "curr_select_bcmain")
      //       {
      //         uint32_t const newVal = hw.getCurrentBCMainSource();
      //         infoSpaceHandler->setUInt32(name, newVal);
      //         updated = true;
      //       }
      //     else if (name == "curr_select_orbmain")
      //       {
      //         uint32_t const newVal = hw.getCurrentOrbitMainSource();
      //         infoSpaceHandler->setUInt32(name, newVal);
      //         updated = true;
      //       }
      //     else if (name == "orbit_status_orbmain")
      //       {
      //         uint32_t const newVal = hw.getOrbitStatus(tcds::definitions::ORB_SIGNAL_ORBMAIN);
      //         infoSpaceHandler->setUInt32(name, newVal);
      //         updated = true;
      //       }
      //   }
      if (!updated)
        {
          updated = tcds::utils::HwInfoSpaceUpdaterBase::updateInfoSpaceItem(item, infoSpaceHandler);
        }
    }

  if (updated)
    {
      item.setValid();
    }
  else
    {
      item.setInvalid();
    }

  return updated;
}

tcds::hwlayervme::VMEDeviceBase const&
tcds::hwutilsvme::HwStatusInfoSpaceUpdaterVME::getHw() const
{
  return dynamic_cast<tcds::hwlayervme::VMEDeviceBase const&>(tcds::utils::HwInfoSpaceUpdaterBase::getHw());
}
