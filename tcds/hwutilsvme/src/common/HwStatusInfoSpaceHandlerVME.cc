#include "tcds/hwutilsvme/HwStatusInfoSpaceHandlerVME.h"

#include "toolbox/string.h"

#include "tcds/utils/Monitor.h"
#include "tcds/utils/WebServer.h"

tcds::hwutilsvme::HwStatusInfoSpaceHandlerVME::HwStatusInfoSpaceHandlerVME(xdaq::Application& xdaqApp,
                                                                           tcds::utils::InfoSpaceUpdater* updater) :
  InfoSpaceHandler(xdaqApp, "tcds-hw-id", updater)
{
  // Global status info.
  createBool("status_global_fpga_bstr1_ok", false, "true/false");
  createBool("status_global_fpga_bstr2_ok", false, "true/false");
  createBool("status_global_epld_muxout_ok", false, "true/false");
  createBool("status_global_local_40mhz_ok", false, "true/false");
  createBool("status_global_ttc1_mezz_absent", false, "false/true");
  createBool("status_global_ttc2_mezz_absent", false, "false/true");
  createBool("status_global_3v3_reg_ok", false, "true/false");
  createBool("status_global_2v5_reg_ok", false, "true/false");

  // BSTR1/2 status info.
  for (int i = 1; i != 3; ++i)
    {
      createBool(toolbox::toString("status_bstr%d_ttc_in_ready", i), false, "true/false");
      createBool(toolbox::toString("status_bstr%d_local_40mhz_ok", i), false, "true/false");
      createBool(toolbox::toString("status_bstr%d_local_orbit_ok", i), false, "true/false");
      createBool(toolbox::toString("status_bstr%d_ttc_serial_b_ok", i), false, "true/false");
      createBool(toolbox::toString("status_bstr%d_ttc_frame_error", i), false, "true/false");
      createBool(toolbox::toString("status_bstr%d_message_error", i), false, "true/false");
      createBool(toolbox::toString("status_bstr%d_irq_set_from_mask", i), false, "true/false");
      createBool(toolbox::toString("status_bstr%d_message_dump_done", i), false, "true/false");

      createUInt32(toolbox::toString("bstr%d_ttc_single_bit_err_cnt", i));
      createUInt32(toolbox::toString("bstr%d_ttc_double_bit_err_cnt", i));
      createUInt32(toolbox::toString("bstr%d_ttc_ready_err_cnt", i));
    }
}

tcds::hwutilsvme::HwStatusInfoSpaceHandlerVME::~HwStatusInfoSpaceHandlerVME()
{
}

void
tcds::hwutilsvme::HwStatusInfoSpaceHandlerVME::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  // Global status info.
  std::string itemSetName = "itemset-hw-status-global";
  monitor.newItemSet(itemSetName);
  monitor.addItem(itemSetName,
                  "status_global_fpga_bstr1_ok",
                  "BSTR1 FPGA ok",
                  this);
  monitor.addItem(itemSetName,
                  "status_global_fpga_bstr2_ok",
                  "BSTR2 FPGA ok",
                  this);
  monitor.addItem(itemSetName,
                  "status_global_epld_muxout_ok",
                  "EPLD output MUX ok",
                  this);
  monitor.addItem(itemSetName,
                  "status_global_local_40mhz_ok",
                  "40 MHz clock ok",
                  this);
  monitor.addItem(itemSetName,
                  "status_global_ttc1_mezz_absent",
                  "TTC1 mezzanine present",
                  this);
  monitor.addItem(itemSetName,
                  "status_global_ttc2_mezz_absent",
                  "TTC2 mezzanine present",
                  this);
  monitor.addItem(itemSetName,
                  "status_global_3v3_reg_ok",
                  "Regulater 3.3 V ok",
                  this);
  monitor.addItem(itemSetName,
                  "status_global_2v5_reg_ok",
                  "Regulater 2.5 V ok",
                  this);

  //----------

  // BSTR1/2 status info.
  for (int i = 1; i != 3; ++i)
    {
      itemSetName = toolbox::toString("itemset-hw-status-bstr%d", i);
      monitor.newItemSet(itemSetName);
      monitor.addItem(itemSetName,
                      toolbox::toString("status_bstr%d_ttc_in_ready", i),
                      "TTC input signal ok",
                      this);
      monitor.addItem(itemSetName,
                      toolbox::toString("status_bstr%d_local_40mhz_ok", i),
                      "40 MHz clock ok",
                      this);
      monitor.addItem(itemSetName,
                      toolbox::toString("status_bstr%d_local_orbit_ok", i),
                      "Orbit signal ok",
                      this);
      monitor.addItem(itemSetName,
                      toolbox::toString("status_bstr%d_ttc_serial_b_ok", i),
                      "Serial-B signal ok",
                      this);
      monitor.addItem(itemSetName,
                      toolbox::toString("status_bstr%d_ttc_frame_error", i),
                      "TTC frame error detected",
                      this);
      monitor.addItem(itemSetName,
                      toolbox::toString("status_bstr%d_message_error", i),
                      "Message error detected",
                      this);
      monitor.addItem(itemSetName,
                      toolbox::toString("status_bstr%d_irq_set_from_mask", i),
                      "IRQ set from mask",
                      this);
      monitor.addItem(itemSetName,
                      toolbox::toString("status_bstr%d_message_dump_done", i),
                      "Message dump finished",
                      this);
      monitor.addItem(itemSetName,
                      toolbox::toString("bstr%d_ttc_single_bit_err_cnt", i),
                      "TTC single-bit error count",
                      this);
      monitor.addItem(itemSetName,
                      toolbox::toString("bstr%d_ttc_double_bit_err_cnt", i),
                      "TTC double-bit error count",
                      this);
      monitor.addItem(itemSetName,
                      toolbox::toString("bstr%d_ttc_ready_err_cnt", i),
                      "TTC-ready error count",
                      this);
    }
}

void
tcds::hwutilsvme::HwStatusInfoSpaceHandlerVME::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                             tcds::utils::Monitor& monitor,
                                                                             std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "Hardware status" : forceTabName;

  webServer.registerTab(tabName,
                        "Hardware information",
                        3);

  // Global status info.
  webServer.registerTable("Global",
                          "Global status info",
                          monitor,
                          "itemset-hw-status-global",
                          tabName);

  // BSTR1/2 status info.
  for (int i = 1; i != 3; ++i)
    {
      webServer.registerTable(toolbox::toString("BSTR%d", i),
                              toolbox::toString("BSTR%d status info", i),
                              monitor,
                              toolbox::toString("itemset-hw-status-bstr%d", i),
                              tabName);
    }
}

// std::string
// tcds::hwutilsvme::HwStatusInfoSpaceHandlerVME::formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const
// {
//   std::string res = tcds::utils::escapeAsJSONString(kInvalidItemString_);
//   if (item->isValid())
//     {
//       std::string name = item->name();
//       if (name == "ttcrx_status")
//         {
//           uint32_t const value = getUInt32(name);
//           tcds::definitions::TTCRX_STATUS const valueEnum =
//             static_cast<tcds::definitions::TTCRX_STATUS>(value);
//           res = tcds::utils::escapeAsJSONString(tcds::hwutilsvme::ttcrxStatusToString(valueEnum));
//         }
//       else if (name == "bst_beam_mode")
//         {
//           uint32_t const value = getUInt32(name);
//           tcds::definitions::BEAM_MODE const valueEnum =
//             static_cast<tcds::definitions::BEAM_MODE>(value);
//           res = tcds::utils::escapeAsJSONString(tcds::utils::beamModeToString(valueEnum));
//         }
//       else if (name == "curr_select_bcmain")
//         {
//           uint32_t const value = getUInt32(name);
//           tcds::definitions::BC_MAIN_SOURCE const valueEnum =
//             static_cast<tcds::definitions::BC_MAIN_SOURCE>(value);
//           res = tcds::utils::escapeAsJSONString(tcds::hwutilsvme::bcMainSourceToString(valueEnum));
//         }
//       else if (name == "curr_select_orbmain")
//         {
//           uint32_t const value = getUInt32(name);
//           tcds::definitions::ORB_MAIN_SOURCE const valueEnum =
//             static_cast<tcds::definitions::ORB_MAIN_SOURCE>(value);
//           res = tcds::utils::escapeAsJSONString(tcds::hwutilsvme::orbMainSourceToString(valueEnum));
//         }
//       else if (name == "qpll_lock_status_bcmain")
//         {
//           uint32_t const value = getUInt32(name);
//           tcds::definitions::QPLL_LOCK_STATUS const valueEnum =
//             static_cast<tcds::definitions::QPLL_LOCK_STATUS>(value);
//           res = tcds::utils::escapeAsJSONString(tcds::hwutilsvme::qpllLockStatusToString(valueEnum));
//         }
//       else if (name == "orbit_status_orbmain")
//         {
//           uint32_t const value = getUInt32(name);
//           tcds::definitions::ORBIT_LOCK_STATUS const valueEnum =
//             static_cast<tcds::definitions::ORBIT_LOCK_STATUS>(value);
//           res = tcds::utils::escapeAsJSONString(tcds::hwutilsvme::orbitLockStatusToString(valueEnum));
//         }
//       else
//         {
//           // For everything else simply call the generic formatter.
//           res = InfoSpaceHandler::formatItem(item);
//         }
//     }
//   return res;
// }
