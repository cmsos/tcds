#include "tcds/hwutilsvme/HwIDInfoSpaceHandlerVME.h"

#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/WebServer.h"

tcds::hwutilsvme::HwIDInfoSpaceHandlerVME::HwIDInfoSpaceHandlerVME(xdaq::Application& xdaqApp,
                                                                   tcds::utils::InfoSpaceUpdater* updater) :
  InfoSpaceHandler(xdaqApp, "tcds-hw-id", updater)
{
  // General board and firmware information.
  createString("board_id",
               tcds::utils::InfoSpaceHandler::kUninitializedString,
               "",
               tcds::utils::InfoSpaceItem::PROCESS);
  createString("firmware_version",
               tcds::utils::InfoSpaceHandler::kUninitializedString,
               "",
               tcds::utils::InfoSpaceItem::PROCESS);
}

tcds::hwutilsvme::HwIDInfoSpaceHandlerVME::~HwIDInfoSpaceHandlerVME()
{
}

void
tcds::hwutilsvme::HwIDInfoSpaceHandlerVME::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  // General board and firmware information.
  monitor.newItemSet("itemset-hw-id");
  monitor.addItem("itemset-hw-id",
                  "board_id",
                  "Board ID",
                  this);
  monitor.addItem("itemset-hw-id",
                  "firmware_version",
                  "Firmware version",
                  this);
}

void
tcds::hwutilsvme::HwIDInfoSpaceHandlerVME::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                         tcds::utils::Monitor& monitor,
                                                                         std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "Hardware ID" : forceTabName;

  webServer.registerTab(tabName,
                        "Hardware identifiers",
                        3);
  webServer.registerTable("Hardware identifiers",
                          "General hardware identifiers",
                          monitor,
                          "itemset-hw-id",
                          tabName);
}
