#include "tcds/hwutilsvme/ConfigurationInfoSpaceHandlerVME.h"

#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Monitor.h"

tcds::hwutilsvme::ConfigurationInfoSpaceHandlerVME::ConfigurationInfoSpaceHandlerVME(xdaq::Application& xdaqApp,
                                                                                     std::string const& name) :
  tcds::utils::ConfigurationInfoSpaceHandler(xdaqApp, name)
{
  // VME connection parameters.

  // The name of the address table file.
  createString("addressTableFile",
               "${XDAQ_ROOT}/etc/tcds/addresstables/unset.txt",
               "",
               tcds::utils::InfoSpaceItem::NOUPDATE,
               true);

  // The CAEN VME bridge unit and chain numbers.
  createUInt32("caenVMEUnitNumber",
               0,
               "",
               tcds::utils::InfoSpaceItem::NOUPDATE,
               true);
  createUInt32("caenVMEChainNumber",
               0,
               "",
               tcds::utils::InfoSpaceItem::NOUPDATE,
               true);

  // The slot number (in the VME crate).
  createUInt32("slotNumber",
               1,
               "",
               tcds::utils::InfoSpaceItem::NOUPDATE,
               true);
}

void
tcds::hwutilsvme::ConfigurationInfoSpaceHandlerVME::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  ConfigurationInfoSpaceHandler::registerItemSetsWithMonitor(monitor);

  // Application configuration parameters.
  std::string const itemSetName = "Application configuration";
  monitor.addItem(itemSetName,
                  "addressTableFile",
                  "Address table file",
                  this);
  monitor.addItem(itemSetName,
                  "caenVMEUnitNumber",
                  "CAEN VME bridge unit number",
                  this);
  monitor.addItem(itemSetName,
                  "caenVMEChainNumber",
                  "CAEN VME bridge chain number",
                  this);
  monitor.addItem(itemSetName,
                  "slotNumber",
                  "VME slot number",
                  this);
}
