#ifndef _tcds_apve_VMEDeviceRFRXD_h_
#define _tcds_apve_VMEDeviceRFRXD_h_

#include <stdint.h>
#include <string>

#include "tcds/hwlayervme/VMEDeviceBase.h"

namespace tcds {
  namespace rfrxd {

    /**
     * Implementation of the VME RFRXD functionality.
     */
    class VMEDeviceRFRXD : public tcds::hwlayervme::VMEDeviceBase
    {

    public:

      VMEDeviceRFRXD();
      virtual ~VMEDeviceRFRXD();

      virtual uint32_t getBaseAddress(uint32_t const slotNumber) const;

      bool isSignalPresent(int const channelNum) const;
      double readFrequency(int const channelNum) const;

    protected:
      virtual std::string readFirmwareVersionImpl() const;

    };

  } // namespace rfrxd
} // namespace tcds

#endif // _tcds_rfrxd_VMEDeviceRFRXD_h_
