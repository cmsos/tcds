#ifndef _tcds_rfrxd_Definitions_h_
#define _tcds_rfrxd_Definitions_h_

namespace tcds {
  namespace definitions {

    int const kChannelNumMin = 1;
    int const kChannelNumMax = 3;

  } // namespace definitions
} // namespace tcds

#endif // _tcds_rfrxd_Definitions_h_
