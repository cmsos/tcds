#ifndef _tcds_rfrxd_SignalsInfoSpaceHandler_h_
#define _tcds_rfrxd_SignalsInfoSpaceHandler_h_

#include <string>

#include "tcds/utils/InfoSpaceHandler.h"

namespace tcds {
  namespace utils {
    class InfoSpaceUpdater;
    class Monitor;
    class WebServer;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace rfrxd {

    class SignalsInfoSpaceHandler : public tcds::utils::InfoSpaceHandler
    {

    public:
      SignalsInfoSpaceHandler(tcds::utils::XDAQAppBase& xdaqApp,
                              tcds::utils::InfoSpaceUpdater* updater);
      virtual ~SignalsInfoSpaceHandler();

      tcds::utils::XDAQAppBase& getOwnerApplication() const;

    protected:
      virtual void registerItemSetsWithMonitor(tcds::utils::Monitor& monitor);
      virtual void registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                 tcds::utils::Monitor& monitor,
                                                 std::string const& forceTabName="");

      virtual std::string formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const;

    };

  } // namespace rfrxd
} // namespace tcds

#endif // _tcds_rfrxd_SignalsInfoSpaceHandler_h_
