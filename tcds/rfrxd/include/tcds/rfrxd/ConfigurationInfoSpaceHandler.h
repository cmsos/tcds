#ifndef _tcds_rfrxd_ConfigurationInfoSpaceHandler_h_
#define _tcds_rfrxd_ConfigurationInfoSpaceHandler_h_

#include "tcds/hwutilsvme/ConfigurationInfoSpaceHandlerVME.h"

namespace xdaq {
  class Application;
}

namespace tcds {
  namespace rfrxd {

    /**
     * This class really only adds the channel input/output labels to
     * the 'normal' ConfigurationInfoSpaceHandlerVME.
     */
    class ConfigurationInfoSpaceHandler :
      public tcds::hwutilsvme::ConfigurationInfoSpaceHandlerVME
    {

    public:
      ConfigurationInfoSpaceHandler(xdaq::Application& xdaqApp);
      virtual ~ConfigurationInfoSpaceHandler();

    };

  } // namespace rfrxd
} // namespace tcds

#endif // _tcds_rfrxd_ConfigurationInfoSpaceHandler_h_
