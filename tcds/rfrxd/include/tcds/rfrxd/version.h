#ifndef _tcds_rfrxd_version_h_
#define _tcds_rfrxd_version_h_

#include <functional>
#include <set>
#include <string>

#include "config/PackageInfo.h"

// !!! Edit these lines to reflect the latest package version. !!!
#define TCDS_RFRXD_VERSION_MAJOR 4
#define TCDS_RFRXD_VERSION_MINOR 15
#define TCDS_RFRXD_VERSION_PATCH 0

// If any previous versions available:
// #define TCDS_RFRXD_PREVIOUS_VERSIONS "3.8.0,3.8.1"
// else:
#undef TCDS_RFRXD_PREVIOUS_VERSIONS

//
// Template macros and boilerplate code.
//
#define TCDS_RFRXD_VERSION_CODE PACKAGE_VERSION_CODE(TCDS_RFRXD_VERSION_MAJOR,TCDS_RFRXD_VERSION_MINOR,TCDS_RFRXD_VERSION_PATCH)
#ifndef TCDS_RFRXD_PREVIOUS_VERSIONS
#define TCDS_RFRXD_FULL_VERSION_LIST PACKAGE_VERSION_STRING(TCDS_RFRXD_VERSION_MAJOR,TCDS_RFRXD_VERSION_MINOR,TCDS_RFRXD_VERSION_PATCH)
#else
#define TCDS_RFRXD_FULL_VERSION_LIST TCDS_RFRXD_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(TCDS_RFRXD_VERSION_MAJOR,TCDS_RFRXD_VERSION_MINOR,TCDS_RFRXD_VERSION_PATCH)
#endif

namespace tcds {
  namespace rfrxd {
    const std::string project = "tcds";
    const std::string package = "rfrxd";
    const std::string versions = TCDS_RFRXD_FULL_VERSION_LIST;
    const std::string description = "CMS software for the RFRXD.";
    const std::string authors = "Jeroen Hegeman";
    const std::string summary = "CMS software for the RFRXD.";
    const std::string link = "https://twiki.cern.ch/twiki/bin/view/CMS/TcdsNotes";
    config::PackageInfo getPackageInfo();
    void checkPackageDependencies();
    std::set<std::string, std::less<std::string> > getPackageDependencies();
  }
}

#endif
