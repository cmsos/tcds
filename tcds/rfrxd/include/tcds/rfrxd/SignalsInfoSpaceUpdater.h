#ifndef _tcds_rfrxd_SignalsInfoSpaceUpdater_h_
#define _tcds_rfrxd_SignalsInfoSpaceUpdater_h_

#include <string>

#include "tcds/utils/HwInfoSpaceUpdaterBase.h"

namespace tcds {
  namespace utils {
    class InfoSpaceHandler;
    class InfoSpaceItem;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace rfrxd {

    class VMEDeviceRFRXD;

    class SignalsInfoSpaceUpdater : public tcds::utils::HwInfoSpaceUpdaterBase
    {

    public:
      SignalsInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                              tcds::rfrxd::VMEDeviceRFRXD const& hw);
      virtual ~SignalsInfoSpaceUpdater();

      virtual bool updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                       tcds::utils::InfoSpaceHandler* const infoSpaceHandler);

    private:
      tcds::rfrxd::VMEDeviceRFRXD const& getHw() const;

      int extractChannelNumber(std::string const& regName) const;

    };

  } // namespace rfrxd
} // namespace tcds

#endif // _tcds_rfrxd_SignalsInfoSpaceUpdater_h_
