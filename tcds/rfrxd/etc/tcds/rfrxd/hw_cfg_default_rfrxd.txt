#----------------------------------------------------------------------
# Default RFRXD configuration.
#----------------------------------------------------------------------

# Input DAC reference values:
#   - for bunch-clocks: 0x09,
#   - for orbit signals: 0xa0.
output_ref_signal_ch1 0x000000a0
output_ref_signal_ch2 0x000000a0
output_ref_signal_ch3 0x000000a0

#----------------------------------------------------------------------
