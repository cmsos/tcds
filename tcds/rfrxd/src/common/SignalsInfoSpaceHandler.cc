#include "tcds/rfrxd/SignalsInfoSpaceHandler.h"

#include "toolbox/string.h"

#include "tcds/rfrxd/Definitions.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/ConfigurationInfoSpaceHandler.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/Utils.h"
#include "tcds/utils/WebServer.h"
#include "tcds/utils/XDAQAppBase.h"

tcds::rfrxd::SignalsInfoSpaceHandler::SignalsInfoSpaceHandler(tcds::utils::XDAQAppBase& xdaqApp,
                                                              tcds::utils::InfoSpaceUpdater* updater) :
  InfoSpaceHandler(xdaqApp, "tcds-rfrxd-signals", updater)
{
  // NOTE: Copy the signal names/labels into this InfoSpace so we can
  // easily monitor them using the XDAQ XMAS infrastructure.
  for (int channelNum = tcds::definitions::kChannelNumMin;
       channelNum <= tcds::definitions::kChannelNumMax;
       ++channelNum)
    {
      // The LHC-RF label.
      createString(toolbox::toString("rf_name_ch%d", channelNum),
                   xdaqApp.getConfigurationInfoSpaceHandler().getString(toolbox::toString("inputLabelChannel%d", channelNum)),
                   "",
                   tcds::utils::InfoSpaceItem::NOUPDATE);

      // The CMS label.
      createString(toolbox::toString("cms_name_ch%d", channelNum),
                   xdaqApp.getConfigurationInfoSpaceHandler().getString(toolbox::toString("outputLabelChannel%d", channelNum)),
                   "",
                   tcds::utils::InfoSpaceItem::NOUPDATE);
    }

  for (int channelNum = tcds::definitions::kChannelNumMin;
       channelNum <= tcds::definitions::kChannelNumMax;
       ++channelNum)
    {
      // The input DAC value.
      createUInt32(toolbox::toString("output_ref_signal_ch%d", channelNum));
      // A signal-present flag.
      createBool(toolbox::toString("signal_present_ch%d", channelNum));
      // The measured frequency (estimate).
      createDouble(toolbox::toString("freq_ch%d", channelNum),
                   0,
                   "freq",
                   tcds::utils::InfoSpaceItem::PROCESS);
    }
}

tcds::rfrxd::SignalsInfoSpaceHandler::~SignalsInfoSpaceHandler()
{
}

tcds::utils::XDAQAppBase&
tcds::rfrxd::SignalsInfoSpaceHandler::getOwnerApplication() const
{
  return static_cast<tcds::utils::XDAQAppBase&>(tcds::utils::InfoSpaceHandler::getOwnerApplication());
}

void
tcds::rfrxd::SignalsInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  for (int channelNum = tcds::definitions::kChannelNumMin;
       channelNum <= tcds::definitions::kChannelNumMax;
       ++channelNum)
    {
      std::string const itemSetName = toolbox::toString("itemset-input%d", channelNum);
      monitor.newItemSet(itemSetName);
      monitor.addItem(itemSetName,
                      toolbox::toString("output_ref_signal_ch%d", channelNum),
                      "Input DAC value",
                      this);
      monitor.addItem(itemSetName,
                      toolbox::toString("signal_present_ch%d", channelNum),
                      "Signal present",
                      this);
      monitor.addItem(itemSetName,
                      toolbox::toString("freq_ch%d", channelNum),
                      "Frequency estimate (Hz)",
                      this);
    }
}

void
tcds::rfrxd::SignalsInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                    tcds::utils::Monitor& monitor,
                                                                    std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "Input signals" : forceTabName;

  webServer.registerTab(tabName,
                        "Input signals",
                        3);

  for (int channelNum = tcds::definitions::kChannelNumMin;
       channelNum <= tcds::definitions::kChannelNumMax;
       ++channelNum)
    {
      std::string const itemSetName = toolbox::toString("itemset-input%d", channelNum);
      std::string const inputSignalName =
        getOwnerApplication().getConfigurationInfoSpaceHandler().getString(toolbox::toString("inputLabelChannel%d", channelNum));
      std::string const outputSignalName =
        getOwnerApplication().getConfigurationInfoSpaceHandler().getString(toolbox::toString("outputLabelChannel%d", channelNum));
      std::string const desc = "CMS name: " + outputSignalName + ", RF name: " + inputSignalName;
      webServer.registerTable(toolbox::toString("Input %d", channelNum),
                              desc,
                              monitor,
                              itemSetName,
                              tabName);
    }
}

std::string
tcds::rfrxd::SignalsInfoSpaceHandler::formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const
{
  std::string res = tcds::utils::escapeAsJSONString(tcds::utils::InfoSpaceHandler::kInvalidItemString);
  if (item->isValid())
    {
      // std::string name = item->name();
        {
          // For everything else simply call the generic formatter.
          res = InfoSpaceHandler::formatItem(item);
        }
    }
  return res;
}
