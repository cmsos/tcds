#include "tcds/rfrxd/ConfigurationInfoSpaceHandler.h"

#include "toolbox/string.h"

#include "tcds/rfrxd/Definitions.h"
#include "tcds/utils/InfoSpaceItem.h"

tcds::rfrxd::ConfigurationInfoSpaceHandler::ConfigurationInfoSpaceHandler(xdaq::Application& xdaqApp) :
  tcds::hwutilsvme::ConfigurationInfoSpaceHandlerVME(xdaqApp, "tcds-application-config-rfrxd")
{
  // The input/output text labels for all three channels.
  for (int channelNum = tcds::definitions::kChannelNumMin;
       channelNum <= tcds::definitions::kChannelNumMax;
       ++channelNum)
    {
      createString(toolbox::toString("inputLabelChannel%d", channelNum),
                   "unknown",
                   "",
                   tcds::utils::InfoSpaceItem::NOUPDATE,
                   true);
      createString(toolbox::toString("outputLabelChannel%d", channelNum),
                   "unknown",
                   "",
                   tcds::utils::InfoSpaceItem::NOUPDATE,
                   true);
    }
}

tcds::rfrxd::ConfigurationInfoSpaceHandler::~ConfigurationInfoSpaceHandler()
{
}
