#include "tcds/rfrxd/VMEDeviceRFRXD.h"

#include <cstddef>
#include <sstream>

tcds::rfrxd::VMEDeviceRFRXD::VMEDeviceRFRXD() :
  VMEDeviceBase()
{
}

tcds::rfrxd::VMEDeviceRFRXD::~VMEDeviceRFRXD()
{
}

uint32_t
tcds::rfrxd::VMEDeviceRFRXD::getBaseAddress(uint32_t const slotNumber) const
{
  return (slotNumber << 20);
}

bool
tcds::rfrxd::VMEDeviceRFRXD::isSignalPresent(int const channelNum) const
{
  std::stringstream tmp;
  tmp << "ch" << std::dec << channelNum;
  std::string const chPrefix = tmp.str();
  bool const signalPresent = (readRegister("signal_present_" + chPrefix) != 0);
  return signalPresent;
}

double
tcds::rfrxd::VMEDeviceRFRXD::readFrequency(int const channelNum) const
{
  // NOTE: This reading represents an approximate frequency
  // estimate. In case no signal is present, it is forced to zero.
  double freq = 0.;
  if (isSignalPresent(channelNum))
    {
      std::stringstream tmp;
      tmp << "ch" << std::dec << channelNum;
      std::string const chPrefix = tmp.str();
      uint32_t const freqHi = readRegister("freq_hi_" + chPrefix);
      uint32_t const freqLo = readRegister("freq_lo_" + chPrefix);
      freq = 1.e6 * (80. * 16. * 22.) / ((freqHi << 16) + freqLo);
    }
  return freq;
}

std::string
tcds::rfrxd::VMEDeviceRFRXD::readFirmwareVersionImpl() const
{
  // NOTE: The decimal (!) printed format of the firmware version
  // spells the firmware date as yyyymmdd.
  uint32_t const tmpLo = hwDevice_.readRegister("firmware_id_lo");
  uint32_t const tmpHi = hwDevice_.readRegister("firmware_id_hi");
  uint32_t const tmpVal = (tmpHi << 16) + tmpLo;
  std::stringstream tmp;
  tmp << std::dec << tmpVal;
  std::string const tmpStr = tmp.str();

  size_t const len = tmpStr.size();
  std::string const res =
    tmpStr.substr(len - 8, 4) +
    "-" +
    tmpStr.substr(len - 4, 2) +
    "-" +
    tmpStr.substr(len - 2, 2);
  return res;
}
