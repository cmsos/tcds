#include "tcds/rfrxd/RFRXDController.h"

#include <string>

#include "toolbox/string.h"
#include "xcept/Exception.h"
#include "xdaq/exception/ApplicationInstantiationFailed.h"

#include "tcds/exception/Exception.h"
#include "tcds/hwlayer/DeviceBase.h"
#include "tcds/hwutilsvme/HwIDInfoSpaceHandlerVME.h"
#include "tcds/hwutilsvme/HwIDInfoSpaceUpdaterVME.h"
#include "tcds/hwutilsvme/Utils.h"
#include "tcds/rfrxd/ConfigurationInfoSpaceHandler.h"
#include "tcds/rfrxd/SignalsInfoSpaceHandler.h"
#include "tcds/rfrxd/SignalsInfoSpaceUpdater.h"
#include "tcds/rfrxd/VMEDeviceRFRXD.h"

XDAQ_INSTANTIATOR_IMPL(tcds::rfrxd::RFRXDController)

tcds::rfrxd::RFRXDController::RFRXDController(xdaq::ApplicationStub* stub)
try
  :
  tcds::utils::XDAQAppWithFSMBasic(stub, std::unique_ptr<tcds::hwlayer::DeviceBase>(new tcds::rfrxd::VMEDeviceRFRXD())),
    soapCmdDumpHardwareState_(*this)
  {
    // Create the InfoSpace holding all configuration information.
    cfgInfoSpaceP_ =
      std::unique_ptr<tcds::utils::ConfigurationInfoSpaceHandler>(new tcds::rfrxd::ConfigurationInfoSpaceHandler(*this));

    // Make sure the correct default hardware configuration file is found.
    cfgInfoSpaceP_->setString("defaultHwConfigurationFilePath",
                              "${XDAQ_ROOT}/etc/tcds/rfrxd/hw_cfg_default_rfrxd.txt");
  }
catch (tcds::exception::Exception const& err)
  {
    std::string msgBase = "Something went wrong instantiating the RFRXDController application";
    std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.what());
    XCEPT_RAISE(xdaq::exception::ApplicationInstantiationFailed, msg.c_str());
  }

tcds::rfrxd::RFRXDController::~RFRXDController()
{
  hwRelease();
}

void
tcds::rfrxd::RFRXDController::setupInfoSpaces()
{
  // Make sure the configuration settings are up-to-date.
  cfgInfoSpaceP_->readInfoSpace();

  // Instantiate all hardware-related InfoSpaceHandlers and InfoSpaceUpdaters.
  hwIDInfoSpaceUpdaterP_ =
    std::unique_ptr<tcds::hwutilsvme::HwIDInfoSpaceUpdaterVME>(new tcds::hwutilsvme::HwIDInfoSpaceUpdaterVME(*this, getHw()));
  hwIDInfoSpaceP_ =
    std::unique_ptr<tcds::hwutilsvme::HwIDInfoSpaceHandlerVME>(new tcds::hwutilsvme::HwIDInfoSpaceHandlerVME(*this, hwIDInfoSpaceUpdaterP_.get()));
  signalsInfoSpaceUpdaterP_ =
    std::unique_ptr<tcds::rfrxd::SignalsInfoSpaceUpdater>(new tcds::rfrxd::SignalsInfoSpaceUpdater(*this, getHw()));
  signalsInfoSpaceP_ =
    std::unique_ptr<tcds::rfrxd::SignalsInfoSpaceHandler>(new tcds::rfrxd::SignalsInfoSpaceHandler(*this, signalsInfoSpaceUpdaterP_.get()));

  // Register all InfoSpaceItems with the Monitor and the WebServer.
  cfgInfoSpaceP_->registerItemSets(monitor_, webServer_);
  appStateInfoSpace_.registerItemSets(monitor_, webServer_);
  hwIDInfoSpaceP_->registerItemSets(monitor_, webServer_);
  signalsInfoSpaceP_->registerItemSets(monitor_, webServer_);
}

tcds::rfrxd::VMEDeviceRFRXD&
tcds::rfrxd::RFRXDController::getHw() const
{
  return static_cast<tcds::rfrxd::VMEDeviceRFRXD&>(*hwP_.get());
}

void
tcds::rfrxd::RFRXDController::hwConnectImpl()
{
  tcds::hwutilsvme::vmeDeviceHwConnectImpl(*cfgInfoSpaceP_, getHw());
}

void
tcds::rfrxd::RFRXDController::hwReleaseImpl()
{
  getHw().hwRelease();
}
