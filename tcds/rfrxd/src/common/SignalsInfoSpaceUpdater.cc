#include "tcds/rfrxd/SignalsInfoSpaceUpdater.h"

#include <cstddef>
#include <sstream>

#include "toolbox/string.h"

#include "tcds/rfrxd/VMEDeviceRFRXD.h"
#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/InfoSpaceItem.h"

tcds::rfrxd::SignalsInfoSpaceUpdater::SignalsInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                                              tcds::rfrxd::VMEDeviceRFRXD const& hw) :
  tcds::utils::HwInfoSpaceUpdaterBase(xdaqApp, hw)
{
}

tcds::rfrxd::SignalsInfoSpaceUpdater::~SignalsInfoSpaceUpdater()
{
}

bool
tcds::rfrxd::SignalsInfoSpaceUpdater::updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                                          tcds::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  bool updated = false;
  tcds::rfrxd::VMEDeviceRFRXD const& hw = getHw();
  if (hw.isReadyForUse())
    {
      std::string name = item.name();
      tcds::utils::InfoSpaceItem::UpdateType updateType = item.updateType();
      if (updateType == tcds::utils::InfoSpaceItem::PROCESS)
        {
          // The 'PROCESS' update type means that there is something
          // special to the variable. Figure out what to do based on the
          // variable name.
          if (toolbox::startsWith(name, "freq_ch"))
            {
              int const channelNumber = extractChannelNumber(name);
              double const newVal = hw.readFrequency(channelNumber);
              infoSpaceHandler->setDouble(name, newVal);
              updated = true;
            }
        }
      if (!updated)
        {
          updated = tcds::utils::HwInfoSpaceUpdaterBase::updateInfoSpaceItem(item, infoSpaceHandler);
        }
    }

  if (updated)
    {
      item.setValid();
    }
  else
    {
      item.setInvalid();
    }

  return updated;
}

tcds::rfrxd::VMEDeviceRFRXD const&
tcds::rfrxd::SignalsInfoSpaceUpdater::getHw() const
{
  return dynamic_cast<tcds::rfrxd::VMEDeviceRFRXD const&>(tcds::utils::HwInfoSpaceUpdaterBase::getHw());
}

int
tcds::rfrxd::SignalsInfoSpaceUpdater::extractChannelNumber(std::string const& regName) const
{
  // TODO TODO TODO
  // This could be done in a nicer way.
  std::string const patLo = "ch";
  size_t const posLo = regName.find(patLo);
  std::string const patHi = ".";
  size_t const posHi = regName.find(patHi, posLo + patLo.size());
  std::stringstream tmp;
  tmp << regName.substr(posLo + patLo.size(), posHi);
  int channelNumber;
  tmp >> channelNumber;
  // TODO TODO TODO end
  return channelNumber;
}
