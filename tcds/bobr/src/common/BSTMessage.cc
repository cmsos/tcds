#include "tcds/bobr/BSTMessage.h"

tcds::bobr::BSTMessage::BSTMessage(std::vector<uint8_t> const& rawData) :
  rawData_(rawData)
{
}

tcds::bobr::BSTMessage::~BSTMessage()
{
}

uint32_t
tcds::bobr::BSTMessage::timestamp() const
{
  // NOTE: This is only the 'coarse' part of the timestamp, down to
  // the seconds.
  uint32_t const tmp0 = rawData_.at(kIndexTimestamp);
  uint32_t const tmp1 = rawData_.at(kIndexTimestamp + 1);
  uint32_t const tmp2 = rawData_.at(kIndexTimestamp + 2);
  uint32_t const tmp3 = rawData_.at(kIndexTimestamp + 3);
  uint32_t const timestamp = (tmp3 << 24) + (tmp2 << 16) + (tmp1 << 8) + tmp0;
  return timestamp;
}

uint8_t
tcds::bobr::BSTMessage::bstMaster() const
{
  uint32_t const tmp = rawData_.at(kIndexBSTMaster);
  uint8_t const bstMaster = (tmp & 0x000000ff);
  return bstMaster;
}

uint32_t
tcds::bobr::BSTMessage::orbitNumber() const
{
  uint32_t const tmp0 = rawData_.at(kIndexOrbitNumber);
  uint32_t const tmp1 = rawData_.at(kIndexOrbitNumber + 1);
  uint32_t const tmp2 = rawData_.at(kIndexOrbitNumber + 2);
  uint32_t const tmp3 = rawData_.at(kIndexOrbitNumber + 3);
  uint32_t const orbitNumber = (tmp3 << 24) + (tmp2 << 16) + (tmp1 << 8) + tmp0;
  return orbitNumber;
}

uint32_t
tcds::bobr::BSTMessage::fillNumber() const
{
  uint32_t const tmp0 = rawData_.at(kIndexFillNumber);
  uint32_t const tmp1 = rawData_.at(kIndexFillNumber + 1);
  uint32_t const tmp2 = rawData_.at(kIndexFillNumber + 2);
  uint32_t const tmp3 = rawData_.at(kIndexFillNumber + 3);
  uint32_t const fillNumber = (tmp3 << 24) + (tmp2 << 16) + (tmp1 << 8) + tmp0;
  return fillNumber;
}

uint16_t
tcds::bobr::BSTMessage::beamMode() const
{
  uint32_t const tmp0 = rawData_.at(kIndexBeamMode);
  uint32_t const tmp1 = rawData_.at(kIndexBeamMode + 1);
  uint16_t const beamMode = (tmp1 << 8) + tmp0;
  return beamMode;
}

double
tcds::bobr::BSTMessage::beamMomentum() const
{
  uint32_t const tmp0 = rawData_.at(kIndexBeamMomentum);
  uint32_t const tmp1 = rawData_.at(kIndexBeamMomentum + 1);
  // NOTE: The following is undocumented, but during the 2016-2017
  // YETS a change was made to the units of this beam momentum
  // information.
  // From an email from Tom Levens:
  //   The scaling factor for the momentum has also changed from
  //   1GeV/LSB to 120MeV/LSB.
  double const momentum = .120 * ((tmp1 << 8) + tmp0);
  return momentum;
}

double
tcds::bobr::BSTMessage::intensityBeam1() const
{
  uint32_t const tmp0 = rawData_.at(kIndexIntensityBeam1);
  uint32_t const tmp1 = rawData_.at(kIndexIntensityBeam1 + 1);
  uint32_t const tmp2 = rawData_.at(kIndexIntensityBeam1 + 2);
  uint32_t const tmp3 = rawData_.at(kIndexIntensityBeam1 + 3);
  uint32_t const tmp = (tmp3 << 24) + (tmp2 << 16) + (tmp1 << 8) + tmp0;
  // NOTE: The beam intensity is originally specified in units of
  // '1e10 charges'.
  double const res = 1.e10 * tmp;
  return res;
}

double
tcds::bobr::BSTMessage::intensityBeam2() const
{
  uint32_t const tmp0 = rawData_.at(kIndexIntensityBeam2);
  uint32_t const tmp1 = rawData_.at(kIndexIntensityBeam2 + 1);
  uint32_t const tmp2 = rawData_.at(kIndexIntensityBeam2 + 2);
  uint32_t const tmp3 = rawData_.at(kIndexIntensityBeam2 + 3);
  uint32_t const tmp = (tmp3 << 24) + (tmp2 << 16) + (tmp1 << 8) + tmp0;
  // NOTE: The beam intensity is originally specified in units of
  // '1e10 charges'.
  double const res = 1.e10 * tmp;
  return res;
}
