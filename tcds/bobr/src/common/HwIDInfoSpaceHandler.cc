#include "tcds/bobr/HwIDInfoSpaceHandler.h"

#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/WebServer.h"

tcds::bobr::HwIDInfoSpaceHandler::HwIDInfoSpaceHandler(xdaq::Application& xdaqApp,
                                                       tcds::utils::InfoSpaceUpdater* updater) :
  InfoSpaceHandler(xdaqApp, "tcds-hw-id", updater)
{
  // General board and firmware information.
  createString("firmware_version",
               tcds::utils::InfoSpaceHandler::kUninitializedString,
               "",
               tcds::utils::InfoSpaceItem::PROCESS);
}

tcds::bobr::HwIDInfoSpaceHandler::~HwIDInfoSpaceHandler()
{
}

void
tcds::bobr::HwIDInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  // General board and firmware information.
  monitor.newItemSet("itemset-hw-id");
  monitor.addItem("itemset-hw-id",
                  "firmware_version",
                  "Firmware version",
                  this);
}

void
tcds::bobr::HwIDInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                tcds::utils::Monitor& monitor,
                                                                std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "Hardware ID" : forceTabName;

  webServer.registerTab(tabName,
                        "Hardware identifiers",
                        3);
  webServer.registerTable("Hardware identifiers",
                          "General hardware identifiers",
                          monitor,
                          "itemset-hw-id",
                          tabName);
}
