#include "tcds/bobr/HwStatusInfoSpaceHandler.h"

#include "tcds/utils/Monitor.h"
#include "tcds/utils/WebServer.h"

tcds::bobr::HwStatusInfoSpaceHandler::HwStatusInfoSpaceHandler(xdaq::Application& xdaqApp,
                                                               tcds::utils::InfoSpaceUpdater* updater) :
  InfoSpaceHandler(xdaqApp, "tcds-hw-status-bobr", updater)
{
  // Global status info.
  createBool("status_global_fpga_bstr1_ok", false, "true/false");
  createBool("status_global_fpga_bstr2_ok", false, "true/false");
  createBool("status_global_epld_muxout_ok", false, "true/false");
  createBool("status_global_local_40mhz_ok", false, "true/false");
  createBool("status_global_ttc1_mezz_absent", false, "false/true");
  createBool("status_global_ttc2_mezz_absent", false, "false/true");
  createBool("status_global_3v3_reg_ok", false, "true/false");
  createBool("status_global_2v5_reg_ok", false, "true/false");
}

tcds::bobr::HwStatusInfoSpaceHandler::~HwStatusInfoSpaceHandler()
{
}

void
tcds::bobr::HwStatusInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  // Global status info.
  std::string itemSetName = "itemset-hw-status-global";
  monitor.newItemSet(itemSetName);
  monitor.addItem(itemSetName,
                  "status_global_fpga_bstr1_ok",
                  "BSTR1 FPGA ok",
                  this);
  monitor.addItem(itemSetName,
                  "status_global_fpga_bstr2_ok",
                  "BSTR2 FPGA ok",
                  this);
  monitor.addItem(itemSetName,
                  "status_global_epld_muxout_ok",
                  "EPLD output MUX ok",
                  this);
  monitor.addItem(itemSetName,
                  "status_global_local_40mhz_ok",
                  "40 MHz clock ok",
                  this);
  monitor.addItem(itemSetName,
                  "status_global_ttc1_mezz_absent",
                  "TTC1 mezzanine present",
                  this);
  monitor.addItem(itemSetName,
                  "status_global_ttc2_mezz_absent",
                  "TTC2 mezzanine present",
                  this);
  monitor.addItem(itemSetName,
                  "status_global_3v3_reg_ok",
                  "Regulator 3.3 V ok",
                  this);
  monitor.addItem(itemSetName,
                  "status_global_2v5_reg_ok",
                  "Regulator 2.5 V ok",
                  this);
}

void
tcds::bobr::HwStatusInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                    tcds::utils::Monitor& monitor,
                                                                    std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "Hardware status" : forceTabName;

  webServer.registerTab(tabName,
                        "Hardware information",
                        1);

  // Global status info.
  webServer.registerTable("Global",
                          "Global status info",
                          monitor,
                          "itemset-hw-status-global",
                          tabName);
}
