#include "tcds/bobr/BOBRController.h"

#include <string>

#include "toolbox/string.h"
#include "xcept/Exception.h"
#include "xdaq/exception/ApplicationInstantiationFailed.h"

#include "tcds/bobr/BSTReceiverInfoSpaceHandler.h"
#include "tcds/bobr/BSTReceiverInfoSpaceUpdater.h"
#include "tcds/bobr/HwIDInfoSpaceHandler.h"
#include "tcds/bobr/HwStatusInfoSpaceHandler.h"
#include "tcds/bobr/VMEDeviceBOBR.h"
#include "tcds/exception/Exception.h"
#include "tcds/hwlayer/DeviceBase.h"
#include "tcds/hwutilsvme/ConfigurationInfoSpaceHandlerVME.h"
#include "tcds/hwutilsvme/HwIDInfoSpaceUpdaterVME.h"
#include "tcds/hwutilsvme/HwStatusInfoSpaceUpdaterVME.h"
#include "tcds/hwutilsvme/Utils.h"

XDAQ_INSTANTIATOR_IMPL(tcds::bobr::BOBRController)

tcds::bobr::BOBRController::BOBRController(xdaq::ApplicationStub* stub)
try
  :
  tcds::utils::XDAQAppWithFSMAutomatic(stub, std::unique_ptr<tcds::hwlayer::DeviceBase>(new tcds::bobr::VMEDeviceBOBR())),
    soapCmdDumpHardwareState_(*this)
  {
    // Create the InfoSpace holding all configuration information.
    cfgInfoSpaceP_ =
      std::unique_ptr<tcds::utils::ConfigurationInfoSpaceHandler>(new tcds::hwutilsvme::ConfigurationInfoSpaceHandlerVME(*this));

    // Make sure the correct default hardware configuration file is
    // found.
    cfgInfoSpaceP_->setString("defaultHwConfigurationFilePath",
                              "${XDAQ_ROOT}/etc/tcds/bobr/hw_cfg_default_bobr.txt");
  }
catch (tcds::exception::Exception const& err)
  {
    std::string msgBase = "Something went wrong instantiating the BOBRController application";
    std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.what());
    XCEPT_RAISE(xdaq::exception::ApplicationInstantiationFailed, msg.c_str());
  }

tcds::bobr::BOBRController::~BOBRController()
{
  hwRelease();
}

void
tcds::bobr::BOBRController::setupInfoSpaces()
{
  // Make sure the configuration settings are up-to-date.
  cfgInfoSpaceP_->readInfoSpace();

    // Instantiate all hardware-related InfoSpaceHandlers and InfoSpaceUpdaters.
    bstReceiverInfoSpaceUpdater1P_ =
      std::unique_ptr<tcds::bobr::BSTReceiverInfoSpaceUpdater>(new tcds::bobr::BSTReceiverInfoSpaceUpdater(*this, getHw(), 1));
    bstReceiverInfoSpaceUpdater2P_ =
      std::unique_ptr<tcds::bobr::BSTReceiverInfoSpaceUpdater>(new tcds::bobr::BSTReceiverInfoSpaceUpdater(*this, getHw(), 2));
    bstReceiverInfoSpace1P_ =
      std::unique_ptr<tcds::bobr::BSTReceiverInfoSpaceHandler>(new tcds::bobr::BSTReceiverInfoSpaceHandler(*this, bstReceiverInfoSpaceUpdater1P_.get(), 1));
    bstReceiverInfoSpace2P_ =
      std::unique_ptr<tcds::bobr::BSTReceiverInfoSpaceHandler>(new tcds::bobr::BSTReceiverInfoSpaceHandler(*this, bstReceiverInfoSpaceUpdater2P_.get(), 2));
    hwIDInfoSpaceUpdaterP_ =
      std::unique_ptr<tcds::hwutilsvme::HwIDInfoSpaceUpdaterVME>(new tcds::hwutilsvme::HwIDInfoSpaceUpdaterVME(*this, getHw()));
    hwIDInfoSpaceP_ =
      std::unique_ptr<tcds::bobr::HwIDInfoSpaceHandler>(new tcds::bobr::HwIDInfoSpaceHandler(*this, hwIDInfoSpaceUpdaterP_.get()));
    hwStatusInfoSpaceUpdaterP_ =
      std::unique_ptr<tcds::hwutilsvme::HwStatusInfoSpaceUpdaterVME>(new tcds::hwutilsvme::HwStatusInfoSpaceUpdaterVME(*this, getHw()));
    hwStatusInfoSpaceP_ =
      std::unique_ptr<tcds::bobr::HwStatusInfoSpaceHandler>(new tcds::bobr::HwStatusInfoSpaceHandler(*this, hwStatusInfoSpaceUpdaterP_.get()));

  // This application uses an automated FSM, so it has no explicit
  // hardware lease owner.
  appStateInfoSpace_.setString("hwLeaseOwnerId", "n/a");

  // Register all InfoSpaceItems with the Monitor and the WebServer.
  cfgInfoSpaceP_->registerItemSets(monitor_, webServer_);
  appStateInfoSpace_.registerItemSets(monitor_, webServer_);
  hwIDInfoSpaceP_->registerItemSets(monitor_, webServer_);
  hwStatusInfoSpaceP_->registerItemSets(monitor_, webServer_);
  bstReceiverInfoSpace1P_->registerItemSets(monitor_, webServer_);
  bstReceiverInfoSpace2P_->registerItemSets(monitor_, webServer_);
}

tcds::bobr::VMEDeviceBOBR&
tcds::bobr::BOBRController::getHw() const
{
  return static_cast<tcds::bobr::VMEDeviceBOBR&>(*hwP_.get());
}

void
tcds::bobr::BOBRController::hwConnectImpl()
{
  tcds::hwutilsvme::vmeDeviceHwConnectImpl(*cfgInfoSpaceP_, getHw());
}

void
tcds::bobr::BOBRController::hwReleaseImpl()
{
  getHw().hwRelease();
}

void
tcds::bobr::BOBRController::zeroActionImpl(toolbox::Event::Reference event)
{
  getHw().resetErrorCounters();
}

void
tcds::bobr::BOBRController::hwCfgFinalizeImpl()
{
  getHw().enableDPRAMWrite();
}
