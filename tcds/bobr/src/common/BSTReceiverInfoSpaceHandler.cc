#include "tcds/bobr/BSTReceiverInfoSpaceHandler.h"

#include <stdint.h>

#include "toolbox/string.h"

#include "tcds/bobr/Definitions.h"
#include "tcds/bobr/Utils.h"
#include "tcds/utils/Definitions.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/Utils.h"
#include "tcds/utils/WebServer.h"

tcds::bobr::BSTReceiverInfoSpaceHandler::BSTReceiverInfoSpaceHandler(xdaq::Application& xdaqApp,
                                                                     tcds::utils::InfoSpaceUpdater* updater,
                                                                     unsigned int const bstrNumber) :
  InfoSpaceHandler(xdaqApp, toolbox::toString("tcds-bobr-bst%d", bstrNumber), updater),
  bstrNumber_(bstrNumber)
{
  // This one is just used for book keeping.
  createUInt32("receiver_id",
               bstrNumber_,
               "",
               tcds::utils::InfoSpaceItem::NOUPDATE);

  //----------

  // This one is just for convenience (and not shown in the monitoring
  // page). Since there are people using the BOBR BST flashlists...
  createString("msg_beam_mode_str",
               tcds::utils::beamModeToString(tcds::definitions::BEAM_MODE_UNKNOWN),
               "",
               tcds::utils::InfoSpaceItem::NOUPDATE);

  //----------

  // BST receiver status info.
  createBool("status_ttc_in_ready",
             false,
             "true/false");
  createBool("status_local_40mhz_ok",
             false,
             "true/false");
  createBool("status_local_orbit_ok",
             false,
             "true/false");
  createBool("status_ttc_serial_b_ok",
             false,
             "true/false");
  createBool("status_ttc_frame_error",
             false,
             "true/false");
  createBool("status_message_recording",
             false,
             "enabled/disabled");
  createBool("status_irq_set_from_mask",
             false,
             "true/false");
  createBool("status_message_dump_done",
             false,
             "true/false");

  createUInt32("ttc_single_bit_err_cnt");
  createUInt32("ttc_double_bit_err_cnt");
  createUInt32("ttc_ready_err_cnt");
  createUInt32("orbit_count");

  //----------

  // BST message contents.

  // NOTE: The following all come from actually unpacking the BST
  // message, not from firmware registers.

  // The BST master.
  createUInt32("msg_bst_master");

  // The (seconds part of the) BST timestamp.
  createUInt32("msg_timestamp", 0, "time_val");

  // The LHC fill number.
  createUInt32("msg_fill_number");

  // The beam mode.
  createUInt32("msg_beam_mode");

  // The number of orbits since injection.
  createUInt32("msg_orbit_number");

  // The beam momentum (in GeV/c).
  createDouble("msg_beam_momentum");

  // The total beam intensities (integer, in 1e10 charges).
  createDouble("msg_total_intensity_beam1");
  createDouble("msg_total_intensity_beam2");
}

tcds::bobr::BSTReceiverInfoSpaceHandler::~BSTReceiverInfoSpaceHandler()
{
}

void
tcds::bobr::BSTReceiverInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  // BST receiver status info.
  std::string itemSetName = toolbox::toString("itemset-bstr%d-status", bstrNumber_);
  monitor.newItemSet(itemSetName);
  monitor.addItem(itemSetName,
                  "status_ttc_in_ready",
                  "TTC input signal ok",
                  this);
  monitor.addItem(itemSetName,
                  "status_local_40mhz_ok",
                  "40 MHz clock ok",
                  this);
  monitor.addItem(itemSetName,
                  "status_local_orbit_ok",
                  "Orbit signal ok",
                  this);
  monitor.addItem(itemSetName,
                  "status_ttc_serial_b_ok",
                  "Serial-B signal ok",
                  this);
  monitor.addItem(itemSetName,
                  "status_ttc_frame_error",
                  "TTC frame error detected",
                  this);
  monitor.addItem(itemSetName,
                  "status_message_recording",
                  "Message recording",
                  this);
  monitor.addItem(itemSetName,
                  "status_irq_set_from_mask",
                  "IRQ set from mask",
                  this);
  monitor.addItem(itemSetName,
                  "status_message_dump_done",
                  "Message dump finished",
                  this);

  monitor.addItem(itemSetName,
                  "ttc_single_bit_err_cnt",
                  "TTC single-bit error count",
                  this);
  monitor.addItem(itemSetName,
                  "ttc_double_bit_err_cnt",
                  "TTC double-bit error count",
                  this);
  monitor.addItem(itemSetName,
                  "ttc_ready_err_cnt",
                  "TTC-ready error count",
                  this);
  monitor.addItem(itemSetName,
                  "orbit_count",
                  "Orbit count",
                  this);

  //----------

  // BST message contents.
  itemSetName = toolbox::toString("itemset-bstr%d-message", bstrNumber_);
  monitor.newItemSet(itemSetName);
  monitor.addItem(itemSetName,
                  "msg_bst_master",
                  "BST master",
                  this);
  monitor.addItem(itemSetName,
                  "msg_timestamp",
                  "Timestamp",
                  this);
  monitor.addItem(itemSetName,
                  "msg_fill_number",
                  "Fill number",
                  this);
  monitor.addItem(itemSetName,
                  "msg_beam_mode",
                  "Beam mode",
                  this);
  monitor.addItem(itemSetName,
                  "msg_orbit_number",
                  "Number of orbits since last injection",
                  this);
  monitor.addItem(itemSetName,
                  "msg_beam_momentum",
                  "Beam momentum (GeV/c)",
                  this);
  monitor.addItem(itemSetName,
                  "msg_total_intensity_beam1",
                  "Beam 1 total intensity (electron charges)",
                  this);
  monitor.addItem(itemSetName,
                  "msg_total_intensity_beam2",
                  "Beam 2 total intensity (electron charges)",
                  this);
}

void
tcds::bobr::BSTReceiverInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                       tcds::utils::Monitor& monitor,
                                                                       std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? toolbox::toString("BSTR%d", bstrNumber_) : forceTabName;

  webServer.registerTab(tabName,
                        toolbox::toString("BST receiver info for receiver %d", bstrNumber_),
                        2);

  webServer.registerTable("BST receiver status",
                          "BST receiver status",
                          monitor,
                          toolbox::toString("itemset-bstr%d-status", bstrNumber_),
                          tabName);

  webServer.registerTable("BST message",
                          "BST message contents",
                          monitor,
                          toolbox::toString("itemset-bstr%d-message", bstrNumber_),
                          tabName);
}

std::string
tcds::bobr::BSTReceiverInfoSpaceHandler::formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const
{
  std::string res = tcds::utils::escapeAsJSONString(kInvalidItemString);
  if (item->isValid())
    {
      std::string name = item->name();
      if (name == "msg_bst_master")
        {
          uint32_t const value = getUInt32(name);
          tcds::definitions::BST_MASTER const valueEnum =
            static_cast<tcds::definitions::BST_MASTER>(value);
          res = tcds::utils::escapeAsJSONString(tcds::bobr::bstMasterToString(valueEnum));
        }
      else if (name == "msg_beam_mode")
        {
          uint32_t const value = getUInt32(name);
          tcds::definitions::BEAM_MODE const valueEnum =
            static_cast<tcds::definitions::BEAM_MODE>(value);
          res = tcds::utils::escapeAsJSONString(tcds::utils::beamModeToString(valueEnum));
        }
      else
        {
          // For everything else simply call the generic formatter.
          res = InfoSpaceHandler::formatItem(item);
        }
    }
  return res;
}
