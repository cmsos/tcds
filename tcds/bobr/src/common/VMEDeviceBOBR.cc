#include "tcds/bobr/VMEDeviceBOBR.h"

#include <string>

#include "toolbox/string.h"

tcds::bobr::VMEDeviceBOBR::VMEDeviceBOBR() :
  VMEDeviceBase()
{
}

tcds::bobr::VMEDeviceBOBR::~VMEDeviceBOBR()
{
}

uint32_t
tcds::bobr::VMEDeviceBOBR::getBaseAddress(uint32_t const slotNumber) const
{
  return (0x000000);
}

std::vector<uint8_t>
tcds::bobr::VMEDeviceBOBR::readBSTMessage(unsigned int const bstrNumber) const
{
  // NOTE: The BOBR does not do VME block transfers. (Unlike the
  // manual claims.) So hence the slightly clumsy approach here.

  std::string const regName = toolbox::toString("bstr%d_bst_message", bstrNumber);
  std::vector<uint32_t> tmp;
  for (uint32_t i = 0; i != kNumBytes; ++i)
    {
      uint32_t const val = readRegisterOffset(regName, i);
      tmp.push_back(val & 0xff);
    }
  std::vector<uint8_t> const res(tmp.begin(), tmp.end());
  return res;
}

void
tcds::bobr::VMEDeviceBOBR::resetErrorCounters() const
{
  // These registers are reset on write.
  std::vector<std::string> regNames;
  for (int i = 1; i != 3; ++i)
    {
      regNames.push_back((toolbox::toString("bstr%d_ttc_single_bit_err_cnt", i)));
      regNames.push_back((toolbox::toString("bstr%d_ttc_double_bit_err_cnt", i)));
      regNames.push_back((toolbox::toString("bstr%d_ttc_ready_err_cnt", i)));
    }
  for (std::vector<std::string>::const_iterator i = regNames.begin();
       i != regNames.end();
       ++i)
    {
      writeRegister(*i, 0);
    }
}

void
tcds::bobr::VMEDeviceBOBR::enableDPRAMWrite() const
{
  // Enable the logging of the incoming BST messages into the
  // Dual-Port RAM.
  writeRegister("ctrl_bsrt1_dpram_enable", 1);
  writeRegister("ctrl_bsrt2_dpram_enable", 1);
}

uint32_t
tcds::bobr::VMEDeviceBOBR::readRegisterOffset(std::string const& regName,
                                              uint32_t const offset) const
{
  // We need this method for readBSTMessage().
  return hwDevice_.readRegister(regName, offset);
}
