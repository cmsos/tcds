#include "tcds/bobr/BSTReceiverInfoSpaceUpdater.h"

#include <stdint.h>
#include <string>
#include <vector>

#include "toolbox/string.h"

#include "tcds/bobr/BSTMessage.h"
#include "tcds/bobr/VMEDeviceBOBR.h"
#include "tcds/utils/Definitions.h"
#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Utils.h"

tcds::bobr::BSTReceiverInfoSpaceUpdater::BSTReceiverInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                                                     tcds::bobr::VMEDeviceBOBR const& hw,
                                                                     unsigned int const bstrNumber) :
  tcds::utils::HwInfoSpaceUpdaterBase(xdaqApp, hw),
  bstrNumber_(bstrNumber)
{
}

tcds::bobr::BSTReceiverInfoSpaceUpdater::~BSTReceiverInfoSpaceUpdater()
{
}

void
tcds::bobr::BSTReceiverInfoSpaceUpdater::updateInfoSpaceImpl(tcds::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  // This InfoSpaceHandler contains two set of items, really. One set
  // of special ones (of which the names start with 'msg_') derived
  // from the BST message, and others that are the usual kind of
  // registers. Both types are handled slightly differently.

  tcds::bobr::VMEDeviceBOBR const& hw = getHw();
  if (hw.isReadyForUse())
    {
      // This loop handles all 'normal' items.
      tcds::utils::InfoSpaceHandler::ItemVec& items = infoSpaceHandler->getItems();
      tcds::utils::InfoSpaceHandler::ItemVec::iterator iter;
      for (iter = items.begin(); iter != items.end(); ++iter)
        {
          std::string const name = iter->name();
          if (!toolbox::startsWith(name, "msg_"))
            {
              try
                {
                  updateInfoSpaceItem(*iter, infoSpaceHandler);
                }
              catch (...)
                {
                  iter->setInvalid();
                  throw;
                }
            }
        }

      // The following handles all 'msg_' items.
      std::vector<uint8_t> rawBSTData = hw.readBSTMessage(bstrNumber_);
      tcds::bobr::BSTMessage bstMessage(rawBSTData);

      uint32_t const bstMaster = bstMessage.bstMaster();
      infoSpaceHandler->setUInt32("msg_bst_master", bstMaster);

      uint32_t const timestamp = bstMessage.timestamp();
      infoSpaceHandler->setUInt32("msg_timestamp", timestamp);

      uint32_t const fillNumber = bstMessage.fillNumber();
      infoSpaceHandler->setUInt32("msg_fill_number", fillNumber);

      uint32_t const beamMode = bstMessage.beamMode();
      infoSpaceHandler->setUInt32("msg_beam_mode", beamMode);

      uint32_t const orbitNumber = bstMessage.orbitNumber();
      infoSpaceHandler->setUInt32("msg_orbit_number", orbitNumber);

      double const momentum = bstMessage.beamMomentum();
      infoSpaceHandler->setDouble("msg_beam_momentum", momentum);

      double const intensityBeam1 = bstMessage.intensityBeam1();
      infoSpaceHandler->setDouble("msg_total_intensity_beam1", intensityBeam1);
      double const intensityBeam2 = bstMessage.intensityBeam2();
      infoSpaceHandler->setDouble("msg_total_intensity_beam2", intensityBeam2);

      // Argh! One special case: the string copy of the beam mode
      // enum.
      tcds::definitions::BEAM_MODE const valueEnum =
        static_cast<tcds::definitions::BEAM_MODE>(beamMode);
      std::string const beamModeStr =
        tcds::utils::beamModeToString(valueEnum);
      infoSpaceHandler->setString("msg_beam_mode_str", beamModeStr);

      infoSpaceHandler->setValid();
    }
  else
    {
      infoSpaceHandler->setInvalid();
    }

  // Now sync everything from the cache to the InfoSpace itself.
  infoSpaceHandler->writeInfoSpace();
}

bool
tcds::bobr::BSTReceiverInfoSpaceUpdater::updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                                             tcds::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  bool updated = false;
  tcds::bobr::VMEDeviceBOBR const& hw = getHw();
  if (hw.isReadyForUse())
    {
      std::string name = item.name();
      tcds::utils::InfoSpaceItem::UpdateType updateType = item.updateType();
      if (updateType == tcds::utils::InfoSpaceItem::HW32)
        {
          if (!toolbox::startsWith(name, "msg_"))
            {
              std::string const regName = toolbox::toString("bstr%d_%s",
                                                            bstrNumber_,
                                                            item.hwName().c_str());
              uint32_t const tmp = hw.readRegister(regName);
              tcds::utils::InfoSpaceItem::ItemType itemType = item.type();
              if (itemType == tcds::utils::InfoSpaceItem::UINT32)
                {
                  infoSpaceHandler->setUInt32(name, tmp);
                  updated = true;
                }
              else if (itemType == tcds::utils::InfoSpaceItem::BOOL)
                {
                  infoSpaceHandler->setBool(name, (tmp != 0));
                  updated = true;
                }
            }
        }
      if (!updated)
        {
          updated = tcds::utils::HwInfoSpaceUpdaterBase::updateInfoSpaceItem(item, infoSpaceHandler);
        }
    }

  if (updated)
    {
      item.setValid();
    }
  else
    {
      item.setInvalid();
    }

  return updated;
}

tcds::bobr::VMEDeviceBOBR const&
tcds::bobr::BSTReceiverInfoSpaceUpdater::getHw() const
{
  return dynamic_cast<tcds::bobr::VMEDeviceBOBR const&>(tcds::utils::HwInfoSpaceUpdaterBase::getHw());
}
