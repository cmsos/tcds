#ifndef _tcds_bobr_Utils_h_
#define _tcds_bobr_Utils_h_

#include <string>

#include "tcds/bobr/Definitions.h"

namespace tcds {
  namespace bobr {

    // Mapping of BST master enums to strings.
    std::string bstMasterToString(tcds::definitions::BST_MASTER const bstMaster);

  } // namespace bobr
} // namespace tcds

#endif // _tcds_bobr_Utils_h_
