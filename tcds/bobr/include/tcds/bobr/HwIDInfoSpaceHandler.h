#ifndef _tcds_bobr_HwIDInfoSpaceHandler_h_
#define _tcds_bobr_HwIDInfoSpaceHandler_h_

#include <string>

#include "tcds/utils/InfoSpaceHandler.h"

namespace xdaq {
  class Application;
}

namespace tcds {
  namespace utils {
    class InfoSpaceUpdater;
    class Monitor;
    class WebServer;
  }
}

namespace tcds {
  namespace bobr {

    class HwIDInfoSpaceHandler : public tcds::utils::InfoSpaceHandler
    {

    public:
      HwIDInfoSpaceHandler(xdaq::Application& xdaqApp,
                           tcds::utils::InfoSpaceUpdater* updater);
      virtual ~HwIDInfoSpaceHandler();

    protected:
      virtual void registerItemSetsWithMonitor(tcds::utils::Monitor& monitor);
      virtual void registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                 tcds::utils::Monitor& monitor,
                                                 std::string const& forceTabName="");
    };

  } // namespace bobr
} // namespace tcds

#endif // _tcds_bobr_HwIDInfoSpaceHandler_h_
