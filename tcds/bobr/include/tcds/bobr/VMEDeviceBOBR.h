#ifndef _tcds_apve_VMEDeviceBOBR_h_
#define _tcds_apve_VMEDeviceBOBR_h_

#include <stdint.h>
#include <string>
#include <vector>

#include "tcds/hwlayervme/VMEDeviceBase.h"

namespace tcds {
  namespace bobr {

    /**
     * Implementation of the VME BOBR functionality.
     */
    class VMEDeviceBOBR : public tcds::hwlayervme::VMEDeviceBase
    {

    public:
      // The BST message consists of 64 bytes.
      static unsigned int const kNumBytes = 64;

      VMEDeviceBOBR();
      virtual ~VMEDeviceBOBR();

      virtual uint32_t getBaseAddress(uint32_t const slotNumber) const;

      std::vector<uint8_t> readBSTMessage(unsigned int const bstrNumber) const;

      void resetErrorCounters() const;

      void enableDPRAMWrite() const;

    private:
      uint32_t readRegisterOffset(std::string const& regName,
                                  uint32_t const offset) const;

    };

  } // namespace bobr
} // namespace tcds

#endif // _tcds_bobr_VMEDeviceBOBR_h_
