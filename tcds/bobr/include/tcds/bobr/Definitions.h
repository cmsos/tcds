#ifndef _tcds_bobr_Definitions_h_
#define _tcds_bobr_Definitions_h_

#include <stdint.h>

namespace tcds {
  namespace definitions {

    enum BST_MASTER {
      BST_MASTER_UNKNOWN = 0,
      BST_MASTER_BEAM1 = 1,
      BST_MASTER_BEAM2 = 2
    };

  } // namespace definitions
} // namespace tcds

#endif // _tcds_bobr_Definitions_h_
