#ifndef _tcds_bobr_BOBRController_h_
#define _tcds_bobr_BOBRController_h_

#include <memory>

#include "toolbox/Event.h"
#include "xdaq/Application.h"

#include "tcds/bobr/VMEDeviceBOBR.h"
#include "tcds/utils/SOAPCmdBase.h"
#include "tcds/utils/SOAPCmdDumpHardwareState.h"
#include "tcds/utils/XDAQAppWithFSMAutomatic.h"

namespace tcds {
  namespace hwutilsvme {
    class HwIDInfoSpaceUpdaterVME;
    class HwStatusInfoSpaceUpdaterVME;
  }
}

namespace xdaq {
  class ApplicationStub;
}

namespace tcds {
  namespace bobr {

    class BSTReceiverInfoSpaceHandler;
    class BSTReceiverInfoSpaceUpdater;
    class HwIDInfoSpaceHandler;
    class HwStatusInfoSpaceHandler;

    class BOBRController : public tcds::utils::XDAQAppWithFSMAutomatic
    {

    public:
      XDAQ_INSTANTIATOR();

      BOBRController(xdaq::ApplicationStub* stub);
      virtual ~BOBRController();

    protected:
      virtual void setupInfoSpaces();

      /**
       * Access the hardware pointer as VMEDeviceBOBR&.
       */
      virtual VMEDeviceBOBR& getHw() const;

      virtual void hwConnectImpl();
      virtual void hwReleaseImpl();

      virtual void zeroActionImpl(toolbox::Event::Reference event);

      virtual void hwCfgFinalizeImpl();

    private:
      // Various InfoSpaces and their InfoSpaceUpdaters.
      std::unique_ptr<tcds::bobr::BSTReceiverInfoSpaceUpdater> bstReceiverInfoSpaceUpdater1P_;
      std::unique_ptr<tcds::bobr::BSTReceiverInfoSpaceUpdater> bstReceiverInfoSpaceUpdater2P_;
      std::unique_ptr<tcds::bobr::BSTReceiverInfoSpaceHandler> bstReceiverInfoSpace1P_;
      std::unique_ptr<tcds::bobr::BSTReceiverInfoSpaceHandler> bstReceiverInfoSpace2P_;
      std::unique_ptr<tcds::hwutilsvme::HwIDInfoSpaceUpdaterVME> hwIDInfoSpaceUpdaterP_;
      std::unique_ptr<tcds::bobr::HwIDInfoSpaceHandler> hwIDInfoSpaceP_;
      std::unique_ptr<tcds::hwutilsvme::HwStatusInfoSpaceUpdaterVME> hwStatusInfoSpaceUpdaterP_;
      std::unique_ptr<tcds::bobr::HwStatusInfoSpaceHandler> hwStatusInfoSpaceP_;

      // The SOAP commands.
      template<typename> friend class tcds::utils::SOAPCmdBase;
      template<typename> friend class tcds::utils::SOAPCmdDumpHardwareState;
      tcds::utils::SOAPCmdDumpHardwareState<BOBRController> soapCmdDumpHardwareState_;

    };

  } // namespace bobr
} // namespace tcds

#endif // _tcds_bobr_BOBRController_h_
