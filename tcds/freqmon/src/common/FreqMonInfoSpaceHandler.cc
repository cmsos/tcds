#include "tcds/freqmon/FreqMonInfoSpaceHandler.h"

#include "toolbox/string.h"

#include "tcds/freqmon/Definitions.h"
#include "tcds/freqmon/InfoSpaceHandler.h"
#include "tcds/freqmon/Utils.h"
#include "tcds/freqmon/WebTableFreqMonTrend.h"
#include "tcds/utils/ConfigurationInfoSpaceHandler.h"
#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/WebServer.h"
#include "tcds/utils/XDAQAppBase.h"

tcds::freqmon::FreqMonInfoSpaceHandler::FreqMonInfoSpaceHandler(xdaq::Application& xdaqApp,
                                                                tcds::utils::InfoSpaceUpdater* updater) :
  tcds::utils::MultiInfoSpaceHandler(xdaqApp, updater)
{
  tcds::utils::ConfigurationInfoSpaceHandler const& cfgInfoSpace =
    getOwnerApplication().getConfigurationInfoSpaceHandler();

  // Pre-build all input labels.
  for (unsigned int source = tcds::definitions::FREQ_SOURCE_MIN;
       source <= tcds::definitions::FREQ_SOURCE_MAX;
       ++source)
    {
      tcds::definitions::FREQ_SOURCE const channel = static_cast<tcds::definitions::FREQ_SOURCE>(source);
      std::string const name = freqChannelToName(channel);
      std::string const label = cfgInfoSpace.getString("label" + name);
      // Channels labeled like 'not used', 'not connected', etc. are
      // considered unused.
      bool const isConnected =
        (toolbox::tolower(label) != toolbox::tolower(tcds::definitions::kUnusedInputLabel));
      inputLabels_[channel] = label;
      inputIsConnected_[channel] = isConnected;
    }

  //----------

  // Various traces of frequency data.
  std::string infoSpaceHandlerName;
  for (unsigned int source = tcds::definitions::FREQ_SOURCE_MIN;
       source <= tcds::definitions::FREQ_SOURCE_MAX;
       ++source)
    {
      tcds::definitions::FREQ_SOURCE const channel = static_cast<tcds::definitions::FREQ_SOURCE>(source);
      std::string const infoSpaceHandlerBaseName = toolbox::toString("tcds-freqmon-data-%d", channel);
      tcds::utils::InfoSpaceHandler* handler = 0;

      // Latest measured frequency.
      infoSpaceHandlerName = infoSpaceHandlerBaseName + "-meas";
      handler = new tcds::freqmon::InfoSpaceHandler(xdaqApp,
                                                    infoSpaceHandlerName,
                                                    updater);
      infoSpaceHandlers_[infoSpaceHandlerName] = handler;
      createFreqChannel(handler,
                        inputLabels_[channel],
                        source,
                        inputIsConnected_[channel],
                        false);

      // Latest stored frequency.
      infoSpaceHandlerName = infoSpaceHandlerBaseName + "-store";
      handler = new tcds::freqmon::InfoSpaceHandler(xdaqApp,
                                                    infoSpaceHandlerName,
                                                    updater);
      infoSpaceHandlers_[infoSpaceHandlerName] = handler;
      createFreqChannel(handler,
                        inputLabels_[channel],
                        source,
                        inputIsConnected_[channel],
                        true);
    }
}

tcds::freqmon::FreqMonInfoSpaceHandler::~FreqMonInfoSpaceHandler()
{
}

tcds::utils::XDAQAppBase&
tcds::freqmon::FreqMonInfoSpaceHandler::getOwnerApplication() const
{
  return static_cast<tcds::utils::XDAQAppBase&>(tcds::utils::MultiInfoSpaceHandler::getOwnerApplication());
}

void
tcds::freqmon::FreqMonInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  // Various traces of frequency data.
  std::string infoSpaceHandlerName;
  std::string itemSetName;
  for (unsigned int source = tcds::definitions::FREQ_SOURCE_MIN;
       source <= tcds::definitions::FREQ_SOURCE_MAX;
       ++source)
    {
      tcds::definitions::FREQ_SOURCE const channel = static_cast<tcds::definitions::FREQ_SOURCE>(source);
      std::string const infoSpaceHandlerBaseName = toolbox::toString("tcds-freqmon-data-%d", channel);
      std::string const itemSetBaseName = toolbox::toString("Frequency %d", channel);

      infoSpaceHandlerName = infoSpaceHandlerBaseName + "-meas";
      itemSetName = itemSetBaseName + " (meas)";
      monitor.newItemSet(itemSetName);
      monitor.addItem(itemSetName,
                      "freq_source_value",
                      inputLabels_[channel],
                      infoSpaceHandlers_.at(infoSpaceHandlerName));
      monitor.addItem(itemSetName,
                      "freq_source_time",
                      "Timestamp",
                      infoSpaceHandlers_.at(infoSpaceHandlerName));

      itemSetName = itemSetName + " (trend)";
      monitor.newItemSet(itemSetName);
      monitor.addItem(itemSetName,
                      "freq_source_trend",
                      "Trend",
                      infoSpaceHandlers_.at(infoSpaceHandlerName));

      infoSpaceHandlerName = infoSpaceHandlerBaseName + "-store";
      itemSetName = itemSetBaseName + " (store)";
      monitor.newItemSet(itemSetName);
      monitor.addItem(itemSetName,
                      "freq_source_value",
                      inputLabels_[channel],
                      infoSpaceHandlers_.at(infoSpaceHandlerName));
      monitor.addItem(itemSetName,
                      "freq_source_time",
                      "Timestamp",
                      infoSpaceHandlers_.at(infoSpaceHandlerName));

      itemSetName = itemSetName + " (trend)";
      monitor.newItemSet(itemSetName);
      monitor.addItem(itemSetName,
                      "freq_source_trend",
                      "Trend",
                      infoSpaceHandlers_.at(infoSpaceHandlerName));
    }
}

void
tcds::freqmon::FreqMonInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                      tcds::utils::Monitor& monitor,
                                                                      std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "Frequencies" : forceTabName;

  webServer.registerTab(tabName,
                        "Clock frequencies. NOTE: All the below measurements are independent"
                        ", and thus subject to their own noise and fluctuations.",
                        3);

  //----------

  // Various traces of frequency data.
  std::string itemSetName;
  for (unsigned int source = tcds::definitions::FREQ_SOURCE_MIN;
       source <= tcds::definitions::FREQ_SOURCE_MAX;
       ++source)
    {
      tcds::definitions::FREQ_SOURCE const channel = static_cast<tcds::definitions::FREQ_SOURCE>(source);
      std::string const itemSetBaseName = toolbox::toString("Frequency %d", channel);
      std::string const infoSpaceHandlerBaseName = toolbox::toString("tcds-freqmon-data-%d", channel);

      itemSetName = itemSetBaseName + " (meas)";
      webServer.registerTable("Measured frequency",
                              "Latest measured clock frequency (MHz)",
                              monitor,
                              itemSetName,
                              tabName,
                              1);
      webServer.registerWebObject<tcds::freqmon::WebTableFreqMonTrend>("Recent trend",
                                                                       "Measured points, without dead-banding (Hz)",
                                                                       monitor,
                                                                       itemSetName + " (trend)",
                                                                       tabName,
                                                                       2);

      itemSetName = itemSetBaseName + " (store)";
      webServer.registerTable("Stored frequency",
                              "Latest stored clock frequency (MHz)",
                              monitor,
                              itemSetName,
                              tabName,
                              1);
      webServer.registerWebObject<tcds::freqmon::WebTableFreqMonTrend>("Recent trend",
                                                                       "Stored points, after dead-banding (Hz)",
                                                                       monitor,
                                                                       itemSetName + " (trend)",
                                                                       tabName,
                                                                       2);
    }
}

void
tcds::freqmon::FreqMonInfoSpaceHandler::createFreqChannel(tcds::utils::InfoSpaceHandler* const handler,
                                                          std::string const sourceLabel,
                                                          unsigned int const idNumber,
                                                          bool const isConnected,
                                                          bool const isStore)
{
  // A flag to indicate which kind of channel this is.
  handler->createBool("is_store",
                      isStore,
                      "",
                      tcds::utils::InfoSpaceItem::NOUPDATE);

  // A flag to indicate whether or not this channel is actually
  // connected to anything.
  handler->createBool("freq_source_is_connected",
                      isConnected,
                      "",
                      tcds::utils::InfoSpaceItem::NOUPDATE);

  // A human-readable label for this channel.
  handler->createString("freq_source_label",
                        sourceLabel,
                        "",
                        tcds::utils::InfoSpaceItem::NOUPDATE);

  // The source number of this channel.
  handler->createUInt32("freq_source_id_number",
                        idNumber,
                        "",
                        tcds::utils::InfoSpaceItem::NOUPDATE);

  // The timestamp corresponding to the measurement for this channel.
  handler->createTimeVal("freq_source_time",
                         0.,
                         "",
                         tcds::utils::InfoSpaceItem::PROCESS);

  // The measured value for this channel.
  handler->createDouble("freq_source_value",
                        0.,
                        "freq_MHz",
                        tcds::utils::InfoSpaceItem::PROCESS);

  //----------

  // The recent trend/history.
  handler->createString("freq_source_trend",
                        "",
                        "",
                        tcds::utils::InfoSpaceItem::PROCESS);
}
