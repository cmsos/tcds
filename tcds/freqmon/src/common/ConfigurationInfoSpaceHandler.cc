#include "tcds/freqmon/ConfigurationInfoSpaceHandler.h"

#include "tcds/freqmon/Definitions.h"
#include "tcds/freqmon/Utils.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Monitor.h"

tcds::freqmon::ConfigurationInfoSpaceHandler::ConfigurationInfoSpaceHandler(xdaq::Application& xdaqApp) :
  tcds::hwutilsscpi::ConfigurationInfoSpaceHandlerSCPI(xdaqApp)
{
  for (unsigned int source = tcds::definitions::FREQ_SOURCE_MIN;
       source <= tcds::definitions::FREQ_SOURCE_MAX;
       ++source)
    {
      tcds::definitions::FREQ_SOURCE const channel = static_cast<tcds::definitions::FREQ_SOURCE>(source);
      std::string const name = freqChannelToName(channel);
      std::string const labelName = "label" + name;
      createString(labelName,
                   "Unknown",
                   "",
                   tcds::utils::InfoSpaceItem::NOUPDATE,
                   true);
    }

  // - The number of measurements to average over.
  createUInt32("freqMonitorNumAvg",
               1,
               "",
               tcds::utils::InfoSpaceItem::NOUPDATE,
               true);

  // - The dead-band variation in Hz.
  createFloat("freqMonitorDeadBand",
              1.,
              "",
              tcds::utils::InfoSpaceItem::NOUPDATE,
              true);

  // - The dead-band override/time-out.
  createString("freqMonitorDeadBandOverrideTime",
               "PT10M",
               "",
               tcds::utils::InfoSpaceItem::NOUPDATE,
               true);

  // - The maximum number of points to keep in the short-term
  //   measurement histories.
  createUInt32("historyMaxNumPoints",
               100,
               "",
               tcds::utils::InfoSpaceItem::NOUPDATE,
               true);

  // - The maximum duration of the short-term measurement histories.
  createString("historyMaxDuration",
               "PT30M",
               "",
               tcds::utils::InfoSpaceItem::NOUPDATE,
               true);
}

void
tcds::freqmon::ConfigurationInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  tcds::hwutilsscpi::ConfigurationInfoSpaceHandlerSCPI::registerItemSetsWithMonitor(monitor);

  std::string const itemSetName = "Application configuration";

  // - The number of measurements to average over.
  monitor.addItem(itemSetName,
                  "freqMonitorNumAvg",
                  "Measurements averaged per data point",
                  this,
                  "Allows averaging a number of single measurements"
                  " for each actual data point. Should not be necessary.");

  // - The dead-band variation in Hz.
  monitor.addItem(itemSetName,
                  "freqMonitorDeadBand",
                  "Dead-band (Hz)",
                  this,
                  "The data point only updates if the value changes"
                  " by at least this much.");

  // - The dead-band override/time-out.
  monitor.addItem(itemSetName,
                  "freqMonitorDeadBandOverrideTime",
                  "Dead-band override time interval",
                  this,
                  "If the data point does not vary enough to exit the dead-band"
                  " it will be updated after this interval anyway.");

  // - The maximum number of points to keep in the short-term
  //   measurement histories.
  monitor.addItem(itemSetName,
                  "historyMaxNumPoints",
                  "Maximum number of history points",
                  this,
                  "The maximum number of points to keep"
                  " in the short term measurement histories.");

  // - The maximum duration of the short-term measurement histories.
  monitor.addItem(itemSetName,
                  "historyMaxDuration",
                  "Maximum history duration",
                  this,
                  "The maximum duration to keep"
                  " for the short term measurement histories.");
}

void
tcds::freqmon::ConfigurationInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                            tcds::utils::Monitor& monitor,
                                                                            std::string const& forceTabName)
{
  tcds::hwutilsscpi::ConfigurationInfoSpaceHandlerSCPI::registerItemSetsWithWebServer(webServer, monitor, forceTabName);
}
