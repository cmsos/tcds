#include "tcds/freqmon/FreqMonDataPoint.h"

tcds::freqmon::FreqMonDataPoint::FreqMonDataPoint(toolbox::TimeVal const& timestamp,
                                                  double const frequency,
                                                  tcds::definitions::REF_SOURCE const& referenceSource) :
  timestamp_(timestamp),
  frequency_(frequency),
  referenceSource_(referenceSource)
{
}

toolbox::TimeVal
tcds::freqmon::FreqMonDataPoint::timestamp() const
{
  return timestamp_;
}

double
tcds::freqmon::FreqMonDataPoint::frequency() const
{
  return frequency_;
}

bool
tcds::freqmon::FreqMonDataPoint::isCompatible(FreqMonDataPoint const& other) const
{
  bool const res = !haveConditionsChanged(other);
  return res;
}

bool
tcds::freqmon::FreqMonDataPoint::haveConditionsChanged(FreqMonDataPoint const& other) const
{
  bool res = false;

  // If the top-level flags change, then yes.
  if (referenceSource_ != other.referenceSource_)
    {
      res = true;
    }

  return res;
}
