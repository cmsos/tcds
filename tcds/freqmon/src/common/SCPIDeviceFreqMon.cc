#include "tcds/freqmon/SCPIDeviceFreqMon.h"

#include <sstream>
#include <string>

#include "toolbox/string.h"

#include "tcds/hwlayerscpi/HwDeviceSCPI.h"

tcds::freqmon::SCPIDeviceFreqMon::SCPIDeviceFreqMon()
{
}

tcds::freqmon::SCPIDeviceFreqMon::~SCPIDeviceFreqMon()
{
}

double
tcds::freqmon::SCPIDeviceFreqMon::readFreqMeasurement(tcds::definitions::FREQ_SOURCE const srcId) const
{
  std::string const cmd = toolbox::toString("CONFIGURE:FREQUENCY 40E6,(@%d)", srcId);
  hwDevice_.execCommand(cmd);
  std::string const rawReply = hwDevice_.execCommand("READ?");
  double res;
  std::stringstream(rawReply) >> res;
  // NOTE: When there is no input signal (i.e., in case of a
  // measurement timeout), the frequency counter tends to reply:
  // '+9.91000000000000E+037'. Let's cut off those and remap them to
  // 0.
  if (res > 1.e37)
    {
      res = 0.;
    }

  return res;
}

tcds::definitions::REF_SOURCE
tcds::freqmon::SCPIDeviceFreqMon::getReferenceSource() const
{
  tcds::definitions::REF_SOURCE res = tcds::definitions::REF_SOURCE_UNKNOWN;

  std::string const cmd = "ROSCILLATOR:SOURCE?";
  std::string const rawReply = hwDevice_.execCommand(cmd);

  if (rawReply == "INT")
    {
      res = tcds::definitions::REF_SOURCE_INTERNAL;
    }
  else if (rawReply == "EXT")
    {
      std::string const cmd = "ROSCILLATOR:EXTERNAL:FREQUENCY?";
      std::string const rawReply = hwDevice_.execCommand(cmd);
      if (rawReply == "+1.00000000E+006")
        {
          res = tcds::definitions::REF_SOURCE_EXTERNAL_1MHZ;
        }
      else if (rawReply == "+5.00000000E+006")
        {
          res = tcds::definitions::REF_SOURCE_EXTERNAL_5MHZ;
        }
      else if (rawReply == "+1.00000000E+007")
        {
          res = tcds::definitions::REF_SOURCE_EXTERNAL_10MHZ;
        }
      else
        {
          res = tcds::definitions::REF_SOURCE_UNKNOWN;
        }
    }
  else
    {
      res = tcds::definitions::REF_SOURCE_UNKNOWN;
    }

  return res;
}
