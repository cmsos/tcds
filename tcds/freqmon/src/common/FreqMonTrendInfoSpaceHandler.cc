#include "tcds/freqmon/FreqMonTrendInfoSpaceHandler.h"

#include <algorithm>
#include <cstddef>
#include <iterator>
#include <sstream>
#include <stdint.h>

#include "toolbox/string.h"

#include "tcds/freqmon/Utils.h"
#include "tcds/freqmon/WebTableFreqMonTrend.h"
#include "tcds/utils/ConfigurationInfoSpaceHandler.h"
#include "tcds/utils/Definitions.h"
#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/Utils.h"
#include "tcds/utils/WebServer.h"
#include "tcds/utils/XDAQAppBase.h"

tcds::freqmon::FreqMonTrendInfoSpaceHandler::FreqMonTrendInfoSpaceHandler(xdaq::Application& xdaqApp,
                                                                tcds::utils::InfoSpaceUpdater* updater) :
  tcds::utils::InfoSpaceHandler(xdaqApp, "freqmon-trend", updater)
{
  tcds::utils::ConfigurationInfoSpaceHandler const& cfgInfoSpace =
    getOwnerApplication().getConfigurationInfoSpaceHandler();

  // Pre-build all input labels.
  for (unsigned int source = tcds::definitions::FREQ_SOURCE_MIN;
       source <= tcds::definitions::FREQ_SOURCE_MAX;
       ++source)
    {
      tcds::definitions::FREQ_SOURCE const channel = static_cast<tcds::definitions::FREQ_SOURCE>(source);
      std::string const name = freqChannelToName(channel);
      std::string const label = cfgInfoSpace.getString("label" + name);
      inputNames_[channel] = name;
      inputLabels_[channel] = label;
    }

  //----------

  // All individual frequency trends.
  for (unsigned int source = tcds::definitions::FREQ_SOURCE_MIN;
       source <= tcds::definitions::FREQ_SOURCE_MAX;
       ++source)
    {
      // tcds::definitions::FREQ_SOURCE const channel = static_cast<tcds::definitions::FREQ_SOURCE>(source);

      createDoubleVec(toolbox::toString("freq_trend_%d", source));

  //     std::string const inputName = inputNames_[channel];
  //     std::string const name = inputName;

  //     tcds::utils::InfoSpaceHandler* handler =
  //       new tcds::utils::InfoSpaceHandler(xdaqApp,
  //                                         toolbox::toString("tcds-freqmon-data-%s",
  //                                         inputName.c_str()),
  //                                         updater);
  //     infoSpaceHandlers_[name] = handler;
  //     createFreqChannel(handler,
  //                       inputLabels_[channel],
  //                       source);
    }
}

tcds::freqmon::FreqMonTrendInfoSpaceHandler::~FreqMonTrendInfoSpaceHandler()
{
}

tcds::utils::XDAQAppBase&
tcds::freqmon::FreqMonTrendInfoSpaceHandler::getOwnerApplication() const
{
  return static_cast<tcds::utils::XDAQAppBase&>(tcds::utils::InfoSpaceHandler::getOwnerApplication());
}

void
tcds::freqmon::FreqMonTrendInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  // All individual frequency trends.
  std::string const itemSetName = "itemset-freqmon-trend-data";
  monitor.newItemSet(itemSetName);
  for (unsigned int source = tcds::definitions::FREQ_SOURCE_MIN;
       source <= tcds::definitions::FREQ_SOURCE_MAX;
       ++source)
    {
      tcds::definitions::FREQ_SOURCE const channel = static_cast<tcds::definitions::FREQ_SOURCE>(source);
  //     std::string const inputName = inputNames_[channel];
  //     std::string const name = inputName;

      monitor.addItem(itemSetName,
                      toolbox::toString("freq_trend_%d", source),
                      inputLabels_[channel],
                      this);
    }
}

void
tcds::freqmon::FreqMonTrendInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                      tcds::utils::Monitor& monitor,
                                                                      std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "Frequency trends" : forceTabName;

  webServer.registerTab(tabName,
                        "Recent frequency trends",
                        1);
  //----------

  // All individual frequency trends.
  webServer.registerWebObject<tcds::freqmon::WebTableFreqMonTrend>("Measured frequency trends",
                                                                   "",
                                                                   monitor,
                                                                   "itemset-freqmon-trend-data",
                                                                   tabName,
                                                                   1);
}

// void
// tcds::freqmon::FreqMonTrendInfoSpaceHandler::createFreqChannel(tcds::utils::InfoSpaceHandler* const handler,
//                                                           std::string const sourceLabel,
//                                                           unsigned int const idNumber)
// {
//   // A human-readable label for this channel.
//   handler->createString("freq_source_label",
//                         sourceLabel,
//                         "",
//                         tcds::utils::InfoSpaceItem::NOUPDATE);

//   // The source number of this channel.
//   handler->createUInt32("freq_source_id_number",
//                         idNumber,
//                         "",
//                         tcds::utils::InfoSpaceItem::NOUPDATE);

//   // The measured value for this channel.
//   handler->createDouble("freq_source_value",
//                         0.,
//                         "freq_MHz",
//                         tcds::utils::InfoSpaceItem::PROCESS);
// }

std::string
tcds::freqmon::FreqMonTrendInfoSpaceHandler::formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const
{
  std::string res = tcds::utils::escapeAsJSONString(tcds::utils::InfoSpaceHandler::kInvalidItemString);
  if (item->isValid())
    {
      std::string name = item->name();
      if (toolbox::startsWith(name, "freq_trend"))
        {
          std::vector<double> const values = getDoubleVec(name);
          std::ostringstream tmpStr;
          std::copy(values.begin(), values.end() - 1,
                    std::ostream_iterator<double>(tmpStr, ", "));
          tmpStr << values.back();
          res = "[" + tmpStr.str() + "]";
        }
      else
        {
          // For everything else simply call the generic formatter.
          res = InfoSpaceHandler::formatItem(item);
        }
    }
  return res;
}
