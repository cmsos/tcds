#include "tcds/freqmon/FreqMon.h"

#include <string>
#include <vector>

#include "toolbox/string.h"
#include "xcept/Exception.h"
#include "xdaq/exception/ApplicationInstantiationFailed.h"

#include "tcds/exception/Exception.h"
#include "tcds/freqmon/ConfigurationInfoSpaceHandler.h"
#include "tcds/freqmon/FreqMonInfoSpaceHandler.h"
#include "tcds/freqmon/FreqMonInfoSpaceUpdater.h"
#include "tcds/hwlayer/DeviceBase.h"
#include "tcds/hwlayerscpi/SCPIDeviceBase.h"
#include "tcds/hwutilsscpi/HwIDInfoSpaceHandlerSCPI.h"
#include "tcds/hwutilsscpi/HwIDInfoSpaceUpdaterSCPI.h"
#include "tcds/hwutilsscpi/HwStatusInfoSpaceHandlerSCPI.h"
#include "tcds/hwutilsscpi/HwStatusInfoSpaceUpdaterSCPI.h"
#include "tcds/hwutilsscpi/Utils.h"

XDAQ_INSTANTIATOR_IMPL(tcds::freqmon::FreqMon)

tcds::freqmon::FreqMon::FreqMon(xdaq::ApplicationStub* stub)
try
  :
  tcds::utils::XDAQAppWithFSMAutomatic(stub, std::unique_ptr<tcds::hwlayer::DeviceBase>(new tcds::freqmon::SCPIDeviceFreqMon()))
    {
      // Create the InfoSpace holding all configuration information.
      cfgInfoSpaceP_ =
        std::unique_ptr<tcds::freqmon::ConfigurationInfoSpaceHandler>(new tcds::freqmon::ConfigurationInfoSpaceHandler(*this));
    }
catch (tcds::exception::Exception const& err)
  {
    std::string msgBase = "Something went wrong instantiating the FreqMon application";
    std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.what());
    XCEPT_RAISE(xdaq::exception::ApplicationInstantiationFailed, msg.c_str());
  }

tcds::freqmon::FreqMon::~FreqMon()
{
}

void
tcds::freqmon::FreqMon::setupInfoSpaces()
{
  // Make sure the configuration settings are up-to-date.
  cfgInfoSpaceP_->readInfoSpace();

  appStateInfoSpace_.setString("hwLeaseOwnerId", "n/a");

  // Instantiate all hardware-dependent InfoSpaceHandlers and
  // InfoSpaceUpdaters.
  freqMonInfoSpaceUpdaterP_ = std::unique_ptr<tcds::freqmon::FreqMonInfoSpaceUpdater>(new tcds::freqmon::FreqMonInfoSpaceUpdater(*this, getHw()));
  freqMonInfoSpaceP_ =
    std::unique_ptr<tcds::freqmon::FreqMonInfoSpaceHandler>(new tcds::freqmon::FreqMonInfoSpaceHandler(*this, freqMonInfoSpaceUpdaterP_.get()));
  hwIDInfoSpaceUpdaterP_ = std::unique_ptr<tcds::hwutilsscpi::HwIDInfoSpaceUpdaterSCPI>(new tcds::hwutilsscpi::HwIDInfoSpaceUpdaterSCPI(*this, getHw()));
  hwIDInfoSpaceP_ =
    std::unique_ptr<tcds::hwutilsscpi::HwIDInfoSpaceHandlerSCPI>(new tcds::hwutilsscpi::HwIDInfoSpaceHandlerSCPI(*this, hwIDInfoSpaceUpdaterP_.get()));
  hwStatusInfoSpaceUpdaterP_ = std::unique_ptr<tcds::hwutilsscpi::HwStatusInfoSpaceUpdaterSCPI>(new tcds::hwutilsscpi::HwStatusInfoSpaceUpdaterSCPI(*this, getHw()));
  hwStatusInfoSpaceP_ =
    std::unique_ptr<tcds::hwutilsscpi::HwStatusInfoSpaceHandlerSCPI>(new tcds::hwutilsscpi::HwStatusInfoSpaceHandlerSCPI(*this, hwStatusInfoSpaceUpdaterP_.get()));

  // Register all InfoSpaceItems with the Monitor.
  cfgInfoSpaceP_->registerItemSets(monitor_, webServer_);
  appStateInfoSpace_.registerItemSets(monitor_, webServer_);
  hwIDInfoSpaceP_->registerItemSets(monitor_, webServer_);
  hwStatusInfoSpaceP_->registerItemSets(monitor_, webServer_);
  freqMonInfoSpaceP_->registerItemSets(monitor_, webServer_);
}

tcds::freqmon::SCPIDeviceFreqMon&
tcds::freqmon::FreqMon::getHw() const
{
  return static_cast<tcds::freqmon::SCPIDeviceFreqMon&>(*hwP_.get());
}

void
tcds::freqmon::FreqMon::hwConnectImpl()
{
  // Connect to the hardware.
  tcds::hwutilsscpi::scpiDeviceHwConnectImpl(*cfgInfoSpaceP_, getHw());
}

void
tcds::freqmon::FreqMon::hwReleaseImpl()
{
  getHw().hwRelease();
}

void
tcds::freqmon::FreqMon::hwConfigureImpl()
{
  // NOTE: The following is the output from the '*LRN?' SCPI command
  // run on the device after configuring it in the desired state. It
  // could be read from a text file, of course, but this is not
  // expected (nor supposed) to change very often.
  std::vector<std::string> commands;
  commands.push_back("*RST");
  commands.push_back(":FUNC \"FREQ\"");
  commands.push_back(":CALC:AVER 0");
  commands.push_back(":CALC:LIM:LOW +0.00000000000000E+000");
  commands.push_back(":CALC:LIM 0");
  commands.push_back(":CALC:LIM:UPP +0.00000000000000E+000");
  commands.push_back(":CALC:SCAL:FUNC NULL");
  commands.push_back(":CALC:SCAL:GAIN +1.00000000000000E+000");
  commands.push_back(":CALC:SCAL:INV 0");
  commands.push_back(":CALC:SCAL:OFFS +0.00000000000000E+000");
  commands.push_back(":CALC:SCAL:REF +0.00000000000000E+000");
  commands.push_back(":CALC:SCAL:REF:AUTO 1");
  commands.push_back(":CALC:SCAL 0");
  commands.push_back(":CALC:SCAL:UNIT \"C\"");
  commands.push_back(":CALC:SCAL:UNIT:STAT 0");
  commands.push_back(":CALC:SMO:RESP FAST");
  commands.push_back(":CALC:SMO 0");
  commands.push_back(":CALC 0");
  commands.push_back(":CALC2:TRAN:HIST:POIN +100");
  commands.push_back(":CALC2:TRAN:HIST:RANG:LOW +9.99999994800928E+006");
  commands.push_back(":CALC2:TRAN:HIST:RANG:UPP +1.00000000094212E+007");
  commands.push_back(":CALC2:TRAN:HIST:RANG:AUTO:COUN +100");
  commands.push_back(":CALC2:TRAN:HIST:RANG:AUTO 1");
  commands.push_back(":CALC2:TRAN:HIST 1");
  commands.push_back(":CAL:VAL +0.00000000000000E+000");
  commands.push_back(":DATA:POIN:EVEN:THR +1");
  commands.push_back(":FORM:BORD NORM");
  commands.push_back(":FORM ASC,15");
  commands.push_back(":HCOP:SDUM:DATA:FORM PNG");
  commands.push_back(":INP:COUP AC");
  commands.push_back(":INP:FILT 0");
  commands.push_back(":INP:IMP +5.00000000E+001");
  commands.push_back(":INP:LEV +0.00000000E+000");
  commands.push_back(":INP:LEV:REL +50");
  commands.push_back(":INP:LEV2 +0.00000000E+000");
  commands.push_back(":INP:LEV2:REL +50");
  commands.push_back(":INP:LEV:AUTO 1");
  commands.push_back(":INP:NREJ 0");
  commands.push_back(":INP:PROB +1");
  commands.push_back(":INP:RANG +5.00000000E+000");
  commands.push_back(":INP:SLOP POS");
  commands.push_back(":INP:SLOP2 POS");
  commands.push_back(":INP2:COUP AC");
  commands.push_back(":INP2:FILT 0");
  commands.push_back(":INP2:IMP +5.00000000E+001");
  commands.push_back(":INP2:LEV +0.00000000E+000");
  commands.push_back(":INP2:LEV:REL +50");
  commands.push_back(":INP2:LEV2 +0.00000000E+000");
  commands.push_back(":INP2:LEV2:REL +50");
  commands.push_back(":INP2:LEV:AUTO 1");
  commands.push_back(":INP2:NREJ 0");
  commands.push_back(":INP2:PROB +1");
  commands.push_back(":INP2:RANG +5.00000000E+000");
  commands.push_back(":INP2:SLOP POS");
  commands.push_back(":INP2:SLOP2 POS");
  commands.push_back(":MMEM:CDIR \"INT:\"");
  commands.push_back(":OUTP:POL NORM");
  commands.push_back(":OUTP 0");
  commands.push_back(":SAMP:COUN +1");
  commands.push_back(":FREQ:GATE:POL NEG");
  commands.push_back(":FREQ:GATE:SOUR TIME");
  commands.push_back(":FREQ:GATE:TIME +1.00000000000000E-001");
  commands.push_back(":FREQ:MODE AUTO");
  commands.push_back(":GATE:EXT:SOUR BNC");
  commands.push_back(":GATE:STAR:DEL:EVEN +1");
  commands.push_back(":GATE:STAR:DEL:SOUR IMM");
  commands.push_back(":GATE:STAR:DEL:TIME +0.00000000000000E+000");
  commands.push_back(":GATE:STAR:SLOP NEG");
  commands.push_back(":GATE:STAR:SOUR IMM");
  commands.push_back(":GATE:STOP:HOLD:EVEN +1");
  commands.push_back(":GATE:STOP:HOLD:SOUR IMM");
  commands.push_back(":GATE:STOP:HOLD:TIME +0.00000000000000E+000");
  commands.push_back(":GATE:STOP:SLOP POS");
  commands.push_back(":GATE:STOP:SOUR IMM");
  commands.push_back(":TINT:GATE:POL NEG");
  commands.push_back(":TINT:GATE:SOUR IMM");
  commands.push_back(":TOT:GATE:POL NEG");
  commands.push_back(":TOT:GATE:SOUR TIME");
  commands.push_back(":TOT:GATE:TIME +1.00000000000000E-001");
  commands.push_back(":TRIG:COUN +1");
  commands.push_back(":TRIG:DEL +0.00000000000000E+000");
  commands.push_back(":TRIG:SLOP NEG");
  commands.push_back(":TRIG:SOUR IMM");

  // For some reason the following are not included in the output of
  // *LRN?.
  commands.push_back("ROSCillator:EXTernal:FREQuency 10.0E6");
  commands.push_back("ROSCillator:SOURce:AUTO ON");

  tcds::hwlayerscpi::SCPIDeviceBase const& hw = getHw();
  hw.reset();
  for (std::vector<std::string>::const_iterator it = commands.begin();
       it != commands.end();
       ++it)
    {
      hw.execCommand(*it);
    }
}
