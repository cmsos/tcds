#include "tcds/freqmon/FreqMonInfoSpaceUpdater.h"

#include <stdint.h>
#include <vector>
#include <iomanip>

#include "toolbox/TimeInterval.h"
#include "toolbox/string.h"
#include "xcept/Exception.h"

#include "tcds/exception/Exception.h"
#include "tcds/freqmon/Definitions.h"
#include "tcds/freqmon/FreqMonDataPoint.h"
#include "tcds/freqmon/SCPIDeviceFreqMon.h"
#include "tcds/hwlayerscpi/Definitions.h"
#include "tcds/utils/ConfigurationInfoSpaceHandler.h"
#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/XDAQAppBase.h"

namespace toolbox {
  namespace exception {
  class Exception;
  }
}

tcds::freqmon::FreqMonInfoSpaceUpdater::FreqMonInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                                                tcds::freqmon::SCPIDeviceFreqMon const& hw) :
  tcds::utils::HwInfoSpaceUpdaterBase(xdaqApp, hw)
{
  tcds::utils::ConfigurationInfoSpaceHandler const& cfgInfoSpace =
    getOwnerApplication().getConfigurationInfoSpaceHandler();

  numAvg_ = cfgInfoSpace.getUInt32("freqMonitorNumAvg");

  deadband_ = cfgInfoSpace.getFloat("freqMonitorDeadBand");
  std::string tmp = cfgInfoSpace.getString("freqMonitorDeadBandOverrideTime");
  try
    {
      deadbandOverrideTime_.fromString(tmp);
    }
  catch (toolbox::exception::Exception& err)
    {
      std::string const msg =
        toolbox::toString("Failed to interpret '%s' as dead-band override interval.",
                          tmp.c_str());
      XCEPT_RAISE(tcds::exception::ValueError, msg.c_str());
    }

  historyMaxNumPoints_ = cfgInfoSpace.getUInt32("historyMaxNumPoints");
  toolbox::TimeInterval const historyMaxDuration(15, 0);
  tmp = cfgInfoSpace.getString("historyMaxDuration");
  try
    {
      historyMaxDuration_.fromString(tmp);
    }
  catch (toolbox::exception::Exception& err)
    {
      std::string const msg =
        toolbox::toString("Failed to interpret '%s' as maximum history duration interval.",
                          tmp.c_str());
      XCEPT_RAISE(tcds::exception::ValueError, msg.c_str());
    }
}

void
tcds::freqmon::FreqMonInfoSpaceUpdater::updateInfoSpaceImpl(tcds::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  uint32_t const srcId = infoSpaceHandler->getUInt32("freq_source_id_number");
  bool const srcIsConnected = infoSpaceHandler->getBool("freq_source_is_connected");

  toolbox::TimeVal const timeNow = toolbox::TimeVal::gettimeofday();
  tcds::freqmon::SCPIDeviceFreqMon const& hw = getHw();
  if (hw.isHwConnected() && srcIsConnected)
    {
      switch (srcId)
        {
        case tcds::definitions::FREQ_SOURCE_INPUT1:
        case tcds::definitions::FREQ_SOURCE_INPUT2:
          break;
        default:
          XCEPT_RAISE(tcds::exception::ValueError,
                      toolbox::toString("Trying to update monitoring info "
                                        "for an unknown frequency monitor "
                                        "input ID: %d",
                                        srcId));
          break;
        }

      // Get a new frequency measurement from the hardware.
      tcds::definitions::FREQ_SOURCE const srcIdEnum =
        static_cast<tcds::definitions::FREQ_SOURCE>(srcId);
      double const valueCurr = hw.readFreqMeasurement(srcIdEnum);
      tcds::definitions::REF_SOURCE const refSource = hw.getReferenceSource();

      // Now we have to see if things have changed sufficiently since
      // last time to warrant storing the data and starting a new
      // collection.
      tcds::freqmon::FreqMonDataPoint const dataPointCurr(timeNow, valueCurr, refSource);

      std::string const key = infoSpaceHandler->name();
      tcds::freqmon::FreqMonDataPointCollection& dataPoints = dataPoints_[key];
      tcds::freqmon::FreqMonDataPointCollection dataPointsPrev;
      if (dataPointsPrev_[key].size() > 0)
        {
          dataPointsPrev = dataPointsPrev_[key].back();
        }
      toolbox::TimeVal& timePreviousUpdate = updateTimestamps_[key];
      bool& firstConnection = firstConnection_[key];
      if (dataPoints.size() != 0)
        {
          tcds::freqmon::FreqMonDataPoint const& dataPointPrev = dataPoints.back();

          // See if the current point can be added to what we already
          // have or not.
          if (!dataPointCurr.isCompatible(dataPointPrev))
            {
              // Store what we have and start over based on our
              // current data point.
              storeAndStartOver(infoSpaceHandler, timeNow);
            }
        }
      {
        // It looks as if nothing has changed, or we cleaned up what
        // we had, so let's add the current data point to our
        // collection.
        dataPoints.push_back(dataPointCurr);

        // Now check if there is reason to store and start over
        // based on normal conditions.
        if ((dataPoints.size() == numAvg_) || firstConnection)
          {
            // We have reached our target number of points to
            // average over. So we potentially have a new set of
            // values to store.
            bool const isStore = infoSpaceHandler->getBool("is_store");
            bool const ignoreDeadband = !isStore;

            if (firstConnection
                || ignoreDeadband
                || dataPoints.differsFrom(dataPointsPrev, deadband_)
                || (toolbox::TimeInterval(timeNow - timePreviousUpdate) >= deadbandOverrideTime_))
              {
                storeAndStartOver(infoSpaceHandler, timeNow);
              }
            else
              {
                dataPoints.clear();
              }
            firstConnection = false;
          }
      }

      infoSpaceHandler->setValid();
    }
  else
    {
      std::string const key = infoSpaceHandler->name();
      tcds::freqmon::FreqMonDataPointCollection& dataPoints = dataPoints_[key];
      if (dataPoints.size())
        {
          storeAndStartOver(infoSpaceHandler, timeNow);
        }

      infoSpaceHandler->setInvalid();

      bool& firstConnection = firstConnection_[key];
      firstConnection = true;
    }
}

tcds::freqmon::SCPIDeviceFreqMon const&
tcds::freqmon::FreqMonInfoSpaceUpdater::getHw() const
{
  return static_cast<tcds::freqmon::SCPIDeviceFreqMon const&>(tcds::utils::HwInfoSpaceUpdaterBase::getHw());
}

void
tcds::freqmon::FreqMonInfoSpaceUpdater::storeAndStartOver(tcds::utils::InfoSpaceHandler* const infoSpaceHandler,
                                                          toolbox::TimeVal const& timeNow)
{
  std::string const key = infoSpaceHandler->name();

  tcds::freqmon::FreqMonDataPointCollection& dataPoints = dataPoints_.at(key);
  toolbox::TimeVal const time = dataPoints.timestamp();
  infoSpaceHandler->setTimeVal("freq_source_time", time);
  double const freq = dataPoints.frequency();
  infoSpaceHandler->setDouble("freq_source_value", freq);

  tcds::freqmon::FreqMonDataPointSeries& dataPointsPrev = dataPointsPrev_.at(key);
  dataPointsPrev.push_back(dataPoints);
  while ((dataPointsPrev.size() > historyMaxNumPoints_) &&
         (toolbox::TimeInterval(dataPointsPrev.back().timestamp() - dataPointsPrev.front().timestamp())
          > historyMaxDuration_))
    {
      dataPointsPrev.erase(dataPointsPrev.begin());
    }
  dataPoints.clear();

  // NOTE: This one is a bit tricky. Keeping track of the history as a
  // string (basically encoded as JSON). But it is the best I could do
  // without lots of (more fundamental) changes.
  std::string const jsonString = dataPointsPrev.getJSONString();
  infoSpaceHandler->setString("freq_source_trend", jsonString);

  toolbox::TimeVal& timePreviousUpdate = updateTimestamps_.at(key);
  timePreviousUpdate = timeNow;

  infoSpaceHandler->writeInfoSpace();
}
