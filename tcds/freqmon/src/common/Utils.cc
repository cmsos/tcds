#include "tcds/freqmon/Utils.h"

#include "toolbox/string.h"
#include "xcept/Exception.h"

#include "tcds/exception/Exception.h"
#include "tcds/freqmon/Definitions.h"

std::map<tcds::definitions::FREQ_SOURCE, std::string>
tcds::freqmon::freqSourceNameMap()
{
  std::map<tcds::definitions::FREQ_SOURCE, std::string> names;

  names[tcds::definitions::FREQ_SOURCE_INPUT1] = "Input1";
  names[tcds::definitions::FREQ_SOURCE_INPUT2] = "Input2";

  return names;
}

std::string
tcds::freqmon::freqChannelToName(tcds::definitions::FREQ_SOURCE const channel)
{
  std::map<tcds::definitions::FREQ_SOURCE, std::string> names = freqSourceNameMap();
  std::map<tcds::definitions::FREQ_SOURCE, std::string>::const_iterator i = names.find(channel);
  if (i == names.end())
    {
      XCEPT_RAISE(tcds::exception::ValueError,
                  toolbox::toString("Number %d is not a valid frequency channel (for input name lookups).",
                                    channel));
    }
  return i->second;
}
