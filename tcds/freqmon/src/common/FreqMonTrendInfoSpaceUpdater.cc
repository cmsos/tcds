#include "tcds/freqmon/FreqMonTrendInfoSpaceUpdater.h"

#include "toolbox/string.h"
#include "xcept/Exception.h"

#include "tcds/exception/Exception.h"
#include "tcds/utils/ConfigurationInfoSpaceHandler.h"
#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/XDAQAppBase.h"

namespace toolbox {
  namespace exception {
  class Exception;
  }
}

tcds::freqmon::FreqMonTrendInfoSpaceUpdater::FreqMonTrendInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                                                tcds::freqmon::SCPIDeviceFreqMon const& hw) :
  tcds::utils::HwInfoSpaceUpdaterBase(xdaqApp, hw)
{
}

tcds::freqmon::SCPIDeviceFreqMon const&
tcds::freqmon::FreqMonTrendInfoSpaceUpdater::getHw() const
{
  return static_cast<tcds::freqmon::SCPIDeviceFreqMon const&>(tcds::utils::HwInfoSpaceUpdaterBase::getHw());
}

bool
tcds::freqmon::FreqMonTrendInfoSpaceUpdater::updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                                                 tcds::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  bool updated = false;
  tcds::freqmon::SCPIDeviceFreqMon const& hw = getHw();
  if (hw.isHwConnected())
    {
      std::string name = item.name();
      tcds::utils::InfoSpaceItem::UpdateType updateType = item.updateType();
      if (updateType == tcds::utils::InfoSpaceItem::PROCESS)
        {
          // The 'PROCESS' update type means that there is something
          // special to the variable. Figure out what to do based on the
          // variable name.
          if (toolbox::startsWith(name, "freq_trend_"))
            {
              tcds::definitions::FREQ_SOURCE srcId = extractFreqSourceNumber(name);
              double const valueCurr = hw.readFreqMeasurement(srcId);

              std::vector<double> newVal = infoSpaceHandler->getDoubleVec(name);
              newVal.push_back(valueCurr);
              infoSpaceHandler->setDoubleVec(name, newVal);
              updated = true;
            }
        }
      if (!updated)
        {
          updated = tcds::utils::HwInfoSpaceUpdaterBase::updateInfoSpaceItem(item, infoSpaceHandler);
        }
    }

  if (updated)
    {
      item.setValid();
    }
  else
    {
      item.setInvalid();
    }

  return updated;
}

tcds::definitions::FREQ_SOURCE
tcds::freqmon::FreqMonTrendInfoSpaceUpdater::extractFreqSourceNumber(std::string const& regName) const
{
  // TODO TODO TODO
  // This could be done in a nicer way.
  std::string const patLo = "freq_trend_";
  size_t const posLo = regName.find(patLo);
  std::stringstream tmp;
  tmp << regName.substr(posLo + patLo.size());
  int srcIdTmp;
  tmp >> srcIdTmp;
  tcds::definitions::FREQ_SOURCE const srcId =
    static_cast<tcds::definitions::FREQ_SOURCE>(srcIdTmp);
  // TODO TODO TODO end
  return srcId;
}
