#include "tcds/freqmon/FreqMonDataPointSeries.h"

#include <iomanip>
#include <sstream>

#include "toolbox/TimeVal.h"

#include "tcds/utils/Utils.h"

tcds::freqmon::FreqMonDataPointSeries::FreqMonDataPointSeries()
{
}

void
tcds::freqmon::FreqMonDataPointSeries::push_back(tcds::freqmon::FreqMonDataPointCollection const& dataPointCollection)
{
  dataPointCollections_.push_back(dataPointCollection);
}

std::vector<tcds::freqmon::FreqMonDataPointCollection>::iterator
tcds::freqmon::FreqMonDataPointSeries::begin()
{
  std::vector<tcds::freqmon::FreqMonDataPointCollection>::iterator tmp = dataPointCollections_.begin();
  return tmp;
}

tcds::freqmon::FreqMonDataPointCollection
tcds::freqmon::FreqMonDataPointSeries::front() const
{
  return dataPointCollections_.front();
}

tcds::freqmon::FreqMonDataPointCollection
tcds::freqmon::FreqMonDataPointSeries::back() const
{
  return dataPointCollections_.back();
}

std::vector<tcds::freqmon::FreqMonDataPointCollection>::iterator
tcds::freqmon::FreqMonDataPointSeries::erase(std::vector<tcds::freqmon::FreqMonDataPointCollection>::iterator position)
{
  return dataPointCollections_.erase(position);
}

size_t
tcds::freqmon::FreqMonDataPointSeries::size() const
{
  return dataPointCollections_.size();
}

void
tcds::freqmon::FreqMonDataPointSeries::clear()
{
  dataPointCollections_.clear();
}

std::string
tcds::freqmon::FreqMonDataPointSeries::getJSONString() const
{
  std::stringstream res;

  res << "[";
  size_t index = 0;
  for (std::vector<tcds::freqmon::FreqMonDataPointCollection>::const_iterator it = dataPointCollections_.begin();
       it != dataPointCollections_.end();
       ++it)
    {
      res << "{";

      // The timestamp.
      res << "\"Timestamp\": \""
          << tcds::utils::formatTimestamp(it->timestamp(), toolbox::TimeVal::gmt, true)
          << "\"";
      res << ", ";

      // The frequency value.
      res << "\"Frequency\": \""
          << std::setprecision(9) << it->frequency()
          << "\"";

      res << "}";
      if (index != (dataPointCollections_.size() - 1))
        {
          res << ", ";
        }
      ++index;
    }
  res << "]";

  return res.str();
}
