#include "tcds/freqmon/InfoSpaceHandler.h"

#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Utils.h"

tcds::freqmon::InfoSpaceHandler::InfoSpaceHandler(xdaq::Application& xdaqApp,
                                                  std::string const& name,
                                                  tcds::utils::InfoSpaceUpdater* updater) :
  tcds::utils::InfoSpaceHandler(xdaqApp, name, updater)
{
}

tcds::freqmon::InfoSpaceHandler::~InfoSpaceHandler()
{
}

void
tcds::freqmon::InfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  tcds::utils::InfoSpaceHandler::registerItemSetsWithMonitor(monitor);
}

void
tcds::freqmon::InfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                               tcds::utils::Monitor& monitor,
                                                               std::string const& forceTabName)
{
  tcds::utils::InfoSpaceHandler::registerItemSetsWithWebServer(webServer, monitor, forceTabName);
}

std::string
tcds::freqmon::InfoSpaceHandler::formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const
{
  std::string res = tcds::utils::escapeAsJSONString(tcds::utils::InfoSpaceHandler::kInvalidItemString);
  if (item->isValid())
    {
      std::string name = item->name();
      if (name == "freq_source_trend")
        {
          // Special in the sense that this is something that needs to
          // be interpreted as a proper JavaScript object, so let's
          // not add any more double quotes.
          res = getString(name);
        }
      else
        {
          // For everything else simply call the generic formatter.
          res = tcds::utils::InfoSpaceHandler::formatItem(item);
        }
    }
  return res;
}
