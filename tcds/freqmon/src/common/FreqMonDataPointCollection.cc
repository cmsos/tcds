#include "tcds/freqmon/FreqMonDataPointCollection.h"

#include <cmath>
#include <sstream>

#include <iomanip>

tcds::freqmon::FreqMonDataPointCollection::FreqMonDataPointCollection()
{
}

void
tcds::freqmon::FreqMonDataPointCollection::push_back(tcds::freqmon::FreqMonDataPoint const& dataPoint)
{
  dataPoints_.push_back(dataPoint);
}

tcds::freqmon::FreqMonDataPoint
tcds::freqmon::FreqMonDataPointCollection::front() const
{
  return dataPoints_.front();
}

tcds::freqmon::FreqMonDataPoint
tcds::freqmon::FreqMonDataPointCollection::back() const
{
  return dataPoints_.back();
}

toolbox::TimeVal
tcds::freqmon::FreqMonDataPointCollection::timestamp() const
{
  toolbox::TimeVal res;
  if (dataPoints_.size() == 1)
    {
      res = dataPoints_.front().timestamp();
    }
  else if (dataPoints_.size() > 2)
    {
      toolbox::TimeVal const from = dataPoints_.front().timestamp();
      toolbox::TimeVal const to = dataPoints_.back().timestamp();
      res = (to + from) / 2.;
    }
  return res;
}

double
tcds::freqmon::FreqMonDataPointCollection::frequency() const
{
  // Just the average of the values of the individual points.
  double sum = 0.;
  for (std::vector<tcds::freqmon::FreqMonDataPoint>::const_iterator i = dataPoints_.begin();
       i != dataPoints_.end();
       ++i)
    {
      sum += i->frequency();
    }
  double const res = sum / dataPoints_.size();
  return res;
}

size_t
tcds::freqmon::FreqMonDataPointCollection::size() const
{
  return dataPoints_.size();
}

void
tcds::freqmon::FreqMonDataPointCollection::clear()
{
  dataPoints_.clear();
}

std::string
tcds::freqmon::FreqMonDataPointCollection::getJSONString() const
{
  std::stringstream res;

  res << "[";
  size_t index = 0;
  for (std::vector<tcds::freqmon::FreqMonDataPoint>::const_iterator it = dataPoints_.begin();
       it != dataPoints_.end();
       ++it)
    {
      res << "{";

      // The timestamp.
      res << "\"Timestamp\": \"" << it->timestamp() << "\"";
      res << ", ";

      // The frequency value.
      res << "\"Frequency\": \"" << it->frequency() << "\"";

      res << "}";
      if (index != (dataPoints_.size() - 1))
        {
          res << ", ";
        }
      ++index;
    }
  res << "]";

  return res.str();
}

bool
tcds::freqmon::FreqMonDataPointCollection::differsFrom(FreqMonDataPointCollection const& other,
                                                       double const abs) const
{
  bool res = (absDiff(frequency(), other.frequency()) >= abs);

  return res;
}

double
tcds::freqmon::FreqMonDataPointCollection::absDiff(double const self, double const other)
{
  double res = fabs(self - other);

  return res;
}
