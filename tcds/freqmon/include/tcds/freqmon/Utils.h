#ifndef _tcds_freqmon_Utils_h_
#define _tcds_freqmon_Utils_h_

#include <map>
#include <string>

#include "tcds/freqmon/Definitions.h"

namespace tcds {
  namespace freqmon {

    // Mapping of frequency channels to names.
    std::map<tcds::definitions::FREQ_SOURCE, std::string> freqSourceNameMap();
    std::string freqChannelToName(tcds::definitions::FREQ_SOURCE const channel);

  } // namespace freqmon
} // namespace tcds

#endif // _tcds_freqmon_Utils_h_
