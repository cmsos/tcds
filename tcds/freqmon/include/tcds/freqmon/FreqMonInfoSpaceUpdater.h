#ifndef _tcds_freqmon_FreqMonInfoSpaceUpdater_h_
#define _tcds_freqmon_FreqMonInfoSpaceUpdater_h_

#include <map>
#include <stddef.h>
#include <string>

#include "toolbox/TimeInterval.h"
#include "toolbox/TimeVal.h"

#include "tcds/freqmon/FreqMonDataPointCollection.h"
#include "tcds/freqmon/FreqMonDataPointSeries.h"
#include "tcds/utils/HwInfoSpaceUpdaterBase.h"

namespace tcds {
  namespace utils {
    class InfoSpaceHandler;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace freqmon {

    class SCPIDeviceFreqMon;

    class FreqMonInfoSpaceUpdater : public tcds::utils::HwInfoSpaceUpdaterBase
    {

    public:
      FreqMonInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                              tcds::freqmon::SCPIDeviceFreqMon const& hw);

    protected:
      virtual void updateInfoSpaceImpl(tcds::utils::InfoSpaceHandler* const infoSpaceHandler);

    private:
      // Several parameters determining:
      // - How many single measurements to average into each data
      //   point.
      size_t numAvg_;
      // - The dead-band (in Hz) applied to data points before
      //   updating.
      float deadband_;
      // - The dead-band override time-out. If the value does not exit
      //   the dead-band within this period, the data point is updated
      //   anyway.
      toolbox::TimeInterval deadbandOverrideTime_;

      // - The maximum number of points to keep in the short-term
      //   measurement histories.
      size_t historyMaxNumPoints_;
      // - The maximum duration of the short-term measurement
      //   histories.
      toolbox::TimeInterval historyMaxDuration_;

      // Our memory of the previous data points we took.
      // NOTE: Since each infospace updater can be used for multiple
      // infospaces, this information needs to be indexed by
      // infospace.
      std::map<std::string, tcds::freqmon::FreqMonDataPointCollection > dataPoints_;
      std::map<std::string, tcds::freqmon::FreqMonDataPointSeries> dataPointsPrev_;
      std::map<std::string, toolbox::TimeVal> updateTimestamps_;

      // This variable is used to keep track of hardware
      // (dis)connections. It helps us provide a preliminary
      // single-measurement data point every time we (re)connect to
      // the hardware. (As opposed to waiting for a full round of
      // averaging.)
      std::map<std::string, bool> firstConnection_;

      tcds::freqmon::SCPIDeviceFreqMon const& getHw() const;

      void storeAndStartOver(tcds::utils::InfoSpaceHandler* const infoSpaceHandler,
                             toolbox::TimeVal const& timeNow);
    };

  } // namespace freqmon
} // namespace tcds

#endif // _tcds_pi_FreqMonInfoSpaceUpdater_h_
