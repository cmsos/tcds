#ifndef _tcds_freqmon_FreqMonTrendInfoSpaceHandler_h_
#define _tcds_freqmon_FreqMonTrendInfoSpaceHandler_h_

#include <map>
#include <string>
#include <vector>

#include "tcds/freqmon/Definitions.h"
#include "tcds/utils/InfoSpaceHandler.h"

namespace tcds {
  namespace utils {
    class InfoSpaceUpdater;
    class Monitor;
    class WebServer;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace freqmon {

    class FreqMonTrendInfoSpaceHandler : public tcds::utils::InfoSpaceHandler
    {

    public:
      FreqMonTrendInfoSpaceHandler(xdaq::Application& xdaqApp,
                                   tcds::utils::InfoSpaceUpdater* updater);
      virtual ~FreqMonTrendInfoSpaceHandler();

      tcds::utils::XDAQAppBase& getOwnerApplication() const;

    protected:
      virtual void registerItemSetsWithMonitor(tcds::utils::Monitor& monitor);
      virtual void registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                 tcds::utils::Monitor& monitor,
                                                 std::string const& forceTabName="");
      virtual std::string formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const;

    private:
      std::map<tcds::definitions::FREQ_SOURCE, std::string> inputNames_;
      std::map<tcds::definitions::FREQ_SOURCE, std::string> inputLabels_;

      // void createFreqChannel(tcds::utils::InfoSpaceHandler* const handler,
      //                        std::string const sourceLabel,
      //                        unsigned int const idNumber);

    };

  } // namespace freqmon
} // namespace tcds

#endif // _tcds_freqmon_FreqMonTrendInfoSpaceHandler_h_
