#ifndef _tcds_freqmon_FreqMon_h_
#define _tcds_freqmon_FreqMon_h_

#include <memory>

#include "xdaq/Application.h"

#include "tcds/freqmon/SCPIDeviceFreqMon.h"
#include "tcds/utils/XDAQAppWithFSMAutomatic.h"

namespace xdaq {
  class ApplicationStub;
}

namespace tcds {
  namespace hwutilsscpi {
    class HwIDInfoSpaceHandlerSCPI;
    class HwIDInfoSpaceUpdaterSCPI;
    class HwStatusInfoSpaceHandlerSCPI;
    class HwStatusInfoSpaceUpdaterSCPI;
  }
}

namespace tcds {
  namespace freqmon {

    class FreqMonInfoSpaceHandler;
    class FreqMonInfoSpaceUpdater;

    class FreqMon : public tcds::utils::XDAQAppWithFSMAutomatic
    {

    public:
      XDAQ_INSTANTIATOR();

      FreqMon(xdaq::ApplicationStub* stub);
      virtual ~FreqMon();

    protected:
      virtual void setupInfoSpaces();

      virtual SCPIDeviceFreqMon& getHw() const;

      virtual void hwConnectImpl();
      virtual void hwReleaseImpl();
      virtual void hwConfigureImpl();

    private:
      // Various InfoSpaces and their InfoSpaceUpdaters.
      std::unique_ptr<tcds::freqmon::FreqMonInfoSpaceUpdater> freqMonInfoSpaceUpdaterP_;
      std::unique_ptr<tcds::freqmon::FreqMonInfoSpaceHandler> freqMonInfoSpaceP_;
      std::unique_ptr<tcds::hwutilsscpi::HwIDInfoSpaceUpdaterSCPI> hwIDInfoSpaceUpdaterP_;
      std::unique_ptr<tcds::hwutilsscpi::HwIDInfoSpaceHandlerSCPI> hwIDInfoSpaceP_;
      std::unique_ptr<tcds::hwutilsscpi::HwStatusInfoSpaceUpdaterSCPI> hwStatusInfoSpaceUpdaterP_;
      std::unique_ptr<tcds::hwutilsscpi::HwStatusInfoSpaceHandlerSCPI> hwStatusInfoSpaceP_;

    };

  } // namespace freqmon
} // namespace tcds

#endif // _tcds_freqmon_FreqMon_h_
