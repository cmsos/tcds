#ifndef _tcds_freqmon_SCPIDeviceFreqMon_h_
#define _tcds_freqmon_SCPIDeviceFreqMon_h_

#include "tcds/freqmon/Definitions.h"
#include "tcds/hwlayerscpi/Definitions.h"
#include "tcds/hwlayerscpi/SCPIDeviceBase.h"

namespace tcds {
  namespace freqmon {

    class SCPIDeviceFreqMon : public tcds::hwlayerscpi::SCPIDeviceBase
    {

    public:
      SCPIDeviceFreqMon();
      virtual ~SCPIDeviceFreqMon();

      virtual tcds::definitions::REF_SOURCE getReferenceSource() const;
      double readFreqMeasurement(tcds::definitions::FREQ_SOURCE const srcId) const;

    };

  } // namespace freqmon
} // namespace tcds

#endif // _tcds_freqmon_SCPIDeviceFreqMon_h_
