#ifndef _tcds_freqmon_FreqMonDataPoint_h_
#define _tcds_freqmon_FreqMonDataPoint_h_

#include "toolbox/TimeVal.h"

#include "tcds/hwlayerscpi/Definitions.h"

namespace tcds {
  namespace freqmon {

    class FreqMonDataPoint
    {
    public:
      FreqMonDataPoint(toolbox::TimeVal const& timestamp,
                       double const frequency,
                       tcds::definitions::REF_SOURCE const& referenceSource);

      toolbox::TimeVal timestamp() const;
      double frequency() const;

      bool isCompatible(FreqMonDataPoint const& other) const;
      bool haveConditionsChanged(FreqMonDataPoint const& other) const;

    private:
      toolbox::TimeVal timestamp_;
      double frequency_;
      tcds::definitions::REF_SOURCE referenceSource_;
    };

  } // namespace freqmon
} // namespace tcds

#endif // _tcds_pi_FreqMonDataPoint_h_
