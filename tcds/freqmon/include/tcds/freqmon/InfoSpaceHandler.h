#ifndef _tcds_freqmon_InfoSpaceHandler_h_
#define _tcds_freqmon_InfoSpaceHandler_h_

#include <string>

#include "tcds/utils/InfoSpaceHandler.h"

namespace xdaq {
  class Application;
}

namespace tcds {
  namespace utils {
    class InfoSpaceUpdater;
    class Monitor;
    class WebServer;
  }
}

namespace tcds {
  namespace freqmon {

    class InfoSpaceHandler : public tcds::utils::InfoSpaceHandler
    {

    public:
      InfoSpaceHandler(xdaq::Application& xdaqApp,
                       std::string const& name,
                       tcds::utils::InfoSpaceUpdater* updater);
      virtual ~InfoSpaceHandler();

    protected:
      virtual void registerItemSetsWithMonitor(tcds::utils::Monitor& monitor);
      virtual void registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                 tcds::utils::Monitor& monitor,
                                                 std::string const& forceTabName="");

      virtual std::string formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const;

    };

  } // namespace freqmon
} // namespace tcds

#endif // _tcds_freqmon_InfoSpaceHandler_h_
