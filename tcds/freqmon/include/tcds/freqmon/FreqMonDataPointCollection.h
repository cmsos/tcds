#ifndef _tcds_freqmon_FreqMonDataPointCollection_h_
#define _tcds_freqmon_FreqMonDataPointCollection_h_

#include <cstddef>
#include <string>
#include <vector>

#include "toolbox/TimeVal.h"

#include "tcds/freqmon/FreqMonDataPoint.h"

namespace tcds {
  namespace freqmon {

    class FreqMonDataPointCollection
    {

    public:
      FreqMonDataPointCollection();

      void push_back(tcds::freqmon::FreqMonDataPoint const& dataPoint);
      tcds::freqmon::FreqMonDataPoint front() const;
      tcds::freqmon::FreqMonDataPoint back() const;

      toolbox::TimeVal timestamp() const;
      double frequency() const;

      size_t size() const;
      void clear();

      std::string getJSONString() const;

      bool differsFrom(FreqMonDataPointCollection const& other, double const abs) const;

    private:
      std::vector<tcds::freqmon::FreqMonDataPoint> dataPoints_;

      static double absDiff(double const self, double const other);

    };

  } // namespace freqmon
} // namespace tcds

#endif // _tcds_freqmon_FreqMonDataPointCollection_h_
