#ifndef _tcds_freqmon_Definitions_h_
#define _tcds_freqmon_Definitions_h_

#include <string>

namespace tcds {
  namespace definitions {

    enum FREQ_SOURCE {FREQ_SOURCE_INPUT1=1,
                      FREQ_SOURCE_INPUT2=2};
    FREQ_SOURCE const FREQ_SOURCE_MIN = FREQ_SOURCE_INPUT1;
    FREQ_SOURCE const FREQ_SOURCE_MAX = FREQ_SOURCE_INPUT2;

    // Define an 'unused' signal name which can be used to indicate
    // unused/disconnected signal inputs.
    std::string const kUnusedInputLabel = "Unused";

  } // namespace definitions
} // namespace tcds

#endif // _tcds_freqmon_Definitions_h_
