#ifndef _tcds_freqmon_FreqMonTrendInfoSpaceUpdater_h_
#define _tcds_freqmon_FreqMonTrendInfoSpaceUpdater_h_

#include <stddef.h>
#include <string>

#include "tcds/freqmon/Definitions.h"
#include "tcds/freqmon/SCPIDeviceFreqMon.h"
#include "tcds/utils/HwInfoSpaceUpdaterBase.h"

namespace tcds {
  namespace utils {
    class InfoSpaceHandler;
    class InfoSpaceItem;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace freqmon {

    class FreqMonTrendInfoSpaceUpdater : public tcds::utils::HwInfoSpaceUpdaterBase
    {

    public:
      FreqMonTrendInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                   tcds::freqmon::SCPIDeviceFreqMon const& hw);

    protected:
      virtual bool updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                       tcds::utils::InfoSpaceHandler* const infoSpaceHandler);

    private:
      tcds::freqmon::SCPIDeviceFreqMon const& getHw() const;

      tcds::definitions::FREQ_SOURCE extractFreqSourceNumber(std::string const& regName) const;
    };

  } // namespace freqmon
} // namespace tcds

#endif // _tcds_pi_FreqMonTrendInfoSpaceUpdater_h_
