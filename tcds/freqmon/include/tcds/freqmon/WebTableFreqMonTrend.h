#ifndef _tcds_freqmon_WebTableFreqMonTrend_h
#define _tcds_freqmon_WebTableFreqMonTrend_h

#include <cstddef>
#include <string>

#include "tcds/utils/WebObject.h"

namespace tcds {
  namespace utils {
    class Monitor;
  }
}

namespace tcds {
  namespace freqmon {

    class WebTableFreqMonTrend : public tcds::utils::WebObject
    {

    public:
      WebTableFreqMonTrend(std::string const& name,
                           std::string const& description,
                           tcds::utils::Monitor const& monitor,
                           std::string const& itemSetName,
                           std::string const& tabName,
                           size_t const colSpan);

      std::string getHTMLString() const;

    };

  } // namespace freqmon
} // namespace tcds

#endif // _tcds_freqmon_WebTableFreqMonTrend_h
