#ifndef _tcds_freqmon_FreqMonDataPointSeries_h_
#define _tcds_freqmon_FreqMonDataPointSeries_h_

#include <stddef.h>
#include <string>
#include <vector>

#include "tcds/freqmon/FreqMonDataPointCollection.h"

namespace tcds {
  namespace freqmon {

    class FreqMonDataPointSeries
    {

    public:
      FreqMonDataPointSeries();

      void push_back(tcds::freqmon::FreqMonDataPointCollection const& dataPointCollection);
      std::vector<tcds::freqmon::FreqMonDataPointCollection>::iterator begin();
      tcds::freqmon::FreqMonDataPointCollection front() const;
      tcds::freqmon::FreqMonDataPointCollection back() const;

      std::vector<tcds::freqmon::FreqMonDataPointCollection>::iterator
      erase(std::vector<tcds::freqmon::FreqMonDataPointCollection>::iterator position);

      size_t size() const;
      void clear();

      std::string getJSONString() const;

    private:
      std::vector<tcds::freqmon::FreqMonDataPointCollection> dataPointCollections_;

    };

  } // namespace freqmon
} // namespace tcds

#endif // _tcds_freqmon_FreqMonDataPointSeries_h_
