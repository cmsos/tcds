#ifndef _tcds_freqmon_FreqMonInfoSpaceHandler_h_
#define _tcds_freqmon_FreqMonInfoSpaceHandler_h_

#include <map>
#include <string>

#include "tcds/freqmon/Definitions.h"
#include "tcds/utils/MultiInfoSpaceHandler.h"

namespace xdaq {
  class Application;
}

namespace tcds {
  namespace utils {
    class InfoSpaceHandler;
    class InfoSpaceUpdater;
    class Monitor;
    class WebServer;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace freqmon {

    class FreqMonInfoSpaceHandler : public tcds::utils::MultiInfoSpaceHandler
    {

    public:
      FreqMonInfoSpaceHandler(xdaq::Application& xdaqApp,
                              tcds::utils::InfoSpaceUpdater* updater);
      virtual ~FreqMonInfoSpaceHandler();

      tcds::utils::XDAQAppBase& getOwnerApplication() const;

    protected:
      virtual void registerItemSetsWithMonitor(tcds::utils::Monitor& monitor);
      virtual void registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                 tcds::utils::Monitor& monitor,
                                                 std::string const& forceTabName="");

    private:
      std::map<tcds::definitions::FREQ_SOURCE, std::string> inputLabels_;
      std::map<tcds::definitions::FREQ_SOURCE, bool> inputIsConnected_;

      void createFreqChannel(tcds::utils::InfoSpaceHandler* const handler,
                             std::string const sourceLabel,
                             unsigned int const idNumber,
                             bool const isConnected,
                             bool const isStore);

    };

  } // namespace freqmon
} // namespace tcds

#endif // _tcds_freqmon_FreqMonInfoSpaceHandler_h_
