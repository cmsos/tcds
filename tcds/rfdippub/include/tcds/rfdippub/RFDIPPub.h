#ifndef _tcds_rfdippub_RFDIPPub_h_
#define _tcds_rfdippub_RFDIPPub_h_

#include <memory>

#include "xdaq/Application.h"

#include "tcds/rfdippub/RFInfoDAQLoop.h"
#include "tcds/utils/XDAQAppBase.h"

namespace xdaq {
  class ApplicationStub;
}

namespace xdata {
  class Event;
}

namespace tcds {
  namespace rfdippub {

    class RF2TTCDataInfoSpaceHandler;
    class RF2TTCDataInfoSpaceUpdater;
    class RFRXDDataInfoSpaceHandler;
    class RFRXDDataInfoSpaceUpdater;

    class RFDIPPub : public tcds::utils::XDAQAppBase
    {
      friend class RFInfoDAQLoop;
      friend class RFInfoPublisher;

    public:
      XDAQ_INSTANTIATOR();

      RFDIPPub(xdaq::ApplicationStub* const stub);
      virtual ~RFDIPPub();

    protected:
      virtual void setupInfoSpaces();

      virtual void actionPerformed(xdata::Event& event);

      // Dummy methods in this case.
      virtual void hwConnectImpl();
      virtual void hwReleaseImpl();
      virtual void hwConfigureImpl();

    private:
      // Various InfoSpaces and their InfoSpaceUpdaters.
      std::unique_ptr<RF2TTCDataInfoSpaceUpdater> rf2ttcInfoSpaceUpdaterP_;
      std::unique_ptr<RF2TTCDataInfoSpaceHandler> rf2ttcInfoSpaceP_;
      std::unique_ptr<RFRXDDataInfoSpaceUpdater> rfrxdInfoSpaceUpdaterP_;
      std::unique_ptr<RFRXDDataInfoSpaceHandler> rfrxdInfoSpaceP_;

      tcds::rfdippub::RFInfoDAQLoop rfInfoDAQLoop_;

    };

  } // namespace rfdippub
} // namespace tcds

#endif // _tcds_rfdippub_RFDIPPub_h_
