#ifndef _tcds_rfdippub_RFRXDDataSnapshot_h_
#define _tcds_rfdippub_RFRXDDataSnapshot_h_

namespace tcds {
  namespace rfdippub {

    class RFRXDDataSnapshot
    {

    public:
      enum STATUS {
        STATUS_GOOD = 0,
        STATUS_BAD = 1
      };

      RFRXDDataSnapshot();
      RFRXDDataSnapshot(double const rfrxFrevB1,
                        double const rfrxFrevB2,
                        double const rfrxF40B1,
                        double const rfrxF40B2,
                        STATUS const status);
      ~RFRXDDataSnapshot();

      double rfrxFrevB1() const;
      double rfrxFrevB2() const;
      double rfrxF40B1() const;
      double rfrxF40B2() const;
      STATUS status() const;
      bool isGood() const;

      void markBad();

    private:
      double rfrxFrevB1_;
      double rfrxFrevB2_;
      double rfrxF40B1_;
      double rfrxF40B2_;
      STATUS status_;

    };

  } // namespace rfdippub
} // namespace tcds

#endif // _tcds_rfdippub_RFRXDDataSnapshot_h_
