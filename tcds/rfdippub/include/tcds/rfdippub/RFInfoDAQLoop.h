#ifndef _tcds_rfdippub_RFInfoDAQLoop_h_
#define _tcds_rfdippub_RFInfoDAQLoop_h_

#include <map>
#include <memory>
#include <string>
#include <sys/types.h>
#include <utility>
#include <vector>

#include "toolbox/exception/Listener.h"
#include "toolbox/lang/Class.h"
#include "xdata/Table.h"

#include "tcds/rfdippub/RF2TTCDataSnapshot.h"
#include "tcds/rfdippub/RFInfoPublisher.h"
#include "tcds/rfdippub/RFRXDDataSnapshot.h"
#include "tcds/utils/Lock.h"

namespace toolbox {
  namespace task {
    class ActionSignature;
    class WorkLoop;
  }
}

namespace xcept {
  class Exception;
}

namespace tcds {
  namespace rfdippub {
    class RFDIPPub;
  }
}

namespace tcds {
  namespace rfdippub {

    class RFInfoDAQLoop :
      public toolbox::exception::Listener,
      public toolbox::lang::Class
    {

    public:
      RFInfoDAQLoop(tcds::rfdippub::RFDIPPub& rfDIPPub);
      virtual ~RFInfoDAQLoop();

      // The toolbox::exception::Listener callback.
      virtual void onException(xcept::Exception& err);

      // Acquisition loop (start/stop) control methods.
      void start();
      void stop();

      bool update(toolbox::task::WorkLoop* workLoop);

      RF2TTCDataSnapshot rf2ttcSnapshot() const;
      RFRXDDataSnapshot rfrxdSnapshot() const;

    private:
      // NOTE: The intended update rate of this information (to the
      // LHC) is about 0.5 Hz. So targeting roughly 1 Hz on our side
      // should be okay.
      // See also:
      // https://indico.cern.ch/event/367052/contributions/1782856/attachments/729636/1001235/LHC_Timing_Fixed_Display.pdf.
      static const useconds_t kLoopRelaxTime = 1000000;

      std::string const rfInfoDAQAlarmName_;
      tcds::rfdippub::RFDIPPub& rfDIPPub_;
      std::unique_ptr<toolbox::task::ActionSignature> daqWorkLoopActionP_;
      std::unique_ptr<toolbox::task::WorkLoop> daqWorkLoopP_;
      std::string workLoopName_;

      mutable tcds::utils::Lock lock_;

      bool shouldCheckConfig_;
      std::string lasURL_;
      std::string flashlistNameRF2TTCClocks_;
      std::string flashlistNameRFRXD_;
      std::string flashlistNameRFRXDLegend_;

      xdata::Table rf2ttcInfo_;
      xdata::Table rfrxdInfo_;
      xdata::Table rfrxdLegend_;
      std::string rfrxdLegendPrevStr_;

      RFInfoPublisher publisher_;

      xdata::Table getRF2TTCInfo() const;
      xdata::Table getRFRXDInfo() const;
      xdata::Table getRFRXDLegend() const;

      RF2TTCDataSnapshot processRF2TTCInfo();
      RFRXDDataSnapshot processRFRXDInfo();
      void publish();

      // The following is the actual RF info we intend to publish.
      RF2TTCDataSnapshot rf2ttcSnapshot_;
      RFRXDDataSnapshot rfrxdSnapshot_;

      // Some misc. helpers.
      std::vector<std::string> rfrxdChannelNames_;
      std::map<std::pair<std::string, std::string>, std::string> rfrxdChannelMap_;

    };

  } // namespace rfdippub
} // namespace tcds

#endif // _tcds_rfdippub_RFInfoDAQLoop_h_
