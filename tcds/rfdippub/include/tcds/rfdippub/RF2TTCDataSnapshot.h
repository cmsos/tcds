#ifndef _tcds_rfdippub_RF2TTCDataSnapshot_h_
#define _tcds_rfdippub_RF2TTCDataSnapshot_h_

#include <string>

#include "toolbox/TimeVal.h"

namespace tcds {
  namespace rfdippub {

    class RF2TTCDataSnapshot
    {

    public:
      enum STATUS {
        STATUS_GOOD = 0,
        STATUS_BAD = 1
      };

      RF2TTCDataSnapshot();
      RF2TTCDataSnapshot(bool const bunchclockBC1QPLLLock,
                         bool const bunchclockBC2QPLLLock,
                         toolbox::TimeVal const& bunchclockBC1QPLLTimestamp,
                         toolbox::TimeVal const& bunchclockBC2QPLLTimestamp,
                         std::string const& bunchclockBCMainSource,
                         STATUS const status);
      ~RF2TTCDataSnapshot();

      bool bunchclockBC1QPLLLock() const;
      bool bunchclockBC2QPLLLock() const;
      toolbox::TimeVal bunchclockBC1QPLLTimestamp() const;
      toolbox::TimeVal bunchclockBC2QPLLTimestamp() const;
      std::string bunchclockBCMainSource() const;
      STATUS status() const;
      bool isGood() const;

      void markBad();

    private:
      bool bunchclockBC1QPLLLock_;
      bool bunchclockBC2QPLLLock_;
      toolbox::TimeVal bunchclockBC1QPLLTimestamp_;
      toolbox::TimeVal bunchclockBC2QPLLTimestamp_;
      std::string bunchclockBCMainSource_;
      STATUS status_;

    };

  } // namespace rfdippub
} // namespace tcds

#endif // _tcds_rfdippub_RF2TTCDataSnapshot_h_
