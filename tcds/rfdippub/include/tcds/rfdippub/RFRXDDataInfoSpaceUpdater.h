#ifndef _tcds_rfdippub_RFRXDDataInfoSpaceUpdater_h_
#define _tcds_rfdippub_RFRXDDataInfoSpaceUpdater_h_

#include "tcds/rfdippub/RFInfoDAQLoop.h"
#include "tcds/rfdippub/RFRXDDataSnapshot.h"
#include "tcds/utils/InfoSpaceUpdater.h"

namespace tcds {
  namespace utils {
    class InfoSpaceHandler;
    class InfoSpaceItem;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace rfdippub {

    class RFRXDDataInfoSpaceUpdater : public tcds::utils::InfoSpaceUpdater
    {

    public:
      RFRXDDataInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                tcds::rfdippub::RFInfoDAQLoop const& rfInfoDAQLoop);
      virtual ~RFRXDDataInfoSpaceUpdater();

    private:
      virtual void updateInfoSpaceImpl(tcds::utils::InfoSpaceHandler* const infoSpaceHandler);

      virtual bool updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                       tcds::utils::InfoSpaceHandler* const infoSpaceHandler);

    private:
      tcds::rfdippub::RFInfoDAQLoop const& rfInfoDAQLoop_;
      tcds::rfdippub::RFRXDDataSnapshot rfrxdInfo_;

    };

  } // namespace rfdippub
} // namespace tcds

#endif // _tcds_rfdippub_RFRXDDataInfoSpaceUpdater_h_
