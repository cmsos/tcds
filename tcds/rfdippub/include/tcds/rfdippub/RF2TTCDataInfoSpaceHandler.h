#ifndef _tcds_rfdippub_RF2TTCDataInfoSpaceHandler_h_
#define _tcds_rfdippub_RF2TTCDataInfoSpaceHandler_h_

#include <string>
#include <map>

#include "tcds/utils/InfoSpaceHandler.h"

namespace xdaq {
  class Application;
}

namespace tcds {
  namespace utils {
    class InfoSpaceUpdater;
    class Monitor;
    class WebServer;
  }
}

namespace tcds {
  namespace rfdippub {

    class RF2TTCDataInfoSpaceHandler : public tcds::utils::InfoSpaceHandler
    {

    public:
      RF2TTCDataInfoSpaceHandler(xdaq::Application& xdaqApp,
                                 tcds::utils::InfoSpaceUpdater* updater);
      virtual ~RF2TTCDataInfoSpaceHandler();

    protected:
      virtual void registerItemSetsWithMonitor(tcds::utils::Monitor& monitor);
      virtual void registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                 tcds::utils::Monitor& monitor,
                                                 std::string const& forceTabName="");

      /* virtual std::string formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const; */

    private:
      std::map<std::string, std::string> bcNames_;

    };

  } // namespace rfdippub
} // namespace tcds

#endif // _tcds_rfdippub_RF2TTCDataInfoSpaceHandler_h_
