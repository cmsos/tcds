#ifndef _tcds_rfdippub_RF2TTCDataInfoSpaceUpdater_h_
#define _tcds_rfdippub_RF2TTCDataInfoSpaceUpdater_h_

#include "tcds/rfdippub/RFInfoDAQLoop.h"
#include "tcds/rfdippub/RF2TTCDataSnapshot.h"
#include "tcds/utils/InfoSpaceUpdater.h"

namespace tcds {
  namespace utils {
    class InfoSpaceHandler;
    class InfoSpaceItem;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace rfdippub {

    class RF2TTCDataInfoSpaceUpdater : public tcds::utils::InfoSpaceUpdater
    {

    public:
      RF2TTCDataInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                 tcds::rfdippub::RFInfoDAQLoop const& rfInfoDAQLoop);
      virtual ~RF2TTCDataInfoSpaceUpdater();

    protected:
      // tcds::utils::XDAQAppBase& getOwnerApplication() const;

      virtual void updateInfoSpaceImpl(tcds::utils::InfoSpaceHandler* const infoSpaceHandler);

      virtual bool updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                       tcds::utils::InfoSpaceHandler* const infoSpaceHandler);

    private:
      tcds::rfdippub::RFInfoDAQLoop const& rfInfoDAQLoop_;
      tcds::rfdippub::RF2TTCDataSnapshot rf2ttcInfo_;

    };

  } // namespace rfdippub
} // namespace tcds

#endif // _tcds_rfdippub_RF2TTCDataInfoSpaceUpdater_h_
