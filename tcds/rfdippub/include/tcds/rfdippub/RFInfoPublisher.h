#ifndef _tcds_rfdippub_RFInfoPublisher_h_
#define _tcds_rfdippub_RFInfoPublisher_h_

#include <string>

namespace xdaq {
  class ApplicationDescriptor;
}

namespace tcds {
  namespace rfdippub {

    class RFDIPPub;
    class RF2TTCDataSnapshot;
    class RFRXDDataSnapshot;

    class RFInfoPublisher
    {

    public:
      RFInfoPublisher(RFDIPPub const& rfDIPPub);
      virtual ~RFInfoPublisher();

      void publish(RF2TTCDataSnapshot const& rf2ttcSnapshot,
                   RFRXDDataSnapshot const& rfrxdSnapshot);

    private:
      void init();

      RFDIPPub const& rfDIPPub_;
      xdaq::ApplicationDescriptor const* psx_;
      std::string pvssHost_;

    };

  } // namespace rfdippub
} // namespace tcds

#endif // _tcds_rfdippub_RFInfoPublisher_h_
