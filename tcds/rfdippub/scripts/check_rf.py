#!/usr/bin/env python

###############################################################################
## Just a little tool to test the LHC RF <-> experiment
## communications, as documented here:
## https://indico.cern.ch/event/367052/contributions/1782856/attachments/729636/1001235/LHC_Timing_Fixed_Display.pdf.
###############################################################################

from __future__ import print_function

import datetime
import requests
import sys
import time
import xml.dom.minidom

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

if __name__ == '__main__':

    PSX_HOST = "xaas-tcdsp5.cms"
    PSX_PORT = 9929

    #----------

    psx_url = "http://{0:s}:{1:d}".format(PSX_HOST, PSX_PORT)

    headers = {
        'Content-Type': 'application/xml',
        'SOAPAction': 'urn:xdaq-application:service=psx'
    }

    #----------

    try:
        while True:
            try:
                # Create the basics of the SOAP request to the PSX server.
                psx_dpget_doc = xml.dom.minidom.Document()
                env = psx_dpget_doc.createElement('SOAP-ENV:Envelope')
                env.setAttribute('SOAP-ENV:encodingStyle', 'http://schemas.xmlsoap.org/soap/encoding/')
                env.setAttributeNS('xmlns', 'xmlns:SOAP-ENV', 'http://schemas.xmlsoap.org/soap/envelope/')
                env.setAttributeNS('xmlns', 'xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance')
                env.setAttributeNS('xmlns', 'xmlns:xsd', 'http://www.w3.org/2001/XMLSchema')
                env.setAttributeNS('xmlns', 'xmlns:SOAP-ENC', 'http://schemas.xmlsoap.org/soap/encoding/')
                psx_dpget_doc.appendChild(env)
                head = psx_dpget_doc.createElement('SOAP-ENV:Header')
                env.appendChild(head)
                body = psx_dpget_doc.createElement('SOAP-ENV:Body')
                env.appendChild(body)
                dpget = psx_dpget_doc.createElement('psx:dpGet')
                dpget.setAttribute('xmlns:psx', 'http://xdaq.cern.ch/xdaq/xsd/2006/psx-pvss-10.xsd')
                body.appendChild(dpget)

                # Add the data points we're after.
                data_points_base = [
                    ['cms_cen_dcs_2:ExternalApps/CMSTCDS/LHC.Timing.RFRX.FREV_B1:_online.._value', 'float'],
                    ['cms_cen_dcs_2:ExternalApps/CMSTCDS/LHC.Timing.RFRX.FREV_B2:_online.._value', 'float'],
                    ['cms_cen_dcs_2:ExternalApps/CMSTCDS/LHC.Timing.RFRX.F40_B1:_online.._value', 'float'],
                    ['cms_cen_dcs_2:ExternalApps/CMSTCDS/LHC.Timing.RFRX.F40_B2:_online.._value', 'float'],
                    ['cms_cen_dcs_2:ExternalApps/CMSTCDS/LHC.Timing.BunchClock.BC1_QPLL_Lock:_online.._value', 'boolean'],
                    ['cms_cen_dcs_2:ExternalApps/CMSTCDS/LHC.Timing.BunchClock.BC2_QPLL_Lock:_online.._value', 'boolean'],
                    ['cms_cen_dcs_2:ExternalApps/CMSTCDS/LHC.Timing.BunchClock.BC1_QPLL_Timestamp:_online.._value', 'timestamp'],
                    ['cms_cen_dcs_2:ExternalApps/CMSTCDS/LHC.Timing.BunchClock.BC2_QPLL_Timestamp:_online.._value', 'timestamp'],
                    ['cms_cen_dcs_2:ExternalApps/CMSTCDS/LHC.Timing.BunchClock.BCMainSource:_online.._value', 'string']
                ]

                data_points = []
                for dp in data_points_base:
                    data_points.append(dp)
                    data_points.append([dp[0].replace("_online", "_original"), dp[1]])
                    data_points.append([dp[0].replace("_online", "_original").replace("_value", "_exp_inv"), "bool"])
                    data_points.append([dp[0].replace("_value", "_invalid"), "bool"])
                    data_points.append([dp[0].replace("_value", "_userbit1"), "bool"])
                    data_points.append([dp[0].replace("_value", "_userbit2"), "bool"])

                data_types = dict(zip([i[0] for i in data_points],
                                      [i[1] for i in data_points]))

                dp_names = [i[0] for i in data_points]
                # for experiment_name in ["CMS"]:
                for dp_name in dp_names:
                    dp = psx_dpget_doc.createElement('psx:dp')
                    dp.setAttribute('name', dp_name)
                    dpget.appendChild(dp)
                # print(psx_dpget_doc.toprettyxml())

                #----------

                # Perform the actual request.
                response = requests.post(psx_url, headers=headers, data=psx_dpget_doc.toxml())
                response_text = response.text.encode('utf-8')
                response_xml = xml.dom.minidom.parseString(response_text)
                print(response_xml.toprettyxml())

                # Extract the raw data point data.
                data_raw = {}
                dps_in_response = response_xml.getElementsByTagName('psx:dp')
                if not len(dps_in_response):
                    raise RuntimeError("No data points returned by PSX server")
                for i in dps_in_response:
                    raw_dp_name = i.getAttribute('name')
                    try:
                        raw_val = i.firstChild.data
                    except AttributeError:
                        raw_val = None
                    data_raw[raw_dp_name] = raw_val

                # Convert data to something more appropriate.
                data = {}
                for (dp_name, dp_val_raw) in data_raw.iteritems():
                    dp_type = data_types[dp_name]
                    if dp_val_raw is None:
                        if dp_type == 'float':
                            dp_val = 0.
                        elif dp_type == 'string':
                            dp_val = ""
                        elif dp_type == 'timestamp':
                            dp_val = datetime.datetime.fromtimestamp(0)
                        elif dp_type == 'bool':
                            dp_val = False
                        else:
                            dp_val = "???"
                    else:
                        if dp_type == 'float':
                            dp_val = float(dp_val_raw)
                        elif dp_type == 'string':
                            dp_val = dp_val_raw
                        elif dp_type == 'timestamp':
                            dp_val = datetime.datetime.fromtimestamp(float(dp_val_raw))
                        elif dp_type == 'bool':
                            dp_val = dp_val_raw
                        else:
                            dp_val = dp_val_raw
                    data[dp_name] = dp_val

                #----------

                def dp_sorter(lhs, rhs):
                    (lhs_main, lhs_special) = lhs[0].split(':')[1:3]
                    (rhs_main, rhs_special) = rhs[0].split(':')[1:3]
                    if lhs_main != rhs_main:
                        return cmp(lhs_main, rhs_main)
                    else:
                        # Sort original values on top.
                        if lhs_special.find("original") > -1 and rhs_special.find("original") < 0:
                            return -1
                        elif lhs_special.find("original") < 0 and rhs_special.find("original") > -1:
                            return 1
                        else:
                            (lhs_special_lo, lhs_special_hi) = lhs_special.split(':')[-1].split('._')
                            (rhs_special_lo, rhs_special_hi) = rhs_special.split(':')[-1].split('._')
                            lhs_special_hi = lhs_special_hi.strip(".")
                            rhs_special_hi = rhs_special_hi.strip(".")
                            if lhs_special_lo == rhs_special_lo:
                                order = ["value", "exp_inv", "invalid", "userbit1", "userbit2"]
                                try:
                                    lhs_ind = order.index(lhs_special_hi)
                                except ValueError:
                                    lhs_ind = len(order)
                                try:
                                    rhs_ind = order.index(rhs_special_hi)
                                except ValueError:
                                    rhs_ind = len(order)
                                return cmp(lhs_ind, rhs_ind)
                            else:
                                return cmp(lhs_special_lo, rhs_special_lo)

                #----------

                # Dump the data.
                raw_dp_names = data.keys()
                max_len = max([len(i) for i in raw_dp_names])
                print("\x1b[2J\x1b[H", file=sys.stderr)
                name_prev = None
                for (name, val) in sorted(data.iteritems(), cmp=dp_sorter):
                    if name_prev and (name.split(":")[1] != name_prev.split(":")[1]):
                        print()
                    print("{1:{0:d}s}: {2:s}".format(max_len, name, str(val)))
                    name_prev = name

            except Exception as err:
                msg = "Something went wrong: {0:s}".format(str(err))
                print(msg, file=sys.stderr)
                break
            time.sleep(1)

    except KeyboardInterrupt:
        # That's fine.
        pass

    #----------

    print("Done")

###############################################################################
