#include "tcds/rfdippub/RFRXDDataInfoSpaceHandler.h"

#include "toolbox/string.h"

#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/WebServer.h"

tcds::rfdippub::RFRXDDataInfoSpaceHandler::RFRXDDataInfoSpaceHandler(xdaq::Application& xdaqApp,
                                                                     tcds::utils::InfoSpaceUpdater* updater) :
  InfoSpaceHandler(xdaqApp, "tcds-rfdippub-rfrxd", updater)
{
  bNames_.clear();
  bNames_["b1"] = "Beam 1";
  bNames_["b2"] = "Beam 2";

  //----------

  // The revolution/orbit and bunch-crossing frequencies of both
  // beams.
  for (std::map<std::string, std::string>::const_iterator it = bNames_.begin();
       it != bNames_.end();
       ++it)
    {
      createDouble("f_rev_" + it->first,
                   0.,
                   "freq_kHz",
                   tcds::utils::InfoSpaceItem::PROCESS);
      createDouble("f_40_" + it->first,
                   0.,
                   "freq_MHz",
                   tcds::utils::InfoSpaceItem::PROCESS);
    }

  // The current status of the DIP publication.
  createBool("dip_pub_status",
             0,
             "good/bad",
             tcds::utils::InfoSpaceItem::PROCESS);
}

tcds::rfdippub::RFRXDDataInfoSpaceHandler::~RFRXDDataInfoSpaceHandler()
{
}

void
tcds::rfdippub::RFRXDDataInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  std::string const itemSetName = "itemset-rfdippub-rfrxd";
  monitor.newItemSet(itemSetName);

  // The revolution/orbit and bunch-crossing frequencies of both
  // beams.
  for (std::map<std::string, std::string>::const_iterator it = bNames_.begin();
       it != bNames_.end();
       ++it)
    {
      monitor.addItem(itemSetName,
                      "f_rev_" + it->first,
                      toolbox::toString("%s orbit frequency (kHz)",
                                        it->second.c_str()),
                      this);
      monitor.addItem(itemSetName,
                      "f_40_" + it->first,
                      toolbox::toString("%s bunch-crossing frequency (MHz)",
                                        it->second.c_str()),
                      this);
    }

  // The current status of the DIP publication.
  monitor.addItem(itemSetName,
                  "dip_pub_status",
                  "Current status of the RFInfo RF2TTC DIP publication",
                  this);
}

void
tcds::rfdippub::RFRXDDataInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                         tcds::utils::Monitor& monitor,
                                                                         std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "RFRXD info" : forceTabName;

  webServer.registerTab(tabName,
                        "Information from the RFRXDController flashlists",
                        1);
  webServer.registerTable("RFRXD",
                          "NOTE: These frequencies are coarse approximations only.",
                          monitor,
                          "itemset-rfdippub-rfrxd",
                          tabName);
}
