#include "tcds/rfdippub/RF2TTCDataInfoSpaceUpdater.h"

#include <string>

#include "xdata/TimeVal.h"

#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/XDAQAppBase.h"

tcds::rfdippub::RF2TTCDataInfoSpaceUpdater::RF2TTCDataInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                                                       tcds::rfdippub::RFInfoDAQLoop const& rfInfoDAQLoop) :
  tcds::utils::InfoSpaceUpdater(xdaqApp),
  rfInfoDAQLoop_(rfInfoDAQLoop)
{
}

tcds::rfdippub::RF2TTCDataInfoSpaceUpdater::~RF2TTCDataInfoSpaceUpdater()
{
}

// tcds::utils::XDAQAppBase&
// tcds::rfdippub::RF2TTCDataInfoSpaceUpdater::getOwnerApplication() const
// {
//   return static_cast<tcds::utils::XDAQAppBase&>(tcds::utils::InfoSpaceUpdater::getOwnerApplication());
// }

void
tcds::rfdippub::RF2TTCDataInfoSpaceUpdater::updateInfoSpaceImpl(tcds::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  // First get the latest info from the RF info DAQ loop.
  rf2ttcInfo_ = rfInfoDAQLoop_.rf2ttcSnapshot();

  // Then do what we normally do.
  tcds::utils::InfoSpaceUpdater::updateInfoSpaceImpl(infoSpaceHandler);
}

bool
tcds::rfdippub::RF2TTCDataInfoSpaceUpdater::updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                                                tcds::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  bool updated = false;
  std::string const name = item.name();
  tcds::utils::InfoSpaceItem::UpdateType const updateType = item.updateType();
  if (updateType == tcds::utils::InfoSpaceItem::PROCESS)
    {
      // The 'PROCESS' update type means that there is something
      // special to the variable. Figure out what to do based on
      // the variable name.
      if (name == "qpll_lock_status_bc1")
        {
          bool const newVal = rf2ttcInfo_.bunchclockBC1QPLLLock();
          infoSpaceHandler->setBool(name, newVal);
          updated = true;
        }
      else if (name == "qpll_lock_status_bc2")
        {
          bool const newVal = rf2ttcInfo_.bunchclockBC2QPLLLock();
          infoSpaceHandler->setBool(name, newVal);
          updated = true;
        }
      else if (name == "qpll_latest_unlock_time_bc1")
        {
          xdata::TimeVal const newVal = rf2ttcInfo_.bunchclockBC1QPLLTimestamp();
          infoSpaceHandler->setTimeVal(name, newVal);
          updated = true;
        }
      else if (name == "qpll_latest_unlock_time_bc2")
        {
          xdata::TimeVal const newVal = rf2ttcInfo_.bunchclockBC2QPLLTimestamp();
          infoSpaceHandler->setTimeVal(name, newVal);
          updated = true;
        }
      else if (name == "bc_main_source")
        {
          std::string const newVal = rf2ttcInfo_.bunchclockBCMainSource();
          infoSpaceHandler->setString(name, newVal);
          updated = true;
        }
      else if (name == "dip_pub_status")
        {
          bool const newVal = rf2ttcInfo_.isGood();
          infoSpaceHandler->setBool(name, newVal);
          updated = true;
        }
    }
  if (!updated)
    {
      updated = tcds::utils::InfoSpaceUpdater::updateInfoSpaceItem(item, infoSpaceHandler);
    }
  if (updated)
    {
      item.setValid();
    }
  return updated;
}
