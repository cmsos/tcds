#include "tcds/rfdippub/RF2TTCDataSnapshot.h"

tcds::rfdippub::RF2TTCDataSnapshot::RF2TTCDataSnapshot() :
  bunchclockBC1QPLLLock_(false),
  bunchclockBC2QPLLLock_(false),
  bunchclockBC1QPLLTimestamp_(0),
  bunchclockBC2QPLLTimestamp_(0),
  bunchclockBCMainSource_ ("unknown"),
  status_(STATUS_BAD)
{
}

tcds::rfdippub::RF2TTCDataSnapshot::RF2TTCDataSnapshot(bool const bunchclockBC1QPLLLock,
                                                       bool const bunchclockBC2QPLLLock,
                                                       toolbox::TimeVal const& bunchclockBC1QPLLTimestamp,
                                                       toolbox::TimeVal const& bunchclockBC2QPLLTimestamp,
                                                       std::string const& bunchclockBCMainSource,
                                                       STATUS const status) :
  bunchclockBC1QPLLLock_(bunchclockBC1QPLLLock),
  bunchclockBC2QPLLLock_(bunchclockBC2QPLLLock),
  bunchclockBC1QPLLTimestamp_(bunchclockBC1QPLLTimestamp),
  bunchclockBC2QPLLTimestamp_(bunchclockBC2QPLLTimestamp),
  bunchclockBCMainSource_(bunchclockBCMainSource),
  status_(status)
{
}

tcds::rfdippub::RF2TTCDataSnapshot::~RF2TTCDataSnapshot()
{
}

bool
tcds::rfdippub::RF2TTCDataSnapshot::bunchclockBC1QPLLLock() const
{
  return bunchclockBC1QPLLLock_;
}

bool
tcds::rfdippub::RF2TTCDataSnapshot::bunchclockBC2QPLLLock() const
{
  return bunchclockBC2QPLLLock_;
}

toolbox::TimeVal
tcds::rfdippub::RF2TTCDataSnapshot::bunchclockBC1QPLLTimestamp() const
{
  return bunchclockBC1QPLLTimestamp_;
}

toolbox::TimeVal
tcds::rfdippub::RF2TTCDataSnapshot::bunchclockBC2QPLLTimestamp() const
{
  return bunchclockBC2QPLLTimestamp_;
}

std::string
tcds::rfdippub::RF2TTCDataSnapshot::bunchclockBCMainSource() const
{
  return bunchclockBCMainSource_;
}

tcds::rfdippub::RF2TTCDataSnapshot::STATUS
tcds::rfdippub::RF2TTCDataSnapshot::status() const
{
  return status_;
}

bool
tcds::rfdippub::RF2TTCDataSnapshot::isGood() const
{
  return (status() == STATUS_GOOD);
}

void
tcds::rfdippub::RF2TTCDataSnapshot::markBad()
{
  status_ = STATUS_BAD;
}
