#include "tcds/rfdippub/RFRXDDataInfoSpaceUpdater.h"

#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/XDAQAppBase.h"

tcds::rfdippub::RFRXDDataInfoSpaceUpdater::RFRXDDataInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                                                     tcds::rfdippub::RFInfoDAQLoop const& rfInfoDAQLoop) :
  tcds::utils::InfoSpaceUpdater(xdaqApp),
  rfInfoDAQLoop_(rfInfoDAQLoop)
{
}

tcds::rfdippub::RFRXDDataInfoSpaceUpdater::~RFRXDDataInfoSpaceUpdater()
{
}

void
tcds::rfdippub::RFRXDDataInfoSpaceUpdater::updateInfoSpaceImpl(tcds::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  // First get the latest info from the RF info DAQ loop.
  rfrxdInfo_ = rfInfoDAQLoop_.rfrxdSnapshot();

  // Then do what we normally do.
  tcds::utils::InfoSpaceUpdater::updateInfoSpaceImpl(infoSpaceHandler);
}

bool
tcds::rfdippub::RFRXDDataInfoSpaceUpdater::updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                                               tcds::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  bool updated = false;
  std::string const name = item.name();
  tcds::utils::InfoSpaceItem::UpdateType const updateType = item.updateType();
  if (updateType == tcds::utils::InfoSpaceItem::PROCESS)
    {
      // The 'PROCESS' update type means that there is something
      // special to the variable. Figure out what to do based on
      // the variable name.
      if (name == "f_rev_b1")
        {
          double const newVal = rfrxdInfo_.rfrxFrevB1();
          infoSpaceHandler->setDouble(name, newVal);
          updated = true;
        }
      else if (name == "f_rev_b2")
        {
          double const newVal = rfrxdInfo_.rfrxFrevB2();
          infoSpaceHandler->setDouble(name, newVal);
          updated = true;
        }
      else if (name == "f_40_b1")
        {
          double const newVal = rfrxdInfo_.rfrxF40B1();
          infoSpaceHandler->setDouble(name, newVal);
          updated = true;
        }
      else if (name == "f_40_b2")
        {
          double const newVal = rfrxdInfo_.rfrxF40B2();
          infoSpaceHandler->setDouble(name, newVal);
          updated = true;
        }
      else if (name == "dip_pub_status")
        {
          bool const newVal = rfrxdInfo_.isGood();
          infoSpaceHandler->setBool(name, newVal);
          updated = true;
        }
    }
  if (!updated)
    {
      updated = tcds::utils::InfoSpaceUpdater::updateInfoSpaceItem(item, infoSpaceHandler);
    }
  if (updated)
    {
      item.setValid();
    }
  return updated;
}
