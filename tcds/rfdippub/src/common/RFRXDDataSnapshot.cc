#include "tcds/rfdippub/RFRXDDataSnapshot.h"

tcds::rfdippub::RFRXDDataSnapshot::RFRXDDataSnapshot() :
  rfrxFrevB1_(0.),
  rfrxFrevB2_(0.),
  rfrxF40B1_(0.),
  rfrxF40B2_(0.),
  status_(STATUS_BAD)
{
}

tcds::rfdippub::RFRXDDataSnapshot::RFRXDDataSnapshot(double const rfrxFrevB1,
                                                     double const rfrxFrevB2,
                                                     double const rfrxF40B1,
                                                     double const rfrxF40B2,
                                                     STATUS const status) :
  rfrxFrevB1_(rfrxFrevB1),
  rfrxFrevB2_(rfrxFrevB2),
  rfrxF40B1_(rfrxF40B1),
  rfrxF40B2_(rfrxF40B2),
  status_(status)
{
}

tcds::rfdippub::RFRXDDataSnapshot::~RFRXDDataSnapshot()
{
}

double
tcds::rfdippub::RFRXDDataSnapshot::rfrxFrevB1() const
{
  return rfrxFrevB1_;
}

double
tcds::rfdippub::RFRXDDataSnapshot::rfrxFrevB2() const
{
  return rfrxFrevB2_;
}

double
tcds::rfdippub::RFRXDDataSnapshot::rfrxF40B1() const
{
  return rfrxF40B1_;
}

double
tcds::rfdippub::RFRXDDataSnapshot::rfrxF40B2() const
{
  return rfrxF40B2_;
}

tcds::rfdippub::RFRXDDataSnapshot::STATUS
tcds::rfdippub::RFRXDDataSnapshot::status() const
{
  return status_;
}

bool
tcds::rfdippub::RFRXDDataSnapshot::isGood() const
{
  return (status() == STATUS_GOOD);
}

void
tcds::rfdippub::RFRXDDataSnapshot::markBad()
{
  status_ = STATUS_BAD;
}
