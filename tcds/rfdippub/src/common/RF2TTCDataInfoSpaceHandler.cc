#include "tcds/rfdippub/RF2TTCDataInfoSpaceHandler.h"

#include "toolbox/string.h"

#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/WebServer.h"

tcds::rfdippub::RF2TTCDataInfoSpaceHandler::RF2TTCDataInfoSpaceHandler(xdaq::Application& xdaqApp,
                                                                       tcds::utils::InfoSpaceUpdater* updater) :
  InfoSpaceHandler(xdaqApp, "tcds-rfdippub-rf2ttc", updater)
{
  bcNames_.clear();
  bcNames_["bc1"] = "BC1";
  bcNames_["bc2"] = "BC2";

  //----------

  // The QPLL locking status of the RF2TTC BC inputs.
  for (std::map<std::string, std::string>::const_iterator it = bcNames_.begin();
       it != bcNames_.end();
       ++it)
    {
      createBool("qpll_lock_status_" + it->first,
                 0,
                 "good/bad",
                 tcds::utils::InfoSpaceItem::PROCESS);
      createTimeVal("qpll_latest_unlock_time_" + it->first,
                    0,
                    "",
                    tcds::utils::InfoSpaceItem::PROCESS);
    }

  // The currently-chosen BC-main source.
  createString("bc_main_source",
               "UNKNOWN",
               "",
               tcds::utils::InfoSpaceItem::PROCESS);

  // The current status of the DIP publication.
  createBool("dip_pub_status",
             0,
             "good/bad",
             tcds::utils::InfoSpaceItem::PROCESS);
}

tcds::rfdippub::RF2TTCDataInfoSpaceHandler::~RF2TTCDataInfoSpaceHandler()
{
}

void
tcds::rfdippub::RF2TTCDataInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  std::string const itemSetName = "itemset-rfdippub-rf2ttc";
  monitor.newItemSet(itemSetName);

  // The QPLL locking status of the RF2TTC BC inputs.
  for (std::map<std::string, std::string>::const_iterator it = bcNames_.begin();
       it != bcNames_.end();
       ++it)
    {
      monitor.addItem(itemSetName,
                      "qpll_lock_status_" + it->first,
                      toolbox::toString("%s QPLL locking status",
                                        it->second.c_str()),
                      this);
      monitor.addItem(itemSetName,
                      "qpll_latest_unlock_time_" + it->first,
                      toolbox::toString("Timestamp of latest %s QPLL unlock",
                                        it->second.c_str()),
                      this);
    }

  // The currently-chosen BC-main source.
  monitor.addItem(itemSetName,
                  "bc_main_source",
                  "Current choice of BC-main",
                  this);

  // The current status of the DIP publication.
  monitor.addItem(itemSetName,
                  "dip_pub_status",
                  "Current status of the RFInfo RF2TTC DIP publication",
                  this);
}

void
tcds::rfdippub::RF2TTCDataInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                          tcds::utils::Monitor& monitor,
                                                                          std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "RF2TTC info" : forceTabName;

  webServer.registerTab(tabName,
                        "Information from the RF2TTCController flashlists",
                        1);
  webServer.registerTable("RF2TTC",
                          "",
                          monitor,
                          "itemset-rfdippub-rf2ttc",
                          tabName);
}
