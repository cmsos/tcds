#include "tcds/rfdippub/RFInfoDAQLoop.h"

#include <algorithm>
#include <cstddef>
#include <iostream>
#include <sstream>
#include <stdint.h>
#include <unistd.h>

#include "toolbox/BSem.h"
#include "toolbox/TimeVal.h"
#include "toolbox/string.h"
#include "toolbox/task/Action.h"
#include "toolbox/task/WorkLoop.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/task/exception/Exception.h"
#include "xcept/Exception.h"
#include "xdaq/ApplicationDescriptor.h"
#include "xdata/Serializable.h"
#include "xdata/TableIterator.h"
#include "xdata/TimeVal.h"
#include "xdata/UnsignedInteger32.h"

#include "tcds/exception/Exception.h"
#include "tcds/rf2ttc/Definitions.h"
#include "tcds/rfdippub/ConfigurationInfoSpaceHandler.h"
#include "tcds/rfdippub/RFDIPPub.h"
#include "tcds/utils/LockGuard.h"
#include "tcds/utils/Utils.h"

tcds::rfdippub::RFInfoDAQLoop::RFInfoDAQLoop(tcds::rfdippub::RFDIPPub& rfDIPPub) :
  rfInfoDAQAlarmName_("RFDIPPUBDAQAlarm"),
  rfDIPPub_(rfDIPPub),
  workLoopName_(""),
  lock_(toolbox::BSem::FULL),
  shouldCheckConfig_(true),
  lasURL_(""),
  flashlistNameRF2TTCClocks_(""),
  flashlistNameRFRXD_(""),
  flashlistNameRFRXDLegend_(""),
  publisher_(rfDIPPub)
{
  std::stringstream workLoopName;
  uint32_t const localId = rfDIPPub_.getApplicationDescriptor()->getLocalId();
  workLoopName << "RFInfoDAQWorkLoop_" << localId;
  workLoopName_ = workLoopName.str();
  daqWorkLoopP_ = std::unique_ptr<toolbox::task::WorkLoop>(toolbox::task::getWorkLoopFactory()->getWorkLoop(workLoopName_, "waiting"));
  daqWorkLoopP_->addExceptionListener(this);

  // Each RFRXD has three channels. This is sufficiently tied to the
  // physical hardware that we can hard-code it here.
  rfrxdChannelNames_.push_back("ch1");
  rfrxdChannelNames_.push_back("ch2");
  rfrxdChannelNames_.push_back("ch3");
}

tcds::rfdippub::RFInfoDAQLoop::~RFInfoDAQLoop()
{
  stop();
  daqWorkLoopP_.reset();
}

void
tcds::rfdippub::RFInfoDAQLoop::onException(xcept::Exception& err)
{
  // Start by raising the alarm.
  std::string const problemDescBase =
    "The RF info DAQ workloop failed: '%s'.";
  std::string const problemDesc = toolbox::toString(problemDescBase.c_str(), err.what());

  XCEPT_DECLARE(tcds::exception::RFInfoDAQFailureAlarm, alarmException0, problemDesc);
  rfDIPPub_.raiseAlarm(rfInfoDAQAlarmName_, alarmException0);

  //--------

  // A little grace period before restarting the workloop may help (in
  // case the underlying problem goes away with time). It also makes
  // the problem more noticeable in the monitoring.
  unsigned const kWorkloopRestartGraceTime = 5;

  bool restartSucceeded = false;
  while (! restartSucceeded)
    {
      ::sleep(kWorkloopRestartGraceTime);
      try
        {
          start();
          restartSucceeded = true;
        }
      catch (xcept::Exception const& err)
        {
          // Raise the alarmWs.
          std::string const problemDesc =
            toolbox::toString("Failed to restart the RF info DAQ workloop: '%s'.",
                              err.what());
          XCEPT_DECLARE(tcds::exception::RFInfoDAQFailureAlarm, alarmException, problemDesc);
          rfDIPPub_.raiseAlarm(rfInfoDAQAlarmName_, alarmException);
        }
      catch (...)
        {
          // Raise the alarms.
          std::string const problemDesc =
            "Failed to restart the RF info DAQ workloop.";
          XCEPT_DECLARE(tcds::exception::RFInfoDAQFailureAlarm, alarmException, problemDesc);
          rfDIPPub_.raiseAlarm(rfInfoDAQAlarmName_, alarmException);
        }
    }

  // Revoke the alarm.
  std::string const reason =
    "Successfully restarted the RF info DAQ workloop.";
  rfDIPPub_.revokeAlarm(rfInfoDAQAlarmName_, reason);
}

void
tcds::rfdippub::RFInfoDAQLoop::start()
{
  std::string const problemDescBase =
    "Failed to start RF info DAQ workloop: '%s'.";
  std::string problemDesc = "";
  if (daqWorkLoopP_.get())
    {
      try
        {
          if (!daqWorkLoopActionP_.get())
            {
              daqWorkLoopActionP_ =
                std::unique_ptr<toolbox::task::ActionSignature>(toolbox::task::bind(this, &tcds::rfdippub::RFInfoDAQLoop::update, "updateAction"));
            }
          // We (try to) remove the old task from the work loop, and
          // then (re)submit the same task again. This covers both the
          // situation in which the run was stopped (and the original
          // task is still present in the work loop queue) and the
          // situation in which an exception was thrown in the task
          // (and the task was removed from the work loop queue).
          try
            {
              daqWorkLoopP_->remove(daqWorkLoopActionP_.get());
            }
          catch (toolbox::task::exception::Exception&)
            {
              // Nothing to do here. We were just making sure that
              // we're not submitting the same task multiple times.
            }
          daqWorkLoopP_->submit(daqWorkLoopActionP_.get());
          if (!daqWorkLoopP_->isActive())
            {
              daqWorkLoopP_->activate();
            }
        }
      catch (toolbox::task::exception::Exception const& err)
        {
          problemDesc = toolbox::toString(problemDescBase.c_str(), err.what());
        }
    }
  else
    {
      problemDesc = toolbox::toString(problemDescBase.c_str(), "Work loop does not exist.");
    }

  // If necessary: raise the alarm.
  if (problemDesc.size())
    {
      XCEPT_DECLARE(tcds::exception::RFInfoDAQFailureAlarm, alarmException, problemDesc);
      rfDIPPub_.raiseAlarm(rfInfoDAQAlarmName_, alarmException);
    }
  else
    {
      rfDIPPub_.revokeAlarm(rfInfoDAQAlarmName_);
    }
}

void
tcds::rfdippub::RFInfoDAQLoop::stop()
{
  rfDIPPub_.revokeAlarm(rfInfoDAQAlarmName_);
  if (daqWorkLoopP_.get())
    {
      if (daqWorkLoopP_->isActive())
        {
          daqWorkLoopP_->cancel();
        }
    }
}

bool
tcds::rfdippub::RFInfoDAQLoop::update(toolbox::task::WorkLoop* workLoop)
{
  if (shouldCheckConfig_)
    {
      // Check if we can find the flashlists we need somewhere.
      std::vector<std::string> const lasURLs =
        rfDIPPub_.getConfigurationInfoSpaceHandler().getStringVec("lasURLs");
      flashlistNameRF2TTCClocks_ =
        rfDIPPub_.getConfigurationInfoSpaceHandler().getString("flashlistNameRF2TTCClocks");
      flashlistNameRFRXD_ =
        rfDIPPub_.getConfigurationInfoSpaceHandler().getString("flashlistNameRFRXD");
      flashlistNameRFRXDLegend_ =
        rfDIPPub_.getConfigurationInfoSpaceHandler().getString("flashlistNameRFRXDLegend");

      std::vector<std::string> flashListNames;
      flashListNames.push_back(flashlistNameRF2TTCClocks_);
      flashListNames.push_back(flashlistNameRFRXD_);
      flashListNames.push_back(flashlistNameRFRXDLegend_);

      for (std::vector<std::string>::const_iterator i = flashListNames.begin();
           i != flashListNames.end();
           ++i)
        {
          std::vector<std::string>::const_iterator const it =
            tcds::utils::findFlashList(*i, lasURLs);

          bool const flashlistFound = (it != lasURLs.end());

          if (flashlistFound)
            {
              lasURL_ = *it;
            }
          else
            {
              std::string const msg = toolbox::toString("Flashlist '%s' is not hosted "
                                                        "by any of the specified LASes.",
                                                        i->c_str());
              XCEPT_DECLARE(tcds::exception::RFInfoDAQFailureAlarm, alarmException, msg);
              rfDIPPub_.raiseAlarm(rfInfoDAQAlarmName_, alarmException);
            }
        }

      //----------

      // Check if the LAS already has any data collected for these
      // flashlists.
      for (std::vector<std::string>::const_iterator i = flashListNames.begin();
           i != flashListNames.end();
           ++i)
        {
          if (!tcds::utils::isFlashListPresentInLAS(lasURL_, flashlistNameRF2TTCClocks_))
            {
              std::string const msg =
                toolbox::toString("No data available yet for flashlist '%s' in LAS '%s'.",
                                  flashlistNameRF2TTCClocks_.c_str(),
                                  lasURL_.c_str());
              XCEPT_DECLARE(tcds::exception::RFInfoDAQFailureAlarm, alarmException, msg);
              rfDIPPub_.raiseAlarm(rfInfoDAQAlarmName_, alarmException);
              shouldCheckConfig_ = true;
              break;
            }
          else
            {
              shouldCheckConfig_ = false;
            }
        }
    }

  //----------

  bool haveRF2TTCInfo = false;
  bool haveRFRXDInfo = false;
  if (!shouldCheckConfig_)
    {
      // Now get all flashlists.
      // NOTE: The RF2TTC and RFRXD information can be treated
      // separately.
      try
        {
          rf2ttcInfo_ = getRF2TTCInfo();
          haveRF2TTCInfo = true;
        }
      catch (xcept::Exception const& err)
        {
          shouldCheckConfig_ = true;
        }

      try
        {
          rfrxdInfo_ = getRFRXDInfo();
          rfrxdLegend_ = getRFRXDLegend();
          haveRFRXDInfo = true;
        }
      catch (xcept::Exception const& err)
        {
          shouldCheckConfig_ = true;
        }
    }

  bool isRF2TTCDataGood = false;
  RF2TTCDataSnapshot rf2ttcSnapshot;
  if (haveRF2TTCInfo)
    {
      try
        {
          rf2ttcSnapshot = processRF2TTCInfo();
          isRF2TTCDataGood = true;
        }
      catch (xcept::Exception const& err)
        {
          shouldCheckConfig_ = true;
          std::string const msg =
            toolbox::toString("Something went wrong processing the RFDIPPub RF2TTC information: '%s'.",
                              err.what());
          XCEPT_DECLARE(tcds::exception::RFInfoDAQFailureAlarm, alarmException, msg);
          rfDIPPub_.raiseAlarm(rfInfoDAQAlarmName_, alarmException);
        }

      {
        tcds::utils::LockGuard<tcds::utils::Lock> guardedLock(lock_);
        if (isRF2TTCDataGood)
          {
            rf2ttcSnapshot_ = rf2ttcSnapshot;
          }
        else
          {
            rf2ttcSnapshot_.markBad();
          }
      }
    }

  bool isRFRXDDataGood = false;
  RFRXDDataSnapshot rfrxdSnapshot;
  if (haveRFRXDInfo)
    {
      try
        {
          rfrxdSnapshot = processRFRXDInfo();
          isRFRXDDataGood = true;
        }
      catch (xcept::Exception const& err)
        {
          shouldCheckConfig_ = true;
          std::string const msg =
            toolbox::toString("Something went wrong processing the RFDIPPub RFRXD information: '%s'.",
                              err.what());
          XCEPT_DECLARE(tcds::exception::RFInfoDAQFailureAlarm, alarmException, msg);
          rfDIPPub_.raiseAlarm(rfInfoDAQAlarmName_, alarmException);
        }

      {
        tcds::utils::LockGuard<tcds::utils::Lock> guardedLock(lock_);
        if (isRFRXDDataGood)
          {
            rfrxdSnapshot_ = rfrxdSnapshot;
          }
        else
          {
            rfrxdSnapshot_.markBad();
          }
      }
    }

  //----------

  // NOTE: We always publish something, just to show that the
  // application is alive. The publication status will reflect if the
  // contents can be trusted or not.
  bool isPublicationGood = false;
  try
    {
      publish();
      isPublicationGood = true;
    }
  catch (xcept::Exception const& err)
    {
      std::string const msg =
        toolbox::toString("Something went wrong publishing the RFDIPPub information: '%s'.",
                          err.what());
      XCEPT_DECLARE(tcds::exception::RFInfoDAQFailureAlarm, alarmException, msg);
      rfDIPPub_.raiseAlarm(rfInfoDAQAlarmName_, alarmException);
    }

  //----------

  bool const isAllGood = isRF2TTCDataGood && isRFRXDDataGood && isPublicationGood;
  if (isAllGood)
    {
      rfDIPPub_.revokeAlarm(rfInfoDAQAlarmName_);
    }

  //----------

  // Wait a little in order not to completely hog the CPU.
  ::usleep(kLoopRelaxTime);

  // Return true in order to schedule the next iteration.
  return true;
}

tcds::rfdippub::RF2TTCDataSnapshot
tcds::rfdippub::RFInfoDAQLoop::rf2ttcSnapshot() const
{
  // Lock to prevent being interrupted by updates.
  tcds::utils::LockGuard<tcds::utils::Lock> guardedLock(lock_);
  return rf2ttcSnapshot_;
}

tcds::rfdippub::RFRXDDataSnapshot
tcds::rfdippub::RFInfoDAQLoop::rfrxdSnapshot() const
{
  // Lock to prevent being interrupted by updates.
  tcds::utils::LockGuard<tcds::utils::Lock> guardedLock(lock_);
  return rfrxdSnapshot_;
}

xdata::Table
tcds::rfdippub::RFInfoDAQLoop::getRF2TTCInfo() const
{
  // Get the information from the LAS.
  xdata::Table const flashlist =
    tcds::utils::getFlashList(lasURL_, flashlistNameRF2TTCClocks_);

  return flashlist;
}

xdata::Table
tcds::rfdippub::RFInfoDAQLoop::getRFRXDInfo() const
{
  // Get the information from the LAS.
  xdata::Table const flashlist =
    tcds::utils::getFlashList(lasURL_, flashlistNameRFRXD_);

  return flashlist;
}

xdata::Table
tcds::rfdippub::RFInfoDAQLoop::getRFRXDLegend() const
{
  // Get the information from the LAS.
  xdata::Table const flashlist =
    tcds::utils::getFlashList(lasURL_, flashlistNameRFRXDLegend_);

  return flashlist;
}

tcds::rfdippub::RF2TTCDataSnapshot
tcds::rfdippub::RFInfoDAQLoop::processRF2TTCInfo()
{
  // RF2TTC bunch clock locking information.
  std::string name = "";
  xdata::Serializable* tmp = 0;
  xdata::UnsignedInteger32* tmpInt = 0;
  xdata::TimeVal* tmpTime = 0;

  std::string const msgBaseMissing =
    "Column '%s' is missing  from the RF2TTC flashlist.";
  std::string const msgBaseWrongType =
    "Column '%s' has the wrong type (%s) in the RF2TTC flashlist.";

  name = "qpll_lock_status_bc1";
  tmp = rf2ttcInfo_.getValueAt(0, name);
  if (!tmp)
    {
      std::string const msg =
        toolbox::toString(msgBaseMissing.c_str(), name.c_str());
      XCEPT_RAISE(tcds::exception::ConfigurationProblem, msg);
    }
  tmpInt = dynamic_cast<xdata::UnsignedInteger32*>(tmp);
  if (!tmpInt)
    {
      std::string const msg =
        toolbox::toString(msgBaseWrongType.c_str(), name.c_str(), tmp->type().c_str());
      XCEPT_RAISE(tcds::exception::ConfigurationProblem, msg);
    }
  tcds::definitions::QPLL_LOCK_STATUS const bc1QPLLLock =
    static_cast<tcds::definitions::QPLL_LOCK_STATUS>(int(*tmpInt));
  bool const bunchclockBC1QPLLLock =
    (bc1QPLLLock == tcds::definitions::QPLL_LOCK_STATUS_LOCKED);

  name = "qpll_lock_status_bc2";
  tmp = rf2ttcInfo_.getValueAt(0, name);
  if (!tmp)
    {
      std::string const msg =
        toolbox::toString(msgBaseMissing.c_str(), name.c_str());
      XCEPT_RAISE(tcds::exception::ConfigurationProblem, msg);
    }
  tmpInt = dynamic_cast<xdata::UnsignedInteger32*>(tmp);
  if (!tmpInt)
    {
      std::string const msg =
        toolbox::toString(msgBaseWrongType.c_str(), name.c_str(), tmp->type().c_str());
      XCEPT_RAISE(tcds::exception::ConfigurationProblem, msg);
    }
  tcds::definitions::QPLL_LOCK_STATUS const bc2QPLLLock =
    static_cast<tcds::definitions::QPLL_LOCK_STATUS>(int(*tmpInt));
  bool const bunchclockBC2QPLLLock =
    (bc2QPLLLock == tcds::definitions::QPLL_LOCK_STATUS_LOCKED);

  name = "qpll_latest_unlock_time_bc1";
  tmp = rf2ttcInfo_.getValueAt(0, name);
  if (!tmp)
    {
      std::string const msg =
        toolbox::toString(msgBaseMissing.c_str(), name.c_str());
      XCEPT_RAISE(tcds::exception::ConfigurationProblem, msg);
    }
  tmpTime = dynamic_cast<xdata::TimeVal*>(tmp);
  if (!tmpTime)
    {
      std::string const msg =
        toolbox::toString(msgBaseWrongType.c_str(), name.c_str(), tmp->type().c_str());
      XCEPT_RAISE(tcds::exception::ConfigurationProblem, msg);
    }
  toolbox::TimeVal const bunchclockBC1QPLLTimestamp = *tmpTime;

  name = "qpll_latest_unlock_time_bc2";
  tmp = rf2ttcInfo_.getValueAt(0, name);
  if (!tmp)
    {
      std::string const msg =
        toolbox::toString(msgBaseMissing.c_str(), name.c_str());
      XCEPT_RAISE(tcds::exception::ConfigurationProblem, msg);
    }
  tmpTime = dynamic_cast<xdata::TimeVal*>(tmp);
  if (!tmpTime)
    {
      std::string const msg =
        toolbox::toString(msgBaseWrongType.c_str(), name.c_str(), tmp->type().c_str());
      XCEPT_RAISE(tcds::exception::ConfigurationProblem, msg);
    }
  toolbox::TimeVal const bunchclockBC2QPLLTimestamp = *tmpTime;

  name = "curr_select_bcmain";
  tmp = rf2ttcInfo_.getValueAt(0, name);
  if (!tmp)
    {
      std::string const msg =
        toolbox::toString(msgBaseMissing.c_str(), name.c_str());
      XCEPT_RAISE(tcds::exception::ConfigurationProblem, msg);
    }
  tmpInt = dynamic_cast<xdata::UnsignedInteger32*>(tmp);
  if (!tmpInt)
    {
      std::string const msg =
        toolbox::toString(msgBaseWrongType.c_str(), name.c_str(), tmp->type().c_str());
      XCEPT_RAISE(tcds::exception::ConfigurationProblem, msg);
    }
  tcds::definitions::BC_MAIN_SOURCE const bcMainSource =
    static_cast<tcds::definitions::BC_MAIN_SOURCE>(int(*tmpInt));
  std::string bunchclockBCMainSource = "UNKNOWN";
  if ((bcMainSource == tcds::definitions::BC_MAIN_SOURCE_INTERNAL) ||
      (bcMainSource == tcds::definitions::BC_MAIN_SOURCE_BCREF))
    {
      bunchclockBCMainSource = "LOCAL";
    }
  else if (bcMainSource == tcds::definitions::BC_MAIN_SOURCE_BC1)
    {
      bunchclockBCMainSource = "BC1";
    }
  else if (bcMainSource == tcds::definitions::BC_MAIN_SOURCE_BC2)
    {
      bunchclockBCMainSource = "BC2";
    }

  tcds::rfdippub::RF2TTCDataSnapshot::STATUS const status =
    tcds::rfdippub::RF2TTCDataSnapshot::STATUS_GOOD;

  return RF2TTCDataSnapshot(bunchclockBC1QPLLLock, bunchclockBC2QPLLLock,
                            bunchclockBC1QPLLTimestamp, bunchclockBC2QPLLTimestamp,
                            bunchclockBCMainSource,
                            status);
}

tcds::rfdippub::RFRXDDataSnapshot
tcds::rfdippub::RFInfoDAQLoop::processRFRXDInfo()
{
  // Check if the RFRXD legend has changed. Should not happen, but
  // just in case. (The following also catches the first iteration, of
  // course.)
  // NOTE: Comparison for xdata::Table itself is not implemented.
  bool const needToCheckLegend = (rfrxdLegend_.toString() != rfrxdLegendPrevStr_);
  if (needToCheckLegend)
    {
      rfrxdLegendPrevStr_ = rfrxdLegend_.toString();
    }

  if (needToCheckLegend)
    {
      // Find out (based on the legends) which RFRXD channels
      // correspond to which signals.

      // We need to find out 1) which application publishes each
      // signal (i.e., which row in the flashlist), and 2) which
      // channel on the hardware is connected to the signal (i.e.,
      // which column).

      std::vector<std::string> rfNamesFound;

      // Inspect the flashlist row by row.
      // NOTE: The following does make several assumptions about the
      // structure of the flashlist...
      for (xdata::Table::iterator row = rfrxdLegend_.begin();
           row != rfrxdLegend_.end();
           ++row)
        {
          for (std::vector<std::string>::const_iterator channelName = rfrxdChannelNames_.begin();
               channelName != rfrxdChannelNames_.end();
               ++channelName)
            {
              // Find out which row and channel in the 'legend' table
              // contains the data for this RF signal name.
              std::string const tmp = toolbox::toString("%s_rf_name", channelName->c_str());
              xdata::Serializable const* const rfNameInRowTmp = row->getField(tmp);
              std::string const rfNameInRow = rfNameInRowTmp->toString();
              xdata::Serializable const* const serviceInRowTmp = row->getField("service");
              std::string const serviceInRow = serviceInRowTmp->toString();
              rfrxdChannelMap_[std::make_pair(serviceInRow, *channelName)] = rfNameInRow;
              rfNamesFound.push_back(rfNameInRow);
            }
        }

      // Let's make sure that we found all signals that we need.
      std::vector<std::string> rfNamesNeeded;
      rfNamesNeeded.push_back("F40.B1.C");
      rfNamesNeeded.push_back("F40.B2.C");
      rfNamesNeeded.push_back("Frev.B1.C");
      rfNamesNeeded.push_back("Frev.B2.C");
      for (std::vector<std::string>::const_iterator rfName = rfNamesNeeded.begin();
           rfName != rfNamesNeeded.end();
           ++rfName)
        {
          if (std::find(rfNamesFound.begin(), rfNamesFound.end(), *rfName) == rfNamesFound.end())
            {
              std::string const msg =
                toolbox::toString("At least one required signal name is missing"
                                  " from the RFRXD flashlists: '%s'.",
                                  rfName->c_str());
              XCEPT_RAISE(tcds::exception::ConfigurationProblem, msg);
            }
        }
    }

  //----------

  // Extract the required information for our DIP publications
  // (documented here:
  // https://indico.cern.ch/event/367052/contributions/1782856/attachments/729636/1001235/LHC_Timing_Fixed_Display.pdf).

  // RFRXD orbit and bunch clock frequency estimates.
  std::map<std::string, double> valueMap;
  for (xdata::Table::iterator row = rfrxdInfo_.begin();
       row != rfrxdInfo_.end();
       ++row)
    {
      for (std::vector<std::string>::const_iterator channelName = rfrxdChannelNames_.begin();
           channelName != rfrxdChannelNames_.end();
           ++channelName)
        {
          xdata::Serializable const* const serviceInRowTmp = row->getField("service");
          std::string const serviceInRow = serviceInRowTmp->toString();

          std::string const tmp = toolbox::toString("freq_%s", channelName->c_str());
          xdata::Serializable const* const freqValueTmp0 = row->getField(tmp);
          xdata::Double const* const freqValueTmp1 = dynamic_cast<xdata::Double const*>(freqValueTmp0);
          double const freqValue = double(*freqValueTmp1);

          std::string const rfName = rfrxdChannelMap_[std::make_pair(serviceInRow, *channelName)];
          valueMap[rfName] = freqValue;
        }
    }
  double const rfrxFrevB1 = valueMap["Frev.B1.C"];
  double const rfrxFrevB2 = valueMap["Frev.B2.C"];
  double const rfrxF40B1 = valueMap["F40.B1.C"];
  double const rfrxF40B2 = valueMap["F40.B2.C"];

  tcds::rfdippub::RFRXDDataSnapshot::STATUS const status =
    tcds::rfdippub::RFRXDDataSnapshot::STATUS_GOOD;

  return RFRXDDataSnapshot(rfrxFrevB1, rfrxFrevB2,
                           rfrxF40B1, rfrxF40B2,
                           status);
}

void
tcds::rfdippub::RFInfoDAQLoop::publish()
{
  // Lock to prevent being interrupted by updates.
  tcds::utils::LockGuard<tcds::utils::Lock> guardedLock(lock_);
  publisher_.publish(rf2ttcSnapshot_, rfrxdSnapshot_);
}
