#include "tcds/rfdippub/RFDIPPub.h"

#include <string>

#include "toolbox/string.h"
#include "xcept/Exception.h"
#include "xdaq/exception/ApplicationInstantiationFailed.h"
#include "xdata/Event.h"

#include "tcds/rfdippub/ConfigurationInfoSpaceHandler.h"
#include "tcds/rfdippub/RF2TTCDataInfoSpaceHandler.h"
#include "tcds/rfdippub/RF2TTCDataInfoSpaceUpdater.h"
#include "tcds/rfdippub/RFRXDDataInfoSpaceHandler.h"
#include "tcds/rfdippub/RFRXDDataInfoSpaceUpdater.h"
#include "tcds/exception/Exception.h"

XDAQ_INSTANTIATOR_IMPL(tcds::rfdippub::RFDIPPub)

tcds::rfdippub::RFDIPPub::RFDIPPub(xdaq::ApplicationStub* const stub)
try
  :
  tcds::utils::XDAQAppBase(stub, std::unique_ptr<tcds::hwlayer::DeviceBase>()),
    rfInfoDAQLoop_(*this)
      {
        // Create the InfoSpace holding all configuration information.
        cfgInfoSpaceP_ =
          std::unique_ptr<ConfigurationInfoSpaceHandler>(new ConfigurationInfoSpaceHandler(*this));
      }
catch (tcds::exception::Exception const& err)
  {
    std::string msgBase = "Something went wrong instantiating the RFDIPPub application";
    std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.what());
    XCEPT_RAISE(xdaq::exception::ApplicationInstantiationFailed, msg.c_str());
  }

tcds::rfdippub::RFDIPPub::~RFDIPPub()
{
  rfInfoDAQLoop_.stop();
}

void
tcds::rfdippub::RFDIPPub::setupInfoSpaces()
{
  // Make sure the configuration settings are up-to-date.
  cfgInfoSpaceP_->readInfoSpace();

  rf2ttcInfoSpaceUpdaterP_ =
    std::unique_ptr<RF2TTCDataInfoSpaceUpdater>(new RF2TTCDataInfoSpaceUpdater(*this, rfInfoDAQLoop_));
  rf2ttcInfoSpaceP_ =
    std::unique_ptr<RF2TTCDataInfoSpaceHandler>(new RF2TTCDataInfoSpaceHandler(*this, rf2ttcInfoSpaceUpdaterP_.get()));
  rfrxdInfoSpaceUpdaterP_ =
    std::unique_ptr<RFRXDDataInfoSpaceUpdater>(new RFRXDDataInfoSpaceUpdater(*this, rfInfoDAQLoop_));
  rfrxdInfoSpaceP_ =
    std::unique_ptr<RFRXDDataInfoSpaceHandler>(new RFRXDDataInfoSpaceHandler(*this, rfrxdInfoSpaceUpdaterP_.get()));

  // We don't have an FSM, so no state either.
  appStateInfoSpace_.setString("stateName", "n/a");
  // Similar for the hardware lease.
  appStateInfoSpace_.setString("hwLeaseOwnerId", "n/a");

  // Register all InfoSpaceItems with the Monitor.
  cfgInfoSpaceP_->registerItemSets(monitor_, webServer_);
  appStateInfoSpace_.registerItemSets(monitor_, webServer_);
  rf2ttcInfoSpaceP_->registerItemSets(monitor_, webServer_);
  rfrxdInfoSpaceP_->registerItemSets(monitor_, webServer_);
}

void
tcds::rfdippub::RFDIPPub::actionPerformed(xdata::Event& event)
{
  tcds::utils::XDAQAppBase::actionPerformed(event);

  // NOTE: After the application has been instantiated and its default
  // settings loaded, the urn:xdaq-event:setDefaultValues event is
  // fired. The urn:xdaq-event:configuration-loaded event is fired
  // after the configuration of the whole XDAQ executive
  // (applications, endpoints, etc.) has been done.
  if (event.type() == "urn:xdaq-event:setDefaultValues")
    {
      try
        {
          rfInfoDAQLoop_.start();
        }
      catch (xcept::Exception& err)
        {
          appStateInfoSpace_.setFSMState("Failed", err.what());
        }
    }
}

void
tcds::rfdippub::RFDIPPub::hwConnectImpl()
{
}

void
tcds::rfdippub::RFDIPPub::hwReleaseImpl()
{
}

void
tcds::rfdippub::RFDIPPub::hwConfigureImpl()
{
}
