#include "tcds/rfdippub/RFInfoPublisher.h"

#include <vector>

#include "xcept/Exception.h"
#include "xdaq/ApplicationDescriptor.h"
#include "xoap/MessageFactory.h"
#include "xoap/MessageReference.h"

#include "tcds/exception/Exception.h"
#include "tcds/rfdippub/ConfigurationInfoSpaceHandler.h"
#include "tcds/rfdippub/RF2TTCDataSnapshot.h"
#include "tcds/rfdippub/RFDIPPub.h"
#include "tcds/rfdippub/RFRXDDataSnapshot.h"
#include "tcds/utils/DIPDP.h"
#include "tcds/utils/DIPUtils.h"
#include "tcds/utils/PSXReply.h"
#include "tcds/utils/SOAPUtils.h"

tcds::rfdippub::RFInfoPublisher::RFInfoPublisher(RFDIPPub const& rfDIPPub) :
  rfDIPPub_(rfDIPPub),
  psx_(0),
  pvssHost_("")
{
}

tcds::rfdippub::RFInfoPublisher::~RFInfoPublisher()
{
}

void
tcds::rfdippub::RFInfoPublisher::init()
{
  // Figure out the location of the PSX server.
  try
    {
      psx_ = rfDIPPub_.getApplicationContext()->getDefaultZone()->getApplicationDescriptor("psx::Application", 0);
    }
  catch (xcept::Exception const& err)
    {
      std::string const msgBase = "Could not locate the PSX application";
      std::string const msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.what());
      XCEPT_RAISE(tcds::exception::RuntimeProblem, msg.c_str());
    }

  // Figure out the name of the WinCC OA (formerly know as 'PVSS')
  // host.
  pvssHost_ = rfDIPPub_.getConfigurationInfoSpaceHandler().getString("pvssHost");
}

void
tcds::rfdippub::RFInfoPublisher::publish(RF2TTCDataSnapshot const& rf2ttcSnapshot,
                                         RFRXDDataSnapshot const& rfrxdSnapshot)
{
  if (!psx_)
    {
      init();
    }

  // NOTE: The fact that these DIP paths are hard-coded here is not a
  // big deal. Different publications have different formats and/or
  // types, so this is not truely configurable anyway. The PVSS host
  // is configurable though.

  std::vector<tcds::utils::DIPDP> dpInfo;

  tcds::utils::DIP_QUAL const dpQualRFRXD =
    rfrxdSnapshot.isGood() ? tcds::utils::DIP_QUAL_GOOD : tcds::utils::DIP_QUAL_BAD;

  // - FREV_B1 (in kHz).
  dpInfo.push_back(tcds::utils::DIPDP(toolbox::toString("%s:ExternalApps/CMS/LHC.Timing.RFRX.FREV_B1",
                                                        pvssHost_.c_str()),
                                      toolbox::toString("%f", 1.e-3 * rfrxdSnapshot.rfrxFrevB1()),
                                      dpQualRFRXD));
  // - FREV_B2 (in kHz).
  dpInfo.push_back(tcds::utils::DIPDP(toolbox::toString("%s:ExternalApps/CMS/LHC.Timing.RFRX.FREV_B2",
                                                        pvssHost_.c_str()),
                                      toolbox::toString("%f", 1.e-3 * rfrxdSnapshot.rfrxFrevB1()),
                                      dpQualRFRXD));
  // - F40_B1 (in kHz).
  dpInfo.push_back(tcds::utils::DIPDP(toolbox::toString("%s:ExternalApps/CMS/LHC.Timing.RFRX.F40_B1",
                                                        pvssHost_.c_str()),
                                      toolbox::toString("%f", 1.e-3 * rfrxdSnapshot.rfrxF40B1()),
                                      dpQualRFRXD));
  // - F40_B2 (in kHz).
  dpInfo.push_back(tcds::utils::DIPDP(toolbox::toString("%s:ExternalApps/CMS/LHC.Timing.RFRX.F40_B2",
                                                        pvssHost_.c_str()),
                                      toolbox::toString("%f", 1.e-3 * rfrxdSnapshot.rfrxF40B2()),
                                      dpQualRFRXD));

  tcds::utils::DIP_QUAL const dpQualRF2TTC =
    rf2ttcSnapshot.isGood() ? tcds::utils::DIP_QUAL_GOOD : tcds::utils::DIP_QUAL_BAD;

  // - BC1_QPLL_Lock.
  dpInfo.push_back(tcds::utils::DIPDP(toolbox::toString("%s:ExternalApps/CMS/LHC.Timing.BunchClock.BC1_QPLL_Lock",
                                                        pvssHost_.c_str()),
                                      rf2ttcSnapshot.bunchclockBC1QPLLLock() ? "true" : "false",
                                      dpQualRF2TTC));
  // - BC2_QPLL_Lock.
  dpInfo.push_back(tcds::utils::DIPDP(toolbox::toString("%s:ExternalApps/CMS/LHC.Timing.BunchClock.BC2_QPLL_Lock",
                                                        pvssHost_.c_str()),
                                      rf2ttcSnapshot.bunchclockBC1QPLLLock() ? "true" : "false",
                                      dpQualRF2TTC));
  // - BC1_QPLL_Timestamp.
  dpInfo.push_back(tcds::utils::DIPDP(toolbox::toString("%s:ExternalApps/CMS/LHC.Timing.BunchClock.BC1_QPLL_Timestamp",
                                                        pvssHost_.c_str()),
                                      toolbox::toString("%i",
                                                        int(round(double(rf2ttcSnapshot.bunchclockBC1QPLLTimestamp())))),
                                      dpQualRF2TTC));
  // - BC2_QPLL_Timestamp.
  dpInfo.push_back(tcds::utils::DIPDP(toolbox::toString("%s:ExternalApps/CMS/LHC.Timing.BunchClock.BC2_QPLL_Timestamp",
                                                        pvssHost_.c_str()),
                                      toolbox::toString("%i",
                                                        int(round(double(rf2ttcSnapshot.bunchclockBC2QPLLTimestamp())))),
                                      dpQualRF2TTC));
  // - BCMainSource.
  dpInfo.push_back(tcds::utils::DIPDP(toolbox::toString("%s:ExternalApps/CMS/LHC.Timing.BunchClock.BCMainSource",
                                                        pvssHost_.c_str()),
                                      rf2ttcSnapshot.bunchclockBCMainSource(),
                                      dpQualRF2TTC));

  xoap::MessageReference dpSetCmd = tcds::utils::makePSXDPSetSOAPCmd(dpInfo);

  xoap::MessageReference reply = xoap::createMessage();
  try
    {
      reply = rfDIPPub_.executeSOAPCommand(dpSetCmd, *psx_);
    }
  catch (xcept::Exception const& err)
    {
      std::string const msgBase = "Could not contact the PSX application"
        " (in order to publish the RF information)";
      std::string const msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.what());
      XCEPT_RAISE(tcds::exception::RuntimeProblem, msg.c_str());
    }

  // // Check for server-side(-reported) issues.
  // if (tcds::utils::soap::hasFault(reply))
  //   {
  //     std::string const msgBase = "Received fault from PSX server "
  //       " (when trying to publish the RF information)";
  //     std::string const msgFault = tcds::utils::soap::extractFaultString(reply);
  //     std::string msg = toolbox::toString("%s: '%s'", msgBase.c_str(), msgFault.c_str());
  //     if (tcds::utils::soap::hasFaultDetail(reply))
  //       {
  //         msg += toolbox::toString("(%s).", tcds::utils::soap::extractFaultDetail(reply));
  //       }
  //     else
  //       {
  //         msg += ".";
  //       }
  //     XCEPT_RAISE(tcds::exception::RuntimeProblem, msg.c_str());
  //   }

  tcds::utils::PSXReply psxReply(reply);
}
