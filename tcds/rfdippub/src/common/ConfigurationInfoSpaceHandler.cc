#include "tcds/rfdippub/ConfigurationInfoSpaceHandler.h"

#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/WebServer.h"

tcds::rfdippub::ConfigurationInfoSpaceHandler::ConfigurationInfoSpaceHandler(xdaq::Application& xdaqApp) :
  tcds::utils::ConfigurationInfoSpaceHandler(xdaqApp)
{
  // Where/how to find the flashlists.
  createStringVec("lasURLs",
                  std::vector<std::string>(),
                  "",
                  tcds::utils::InfoSpaceItem::NOUPDATE,
                  true);
  createString("flashlistNameRF2TTCClocks",
               "tcds_rf2ttc_clocks",
               "",
               tcds::utils::InfoSpaceItem::NOUPDATE,
               true);
  createString("flashlistNameRFRXD",
               "tcds_rfrxd_signals",
               "",
               tcds::utils::InfoSpaceItem::NOUPDATE,
               true);
  createString("flashlistNameRFRXDLegend",
               "tcds_rfrxd_legend",
               "",
               tcds::utils::InfoSpaceItem::NOUPDATE,
               true);

  //----------

  // The PVSS host name for the DIP publications.
  createString("pvssHost",
               "dummy",
               "",
               tcds::utils::InfoSpaceItem::NOUPDATE,
               true);
}

void
tcds::rfdippub::ConfigurationInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  std::string const itemSetName = "itemset-app-config";
  monitor.newItemSet(itemSetName);
  monitor.addItem(itemSetName,
                  "lasURLs",
                  "LAS URLs to search for the below flashlist",
                  this);
  monitor.addItem(itemSetName,
                  "flashlistNameRF2TTCClocks",
                  "Flashlist name for the RF2TTC bunch clock information",
                  this);
  monitor.addItem(itemSetName,
                  "flashlistNameRFRXD",
                  "Flashlist name for the RFRXD information",
                  this);
  monitor.addItem(itemSetName,
                  "flashlistNameRFRXDLegend",
                  "Flashlist name for the RFRXD legend",
                  this);
}

void
tcds::rfdippub::ConfigurationInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                             tcds::utils::Monitor& monitor,
                                                                             std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "Configuration" : forceTabName;

  webServer.registerTab(tabName,
                        "Configuration parameters",
                        2);
  webServer.registerTable("Application configuration",
                          "Application configuration parameters",
                          monitor,
                          "itemset-app-config",
                          tabName);
}
