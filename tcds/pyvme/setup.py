#!/usr/bin/env python

from distutils.core import setup
from distutils.core import Extension
import os

# WORKAROUND WORKAROUND WORKAROUND
# The following line makes distutils work on AFS. In AFS hard-links
# between directories are not allowed, making the build fail with a
# complaint like 'error: Invalid cross-device link.' Removing os.link
# makes distutils make copies instead of hard-links.
del os.link
# WORKAROUND WORKAROUND WORKAROUND end

VERSION_STRING = os.environ["PACKAGE_VER_MAJOR"] + \
                 "." + \
                 os.environ["PACKAGE_VER_MINOR"] + \
                 "." + \
                 os.environ["PACKAGE_VER_PATCH"]

NAME_STRING = 'cmsos-' + \
              os.environ['PROJECT_NAME'] + \
              '-' + \
              os.environ['PACKAGE_NAME']

BUILD_HOME = os.environ["BUILD_HOME"]
XDAQ_PLATFORM = os.environ["XDAQ_PLATFORM"]

XDAQ_ROOT = os.environ["XDAQ_ROOT"]

CAENVME_PREFIX = BUILD_HOME + "/daq/extern/caen/caenvmelib/" + XDAQ_PLATFORM
CAENVME_INCLUDE_PREFIX = CAENVME_PREFIX + "/include"
CAENVME_LIB_PREFIX = CAENVME_PREFIX + "/lib"

setup(name = NAME_STRING,
      version = VERSION_STRING,
      description = "Little Python wrapper to access a VME crate via a CAEN VME bridge.",
      author = "Jeroen Hegeman",
      author_email = "jeroen.hegeman@cern.ch",
      url = "https://twiki.cern.ch/twiki/bin/view/CMS/TcdsNotes",
      ext_modules=[Extension("pyvme",
                             sources=["vme.c"],
                             extra_compile_args=["-DLINUX"],
                             include_dirs=[CAENVME_INCLUDE_PREFIX,
                                           XDAQ_ROOT + "/include"],
                             library_dirs=[CAENVME_LIB_PREFIX,
                                           XDAQ_ROOT + "/lib"],
                             libraries=["CAENVME"])],
      scripts = [])
