#!/usr/bin/env python

import sys
sys.path.append("/afs/cern.ch/user/j/jhegeman/work_in_progress/cms_tcds/sw_devel/cmsos/trunk/daq/tcds/pyvme/build/lib.linux-x86_64-2.6")

import pyvme

import pdb

if __name__ == "__main__":

    unit = 0
    chain = 0
    base_address = (0x9 << 20)
    vme = pyvme.VME(unit, chain)

    #----------

    tmp = vme.read_register(base_address + 0x00000, 0x09, 4)
    print "TEST 0: read = 0x{0:x}".format(tmp)

    #----------

    # This should be the bc1_dac register.
    address = 0x7fbec
    tmp = vme.read_register(base_address + address, 0x09, 4)
    print "TEST 1: read = 0x{0:x}".format(tmp)

    print "TEST 2: trying to write 0x{0:x}".format(tmp+1)
    vme.write_register(base_address + address, 0x09, 4, tmp+1)

    tmp = vme.read_register(base_address + address, 0x09, 4)
    print "TEST 3: read = 0x{0:x}".format(tmp)

    #----------

    print "Done"
