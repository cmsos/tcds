// NOTE: Python.h should be the _first_ header that is included.
#include <Python.h>

#include <stdint.h>
#include <structmember.h>

// CAEN VME-library includes.
#include "CAENVMElib.h"
#include "CAENVMEtypes.h"

//-----------------------------------------------------------------------------

#define CAENVME_INVALID_HANDLE -1

// The name of the module.
static char const moduleName[] = "pyvme";

// The doc-string of the module.
static char const moduleDocString[] =
  "Little Python wrapper to access a VME crate via a CAEN VME bridge.";

// We can handle up to eight PCIe cards, with four links each.
static int32_t handles[8][4] = {{CAENVME_INVALID_HANDLE,
                                 CAENVME_INVALID_HANDLE,
                                 CAENVME_INVALID_HANDLE,
                                 CAENVME_INVALID_HANDLE},
                                {CAENVME_INVALID_HANDLE,
                                 CAENVME_INVALID_HANDLE,
                                 CAENVME_INVALID_HANDLE,
                                 CAENVME_INVALID_HANDLE},
                                {CAENVME_INVALID_HANDLE,
                                 CAENVME_INVALID_HANDLE,
                                 CAENVME_INVALID_HANDLE,
                                 CAENVME_INVALID_HANDLE},
                                {CAENVME_INVALID_HANDLE,
                                 CAENVME_INVALID_HANDLE,
                                 CAENVME_INVALID_HANDLE,
                                 CAENVME_INVALID_HANDLE},
                                {CAENVME_INVALID_HANDLE,
                                 CAENVME_INVALID_HANDLE,
                                 CAENVME_INVALID_HANDLE,
                                 CAENVME_INVALID_HANDLE},
                                {CAENVME_INVALID_HANDLE,
                                 CAENVME_INVALID_HANDLE,
                                 CAENVME_INVALID_HANDLE,
                                 CAENVME_INVALID_HANDLE},
                                {CAENVME_INVALID_HANDLE,
                                 CAENVME_INVALID_HANDLE,
                                 CAENVME_INVALID_HANDLE,
                                 CAENVME_INVALID_HANDLE},
                                {CAENVME_INVALID_HANDLE,
                                 CAENVME_INVALID_HANDLE,
                                 CAENVME_INVALID_HANDLE,
                                 CAENVME_INVALID_HANDLE}};
static int32_t handleCounts[8][4] = {{0, 0, 0, 0},
                                     {0, 0, 0, 0},
                                     {0, 0, 0, 0},
                                     {0, 0, 0, 0},
                                     {0, 0, 0, 0},
                                     {0, 0, 0, 0},
                                     {0, 0, 0, 0},
                                     {0, 0, 0, 0}};

//-----------------------------------------------------------------------------

// First part of the definition of the main (Python) VME object
// provided by this module.
typedef struct {
  PyObject_HEAD
  short unit;
  short chain;
  int32_t handle;
} VME;

//-----------------------------------------------------------------------------

// De-allocator method.
static void
vme_dealloc(VME* self)
{
  handleCounts[self->unit][self->chain] -= 1;
  if (handleCounts[self->unit][self->chain] == 0)
    {
      CAENVME_End(self->handle);
      handles[self->unit][self->chain] = CAENVME_INVALID_HANDLE;
    }
  self->ob_type->tp_free((PyObject*)self);
}

//-----------------------------------------------------------------------------

// The Python __new__ method of the VME object.
static PyObject*
vme_new(PyTypeObject* type, PyObject* args, PyObject* kwds)
{
  VME* self;
  self = (VME*)type->tp_alloc(type, 0);
  return (PyObject*)self;
}

//-----------------------------------------------------------------------------

// The Python __init__ method of the VME object.
static int
vme_init(VME* self, PyObject* args, PyObject* kwds)
{
  self->handle = CAENVME_INVALID_HANDLE;
  short unit;
  short chain;

  static char* kwList[] = {"unit",
                           "chain",
                           NULL};

  if (!PyArg_ParseTupleAndKeywords(args, kwds,
                                   "hh", kwList,
                                   &unit, &chain))
    {
      PyErr_SetString(PyExc_Exception, "Error parsing parameters.");
      return -1;
    }

  // BUG BUG BUG
  // Need to check that unit and chain are in range.
  // BUG BUG BUG end

  if (handles[unit][chain] == CAENVME_INVALID_HANDLE)
    {
      CAENVME_API status = CAENVME_Init(cvV2718, unit, chain, &self->handle);

      if (status != cvSuccess)
        {
          PyErr_SetString(PyExc_Exception, "Error initializing CAEN VME bridge.");
          self->handle = CAENVME_INVALID_HANDLE;
          return -1;
        }
      handles[unit][chain] = self->handle;
    }
  else
    {
      self->handle = handles[unit][chain];
    }
  handleCounts[self->unit][self->chain] += 1;

  return 0;
}

//-----------------------------------------------------------------------------

// Single-register read.
static PyObject*
readRegister(VME* self, PyObject* args, PyObject* kwds)
{
  unsigned long addressTmp = 0x0;
  unsigned short addressModifierTmp;
  unsigned short dataWidthTmp;

  static char* kwList[] = {"address",
                           "address_modifier",
                           "data_width",
                           NULL};

  if (!PyArg_ParseTupleAndKeywords(args, kwds,
                                   "KHH", kwList,
                                   &addressTmp,
                                   &addressModifierTmp,
                                   &dataWidthTmp))
    {
      PyErr_SetString(PyExc_Exception, "Error parsing parameters.");
      return NULL;
    }
  uint32_t address = addressTmp;
  CVAddressModifier addressModifier = addressModifierTmp;
  CVDataWidth dataWidth = dataWidthTmp;

  /* printf("readRegister()\n"); */
  /* printf("  address = 0x%x\n", address); */
  /* printf("  addressModifier = 0x%x\n", addressModifier); */
  /* printf("  dataWidth = %d\n", dataWidth); */

  uint32_t result;
  CAENVME_API const status =
    CAENVME_ReadCycle(self->handle, address, (void*)&result, addressModifier, dataWidth);
  if (status != cvSuccess)
    {
      PyErr_SetString(PyExc_Exception, "Error reading from CAEN VME bridge.");
      return NULL;
    }

  return Py_BuildValue("K", result);
}

//-----------------------------------------------------------------------------

// Single-register read.
static PyObject*
writeRegister(VME* self, PyObject* args, PyObject* kwds)
{
  unsigned long addressTmp = 0x0;
  unsigned short addressModifierTmp;
  unsigned short dataWidthTmp;
  unsigned long dataTmp = 0x0;

  static char* kwList[] = {"address",
                           "address_modifier",
                           "data_width",
                           "data",
                           NULL};

  if (!PyArg_ParseTupleAndKeywords(args, kwds,
                                   "kHHk", kwList,
                                   &addressTmp,
                                   &addressModifierTmp,
                                   &dataWidthTmp,
                                   &dataTmp))
    {
      PyErr_SetString(PyExc_Exception, "Error parsing parameters.");
      return NULL;
    }
  uint32_t address = addressTmp;
  CVAddressModifier addressModifier = addressModifierTmp;
  CVDataWidth dataWidth = dataWidthTmp;
  uint32_t data = dataTmp;

  /* printf("writeRegister()\n"); */
  /* printf("  address = 0x%x\n", address); */
  /* printf("  addressModifier = 0x%x\n", addressModifier); */
  /* printf("  dataWidth = %d\n", dataWidth); */
  /* printf("  data = %d\n", data); */

  CAENVME_API const status =
    CAENVME_WriteCycle(self->handle, address, (void*)&data, addressModifier, dataWidth);
  if (status != cvSuccess)
    {
      PyErr_SetString(PyExc_Exception, "Error writing to CAEN VME bridge.");
      printf("ERROR %d\n", status);
      return NULL;
    }

  // Python's equivalent of a void return value.
  Py_RETURN_NONE;
}

//-----------------------------------------------------------------------------

static PyMethodDef VME_methods[] = {
  {"read_register", (PyCFunction)readRegister, METH_VARARGS | METH_KEYWORDS, "Single-register read."},
  {"write_register", (PyCFunction)writeRegister, METH_VARARGS | METH_KEYWORDS, "Single-register write."},
  {NULL} /* Sentinel */
};

//-----------------------------------------------------------------------------

// Second part of the definition of the main (Python) VME object
// provided by this module.
static PyTypeObject VMEType = {
    PyObject_HEAD_INIT(NULL)
    0,                         /*ob_size*/
    "vme.VME",                 /*tp_name*/
    sizeof(VME),               /*tp_basicsize*/
    0,                         /*tp_itemsize*/
    (destructor)vme_dealloc,   /*tp_dealloc*/
    0,                         /*tp_print*/
    0,                         /*tp_getattr*/
    0,                         /*tp_setattr*/
    0,                         /*tp_compare*/
    0,                         /*tp_repr*/
    0,                         /*tp_as_number*/
    0,                         /*tp_as_sequence*/
    0,                         /*tp_as_mapping*/
    0,                         /*tp_hash */
    0,                         /*tp_call*/
    0,                         /*tp_str*/
    0,                         /*tp_getattro*/
    0,                         /*tp_setattro*/
    0,                         /*tp_as_buffer*/
    Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE, /*tp_flags*/
    "CAEN VME bridge object",  /* tp_doc */
    0,                         /* tp_traverse */
    0,                         /* tp_clear */
    0,                         /* tp_richcompare */
    0,                         /* tp_weaklistoffset */
    0,                         /* tp_iter */
    0,                         /* tp_iternext */
    VME_methods,               /* tp_methods */
    0,                         /* tp_members */
    0,                         /* tp_getset */
    0,                         /* tp_base */
    0,                         /* tp_dict */
    0,                         /* tp_descr_get */
    0,                         /* tp_descr_set */
    0,                         /* tp_dictoffset */
    (initproc)vme_init,        /* tp_init */
    0,                         /* tp_alloc */
    vme_new                    /* tp_new */
};

//-----------------------------------------------------------------------------

// The module functions.
static PyMethodDef moduleMethods[] = {
  {NULL}  /* Sentinel */
};

//-----------------------------------------------------------------------------

PyMODINIT_FUNC
initpyvme(void)
{
  VMEType.tp_new = PyType_GenericNew;
  if (PyType_Ready(&VMEType) < 0)
    {
      return;
    }

  PyObject* m = Py_InitModule3(moduleName, moduleMethods, moduleDocString);

  Py_INCREF(&VMEType);
  PyModule_AddObject(m, "VME", (PyObject*)&VMEType);
}

//-----------------------------------------------------------------------------
