#include "tcds/deadwood/ConfigurationInfoSpaceHandler.h"

tcds::deadwood::ConfigurationInfoSpaceHandler::ConfigurationInfoSpaceHandler(xdaq::Application& xdaqApp) :
  tcds::utils::ConfigurationInfoSpaceHandler(xdaqApp)
{
}

void
tcds::deadwood::ConfigurationInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  utils::ConfigurationInfoSpaceHandler::registerItemSetsWithMonitor(monitor);
}

void
tcds::deadwood::ConfigurationInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                             tcds::utils::Monitor& monitor,
                                                                             std::string const& forceTabName)
{
  utils::ConfigurationInfoSpaceHandler::registerItemSetsWithWebServer(webServer, monitor, forceTabName);
}
