#include "tcds/deadwood/DeadWood.h"

#include <algorithm>
#include <ctime>
#include <memory>

#include "toolbox/string.h"
#include "toolbox/TimeVal.h"
#include "xcept/Exception.h"
#include "xdaq/exception/ApplicationInstantiationFailed.h"

#include "tcds/deadwood/ConfigurationInfoSpaceHandler.h"
#include "tcds/exception/Exception.h"
#include "tcds/hwlayer/DeviceBase.h"
#include "tcds/utils/ApplicationStateInfoSpaceHandler.h"
#include "tcds/utils/ConfigurationInfoSpaceHandler.h"
#include "tcds/utils/FSMSOAPParHelper.h"

XDAQ_INSTANTIATOR_IMPL(tcds::deadwood::DeadWood)

tcds::deadwood::DeadWood::DeadWood(xdaq::ApplicationStub* const stub)
try
  :
  tcds::utils::XDAQAppWithFSMForPMs(stub, std::unique_ptr<tcds::hwlayer::DeviceBase>(new tcds::deadwood::DummyDevice())),
    randomDevice_(),
    randomGenerator_(randomDevice_()),
    soapCmdInitCyclicGenerators_(*this),
    soapCmdSendBCommand_(*this),
    soapCmdSendBgo_(*this),
    soapCmdSendL1A_(*this)
  {
    // Create the InfoSpace holding all configuration information.
    cfgInfoSpaceP_ =
      std::unique_ptr<ConfigurationInfoSpaceHandler>(new ConfigurationInfoSpaceHandler(*this));
  }
catch (tcds::exception::Exception const& err)
  {
    std::string msgBase = "Something went wrong instantiating the DeadWood application";
    std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.what());
    XCEPT_RAISE(xdaq::exception::ApplicationInstantiationFailed, msg.c_str());
  }

tcds::deadwood::DeadWood::~DeadWood()
{
}

void
tcds::deadwood::DeadWood::setupInfoSpaces()
{
  // Make sure the configuration settings are up-to-date.
  cfgInfoSpaceP_->readInfoSpace();

  // Register all InfoSpaceItems with the Monitor.
  cfgInfoSpaceP_->registerItemSets(monitor_, webServer_);
  appStateInfoSpace_.registerItemSets(monitor_, webServer_);
}

void
tcds::deadwood::DeadWood::hwConnectImpl()
{
}

void
tcds::deadwood::DeadWood::hwReleaseImpl()
{
}

void
tcds::deadwood::DeadWood::hwConfigureImpl()
{
}

std::vector<tcds::utils::FSMSOAPParHelper>
tcds::deadwood::DeadWood::expectedFSMSoapParsImpl(std::string const& commandName) const
{
  // Define what we expect in terms of parameters for each SOAP FSM
  // command.
  // NOTE: The DeadWood is special in the sense that it has to be able
  // to accept any parameter that any other application can accept.

  std::vector<tcds::utils::FSMSOAPParHelper> params;
  if ((commandName == "Configure") ||
      (commandName == "Reconfigure"))
    {
      // From anything with hardware.
      params.push_back(tcds::utils::FSMSOAPParHelper(commandName, "hardwareConfigurationString", false));
      // From the CPM/LPM/PI.
      params.push_back(tcds::utils::FSMSOAPParHelper(commandName, "fedEnableMask", false));
      // From the CPM.
      params.push_back(tcds::utils::FSMSOAPParHelper(commandName, "noBeamActive", false));
      // From the PI.
      params.push_back(tcds::utils::FSMSOAPParHelper(commandName, "usePrimaryTCDS", false));
      params.push_back(tcds::utils::FSMSOAPParHelper(commandName, "skipPLLReset", false));
    }
  else if (commandName == "Enable")
    {
      // NOTE: The runNumber is the only required parameter.
      params.push_back(tcds::utils::FSMSOAPParHelper(commandName, "runNumber", true));
    }

  return params;
}

tcds::deadwood::DummyDevice&
tcds::deadwood::DeadWood::getHw() const
{
  return static_cast<tcds::deadwood::DummyDevice&>(*hwP_.get());
}

void
tcds::deadwood::DeadWood::configureActionImpl(toolbox::Event::Reference event)
{
  takeRandomNap(2);
  tcds::utils::XDAQAppWithFSMForPMs::configureActionImpl(event);
}

void
tcds::deadwood::DeadWood::enableActionImpl(toolbox::Event::Reference event)
{
  takeRandomNap(2);
}

void
tcds::deadwood::DeadWood::failActionImpl(toolbox::Event::Reference event)
{
  takeRandomNap(2);
}

void
tcds::deadwood::DeadWood::haltActionImpl(toolbox::Event::Reference event)
{
  takeRandomNap(2);
  tcds::utils::XDAQAppWithFSMForPMs::haltActionImpl(event);
}

void
tcds::deadwood::DeadWood::pauseActionImpl(toolbox::Event::Reference event)
{
  takeRandomNap(2);
}

void
tcds::deadwood::DeadWood::resumeActionImpl(toolbox::Event::Reference event)
{
  takeRandomNap(2);
}

void
tcds::deadwood::DeadWood::stopActionImpl(toolbox::Event::Reference event)
{
  takeRandomNap(2);
}

void
tcds::deadwood::DeadWood::ttcHardResetActionImpl(toolbox::Event::Reference event)
{
  takeRandomNap(2);
}

void
tcds::deadwood::DeadWood::ttcResyncActionImpl(toolbox::Event::Reference event)
{
  takeRandomNap(2);
}

void
tcds::deadwood::DeadWood::takeRandomNap(size_t const seconds) const
{
  double const scale = 1.e9;
  double const mean = scale * seconds;
  double const std = mean;

  std::normal_distribution<> normalDist(mean, std);
  double const tmp = std::max(normalDist(randomGenerator_), 0.);
  time_t const nSeconds = tmp / scale;
  long const nNanoSeconds = tmp - (nSeconds * scale);
  struct timespec tsReq;
  struct timespec tsRem;
  tsReq.tv_sec = nSeconds;
  tsReq.tv_nsec = nNanoSeconds;
  ::nanosleep(&tsReq, &tsRem);
}
