#ifndef _tcds_deadwood_DummyDevice_h_
#define _tcds_deadwood_DummyDevice_h_

#include <stdint.h>
#include <string>
#include <vector>

#include "tcds/hwlayer/ConfigurationProcessor.h"
#include "tcds/hwlayer/DeviceBase.h"
#include "tcds/hwlayer/IHwDevice.h"
#include "tcds/hwlayer/RegisterInfo.h"
#include "tcds/utils/Definitions.h"

namespace tcds {
  namespace deadwood {

    /**
     * This is a dummy device class for use with the DeadWood TCDS
     * dummy controller application. It's only purpose is to provide a
     * back-end accepting _any_ command associated with _any_ SOAP
     * command. (See the DeadWood documentation for details.) No
     * hardware is connected, so no implementation exists for anything
     * in this class.
     */
    class DummyDevice : public tcds::hwlayer::DeviceBase
    {

    public:
      DummyDevice();
      virtual ~DummyDevice();

      virtual std::vector<std::string> getRegisterNamesImpl() const;
      virtual tcds::hwlayer::RegisterInfo::RegInfoVec getRegisterInfosImpl() const;

      virtual uint32_t readRegisterImpl(std::string const& regName) const;
      virtual void writeRegisterImpl(std::string const& regName,
                                     uint32_t const regVal) const;

      virtual std::vector<uint32_t> readBlockImpl(std::string const& regName,
                                                  uint32_t const nWords) const;
      virtual std::vector<uint32_t> readBlockOffsetImpl(std::string const& regName,
                                                        uint32_t const nWords,
                                                        uint32_t const offset) const;
      virtual void writeBlockImpl(std::string const& regName,
                                  std::vector<uint32_t> const& regVals) const;

      virtual uint32_t getBlockSizeImpl(std::string const& regName) const;

      virtual void writeHardwareConfigurationImpl(tcds::hwlayer::ConfigurationProcessor::RegValVec const& cfg) const;
      virtual tcds::hwlayer::DeviceBase::RegContentsVec
        readHardwareConfigurationImpl(tcds::hwlayer::RegisterInfo::RegInfoVec const& regInfos) const;

      virtual tcds::hwlayer::RegDumpVec dumpRegisterContentsImpl() const;

      void sendBCommand(tcds::definitions::BCOMMAND_TYPE const bcommandType,
                        tcds::definitions::bcommand_data_t const data,
                        tcds::definitions::bcommand_address_t const address,
                        tcds::definitions::bcommand_address_t const subAddress=0x0,
                        tcds::definitions::BCOMMAND_ADDRESS_TYPE const addressType=tcds::definitions::BCOMMAND_ADDRESS_TYPE_INTERNAL) const;
      void sendBgo(tcds::definitions::BGO_NUM const bgoNumber) const;
      void sendL1A() const;

    protected:
      virtual bool isReadyForUseImpl() const;

    };

  } // namespace deadwood
} // namespace tcds

#endif // _tcds_deadwood_DummyDevice_h_
