#ifndef _tcds_deadwood_DeadWood_h_
#define _tcds_deadwood_DeadWood_h_

#include <random>
#include <stddef.h>
#include <string>
#include <vector>

#include "toolbox/Event.h"
#include "xdaq/Application.h"

#include "tcds/deadwood/DummyDevice.h"
#include "tcds/utils/FSMSOAPParHelper.h"
#include "tcds/utils/SOAPCmdBase.h"
#include "tcds/utils/SOAPCmdInitCyclicGenerators.h"
#include "tcds/utils/SOAPCmdSendBCommand.h"
#include "tcds/utils/SOAPCmdSendBgo.h"
#include "tcds/utils/SOAPCmdSendL1A.h"
#include "tcds/utils/XDAQAppWithFSMForPMs.h"

namespace xdaq {
  class ApplicationStub;
}

namespace tcds {
  namespace deadwood {

    /**
     * The DeadWood is a dummy application for subsystems to practice
     * with the SOAP interface to the TCDS control applications. In
     * that context it accepts _any_ SOAP command implemented in _any_
     * of the TCDS control applications. For these SOAP commands all
     * syntax- and parameter checking is performed as usual, but since
     * no hardware is connected, nothing actually happens.
     */
    class DeadWood : public tcds::utils::XDAQAppWithFSMForPMs
    {

    public:
      XDAQ_INSTANTIATOR();

      DeadWood(xdaq::ApplicationStub* const stub);
      virtual ~DeadWood();

    protected:
      virtual void setupInfoSpaces();

      /**
       * Access the hardware pointer as DummyDevice&.
       */
      virtual DummyDevice& getHw() const;

      virtual void configureActionImpl(toolbox::Event::Reference event);
      virtual void enableActionImpl(toolbox::Event::Reference event);
      virtual void failActionImpl(toolbox::Event::Reference event);
      virtual void haltActionImpl(toolbox::Event::Reference event);
      virtual void pauseActionImpl(toolbox::Event::Reference event);
      virtual void resumeActionImpl(toolbox::Event::Reference event);
      virtual void stopActionImpl(toolbox::Event::Reference event);
      virtual void ttcResyncActionImpl(toolbox::Event::Reference event);
      virtual void ttcHardResetActionImpl(toolbox::Event::Reference event);

      // Dummy methods in this case.
      virtual void hwConnectImpl();
      virtual void hwReleaseImpl();
      virtual void hwConfigureImpl();

      virtual std::vector<tcds::utils::FSMSOAPParHelper> expectedFSMSoapParsImpl(std::string const& commandName) const;

    private:
      // A little helper to generate a bit of spread on the state
      // transition durations.
      void takeRandomNap(size_t const seconds) const;

      mutable std::random_device randomDevice_;
      mutable std::mt19937 randomGenerator_;

      // The SOAP commands.
      template<typename> friend class tcds::utils::SOAPCmdBase;
      template<typename> friend class tcds::utils::SOAPCmdInitCyclicGenerators;
      template<typename> friend class tcds::utils::SOAPCmdSendBCommand;
      template<typename> friend class tcds::utils::SOAPCmdSendBgo;
      template<typename> friend class tcds::utils::SOAPCmdSendL1A;
      tcds::utils::SOAPCmdSendBgo<DeadWood> soapCmdInitCyclicGenerators_;
      tcds::utils::SOAPCmdSendBgo<DeadWood> soapCmdSendBCommand_;
      tcds::utils::SOAPCmdSendBgo<DeadWood> soapCmdSendBgo_;
      tcds::utils::SOAPCmdSendL1A<DeadWood> soapCmdSendL1A_;

    };

  } // namespace deadwood
} // namespace tcds

#endif // _tcds_deadwood_DeadWood_h_
