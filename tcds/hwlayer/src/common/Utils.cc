#include "tcds/hwlayer/Utils.h"

#include <cassert>
#include <climits>
#include <cstdlib>
#include <list>
#include <sstream>
#include <cstddef>
#include <ctime>

#include "toolbox/string.h"

uint32_t
tcds::hwlayer::hexStringToUInt32(std::string const& stringIn)
{
  std::stringstream tmp;
  tmp << std::hex << stringIn;
  uint32_t res;
  tmp >> res;
  return res;
}

std::vector<uint32_t>
tcds::hwlayer::hexStringToUInt32Vec(std::string const& stringIn, std::string const& delim)
{
  std::list<std::string> tmp = toolbox::parseTokenList(stringIn, delim);
  std::vector<uint32_t> res;
  for (std::list<std::string>::const_iterator it = tmp.begin();
       it != tmp.end();
       ++it)
    {
      if (it->size())
        {
          res.push_back(hexStringToUInt32(*it));
        }
    }
  return res;
}

std::string
tcds::hwlayer::uint32ToString(uint32_t const val)
{
  // ASSERT ASSERT ASSERT
  // This only works if chars are at least 8-bit variables.
  assert (CHAR_BIT >= 8);
  // ASSERT ASSERT ASSERT end

  std::stringstream res;
  res << char((val & uint32_t(0xff000000)) / 16777216);
  res << char((val & uint32_t(0x00ff0000)) / 65536);
  res << char((val & uint32_t(0x0000ff00)) / 256);
  res << char((val & uint32_t(0x000000ff)));
  return res.str();
}

std::string
tcds::hwlayer::trimString(std::string const& stringToTrim,
                          std::string const& whitespace)
{
  size_t const strBegin = stringToTrim.find_first_not_of(whitespace);
  if (strBegin == std::string::npos)
    {
      // No content at all in this string.
      return "";
    }

  size_t const strEnd = stringToTrim.find_last_not_of(whitespace);
  size_t const strRange = strEnd - strBegin + 1;

  return stringToTrim.substr(strBegin, strRange);
}

int
tcds::hwlayer::nanosleep(long const nNanoseconds)
{
  struct timespec ts;
  struct timespec td;
  ts.tv_sec = 0;
  ts.tv_nsec = nNanoseconds;
  int const res = ::nanosleep(&ts, &td);
  return res;
}

int
tcds::hwlayer::sleep(time_t const nSeconds)
{
  struct timespec ts;
  struct timespec td;
  ts.tv_sec = nSeconds;
  ts.tv_nsec = 0;
  int const res = ::nanosleep(&ts, &td);
  return res;
}

int
tcds::hwlayer::randomSleep(time_t const nSeconds)
{
  double const scale = 1.e9;
  double const nNanoseconds = scale * nSeconds;

  std::srand(time(0));
  double const rnd = nNanoseconds * (std::rand() / double(RAND_MAX));

  time_t const nSec = rnd / scale;
  long const nNanoSec = rnd - (nSec * scale);
  struct timespec tsReq;
  struct timespec tsRem;
  tsReq.tv_sec = nSec;
  tsReq.tv_nsec = nNanoSec;
  return ::nanosleep(&tsReq, &tsRem);
}
