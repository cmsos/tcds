#include "tcds/hwlayer/I2CAccessor.h"

#include <cassert>
#include <cstdlib>
#include <sstream>
#include <syscall.h>
#include <unistd.h>

#include "toolbox/string.h"
#include "xcept/Exception.h"

#include "tcds/exception/Exception.h"
#include "tcds/hwlayer/IHwDevice.h"
#include "tcds/hwlayer/Utils.h"

namespace tcds {
  namespace hwlayer {
    std::ostream& operator<<(std::ostream& os,
                             I2CAccessor::I2CMaster const& busMaster)
    {
      switch(busMaster)
        {
        case I2CAccessor::I2CMasterSystem:
          os << "system";
          break;
        case I2CAccessor::I2CMasterUser:
          os << "user";
          break;
        default:
          // ASSERT ASSERT ASSERT
          assert (false);
          // ASSERT ASSERT ASSERT end
          break;
        }
      return os;
    }
  }
}

tcds::hwlayer::I2CAccessor::I2CAccessor(IHwDevice& device,
                                        I2CMaster const busMaster,
                                        I2CBus const bus) :
  hwDevice_(device),
  i2cMaster_(busMaster),
  i2cBus_(bus)
{
  // Convert the I2CBus enum into a bus name in string format that we
  // need in order to find the associated register.
  std::stringstream tmp;
  tmp << i2cMaster_;
  busName_ = tmp.str();

  // Acquire the bus, and if we succeed, configure the bus.
  acquire();
  configure();
}

tcds::hwlayer::I2CAccessor::~I2CAccessor()
{
  release();
}

uint32_t
tcds::hwlayer::I2CAccessor::readRegister(uint32_t const slaveAddress) const
{
  bool const isWrite = false;
  bool const isMemory = false;
  uint32_t const memoryAddress = 0x0;
  uint32_t const data = 0x0;
  uint32_t res = i2cTransaction(slaveAddress, isWrite, memoryAddress, data, isMemory);
  return res;
}

void
tcds::hwlayer::I2CAccessor::writeRegister(uint32_t const slaveAddress,
                                          uint32_t const data) const
{
  bool const isWrite = true;
  bool const isMemory = false;
  uint32_t const memoryAddress = 0x0;
  i2cTransaction(slaveAddress, isWrite, memoryAddress, data, isMemory);
}

uint32_t
tcds::hwlayer::I2CAccessor::readMemoryEntry(uint32_t const slaveAddress,
                                            uint32_t const memoryAddress) const
{
  bool const isWrite = false;
  bool const isMemory = true;
  uint32_t const data = 0x0;
  uint32_t res = i2cTransaction(slaveAddress, isWrite, memoryAddress, data, isMemory);
  return res;
}

void
tcds::hwlayer::I2CAccessor::writeMemoryEntry(uint32_t const slaveAddress,
                                             uint32_t const memoryAddress,
                                             uint32_t const data) const
{
  bool const isWrite = true;
  bool const isMemory = true;
  i2cTransaction(slaveAddress, isWrite, memoryAddress, data, isMemory);
}

void
tcds::hwlayer::I2CAccessor::acquire() const
{
  std::string const regNameBase = busName_ + ".i2c.settings";
  pid_t const tidRaw = pid_t(::syscall(SYS_gettid));
  // We only have eight bits for the bus owner id. So let's take the
  // lower bits of the thread ID.
  uint32_t const tid = (tidRaw & 0xff);

  bool success = false;
  size_t numTries = 0;
  while (!success && (numTries < kMaxNumTriesAcquireBus))
    {
      hwDevice_.writeRegister(regNameBase + ".bus_owner_write", tid);
      // Upon the 0->1 transition of the enable bit the contents of
      // bus_owner_write gets copied into bus_owner_read. If we read
      // back our own (pseudo-)thread-id, we are the bus owner.
      hwDevice_.writeRegister(regNameBase + ".enable", 0x1);
      uint32_t const busOwner = hwDevice_.readRegister(regNameBase + ".bus_owner_read");
      numTries += 1;
      if (busOwner == uint32_t(tid))
        {
          success = true;
          break;
        }
      else
      {
        // NOTE: Use a random sleep interval before trying again. This
        // is to avoid that a bus contention due to several
        // applications polling in-sync, persists.
        long const maxNsec = 100000000;
        float const delayRnd = (1. * rand() / RAND_MAX) * maxNsec;
        tcds::hwlayer::nanosleep(delayRnd);
      }
    }
  if (!success)
    {
      // NOTE: This also implies that it was not us who enabled the
      // bus, so we don't disable it!
      // NOTE: This is safe, since throwing the next exception will
      // _not_ call the destructor, because acquire() is called from
      // within our own constructor.
      std::string const msg = "I2C " + busName_ + " bus is busy.";
      XCEPT_RAISE(tcds::exception::I2CBusBusy, msg.c_str());
    }
}

void
tcds::hwlayer::I2CAccessor::release() const
{
  std::string const regNameBase = busName_ + ".i2c.settings";
  hwDevice_.writeRegister(regNameBase + ".enable", 0x0);
  hwDevice_.writeRegister(regNameBase + ".bus_owner_write", 0x0);
}

void
tcds::hwlayer::I2CAccessor::configure() const
{
  std::string const regNameBase = busName_ + ".i2c.settings";
  hwDevice_.writeRegister(regNameBase + ".prescaler", kI2CPrescaler);
  uint32_t busSelectVal = 0x0;
  switch(i2cBus_)
    {
    case I2CBus0:
      busSelectVal = 0x0;
      break;
    case I2CBus1:
      busSelectVal = 0x1;
      break;
    default:
      // ASSERT ASSERT ASSERT
      assert (false);
      // ASSERT ASSERT ASSERT end
      break;
    }
  hwDevice_.writeRegister(regNameBase + ".bus_select", busSelectVal);
}

uint32_t
tcds::hwlayer::I2CAccessor::i2cTransaction(uint32_t const slaveAddress,
                                           bool const isWrite,
                                           uint32_t const memoryAddress,
                                           uint32_t const writeData,
                                           bool const isMemory) const
{
  std::string const regNameBase = busName_ + ".i2c";

  // Write a low strobe to prepare for the transaction.
  hwDevice_.writeRegister(regNameBase + ".command.strobe", 0x0);

  // Write all command details.
  hwDevice_.writeRegister(regNameBase + ".command.slv_addr", slaveAddress);
  uint32_t isWriteVal = 0x0;
  if (isWrite)
    {
      isWriteVal = 0x1;
    }
  hwDevice_.writeRegister(regNameBase + ".command.wr_en", isWriteVal);
  hwDevice_.writeRegister(regNameBase + ".command.mem_addr", memoryAddress);
  hwDevice_.writeRegister(regNameBase + ".command.wrdata", writeData);
  uint32_t isMemoryVal = 0x0;
  if (isMemory)
    {
      isMemoryVal = 0x1;
    }
  hwDevice_.writeRegister(regNameBase + ".command.is_mem", isMemoryVal);
  hwDevice_.writeRegister(regNameBase + ".command.mode16b", 0x0);

  // Write a high strobe to trigger the transaction.
  hwDevice_.writeRegister(regNameBase + ".command.strobe", 0x1);

  // Wait for the I2C transaction to finish.
  uint32_t i2cStatus = hwDevice_.readRegister(regNameBase + ".reply.status");
  size_t numTries = 1;
  while ((i2cStatus == kI2CStatusBusy) && (numTries < kMaxNumTriesTransactionReply))
    {
      i2cStatus = hwDevice_.readRegister(regNameBase + ".reply.status");
      numTries += 1;
      tcds::hwlayer::nanosleep(100000000);
    }

  uint32_t res = 0x1;
  if (i2cStatus == kI2CStatusSuccess)
    {
      res = hwDevice_.readRegister(regNameBase + ".reply.rddata_lo");
    }
  else
    {
      if (i2cStatus == kI2CStatusBusy)
        {
          std::string const msg = "The I2C transaction did not finish.";
          XCEPT_RAISE(tcds::exception::I2CProblem, msg.c_str());
        }
      else
        {
          std::string const msg = toolbox::toString("The I2C transaction failed (status 0x%d).",
                                                    i2cStatus);
          XCEPT_RAISE(tcds::exception::I2CProblem, msg.c_str());
        }
    }

  return res;
}
