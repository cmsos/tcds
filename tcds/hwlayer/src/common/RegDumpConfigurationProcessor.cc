#include "tcds/hwlayer/RegDumpConfigurationProcessor.h"

#include <algorithm>
#include <iomanip>
#include <ios>
#include <list>
#include <map>
#include <sstream>
#include <cstddef>
#include <stdint.h>
#include <utility>
#include <vector>

#include "toolbox/regex.h"
#include "toolbox/string.h"
#include "xcept/Exception.h"

#include "tcds/exception/Exception.h"
#include "tcds/hwlayer/Utils.h"

tcds::hwlayer::RegDumpConfigurationProcessor::RegDumpConfigurationProcessor() :
  ConfigurationProcessor()
{
}

tcds::hwlayer::RegDumpConfigurationProcessor::~RegDumpConfigurationProcessor()
{
}

tcds::hwlayer::RegDumpConfigurationProcessor::RegValVec
tcds::hwlayer::RegDumpConfigurationProcessor::parseImpl(std::string const& configurationString) const
{
  std::string const whiteSpaceSet = "[ \t]";
  std::string const nonWhiteSpaceSet = "[^ \t]";
  std::string const valueSet = "[0-9a-fA-F]";

  std::string const whiteSpaceBlock = whiteSpaceSet + "*";
  std::string const nonWhiteSpaceBlock = nonWhiteSpaceSet + "+";
  std::string const valueBlock = "0x" + valueSet + "{8}";

  std::string const nonWhiteSpaceGroup = "(" + nonWhiteSpaceBlock + ")";
  std::string const valueGroup = "(" + valueBlock  + ")";
  std::string const valuesGroup = "((" + valueGroup + whiteSpaceSet + "*?" + ")+)";

  std::string const pattern =
    "^" +
    whiteSpaceBlock +
    nonWhiteSpaceGroup +
    whiteSpaceBlock +
    valuesGroup +
    whiteSpaceBlock +
    "$";

  // NOTE: The following is a bit convoluted due to the fact that we
  // would like to keep the order of the registers intact (easier for
  // the user, and for debugging).

  // Since multi-word registers are spread over several lines, we keep
  // track of everything in a map, using the register name as key, and
  // only in the end turn everything into a simple vector.
  typedef std::map<std::string, std::vector<uint32_t> > RegValMap;
  RegValMap tmpRegVals;
  std::vector<std::string> regNames;
  std::list<std::string> cfgLines = toolbox::parseTokenList(configurationString, "\n");
  for (std::list<std::string>::const_iterator it = cfgLines.begin();
       it != cfgLines.end();
       ++it)
    {
      std::string cleanedLine = trimString(*it);

      // Skip empty lines.
      if (cleanedLine.size() < 1)
        {
          continue;
        }

      // Remove comment and ignore line if nothing else left.
      size_t ind = cleanedLine.find(kCommentChar);
      if (ind != std::string::npos)
        {
          cleanedLine.erase(ind);
        }
      if (cleanedLine.size() < 1)
        {
          continue;
        }

      std::vector<std::string> pieces;
      if (toolbox::regx_match(cleanedLine, pattern, pieces))
        {
          std::string const regName = pieces.at(1);
          std::string const regValuesStr = pieces.at(2);
          std::vector<uint32_t> regValues = tcds::hwlayer::hexStringToUInt32Vec(regValuesStr);
          std::vector<uint32_t>& tmp = tmpRegVals[regName];
          tmp.insert(tmp.end(), regValues.begin(), regValues.end());
          if (std::find(regNames.begin(), regNames.end(), regName) == regNames.end())
            {
              regNames.push_back(regName);
            }
        }
      else
        {
          std::string msg = "Problem parsing configuration line '" +
            *it +
            "'.";
          XCEPT_RAISE(tcds::exception::ConfigurationParseProblem, msg);
        }
    }

  // Now turn everything from a map into a vector and return the
  // result.
  ConfigurationProcessor::RegValVec res;
  for (std::vector<std::string>::const_iterator it = regNames.begin();
       it != regNames.end();
       ++it)
    {
      res.push_back(std::make_pair(*it, tmpRegVals[*it]));
    }

  return res;
}

std::string
tcds::hwlayer::RegDumpConfigurationProcessor::composeImpl(tcds::hwlayer::ConfigurationProcessor::RegValVec const& hwConfiguration) const
{
  tcds::hwlayer::ConfigurationProcessor::RegValVec hwCfg(hwConfiguration);

  // Sort by name. (By default std::sort() will sort pairs by their
  // first field.)
  std::sort(hwCfg.begin(), hwCfg.end());

  std::vector<std::string> names;
  std::vector<std::string> values;

  std::stringstream tmp;
  tmp << std::hex << std::setfill('0');
  for (tcds::hwlayer::ConfigurationProcessor::RegValVec::const_iterator i = hwCfg.begin();
       i != hwCfg.end();
       ++i)
    {
      std::string const name = i->first;
      std::vector<uint32_t> tmpVals = i->second;

      // NOTE: RAM entries always consist of a data word and a control
      // word, so these are written out in pairs. Anything else is
      // simply written out word-for-word.
      size_t stepSize = 1;
      if (name.compare(name.size() - 4, 4, ".ram") == 0)
        {
          stepSize = 2;
        }
      for (std::vector<uint32_t>::const_iterator j = tmpVals.begin();
           j < tmpVals.end();
           j += stepSize)
        {
          names.push_back(name);
          tmp.str("");
          for (size_t k = 0; k != stepSize; ++k)
            {
              tmp << "0x" << std::setw(8) << *(j + k);
              if (k != (stepSize - 1))
                {
                  tmp << " ";
                }
            }
          values.push_back(tmp.str());
        }
    }

  std::string res;

  if (names.size() > 0)
    {
      // Find the longest name and value so we can properly align the
      // columns.
      size_t const maxLenName =
        std::max_element(names.begin(), names.end(), stringCmp())->size();

      // Stuff everything into one big string, nicely aligned into
      // columns.
      for (size_t i = 0; i != names.size(); ++i)
        {
          std::stringstream tmp;
          tmp << std::left
              << std::setw(maxLenName) << names.at(i)
              << " "
              << values.at(i)
              << std::endl;
          res += tmp.str();
        }
    }

  return res;
}
