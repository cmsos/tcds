#include "tcds/hwlayer/version.h"

#include "config/version.h"
#include "toolbox/version.h"
#include "xcept/version.h"

#include "tcds/exception/version.h"

GETPACKAGEINFO(tcds::hwlayer)

void
tcds::hwlayer::checkPackageDependencies()
{
  CHECKDEPENDENCY(config);
  CHECKDEPENDENCY(toolbox);
  CHECKDEPENDENCY(xcept);

  CHECKDEPENDENCY(tcds::exception);
}

std::set<std::string, std::less<std::string> >
tcds::hwlayer::getPackageDependencies()
{
  std::set<std::string, std::less<std::string> > dependencies;

  ADDDEPENDENCY(dependencies, config);
  ADDDEPENDENCY(dependencies, toolbox);
  ADDDEPENDENCY(dependencies, xcept);

  ADDDEPENDENCY(dependencies, tcds::exception);

  return dependencies;
}
