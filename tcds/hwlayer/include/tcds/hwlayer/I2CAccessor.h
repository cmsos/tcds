#ifndef _tcds_hwlayer_I2CAccessor_h_
#define _tcds_hwlayer_I2CAccessor_h_

#include <ostream>
#include <stddef.h>
#include <stdint.h>
#include <string>

namespace tcds {
  namespace hwlayer {

    class IHwDevice;

    /**
       Class implementing I2C access for GLIB and FC7. Intended to be
       used in RAII fashion. The selected bus is enabled in the
       constructor and freed in the destructor.

       NOTE: Typically, in the TCDS firmware, multiple I2C masters are
       implemented, each connected to different physical SDA/SCL lines
       (and each with its own registers). On the GLIB/FC7, for
       example, one master is connected to the carrier I2C buses, one
       master is connected to the I2C buses on the FMCs. Each master
       can control multiple buses. The 'busMaster' parameter selects
       the bus master, the 'bus' parameter selects the actual I2C bus.
     */
    class I2CAccessor
    {

    public:
      enum I2CMaster {I2CMasterSystem, I2CMasterUser};
      enum I2CBus {I2CBus0, I2CBus1};
      friend std::ostream& operator<<(std::ostream& os,
                                      I2CAccessor::I2CMaster const& busMaster);

      I2CAccessor(IHwDevice& device,
                  I2CMaster const busMaster,
                  I2CBus const bus);
      ~I2CAccessor();

      uint32_t readRegister(uint32_t const slaveAddress) const;
      void writeRegister(uint32_t const slaveAddress,
                         uint32_t const data) const;

      uint32_t readMemoryEntry(uint32_t const slaveAddress,
                               uint32_t const memoryAddress) const;
      void writeMemoryEntry(uint32_t const slaveAddress,
                            uint32_t const memoryAddress,
                            uint32_t const data) const;

    private:
      static size_t const kMaxNumTriesAcquireBus = 10;
      static size_t const kMaxNumTriesTransactionReply = 10;
      static uint32_t const kI2CStatusBusy = 0x0;
      static uint32_t const kI2CStatusSuccess = 0x1;
      static uint32_t const kI2CPrescaler = 500;

      // NOTE: The acquire() method should _only_ be called from the
      // constructor. See the comments in acquire() for details.
      void acquire() const;
      void release() const;
      void configure() const;

      /**
         This method is the real work-horse of this class. The actual
         I2C transaction takes place here.
       */
      uint32_t i2cTransaction(uint32_t const slaveAddress,
                              bool const isWrite,
                              uint32_t const memoryAddress,
                              uint32_t const writeData,
                              bool const isMemory) const;

      IHwDevice& hwDevice_;
      I2CMaster const i2cMaster_;
      I2CBus const i2cBus_;
      std::string busName_;

    };

  } // namespace hwlayer
} // namespace tcds

#endif // _tcds_hwlayer_I2CAccessor_h_
