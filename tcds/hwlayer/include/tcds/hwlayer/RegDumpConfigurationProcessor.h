#ifndef _tcds_hwlayer_RegDumpConfigurationProcessor_h_
#define _tcds_hwlayer_RegDumpConfigurationProcessor_h_

#include <functional>
#include <string>

#include "tcds/hwlayer/ConfigurationProcessor.h"

namespace tcds {
  namespace hwlayer {

    class RegDumpConfigurationProcessor : public ConfigurationProcessor
    {

    public:
      static char const kCommentChar = '#';

      RegDumpConfigurationProcessor();
      virtual ~RegDumpConfigurationProcessor();

    protected:
      virtual RegValVec parseImpl(std::string const& configurationString) const;
      virtual std::string composeImpl(tcds::hwlayer::ConfigurationProcessor::RegValVec const& hwConfiguration) const;

    private:
      struct stringCmp : std::binary_function<std::string, std::string, bool> {
        bool operator() (std::string const& a, std::string const& b)
        {
          return (a.size() < b.size());
        }
      };

    };

  } // namespace hwlayer
} // namespace tcds

#endif // _tcds_hwlayer_RegDumpConfigurationProcessor_h_
