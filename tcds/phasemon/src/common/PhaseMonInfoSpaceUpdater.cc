#include "tcds/phasemon/PhaseMonInfoSpaceUpdater.h"

#include <stdint.h>
#include <string>

#include "tcds/phasemon/Definitions.h"
#include "tcds/phasemon/TCADevicePhaseMon.h"
#include "tcds/phasemon/Utils.h"
#include "tcds/utils/ConfigurationInfoSpaceHandler.h"
#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/XDAQAppBase.h"

tcds::phasemon::PhaseMonInfoSpaceUpdater::PhaseMonInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                                                   tcds::phasemon::TCADevicePhaseMon const& hw) :
  tcds::utils::HwInfoSpaceUpdaterBase(xdaqApp, hw)
{
}

tcds::phasemon::PhaseMonInfoSpaceUpdater::~PhaseMonInfoSpaceUpdater()
{
}

bool
tcds::phasemon::PhaseMonInfoSpaceUpdater::updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                                              tcds::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  bool updated = false;
  tcds::phasemon::TCADevicePhaseMon const& hw = getHw();
  if (hw.isReadyForUse())
    {
      std::string name = item.name();
      tcds::utils::InfoSpaceItem::UpdateType updateType = item.updateType();
      if (updateType == tcds::utils::InfoSpaceItem::PROCESS)
        {
          // The 'PROCESS' update type means that there is something
          // special to the variable. Figure out what to do based on the
          // variable name.
          if (name == "ref_clock.src_id")
            {
              tcds::definitions::PHASEMON_REF_CLK_SRC valueEnum = hw.getSelectedRefClk();
              uint32_t const newVal = static_cast<uint32_t>(valueEnum);
              infoSpaceHandler->setUInt32(name, newVal);
              updated = true;
            }
          else if (name == "ref_clock.label")
            {
              tcds::utils::ConfigurationInfoSpaceHandler const& cfgInfoSpace =
                getOwnerApplication().getConfigurationInfoSpaceHandler();
              tcds::definitions::PHASEMON_REF_CLK_SRC valueEnum = hw.getSelectedRefClk();
              std::string const labelName = refClkSrcToLabelName(valueEnum);
              std::string const newVal = cfgInfoSpace.getString(labelName);
              infoSpaceHandler->setString(name, newVal);
              updated = true;
            }
          else if (name == "meas.result")
            {
              std::string const hwName = item.hwName();
              tcds::definitions::PHASEMON_INPUT inputNumber = inputRegNameToNumber(hwName);
              double const newVal = hw.readPhaseMeasurement(inputNumber);
              infoSpaceHandler->setDouble(name, newVal);
              updated = true;
            }
          else if (name == "meas.status")
            {
              std::string const hwName = item.hwName();
              tcds::definitions::PHASEMON_INPUT inputNumber = inputRegNameToNumber(hwName);
              uint32_t const newVal = hw.getPhaseMeasurementStatus(inputNumber);
              infoSpaceHandler->setUInt32(name, newVal);
              updated = true;
            }
        }
      if (!updated)
        {
          updated = tcds::utils::HwInfoSpaceUpdaterBase::updateInfoSpaceItem(item, infoSpaceHandler);
        }
    }

  if (updated)
    {
      item.setValid();
    }
  else
    {
      item.setInvalid();
    }

  return updated;
}

tcds::phasemon::TCADevicePhaseMon const&
tcds::phasemon::PhaseMonInfoSpaceUpdater::getHw() const
{
  return dynamic_cast<tcds::phasemon::TCADevicePhaseMon const&>(tcds::utils::HwInfoSpaceUpdaterBase::getHw());
}
