#include "tcds/phasemon/SFPInfoSpaceHandler.h"

#include <cstddef>

#include "toolbox/string.h"

#include "tcds/hwlayertca/Definitions.h"
#include "tcds/hwutilstca/SFPInfoSpaceUpdater.h"
#include "tcds/hwlayertca/Utils.h"
#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/WebServer.h"
#include "tcds/utils/XDAQAppBase.h"

tcds::phasemon::SFPInfoSpaceHandler::SFPInfoSpaceHandler(tcds::utils::XDAQAppBase& xdaqApp,
                                                         tcds::hwutilstca::SFPInfoSpaceUpdater* updater) :
  tcds::hwutilstca::SFPInfoSpaceHandlerBase(xdaqApp, updater)
{
  // The SFPs on the L8/TCDS FMC 8SFP FMC.
  for (unsigned int source = tcds::definitions::SFP_PHASEMON_MIN;
       source <= tcds::definitions::SFP_PHASEMON_MAX;
       ++source)
    {
      tcds::definitions::SFP_PHASEMON const sourceEnum =
        static_cast<tcds::definitions::SFP_PHASEMON>(source);
      std::string const name = tcds::hwlayertca::sfpToRegName(sourceEnum);
      std::string const infoSpaceHandlerName = toolbox::toString("tcds-sfp-info-phasemon-%s",
                                                                 name.c_str());

      tcds::utils::InfoSpaceHandler* handler =
        new tcds::utils::InfoSpaceHandler(xdaqApp, infoSpaceHandlerName, updater);
      infoSpaceHandlers_[name] = handler;
      createSFPChannel(handler,
                       tcds::definitions::kSFP_TYPE_PHASEMON,
                       source,
                       name);
    }
}

tcds::phasemon::SFPInfoSpaceHandler::~SFPInfoSpaceHandler()
{
  // Cleanup of InfoSpaceHandlers happens in the MultiInfoSpaceHandler
  // base class.
}

void
tcds::phasemon::SFPInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  // The SFPs on the L8/TCDS FMC 8SFP FMC.
  for (unsigned int source = tcds::definitions::SFP_PHASEMON_MIN;
       source <= tcds::definitions::SFP_PHASEMON_MAX;
       ++source)
    {
      tcds::definitions::SFP_PHASEMON const sourceEnum =
        static_cast<tcds::definitions::SFP_PHASEMON>(source);
      std::string const name = tcds::hwlayertca::sfpToShortName(sourceEnum);
      std::string const itemSetName = toolbox::toString("itemset-sfp-status-%s",
                                                        name.c_str());
      registerSFPChannel(tcds::hwlayertca::sfpToRegName(sourceEnum), itemSetName, monitor);
    }
}

void
tcds::phasemon::SFPInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                   tcds::utils::Monitor& monitor,
                                                                   std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "SFPs" : forceTabName;
  size_t const numColumns = 4;

  webServer.registerTab(tabName, "SFP monitoring", numColumns);

  //----------

  // The SFPs on the L8/TCDS FMC 8SFP FMC.
  for (unsigned int source = tcds::definitions::SFP_PHASEMON_MIN;
       source <= tcds::definitions::SFP_PHASEMON_MAX;
       ++source)
    {
      tcds::definitions::SFP_PHASEMON const sourceEnum =
        static_cast<tcds::definitions::SFP_PHASEMON>(source);
      std::string const name = tcds::hwlayertca::sfpToShortName(sourceEnum);
      std::string const itemSetName = toolbox::toString("itemset-sfp-status-%s",
                                                        name.c_str());
      webServer.registerTable(tcds::hwlayertca::sfpToLongName(sourceEnum),
                              "",
                              monitor,
                              itemSetName,
                              tabName);
    }

  size_t numSFPs = (tcds::definitions::SFP_PHASEMON_MAX -
                    tcds::definitions::SFP_PHASEMON_MIN) + 1;
  size_t numLeft = numSFPs % numColumns;
  if (numLeft)
    {
      size_t const numSpacers = numColumns - numLeft;
      webServer.registerSpacer("Spacer",
                               "",
                               monitor,
                               "",
                               tabName,
                               numSpacers);
    }
}
