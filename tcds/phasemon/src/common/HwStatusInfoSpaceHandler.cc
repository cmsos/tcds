#include "tcds/phasemon/HwStatusInfoSpaceHandler.h"

#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/WebServer.h"

tcds::phasemon::HwStatusInfoSpaceHandler::HwStatusInfoSpaceHandler(xdaq::Application& xdaqApp,
                                                                   tcds::utils::InfoSpaceUpdater* updater) :
  InfoSpaceHandler(xdaqApp, "tcds-hw-status-phasemon", updater)
{
  // Hardware status.
  createBool("ttc_clock_stable",
             false,
             "true/false",
             tcds::utils::InfoSpaceItem::PROCESS);
  createBool("ttc_clock_up",
             false,
             "true/false",
             tcds::utils::InfoSpaceItem::PROCESS);
  createBool("meas_common.mmcm1_locked",
             false,
             "true/false");
  createBool("meas_common.mmcm2_locked",
             false,
             "true/false");
}

tcds::phasemon::HwStatusInfoSpaceHandler::~HwStatusInfoSpaceHandler()
{
}

void
tcds::phasemon::HwStatusInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  // Hardware status.
  std::string itemSetName = "itemset-hw-status";
  monitor.newItemSet(itemSetName);
  monitor.addItem(itemSetName,
                  "ttc_clock_stable",
                  "TTC clock stable",
                  this);
  monitor.addItem(itemSetName,
                  "ttc_clock_up",
                  "TTC clock PLL locked",
                  this);
  monitor.addItem(itemSetName,
                  "meas_common.mmcm1_locked",
                  "MMCM1 locked",
                  this);
  monitor.addItem(itemSetName,
                  "meas_common.mmcm2_locked",
                  "MMCM2 locked",
                  this);
}

void
tcds::phasemon::HwStatusInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                        tcds::utils::Monitor& monitor,
                                                                        std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "Hardware status" : forceTabName;

  webServer.registerTab(tabName,
                        "Hardware information",
                        3);
  webServer.registerTable("Hardware status",
                          "Hardware status info",
                          monitor,
                          "itemset-hw-status",
                          tabName);
}
