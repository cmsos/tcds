#include "tcds/phasemon/PhaseMon.h"

#include <stdint.h>
#include <string>
#include <vector>

#include "toolbox/string.h"
#include "xcept/Exception.h"
#include "xdaq/exception/ApplicationInstantiationFailed.h"

#include "tcds/exception/Exception.h"
#include "tcds/hwlayer/DeviceBase.h"
#include "tcds/hwutilstca/HwIDInfoSpaceHandlerTCA.h"
#include "tcds/hwutilstca/HwIDInfoSpaceUpdaterTCA.h"
#include "tcds/hwutilstca/SFPInfoSpaceUpdater.h"
#include "tcds/hwutilstca/Utils.h"
#include "tcds/phasemon/ConfigurationInfoSpaceHandler.h"
#include "tcds/phasemon/Definitions.h"
#include "tcds/phasemon/HwStatusInfoSpaceHandler.h"
#include "tcds/phasemon/HwStatusInfoSpaceUpdater.h"
#include "tcds/phasemon/PhaseMonInfoSpaceUpdater.h"
#include "tcds/phasemon/PhasesInfoSpaceHandler.h"
#include "tcds/phasemon/SFPInfoSpaceHandler.h"
#include "tcds/utils/LogMacros.h"

XDAQ_INSTANTIATOR_IMPL(tcds::phasemon::PhaseMon)

tcds::phasemon::PhaseMon::PhaseMon(xdaq::ApplicationStub* stub)
try
  :
  tcds::utils::XDAQAppWithFSMAutomatic(stub, std::unique_ptr<tcds::hwlayer::DeviceBase>(new tcds::phasemon::TCADevicePhaseMon()))
  {
    // Create the InfoSpace holding all configuration information.
    cfgInfoSpaceP_ =
      std::unique_ptr<tcds::phasemon::ConfigurationInfoSpaceHandler>(new tcds::phasemon::ConfigurationInfoSpaceHandler(*this));

    // Make sure the correct default hardware configuration file is
    // found.
    cfgInfoSpaceP_->setString("defaultHwConfigurationFilePath",
                              "${XDAQ_ROOT}/etc/tcds/phasemon/hw_cfg_default_phasemon.txt");
  }
catch (tcds::exception::Exception const& err)
  {
    std::string msgBase = "Something went wrong instantiating the PhaseMon application";
    std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.what());
    XCEPT_RAISE(xdaq::exception::ApplicationInstantiationFailed, msg.c_str());
  }

tcds::phasemon::PhaseMon::~PhaseMon()
{
}

tcds::phasemon::ConfigurationInfoSpaceHandler const&
tcds::phasemon::PhaseMon::getConfigurationInfoSpaceHandler() const
{
  return static_cast<tcds::phasemon::ConfigurationInfoSpaceHandler const&>(tcds::utils::XDAQAppWithFSMAutomatic::getConfigurationInfoSpaceHandler());
}

void
tcds::phasemon::PhaseMon::setupInfoSpaces()
{
  // Make sure the configuration settings are up-to-date.
  cfgInfoSpaceP_->readInfoSpace();

  // // We don't have an FSM, so no state either.
  // appStateInfoSpace_.setString("stateName", "n/a");
  appStateInfoSpace_.setString("hwLeaseOwnerId", "n/a");

  // Instantiate all hardware-dependent InfoSpaceHandlers and InfoSpaceUpdaters.
  hwIDInfoSpaceUpdaterP_ =
    std::unique_ptr<tcds::hwutilstca::HwIDInfoSpaceUpdaterTCA>(new tcds::hwutilstca::HwIDInfoSpaceUpdaterTCA(*this, getHw()));
  hwIDInfoSpaceP_ =
    std::unique_ptr<tcds::hwutilstca::HwIDInfoSpaceHandlerTCA>(new tcds::hwutilstca::HwIDInfoSpaceHandlerTCA(*this, hwIDInfoSpaceUpdaterP_.get()));
  hwStatusInfoSpaceUpdaterP_ =
    std::unique_ptr<tcds::phasemon::HwStatusInfoSpaceUpdater>(new tcds::phasemon::HwStatusInfoSpaceUpdater(*this, getHw()));
  hwStatusInfoSpaceP_ =
    std::unique_ptr<tcds::phasemon::HwStatusInfoSpaceHandler>(new tcds::phasemon::HwStatusInfoSpaceHandler(*this, hwStatusInfoSpaceUpdaterP_.get()));

  phasemonInfoSpaceUpdaterP_ = std::unique_ptr<tcds::phasemon::PhaseMonInfoSpaceUpdater>(new tcds::phasemon::PhaseMonInfoSpaceUpdater(*this, getHw()));

  phasesInfoSpaceP_ =
    std::unique_ptr<tcds::phasemon::PhasesInfoSpaceHandler>(new tcds::phasemon::PhasesInfoSpaceHandler(*this, phasemonInfoSpaceUpdaterP_.get()));
  sfpInfoSpaceUpdaterP_ =
    std::unique_ptr<tcds::hwutilstca::SFPInfoSpaceUpdater>(new tcds::hwutilstca::SFPInfoSpaceUpdater(*this, getHw()));
  sfpInfoSpaceP_ =
    std::unique_ptr<SFPInfoSpaceHandler>(new SFPInfoSpaceHandler(*this, sfpInfoSpaceUpdaterP_.get()));

  // NOTE: The PhaseMon type (i.e., 'CLK' or 'TTC') determines which
  // signals we should monitor.
  uint32_t const tmpType = cfgInfoSpaceP_->getUInt32("phasemonType");
  tcds::definitions::PHASEMON_TYPE const phasemonType =
    static_cast<tcds::definitions::PHASEMON_TYPE>(tmpType);
  std::vector<tcds::definitions::PHASEMON_INPUT> inputList;
  // Most signals exist in both PhaseMon versions.
  inputList.push_back(tcds::definitions::PHASEMON_INPUT_FCLKA);
  inputList.push_back(tcds::definitions::PHASEMON_INPUT_ECL_REFCLK);
  inputList.push_back(tcds::definitions::PHASEMON_INPUT_SFP_REFCLK);
  inputList.push_back(tcds::definitions::PHASEMON_INPUT_SFP1);
  inputList.push_back(tcds::definitions::PHASEMON_INPUT_SFP2);
  inputList.push_back(tcds::definitions::PHASEMON_INPUT_SFP3);
  inputList.push_back(tcds::definitions::PHASEMON_INPUT_SFP4);
  inputList.push_back(tcds::definitions::PHASEMON_INPUT_SFP5);
  inputList.push_back(tcds::definitions::PHASEMON_INPUT_SFP6);
  inputList.push_back(tcds::definitions::PHASEMON_INPUT_SFP7);
  // Some signals are only availably in the 'clocks' PhaseMon.
  if (phasemonType == tcds::definitions::PHASEMON_TYPE_CLK)
    {
      inputList.push_back(tcds::definitions::PHASEMON_INPUT_ECL_TTCORB);
      inputList.push_back(tcds::definitions::PHASEMON_INPUT_ECL_TR0);
      inputList.push_back(tcds::definitions::PHASEMON_INPUT_ECL_TR1);
    }

  // Register all InfoSpaceItems with the Monitor.
  cfgInfoSpaceP_->registerItemSets(monitor_, webServer_);
  appStateInfoSpace_.registerItemSets(monitor_, webServer_);
  hwIDInfoSpaceP_->registerItemSets(monitor_, webServer_);
  hwStatusInfoSpaceP_->registerItemSets(monitor_, webServer_);
  sfpInfoSpaceP_->registerItemSets(monitor_, webServer_);
  phasesInfoSpaceP_->registerItemSets(monitor_, webServer_);
}

void
tcds::phasemon::PhaseMon::enableActionImpl(toolbox::Event::Reference event)
{
  // We want to do what we always do.
  tcds::utils::XDAQAppWithFSMAutomatic::enableActionImpl(event);

  // But we also want to fire our InfoSpace(s) with measurements once
  // just to make sure we have a baseline recorded.
  phasesInfoSpaceP_->writeInfoSpace();
}

tcds::phasemon::TCADevicePhaseMon&
tcds::phasemon::PhaseMon::getHw() const
{
  return static_cast<tcds::phasemon::TCADevicePhaseMon&>(*hwP_.get());
}

void
tcds::phasemon::PhaseMon::hwConnectImpl()
{
  // Connect to the hardware.
  tcds::hwutilstca::tcaDeviceHwConnectImpl(*cfgInfoSpaceP_, getHw());
}

void
tcds::phasemon::PhaseMon::hwReleaseImpl()
{
  getHw().hwRelease();
}

// void
// tcds::phasemon::PhaseMon::hwCfgInitializeImpl()
// {
//   tcds::phasemon::TCADevicePhaseMon& hw = getHw();
// }

void
tcds::phasemon::PhaseMon::hwCfgFinalizeImpl()
{
  tcds::phasemon::TCADevicePhaseMon& hw = getHw();

  //----------

  // First: disable everything.
  hw.disablePhaseMeasurements();

  //----------

  // Then configure the reference clock source.
  uint32_t const tmpRefClkSrc = cfgInfoSpaceP_->getUInt32("refClkSrc");
  tcds::definitions::PHASEMON_REF_CLK_SRC const refClkSrc =
    static_cast<tcds::definitions::PHASEMON_REF_CLK_SRC>(tmpRefClkSrc);
  hw.selectRefClock(refClkSrc);

  //----------

  // Check for the presence of the reference clock (which is basically
  // the TTC clock). Without reference clock it is no use continuing
  // (and apart from that, some registers will not be accessible).
  if (!(hw.isTTCClockUp() && hw.isTTCClockStable()))
    {
      std::string const msg =
        "Could not configure the hardware: no TTC/reference clock present, or clock not stable.";
      ERROR(msg);
      XCEPT_RAISE(tcds::exception::TTCClockProblem, msg.c_str());
    }

  //----------

  // Check that both MMCMs used by the phase measurements are locked.
  if (!hw.isPhaseMonLocked())
    {
      std::string const msg =
        "Could not configure the hardware: PhaseMon MMCMs are not locked."
        " Possible TTC/reference clock problem.";
      ERROR(msg);
      XCEPT_RAISE(tcds::exception::TTCClockProblem, msg.c_str());
    }

  //----------

  // Configure the correct signal types for all SFP input signals,
  // depending on the PhaseMon type we're dealing with: 40 MHz clocks
  // for the 'CLK' PhaseMon, or TTC streams for the 'TTC' PhaseMon.
  uint32_t const tmpType = cfgInfoSpaceP_->getUInt32("phasemonType");
  tcds::definitions::PHASEMON_TYPE const phasemonType =
    static_cast<tcds::definitions::PHASEMON_TYPE>(tmpType);
  uint32_t signalType = tcds::definitions::PHASEMON_SIGNAL_TYPE_CLOCK;
  if (phasemonType == tcds::definitions::PHASEMON_TYPE_TTC)
    {
      signalType = tcds::definitions::PHASEMON_SIGNAL_TYPE_TTC;
    }
  for (int inputNum = tcds::definitions::PHASEMON_INPUT_SFP1;
       inputNum <= tcds::definitions::PHASEMON_INPUT_SFP7;
       ++inputNum)
    {
      std::string const regName = toolbox::toString("signal_type_config.meas_sfp%d",
                                                    inputNum);
      hw.writeRegister(regName, signalType);
    }

  //----------

  // Flag which inputs are connected and which ones are
  // not. Disconnected inputs are not included in the phase
  // measurements etc.
  // NOTE: Inputs with a label starting with 'not ' (e.g., 'not
  // applicable', 'not connected') are considered
  // disconnected. Otherwise they are considered connected.
  std::vector<tcds::definitions::PHASEMON_INPUT> connected;
  for (int inputNum = tcds::definitions::kPhaseInputMin;
       inputNum <= tcds::definitions::kPhaseInputMax;
       ++inputNum)
    {
      tcds::definitions::PHASEMON_INPUT const input =
        static_cast<tcds::definitions::PHASEMON_INPUT>(inputNum);
      if (input == tcds::definitions::PHASEMON_INPUT_FCLKA)
        {
          // Some things are of course always connected (to external
          // sources).
          connected.push_back(tcds::definitions::PHASEMON_INPUT_FCLKA);
        }
      else if (input == tcds::definitions::PHASEMON_INPUT_TTC_FMC)
        {
          // Some things are of course never connected (to external
          // sources).
        }
      else
        {
          if (getConfigurationInfoSpaceHandler().isInputEnabled(input))
            {
              connected.push_back(input);
            }
        }
    }

  // NOTE: If this is a PhaseMon-TTC, we also need to enable the TTC
  // FMC input, since this is the one used to measure the phases of
  // the encoded TTC streams coming into the SFPs.
  if (phasemonType == tcds::definitions::PHASEMON_TYPE_TTC)
    {
      connected.push_back(tcds::definitions::PHASEMON_INPUT_TTC_FMC);
    }

  //----------

  // Finally enable all necessary phase monitoring blocks.
  hw.enablePhaseMeasurements(connected);
}
