#include "tcds/phasemon/PhaseMonInfoSpaceHandler.h"

#include <stdint.h>
#include <string>

#include "toolbox/string.h"

#include "tcds/phasemon/ConfigurationInfoSpaceHandler.h"
#include "tcds/phasemon/Definitions.h"
#include "tcds/phasemon/PhaseMon.h"
#include "tcds/phasemon/Utils.h"
#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/Utils.h"
#include "tcds/utils/WebServer.h"

tcds::phasemon::PhaseMonInfoSpaceHandler::PhaseMonInfoSpaceHandler(tcds::phasemon::PhaseMon& xdaqApp,
                                                                   tcds::utils::InfoSpaceUpdater* updater,
                                                                   tcds::definitions::PHASEMON_INPUT const input) :
  InfoSpaceHandler(xdaqApp, "tcds-phasemon-data-" + inputNumberToRegName(input), updater),
  phaseMonInput_(input)
{
  // Keep track of the currently selected reference clock.
  // NOTE: The source of this value is a bit convoluted.
  createUInt32("ref_clock.src_id",
               tcds::definitions::PHASEMON_REF_CLK_SRC_UNKNOWN,
               "",
               tcds::utils::InfoSpaceItem::PROCESS);
  createString("ref_clock.label",
               "unknown",
               "",
               tcds::utils::InfoSpaceItem::PROCESS);

  //----------

  // And then the measurement.

  // The signal source (which is a fixed value).
  createUInt32("meas.src_id",
               phaseMonInput_,
               "",
               tcds::utils::InfoSpaceItem::NOUPDATE,
               true);

  // The signal label (which is a fixed value), basically just copied
  // from the ConfigurationInfoSpaceHandler.
  // NOTE: This of course assumes that the configuration is complete
  // and up-to-date.
  tcds::phasemon::ConfigurationInfoSpaceHandler const& cfgInfoSpace =
    PhaseMonInfoSpaceHandler::getOwnerApplication().getConfigurationInfoSpaceHandler();
  std::string const labelName = "meas.label";
  // NOTE: FCLKA has no (customisable) label.
  std::string labelVal = "FCLKA";
  if (phaseMonInput_ != tcds::definitions::PHASEMON_INPUT_FCLKA)
    {
      labelVal = cfgInfoSpace.getString(inputNumberToLabelName(phaseMonInput_));
    }
  createString(labelName,
               labelVal,
               "",
               tcds::utils::InfoSpaceItem::NOUPDATE,
               true);

  // NOTE: This is intended to be used with the PhaseMon firmware
  // auto-scanner feature, not with the 'plain', manual measurements.

  // The measurement result.
  createDouble("meas.result",
               0,
               "*1.e9:%.3f",
               tcds::utils::InfoSpaceItem::PROCESS,
               false,
               inputNumberToRegName(phaseMonInput_));

  // The measurement status.
  createUInt32("meas.status",
               tcds::definitions::PHASEMON_INPUT_STATUS_DISABLED,
               "",
               tcds::utils::InfoSpaceItem::PROCESS,
               false,
               inputNumberToRegName(phaseMonInput_));
}

tcds::phasemon::PhaseMonInfoSpaceHandler::~PhaseMonInfoSpaceHandler()
{
}

tcds::phasemon::PhaseMon&
tcds::phasemon::PhaseMonInfoSpaceHandler::getOwnerApplication() const
{
  return static_cast<tcds::phasemon::PhaseMon&>(tcds::utils::InfoSpaceHandler::getOwnerApplication());
}

std::string
tcds::phasemon::PhaseMonInfoSpaceHandler::formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const
{
  // Some specialized formatting rules for enums.
  std::string const name = item->name();
  std::string res = tcds::utils::escapeAsJSONString(tcds::utils::InfoSpaceHandler::kInvalidItemString);
  if (item->isValid())
    {
      if (name == "ref_clock.src_id")
        {
          uint32_t const value = getUInt32(name);
          tcds::definitions::PHASEMON_REF_CLK_SRC valueEnum =
            static_cast<tcds::definitions::PHASEMON_REF_CLK_SRC>(value);
          res = tcds::utils::escapeAsJSONString(refClkSrcToString(valueEnum));
        }
      else if (name == "meas.src_id")
        {
          uint32_t const value = getUInt32(name);
          tcds::definitions::PHASEMON_INPUT valueEnum =
            static_cast<tcds::definitions::PHASEMON_INPUT>(value);
          res = tcds::utils::escapeAsJSONString(inputNumberToName(valueEnum));
        }
      else if (name == "meas.status")
        {
          uint32_t const value = getUInt32(name);
          tcds::definitions::PHASEMON_INPUT_STATUS valueEnum =
            static_cast<tcds::definitions::PHASEMON_INPUT_STATUS>(value);
          res = tcds::utils::escapeAsJSONString(inputStatusToString(valueEnum));
        }
      else
        {
          // For all non-enums simply call the generic formatter.
          res = tcds::utils::InfoSpaceHandler::formatItem(item);
        }
    }

  return res;
}

void
tcds::phasemon::PhaseMonInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  std::string const itemSetName = "itemset-phasemon-results-" + inputNumberToRegName(phaseMonInput_);
  monitor.newItemSet(itemSetName);

  // The reference clock information.
  monitor.addItem(itemSetName,
                  "ref_clock.src_id",
                  "Reference clock source",
                  this);
  monitor.addItem(itemSetName,
                  "ref_clock.label",
                  "Reference clock label",
                  this);

  //----------

  // And then the measurement.

  // The signal source.
  std::string name = "meas.src_id";
  std::string desc = "Signal source";
  monitor.addItem(itemSetName, name, desc, this);

  // The signal label.
  name = "meas.label";
  desc = "Signal label";
  monitor.addItem(itemSetName, name, desc, this);

  // The measurement result.
  name = "meas.result";
  desc = "Phase (ns)";
  monitor.addItem(itemSetName, name, desc, this);

  // The measurement status.
  name = "meas.status";
  std::string const help = "Shows whether the channel is enabled"
    " and correctly taking phase measurements. A 'bad' status indicates"
    " a timeout or a measurement problem.";
  desc = "Acquisition status";
  monitor.addItem(itemSetName,
                  name,
                  desc,
                  this,
                  help);
}

void
tcds::phasemon::PhaseMonInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                        tcds::utils::Monitor& monitor,
                                                                        std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "Phases" : forceTabName;

  webServer.registerTab(tabName,
                        "Phase of input signal w.r.t. the selected reference clock",
                        4);
  registerItemSetsWithWebServerHelper(webServer,
                                      monitor,
                                      tabName,
                                      phaseMonInput_);
}

void
tcds::phasemon::PhaseMonInfoSpaceHandler::registerItemSetsWithWebServerHelper(tcds::utils::WebServer& webServer,
                                                                              tcds::utils::Monitor& monitor,
                                                                              std::string const& tabName,
                                                                              tcds::definitions::PHASEMON_INPUT const input,
                                                                              size_t const colSpan)
{
  std::string const itemSetName =
    toolbox::toString("itemset-phasemon-results-%s",
                      inputNumberToRegName(input).c_str());

  std::string tableLabel = inputLabel(input);
  if (input != tcds::definitions::PHASEMON_INPUT_FCLKA)
    {
      std::string const tmp = inputNumberToName(input);
      if (tmp != tableLabel)
        {
          tableLabel += (" - " + tmp);
        }
    }
  webServer.registerTable(tableLabel,
                          "",
                          monitor,
                          itemSetName,
                          tabName,
                          colSpan);
}

bool
tcds::phasemon::PhaseMonInfoSpaceHandler::inputMatchesRefClkSrc(tcds::definitions::PHASEMON_INPUT const input,
                                                                tcds::definitions::PHASEMON_REF_CLK_SRC const refClkSrc) const
{
  bool const matchFCLKA = ((input == tcds::definitions::PHASEMON_INPUT_FCLKA) &&
                           (refClkSrc == tcds::definitions::PHASEMON_REF_CLK_SRC_FCLKA));
  bool const matchSFP = ((input == tcds::definitions::PHASEMON_INPUT_SFP_REFCLK) &&
                         (refClkSrc == tcds::definitions::PHASEMON_REF_CLK_SRC_SFP));
  bool const matchLemo = ((input == tcds::definitions::PHASEMON_INPUT_ECL_REFCLK) &&
                          (refClkSrc == tcds::definitions::PHASEMON_REF_CLK_SRC_LEMO));
  bool const res = matchFCLKA || matchSFP || matchLemo;
  return res;
}

std::string
tcds::phasemon::PhaseMonInfoSpaceHandler::inputLabel(tcds::definitions::PHASEMON_INPUT const input) const
{
  std::string label = inputNumberToName(input);
  if (input != tcds::definitions::PHASEMON_INPUT_FCLKA)
    {
      tcds::phasemon::ConfigurationInfoSpaceHandler const& cfgInfoSpace =
        getOwnerApplication().getConfigurationInfoSpaceHandler();
      std::string const labelName = inputNumberToLabelName(input);
      std::string const tmpLabel = cfgInfoSpace.getString(labelName);
      if (cfgInfoSpace.isInputEnabled(input) &&
          (tmpLabel.size() > 0))
        {
          label = tmpLabel;
        }
    }

  return label;
}
