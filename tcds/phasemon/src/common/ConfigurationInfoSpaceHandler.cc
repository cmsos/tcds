#include "tcds/phasemon/ConfigurationInfoSpaceHandler.h"

#include <stdint.h>

#include "toolbox/string.h"

#include "tcds/hwutilstca/SFPMonitoringConfigHelpers.h"
#include "tcds/phasemon/Definitions.h"
#include "tcds/phasemon/Utils.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/Utils.h"
#include "tcds/utils/WebServer.h"

tcds::phasemon::ConfigurationInfoSpaceHandler::ConfigurationInfoSpaceHandler(xdaq::Application& xdaqApp) :
  tcds::hwutilstca::ConfigurationInfoSpaceHandlerTCA(xdaqApp,
                                                     "tcds-application-config-phasemon")
{
  // Input labels for the various reference clock sources.
  createString("refClkLabelLemo",
               "",
               "",
               tcds::utils::InfoSpaceItem::NOUPDATE,
               true);
  createString("refClkLabelSFP",
               "",
               "",
               tcds::utils::InfoSpaceItem::NOUPDATE,
               true);

  // Input labels for the signals connected to the L8 SFPs.
  for (int inputNum = tcds::definitions::PHASEMON_INPUT_SFP1;
       inputNum <= tcds::definitions::PHASEMON_INPUT_SFP7;
       ++inputNum)
    {
      createString(toolbox::toString("inputLabelSFP%d", inputNum),
                   "",
                   "",
                   tcds::utils::InfoSpaceItem::NOUPDATE,
                   true);
    }

  // Input label for FCLKA (i.e., the backplane TTC clock).
  createString("inputLabelFCLKA",
               "Backplane TTC clock",
               "",
               tcds::utils::InfoSpaceItem::NOUPDATE,
               true);

  // Input labels for the signals connected to the L12 Lemo inputs.
  createString("inputLabelLemoTTCOrb",
               "",
               "",
               tcds::utils::InfoSpaceItem::NOUPDATE,
               true);
  createString("inputLabelLemoTr0",
               "",
               "",
               tcds::utils::InfoSpaceItem::NOUPDATE,
               true);
  createString("inputLabelLemoTr1",
               "",
               "",
               tcds::utils::InfoSpaceItem::NOUPDATE,
               true);

  //----------

  // Hardware-wise, the first thing to know is whether this PhaseMon
  // is decked out with hardware for phase measurements of 40 MHz
  // clocks or of TTC streams.
  createUInt32("phasemonType",
               tcds::definitions::PHASEMON_TYPE_TTC,
               "",
               tcds::utils::InfoSpaceItem::NOUPDATE,
               true);

  //----------

  // The choice of reference clock.
  createUInt32("refClkSrc",
               tcds::definitions::PHASEMON_REF_CLK_SRC_FCLKA,
               "",
               tcds::utils::InfoSpaceItem::NOUPDATE,
               true);

  //----------

  // SFP monitoring parameters.
  tcds::hwutilstca::SFPMonitoringConfigCreateItems(this);
}

std::string
tcds::phasemon::ConfigurationInfoSpaceHandler::formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const
{
  // Some specialized formatting rules for enums.
  std::string const name = item->name();
  std::string res = tcds::utils::escapeAsJSONString(tcds::utils::InfoSpaceHandler::kInvalidItemString);
  if (name == "phasemonType")
    {
      uint32_t const value = getUInt32(name);
      tcds::definitions::PHASEMON_TYPE valueEnum =
        static_cast<tcds::definitions::PHASEMON_TYPE>(value);
      res = tcds::utils::escapeAsJSONString(phasemonTypeToString(valueEnum));
    }
  else if (name == "refClkSrc")
    {
      uint32_t const value = getUInt32(name);
      tcds::definitions::PHASEMON_REF_CLK_SRC valueEnum =
        static_cast<tcds::definitions::PHASEMON_REF_CLK_SRC>(value);
      res = tcds::utils::escapeAsJSONString(refClkSrcToString(valueEnum));
    }
  else
    {
      // For all non-enums simply call the generic formatter.
      res = tcds::hwutilstca::ConfigurationInfoSpaceHandlerTCA::formatItem(item);
    }

  return res;
}

void
tcds::phasemon::ConfigurationInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  // Application configuration items.
  std::string itemSetName = "Application configuration";
  monitor.newItemSet(itemSetName);
  monitor.addItem(itemSetName,
                  "monitoringInterval",
                  "Monitoring update interval",
                  this);
  monitor.addItem(itemSetName,
                  "ipbusConnectionsFile",
                  "IPbus connections file",
                  this);
  monitor.addItem(itemSetName,
                  "ipbusConnection",
                  "IPbus connection name",
                  this);

  //----------

  itemSetName = "itemset-hw-cfg-phasemon";
  monitor.newItemSet(itemSetName);
  monitor.addItem(itemSetName,
                  "phasemonType",
                  "PhaseMon hardware type",
                  this,
                  "Two types of PhaseMon hardware exist: one to measure phases of TTC streams and one to measure phases of 40 MHz clocks.");
  monitor.addItem(itemSetName,
                  "refClkSrc",
                  "Reference clock source",
                  this,
                  "Each PhaseMon type can either use FCLKA as reference clock, or an external reference clock. The external reference clock can either come from the TTC-clock Lemo or from the SFP8 RX.");

  //----------

  // SFP monitoring parameters.
  tcds::hwutilstca::SFPMonitoringRegisterItemsWithMonitor(this, monitor);
}

void
tcds::phasemon::ConfigurationInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                             tcds::utils::Monitor& monitor,
                                                                             std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "Configuration" : forceTabName;

  webServer.registerTab(tabName,
                        "Configuration parameters",
                        3);
  webServer.registerTable("Application configuration",
                          "Application configuration parameters",
                          monitor,
                          "Application configuration",
                          tabName,
                          3);
  webServer.registerTable("Hardware configuration",
                          "Hardware configuration parameters",
                          monitor,
                          "itemset-hw-cfg-phasemon",
                          tabName,
                          3);
}

bool
tcds::phasemon::ConfigurationInfoSpaceHandler::isInputEnabled(tcds::definitions::PHASEMON_INPUT const input) const
{
  std::string const labelName = inputNumberToLabelName(input);
  std::string const label = toolbox::tolower(getString(labelName));
  bool const res = !toolbox::startsWith(label, "not ");
  return res;
}
