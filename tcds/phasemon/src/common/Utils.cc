#include "tcds/phasemon/Utils.h"

#include "toolbox/string.h"
#include "xcept/Exception.h"

#include "tcds/exception/Exception.h"

std::map<tcds::definitions::PHASEMON_TYPE, std::string>
tcds::phasemon::phasemonTypeMap()
{
  std::map<tcds::definitions::PHASEMON_TYPE, std::string> mapping;

  mapping[tcds::definitions::PHASEMON_TYPE_CLK] = "PhaseMon for 40 MHz clocks";
  mapping[tcds::definitions::PHASEMON_TYPE_TTC] = "PhaseMon for TTC streams";

  return mapping;
}

std::string
tcds::phasemon::phasemonTypeToString(tcds::definitions::PHASEMON_TYPE const type)
{
  std::map<tcds::definitions::PHASEMON_TYPE, std::string> mapping = phasemonTypeMap();

  std::map<tcds::definitions::PHASEMON_TYPE, std::string>::const_iterator i =
    mapping.find(type);
  if (i == mapping.end())
    {
      XCEPT_RAISE(tcds::exception::ValueError,
                  toolbox::toString("'%d' is not a valid PhaseMon hardware type.",
                                    type));
    }

  std::string const res = i->second;
  return res;
}

std::map<tcds::definitions::PHASEMON_REF_CLK_SRC, std::string>
tcds::phasemon::refClkSrcMap()
{
  std::map<tcds::definitions::PHASEMON_REF_CLK_SRC, std::string> mapping;

  mapping[tcds::definitions::PHASEMON_REF_CLK_SRC_UNKNOWN] = "unknown";
  mapping[tcds::definitions::PHASEMON_REF_CLK_SRC_FCLKA] = "FCLKA";
  mapping[tcds::definitions::PHASEMON_REF_CLK_SRC_SFP] = "SFP8 RX";
  mapping[tcds::definitions::PHASEMON_REF_CLK_SRC_LEMO] = "Lemo 'TTC-clock'";

  return mapping;
}

std::string
tcds::phasemon::refClkSrcToString(tcds::definitions::PHASEMON_REF_CLK_SRC const src)
{
  std::map<tcds::definitions::PHASEMON_REF_CLK_SRC, std::string> mapping = refClkSrcMap();

  std::map<tcds::definitions::PHASEMON_REF_CLK_SRC, std::string>::const_iterator i =
    mapping.find(src);
  if (i == mapping.end())
    {
      XCEPT_RAISE(tcds::exception::ValueError,
                  toolbox::toString("'%d' is not a valid PhaseMon reference clock type.",
                                    src));
    }

  std::string const res = i->second;
  return res;
}

std::map<tcds::definitions::PHASEMON_INPUT, std::string>
tcds::phasemon::inputRegNameMap()
{
  std::map<tcds::definitions::PHASEMON_INPUT, std::string> mapping;

  mapping[tcds::definitions::PHASEMON_INPUT_SFP1] = "sfp1";
  mapping[tcds::definitions::PHASEMON_INPUT_SFP2] = "sfp2";
  mapping[tcds::definitions::PHASEMON_INPUT_SFP3] = "sfp3";
  mapping[tcds::definitions::PHASEMON_INPUT_SFP4] = "sfp4";
  mapping[tcds::definitions::PHASEMON_INPUT_SFP5] = "sfp5";
  mapping[tcds::definitions::PHASEMON_INPUT_SFP6] = "sfp6";
  mapping[tcds::definitions::PHASEMON_INPUT_SFP7] = "sfp7";
  mapping[tcds::definitions::PHASEMON_INPUT_FCLKA] = "fclka";
  mapping[tcds::definitions::PHASEMON_INPUT_TTC_FMC] = "ttc_fmc";
  mapping[tcds::definitions::PHASEMON_INPUT_ECL_TTCORB] = "ecl_ttcorb";
  mapping[tcds::definitions::PHASEMON_INPUT_ECL_TR0] = "ecl_tr0";
  mapping[tcds::definitions::PHASEMON_INPUT_ECL_TR1] = "ecl_tr1";
  mapping[tcds::definitions::PHASEMON_INPUT_ECL_REFCLK] = "ecl_refclk";
  mapping[tcds::definitions::PHASEMON_INPUT_SFP_REFCLK] = "sfp_refclk";

  return mapping;
}

std::string
tcds::phasemon::inputNumberToRegName(tcds::definitions::PHASEMON_INPUT const inputNum)
{
  std::map<tcds::definitions::PHASEMON_INPUT, std::string> mapping = inputRegNameMap();

  std::map<tcds::definitions::PHASEMON_INPUT, std::string>::const_iterator i =
    mapping.find(inputNum);
  if (i == mapping.end())
    {
      XCEPT_RAISE(tcds::exception::ValueError,
                  toolbox::toString("'%d' is not a valid PhaseMon input number.",
                                    inputNum));
    }

  std::string const res = i->second;
  return res;
}

std::map<tcds::definitions::PHASEMON_INPUT, std::string>
tcds::phasemon::inputNameMap()
{
  std::map<tcds::definitions::PHASEMON_INPUT, std::string> mapping;

  mapping[tcds::definitions::PHASEMON_INPUT_SFP1] = "SFP 1";
  mapping[tcds::definitions::PHASEMON_INPUT_SFP2] = "SFP 2";
  mapping[tcds::definitions::PHASEMON_INPUT_SFP3] = "SFP 3";
  mapping[tcds::definitions::PHASEMON_INPUT_SFP4] = "SFP 4";
  mapping[tcds::definitions::PHASEMON_INPUT_SFP5] = "SFP 5";
  mapping[tcds::definitions::PHASEMON_INPUT_SFP6] = "SFP 6";
  mapping[tcds::definitions::PHASEMON_INPUT_SFP7] = "SFP 7";
  mapping[tcds::definitions::PHASEMON_INPUT_FCLKA] = "FCLKA";
  mapping[tcds::definitions::PHASEMON_INPUT_TTC_FMC] = "TTC FMC";
  mapping[tcds::definitions::PHASEMON_INPUT_ECL_TTCORB] = "Lemo 'TTC orbit'";
  mapping[tcds::definitions::PHASEMON_INPUT_ECL_TR0] = "Lemo 'tr0'";
  mapping[tcds::definitions::PHASEMON_INPUT_ECL_TR1] = "Lemo 'tr1'";
  mapping[tcds::definitions::PHASEMON_INPUT_ECL_REFCLK] = "Lemo ref. clk.";
  mapping[tcds::definitions::PHASEMON_INPUT_SFP_REFCLK] = "SFP ref. clk.";

  return mapping;
}

std::string
tcds::phasemon::inputNumberToName(tcds::definitions::PHASEMON_INPUT const inputNum)
{
  std::map<tcds::definitions::PHASEMON_INPUT, std::string> mapping = inputNameMap();

  std::map<tcds::definitions::PHASEMON_INPUT, std::string>::const_iterator i =
    mapping.find(inputNum);
  if (i == mapping.end())
    {
      XCEPT_RAISE(tcds::exception::ValueError,
                  toolbox::toString("'%d' is not a valid PhaseMon input number.",
                                    inputNum));
    }

  std::string const res = i->second;
  return res;
}

tcds::definitions::PHASEMON_INPUT
tcds::phasemon::inputRegNameToNumber(std::string const& regName)
{
  std::map<tcds::definitions::PHASEMON_INPUT, std::string> mapping = inputRegNameMap();
  std::map<tcds::definitions::PHASEMON_INPUT, std::string>::const_iterator res = mapping.end();
  std::map<tcds::definitions::PHASEMON_INPUT, std::string>::const_iterator i;
  for (i = mapping.begin(); i != mapping.end(); ++i)
    {
      if (i->second == regName)
        {
          res = i;
          break;
        }
    }
  if (res == mapping.end())
    {
      XCEPT_RAISE(tcds::exception::ValueError,
                  toolbox::toString("'%s' is not a valid PhaseMon input register name.",
                                    regName.c_str()));
    }
  return res->first;
}

std::map<tcds::definitions::PHASEMON_INPUT, std::string>
tcds::phasemon::inputLabelNameMap()
{
  std::map<tcds::definitions::PHASEMON_INPUT, std::string> mapping;

  mapping[tcds::definitions::PHASEMON_INPUT_SFP1] = "inputLabelSFP1";
  mapping[tcds::definitions::PHASEMON_INPUT_SFP2] = "inputLabelSFP2";
  mapping[tcds::definitions::PHASEMON_INPUT_SFP3] = "inputLabelSFP3";
  mapping[tcds::definitions::PHASEMON_INPUT_SFP4] = "inputLabelSFP4";
  mapping[tcds::definitions::PHASEMON_INPUT_SFP5] = "inputLabelSFP5";
  mapping[tcds::definitions::PHASEMON_INPUT_SFP6] = "inputLabelSFP6";
  mapping[tcds::definitions::PHASEMON_INPUT_SFP7] = "inputLabelSFP7";
  mapping[tcds::definitions::PHASEMON_INPUT_FCLKA] = "inputLabelFCLKA";
  mapping[tcds::definitions::PHASEMON_INPUT_ECL_TTCORB] = "inputLabelLemoTTCOrb";
  mapping[tcds::definitions::PHASEMON_INPUT_ECL_TR0] = "inputLabelLemoTr0";
  mapping[tcds::definitions::PHASEMON_INPUT_ECL_TR1] = "inputLabelLemoTr1";
  mapping[tcds::definitions::PHASEMON_INPUT_ECL_REFCLK] = "refClkLabelLemo";
  mapping[tcds::definitions::PHASEMON_INPUT_SFP_REFCLK] = "refClkLabelSFP";

  return mapping;
}

std::string
tcds::phasemon::inputNumberToLabelName(tcds::definitions::PHASEMON_INPUT const inputNum)
{
  std::map<tcds::definitions::PHASEMON_INPUT, std::string> mapping = inputLabelNameMap();

  std::map<tcds::definitions::PHASEMON_INPUT, std::string>::const_iterator i =
    mapping.find(inputNum);
  if (i == mapping.end())
    {
      XCEPT_RAISE(tcds::exception::ValueError,
                  toolbox::toString("'%d' is not a valid (labeled!) PhaseMon input number.",
                                    inputNum));
    }

  std::string const res = i->second;
  return res;
}

std::string
tcds::phasemon::refClkSrcToLabelName(tcds::definitions::PHASEMON_REF_CLK_SRC const input)
{
  std::map<tcds::definitions::PHASEMON_INPUT, std::string> mapping = inputLabelNameMap();
  std::string res = "UNKNOWN_PHASEMON_REF_CLK_SRC";
  if (input == tcds::definitions::PHASEMON_REF_CLK_SRC_LEMO)
    {
      res = mapping[tcds::definitions::PHASEMON_INPUT_ECL_REFCLK];
    }
  else if (input == tcds::definitions::PHASEMON_REF_CLK_SRC_SFP)
    {
      res = mapping[tcds::definitions::PHASEMON_INPUT_SFP_REFCLK];
    }
  else if (input == tcds::definitions::PHASEMON_REF_CLK_SRC_FCLKA)
    {
      res = mapping[tcds::definitions::PHASEMON_INPUT_FCLKA];
    }
  return res;
}

// std::map<tcds::definitions::PHASEMON_SIGNAL_TYPE, std::string>
// tcds::phasemon::signalTypeMap()
// {
//   std::map<tcds::definitions::PHASEMON_SIGNAL_TYPE, std::string> mapping;

//   mapping[tcds::definitions::PHASEMON_SIGNAL_TYPE_CLOCK] = "40 MHz clock";
//   mapping[tcds::definitions::PHASEMON_SIGNAL_TYPE_TTC] = "TTC stream";

//   return mapping;
// }

// std::string
// tcds::phasemon::signalTypeToString(tcds::definitions::PHASEMON_SIGNAL_TYPE const type)
// {
//   std::map<tcds::definitions::PHASEMON_SIGNAL_TYPE, std::string> mapping = signalTypeMap();

//   std::map<tcds::definitions::PHASEMON_SIGNAL_TYPE, std::string>::const_iterator i =
//     mapping.find(type);
//   if (i == mapping.end())
//     {
//       XCEPT_RAISE(tcds::exception::ValueError,
//                   toolbox::toString("'%d' is not a valid PhaseMon input signal type.",
//                                     type));
//     }

//   std::string const res = i->second;
//   return res;
// }

std::map<tcds::definitions::PHASEMON_INPUT_STATUS, std::string>
tcds::phasemon::inputStatusMap()
{
  std::map<tcds::definitions::PHASEMON_INPUT_STATUS, std::string> mapping;

  mapping[tcds::definitions::PHASEMON_INPUT_STATUS_DISABLED] = "disabled";
  mapping[tcds::definitions::PHASEMON_INPUT_STATUS_GOOD] = "good";
  mapping[tcds::definitions::PHASEMON_INPUT_STATUS_BAD] = "bad";

  return mapping;
}

std::string
tcds::phasemon::inputStatusToString(tcds::definitions::PHASEMON_INPUT_STATUS const status)
{
  std::map<tcds::definitions::PHASEMON_INPUT_STATUS, std::string> mapping = inputStatusMap();

  std::map<tcds::definitions::PHASEMON_INPUT_STATUS, std::string>::const_iterator i =
    mapping.find(status);
  if (i == mapping.end())
    {
      XCEPT_RAISE(tcds::exception::ValueError,
                  toolbox::toString("'%d' is not a valid PhaseMon input status type.",
                                    status));
    }

  std::string const res = i->second;
  return res;
}
