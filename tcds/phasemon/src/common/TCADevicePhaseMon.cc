#include "tcds/phasemon/TCADevicePhaseMon.h"

#include <algorithm>
#include <memory>
#include <stdint.h>

#include "toolbox/string.h"
#include "xcept/Exception.h"

#include "tcds/exception/Exception.h"
#include "tcds/hwlayertca/BootstrapHelper.h"
#include "tcds/hwlayertca/Definitions.h"
#include "tcds/hwlayertca/TCACarrierBase.h"
#include "tcds/hwlayertca/TCACarrierFC7.h"
#include "tcds/phasemon/Definitions.h"
#include "tcds/phasemon/Utils.h"

tcds::phasemon::TCADevicePhaseMon::TCADevicePhaseMon() :
  TCADeviceBase(std::unique_ptr<tcds::hwlayertca::TCACarrierBase>(new tcds::hwlayertca::TCACarrierFC7(hwDevice_)))
{
}

tcds::phasemon::TCADevicePhaseMon::~TCADevicePhaseMon()
{
}

std::string
tcds::phasemon::TCADevicePhaseMon::regNamePrefixImpl() const
{
  // All the PhaseMon registers start with 'phasemon.'.
  return "phasemon.";
}

bool
tcds::phasemon::TCADevicePhaseMon::bootstrapDoneImpl() const
{
  tcds::hwlayertca::BootstrapHelper h(*this);
  return h.bootstrapDone();
}

void
tcds::phasemon::TCADevicePhaseMon::runBootstrapImpl() const
{
  tcds::hwlayertca::BootstrapHelper h(*this);
  return h.runBootstrap();
}

void
tcds::phasemon::TCADevicePhaseMon::selectRefClock(tcds::definitions::PHASEMON_REF_CLK_SRC const refClkSrc) const
{
  bool refClkFromFabric;
  bool externalRefClkFromLemo;

  switch (refClkSrc)
    {
    case tcds::definitions::PHASEMON_REF_CLK_SRC_FCLKA:
      refClkFromFabric = true;
      externalRefClkFromLemo = false;
      break;
    case tcds::definitions::PHASEMON_REF_CLK_SRC_SFP:
      refClkFromFabric = false;
      externalRefClkFromLemo = false;
      break;
    case tcds::definitions::PHASEMON_REF_CLK_SRC_LEMO:
      refClkFromFabric = false;
      externalRefClkFromLemo = true;
      break;
    default:
      std::string const msg =
        toolbox::toString("'%d' is not a valid reference clock type.", refClkSrc);
        XCEPT_RAISE(tcds::exception::ValueError, msg.c_str());
      break;
    }

  selectRefClock(refClkFromFabric, externalRefClkFromLemo);
}

void
tcds::phasemon::TCADevicePhaseMon::selectRefClock(bool const refClkFromFabric,
                                                  bool const externalRefClkFromLemo) const
{
  writeRegister("refclk_select.master", !refClkFromFabric);
  writeRegister("refclk_select.external", externalRefClkFromLemo);

  // NOTE: The reset of the TTC PLL will lead to a reset of both MMCMs
  // used for the phase measurements as well.
  resetPLL();
}

tcds::definitions::PHASEMON_REF_CLK_SRC
tcds::phasemon::TCADevicePhaseMon::getSelectedRefClk() const
{
  tcds::definitions::PHASEMON_REF_CLK_SRC res =
    tcds::definitions::PHASEMON_REF_CLK_SRC_UNKNOWN;
  bool const isInternal = (readRegister("refclk_select.master") == 0x0);
  if (isInternal)
    {
      res = tcds::definitions::PHASEMON_REF_CLK_SRC_FCLKA;
    }
  else
    {
      bool const isECL = (readRegister("refclk_select.external") == 0x1);
      if (isECL)
        {
          res = tcds::definitions::PHASEMON_REF_CLK_SRC_LEMO;
        }
      else
        {
          res = tcds::definitions::PHASEMON_REF_CLK_SRC_SFP;
        }
    }
  return res;
}

bool
tcds::phasemon::TCADevicePhaseMon::isPhaseMonLocked() const
{
  // Check that both MMCMs used by the phase measurements are locked.
  uint32_t const tmp1 = readRegister("meas_common.mmcm1_locked");
  uint32_t const tmp2 = readRegister("meas_common.mmcm2_locked");
  bool const isMMCM1Locked = (tmp1 == 0x1);
  bool const isMMCM2Locked = (tmp2 == 0x1);
  return (isMMCM1Locked && isMMCM2Locked);
}

void
tcds::phasemon::TCADevicePhaseMon::enablePhaseMeasurements(std::vector<tcds::definitions::PHASEMON_INPUT> connected) const
{
  // NOTE: This method sets up the hardware to use the auto-scanner
  // built into the firmware (which loops over all enabled inputs and
  // measures the phase between the input signal and the selected
  // reference clock for each input). This avoids the software having
  // to do signal switching at the cost of slightly trickier
  // monitoring.

  // Configure the measurement duration.
  // NOTE: Don't put this too high. The monitoring will expect new
  // numbers approximately once per second.
  writeRegister("meas_common.measurement_duration",
                tcds::definitions::kWRMeasDuration);

  // Enable all inputs that are specified as connected.
  for (int inputNum = tcds::definitions::kPhaseInputMin;
        inputNum <= tcds::definitions::kPhaseInputMax;
       ++inputNum)
    {
      tcds::definitions::PHASEMON_INPUT const input =
        static_cast<tcds::definitions::PHASEMON_INPUT>(inputNum);
      std::string const regName =
        toolbox::toString("meas_%s.disconnected", inputNumberToRegName(input).c_str());
      uint32_t newVal = 0x1;
      if (std::find(connected.begin(), connected.end(), input) != connected.end())
        {
          newVal = 0x0;
        }
      writeRegister(regName, newVal);
    }

  // Enable the auto-scanner. (It will automatically start.)
  writeRegister("autoscan.enable", 0x1);
}

void
tcds::phasemon::TCADevicePhaseMon::disablePhaseMeasurements() const
{
  // Disable the auto-scanner.
  writeRegister("autoscan.enable", 0x0);

  // Disable all inputs.
  for (int inputNum = tcds::definitions::kPhaseInputMin;
       inputNum <= tcds::definitions::kPhaseInputMax;
       ++inputNum)
    {
      tcds::definitions::PHASEMON_INPUT const input =
        static_cast<tcds::definitions::PHASEMON_INPUT>(inputNum);
      std::string const regName =
        toolbox::toString("meas_%s.disconnected", inputNumberToRegName(input).c_str());
      writeRegister(regName, 0x1);
    }
}

double
tcds::phasemon::TCADevicePhaseMon::readPhaseMeasurement(tcds::definitions::PHASEMON_INPUT const inputNum) const
{
  // NOTE: The enablePhaseMeasurements() method sets up the hardware
  // to use the auto-scanner measurements. So these are the ones we
  // need to read back here.

  std::string const regName =
    toolbox::toString("autoscan.meas_%s.result", inputNumberToRegName(inputNum).c_str());

  uint32_t const rawVal = readRegister(regName);
  double const res =
    tcds::definitions::kWRCalFactor * rawVal  / tcds::definitions::kWRMeasDuration;
  return res;
}

bool
tcds::phasemon::TCADevicePhaseMon::isInputConnected(tcds::definitions::PHASEMON_INPUT const inputNum) const
{
  std::string const regName =
    toolbox::toString("meas_%s.disconnected", inputNumberToRegName(inputNum).c_str());

  uint32_t const value = readRegister(regName);

  bool const isConnected = (value == 0x0);
  return isConnected;
}

tcds::definitions::PHASEMON_INPUT_STATUS
tcds::phasemon::TCADevicePhaseMon::getPhaseMeasurementStatus(tcds::definitions::PHASEMON_INPUT const inputNum) const
{
  // NOTE: The enablePhaseMeasurements() method sets up the hardware
  // to use the auto-scanner measurements. So these are the ones we
  // need to read back here. The over-all status is basically a
  // combination of the different time-outs and the data-valid, unless
  // the input is listed as disconnected.

  tcds::definitions::PHASEMON_INPUT_STATUS status = tcds::definitions::PHASEMON_INPUT_STATUS_DISABLED;

  if (isInputConnected(inputNum))
    {
      std::string const regNameBase =
        toolbox::toString("autoscan.meas_%s", inputNumberToRegName(inputNum).c_str());

      bool const dataValid = (readRegister(regNameBase + ".data_valid") == 0x1);
      bool const timeoutDMDT = (readRegister(regNameBase + ".timeout_dmdt") == 0x1);

      // Only the SFP inputs can be TTC streams. The Lemo inputs can
      // only be clocks, and have no TTC FMC timeout registers.
      bool timeoutTTCFMC = false;
      if ((tcds::definitions::PHASEMON_INPUT_SFP1 <= inputNum) &&
          (inputNum <= tcds::definitions::PHASEMON_INPUT_SFP7))
        {
          timeoutTTCFMC = (readRegister(regNameBase + ".timeout_ttc_fmc") == 0x1);
        }

      double const allOk = dataValid && !timeoutTTCFMC && !timeoutDMDT;

      if (allOk)
        {
          status = tcds::definitions::PHASEMON_INPUT_STATUS_GOOD;
        }
      else
        {
          status = tcds::definitions::PHASEMON_INPUT_STATUS_BAD;
        }
    }

  return status;
}
