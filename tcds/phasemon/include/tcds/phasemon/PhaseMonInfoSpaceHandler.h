#ifndef _tcds_phasemon_PhaseMonInfoSpaceHandler_h_
#define _tcds_phasemon_PhaseMonInfoSpaceHandler_h_

#include <stddef.h>
#include <string>

#include "tcds/phasemon/Definitions.h"
#include "tcds/utils/InfoSpaceHandler.h"

namespace tcds {
  namespace utils {
    class InfoSpaceUpdater;
    class Monitor;
    class WebServer;
  }
}

namespace tcds {
  namespace phasemon {

    class PhaseMon;

    class PhaseMonInfoSpaceHandler : public tcds::utils::InfoSpaceHandler
    {

    public:
      PhaseMonInfoSpaceHandler(tcds::phasemon::PhaseMon& xdaqApp,
                               tcds::utils::InfoSpaceUpdater* updater,
                               tcds::definitions::PHASEMON_INPUT const input);
      virtual ~PhaseMonInfoSpaceHandler();

      virtual tcds::phasemon::PhaseMon& getOwnerApplication() const;

    protected:
      virtual void registerItemSetsWithMonitor(tcds::utils::Monitor& monitor);
      virtual void registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                 tcds::utils::Monitor& monitor,
                                                 std::string const& forceTabName="");

      virtual std::string formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const;

    private:
      void registerItemSetsWithWebServerHelper(tcds::utils::WebServer& webServer,
                                               tcds::utils::Monitor& monitor,
                                               std::string const& tabName,
                                               tcds::definitions::PHASEMON_INPUT const input,
                                               size_t const colSpan=1);

      bool inputMatchesRefClkSrc(tcds::definitions::PHASEMON_INPUT const input,
                                 tcds::definitions::PHASEMON_REF_CLK_SRC const refClkSrc) const;

      std::string inputLabel(tcds::definitions::PHASEMON_INPUT const input) const;

      tcds::definitions::PHASEMON_INPUT phaseMonInput_;

    };

  } // namespace phasemon
} // namespace tcds

#endif // _tcds_phasemon_PhaseMonInfoSpaceHandler_h_
