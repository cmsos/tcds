#ifndef _tcds_phasemon_PhaseMon_h_
#define _tcds_phasemon_PhaseMon_h_

#include <memory>

#include "toolbox/Event.h"
#include "xdaq/Application.h"

#include "tcds/phasemon/ConfigurationInfoSpaceHandler.h"
#include "tcds/phasemon/TCADevicePhaseMon.h"
#include "tcds/utils/XDAQAppWithFSMAutomatic.h"

namespace tcds {
  namespace hwutilstca {
    class HwIDInfoSpaceHandlerTCA;
    class HwIDInfoSpaceUpdaterTCA;
    class SFPInfoSpaceUpdater;
  }
}

namespace xdaq {
  class ApplicationStub;
}

namespace tcds {
  namespace phasemon {

    class HwStatusInfoSpaceHandler;
    class HwStatusInfoSpaceUpdater;
    /* class PhaseMonInfoSpaceHandler; */
    class PhaseMonInfoSpaceUpdater;
    class PhasesInfoSpaceHandler;
    class SFPInfoSpaceHandler;

    /*
      It should be noted that the PhaseMon application is subtly
      different from any other TCDS control application. As is the
      PhaseMon firmware.

      The 'real PhaseMon' is a combination of two FC7 boards, each
      with different FMCs: a PhaseMon-CLK and a PhaseMon-TTC. The
      former is equipped with a 'six lemo' FMC and accepts pure 40 MHz
      clocks for reference and as measurement inputs. The PhaseMon-CLK
      also outputs an optical clock on the TX of the last SFP on the
      bottom ('eight SFP') FMC. This optical clock can be input to the
      RX of the last SFP on the bottom ('eight SFP') FMC on the
      PhaseMon-TTC to be used as reference clock. The top FMC site of
      the PhaseMon-TTC is equipped with a 'TTC FMC' in order to
      measure phases of encoded TTC streams.

      Even though the top FMC on both PhaseMon FC7s are different, the
      firmware is compatible with both. The actual differences are
      triggered by the software configuring the board either in
      'PhaseMon-TTC' or in 'PhaseMon-CLK' mode by the software as
      specified by the 'phasemonType' configuration parameter.

      The above means that the PhaseMon control application also
      operates in pairs: one connected to each of the two FC7s.
      */
    class PhaseMon : public tcds::utils::XDAQAppWithFSMAutomatic
    {

    public:
      XDAQ_INSTANTIATOR();

      PhaseMon(xdaq::ApplicationStub* stub);
      virtual ~PhaseMon();

      virtual ConfigurationInfoSpaceHandler const& getConfigurationInfoSpaceHandler() const;

    protected:
      virtual void setupInfoSpaces();

      virtual void enableActionImpl(toolbox::Event::Reference event);

      /**
       * Access the hardware pointer as TCADevicePhaseMon&.
       */
      virtual TCADevicePhaseMon& getHw() const;

      /* virtual void actionPerformed(xdata::Event& event); */

      virtual void hwConnectImpl();
      virtual void hwReleaseImpl();

      /* virtual void hwCfgInitializeImpl(); */
      virtual void hwCfgFinalizeImpl();

    private:
      // Various InfoSpaces and their InfoSpaceUpdaters.
      std::unique_ptr<tcds::hwutilstca::HwIDInfoSpaceUpdaterTCA> hwIDInfoSpaceUpdaterP_;
      std::unique_ptr<tcds::hwutilstca::HwIDInfoSpaceHandlerTCA> hwIDInfoSpaceP_;
      std::unique_ptr<tcds::phasemon::HwStatusInfoSpaceUpdater> hwStatusInfoSpaceUpdaterP_;
      std::unique_ptr<tcds::phasemon::HwStatusInfoSpaceHandler> hwStatusInfoSpaceP_;
      std::unique_ptr<tcds::phasemon::PhaseMonInfoSpaceUpdater> phasemonInfoSpaceUpdaterP_;
      std::unique_ptr<tcds::phasemon::PhasesInfoSpaceHandler> phasesInfoSpaceP_;
      std::unique_ptr<tcds::phasemon::SFPInfoSpaceHandler> sfpInfoSpaceP_;
      std::unique_ptr<tcds::hwutilstca::SFPInfoSpaceUpdater> sfpInfoSpaceUpdaterP_;

    };

  } // namespace phasemon
} // namespace tcds

#endif // _tcds_phasemon_PhaseMon_h_
