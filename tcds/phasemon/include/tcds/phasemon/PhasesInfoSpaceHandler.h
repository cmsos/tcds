#ifndef _tcds_phasemon_PhasesInfoSpaceHandler_h_
#define _tcds_phasemon_PhasesInfoSpaceHandler_h_

#include "tcds/utils/MultiInfoSpaceHandler.h"

namespace tcds {
  namespace utils {
    class InfoSpaceUpdater;
  }
}

namespace tcds {
  namespace phasemon {

    class PhaseMon;

    class PhasesInfoSpaceHandler : public tcds::utils::MultiInfoSpaceHandler
    {

    public:
      PhasesInfoSpaceHandler(tcds::phasemon::PhaseMon& xdaqApp,
                             tcds::utils::InfoSpaceUpdater* updater);
      virtual ~PhasesInfoSpaceHandler();

      tcds::phasemon::PhaseMon& getOwnerApplication() const;

    };

  } // namespace phasemon
} // namespace tcds

#endif // _tcds_phasemon_PhasesInfoSpaceHandler_h_
