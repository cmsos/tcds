#ifndef _tcds_phasemon_PhaseMonInfoSpaceUpdater_h_
#define _tcds_phasemon_PhaseMonInfoSpaceUpdater_h_

#include "tcds/utils/HwInfoSpaceUpdaterBase.h"

namespace tcds {
  namespace utils {
    class InfoSpaceHandler;
    class InfoSpaceItem;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace phasemon {

    class TCADevicePhaseMon;

    class PhaseMonInfoSpaceUpdater : public tcds::utils::HwInfoSpaceUpdaterBase
    {

    public:
      PhaseMonInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                               tcds::phasemon::TCADevicePhaseMon const& hw);
      virtual ~PhaseMonInfoSpaceUpdater();

      virtual bool updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                       tcds::utils::InfoSpaceHandler* const infoSpaceHandler);

    private:
      tcds::phasemon::TCADevicePhaseMon const& getHw() const;

    };

  } // namespace phasemon
} // namespace tcds

#endif // _tcds_phasemon_PhaseMonInfoSpaceUpdater_h_
