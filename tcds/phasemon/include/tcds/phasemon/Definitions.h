#ifndef _tcds_phasemon_Definitions_h_
#define _tcds_phasemon_Definitions_h_

namespace tcds {
  namespace definitions {

    // Two types of PhaseMon exist:
    // - One with an 8-SFP FMC and a TTC FMC, equiped for measuring
    //   phases of TTC streams.
    // - One with an 8-SFP FMC and a 6-Lemo FMC, equiped for measuring
    //   phases of 40 MHz clocks.
    enum PHASEMON_TYPE {
      PHASEMON_TYPE_TTC=0,
      PHASEMON_TYPE_CLK=1
    };

    // The reference clocks that can be selected.
    enum PHASEMON_REF_CLK_SRC {
      PHASEMON_REF_CLK_SRC_FCLKA=0,
      PHASEMON_REF_CLK_SRC_SFP=1,
      PHASEMON_REF_CLK_SRC_LEMO=2,
      PHASEMON_REF_CLK_SRC_UNKNOWN,
    };

    // The 'phase measurement input' numbering.
    // NOTE: There are nine phase comparators. The first seven are
    // connected to real SFP inputs. The seventh takes its input from
    // the 40 MHz output clock from the TTC FMC.  The eighth one
    // compares FCLKA to the selected RefClk.
    enum PHASEMON_INPUT {
      PHASEMON_INPUT_SFP1=1,
      PHASEMON_INPUT_SFP2=2,
      PHASEMON_INPUT_SFP3=3,
      PHASEMON_INPUT_SFP4=4,
      PHASEMON_INPUT_SFP5=5,
      PHASEMON_INPUT_SFP6=6,
      PHASEMON_INPUT_SFP7=7,
      PHASEMON_INPUT_FCLKA=8,
      PHASEMON_INPUT_TTC_FMC=9,
      PHASEMON_INPUT_ECL_TTCORB=10,
      PHASEMON_INPUT_ECL_TR0=11,
      PHASEMON_INPUT_ECL_TR1=12,
      PHASEMON_INPUT_ECL_REFCLK=13,
      PHASEMON_INPUT_SFP_REFCLK=14
    };
    int const kPhaseInputMin = PHASEMON_INPUT_SFP1;
    int const kPhaseInputMax = PHASEMON_INPUT_SFP_REFCLK;

    // The possible input signal types: 40 MHz clock and TTC stream.
    enum PHASEMON_SIGNAL_TYPE {
      PHASEMON_SIGNAL_TYPE_CLOCK = 0,
      PHASEMON_SIGNAL_TYPE_TTC = 1
    };

    // Possible input status situations.
    enum PHASEMON_INPUT_STATUS {
      PHASEMON_INPUT_STATUS_DISABLED = 0,
      PHASEMON_INPUT_STATUS_GOOD = 1,
      PHASEMON_INPUT_STATUS_BAD = 2
    };

  } // namespace definitions
} // namespace tcds

#endif // _tcds_phasemon_Definitions_h_
