#ifndef _tcds_phasemon_Utils_h_
#define _tcds_phasemon_Utils_h_

#include <map>
#include <string>

#include "tcds/phasemon/Definitions.h"

namespace tcds {
  namespace phasemon {

    // The mapping between the PhaseMon hardware types and some
    // readable strings.
    std::map<tcds::definitions::PHASEMON_TYPE, std::string> phasemonTypeMap();
    std::string phasemonTypeToString(tcds::definitions::PHASEMON_TYPE const type);

    // The mapping between reference clock sources and some readable strings.
    std::map<tcds::definitions::PHASEMON_REF_CLK_SRC, std::string> refClkSrcMap();
    std::string refClkSrcToString(tcds::definitions::PHASEMON_REF_CLK_SRC const src);

    // The mapping between the phase measurement 'channels' and the
    // corresponding register names, label names, etc.
    std::map<tcds::definitions::PHASEMON_INPUT, std::string> inputRegNameMap();
    std::string inputNumberToRegName(tcds::definitions::PHASEMON_INPUT const input);
    std::map<tcds::definitions::PHASEMON_INPUT, std::string> inputNameMap();
    std::string inputNumberToName(tcds::definitions::PHASEMON_INPUT const input);
    tcds::definitions::PHASEMON_INPUT inputRegNameToNumber(std::string const& regName);
    std::map<tcds::definitions::PHASEMON_INPUT, std::string> inputLabelNameMap();
    std::string inputNumberToLabelName(tcds::definitions::PHASEMON_INPUT const input);
    std::string refClkSrcToLabelName(tcds::definitions::PHASEMON_REF_CLK_SRC const input);

    /* // The mapping between the input signal types (40 MHz clock and */
    /* // TTC stream) and some readable strings. */
    /* std::map<tcds::definitions::PHASEMON_SIGNAL_TYPE, std::string> signalTypeMap(); */
    /* std::string signalTypeToString(tcds::definitions::PHASEMON_SIGNAL_TYPE const type); */

    // Mapping between input status values and readable strings.
    std::map<tcds::definitions::PHASEMON_INPUT_STATUS, std::string> inputStatusMap();
    std::string inputStatusToString(tcds::definitions::PHASEMON_INPUT_STATUS const status);

  } // namespace phasemon
} // namespace tcds

#endif // _tcds_phasemon_Utils_h_
