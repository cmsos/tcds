#!/usr/bin/env python

###############################################################################
## A hackish script to configure a TTCci to mimic a few lumi sections
## worth of lumi-DAQ synchronisation data.
# NOTE: This has been extremely tweaked to the TTCci. Don't use it for
# anything else.
###############################################################################

import sys
import commands

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

# These two parameters allow one to configure the VME slot of the
# TTCci module to be used, as well as the B-channels to be used.
# NOTE: Valid range: [0, 15].
TTCCI_SLOT_NUMBER = 7

# This is the B-channel that sends the actual data (i.e., the 32-bit
# numbers).
BCHANNEL_NUMBER_DATA = 15

# This is the B-channel that sends the lumi nibble ticks.
BCHANNEL_NUMBER_TICKS = 14

# The data sent on the B-channel as lumi nibble marker.
NIBBLE_MARKER_DATA_WORD = 0x10

# The durations of lumi nibbles and sections.
NUM_ORBITS_PER_NIBBLE = 2**12
NUM_NIBBLES_PER_SECTION = 16

# The `filler' short B-command data used as spacer between the various
# nibble counters and markers.
FILLER_DATA_WORD = 0x0

###############################################################################

# VME A24 address map: mapping from slot numbers to TTCci base
# addresses.
# NOTE: Only the below slots are supported.
ttcci_base_addresses = {
    2 : 0x100000,
    3 : 0x180000,
    4 : 0x200000,
    5 : 0x280000,
    6 : 0x300000,
    7 : 0x380000,
    8 : 0x400000,
    9 : 0x480000,
    10 : 0x500000,
    11 : 0x580000,
    12 : 0x600000,
    13 : 0x680000,
    14 : 0x700000,
    15 :0x780000
}

bchannel_ram_base_addresses = {
    0 : 0x20000,
    1 : 0x22000,
    2 : 0x24000,
    3 : 0x26000,
    4 : 0x28000,
    5 : 0x2a000,
    6 : 0x2c000,
    7 : 0x2e000,
    8 : 0x30000,
    9 : 0x32000,
    10 : 0x34000,
    11 : 0x36000,
    12 : 0x38000,
    13 : 0x3a000,
    14 : 0x3c000,
    15 : 0x3e000
}

# The TTCci VME access works in words of 32 bits, or 4 bytes.
DATA_SIZE = 4

NUM_ENTRIES_IN_BCHANNEL_RAM = 1024
NUM_WORDS_PER_BCHANNEL_RAM_ENTRY = 2
NUM_WORDS_IN_BCHANNEL_RAM = NUM_ENTRIES_IN_BCHANNEL_RAM * NUM_WORDS_PER_BCHANNEL_RAM_ENTRY

# Bit masks for the overall B-channel configuration.
OR_MASK_SINGLE_B_COMMAND = 1 << 31
OR_MASK_DOUBLE_B_COMMAND = 1 << 30
OR_MASK_REPETITIVE = 1 << 28

# Bit masks etc. for B-channel RAM control words.
OR_MASK_LONG_B_COMMAND = 1 << (32 - 32)
OR_MASK_NOTRANSMIT = 1 << (33 - 32)
OR_MASK_RAMEMPTY = 1 << (34 - 32)
BIT_NUM_POSTSCALE = 48 - 32

# The maximum postscale value in the B-channel RAM (16 bits).
MAX_POSTSCALE = 0xffff

# Bit masks etc. for configuration of long B-commands.
OR_MASK_ADDRESSED_COMMAND = 1 << 16
AND_MASK_ADDRESS = 0x7fff
AND_MASK_SUBADDRESS = 0xff
AND_MASK_DATA = 0xff
BIT_NUM_ADDRESS = 18
BIT_NUM_EXTERNAL_FLAG = 17
BIT_NUM_SUBADDRESS = 8
BIT_NUM_DATA = 0

TYPE_NIBBLE_NUMBER = 0x1
TYPE_SECTION_NUMBER = 0x2

# The lowest valid nibble and section numbers.
NIBBLE_NUMBER_START = 1
SECTION_NUMBER_START = 1

FMT_STR_32BIT = "{0:0>32b}"

###############################################################################

def chunkify(input_list, chunk_size):
    res = [input_list[i:i + chunk_size] for i in range(0, len(input_list), chunk_size)]
    # End of chunkify().
    return res

###############################################################################

def writeWord(address, word):
    cmd = "{0} -o 0x{1:x} -w 0x{2:0>8x}".format(debugvme_cmd, address, word)
    # print "DEBUG JGH writing to address 0x{0:0>8x}.".format(address)
    # print "DEBUG JGH   cmd = '{0}'.".format(cmd)
    executeCommand(cmd)
    # End of writeWord().

###############################################################################

def writeEndOfSequence(address):
    # Add a 'RamEmpty' word, signalling the end of a B-channel RAM sequence.
    data_word = 0x0
    ctrl_word = OR_MASK_RAMEMPTY | OR_MASK_NOTRANSMIT
    writeWord(address, data_word)
    writeWord(address + DATA_SIZE, ctrl_word)
    # End of writeEndOfSequence().

###############################################################################

def executeCommand(cmd):
    (status, output) = commands.getstatusoutput(cmd)
    if status != 0:
        print >> sys.stderr, "ERROR Could not write to the TTCci."
        print >> sys.stderr, "ERROR   cmd = '{0}'.".format(cmd)
        print >> sys.stderr, "ERROR   status = {0:d}.".format(status)
        print >> sys.stderr, "ERROR   output = {0}.".format(output)
        sys.exit(1)
    # End of executeCommand().

###############################################################################

def buildLongBCommandWord(address, sub_address, is_external, data):
    tmp_res = ((address & AND_MASK_ADDRESS) << BIT_NUM_ADDRESS) + \
              ((is_external == True) << BIT_NUM_EXTERNAL_FLAG) + \
              ((sub_address & AND_MASK_SUBADDRESS) << BIT_NUM_SUBADDRESS) + \
              ((data & AND_MASK_DATA) << BIT_NUM_DATA)
    res = tmp_res | OR_MASK_ADDRESSED_COMMAND
    # End of buildLongBCommandWord().
    return res

###############################################################################

if __name__ == "__main__":

    print "Each lumi nibble contains {0:d} orbits.".format(NUM_ORBITS_PER_NIBBLE)
    print "Each lumi section contains {0:d} nibbles.".format(NUM_NIBBLES_PER_SECTION)

    # Determine the number of lumi sections that fit into the TTCci
    # B-channel RAM based on the number of nibbles per section and the
    # fact that nine RAM entries per nibble are required (four for the
    # section number, four for the nibble number, and one for the
    # pause).
    num_sections = (NUM_ENTRIES_IN_BCHANNEL_RAM / 9) / NUM_NIBBLES_PER_SECTION
    print "--> The RAM can fit {0:d} lumi sections worth of synchronisation data.".format(num_sections)

    debugvme_lib_path = "/opt/xdaq/lib/"
    debugvme_bin_path = "/opt/xdaq/bin/"
    vme_address_modifier = 0x39

    base_address = ttcci_base_addresses[TTCCI_SLOT_NUMBER]
    tmp = "Configuring TTCci in VME slot #{0:d} (base address 0x{1:x})."
    print tmp.format(TTCCI_SLOT_NUMBER, base_address)

    debug_vmd_cmd_str = "export LD_LIBRARY_PATH={0}; " \
                        "{1}/DebugVME.exe -b 0x{2:x} -m 0x{3:x}"
    debugvme_cmd = debug_vmd_cmd_str.format(debugvme_lib_path,
                                            debugvme_bin_path,
                                            base_address,
                                            vme_address_modifier)

    #----------

    # Setup the B-channel to send lumi nibble ticks.
    bchannel_ram_base_address = bchannel_ram_base_addresses[BCHANNEL_NUMBER_TICKS]
    msg = "Configuring B-channel #{0:d} at address 0x{1:x} " \
          "to send lumi nibble markers."
    print msg.format(BCHANNEL_NUMBER_TICKS, bchannel_ram_base_address)
    adddress_bch_ihb_ofs = 0x0100 + (0x0010 * BCHANNEL_NUMBER_TICKS)
    adddress_bch_psc_ofs = adddress_bch_ihb_ofs + 0x0004
    adddress_bch_pos_ofs = adddress_bch_ihb_ofs + 0x0008
    bchannel_ctrl_word = OR_MASK_SINGLE_B_COMMAND | OR_MASK_REPETITIVE
    writeWord(adddress_bch_ihb_ofs, bchannel_ctrl_word)
    prescale = NUM_ORBITS_PER_NIBBLE
    writeWord(adddress_bch_psc_ofs, prescale)
    writeWord(adddress_bch_pos_ofs, 0x0)

    # First clear the B-channel RAM.
    print "  Clearing B-channel RAM."
    writeEndOfSequence(bchannel_ram_base_address)

    # Now build a sequence of lumi nibbles for a single lumi section.
    print "  Configuring B-channel RAM."
    address = bchannel_ram_base_address
    # Data word.
    data_word = NIBBLE_MARKER_DATA_WORD
    writeWord(address, data_word)
    # Control word.
    ctrl_word = (1 << BIT_NUM_POSTSCALE)
    writeWord(address + DATA_SIZE, ctrl_word)
    writeEndOfSequence(address + 2 * DATA_SIZE)

    #----------

    # Setup the B-channel to send the data (i.e., the actual numbers).
    bchannel_ram_base_address = bchannel_ram_base_addresses[BCHANNEL_NUMBER_DATA]
    msg = "Configuring B-channel #{0:d} at address 0x{1:x} " \
          "to send synchronization data."
    print msg.format(BCHANNEL_NUMBER_DATA, bchannel_ram_base_address)
    adddress_bch_ihb_ofs = 0x0100 + (0x0010 * BCHANNEL_NUMBER_DATA)
    adddress_bch_psc_ofs = adddress_bch_ihb_ofs + 0x0004
    adddress_bch_pos_ofs = adddress_bch_ihb_ofs + 0x0008
    bchannel_ctrl_word = OR_MASK_SINGLE_B_COMMAND | OR_MASK_REPETITIVE
    writeWord(adddress_bch_ihb_ofs, bchannel_ctrl_word)
    writeWord(adddress_bch_psc_ofs, 0x0)
    writeWord(adddress_bch_pos_ofs, 0x0)

    # First clear the B-channel RAM.
    print "  Clearing B-channel RAM."
    writeEndOfSequence(bchannel_ram_base_address)

    # Now build a sequence of lumi nibbles for a single lumi section.
    print "  Filling B-channel RAM. (This takes a while.)"
    entry_index = 0
    for section_number in xrange(SECTION_NUMBER_START, SECTION_NUMBER_START + num_sections):
        # print "DEBUG JGH section #{0:d}.".format(section_number)
        section_number_str = FMT_STR_32BIT.format(section_number)
        # print "DEBUG JGH (0b{0}).".format(section_number_str)
        section_words = chunkify(section_number_str, 8)[::-1]
        for nibble_number in xrange(NIBBLE_NUMBER_START, NIBBLE_NUMBER_START + NUM_NIBBLES_PER_SECTION):
            # print "DEBUG JGH   nibble #{0:d}.".format(nibble_number)
            nibble_number_str = FMT_STR_32BIT.format(nibble_number)
            # print "DEBUG JGH   (0b{0}).".format(nibble_number_str)
            nibble_words = chunkify(nibble_number_str, 8)[::-1]
            for (i, word) in enumerate(section_words):
                offset = entry_index * NUM_WORDS_PER_BCHANNEL_RAM_ENTRY * DATA_SIZE
                address = bchannel_ram_base_address + offset
                # print "DEBUG JGH   0 entry #{0:d}.".format(entry_index)
                # Data word.
                identifier = i + (TYPE_SECTION_NUMBER << 4)
                data_word = buildLongBCommandWord(0,
                                                  identifier,
                                                  True,
                                                  int(word, 2))
                writeWord(address, data_word)
                # Control word.
                ctrl_word = (1 << BIT_NUM_POSTSCALE) | OR_MASK_LONG_B_COMMAND
                writeWord(address + DATA_SIZE, ctrl_word)
                entry_index += 1
                # ASSERT ASSERT ASSERT
                assert (entry_index <= NUM_WORDS_IN_BCHANNEL_RAM)
                # ASSERT ASSERT ASSERT end
            for (i, word) in enumerate(nibble_words):
                offset = entry_index * NUM_WORDS_PER_BCHANNEL_RAM_ENTRY * DATA_SIZE
                address = bchannel_ram_base_address + offset
                # print "DEBUG JGH   1 entry #{0:d}.".format(entry_index)
                # Data word.
                identifier = i + (TYPE_NIBBLE_NUMBER << 4)
                data_word = buildLongBCommandWord(0,
                                                  identifier,
                                                  True,
                                                  int(word, 2))
                writeWord(address, data_word)
                # Control word.
                ctrl_word = (1 << BIT_NUM_POSTSCALE) | OR_MASK_LONG_B_COMMAND
                writeWord(address + DATA_SIZE, ctrl_word)
                entry_index += 1
                # ASSERT ASSERT ASSERT
                assert (entry_index <= NUM_WORDS_IN_BCHANNEL_RAM)
                # ASSERT ASSERT ASSERT end

            # Insert a pause long enough to fill the rest of the lumi nibble.
            offset = entry_index * NUM_WORDS_PER_BCHANNEL_RAM_ENTRY * DATA_SIZE
            address = bchannel_ram_base_address + offset
            data_word = FILLER_DATA_WORD
            # NOTE: We need eight orbits to send two 32-bit numbers
            # (i.e., the section number and the nibble number).
            postscale = NUM_ORBITS_PER_NIBBLE - 8
            # ASSERT ASSERT ASSERT
            assert postscale <= MAX_POSTSCALE
            # ASSERT ASSERT ASSERT end
            ctrl_word = (postscale << BIT_NUM_POSTSCALE)
            writeWord(address, data_word)
            writeWord(address + DATA_SIZE, ctrl_word)
            entry_index += 1
            # ASSERT ASSERT ASSERT
            assert (entry_index <= NUM_WORDS_IN_BCHANNEL_RAM)
            # ASSERT ASSERT ASSERT end

    # Now add the 'RamEmpty' flag, signalling the end of the sequence.
    offset = entry_index * NUM_WORDS_PER_BCHANNEL_RAM_ENTRY * DATA_SIZE
    writeEndOfSequence(bchannel_ram_base_address + offset)

    #----------

    print "Done"

###############################################################################
