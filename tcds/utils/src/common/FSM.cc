#include "tcds/utils/FSM.h"

#include <inttypes.h>
#include <stdint.h>
#include <vector>

#include "toolbox/Event.h"
#include "toolbox/fsm/AsynchronousFiniteStateMachine.h"
#include "toolbox/fsm/FiniteStateMachine.h"
#include "toolbox/fsm/InvalidInputEvent.h"
#include "toolbox/fsm/exception/Exception.h"
#include "toolbox/string.h"
#include "xcept/Exception.h"
#include "xcept/tools.h"
#include "xdaq/ApplicationDescriptor.h"
#include "xdaq2rc/ClassnameAndInstance.h"
#include "xdata/Bag.h"
#include "xdata/Serializable.h"
#include "xdata/String.h"
#include "xoap/MessageFactory.h"
#include "xoap/exception/Exception.h"

#include "tcds/exception/Exception.h"
#include "tcds/utils/ConfigurationInfoSpaceHandler.h"
#include "tcds/utils/LogMacros.h"
#include "tcds/utils/OwnerId.h"
#include "tcds/utils/SOAPUtils.h"
#include "tcds/utils/XDAQAppWithFSMBase.h"

tcds::utils::FSM::FSM(XDAQAppWithFSMBase* const xdaqApp) :
  fsmP_(0),
  xdaqAppP_(xdaqApp),
  logger_(xdaqAppP_->getApplicationLogger()),
  rcmsNotifier_(xdaqAppP_)
{
  // Find our connection to RCMS.
  rcmsNotifier_.findRcmsStateListener();

  // Create the underlying Finite State Machine itself.
  std::string const commandLoopName =
    toolbox::toString("FSMCommandLoop_lid%d",
                      xdaqAppP_->getApplicationDescriptor()->getLocalId());
  fsmP_ = new toolbox::fsm::AsynchronousFiniteStateMachine(commandLoopName);

  // Setup the FSM with states and transitions.
  // NOTE: There is also the hard-coded 'F'/'Failed' state.
  fsmP_->addState('q', "ColdResetting",        this, &FSM::stateChangedWithNotification);
  fsmP_->addState('c', "Configuring",          this, &FSM::stateChangedWithNotification);
  fsmP_->addState('C', "Configured",           this, &FSM::stateChangedWithNotification);
  fsmP_->addState('e', "Enabling",             this, &FSM::stateChangedWithNotification);
  fsmP_->addState('E', "Enabled",              this, &FSM::stateChangedWithNotification);
  fsmP_->addState('h', "Halting",              this, &FSM::stateChangedWithNotification);
  fsmP_->addState('H', "Halted",               this, &FSM::stateChangedWithNotification);
  fsmP_->addState('p', "Pausing",              this, &FSM::stateChangedWithNotification);
  fsmP_->addState('P', "Paused",               this, &FSM::stateChangedWithNotification);
  fsmP_->addState('r', "Resuming",             this, &FSM::stateChangedWithNotification);
  fsmP_->addState('s', "Stopping",             this, &FSM::stateChangedWithNotification);
  fsmP_->addState('z', "PostConfigureZeroing", this, &FSM::stateChangedWithNotification);
  fsmP_->addState('y', "PreEnableZeroing",     this, &FSM::stateChangedWithNotification);

  // NOTE: We ran out of appropriate letters, so let's just use
  // numbers for some of the intermediate states.
  fsmP_->addState('0', "TTCResyncing",     this, &FSM::stateChangedWithNotification);
  fsmP_->addState('1', "TTCHardResetting", this, &FSM::stateChangedWithNotification);

  // ColdReset: H -> H.
  fsmP_->addStateTransition('H', 'q', "ColdReset");
  fsmP_->addStateTransition('q', 'H', "ColdResettingDone", xdaqAppP_, &XDAQAppWithFSMBase::coldResetAction);

  // Configure: H -> C.
  fsmP_->addStateTransition('H', 'c', "Configure");
  fsmP_->addStateTransition('c', 'z', "ConfiguringDone", xdaqAppP_, &XDAQAppWithFSMBase::configureAction);
  fsmP_->addStateTransition('z', 'C', "PostConfigureZeroingDone", xdaqAppP_, &XDAQAppWithFSMBase::zeroAction);

  // Reconfigure: C -> C.
  fsmP_->addStateTransition('C', 'c', "Reconfigure");

  // Enable: C -> E.
  fsmP_->addStateTransition('C', 'y', "Enable");
  fsmP_->addStateTransition('y', 'e', "PreEnableZeroingDone", xdaqAppP_, &XDAQAppWithFSMBase::zeroAction);
  fsmP_->addStateTransition('e', 'E', "EnablingDone", xdaqAppP_, &XDAQAppWithFSMBase::enableAction);

  // Pause: E -> P.
  fsmP_->addStateTransition('E', 'p', "Pause");
  fsmP_->addStateTransition('p', 'P', "PausingDone", xdaqAppP_, &XDAQAppWithFSMBase::pauseAction);

  // Resume: P -> E.
  fsmP_->addStateTransition('P', 'r', "Resume");
  fsmP_->addStateTransition('r', 'E', "ResumingDone", xdaqAppP_, &XDAQAppWithFSMBase::resumeAction);

  // Stop: E/P -> S.
  fsmP_->addStateTransition('E', 's', "Stop");
  fsmP_->addStateTransition('P', 's', "Stop");
  fsmP_->addStateTransition('s', 'C', "StoppingDone", xdaqAppP_, &XDAQAppWithFSMBase::stopAction);

  // Halt: C/E/F/H/P -> H.
  fsmP_->addStateTransition('C', 'h', "Halt");
  fsmP_->addStateTransition('E', 'h', "Halt");
  fsmP_->addStateTransition('F', 'h', "Halt");
  fsmP_->addStateTransition('H', 'h', "Halt");
  fsmP_->addStateTransition('P', 'h', "Halt");
  fsmP_->addStateTransition('h', 'H', "HaltingDone", xdaqAppP_, &XDAQAppWithFSMBase::haltAction);

  // The following are two special transitions from Paused to Paused:
  // - TTCResync:    Paused -> TTCResyncing -> Paused.
  // - TTCHardReset: Paused -> TTCHardReseting -> Paused.
  fsmP_->addStateTransition('P', '0', "TTCResync");
  fsmP_->addStateTransition('0', 'P', "TTCResyncingDone", xdaqAppP_, &XDAQAppWithFSMBase::ttcResyncAction);
  fsmP_->addStateTransition('P', '1', "TTCHardReset");
  fsmP_->addStateTransition('1', 'P', "TTCHardResettingDone", xdaqAppP_, &XDAQAppWithFSMBase::ttcHardResetAction);

  // The following are special, for the special case of failure.
  // NOTE: The following automatically creates an 'X' -> 'F'
  // transition for all states 'X'.
  std::vector<toolbox::fsm::State> const states = fsmP_->getStates();
  for (std::vector<toolbox::fsm::State>::const_iterator it = states.begin();
       it != states.end();
       ++it)
    {
      fsmP_->addStateTransition(*it, 'F', "Fail");
    }
  fsmP_->setFailedStateTransitionAction(xdaqAppP_, &XDAQAppWithFSMBase::failAction);
  fsmP_->setFailedStateTransitionChanged(this, &FSM::stateChangedToFailedWithNotification);
  fsmP_->setStateName('F', "Failed");
  fsmP_->setInvalidInputStateTransitionAction(this, &FSM::invalidStateTransitionAction);

  //----------

  // Now bolt on a little bit of extra functionality: automatic transitions.
  // NOTE: The XDAQ FSM only supports methods that are called upon
  // state changes, i.e., when _leaving_ a state, the following looks
  // a bit clumsy, name-wise.

  // One entry for each of the intermediate '***ing' transition states.
  // E.g.: when done with 'ColdResetting' continue with 'ColdResettingDone'.
  automaticTransitions_["ColdResetting"] = "ColdResettingDone";
  automaticTransitions_["Configuring"] = "ConfiguringDone";
  automaticTransitions_["Enabling"] = "EnablingDone";
  automaticTransitions_["Halting"] = "HaltingDone";
  automaticTransitions_["Pausing"] = "PausingDone";
  automaticTransitions_["Resuming"] = "ResumingDone";
  automaticTransitions_["Stopping"] = "StoppingDone";
  automaticTransitions_["PostConfigureZeroing"] = "PostConfigureZeroingDone";
  automaticTransitions_["PreEnableZeroing"] = "PreEnableZeroingDone";
  automaticTransitions_["TTCResyncing"] = "TTCResyncingDone";
  automaticTransitions_["TTCHardResetting"] = "TTCHardResettingDone";

  //----------

  // Start out with the FSM in its initial state: Halted.
  fsmP_->setInitialState('H');
  reset("Startup.");
}

tcds::utils::FSM::~FSM()
{
  if (fsmP_ != 0)
    {
      delete fsmP_;
      fsmP_ = 0;
    }
}

xoap::MessageReference
tcds::utils::FSM::changeState(xoap::MessageReference msg)
{
  // NOTE: Since we only get here after a successful path through the
  // XDAQ SOAP callback system, the SOAP command name extraction
  // should not pose any problem. Extracting the parameters can prove
  // tricky though.

  std::string errMsg = "";
  std::string commandName = "undefined";
  bool haveCommandName = false;
  bool foundProblem = false;
  try
    {
      commandName = tcds::utils::soap::extractSOAPCommandName(msg);
      haveCommandName = true;
    }
  catch(xcept::Exception& err)
    {
      foundProblem = true;
      std::string const msgBase = "Failed to extract command name from SOAP message";
      ERROR(msgBase + ": '" + xcept::stdformat_exception_history(err) + "'.");
      errMsg = msgBase + ": '" + err.message() + "'.";
    }

  // uint32_t rcmsSessionId = 0;
  // bool haveRCMSSessionId = false;
  // try
  //   {
  //     rcmsSessionId = tcds::utils::soap::extractSOAPCommandRCMSSessionId(msg);
  //     haveRCMSSessionId = true;
  //   }
  // catch(xcept::Exception& err)
  //   {
  //     foundProblem = true;
  //     std::string const msgBase = "Failed to extract RCMS session ID attribute from SOAP message";
  //     ERROR(msgBase + ": '" + xcept::stdformat_exception_history(err) + "'.");
  //     errMsg = msgBase + ": '" + err.message() + "'.";
  //   }

  // std::string requestorId = "undefined";
  // bool haveRequestorId = false;
  // try
  //   {
  //     requestorId = tcds::utils::soap::extractSOAPCommandRequestorId(msg);
  //     haveRequestorId = true;
  //   }
  // catch(xcept::Exception& err)
  //   {
  //     foundProblem = true;
  //     std::string const msgBase = "Failed to extract requestor ID attribute from SOAP message";
  //     ERROR(msgBase + ": '" + xcept::stdformat_exception_history(err) + "'.");
  //     errMsg = msgBase + ": '" + err.message() + "'.";
  //   }

  tcds::utils::OwnerId requestorId;
  bool haveRequestorId = false;
  try
    {
      requestorId = tcds::utils::soap::extractSOAPCommandOwnerId(msg);
      haveRequestorId = true;
    }
  catch(xcept::Exception& err)
    {
      foundProblem = true;
      std::string const msgBase = "Failed to extract requestor ID attribute from SOAP message";
      ERROR(msgBase + ": '" + xcept::stdformat_exception_history(err) + "'.");
      errMsg = msgBase + ": '" + err.message() + "'.";
    }

  if (foundProblem)
    {
      // Somehow we don't understand the SOAP message. Log an error
      // and flag that we are having trouble.
      std::string histMsg = "Ignoring ununderstood FSM SOAP command";
      if (haveCommandName)
        {
          histMsg += " '" + commandName + "'";
        }
      // if (haveRCMSSessionId)
      //   {
      //     histMsg += toolbox::toString(" RCMS session ID: '%u'.",
      //                                  rcmsSessionId);
      //   }
      if (haveRequestorId)
        {
          histMsg += " Requestor: " + requestorId.asString();
        }
      xdaqAppP_->appStateInfoSpace_.addHistoryItem(histMsg);
      std::string const fullMsg = histMsg + " " + errMsg;
      ERROR(fullMsg);
      XCEPT_DECLARE(tcds::exception::FSMTransitionProblem,
                    top,
                    fullMsg);
      xdaqAppP_->notifyQualified("error", top);
      tcds::utils::soap::SOAPFaultCodeType faultCode = tcds::utils::soap::SOAPFaultCodeSender;
      std::string const faultString = histMsg.c_str();
      std::string const faultDetail = errMsg.c_str();
      xoap::MessageReference reply =
        tcds::utils::soap::makeSOAPFaultReply(xdaqAppP_,
                                              msg,
                                              faultCode,
                                              faultString,
                                              faultDetail);
      return reply;
    }

  //----------

  // Now a little bit of checking; we should have received at least
  // one of 'xdaq:rcmsSessionId' or 'xdaq:actionrequestorId'.
  if (!requestorId.isValid())
    {
      std::string const histMsg =
        toolbox::toString("Ignoring FSM SOAP command '%s' from unidentified requestor.",
                          commandName.c_str());
      // if (rcmsSessionId != 0)
      //   {
      //     histMsg += toolbox::toString(" RCMS session ID: '%u'.",
      //                                  rcmsSessionId);
      //   }
      // if (!requestorId.empty())
      //   {
      //     histMsg += " Requestor: '" + requestorId + "'";
      //   }
      xdaqAppP_->appStateInfoSpace_.addHistoryItem(histMsg);
      std::string const errMsg =
        "The '" + commandName + "' SOAP command requires at least one"
        " of the 'xdaq:rcmsSessionId' or 'xdaq:actionrequestorId' attributes.";
      std::string const fullMsg = histMsg + " " + errMsg;
      ERROR(fullMsg);
      XCEPT_DECLARE(tcds::exception::FSMTransitionProblem,
                    top,
                    fullMsg);
      xdaqAppP_->notifyQualified("error", top);
      tcds::utils::soap::SOAPFaultCodeType faultCode = tcds::utils::soap::SOAPFaultCodeSender;
      std::string const faultString = histMsg.c_str();
      std::string const faultDetail = errMsg.c_str();
      xoap::MessageReference reply =
        tcds::utils::soap::makeSOAPFaultReply(xdaqAppP_,
                                              msg,
                                              faultCode,
                                              faultString,
                                              faultDetail);
      return reply;
    }

  //----------

  // // Now do a little bit of data-massage: fill the hardware lease
  // // owner based on the RCMS session ID if only the latter was
  // // specified.
  // if (requestorId.empty())
  //   {
  //     requestorId = toolbox::toString("RCMS_session_%u", rcmsSessionId);
  //     INFO("No actionRequestorId specified. "
  //          "Setting actionRequestorId to " << requestorId.asString()
  //          << " based on rcmsSessionId.");
  //   }

  //----------

  INFO(toolbox::toString("Received FSM transition command '%s' from %s.",
                         commandName.c_str(),
                         requestorId.asString().c_str()));

  //----------

  // Figure out if the current FSM command is allowed, based on our
  // (lack of) hardware lease.

  // First question: Is there a lease owner currently?
  if (xdaqAppP_->isHwLeased() ||
      (xdaqAppP_->wasHwLeased() && (requestorId == xdaqAppP_->getExpiredHwLeaseOwnerId())))
    {
      // Second question: Who is the current lease owner? Or who was
      // the owner who lost his lease?
      tcds::utils::OwnerId leaseOwnerId;
      if (xdaqAppP_->isHwLeased())
        {
          leaseOwnerId = xdaqAppP_->getHwLeaseOwnerId();
        }
      else if (xdaqAppP_->wasHwLeased())
        {
          leaseOwnerId = xdaqAppP_->getExpiredHwLeaseOwnerId();
        }

      // Third question: Does the current request come from our
      // hardware owner?
      if (requestorId != leaseOwnerId)
        {
          std::string const histMsg =
            toolbox::toString("Ignoring state transition command '%s' requested by %s: "
                              "the current hardware lease owner is %s.",
                              commandName.c_str(),
                              requestorId.asString().c_str(),
                              leaseOwnerId.asString().c_str());
          xdaqAppP_->appStateInfoSpace_.addHistoryItem(histMsg);
          // No match -> send error back.
          std::string msgBase = histMsg;
          ERROR(msgBase);
          XCEPT_DECLARE(tcds::exception::FSMTransitionProblem, top, msgBase);
          xdaqAppP_->notifyQualified("error", top);
          tcds::utils::soap::SOAPFaultCodeType faultCode = tcds::utils::soap::SOAPFaultCodeReceiver;
          std::string faultString = msgBase;
          xoap::MessageReference reply =
            tcds::utils::soap::makeSOAPFaultReply(xdaqAppP_,
                                                  msg,
                                                  faultCode,
                                                  faultString);
          return reply;
        }
    }
  else
    {
      // No lease holder, and no expired lease either. In this case we
      // can either 'ColdReset', 'Configure' (the usual path), or
      // 'Halt' (to recover from a lost RunControl session). Any other
      // request will result in an error.
      if ((commandName != "ColdReset") &&
          (commandName != "Configure") &&
          (commandName != "Halt"))
        {
          std::string const histMsg =
            toolbox::toString("Ignoring state transition command '%s' requested by %s: "
                              "there is currently no hardware lease owner. "
                              "Without hardware lease holder "
                              "only 'ColdReset', Configure', and 'Halt' "
                              "FSM commands are allowed.",
                              commandName.c_str(),
                              requestorId.asString().c_str());
          xdaqAppP_->appStateInfoSpace_.addHistoryItem(histMsg);
          std::string msgBase = histMsg;
          ERROR(msgBase);
          XCEPT_DECLARE(tcds::exception::FSMTransitionProblem, top, msgBase);
          xdaqAppP_->notifyQualified("error", top);
          tcds::utils::soap::SOAPFaultCodeType faultCode = tcds::utils::soap::SOAPFaultCodeReceiver;
          std::string faultString = msgBase;
          xoap::MessageReference reply =
            tcds::utils::soap::makeSOAPFaultReply(xdaqAppP_,
                                                  msg,
                                                  faultCode,
                                                  faultString);
          return reply;
        }
    }

  //----------

  // Once we get here, let's see if the requested state transition is
  // a valid one for the current state.
  std::map<std::string, toolbox::fsm::State> allowedTransitions =
    fsmP_->getTransitions(fsmP_->getCurrentState());
  if (allowedTransitions.find(commandName) == allowedTransitions.end())
    {
      std::string const histMsg =
        toolbox::toString("Ignoring invalid state transition '%s' from state '%s', "
                          "requested by %s.",
                          commandName.c_str(),
                          getCurrentStateName().c_str(),
                          requestorId.asString().c_str());
      xdaqAppP_->appStateInfoSpace_.addHistoryItem(histMsg);
      std::string msgBase = histMsg;
      ERROR(msgBase);
      XCEPT_DECLARE(tcds::exception::FSMTransitionProblem, top, msgBase);
      xdaqAppP_->notifyQualified("error", top);
      tcds::utils::soap::SOAPFaultCodeType faultCode = tcds::utils::soap::SOAPFaultCodeReceiver;
      std::string faultString = msgBase;
      xoap::MessageReference reply =
        tcds::utils::soap::makeSOAPFaultReply(xdaqAppP_,
                                              msg,
                                              faultCode,
                                              faultString);
      return reply;
    }

  //----------

  try
    {
      xdaqAppP_->loadSOAPCommandParameters(msg);
    }
  catch(xcept::Exception& err)
    {
      std::string const histMsg =
        toolbox::toString("Ignoring SOAP command '%s' requested by %s: "
                          "failed to import the command parameters: '%s'.",
                          commandName.c_str(),
                          requestorId.asString().c_str(),
                          err.what());
      xdaqAppP_->appStateInfoSpace_.addHistoryItem(histMsg);
      std::string msgBase = histMsg;
      ERROR(toolbox::toString("%s: '%s'.",
                              msgBase.c_str(),
                              xcept::stdformat_exception_history(err).c_str()));
      XCEPT_DECLARE_NESTED(tcds::exception::FSMTransitionProblem, top,
                           toolbox::toString("%s.", msgBase.c_str()), err);
      xdaqAppP_->notifyQualified("error", top);
      tcds::utils::soap::SOAPFaultCodeType faultCode = tcds::utils::soap::SOAPFaultCodeSender;
      std::string faultString = msgBase.c_str();
      std::string faultDetail = err.message();
      xoap::MessageReference reply =
        tcds::utils::soap::makeSOAPFaultReply(xdaqAppP_,
                                              msg,
                                              faultCode,
                                              faultString,
                                              faultDetail);
      return reply;
    }

  //----------

  // If we get here, all is good. Take the lease and then perform the
  // state transition.

  // But first mark the transition in the application history.
  std::string histMsg = toolbox::toString("Received '%s' SOAP command from %s.",
                                          commandName.c_str(),
                                          requestorId.asString().c_str());
  xdaqAppP_->appStateInfoSpace_.addHistoryItem(histMsg);

  xdaqAppP_->assignHwLease(requestorId);
  try
    {
      toolbox::Event::Reference event(new toolbox::Event(commandName, this));
      fsmP_->fireEvent(event);
    }
  catch(toolbox::fsm::exception::Exception& err)
    {
      std::string const histMsg =
        toolbox::toString("Ignoring SOAP command '%s' requested by %s: "
                          "failed to execute the corresponding FSM transition.",
                          commandName.c_str(),
                          requestorId.asString().c_str());
      xdaqAppP_->appStateInfoSpace_.addHistoryItem(histMsg);
      std::string msgBase = histMsg;
      ERROR(toolbox::toString("%s: %s.",
                              msgBase.c_str(),
                              err));
      XCEPT_DECLARE_NESTED(tcds::exception::FSMTransitionProblem, top,
                           toolbox::toString("%s.", msgBase.c_str()), err);
      xdaqAppP_->notifyQualified("error", top);
      tcds::utils::soap::SOAPFaultCodeType faultCode = tcds::utils::soap::SOAPFaultCodeSender;
      std::string faultString = toolbox::toString("Failed to fire '%s' event.",
                                                  commandName.c_str());
      std::string faultDetail = toolbox::toString("%s: %s.",
                                                  msgBase.c_str(),
                                                  err.message().c_str());
      std::string faultActor = xdaqAppP_->getFullURL();
      xoap::MessageReference reply =
        tcds::utils::soap::makeSOAPFaultReply(xdaqAppP_,
                                              msg,
                                              faultCode,
                                              faultString,
                                              faultDetail);
      return reply;
    }

  //----------

  // Once we get here, the state transition has been triggered. Notify
  // the requestor that so far everything is fine. Any command 'X'
  // replies with 'XResponse'. Upon arrival in the next state (e.g.,
  // 'Configuring' for 'Configure') an asynchronous notification is
  // sent to RunControl with the name of the new state.
  std::string soapProtocolVersion = msg->getImplementationFactory()->getProtocolVersion();
  try
    {
      xoap::MessageReference reply =
        tcds::utils::soap::makeCommandSOAPReply(soapProtocolVersion,
                                                commandName,
                                                fsmP_->getStateName(fsmP_->getCurrentState()));
      return reply;
    }
  catch(xcept::Exception& err)
    {
      std::string msgBase =
        toolbox::toString("Failed to create FSM SOAP reply for command '%s'",
                          commandName.c_str());
      ERROR(toolbox::toString("%s: %s.",
                              msgBase.c_str(),
                              err));
      XCEPT_DECLARE_NESTED(tcds::exception::RuntimeProblem, top,
                           toolbox::toString("%s.", msgBase.c_str()), err);
      xdaqAppP_->notifyQualified("error", top);
      XCEPT_RETHROW(xoap::exception::Exception, msgBase, err);
    }

  //----------

  // Should only get here in case the above SOAP reply fails. In that
  // case: return an empty message.
  return xoap::createMessage();
}

void
tcds::utils::FSM::stateChangedWithNotification(toolbox::fsm::FiniteStateMachine& fsm)
{
  logStateChangeDetails();

  //----------

  std::string const stateName = getCurrentStateName();
  uint32_t const runNumber = xdaqAppP_->cfgInfoSpaceP_->getUInt32("runNumber");

  // Mark the transition in the application history.
  std::string histMsg = toolbox::toString("FSM state changed to '%s'.",
                                          stateName.c_str());
  if (stateName == "Enabled")
    {
      histMsg.append(toolbox::toString(" (run #%" PRIu32 ")", runNumber));
    }
  xdaqAppP_->appStateInfoSpace_.addHistoryItem(histMsg);

  xdaqAppP_->appStateInfoSpace_.setFSMState(stateName);
  notifyRCMS(stateName, "Normal state change.");

  //----------

  // Check if from this state an automatic 'forward' should be
  // performed
  std::map<std::string, std::string>::const_iterator const
    tmp = automaticTransitions_.find(stateName);
  if (tmp != automaticTransitions_.end())
    {
      std::string const commandName = tmp->second;
      DEBUG("'"
            << stateName
            << "' is an intermediate/auto-forward state --> forwarding to '"
            << commandName
            << "'");

      try
        {
          toolbox::Event::Reference event(new toolbox::Event(commandName, this));
          fsmP_->fireEvent(event);
        }
      catch(toolbox::fsm::exception::Exception& err)
        {
          std::string msgBase =
            toolbox::toString("Problem executing the FSM '%s' command",
                              commandName.c_str());
          ERROR(toolbox::toString("%s: %s.",
                                  msgBase.c_str(),
                                  err));
          XCEPT_DECLARE_NESTED(tcds::exception::FSMTransitionProblem, top,
                               toolbox::toString("%s.", msgBase.c_str()), err);
          xdaqAppP_->notifyQualified("error", top);
          XCEPT_RETHROW(xoap::exception::Exception, msgBase, err);
        }
    }
}

void
tcds::utils::FSM::stateChangedToFailedWithNotification(toolbox::fsm::FiniteStateMachine& fsm)
{
  logStateChangeDetails();

  // Mark the transition in the application history.
  std::string const msg = toolbox::toString("FSM state changed to '%s'.",
                                            getCurrentStateName().c_str());
  xdaqAppP_->appStateInfoSpace_.addHistoryItem(msg);

  ERROR("State has changed to 'Failed.'");

  // NOTE: Don't notify RCMS. This has already happened from
  // gotoFailed(), where more detailed failure information is
  // available.
}

void
tcds::utils::FSM::coldReset()
{
  toolbox::Event::Reference const event(new toolbox::Event("ColdReset", this));
  fsmP_->fireEvent(event);
}

void
tcds::utils::FSM::configure()
{
  toolbox::Event::Reference const event(new toolbox::Event("Configure", this));
  fsmP_->fireEvent(event);
}

void
tcds::utils::FSM::reconfigure()
{
  toolbox::Event::Reference const event(new toolbox::Event("Reconfigure", this));
  fsmP_->fireEvent(event);
}

void
tcds::utils::FSM::enable()
{
  toolbox::Event::Reference const event(new toolbox::Event("Enable", this));
  fsmP_->fireEvent(event);
}

void
tcds::utils::FSM::pause()
{
  toolbox::Event::Reference const event(new toolbox::Event("Pause", this));
  fsmP_->fireEvent(event);
}

void
tcds::utils::FSM::resume()
{
  toolbox::Event::Reference const event(new toolbox::Event("Resume", this));
  fsmP_->fireEvent(event);
}

void
tcds::utils::FSM::stop()
{
  toolbox::Event::Reference const event(new toolbox::Event("Stop", this));
  fsmP_->fireEvent(event);
}

void
tcds::utils::FSM::halt()
{
  toolbox::Event::Reference const event(new toolbox::Event("Halt", this));
  fsmP_->fireEvent(event);
}

void
tcds::utils::FSM::fail()
{
  toolbox::Event::Reference const event(new toolbox::Event("Fail", this));
  fsmP_->fireEvent(event);
}

std::string
tcds::utils::FSM::getCurrentStateName() const
{
  toolbox::fsm::State currentState = fsmP_->getCurrentState();
  std::string stateName = fsmP_->getStateName(currentState);
  return stateName;
}

void
tcds::utils::FSM::gotoFailed(std::string const& reason)
{
  xdaqAppP_->appStateInfoSpace_.setFSMState("Failed", reason);
  ERROR("Going to 'Failed' state. Reason: '" << reason << "'.");

  // Notify RCMS from here, since here we still have the detailed
  // failure information.
  std::string const className =
    xdaqAppP_->getApplicationDescriptor()->getClassName();
  std::string const serviceName =
    xdaqAppP_->getApplicationDescriptor()->getAttribute("service");
  std::string const rcmsMsg =
    toolbox::toString("%s for service '%s' failed: '%s'. More detail at: %s.",
                      className.c_str(),
                      serviceName.c_str(),
                      reason.c_str(),
                      xdaqAppP_->getFullURL().c_str());
  notifyRCMS(fsmP_->getStateName('F'), rcmsMsg);

  // Raise an exception so the FSM indeed goes to 'Failed.'
  XCEPT_RAISE(toolbox::fsm::exception::Exception, reason);
}

void
tcds::utils::FSM::gotoFailed(xcept::Exception& err)
{
  std::string reason = err.message();
  gotoFailed(reason);
}

void
tcds::utils::FSM::invalidStateTransitionAction(toolbox::Event::Reference event)
{
  toolbox::fsm::InvalidInputEvent& invalidInputEvent =
    dynamic_cast<toolbox::fsm::InvalidInputEvent&>(*event);
  std::string const fromState = fsmP_->getStateName(invalidInputEvent.getFromState());
  std::string const toState = invalidInputEvent.getInput();
  std::string const histMsg =
    toolbox::toString("Ignoring invalid state transition '%s' from state '%s'.",
                      toState.c_str(),
                      fromState.c_str());
  xdaqAppP_->appStateInfoSpace_.addHistoryItem(histMsg);
  std::string msgBase = histMsg;
  std::string const msg =
    toolbox::toString("%s I'm not deaf, "
                      "I'm just ignoring you.",
                      msgBase.c_str());
  ERROR(msg);
}

void
tcds::utils::FSM::notifyRCMS(std::string const& stateName, std::string const& msg)
{
  // Notify RCMS of a state change.
  // NOTE: Should only be used for state _changes_.

  // NOTE: An empty RCMS URL signifies a wish to skip such
  // notifications.
  xdata::Serializable const* const tmp = xdaqAppP_->getApplicationInfoSpace()->find("rcmsStateListener");
  xdata::Bag<xdaq2rc::ClassnameAndInstance> const* const bag = dynamic_cast<xdata::Bag<xdaq2rc::ClassnameAndInstance> const* const>(tmp);
  xdata::String const* const tmpStr = dynamic_cast<xdata::String const* const>(bag->getField("url"));
  std::string const url = *tmpStr;

  if (url.empty())
    {
      INFO("Not notifying RCMS of state change to '"
           << stateName
           << "' (since the RCMS URL was set to the empty string).");
    }
  else
    {
      INFO("Notifying RCMS of state change to '" << stateName << "'.");
      try
        {
          rcmsNotifier_.stateChanged(stateName, msg);
        }
      catch(xcept::Exception& err)
        {
          ERROR("Failed to notify RCMS of state change: "
                << xcept::stdformat_exception_history(err));
          XCEPT_DECLARE_NESTED(tcds::exception::RCMSNotificationError, top,
                               "Failed to notify RCMS of state change.", err);
          xdaqAppP_->notifyQualified("error", top);
        }
    }
}

void
tcds::utils::FSM::reset(std::string const& msg)
{
  fsmP_->reset();
  std::string stateName = fsmP_->getStateName(fsmP_->getCurrentState());
  xdaqAppP_->appStateInfoSpace_.setFSMState(stateName);
  notifyRCMS(getCurrentStateName(), msg);
}

void
tcds::utils::FSM::logStateChangeDetails() const
{
  std::string const className =
    xdaqAppP_->getApplicationDescriptor()->getClassName();
  std::string const serviceName =
    xdaqAppP_->getApplicationDescriptor()->getAttribute("service");
  std::string const stateName = getCurrentStateName();
  uint32_t const runNumber = xdaqAppP_->cfgInfoSpaceP_->getUInt32("runNumber");

  INFO("State for " << className << "' " << serviceName << "' has changed."
       << " Current state is now '" << stateName << "'.");
  INFO("RunControl session in charge: '"
       << xdaqAppP_->appStateInfoSpace_.getUInt32("rcmsSessionId")
       << "'.");
  INFO("Hardware lease owner: '"
       << xdaqAppP_->appStateInfoSpace_.getString("hwLeaseOwnerId")
       << "'.");
  INFO("Run number: " << runNumber << ".");
}
