#include "tcds/utils/FSMDriver.h"

#include <unistd.h>

#include "toolbox/TimeInterval.h"
#include "toolbox/TimeVal.h"
#include "toolbox/string.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/task/exception/Exception.h"
#include "xcept/Exception.h"
#include "xdaq/ApplicationDescriptor.h"

#include "tcds/exception/Exception.h"
#include "tcds/utils/XDAQAppWithFSMAutomatic.h"

tcds::utils::FSMDriver::FSMDriver(XDAQAppWithFSMAutomatic* const xdaqApp) :
  fsmDriverAlarmName_("FSMDriverAlarm"),
  xdaqAppP_(xdaqApp)
{
  timerName_ = toolbox::toString("FSMDriverTimer_lid%d",
                                 xdaqAppP_->getApplicationDescriptor()->getLocalId());
}

tcds::utils::FSMDriver::~FSMDriver()
{
  stop();
  try
    {
      toolbox::task::getTimerFactory()->removeTimer(timerName_);
    }
  catch (...)
    {
    }
  timerP_.reset();
}

void
tcds::utils::FSMDriver::start()
{
  // If we get here and there is no timer (yet), we have to create it
  // first.
  if (!timerP_.get())
    {
      try
        {
          timerP_ = std::unique_ptr<toolbox::task::Timer>(toolbox::task::getTimerFactory()->createTimer(timerName_));
          timerP_->addExceptionListener(this);
        }
      catch (toolbox::task::exception::Exception const& err)
        {
          // Raise the alarm.
          std::string const problemDesc =
            toolbox::toString("Failed to create FSMDriver timer: '%s'.", err.what());
          XCEPT_DECLARE(tcds::exception::FSMDriverFailureAlarm, alarmException, problemDesc);
          xdaqAppP_->raiseAlarm(fsmDriverAlarmName_, alarmException);
        }
    }

  //----------

  toolbox::TimeInterval interval(kCheckInterval_, 0);
  toolbox::TimeVal start = toolbox::TimeVal::gettimeofday();
  try
    {
      timerP_->scheduleAtFixedRate(start, this, interval, 0, "");
    }
  catch (toolbox::task::exception::Exception const& err)
    {
      // Raise the alarm.
      std::string const problemDesc =
        toolbox::toString("Failed to start FSMDriver timer: '%s'.", err.what());
      XCEPT_DECLARE(tcds::exception::FSMDriverFailureAlarm, alarmException, problemDesc);
      xdaqAppP_->raiseAlarm(fsmDriverAlarmName_, alarmException);
    }
}

void
tcds::utils::FSMDriver::stop()
{
  if (timerP_.get() && timerP_->isActive())
    {
      timerP_->stop();
    }
}

void
tcds::utils::FSMDriver::timeExpired(toolbox::task::TimerEvent& e)
{
  std::string const stateName = xdaqAppP_->getCurrentStateName();
  // If the state name is 'Enabled', we're happy.
  if (stateName != "Enabled")
    {
      // If not, we want to take action. The action to be taken
      // depends on the current state.

      // NOTE: Just a little grace period here to make sure things
      // don't fly into an infinite loop.
      ::sleep(kGracePeriod_);

      if (stateName == "Failed")
        {
          xdaqAppP_->halt();
        }
      else if (stateName == "Halted")
        {
          xdaqAppP_->configure();
        }
      else if (stateName == "Configured")
        {
          xdaqAppP_->enable();
        }
    }
}

void
tcds::utils::FSMDriver::onException(xcept::Exception& err)
{
  // Raise the alarm.
  std::string const problemDesc =
    toolbox::toString("Problem in FSMDriver: '%s'.", err.what());
  XCEPT_DECLARE(tcds::exception::FSMDriverFailureAlarm, alarmException, problemDesc);
  xdaqAppP_->raiseAlarm(fsmDriverAlarmName_, alarmException);
}
