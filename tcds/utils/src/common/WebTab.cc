#include "tcds/utils/WebTab.h"

#include <cmath>
#include <sstream>

#include "tcds/utils/WebObject.h"

tcds::utils::WebTab::WebTab(std::string const& name,
                            std::string const& description,
                            size_t const numColumns) :
  name_(name),
  description_(description),
  numColumns_(numColumns)
{
}

tcds::utils::WebTab::~WebTab()
{
  std::vector<WebObject*>::iterator iter;
  for (iter = webObjects_.begin(); iter != webObjects_.end(); ++iter)
    {
      if (*iter != 0)
        {
          delete *iter;
        }
    }
}

std::string
tcds::utils::WebTab::getName() const
{
  return name_;
}

std::string
tcds::utils::WebTab::getDescription() const
{
  return description_;
}

size_t
tcds::utils::WebTab::getNumColumns() const
{
  return numColumns_;
}

std::string
tcds::utils::WebTab::getHTMLString() const
{
  // NOTE: This has been written to work with the new XDAQ12-style
  // HyperDAQ tabs and doT.js.

  std::stringstream res;

  // An overall <div> encapsulating a single tab.
  res << "<div class=\"tcds-tab-content\">";
  res << "\n";

  res << "<div class=\"tcds-tab-name-wrapper\">";
  res << "\n";
  res << "<p class=\"tcds-tab-name\">"
      << name_
      << "</p>";
  res << "\n";

  res << "<p class=\"tcds-tab-description\">"
      << description_
      << "</p>";
  res << "\n";
  res << "</div>";
  res << "\n";

  //----------

  // First the encapsulating div containing all the individual HTML
  // tables.
  res << "<table class=\"tcds-tab-table\">";
  res << "\n";
  res << "<tbody>";
  res << "\n";

  size_t iCol = 0;
  std::vector<WebObject*>::const_iterator iter;
  for (iter = webObjects_.begin(); iter != webObjects_.end(); ++iter)
    {
      if (iCol == 0)
        {
          res << "<tr>";
          res << "\n";
        }
      // NOTE: Use style in order to avoid the validator's complaint
      // that 'The width attribute on the td element is obsolete. Use
      // CSS instead.'
      res << "<td colspan=\"" << (*iter)->getColSpan() <<  "\" "
          << "style=\"width: " << int(std::floor(100. * (*iter)->getColSpan() / numColumns_)) << "%;\">";
      res << "\n";
      res << (*iter)->getHTMLString();
      res << "\n";
      res << "</td>";
      res << "\n";

      iCol += (*iter)->getColSpan();
      if (iCol >= numColumns_)
        {
          res << "</tr>";
          res << "\n";
          iCol = 0;
        }
    }
  if (iCol != 0)
    {
      res << "</tr>";
      res << "\n";
    }

  // Close the encapsulating table for this tab.
  res << "</tbody>";
  res << "\n";
  res << "</table>";
  res << "\n";

  //----------

  // Closing of the overall <div>.
  res << "</div>";

  return res.str();
}

std::string
tcds::utils::WebTab::getJSONString() const
{
  std::stringstream res;

  std::vector<WebObject*>::const_iterator iter;
  for (iter = webObjects_.begin(); iter != webObjects_.end(); ++iter)
    {
      std::string jsonTmp = (*iter)->getJSONString();
      if (!jsonTmp.empty())
        {
          if (!res.str().empty())
            {
              res << ",\n";
            }
          res << jsonTmp;
        }
    }

  return res.str();
}

void
tcds::utils::WebTab::addWebObject(std::unique_ptr<WebObject> object)
{
  // NOTE: This is a bit nasty. We want to take ownership, so the
  // unique_ptr makes sense. But we can't put those into STL
  // containers...
  webObjects_.push_back(object.get());
  object.release();
}
