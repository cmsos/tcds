#include "tcds/utils/DIPUtils.h"

#include "toolbox/string.h"
#include "xcept/Exception.h"
#include "xoap/MessageFactory.h"
#include "xoap/filter/MessageFilter.h"
#include "xoap/SOAPElement.h"
#include "xoap/SOAPEnvelope.h"

#include "tcds/exception/Exception.h"
#include "tcds/utils/DIPDP.h"
#include "tcds/utils/SOAPUtils.h"

std::map<std::pair<bool, bool>, tcds::utils::DIP_QUAL>
tcds::utils::dipQualMap()
{
  // Map from WinCC OA (userbit1, userbit2) to DIP quality.
  std::map<std::pair<bool, bool> , tcds::utils::DIP_QUAL> res;

  res.insert(std::make_pair(std::make_pair(false, true), tcds::utils::DIP_QUAL_UNINITIALIZED));
  res.insert(std::make_pair(std::make_pair(false, false), tcds::utils::DIP_QUAL_BAD));
  res.insert(std::make_pair(std::make_pair(true, true), tcds::utils::DIP_QUAL_GOOD));
  res.insert(std::make_pair(std::make_pair(true, false), tcds::utils::DIP_QUAL_UNCERTAIN));

  return res;
}

tcds::utils::DIP_QUAL
tcds::utils::dipQualFromUserBits(bool const userBit1, bool const userBit2)
{
  return dipQualMap()[std::make_pair(userBit1, userBit2)];
}

std::pair<bool, bool>
tcds::utils::userBitsFromDipQual(DIP_QUAL const dipQual)
{
  // NOTE: The following is ugly, and relies on the map covering all
  // possibilities...

  std::map<std::pair<bool, bool>, tcds::utils::DIP_QUAL> map =
    tcds::utils::dipQualMap();

  std::map<std::pair<bool, bool>, tcds::utils::DIP_QUAL>::const_iterator i;
  for (i = map.begin(); i != map.end(); ++i)
    {
      if (i->second == dipQual)
        {
          break;
        }
    }

  return i->first;
}

std::map<tcds::utils::DIP_QUAL, std::string>
tcds::utils::dipQualStringMap()
{
  std::map<tcds::utils::DIP_QUAL, std::string> res;

  res.insert(std::make_pair(tcds::utils::DIP_QUAL_UNINITIALIZED, "UNINITIALIZED"));
  res.insert(std::make_pair(tcds::utils::DIP_QUAL_BAD, "BAD"));
  res.insert(std::make_pair(tcds::utils::DIP_QUAL_GOOD, "GOOD"));
  res.insert(std::make_pair(tcds::utils::DIP_QUAL_UNCERTAIN, "UNCERTAIN"));

  return res;
}

std::string
tcds::utils::dipQualToString(tcds::utils::DIP_QUAL const dipQual)
{
  return dipQualStringMap()[dipQual];
}

xoap::MessageReference
tcds::utils::makePSXDPGetSOAPCmd(std::string const& dpName)
{
  std::string const PSX_NS_URI = "http://xdaq.cern.ch/xdaq/xsd/2006/psx-pvss-10.xsd";

  xoap::MessageReference msg = xoap::createMessage();
  xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();
  envelope.addNamespaceDeclaration("xsi",
                                   "http://www.w3.org/2001/XMLSchema-instance");
  envelope.addNamespaceDeclaration("xsd",
                                   "http://www.w3.org/2001/XMLSchema");
  envelope.addNamespaceDeclaration("soapenc",
                                   "http://schemas.xmlsoap.org/soap/encoding/");
  xoap::SOAPBody body = envelope.getBody();
  xoap::SOAPName commandName = envelope.createName("dpGet",
                                                   "psx",
                                                   PSX_NS_URI);
  xoap::SOAPElement commandElement = body.addBodyElement(commandName);

  // The main datapoint.
  xoap::SOAPName dataPointName = envelope.createName("dp",
                                                     "psx",
                                                     PSX_NS_URI);
  xoap::SOAPElement dataPointElement = commandElement.addChildElement(dataPointName);
  xoap::SOAPName nameAttribute = envelope.createName("name",
                                                     "",
                                                     "");
  // NOTE: Read _from_ '_online'.
  dataPointElement.addAttribute(nameAttribute, dpName + ":_online.._value");

  // The (first two) user-bits encoding the DIP quality.
  // See also: https://readthedocs.web.cern.ch/display/ICKB/DIP+FAQ#DIPFAQ-HowistheDIPqualityinformationmappedtoWinCCOAuserandinvalidbitsinWinCCOA?
  // - User-bit 1.
  xoap::SOAPName dataPointUserBit1Name = envelope.createName("dp",
                                                             "psx",
                                                             PSX_NS_URI);
  xoap::SOAPElement dataPointUserBit1Element = commandElement.addChildElement(dataPointUserBit1Name);
  xoap::SOAPName nameUserBit1Attribute = envelope.createName("name",
                                                             "",
                                                             "");
  dataPointUserBit1Element.addAttribute(nameUserBit1Attribute, dpName + ":_online.._userbit1");
  // - User-bit 2.
  xoap::SOAPName dataPointUserBit2Name = envelope.createName("dp",
                                                             "psx",
                                                             PSX_NS_URI);
  xoap::SOAPElement dataPointUserBit2Element = commandElement.addChildElement(dataPointUserBit2Name);
  xoap::SOAPName nameUserBit2Attribute = envelope.createName("name",
                                                             "",
                                                             "");
  dataPointUserBit2Element.addAttribute(nameUserBit2Attribute, dpName + ":_online.._userbit2");

  // The 'invalid' bit from PVSS.
  xoap::SOAPName dataPointInvalidFlagName = envelope.createName("dp",
                                                                "psx",
                                                                PSX_NS_URI);
  xoap::SOAPElement dataPointInvalidFlagElement = commandElement.addChildElement(dataPointInvalidFlagName);
  xoap::SOAPName nameInvalidFlagAttribute = envelope.createName("name",
                                                                "",
                                                                "");
  dataPointInvalidFlagElement.addAttribute(nameInvalidFlagAttribute, dpName + ":_online.._invalid");

  return msg;
}

xoap::MessageReference
tcds::utils::makePSXDPSetSOAPCmd(std::string const& dpName,
                                 std::string const& dpValue,
                                 tcds::utils::DIP_QUAL const dpQual)
{
  return makePSXDPSetSOAPCmd(std::vector<tcds::utils::DIPDP>{tcds::utils::DIPDP(dpName, dpValue, dpQual)});
}

xoap::MessageReference
tcds::utils::makePSXDPSetSOAPCmd(std::vector<tcds::utils::DIPDP> const& dpInfo)
{
  // NOTE: With the WinCC OA -> DIP bridge in between, we can only
  // publish 'good' and 'bad' DIP qualities. So this method converts
  // anything that is not 'good' to 'bad'.

  std::string const PSX_NS_URI = "http://xdaq.cern.ch/xdaq/xsd/2006/psx-pvss-10.xsd";

  xoap::MessageReference msg = xoap::createMessage();
  xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();
  envelope.addNamespaceDeclaration("xsi",
                                   "http://www.w3.org/2001/XMLSchema-instance");
  envelope.addNamespaceDeclaration("xsd",
                                   "http://www.w3.org/2001/XMLSchema");
  envelope.addNamespaceDeclaration("soapenc",
                                   "http://schemas.xmlsoap.org/soap/encoding/");
  xoap::SOAPBody body = envelope.getBody();
  xoap::SOAPName commandName = envelope.createName("dpSet",
                                                   "psx",
                                                   PSX_NS_URI);
  xoap::SOAPElement commandElement = body.addBodyElement(commandName);

  for (std::vector<tcds::utils::DIPDP>::const_iterator i = dpInfo.begin();
       i != dpInfo.end();
       ++i)
    {
      std::string const dpName = i->dpName();
      std::string const dpValue = i->dpValue();
      tcds::utils::DIP_QUAL const dpQual = i->dpQual();

      // The main datapoint.
      xoap::SOAPName dataPointName = envelope.createName("dp",
                                                         "psx",
                                                         PSX_NS_URI);
      xoap::SOAPElement dataPointElement = commandElement.addChildElement(dataPointName);
      xoap::SOAPName nameAttribute = envelope.createName("name",
                                                         "",
                                                         "");
      // NOTE: Publish _to_ '_original'.
      dataPointElement.addAttribute(nameAttribute, dpName + ":_original.._value");
      dataPointElement.addTextNode(dpValue);

      // The _exp_inv flag to indicate the publication quality.
      xoap::SOAPName dataPointExpInvName = envelope.createName("dp",
                                                               "psx",
                                                               PSX_NS_URI);
      xoap::SOAPElement dataPointExpInvElement = commandElement.addChildElement(dataPointExpInvName);
      xoap::SOAPName nameExpInvAttribute = envelope.createName("name",
                                                               "",
                                                               "");
      dataPointExpInvElement.addAttribute(nameExpInvAttribute, dpName + ":_original.._exp_inv");
      dataPointExpInvElement.addTextNode(dpQual == tcds::utils::DIP_QUAL_GOOD ? "false" : "true");
    }

  return msg;
}

std::string
tcds::utils::extractPSXDPGetReply(xoap::MessageReference& msg,
                                  std::string const& dpName)
{
  xoap::filter::MessageFilter filter("//psx:dp[@name='" + dpName + "']");
  if (!filter.match(msg))
    {
      std::string const err =
        toolbox::toString("Could not find parameter '%s' in PSX dpGet SOAP reply.",
                          dpName.c_str());
      XCEPT_RAISE(tcds::exception::SOAPFormatProblem, err);
    }
  std::list<xoap::SOAPElement> elements = filter.extract(msg);
  if (elements.size() == 0)
    {
      std::string const err =
        toolbox::toString("Could not find parameter '%s' in PSX dpGet SOAP reply.",
                          dpName.c_str());
      XCEPT_RAISE(tcds::exception::SOAPFormatProblem, err);
    }
  else if (elements.size() > 1)
    {
      std::string const err =
        toolbox::toString("Found %d replies (instead of 1) for parameter '%s' in PSX dpGet SOAP reply.",
                          elements.size(),
                          dpName.c_str());
      XCEPT_RAISE(tcds::exception::SOAPFormatProblem, err);
    }

  std::string const dpVal = elements.front().getTextContent();
  return dpVal;
}

std::string
tcds::utils::extractPSXDPSetReply(xoap::MessageReference& msg,
                                  std::string const& dpName)
{
  // xoap::filter::MessageFilter filter("//psx:dp[@name='" + dpName + "']");
  // if (!filter.match(msg))
  //   {
  //     std::string const err =
  //       toolbox::toString("Could not find parameter '%s' in PSX dpGet SOAP reply.",
  //                         dpName.c_str());
  //     XCEPT_RAISE(tcds::exception::SOAPFormatProblem, err);
  //   }
  // std::list<xoap::SOAPElement> elements = filter.extract(msg);
  // if (elements.size() == 0)
  //   {
  //     std::string const err =
  //       toolbox::toString("Could not find parameter '%s' in PSX dpGet SOAP reply.",
  //                         dpName.c_str());
  //     XCEPT_RAISE(tcds::exception::SOAPFormatProblem, err);
  //   }
  // else if (elements.size() > 1)
  //   {
  //     std::string const err =
  //       toolbox::toString("Found %d replies (instead of 1) for parameter '%s' in PSX dpGet SOAP reply.",
  //                         elements.size(),
  //                         dpName.c_str());
  //     XCEPT_RAISE(tcds::exception::SOAPFormatProblem, err);
  //   }

  // std::string const dpVal = elements.front().getTextContent();
  // return dpVal;
  return "";
}
