#include "tcds/utils/InfoSpaceUpdaterPlain.h"

#include "tcds/utils/InfoSpaceUpdater.h"

tcds::utils::InfoSpaceUpdaterPlain::InfoSpaceUpdaterPlain(xdaq::Application& xdaqApp) :
  tcds::utils::InfoSpaceUpdater(xdaqApp)
{
}

tcds::utils::InfoSpaceUpdaterPlain::~InfoSpaceUpdaterPlain()
{
}
