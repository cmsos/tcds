#include "tcds/utils/ApplicationStateInfoSpaceHandler.h"

#include "log4cplus/loggingmacros.h"

#include <stdint.h>
#include <inttypes.h>
#include <algorithm>

#include "toolbox/string.h"
#include "xdaq/Application.h"
#include "xdaq/ApplicationDescriptor.h"

#include "tcds/utils/InfoSpaceHandlerLocker.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/OwnerId.h"
#include "tcds/utils/Utils.h"
#include "tcds/utils/WebServer.h"
#include "tcds/utils/WebTableAppHist.h"

std::string const tcds::utils::ApplicationStateInfoSpaceHandler::kAllOKString = "All OK";
std::string const tcds::utils::ApplicationStateInfoSpaceHandler::kNoProblemString = "-";

tcds::utils::ApplicationStateInfoSpaceHandler::ApplicationStateInfoSpaceHandler(xdaq::Application& xdaqApp,
                                                                                tcds::utils::InfoSpaceUpdater* updater) :
  tcds::utils::InfoSpaceHandler(xdaqApp, "tcds-application-state", updater, xdaqApp.getApplicationInfoSpace())
{
  // NOTE: In order for RunControl to work, each application needs to
  // have a 'stateName' string in the default application
  // InfoSpace. Therefore this InfoSpaceHandler uses the built-in
  // 'mirroring' capabilities of the InfoSpaceHandler to copy its
  // contents into the default application InfoSpace.

  // These are _real_ application state variables.
  createString("stateName",
               tcds::utils::InfoSpaceHandler::kUninitializedString,
               "",
               InfoSpaceItem::NOUPDATE,
               true);
  createString("applicationState",
               tcds::utils::InfoSpaceHandler::kUninitializedString,
               "",
               InfoSpaceItem::NOUPDATE,
               true);
  createString("problemDescription",
               tcds::utils::InfoSpaceHandler::kUninitializedString,
               "",
               InfoSpaceItem::NOUPDATE,
               true);

  // The session ID of the RunControl session in charge of this
  // application. In the 'normal' case, in which there is a RunControl
  // session driving the current run, this also defines the hardware
  // lease owner. In 'special' cases where there is something else
  // driving the run, the rcmsSessionId should be set to zero, and the
  // hwLeaseOwnerId should be set to something adequately descriptive.
  createUInt32("rcmsSessionId", 0, "", InfoSpaceItem::NOUPDATE, true);

  // A string identifying the remote SOAP sender that owns the
  // hardware lease.
  // NOTE: This may be a RunControl session, or something else.
  createString("hwLeaseOwnerId", "", "", InfoSpaceItem::NOUPDATE, true);

  // A flag showing if we're in maintenance mode or in normal
  // operations mode.
  createBool("maintenanceMode", false, "", InfoSpaceItem::NOUPDATE, true);

  // Some time-keeping variables (for monitoring only, really).
  createDouble("upTime", 0., "time_interval", InfoSpaceItem::NOUPDATE, true);
  createTimeVal("latestMonitoringUpdate", 0., "", InfoSpaceItem::NOUPDATE, true);
  createDouble("latestMonitoringUpdateDouble", 0., "%.6f", InfoSpaceItem::NOUPDATE, true);
  createDouble("latestMonitoringDuration", 0., "%.3f", InfoSpaceItem::NOUPDATE, true);

  // Keep track of what happened.
  createString("history",
               tcds::utils::InfoSpaceHandler::kUninitializedString,
               "",
               InfoSpaceItem::NOUPDATE,
               true);

  //----------

  // Update the session ID attached to the application descriptor.
  // NOTE: The main reason this is done here already is that (the
  // first time around) this also _creates_ the property (if it does
  // not exist yet).
  updateSessionIdInApplicationDescriptor();
}

tcds::utils::ApplicationStateInfoSpaceHandler::~ApplicationStateInfoSpaceHandler()
{
}

void
tcds::utils::ApplicationStateInfoSpaceHandler::initialize()
{
  InfoSpaceHandlerLocker(this);
  problems_.clear();
  updateStatusDescription();
}

std::string
tcds::utils::ApplicationStateInfoSpaceHandler::formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const
{
  std::string res = tcds::utils::escapeAsJSONString(tcds::utils::InfoSpaceHandler::kInvalidItemString);
  if (item->isValid())
    {
      std::string name = item->name();
      if (name == "history")
        {
          // Special in the sense that this is something that needs to
          // be interpreted as a proper JavaScript object, so let's
          // not add any more double quotes.
          res = getString(name);
        }
      else if (name == "maintenanceMode")
        {
          bool const value = getBool(name);
          if (value)
            {
              res = "Maintenance mode";
            }
          else
            {
              res = "Operations mode";
            }
          res = tcds::utils::escapeAsJSONString(res);
        }
      else
        {
          // For everything else simply call the generic formatter.
          res = InfoSpaceHandler::formatItem(item);
        }
    }
  return res;
}

void
tcds::utils::ApplicationStateInfoSpaceHandler::addProblem(std::string const& keyword,
                                                          std::string const& problemDescription)
{
  InfoSpaceHandlerLocker(this);
  problems_[keyword] = problemDescription;
  updateStatusDescription();
  addHistoryItem(problemDescription);
}

void
tcds::utils::ApplicationStateInfoSpaceHandler::removeProblem(std::string const& keyword)
{
  InfoSpaceHandlerLocker(this);
  problems_.erase(keyword);
  updateStatusDescription();
}

void
tcds::utils::ApplicationStateInfoSpaceHandler::addHistoryItem(std::string const& desc)
{
  LOG4CPLUS_INFO(getOwnerApplication().getApplicationLogger(), desc);

  InfoSpaceHandlerLocker(this);
  // Keep track of the underlying history items first.
  history_.addItem(desc);

  // Update the history string used for the table.
  std::string const histString = history_.getJSONString();
  setString("history", histString);
  writeInfoSpace();
}

void
tcds::utils::ApplicationStateInfoSpaceHandler::setFSMState(std::string const& stateDescription,
                                                           std::string const& problemDescription)
{
  std::string const keyword = "FSM transition problem";
  InfoSpaceHandlerLocker(this);
  setString("stateName", stateDescription);
  if (problemDescription != kNoProblemString)
    {
      addProblem(keyword, problemDescription);
    }
  else
    {
      removeProblem(keyword);
    }
}

void
tcds::utils::ApplicationStateInfoSpaceHandler::setOwnerId(tcds::utils::OwnerId const& ownerId)
{
  setUInt32("rcmsSessionId", ownerId.rcmsSessionId());
  setString("hwLeaseOwnerId", ownerId.sessionName());
  updateSessionIdInApplicationDescriptor();
}

void
tcds::utils::ApplicationStateInfoSpaceHandler::addMonitoringProblem(std::string const& problemDescription)
{
  std::string const keyword = "Monitoring problem";
  addProblem(keyword, problemDescription);
}

void
tcds::utils::ApplicationStateInfoSpaceHandler::removeMonitoringProblem()
{
  std::string const keyword = "Monitoring problem";
  removeProblem(keyword);
}

void
tcds::utils::ApplicationStateInfoSpaceHandler::setApplicationState(std::string const& stateDescription)
{
  setString("applicationState", stateDescription);
}

void
tcds::utils::ApplicationStateInfoSpaceHandler::setProblemDescription(std::string const& problemDescription)
{
  setString("problemDescription", problemDescription);
}

void
tcds::utils::ApplicationStateInfoSpaceHandler::updateStatusDescription()
{
  if (problems_.size() == 0)
    {
      setApplicationState(kAllOKString);
      setProblemDescription(kNoProblemString);
    }
  else
    {
      std::string tmp = "";
      std::string problemList = "";
      for (std::map<std::string, std::string>::const_iterator i = problems_.begin();
           i != problems_.end();
           ++i)
        {
          if (!tmp.empty())
            {
              tmp += ", ";
              problemList += "\n";
            }
          tmp += i->first;
          problemList += i->first;
          problemList += ":\n  ";
          problemList += i->second;
        }

      std::string problemSummary;
      if (problems_.size() == 1)
        {
          problemSummary =
            toolbox::toString("Detected a problem: %s", tmp.c_str());
        }
      else
        {
          problemSummary =
            toolbox::toString("Detected %d problems: %s.", problems_.size(), tmp.c_str());
        }

      setApplicationState(problemSummary);
      setProblemDescription(problemList);
    }
  writeInfoSpace();
}

void
tcds::utils::ApplicationStateInfoSpaceHandler::updateSessionIdInApplicationDescriptor() const
{
  // NOTE: The reason this business is ugly is two-fold:
  // - The RCMS and XDAQ developers did not agree on the session ID
  //   type. In RCMS it is an unsigned integer, in XDAQ a string.
  // - XDAQ is not equipped to handle dynamic session IDs. The only
  //   normal way to assign a session ID is via the command line upon
  //   executive startup. The TCDS control applications run as
  //   services and get dynamically assigned to RCMS sessions through
  //   SOAP command parameters.

  // NOTE: Because the RCMS session ID for TCDS applications can be
  // assigned per application (as opposed to per executive), we cannot
  // use the built-in session ID in the application context.

  // NOTE: Because internally the TCDS control software uses the
  // original RunControl convention of a numeric session ID, the 'no
  // session in control' case is covered by a zero value. For the
  // string representation this has to be translated to an empty
  // string.

  uint32_t const sessionUInt = getUInt32("rcmsSessionId");
  std::string const sessionStr = (sessionUInt == 0) ? "" : toolbox::toString("%" PRIu32, sessionUInt);
  // std::cout << "DEBUG JGH ABCDE sessionUInt = " << sessionUInt << " -> '" << sessionStr << "'" << std::endl;
  getOwnerApplication().editApplicationDescriptor()->setAttribute("session", sessionStr);
}

void
tcds::utils::ApplicationStateInfoSpaceHandler::registerItemSetsWithMonitor(Monitor& monitor)
{
  std::string itemSetName = "Application state";
  monitor.newItemSet(itemSetName);
  monitor.addItem(itemSetName,
                  "stateName",
                  "Application FSM state",
                  this);
  monitor.addItem(itemSetName,
                  "applicationState",
                  "Application status",
                  this,
                  "The current status of the application. Should be 'All OK' if all is fine.");
  monitor.addItem(itemSetName,
                  "problemDescription",
                  "Problem description",
                  this,
                  "Description of any current problems experienced by the application, or '-' if no problems are observed.");

  monitor.addItem(itemSetName,
                  "rcmsSessionId",
                  "RCMS session identifier of the RunControl session in charge",
                  this,
                  "The identifier of the RunControl session controlling this application. Zero for non-RunControl sessions.");
  monitor.addItem(itemSetName,
                  "hwLeaseOwnerId",
                  "Identifier of RunControl or other session in charge",
                  this,
                  "The identifier of the RunControl or other control session controlling this application. Only SOAP commands from this source will be accepted.");

  monitor.addItem(itemSetName,
                  "maintenanceMode",
                  "Application mode",
                  this,
                  "In 'operations mode' the application is ready for data-taking operations. In 'maintenance mode' the application behaves differently. The latter mode is to be used only for tests and measurements on the TCDS system.");

  monitor.addItem(itemSetName,
                  "upTime",
                  "Uptime",
                  this,
                  "The total time that has passed since the start of this application.");
  monitor.addItem(itemSetName,
                  "latestMonitoringUpdate",
                  "Latest monitoring update time",
                  this,
                  "The timestamp of the end of the most recent round of monitoring updates.");
  monitor.addItem(itemSetName,
                  "latestMonitoringUpdateDouble",
                  "Latest monitoring update timestamp",
                  this,
                  "The same as the above, but formatted differently.");
  monitor.addItem(itemSetName,
                  "latestMonitoringDuration",
                  "Latest monitoring update duration (s)",
                  this,
                  "The time taken by the control application in the most recent round of monitoring updates. NOTE: This is unrelated to the update rate itself.");

  itemSetName = "itemset-application-status-history";
  monitor.newItemSet(itemSetName);
  monitor.addItem(itemSetName,
                  "history",
                  "Application status history",
                  this);
}

void
tcds::utils::ApplicationStateInfoSpaceHandler::registerItemSetsWithWebServer(WebServer& webServer,
                                                                             Monitor& monitor,
                                                                             std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "Application status" : forceTabName;

  webServer.registerTab(tabName,
                        "Application information",
                        1);
  webServer.registerTable("Application state",
                          "Info on the state of the XDAQ application",
                          monitor,
                          "Application state",
                          tabName);
  webServer.registerWebObject<WebTableAppHist>("History",
                                               "Application status and command history",
                                               monitor,
                                               "itemset-application-status-history",
                                               tabName);
}
