#include "tcds/utils/Layout.h"

#include <cstddef>

#include "xdaq/Application.h"
#include "xdaq/ApplicationDescriptor.h"
#include "xgi/Output.h"
#include "xgi/framework/UIManager.h"

#include "tcds/utils/version.h"

tcds::utils::Layout::Layout()
{
}

tcds::utils::Layout::~Layout()
{
}

void
tcds::utils::Layout::getHTMLHeader(xgi::framework::UIManager* manager,
                                   xgi::Input* in,
                                   xgi::Output* out)
{
  // NOTE: The approach of this method is not super-nice. It calls the
  // 'default' HyperDAQ HTML header method, and then modifies the
  // output.
  // The advantage is that we get our desired behaviour, and whenever
  // the 'default' HyperDAQ behaviour changes, we get that too.

  xgi::Output tmpOut;
  tmpOut.str("");
  tmpOut.setHTTPResponseHeader(out->getHTTPResponseHeader());

  // First get the 'default' HyperDAQ layout.
  hyperdaq::framework::Layout::getHTMLHeader(manager, in, &tmpOut);

  // Then tweak things a bit by inserting our stuff.

  std::string insertStr = "";
  // We're going to add some JavaScript.
  // NOTE: These _have to be_ deferred, because we only want to affect
  // TCDS applications (and not all other XDAQ applications as well),
  // and the application type we can only determine after the DOM has
  // been loaded.
  insertStr += buildJSLink("/tcds/utils/html/js/tcds_init.js", JS_SCRIPT_TYPE_DEFER);
  insertStr += "\n";
  insertStr += buildJSLink("/tcds/utils/html/js/tcds_loader.js", JS_SCRIPT_TYPE_DEFER);
  insertStr += "\n";
  insertStr += buildJSLink("/tcds/utils/html/js/tcds.js", JS_SCRIPT_TYPE_DEFER);
  insertStr += "\n";

  // We're going to insert all this just at the end of the <head>.
  std::string layoutStr = tmpOut.str();
  size_t pos = layoutStr.find("</head>");
  if (pos == std::string::npos)
    {
      pos = layoutStr.size();
    }
  layoutStr.insert(pos, insertStr);

  //----------

  // HACK HACK HACK
  // The HTML body generated by the HyperDAQ framework does contain
  // data-app-name and data-app-instance attributes, but no
  // data-app-service. So we add that ourselves.
  // See also: https://gitlab.cern.ch/cmsos/core/issues/102.
  pos = layoutStr.find("<body");
  pos = layoutStr.find("data-app-name", pos + 1);
  if (pos == std::string::npos)
    {
      pos = layoutStr.size();
    }
  std::string const service =
    manager->getApplication()->getApplicationDescriptor()->getAttribute("service");
  insertStr = "data-app-service=\"" + service + "\" ";
  layoutStr.insert(pos, insertStr);
  // HACK HACK HACK end

  //----------

  // Finally: return our 'fixed' result.
  out->setHTTPResponseHeader(tmpOut.getHTTPResponseHeader());
  *out << layoutStr;
}

void
tcds::utils::Layout::getHTMLFooter(xgi::framework::UIManager* manager,
                                   xgi::Input* in,
                                   xgi::Output* out)
{
  // NOTE: Similar approach as for the header. See above for details.
  xgi::Output tmpOut;
  tmpOut.str("");
  tmpOut.setHTTPResponseHeader(out->getHTTPResponseHeader());

  hyperdaq::framework::Layout::getHTMLFooter(manager, in, &tmpOut);

  std::string insertStr = "";
  insertStr += " - TCDS software version ";
  // NOTE: All TCDS software packages should have the same version
  // numbers, all the time.
  insertStr += PACKAGE_VERSION_STRING(TCDS_UTILS_VERSION_MAJOR,
                                      TCDS_UTILS_VERSION_MINOR,
                                      TCDS_UTILS_VERSION_PATCH);
  insertStr += " ";

  std::string layoutStr = tmpOut.str();
  size_t pos = layoutStr.find("- Copyright");
  if (pos == std::string::npos)
    {
      pos = layoutStr.size();
    }
  layoutStr.insert(pos, insertStr);

  out->setHTTPResponseHeader(tmpOut.getHTTPResponseHeader());
  *out << layoutStr;
}

std::string
tcds::utils::Layout::buildJSLink(std::string const& jsFile,
                                 JS_SCRIPT_TYPE const type,
                                 std::string const& pars) const
{
  std::string typeStr = "";
  if (type == JS_SCRIPT_TYPE_ASYNC)
    {
      typeStr = "async ";
    }
  else if (type == JS_SCRIPT_TYPE_DEFER)
    {
      typeStr = "defer ";
    }
  else if (type == JS_SCRIPT_TYPE_PLAIN)
    {
      typeStr = "async=\"false\" ";
    }
  std::string res =
    "<script "
    + typeStr
    + "type=\"text/javascript\" "
    + "src=\"" + jsFile + "\"" ;
  if (!pars.empty())
    {
      res += " " + pars;
    }
  res += "></script>";
  return res;
}
