#include "tcds/utils/CurlGetter.h"

#include <curl/curl.h>
#include <curl/easy.h>

#include "toolbox/net/URL.h"
#include "toolbox/string.h"
#include "xcept/Exception.h"

#include "tcds/exception/Exception.h"

namespace toolbox {
  namespace net {
    namespace exception {
      class MalformedURL;
    }
  }
}

size_t
tcds::utils::CurlGetter::staticWriteCallback(char* buffer, size_t size, size_t nItems, void* getter)
{
  return static_cast<tcds::utils::CurlGetter*>(getter)->receive(buffer, size, nItems);
}

tcds::utils::CurlGetter::CurlGetter()
{
  // NOTE: The following call is utterly _not_ thread-safe. See:
  // curl_global_init(3).
  if (curl_global_init(CURL_GLOBAL_ALL) != 0)
    {
      std::string const errMsg = "Failed to initialise curl global variables";
      XCEPT_RAISE(tcds::exception::RuntimeProblem, errMsg);
    }

  curl_ = curl_easy_init();
  if (!curl_)
    {
      std::string const errMsg = "Failed to initialise curl";
      XCEPT_RAISE(tcds::exception::RuntimeProblem, errMsg);
    }
}

tcds::utils::CurlGetter::~CurlGetter()
{
  curl_easy_cleanup(curl_);
  // NOTE: The following call is utterly _not_ thread-safe. See:
  // curl_global_cleanup(3).
  curl_global_cleanup();
}

std::string
tcds::utils::CurlGetter::buildFullURL(std::string const& scheme,
                                      std::string const& address,
                                      std::string const& path,
                                      std::string const& parameters,
                                      std::string const& query,
                                      std::string const& fragment)
{
  std::string const baseURL = scheme + "://" + address + "/" + path;
  return buildFullURL(baseURL, parameters, query, fragment);
}

std::string
tcds::utils::CurlGetter::buildFullURL(std::string const& baseURL,
                                      std::string const& parameters,
                                      std::string const& query,
                                      std::string const& fragment)
{
  std::string url = baseURL;
  if (!parameters.empty())
    {
      url += ";" + parameters;
    }
  if (!query.empty())
    {
      url += "?" + query;
    }
  if (!fragment.empty())
    {
      url += "#" + fragment;
    }

  return url;
}

std::string
tcds::utils::CurlGetter::get(std::string const& scheme,
                             std::string const& address,
                             std::string const& path,
                             std::string const& parameters,
                             std::string const& query,
                             std::string const& fragment)
{
  std::string const url = buildFullURL(scheme, address, path, parameters, query, fragment);
  return get(url);
}

std::string
tcds::utils::CurlGetter::get(std::string const& baseURL,
                             std::string const& parameters,
                             std::string const& query,
                             std::string const& fragment)
{
  std::string const url = buildFullURL(baseURL, parameters, query, fragment);
  return get(url);
}

std::string
tcds::utils::CurlGetter::get(std::string const& url)
{
  // See if the URL is usable.
  try
    {
      toolbox::net::URL urlTmp(url);
    }
  catch (toolbox::net::exception::MalformedURL const& err)
    {
      std::string const msgBase = "Stumbled across a malformed URL";
      std::string const msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), url.c_str());
      XCEPT_RAISE(tcds::exception::RuntimeProblem, msg.c_str());
    }

  // Let's get the result.
  receiveBuffer_.clear();
  try
    {
      toolbox::net::URL urlTmp(url);
      performGetRequest(urlTmp);
    }
  catch (xcept::Exception const& err)
    {
      std::string const msgBase = toolbox::toString("Failed to GET '%s'", url.c_str());
      std::string const msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.what());
      XCEPT_RAISE(tcds::exception::RuntimeProblem, msg.c_str());
    }

  std::string const res = receiveBuffer_;
  return res;
}

void
tcds::utils::CurlGetter::performGetRequest(toolbox::net::URL const& url)
{
  curl_easy_reset(curl_);

  // Protect against a CURL bug.
  // http://stackoverflow.com/questions/9191668/error-longjmp-causes-uninitialized-stack-frame
  curl_easy_setopt(curl_, CURLOPT_NOSIGNAL, 1L);

  // Specify which URL to get.
  curl_easy_setopt(curl_, CURLOPT_URL, url.toString().c_str());

  // Specify the HTTP method to use: GET.
  curl_easy_setopt(curl_, CURLOPT_HTTPGET, 1L);

  // Specify a user-agent, just for completeness.
  curl_easy_setopt(curl_, CURLOPT_USERAGENT, "libcurl-agent/1.0");

  // Specify timeout values: 2 seconds.
  long const timeout = 2;
  curl_easy_setopt(curl_, CURLOPT_TIMEOUT, timeout);
  curl_easy_setopt(curl_, CURLOPT_CONNECTTIMEOUT, timeout);

  // Specify the callback method for writing received data.
  curl_easy_setopt(curl_, CURLOPT_WRITEFUNCTION, tcds::utils::CurlGetter::staticWriteCallback);

  // Specify the data pointer to pass to the write callback.
  curl_easy_setopt(curl_, CURLOPT_WRITEDATA, static_cast<void*>(this));

  // Try to prevent infinite redirect loops.
  curl_easy_setopt(curl_, CURLOPT_MAXREDIRS, 10L);

  // Make sure that the call to curl_easy_perform fails in case of
  // HTTP errors (i.e., status codes >= 400).
  curl_easy_setopt(curl_, CURLOPT_FAILONERROR, 1L);

  // Provide a buffer for error messages, if needed.
  errorBuffer_ = std::string(CURL_ERROR_SIZE, '\0');
  curl_easy_setopt(curl_, CURLOPT_ERRORBUFFER, errorBuffer_.c_str());

  // Clear our receiving buffer.
  receiveBuffer_.clear();

  // Engage!
  CURLcode const status = curl_easy_perform(curl_);

  // See what happened.
  if (status != CURLE_OK)
    {
      size_t const len = errorBuffer_.size();
      std::string errMsg;
      if (len != 0)
        {
          errMsg = errorBuffer_;
        }
      else
        {
          errMsg = curl_easy_strerror(status);
        }
      // The exception we throw depends on the kind of error we
      // encountered.
      if (status == CURLE_HTTP_RETURNED_ERROR)
        {
          XCEPT_RAISE(tcds::exception::HTTPProblem, errMsg);
        }
      else
        {
          XCEPT_RAISE(tcds::exception::RuntimeProblem, errMsg);
        }
    }
}

size_t
tcds::utils::CurlGetter::receive(char* const buffer, size_t const size, size_t const nItems)
{
  size_t const realSize = size * nItems;
  receiveBuffer_.append(static_cast<char const*>(buffer), realSize);
  return realSize;
}
