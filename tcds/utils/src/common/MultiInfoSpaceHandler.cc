#include "tcds/utils/MultiInfoSpaceHandler.h"

#include "xdaq/Application.h"

#include "tcds/utils/InfoSpaceHandler.h"

tcds::utils::MultiInfoSpaceHandler::MultiInfoSpaceHandler(xdaq::Application& xdaqApp,
                                                          InfoSpaceUpdater* const updater) :
  XDAQObject(xdaqApp),
  logger_(xdaqApp.getApplicationLogger())
{
}

tcds::utils::MultiInfoSpaceHandler::~MultiInfoSpaceHandler()
{
  for (std::map<std::string, tcds::utils::InfoSpaceHandler*>::iterator
         i = infoSpaceHandlers_.begin();
       i != infoSpaceHandlers_.end();
       ++i)
    {
      delete i->second;
      infoSpaceHandlers_.erase(i);
    }
}

void
tcds::utils::MultiInfoSpaceHandler::registerItemSets(Monitor& monitor,
                                                     WebServer& webServer)
{
  registerItemSetsWithMonitor(monitor);
  registerItemSetsWithWebServer(webServer, monitor);
}

void
tcds::utils::MultiInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  for (std::map<std::string, tcds::utils::InfoSpaceHandler*>::iterator
         i = infoSpaceHandlers_.begin();
       i != infoSpaceHandlers_.end();
       ++i)
    {
      i->second->registerItemSetsWithMonitor(monitor);
    }
}

void
tcds::utils::MultiInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                  tcds::utils::Monitor& monitor,
                                                                  std::string const& forceTabName)
{
  for (std::map<std::string, tcds::utils::InfoSpaceHandler*>::iterator
         i = infoSpaceHandlers_.begin();
       i != infoSpaceHandlers_.end();
       ++i)
    {
      i->second->registerItemSetsWithWebServer(webServer, monitor, forceTabName);
    }
}

void
tcds::utils::MultiInfoSpaceHandler::writeInfoSpace(bool const force)
{
  for (std::map<std::string, tcds::utils::InfoSpaceHandler*>::const_iterator
         it = infoSpaceHandlers_.begin();
       it != infoSpaceHandlers_.end();
       ++it)
    {
      it->second->writeInfoSpace(true);
    }
}
