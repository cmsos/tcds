#include "tcds/utils/OwnerId.h"

#include "toolbox/string.h"

bool
tcds::utils::operator==(tcds::utils::OwnerId const& lhs,
                        tcds::utils::OwnerId const& rhs)
{
  return ((lhs.rcmsSessionId() == rhs.rcmsSessionId()) &&
          (lhs.sessionName() == rhs.sessionName()));
}

bool
tcds::utils::operator!=(tcds::utils::OwnerId const& lhs,
                        tcds::utils::OwnerId const& rhs)
{
  return !(lhs == rhs);
}

tcds::utils::OwnerId::OwnerId(uint32_t const rcmsSessionId,
                              std::string const& sessionName) :
  rcmsSessionId_(rcmsSessionId),
  sessionName_(sessionName)
{
  // Special case for the 'anonymous RCMS session': set the session
  // name to something descriptive.
  if ((rcmsSessionId_ != 0) && sessionName_.empty())
    {
      sessionName_ = toolbox::toString("rcms_session_%u", rcmsSessionId_);
    }
}

std::string
tcds::utils::OwnerId::asString() const
{
  std::string res;
  if (!isValid())
    {
      res = "'Invalid owner'";
    }
  else
    {
      res = "'" + sessionName_ + "'";
      if (rcmsSessionId_ == 0)
        {
          res += " (which is a non-RCMS session)";
        }
      else
        {
          res += toolbox::toString(" (RCMS session #%u)",
                                   rcmsSessionId_);
        }
    }
  return res;
}

uint32_t
tcds::utils::OwnerId::rcmsSessionId() const
{
  return rcmsSessionId_;
}

std::string const
tcds::utils::OwnerId::sessionName() const
{
  return sessionName_;
}

bool
tcds::utils::OwnerId::isValid() const
{
  return ((rcmsSessionId_ != 0) || (!sessionName_.empty()));
}

void
tcds::utils::OwnerId::clear()
{
  rcmsSessionId_ = 0;
  sessionName_.clear();
}
