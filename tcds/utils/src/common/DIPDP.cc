#include "tcds/utils/DIPDP.h"

tcds::utils::DIPDP::DIPDP(std::string const& dpName,
                          std::string const& dpValue,
                          tcds::utils::DIP_QUAL const dpQual) :
  dpName_(dpName),
  dpValue_(dpValue),
  dpQual_(dpQual)
{
}

std::string
tcds::utils::DIPDP::dpName() const
{
  return dpName_;
}

std::string
tcds::utils::DIPDP::dpValue() const
{
  return dpValue_;
}

tcds::utils::DIP_QUAL
tcds::utils::DIPDP::dpQual() const
{
  return dpQual_;
}
