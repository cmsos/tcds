#include "tcds/utils/MonitorUpdateEvent.h"

tcds::utils::MonitorUpdateEvent::MonitorUpdateEvent(void* originator) :
  toolbox::Event("MonitoringUpdated", originator)
{
}
