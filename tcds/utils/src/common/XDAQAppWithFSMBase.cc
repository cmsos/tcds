#include "tcds/utils/XDAQAppWithFSMBase.h"

#include <algorithm>
#include <fstream>
#include <set>
#include <utility>
#include <vector>

#include "toolbox/string.h"
#include "toolbox/TimeVal.h"
#include "xcept/Exception.h"

#include "tcds/exception/Exception.h"
#include "tcds/hwlayer/ConfigurationProcessor.h"
#include "tcds/hwlayer/DeviceBase.h"
#include "tcds/hwlayer/RegDumpConfigurationProcessor.h"
#include "tcds/hwlayer/RegisterInfo.h"
#include "tcds/utils/ConfigurationInfoSpaceHandler.h"
#include "tcds/utils/Lock.h"
#include "tcds/utils/LockGuard.h"
#include "tcds/utils/LogMacros.h"
#include "tcds/utils/RegCheckResult.h"
#include "tcds/utils/Utils.h"

tcds::utils::XDAQAppWithFSMBase::XDAQAppWithFSMBase(xdaq::ApplicationStub* const stub,
                                                    std::unique_ptr<tcds::hwlayer::DeviceBase> hw) :
  XDAQAppBase(stub, std::move(hw)),
  fsm_(this)
{
}

tcds::utils::XDAQAppWithFSMBase::~XDAQAppWithFSMBase()
{
}

void
tcds::utils::XDAQAppWithFSMBase::coldResetAction(toolbox::Event::Reference event)
{
  INFO("XDAQAppWithFSMBase::coldResetAction()");
  toolbox::TimeVal timeBegin = toolbox::TimeVal::gettimeofday();
  LockGuard<Lock> guardedLock(monitoringLock_);

  try
    {
      coldResetActionImpl(event);
    }
  catch (xcept::Exception& err)
    {
      std::string msgBase = "State transition 'ColdReset' failed";
      std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
      ERROR(msg);
      XCEPT_DECLARE_NESTED(tcds::exception::RuntimeProblem, top, msg, err);
      notifyQualified("error", top);
      fsm_.gotoFailed(top);
    }

  toolbox::TimeVal timeEnd = toolbox::TimeVal::gettimeofday();
  INFO("XDAQAppWithFSMBase::coldResetAction() took "
       << tcds::utils::formatDeltaTString(timeBegin, timeEnd, true)
       << ".");
}

void
tcds::utils::XDAQAppWithFSMBase::coldReset()
{
  fsm_.coldReset();
}

void
tcds::utils::XDAQAppWithFSMBase::configure()
{
  fsm_.configure();
}

void
tcds::utils::XDAQAppWithFSMBase::enable()
{
  fsm_.enable();
}

void
tcds::utils::XDAQAppWithFSMBase::fail()
{
  fsm_.fail();
}

void
tcds::utils::XDAQAppWithFSMBase::halt()
{
  fsm_.halt();
}

void
tcds::utils::XDAQAppWithFSMBase::pause()
{
  fsm_.pause();
}

void
tcds::utils::XDAQAppWithFSMBase::reconfigure()
{
  fsm_.reconfigure();
}

void
tcds::utils::XDAQAppWithFSMBase::resume()
{
  fsm_.resume();
}

void
tcds::utils::XDAQAppWithFSMBase::stop()
{
  fsm_.stop();
}

std::string
tcds::utils::XDAQAppWithFSMBase::getCurrentStateName() const
{
  return fsm_.getCurrentStateName();
}

void
tcds::utils::XDAQAppWithFSMBase::configureAction(toolbox::Event::Reference event)
{
  INFO("XDAQAppWithFSMBase::configureAction()");
  toolbox::TimeVal timeBegin = toolbox::TimeVal::gettimeofday();

  LockGuard<Lock> guardedLock(monitoringLock_);

  try
    {
      configureActionImpl(event);
    }
  catch (xcept::Exception& err)
    {
      std::string msgBase = "State transition 'Configure' failed";
      std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
      ERROR(msg);
      XCEPT_DECLARE_NESTED(tcds::exception::RuntimeProblem, top, msg, err);
      notifyQualified("error", top);
      fsm_.gotoFailed(top);
    }

  // Make sure the configuration InfoSpace stays in-sync.
  cfgInfoSpaceP_->writeInfoSpace();

  toolbox::TimeVal timeEnd = toolbox::TimeVal::gettimeofday();
  INFO("XDAQAppWithFSMBase::configureAction() took "
       << tcds::utils::formatDeltaTString(timeBegin, timeEnd, true)
       << ".");
}

void
tcds::utils::XDAQAppWithFSMBase::enableAction(toolbox::Event::Reference event)
{
  INFO("XDAQAppWithFSMBase::enableAction()");
  toolbox::TimeVal timeBegin = toolbox::TimeVal::gettimeofday();
  LockGuard<Lock> guardedLock(monitoringLock_);

  try
    {
      enableActionImpl(event);
    }
  catch (xcept::Exception& err)
    {
      std::string msgBase = "State transition 'Enable' failed";
      std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
      ERROR(msg);
      XCEPT_DECLARE_NESTED(tcds::exception::RuntimeProblem, top, msg, err);
      notifyQualified("error", top);
      fsm_.gotoFailed(top);
    }

  toolbox::TimeVal timeEnd = toolbox::TimeVal::gettimeofday();
  INFO("XDAQAppWithFSMBase::enableAction() took "
       << tcds::utils::formatDeltaTString(timeBegin, timeEnd, true)
       << ".");
}

void
tcds::utils::XDAQAppWithFSMBase::failAction(toolbox::Event::Reference event)
{
  INFO("XDAQAppWithFSMBase::failAction()");
  toolbox::TimeVal timeBegin = toolbox::TimeVal::gettimeofday();
  LockGuard<Lock> guardedLock(monitoringLock_);

  try
    {
      failActionImpl(event);
    }
  catch (xcept::Exception& err)
    {
      std::string msgBase = "State transition failed";
      std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
      ERROR(msg);
      XCEPT_DECLARE_NESTED(tcds::exception::RuntimeProblem, top, msg, err);
      notifyQualified("error", top);
      // NOTE: The failAction() method is called upon arriving in the
      // 'Failed' state, so if anything goes wrong, we should _not_
      // retrigger the same procedure by triggering another transition
      // into the 'Failed' state. So no gotoFailed() here.
    }

  toolbox::TimeVal timeEnd = toolbox::TimeVal::gettimeofday();
  INFO("XDAQAppWithFSMBase::failAction() took "
       << tcds::utils::formatDeltaTString(timeBegin, timeEnd, true)
       << ".");
}

void
tcds::utils::XDAQAppWithFSMBase::haltAction(toolbox::Event::Reference event)
{
  INFO("XDAQAppWithFSMBase::haltAction()");
  toolbox::TimeVal timeBegin = toolbox::TimeVal::gettimeofday();
  LockGuard<Lock> guardedLock(monitoringLock_);

  try
    {
      haltActionImpl(event);
    }
  catch (xcept::Exception& err)
    {
      std::string msgBase = "State transition 'Halt' failed";
      std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
      ERROR(msg);
      XCEPT_DECLARE_NESTED(tcds::exception::RuntimeProblem, top, msg, err);
      notifyQualified("error", top);
      fsm_.gotoFailed(top);
    }

  // If we got here, all is well. Hardware has been released. Revoke
  // the hardware lease as well now.
  revokeHwLease();

  // Reset the run number as well.
  cfgInfoSpaceP_->setUInt32("runNumber", 0);
  cfgInfoSpaceP_->writeInfoSpace();

  toolbox::TimeVal timeEnd = toolbox::TimeVal::gettimeofday();
  INFO("XDAQAppWithFSMBase::haltAction() took "
       << tcds::utils::formatDeltaTString(timeBegin, timeEnd, true)
       << ".");
}

void
tcds::utils::XDAQAppWithFSMBase::pauseAction(toolbox::Event::Reference event)
{
  INFO("XDAQAppWithFSMBase::pauseAction()");
  toolbox::TimeVal timeBegin = toolbox::TimeVal::gettimeofday();
  LockGuard<Lock> guardedLock(monitoringLock_);

  try
    {
      pauseActionImpl(event);
    }
  catch (xcept::Exception& err)
    {
      std::string msgBase = "State transition 'Pause' failed";
      std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
      ERROR(msg);
      XCEPT_DECLARE_NESTED(tcds::exception::RuntimeProblem, top, msg, err);
      notifyQualified("error", top);
      fsm_.gotoFailed(top);
    }

  toolbox::TimeVal timeEnd = toolbox::TimeVal::gettimeofday();
  INFO("XDAQAppWithFSMBase::pauseAction() took "
       << tcds::utils::formatDeltaTString(timeBegin, timeEnd, true)
       << ".");
}

void
tcds::utils::XDAQAppWithFSMBase::resumeAction(toolbox::Event::Reference event)
{
  INFO("XDAQAppWithFSMBase::resumeAction()");
  toolbox::TimeVal timeBegin = toolbox::TimeVal::gettimeofday();
  LockGuard<Lock> guardedLock(monitoringLock_);

  try
    {
      resumeActionImpl(event);
    }
  catch (xcept::Exception& err)
    {
      std::string msgBase = "State transition 'Resume' failed";
      std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
      ERROR(msg);
      XCEPT_DECLARE_NESTED(tcds::exception::RuntimeProblem, top, msg, err);
      notifyQualified("error", top);
      fsm_.gotoFailed(top);
    }

  toolbox::TimeVal timeEnd = toolbox::TimeVal::gettimeofday();
  INFO("XDAQAppWithFSMBase::resumeAction() took "
       << tcds::utils::formatDeltaTString(timeBegin, timeEnd, true)
       << ".");
}

void
tcds::utils::XDAQAppWithFSMBase::stopAction(toolbox::Event::Reference event)
{
  INFO("XDAQAppWithFSMBase::stopAction()");
  toolbox::TimeVal timeBegin = toolbox::TimeVal::gettimeofday();
  LockGuard<Lock> guardedLock(monitoringLock_);

  try
    {
      stopActionImpl(event);
    }
  catch (xcept::Exception& err)
    {
      std::string msgBase = "State transition 'Stop' failed";
      std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
      ERROR(msg);
      XCEPT_DECLARE_NESTED(tcds::exception::RuntimeProblem, top, msg, err);
      notifyQualified("error", top);
      fsm_.gotoFailed(top);
    }

  toolbox::TimeVal timeEnd = toolbox::TimeVal::gettimeofday();
  INFO("XDAQAppWithFSMBase::stopAction() took "
       << tcds::utils::formatDeltaTString(timeBegin, timeEnd, true)
       << ".");
}

void
tcds::utils::XDAQAppWithFSMBase::ttcHardResetAction(toolbox::Event::Reference event)
{
  INFO("XDAQAppWithFSMBase::ttcHardResetAction()");
  toolbox::TimeVal timeBegin = toolbox::TimeVal::gettimeofday();
  LockGuard<Lock> guardedLock(monitoringLock_);

  try
    {
      ttcHardResetActionImpl(event);
    }
  catch (xcept::Exception& err)
    {
      std::string msgBase = "State transition 'TTCHardReset' failed";
      std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
      ERROR(msg);
      XCEPT_DECLARE_NESTED(tcds::exception::RuntimeProblem, top, msg, err);
      notifyQualified("error", top);
      fsm_.gotoFailed(top);
    }

  toolbox::TimeVal timeEnd = toolbox::TimeVal::gettimeofday();
  INFO("XDAQAppWithFSMBase::ttcHardResetAction() took "
       << tcds::utils::formatDeltaTString(timeBegin, timeEnd, true)
       << ".");
}

void
tcds::utils::XDAQAppWithFSMBase::ttcResyncAction(toolbox::Event::Reference event)
{
  INFO("XDAQAppWithFSMBase::ttcResyncAction()");
  toolbox::TimeVal timeBegin = toolbox::TimeVal::gettimeofday();
  LockGuard<Lock> guardedLock(monitoringLock_);

  try
    {
      ttcResyncActionImpl(event);
    }
  catch (xcept::Exception& err)
    {
      std::string msgBase = "State transition 'TTCResync' failed";
      std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
      ERROR(msg);
      XCEPT_DECLARE_NESTED(tcds::exception::RuntimeProblem, top, msg, err);
      notifyQualified("error", top);
      fsm_.gotoFailed(top);
    }

  toolbox::TimeVal timeEnd = toolbox::TimeVal::gettimeofday();
  INFO("XDAQAppWithFSMBase::ttcResyncAction() took "
       << tcds::utils::formatDeltaTString(timeBegin, timeEnd, true)
       << ".");
}

void
tcds::utils::XDAQAppWithFSMBase::zeroAction(toolbox::Event::Reference event)
{
  INFO("XDAQAppWithFSMBase::zeroAction()");
  toolbox::TimeVal timeBegin = toolbox::TimeVal::gettimeofday();
  LockGuard<Lock> guardedLock(monitoringLock_);

  try
    {
      zeroActionImpl(event);
    }
  catch (xcept::Exception& err)
    {
      std::string msgBase = "State transition 'Zero' failed";
      std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
      ERROR(msg);
      XCEPT_DECLARE_NESTED(tcds::exception::RuntimeProblem, top, msg, err);
      notifyQualified("error", top);
      fsm_.gotoFailed(top);
    }

  toolbox::TimeVal timeEnd = toolbox::TimeVal::gettimeofday();
  INFO("XDAQAppWithFSMBase::zeroAction() took "
       << tcds::utils::formatDeltaTString(timeBegin, timeEnd, true)
       << ".");
}

xoap::MessageReference
tcds::utils::XDAQAppWithFSMBase::changeState(xoap::MessageReference msg)
{
  return changeStateImpl(msg);
}

void
tcds::utils::XDAQAppWithFSMBase::coldResetActionImpl(toolbox::Event::Reference event)
{
  try
    {
      hwColdReset();
    }
  catch (xcept::Exception& err)
    {
      std::string msgBase = "Could not ColdReset the hardware";
      std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
      ERROR(msg);
      XCEPT_DECLARE_NESTED(tcds::exception::HardwareProblem, top, msg, err);
      notifyQualified("error", top);
      throw;
      // fsm_.gotoFailed(top);
    }
}

void
tcds::utils::XDAQAppWithFSMBase::configureActionImpl(toolbox::Event::Reference event)
{
  try
    {
      hwConnect();
    }
  catch (xcept::Exception& err)
    {
      std::string msgBase = "Could not connect to the hardware";
      std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
      ERROR(msg);
      XCEPT_DECLARE_NESTED(tcds::exception::HardwareProblem, top, msg, err);
      notifyQualified("fatal", top);
      throw;
      // fsm_.gotoFailed(top);
    }

  try
    {
      hwConfigure();
    }
  catch (xcept::Exception& err)
    {
      std::string msgBase = "Could not configure the hardware";
      std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
      ERROR(msg);
      XCEPT_DECLARE_NESTED(tcds::exception::HardwareProblem, top, msg, err);
      notifyQualified("error", top);
      throw;
      // fsm_.gotoFailed(top);
    }
}

void
tcds::utils::XDAQAppWithFSMBase::enableActionImpl(toolbox::Event::Reference event)
{
  // try
  //   {
  //     hwP_->hwEnable();
  //   }
  // catch (xcept::Exception& err)
  //   {
  //     std::string msgBase = "Could not enable the hardware";
  //     std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
  //     ERROR(msg);
  //     XCEPT_DECLARE_NESTED(tcds::exception::HardwareProblem, top, msg, err);
  //     notifyQualified("fatal", top);
  //     throw;
  //     // fsm_.gotoFailed(top);
  //   }
}

void
tcds::utils::XDAQAppWithFSMBase::failActionImpl(toolbox::Event::Reference event)
{
  // BUG BUG BUG
  // Figure out if we really want to do this.
  // if (hwP_ != 0)
  //   {
  //     // BUG BUG BUG
  //     // This is a mess (and may need a lock, actually).
  //     hwP_->hwRelease();
  //     // BUG BUG BUG end
  //   }
  // BUG BUG BUG end
}

void
tcds::utils::XDAQAppWithFSMBase::haltActionImpl(toolbox::Event::Reference event)
{
  // try
  //   {
  //     hwP_->hwHalt();
  //   }
  // catch (xcept::Exception& err)
  //   {
  //     std::string msgBase = "Could not halt the hardware";
  //     std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
  //     ERROR(msg);
  //     XCEPT_DECLARE_NESTED(tcds::exception::HardwareProblem, top, msg, err);
  //     notifyQualified("fatal", top);
  //     throw;
  //     // fsm_.gotoFailed(top);
  //   }

  try
    {
      hwRelease();
    }
  catch (xcept::Exception& err)
    {
      std::string msgBase = "Could not release the hardware";
      std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
      ERROR(msg);
      XCEPT_DECLARE_NESTED(tcds::exception::HardwareProblem, top, msg, err);
      notifyQualified("fatal", top);
      throw;
      // fsm_.gotoFailed(top);
    }
}

void
tcds::utils::XDAQAppWithFSMBase::pauseActionImpl(toolbox::Event::Reference event)
{
  // try
  //   {
  //     hwP_->hwPause();
  //   }
  // catch (xcept::Exception& err)
  //   {
  //     std::string msgBase = "Could not pause the hardware";
  //     std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
  //     ERROR(msg);
  //     XCEPT_DECLARE_NESTED(tcds::exception::HardwareProblem, top, msg, err);
  //     notifyQualified("fatal", top);
  //     throw;
  //     // fsm_.gotoFailed(top);
  //   }

}

void
tcds::utils::XDAQAppWithFSMBase::resumeActionImpl(toolbox::Event::Reference event)
{
  // try
  //   {
  //     hwP_->hwResume();
  //   }
  // catch (xcept::Exception& err)
  //   {
  //     std::string msgBase = "Could not resume the hardware";
  //     std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
  //     ERROR(msg);
  //     XCEPT_DECLARE_NESTED(tcds::exception::HardwareProblem, top, msg, err);
  //     notifyQualified("fatal", top);
  //     throw;
  //     // fsm_.gotoFailed(top);
  //   }
}

void
tcds::utils::XDAQAppWithFSMBase::stopActionImpl(toolbox::Event::Reference event)
{
}

void
tcds::utils::XDAQAppWithFSMBase::ttcHardResetActionImpl(toolbox::Event::Reference event)
{
}

void
tcds::utils::XDAQAppWithFSMBase::ttcResyncActionImpl(toolbox::Event::Reference event)
{
}

void
tcds::utils::XDAQAppWithFSMBase::zeroActionImpl(toolbox::Event::Reference event)
{
}

void
tcds::utils::XDAQAppWithFSMBase::hwColdReset()
{
  hwColdResetImpl();
}

void
tcds::utils::XDAQAppWithFSMBase::hwColdResetImpl()
{
  // The default does nothing.
}

void
tcds::utils::XDAQAppWithFSMBase::hwConfigure()
{
  hwConfigureImpl();
}

void
tcds::utils::XDAQAppWithFSMBase::hwConfigureImpl()
{
  // Extract the hardware configuration string and parse it into a
  // register-value list.
  std::string const configurationString =
    cfgInfoSpaceP_->getString("hardwareConfigurationStringReceived");
  tcds::hwlayer::RegDumpConfigurationProcessor cfgProcessor;
  tcds::hwlayer::ConfigurationProcessor::RegValVec const cfgInfoUser =
    cfgProcessor.parse(configurationString);

  // Load and parse the default configuration file.
  std::string const tmp = cfgInfoSpaceP_->getString("defaultHwConfigurationFilePath");
  std::string const defaultConfigurationFileName = tcds::utils::expandPathName(tmp);
  std::ifstream inputFile(defaultConfigurationFileName.c_str());
  std::string defaultConfigurationString = "";
  if (!inputFile.is_open() || !inputFile.good())
    {
      std::string const msg =
        toolbox::toString("Failed to read default hardware configuration from file '%s'.",
                          defaultConfigurationFileName.c_str());
      XCEPT_RAISE(tcds::exception::ConfigurationProblem, msg);
    }
  else
    {
      inputFile.seekg(0, std::ios::end);
      defaultConfigurationString.resize(inputFile.tellg());
      inputFile.seekg(0, std::ios::beg);
      inputFile.read(&defaultConfigurationString[0], defaultConfigurationString.size());
    }
  inputFile.close();
  cfgInfoSpaceP_->setString("hardwareConfigurationStringDefault", defaultConfigurationString);
  tcds::hwlayer::ConfigurationProcessor::RegValVec cfgInfoDefault =
    cfgProcessor.parse(defaultConfigurationString);

  // Sanitize the default hardware configuration to remove registers
  // that exist but that may not be under the control of the current
  // application. At the time of writing the only example are the
  // registers governing the LPM external trigger inputs. Depending on
  // the application's configuration it may have control over none,
  // either, or both trigger inputs.
  // The sanitizing is done by calling isRegisterAllowed()
  // 'preemptively' on the default configuration and filter out
  // registers that are flagged as 'unavailable.' The real issues
  // (i.e., registers flagged as 'disallowed' will be handled in the
  // final pass after merging with the user-specified configuration.
  tcds::hwlayer::RegisterInfo::RegInfoVec regInfos = hwP_->getRegisterInfos();
  tcds::hwlayer::ConfigurationProcessor::RegValVec::iterator i = cfgInfoDefault.begin();
  while (i != cfgInfoDefault.end())
    {
      tcds::hwlayer::RegisterInfo::RegInfoVec::const_iterator regInfo;
      regInfo = std::find_if(regInfos.begin(),
                             regInfos.end(),
                             tcds::hwlayer::RegisterInfo::RegInfoNameMatches(i->first));
      // NOTE: Registers that don't exist (as in: can't be found)
      // won't be treated here. Just ignore them for the moment. They
      // will be flagged a bit further below.
      if ((regInfo != regInfos.end())
          && (isRegisterAllowed(*regInfo) == tcds::utils::kRegCheckResultUnavailable))
        {
          // Careful: in-place modification!
          i = cfgInfoDefault.erase(i);
        }
      else
        {
          ++i;
        }
    }

  // Merge the default and the user configurations.
  tcds::hwlayer::ConfigurationProcessor::RegValVec cfgInfo = cfgInfoUser;
  std::vector<std::string> regNamesInUserConfig;
  for (tcds::hwlayer::ConfigurationProcessor::RegValVec::const_iterator i = cfgInfoUser.begin();
       i != cfgInfoUser.end();
       ++i)
    {
      regNamesInUserConfig.push_back(i->first);
    }
  for (tcds::hwlayer::ConfigurationProcessor::RegValVec::const_iterator i = cfgInfoDefault.begin();
       i != cfgInfoDefault.end();
       ++i)
    {
      if (std::find(regNamesInUserConfig.begin(),
                    regNamesInUserConfig.end(),
                    i->first) == regNamesInUserConfig.end())
        {
          cfgInfo.push_back(*i);
        }
    }

  // Be explicit about empty 'applied' hardware configuration strings.
  std::string appliedCfgStr = cfgProcessor.compose(cfgInfo);
  if (appliedCfgStr.empty())
    {
      appliedCfgStr = "# Empty.";
    }
  cfgInfoSpaceP_->setString("hardwareConfigurationStringApplied", appliedCfgStr);

  //----------

  // Make sure that we're not accessing any registers that we are not
  // allowed to access.

  // Start by building a list of registers that exist in the hardware
  // (or at least in the address table).
  std::vector<std::string> regNamesVec = hwP_->getRegisterNames();
  std::set<std::string> regNamesSet(regNamesVec.begin(), regNamesVec.end());

  // Build a list of registers mentioned in the configuration.
  std::vector<std::string> cfgRegNamesVec;
  cfgRegNamesVec.reserve(cfgInfo.size());
  for (tcds::hwlayer::ConfigurationProcessor::RegValVec::const_iterator i = cfgInfo.begin();
       i != cfgInfo.end();
       ++i)
    {
      cfgRegNamesVec.push_back(i->first);
    }
  std::set<std::string> const cfgRegNamesSet(cfgRegNamesVec.begin(), cfgRegNamesVec.end());

  for (std::set<std::string>::const_iterator i = cfgRegNamesSet.begin();
       i != cfgRegNamesSet.end();
       ++i)
    {
      // Check if the register that is specified in the configuration
      // actually exists.
      if ((regNamesSet.erase(*i)) < 1)
        {
          // Find out if this register came from the user-config or
          // from the default config.
          bool const isRegFromUserConfig = (std::find(regNamesInUserConfig.begin(),
                                                      regNamesInUserConfig.end(),
                                                      *i) != regNamesInUserConfig.end());
          std::string tmp = "which came from the default configuration";
          if (isRegFromUserConfig)
            {
              tmp = "which came from the user-specified configuration";
            }
          std::string const msg =
            toolbox::toString("Register '%s' (%s) does not exist.",
                              i->c_str(),
                              tmp.c_str());
          XCEPT_RAISE(tcds::exception::ValueError, msg);
        }

      // Check if the register that is specified in the configuration
      // is allowed to be configured.
      tcds::hwlayer::RegisterInfo::RegInfoVec::const_iterator regInfo;
      regInfo = std::find_if(regInfos.begin(),
                             regInfos.end(),
                             tcds::hwlayer::RegisterInfo::RegInfoNameMatches(*i));
      // This should never happen, really.
      if (regInfo == regInfos.end())
        {
          std::string const msg =
            toolbox::toString("Register '%s' (%s) does not exist "
                              "(but somehow passed the check on register existence?).",
                              i->c_str(),
                              tmp.c_str());
          XCEPT_RAISE(tcds::exception::SoftwareProblem, msg);
        }
      tcds::utils::RegCheckResult const res = isRegisterAllowed(*regInfo);
      if (!res)
        {
          // Find out if this register came from the user-config or
          // from the default config.
          bool const isRegFromUserConfig = (std::find(regNamesInUserConfig.begin(),
                                                      regNamesInUserConfig.end(),
                                                      *i) != regNamesInUserConfig.end());
          std::string tmp = "which came from the default configuration";
          if (isRegFromUserConfig)
            {
              tmp = "which came from the user-specified configuration";
            }
          std::string detail = "is not allowed to be configured";
          if (res == tcds::utils::kRegCheckResultUnavailable)
            {
              detail = "is not accessible to this application";
            }
          std::string const msg =
            toolbox::toString("Register '%s' (%s) %s.",
                              i->c_str(),
                              tmp.c_str(),
                              detail.c_str());
          XCEPT_RAISE(tcds::exception::ValueError, msg);
        }
    }

  //----------

  // Initialize (or reset) the hardware (or at least the functional
  // firmware block) we're connected to.
  hwCfgInitialize();

  // Do the actual configuration.
  hwP_->writeHardwareConfiguration(cfgInfo);

  // Finalize the configuration.
  hwCfgFinalize();
}

void
tcds::utils::XDAQAppWithFSMBase::hwCfgInitialize()
{
  hwCfgInitializeImpl();
}

void
tcds::utils::XDAQAppWithFSMBase::hwCfgInitializeImpl()
{
}

void
tcds::utils::XDAQAppWithFSMBase::hwCfgFinalize()
{
  hwCfgFinalizeImpl();
}

void
tcds::utils::XDAQAppWithFSMBase::hwCfgFinalizeImpl()
{
}

// This simply forwards the message to the FSM object, since it is
// technically not possible to bind directly to anything but an
// xdaq::Application.
xoap::MessageReference
tcds::utils::XDAQAppWithFSMBase::changeStateImpl(xoap::MessageReference msg)
{
  return fsm_.changeState(msg);
}
