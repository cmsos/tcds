#include "tcds/utils/WebTableAppHist.h"

#include <sstream>
#include <string>

tcds::utils::WebTableAppHist::WebTableAppHist(std::string const& name,
                                              std::string const& description,
                                              tcds::utils::Monitor const& monitor,
                                              std::string const& itemSetName,
                                              std::string const& tabName,
                                              size_t const colSpan) :
  tcds::utils::WebObject(name, description, monitor, itemSetName, tabName, colSpan)
{
}

std::string
tcds::utils::WebTableAppHist::getHTMLString() const
{
  // NOTE: This has been written to work with the new XDAQ12-style
  // HyperDAQ tabs and doT.js.

  std::stringstream res;

  res << "<div class=\"tcds-item-table-wrapper\">"
      << "\n";

  res << "<p class=\"tcds-item-table-title\">"
      << getName()
      << "</p>";
  res << "\n";

  res << "<p class=\"tcds-item-table-description\">"
      << getDescription()
      << "</p>";
  res << "\n";

  // NOTE: This one is kinda special. Mainly due to the fact that this
  // table can become quite long. Therefore this is handled
  // completely in JavaScript (with the help of a cool, virtual
  // scrolling grid plugin).
  res << "<div id=\"application-status-history-placeholder\"></div>"
      << "\n";

  res << "</div>"
      << "\n";

  return res.str();
}
