#include "tcds/utils/XDAQAppWithFSMAutomatic.h"

#include "toolbox/Event.h"
#include "xdaq/InstantiateApplicationEvent.h"

#include "tcds/hwlayer/DeviceBase.h"

tcds::utils::XDAQAppWithFSMAutomatic::XDAQAppWithFSMAutomatic(xdaq::ApplicationStub* const stub,
                                                              std::unique_ptr<tcds::hwlayer::DeviceBase> hw) :
  XDAQAppWithFSMBase(stub, std::move(hw)),
  fsmDriver_(this)
{
  // This 'automatic' state machine is supposed to run
  // itself. Therefore none of the state transitions are exposed to
  // the outside world.
}

tcds::utils::XDAQAppWithFSMAutomatic::~XDAQAppWithFSMAutomatic()
{
  fsmDriver_.stop();
}

void
tcds::utils::XDAQAppWithFSMAutomatic::actionPerformed(toolbox::Event& event)
{
  // This is called after an application has been fully
  // instantiated. Here it is used to detect when our application is
  // fully up and running (according to XDAQ).

  if (event.type() == "urn:xdaq-event:InstantiateApplication")
    {
      xdaq::InstantiateApplicationEvent& de =
        dynamic_cast<xdaq::InstantiateApplicationEvent&>(event);
      if (de.getApplicationDescriptor() == getApplicationDescriptor())
        {
          // First do what we would always do.
          tcds::utils::XDAQAppWithFSMBase::actionPerformed(event);

          // Then fire up the 'automatic FSM driver'.
          fsmDriver_.start();
        }
    }
}

void
tcds::utils::XDAQAppWithFSMAutomatic::raiseAlarm(std::string const& name,
                                                 xcept::Exception& err)
{
  tcds::utils::XDAQAppWithFSMBase::raiseAlarm(name, err);
}

void
tcds::utils::XDAQAppWithFSMAutomatic::revokeAlarm(std::string const& name)
{
  tcds::utils::XDAQAppWithFSMBase::revokeAlarm(name);
}

void
tcds::utils::XDAQAppWithFSMAutomatic::handleMonitoringProblem(std::string const& problemDesc)
{
  // In order for the automated FSM to be able to recover from a
  // monitoring problem (by reconnecting and reconfiguring the
  // hardware) we need to ensure that monitoring problems bring the
  // application to the Failed state. (But first do what we always do.)

  tcds::utils::XDAQAppWithFSMBase::handleMonitoringProblem(problemDesc);

  fail();
}
