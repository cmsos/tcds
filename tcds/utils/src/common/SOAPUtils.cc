#include "tcds/utils/SOAPUtils.h"

#include <cassert>
#include <list>
#include <locale>
#include <utility>

#include "xdaq/ApplicationDescriptor.h"
#include "xercesc/dom/DOMElement.hpp"
#include "xercesc/dom/DOMException.hpp"
#include "xercesc/dom/DOMNode.hpp"

#include "toolbox/string.h"
#include "xcept/Exception.h"
#include "xdaq/Application.h"
#include "xdaq/NamespaceURI.h"
#include "xdata/Boolean.h"
#include "xdata/String.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/soap/Serializer.h"
#include "xoap/MessageFactory.h"
#include "xoap/MessageReference.h"
#include "xoap/SOAPBody.h"
#include "xoap/SOAPConstants.h"
#include "xoap/SOAPElement.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPFault.h"
#include "xoap/SOAPName.h"
#include "xoap/SOAPPart.h"
#include "xoap/domutils.h"
#include "xoap/filter/MessageFilter.h"

#include "tcds/exception/Exception.h"
#include "tcds/utils/Utils.h"

xoap::MessageReference
tcds::utils::soap::makeParameterGetSOAPCmd(std::string const& className,
                                           std::string const& parName,
                                           std::string const& parType)
{
  std::string const applicationNameSpace =
    toolbox::toString("urn:xdaq-application:%s", className.c_str());
  xoap::MessageReference msg = xoap::createMessage();
  xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();
  envelope.addNamespaceDeclaration("xsi",
                                   "http://www.w3.org/2001/XMLSchema-instance");
  envelope.addNamespaceDeclaration("xsd",
                                   "http://www.w3.org/2001/XMLSchema");
  envelope.addNamespaceDeclaration("soapenc",
                                   "http://schemas.xmlsoap.org/soap/encoding/");
  xoap::SOAPBody body = envelope.getBody();
  xoap::SOAPName commandName = envelope.createName("ParameterGet",
                                                   "xdaq",
                                                   XDAQ_NS_URI);
  xoap::SOAPElement commandElement = body.addBodyElement(commandName);
  xoap::SOAPName propertiesName = envelope.createName("properties",
                                                      "p",
                                                      applicationNameSpace);
  xoap::SOAPElement propertiesElement = commandElement.addChildElement(propertiesName);
  xoap::SOAPName propertiesTypeName = envelope.createName("type",
                                                          "xsi",
                                                          "http://www.w3.org/2001/XMLSchema-instance");
  propertiesElement.addAttribute(propertiesTypeName, "soapenc:Struct");

  xoap::SOAPName propertyName = envelope.createName(parName, "p", applicationNameSpace);
  xoap::SOAPElement propertyElement = propertiesElement.addChildElement(propertyName);
  xoap::SOAPName propertyTypeName = envelope.createName("type",
                                                        "xsi",
                                                        "http://www.w3.org/2001/XMLSchema-instance");
  propertyElement.addAttribute(propertyTypeName, parType);
  return msg;
}

xoap::MessageReference
tcds::utils::soap::makeSOAPFaultReply(xdaq::Application* const xdaqApp,
                                      xoap::MessageReference const& msg,
                                      SOAPFaultCodeType const faultCode,
                                      std::string const& faultString,
                                      std::string const& faultDetail)
{
  std::string const soapProtocolVersion =
    msg->getImplementationFactory()->getProtocolVersion();
  std::string const serviceName = xdaqApp->getApplicationDescriptor()->getAttribute("service");
  std::string const faultStringTmp = toolbox::toString("Service %s: %s",
                                                       serviceName.c_str(),
                                                       faultString.c_str());
  return makeSOAPFaultReply(soapProtocolVersion, faultCode, faultStringTmp, faultDetail);
}

xoap::MessageReference
tcds::utils::soap::makeCommandSOAPReply(xoap::MessageReference const& msg,
                                        std::string const& fsmState)
{
  std::string const soapProtocolVersion =
    msg->getImplementationFactory()->getProtocolVersion();
  std::string const command = extractSOAPCommandName(msg);
  return makeCommandSOAPReply(soapProtocolVersion, command, fsmState);
}

xoap::MessageReference
tcds::utils::soap::makeCommandSOAPReply(xoap::MessageReference const& msg,
                                        std::string const& replyMsg,
                                        std::string const& fsmState)
{
  std::string const soapProtocolVersion =
    msg->getImplementationFactory()->getProtocolVersion();
  std::string const command = extractSOAPCommandName(msg);
  return makeCommandSOAPReply(soapProtocolVersion, command, replyMsg, fsmState);
}

xoap::MessageReference
tcds::utils::soap::makeSOAPFaultReply(std::string const& soapProtocolVersion,
                                      SOAPFaultCodeType const faultCode,
                                      std::string const& faultString,
                                      std::string const& faultDetail)
{
  xoap::MessageFactory* messageFactory = xoap::MessageFactory::getInstance(soapProtocolVersion);
  xoap::MessageReference reply = messageFactory->createMessage();
  xoap::SOAPBody body = reply->getSOAPPart().getEnvelope().getBody();
  xoap::SOAPFault fault = body.addFault();
  if (messageFactory->getProtocolVersion() == xoap::SOAPConstants::SOAP_1_1_PROTOCOL)
    {
      switch (faultCode)
        {
        case SOAPFaultCodeSender:
          fault.setFaultCode("Client");
          break;
        case SOAPFaultCodeReceiver:
          fault.setFaultCode("Server");
          break;
        default:
          // ASSERT ASSERT ASSERT
          // Should never get here.
          assert (false);
          // ASSERT ASSERT ASSERT end
          break;
        }
      fault.setFaultString(faultString);
    }
  else if (messageFactory->getProtocolVersion() == xoap::SOAPConstants::SOAP_1_2_PROTOCOL)
    {
      std::string faultCodeString = "";
      switch (faultCode)
        {
        case SOAPFaultCodeSender:
          faultCodeString = "Sender";
          break;
        case SOAPFaultCodeReceiver:
          faultCodeString = "Receiver";
          break;
        default:
          // ASSERT ASSERT ASSERT
          // Should never get here.
          assert (false);
          // ASSERT ASSERT ASSERT end
          break;
        }
      xoap::SOAPName faultCodeQName(faultCodeString,
                                    messageFactory->getEnvelopePrefix(),
                                    xoap::SOAPConstants::URI_NS_SOAP_1_2_ENVELOPE);
      fault.setFaultCode(faultCodeQName);
      fault.addFaultReasonText(faultString, std::locale("en_US"));
    }
  else
    {
      // ASSERT ASSERT ASSERT
      // Should never get here.
      assert (false);
      // ASSERT ASSERT ASSERT end
    }

  if (faultDetail != "")
    {
      xoap::SOAPElement detail = fault.addDetail();
      detail.addTextNode(faultDetail);
    }

  return reply;
}

xoap::MessageReference
tcds::utils::soap::makeCommandSOAPReply(std::string const& soapProtocolVersion,
                                        std::string const& command,
                                        std::string const& fsmState)
{
  xoap::MessageFactory* messageFactory = xoap::MessageFactory::getInstance(soapProtocolVersion);
  xoap::MessageReference reply = messageFactory->createMessage();
  xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
  xoap::SOAPBody body = envelope.getBody();
  std::string replyString = command + "Response";
  xoap::SOAPName replyName = envelope.createName(replyString, "xdaq", XDAQ_NS_URI);
  xoap::SOAPElement replyElement = body.addBodyElement(replyName);

  if (fsmState != "")
    {
      xoap::SOAPName elementName = envelope.createName("state", "xdaq", XDAQ_NS_URI);
      xoap::SOAPElement stateElement = replyElement.addChildElement(elementName);
      xoap::SOAPName attributeName = envelope.createName("stateName", "xdaq", XDAQ_NS_URI);
      stateElement.addAttribute(attributeName, fsmState);
    }

  return reply;
}

xoap::MessageReference
tcds::utils::soap::makeCommandSOAPReply(std::string const& soapProtocolVersion,
                                        std::string const& command,
                                        std::string const& replyMsg,
                                        std::string const& fsmState)
{

  xoap::MessageReference reply =
    tcds::utils::soap::makeCommandSOAPReply(soapProtocolVersion, command, fsmState);

  xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
  xoap::SOAPName elementName = envelope.createName("contents", "xdaq", XDAQ_NS_URI);
  xoap::SOAPElement replyElement = extractBodyNode(reply);
  xoap::SOAPElement msgElement = replyElement.addChildElement(elementName);
  msgElement.addTextNode(replyMsg);

  return reply;
}

std::string
tcds::utils::soap::extractSOAPCommandName(xoap::MessageReference const& msg)
{
  // The body should contain a single node with the name of the
  // command to execute.
  xoap::SOAPElement commandNode = extractBodyNode(msg);
  xoap::SOAPName commandSOAPName = commandNode.getElementName();
  std::string const commandName = commandSOAPName.getLocalName();

  return commandName;
}

std::string
tcds::utils::soap::extractSOAPCommandRCMSURL(xoap::MessageReference const& msg)
{
  // The body should contain a single node with the name of the
  // command to execute. The originator can include a new value of the
  // URL of where to send RCMS state change notifications using the
  // 'xdaq:rcmsURL' attribute.
  // NOTE: An empty string as rcmsURL is used to disable the
  // asynchronous state change notifications.
  xoap::SOAPElement commandNode = extractBodyNode(msg);
  xoap::SOAPName rcmsURLSOAPName("rcmsURL", "xdaq", "");
  std::string rcmsURL = commandNode.getAttributeValue(rcmsURLSOAPName);
  return rcmsURL;
}

uint32_t
tcds::utils::soap::extractSOAPCommandRCMSSessionId(xoap::MessageReference const& msg)
{
  // The body should contain a single node with the name of the
  // command to execute. The originator may identify itself using the
  // 'xdaq:rcmsSessionId' attribute.

  //  // NOTE: The default value here also serves to flag 'invalid'
  //  // values.
  uint32_t const kDefaultVal = 0;

  uint32_t rcmsSessionId = kDefaultVal;
  // NOTE: The 'xdaq:rcmsSessionId' attribute is optional.
  if (hasSOAPCommandAttribute(msg, "rcmsSessionId"))
    {
      // The attribute is present. Now extract it and check its value.
      xoap::SOAPElement commandNode = extractBodyNode(msg);
      xoap::SOAPName rcmsSessionIdSOAPName("rcmsSessionId", "xdaq", "");
      std::string rcmsSessionIdTmp = commandNode.getAttributeValue(rcmsSessionIdSOAPName);
      try
        {
          rcmsSessionId = parseIntFromString(rcmsSessionIdTmp);
        }
      catch(xcept::Exception& err)
        {
          std::string const msg =
            toolbox::toString("Failed to extract attribute 'xdaq:rcmsSessionId': '%s'.",
                              err.message().c_str());
          XCEPT_RAISE(tcds::exception::SOAPFormatProblem, msg);
        }

      // if (rcmsSessionId == kDefaultVal)
      //   {
      //     std::string const msg =
      //       toolbox::toString("The SOAP command attribute 'xdaq:rcmsSessionId' is optional, "
      //                         "but if present it should be non-zero.",
      //                         kDefaultVal);
      //     XCEPT_RAISE(tcds::exception::SOAPFormatProblem, msg);
      //   }
    }

  return rcmsSessionId;
}

std::string
tcds::utils::soap::extractSOAPCommandRequestorId(xoap::MessageReference const& msg)
{
  // The body should contain a single node with the name of the
  // command to execute. The originator may identify itself using the
  // 'xdaq:actionRequestorId' attribute.

  //  // NOTE: The default value here also serves to flag 'invalid'
  //  // values.
  std::string const kDefaultVal = "";

  std::string requestorId = kDefaultVal;
  // NOTE: The 'xdaq:actionRequestorId' attribute is optional.
  if (hasSOAPCommandAttribute(msg, "actionRequestorId"))
    {
      // The attribute is present. Now extract it.
      xoap::SOAPElement commandNode = extractBodyNode(msg);
      xoap::SOAPName requestorIdSOAPName("actionRequestorId", "xdaq", "");
      requestorId = commandNode.getAttributeValue(requestorIdSOAPName);

      // if (requestorId == kDefaultVal)
      //   {
      //     XCEPT_RAISE(tcds::exception::SOAPFormatProblem,
      //                 "The SOAP command attribute attribute 'xdaq:actionRequestorId' is optional, "
      //                 "but if present it should be non-empty.");
      //   }
    }

  return requestorId;
}

tcds::utils::OwnerId
tcds::utils::soap::extractSOAPCommandOwnerId(xoap::MessageReference const& msg)
{
  uint32_t const rcmsSessionId = extractSOAPCommandRCMSSessionId(msg);
  std::string const requestorId = extractSOAPCommandRequestorId(msg);

  return tcds::utils::OwnerId(rcmsSessionId, requestorId);
}

bool
tcds::utils::soap::hasFault(xoap::MessageReference const& msg)
{
  return msg->getSOAPPart().getEnvelope().getBody().hasFault();
}

bool
tcds::utils::soap::hasFaultDetail(xoap::MessageReference const& msg)
{
  bool res = false;
  if (tcds::utils::soap::hasFault(msg))
    {
      res = msg->getSOAPPart().getEnvelope().getBody().getFault().hasDetail();
    }
  return res;
}

std::string
tcds::utils::soap::extractFaultString(xoap::MessageReference const& msg)
{
  std::string res("");
  if (tcds::utils::soap::hasFault(msg))
    {
      xoap::SOAPFault fault = msg->getSOAPPart().getEnvelope().getBody().getFault();
      res = fault.getFaultString();
    }
  return res;
}

std::string
tcds::utils::soap::extractFaultDetail(xoap::MessageReference const& msg)
{
  std::string res("");
  if (tcds::utils::soap::hasFaultDetail(msg))
    {
      xoap::SOAPFault fault = msg->getSOAPPart().getEnvelope().getBody().getFault();
      res = fault.getDetail().getTextContent();
    }
  return res;
}

bool
tcds::utils::soap::hasSOAPCommandAttribute(xoap::MessageReference const& msg,
                                           std::string const& attributeName)
{
  bool res = false;
  xoap::SOAPElement commandNode = extractBodyNode(msg);
  xoap::SOAPName soapName(attributeName, "xdaq", "");
  try
    {
      // NOTE: Not very pretty, but this is what xoap::SOAPElement
      // does as well.
      DOMElement const* const node = dynamic_cast<DOMElement*>(commandNode.getDOM());
      std::string const nameStr = soapName.getQualifiedName();
      res = node->hasAttribute(xoap::XStr(nameStr));
    }
  catch (DOMException& de)
    {
      std::string const msg =
        toolbox::toString("Failed to check SOAP message for 'xdaq:%s' attribute: '%s'.",
                          attributeName.c_str(),
                          xoap::XMLCh2String(de.msg).c_str());
      XCEPT_RAISE(tcds::exception::SOAPFormatProblem, msg);
    }

  return res;
}

bool
tcds::utils::soap::hasSOAPCommandParameter(xoap::MessageReference const& msg,
                                           std::string const& parameterName)
{
  bool res = false;
  try
    {
      extractSOAPCommandParameterElement(msg, parameterName);
      res = true;
    }
  catch(tcds::exception::SOAPFormatProblem const&)
    {
      // Nothing to be done.
    }
  return res;
}

bool
tcds::utils::soap::extractSOAPCommandParameterBool(xoap::MessageReference const& msg,
                                                   std::string const& parameterName)
{
  xdata::soap::Serializer serializer;
  xdata::Boolean tmpVal;
  xoap::SOAPElement element = extractSOAPCommandParameterElement(msg, parameterName);
  try
    {
      serializer.import(&tmpVal, element.getDOM());
    }
  catch(xcept::Exception& err)
    {
      std::string const msg =
        toolbox::toString("Failed to import SOAP command parameter 'xdaq:%s': '%s'.",
                          parameterName.c_str(),
                          err.message().c_str());
      XCEPT_RAISE(tcds::exception::SOAPFormatProblem, msg);
    }
  return bool(tmpVal);
}

std::string
tcds::utils::soap::extractSOAPCommandParameterString(xoap::MessageReference const& msg,
                                                     std::string const& parameterName)
{
  xdata::soap::Serializer serializer;
  xdata::String tmpVal;
  xoap::SOAPElement element = extractSOAPCommandParameterElement(msg, parameterName);
  try
    {
      serializer.import(&tmpVal, element.getDOM());
    }
  catch(xcept::Exception& err)
    {
      std::string const msg =
        toolbox::toString("Failed to import SOAP command parameter 'xdaq:%s': '%s'.",
                          parameterName.c_str(),
                          err.message().c_str());
      XCEPT_RAISE(tcds::exception::SOAPFormatProblem, msg);
    }
  return std::string(tmpVal);
}

uint32_t
tcds::utils::soap::extractSOAPCommandParameterUnsignedInteger(xoap::MessageReference const& msg,
                                                              std::string const& parameterName)
{
  xdata::soap::Serializer serializer;
  xdata::UnsignedInteger32 tmpVal;
  xoap::SOAPElement element = extractSOAPCommandParameterElement(msg, parameterName);
  try
    {
      serializer.import(&tmpVal, element.getDOM());
    }
  catch(xcept::Exception& err)
    {
      std::string const msg =
        toolbox::toString("Failed to import SOAP command parameter 'xdaq:%s': '%s'.",
                          parameterName.c_str(),
                          err.message().c_str());
      XCEPT_RAISE(tcds::exception::SOAPFormatProblem, msg);
    }
  return uint32_t(tmpVal);
}

xoap::SOAPElement
tcds::utils::soap::extractSOAPCommandParameterElement(xoap::MessageReference const& msg,
                                                      std::string const& parameterName)
{
  // The body should contain a single node with the name of the
  // command to execute, with (sometimes optional) parameters.
  xoap::SOAPElement commandNode = extractBodyNode(msg);
  xoap::SOAPName soapName(parameterName, "xdaq", "");
  xoap::SOAPElement element = extractChildNode(commandNode, soapName);
  return element;
}

void
tcds::utils::soap::addSOAPReplyParameter(xoap::MessageReference const& msg,
                                         std::string const& parameterName,
                                         bool const val)
{
  xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();
  xoap::SOAPName elementName = envelope.createName(parameterName,
                                                   "xdaq",
                                                   XDAQ_NS_URI);
  xoap::SOAPElement replyNode = extractBodyNode(msg);
  xoap::SOAPElement element = replyNode.addChildElement(elementName);

  xdata::soap::Serializer serializer;
  xdata::Boolean tmpVal(val);
  try
    {
      serializer.exportAll(&tmpVal, dynamic_cast<DOMElement*>(element.getDOM()), true);
    }
  catch(xcept::Exception& err)
    {
      std::string const msg =
        toolbox::toString("Failed to export SOAP command parameter 'xdaq:%s': '%s'.",
                          parameterName.c_str(),
                          err.message().c_str());
      XCEPT_RAISE(tcds::exception::RuntimeProblem, msg);
    }
}

void
tcds::utils::soap::addSOAPReplyParameter(xoap::MessageReference const& msg,
                                         std::string const& parameterName,
                                         std::string const& val)
{
  xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();
  xoap::SOAPName elementName = envelope.createName(parameterName,
                                                   "xdaq",
                                                   XDAQ_NS_URI);
  xoap::SOAPElement replyNode = extractBodyNode(msg);
  xoap::SOAPElement element = replyNode.addChildElement(elementName);

  xdata::soap::Serializer serializer;
  xdata::String tmpVal(val);
  try
    {
      serializer.exportAll(&tmpVal, dynamic_cast<DOMElement*>(element.getDOM()), true);
    }
  catch(xcept::Exception& err)
    {
      std::string const msg =
        toolbox::toString("Failed to export SOAP command parameter 'xdaq:%s': '%s'.",
                          parameterName.c_str(),
                          err.message().c_str());
      XCEPT_RAISE(tcds::exception::RuntimeProblem, msg);
    }
}

void
tcds::utils::soap::addSOAPReplyParameter(xoap::MessageReference const& msg,
                                         std::string const& parameterName,
                                         uint32_t const val)
{
  xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();
  xoap::SOAPName elementName = envelope.createName(parameterName,
                                                   "xdaq",
                                                   XDAQ_NS_URI);
  xoap::SOAPElement replyNode = extractBodyNode(msg);
  xoap::SOAPElement element = replyNode.addChildElement(elementName);

  xdata::soap::Serializer serializer;
  xdata::UnsignedInteger32 tmpVal(val);
  try
    {
      serializer.exportAll(&tmpVal, dynamic_cast<DOMElement*>(element.getDOM()), true);
    }
  catch(xcept::Exception& err)
    {
      std::string const msg =
        toolbox::toString("Failed to export SOAP command parameter 'xdaq:%s': '%s'.",
                          parameterName.c_str(),
                          err.message().c_str());
      XCEPT_RAISE(tcds::exception::RuntimeProblem, msg);
    }
}

xoap::SOAPElement
tcds::utils::soap::extractBodyNode(xoap::MessageReference const& msg)
{
  std::vector<xoap::SOAPElement> bodyList = extractBodyNodes(msg);
  if (bodyList.size() != 1)
    {
      XCEPT_RAISE(tcds::exception::SOAPFormatProblem,
                  toolbox::toString("Expected exactly one element "
                                    "in the body of the SOAP command message "
                                    "but found %d.", bodyList.size()));
    }
  return bodyList.at(0);
}

std::vector<xoap::SOAPElement>
tcds::utils::soap::extractBodyNodes(xoap::MessageReference const& msg)
{
  std::vector<xoap::SOAPElement> bodyList =
    msg->getSOAPPart().getEnvelope().getBody().getChildElements();
  std::vector<xoap::SOAPElement> res;
  // Remove 'empty' text nodes (e.g., line-breaks, etc.).
  for (std::vector<xoap::SOAPElement>::iterator i = bodyList.begin();
       i != bodyList.end();
       ++i)
    {
      if (i->getElementName().getQualifiedName() != "#text")
        {
          res.push_back(*i);
        }
    }
  return res;
}

xoap::SOAPElement
tcds::utils::soap::extractChildNode(xoap::SOAPElement& node,
                                    xoap::SOAPName& childName)
{
  std::vector<xoap::SOAPElement> children = node.getChildElements(childName);
  if (children.size() != 1)
    {
      XCEPT_RAISE(tcds::exception::SOAPFormatProblem,
                  toolbox::toString("Expected exactly one child element "
                                    "with name '%s' "
                                    "but found %d.",
                                    childName.getQualifiedName().c_str(),
                                    children.size()));
    }
  return children.at(0);
}
