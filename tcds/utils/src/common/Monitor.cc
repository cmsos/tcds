#include "tcds/utils/Monitor.h"

#include <cmath>
#include <cstdlib>
#include <exception>
#include <memory>
#include <set>
#include <sstream>
#include <unistd.h>

#include "toolbox/TimeInterval.h"
#include "toolbox/TimeVal.h"
#include "toolbox/string.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerFactory.h"
#include "xcept/Exception.h"
#include "xdaq/ApplicationDescriptor.h"

#include "tcds/exception/Exception.h"
#include "tcds/utils/ConfigurationInfoSpaceHandler.h"
#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/Lock.h"
#include "tcds/utils/LockGuard.h"
#include "tcds/utils/LogMacros.h"
#include "tcds/utils/MonitorItem.h"
#include "tcds/utils/MonitorUpdateEvent.h"
#include "tcds/utils/XDAQAppBase.h"

namespace toolbox {
  namespace exception {
    class Exception;
  }
}

tcds::utils::Monitor::Monitor(tcds::utils::XDAQAppBase& xdaqApp, std::string const& name) :
  TCDSObject(xdaqApp),
  logger_(xdaqApp.getApplicationLogger()),
  alarmName_("MonitoringAlarm"),
  isInAlarm_(false),
  alarmDescription_(""),
  timeStart_(toolbox::TimeVal::gettimeofday())
{
  if (name.empty())
    {
      timerName_ = toolbox::toString("MonitoringTimer_lid%d",
                                    getOwnerApplication().getApplicationDescriptor()->getLocalId());
    }
  else
    {
      timerName_ = toolbox::toString("%s_lid%d",
                                    name.c_str(),
                                    getOwnerApplication().getApplicationDescriptor()->getLocalId());
    }
  timerP_ = std::unique_ptr<toolbox::task::Timer>(toolbox::task::getTimerFactory()->createTimer(timerName_));
  timerP_->addExceptionListener(this);
}

tcds::utils::Monitor::~Monitor()
{
  stopMonitoring();
  try
    {
      toolbox::task::getTimerFactory()->removeTimer(timerName_);
    }
  catch (...)
    {
    }
  timerP_.reset();
}

void
tcds::utils::Monitor::addActionListener(toolbox::ActionListener* const listener)
{
  dispatcher_.addActionListener(listener);
}

void
tcds::utils::Monitor::removeActionListener(toolbox::ActionListener* const listener)
{
  dispatcher_.removeActionListener(listener);
}

void
tcds::utils::Monitor::startMonitoring()
{
  std::string const tmp = getOwnerApplication().cfgInfoSpaceP_->getString("monitoringInterval");
  toolbox::TimeInterval monitoringInterval;
  try
    {
      monitoringInterval.fromString(tmp);
    }
  catch (toolbox::exception::Exception& err)
    {
      std::string const msg =
        toolbox::toString("Failed to start monitoring thread. " \
                          "Failed to interpret '%s' as update interval.",
                          tmp.c_str());
      raiseMonitoringAlarm(msg);
      return;
    }

  // Check for zero-intervals. These imply no monitoring updates at
  // all.
  if (monitoringInterval != toolbox::TimeInterval(0))
    {
      // Just add a little bit of a random delay, so not all
      // monitoring applications run in-sync and create bandwidth
      // issues.
      // NOTE: This is especially important for the connections to the
      // hardware. At the hardware level we're often dealing with a
      // single, physical bus (e.g., the I2C bus on the uTCA carrier
      // boards) that just cannot be accessed by more than a single
      // entity at any point in time.
      float const delayRnd = (1. * rand() / RAND_MAX) * double(monitoringInterval);
      float const delaySec = std::floor(delayRnd);
      float const delayUsec = (delayRnd - delaySec) * 1000000;
      toolbox::TimeInterval const delay(delaySec, delayUsec);
      toolbox::TimeVal start = toolbox::TimeVal::gettimeofday() + delay;

      try
        {
          // This schedules periodic calls to
          // tcds::utils::Monitor::timeExpired().
          // NOTE: The way the XDAQ toolbox::task::Timer handles this,
          // it always waits for the callback to finish before
          // rescheduling. This is the right thing to do for periodic
          // regreshes, in case the update takes longer than the
          // update interval.
          timerP_->scheduleAtFixedRate(start, this, monitoringInterval, 0, "");
        }
      catch (xcept::Exception& err)
        {
          std::string const msg =
            toolbox::toString("Failed to activate the monitoring timer: '%s'.",
                              err.what());
          raiseMonitoringAlarm(msg);
        }
    }
  revokeMonitoringAlarm();
}

void
tcds::utils::Monitor::stopMonitoring()
{
  INFO("Stopping monitoring timer.");
  revokeMonitoringAlarm();
  if (timerP_.get() && timerP_->isActive())
    {
      timerP_->stop();
    }
  INFO("Monitoring timer stopped.");
}

tcds::utils::Monitor::StringPairVector
tcds::utils::Monitor::getFormattedItemSet(std::string const& itemSetName) const
{
  tcds::utils::Monitor::StringPairVector res;

  // Get the requested ItemSet and complain if it does not exist.
  MonitorItemMap::const_iterator itemSet = itemSets_.find(itemSetName);
  if (itemSet == itemSets_.end())
    {
      std::string msg = "No ItemSet with name '" +
        itemSetName +
        "' exists in the monitor.";
      ERROR(msg);
      XCEPT_RAISE(tcds::exception::SoftwareProblem, msg);
    }

  std::vector<std::pair<std::string, MonitorItem> > items = itemSet->second;
  std::vector<std::pair<std::string, MonitorItem> >::const_iterator iter;
  for (iter = items.begin(); iter != items.end(); ++iter)
    {
      MonitorItem const item = iter->second;
      std::string const desc = item.getDescription();
      std::string value = "";
      try
        {
          value = item.getInfoSpaceHandler()->getFormatted(item.getName());
        }
      catch (tcds::exception::Exception& err)
        {
          std::string const msg =
            toolbox::toString("Failed to format monitoring item '%s': %s.",
                              item.getName().c_str(),
                              err.what());
          ERROR(msg);
          XCEPT_RAISE(tcds::exception::SoftwareProblem, msg);
        }
      res.push_back(std::make_pair(desc, value));
    }

  return res;
}

tcds::utils::Monitor::StringPairVector
tcds::utils::Monitor::getItemSetDocStrings(std::string const& itemSetName) const
{
  tcds::utils::Monitor::StringPairVector res;

  // Get the requested ItemSet and complain if it does not exist.
  MonitorItemMap::const_iterator itemSet = itemSets_.find(itemSetName);
  if (itemSet == itemSets_.end())
    {
      std::string msg = "No ItemSet with name '" +
        itemSetName +
        "' exists in the monitor.";
      ERROR(msg);
      XCEPT_RAISE(tcds::exception::SoftwareProblem, msg);
    }

  std::vector<std::pair<std::string, MonitorItem> > items = itemSet->second;
  std::vector<std::pair<std::string, MonitorItem> >::const_iterator iter;
  for (iter = items.begin(); iter != items.end(); ++iter)
    {
      MonitorItem const item = iter->second;
      std::string const desc = item.getDescription();
      std::string const docString = item.getDocString();
      res.push_back(std::make_pair(desc, docString));
    }

  return res;
}

void
tcds::utils::Monitor::onException(xcept::Exception& err)
{
  handleException(err);
}

void
tcds::utils::Monitor::handleException(xcept::Exception& err)
{
  std::string msg =
    toolbox::toString("Monitoring failure detected: '%s'.", err.what());
  ERROR(msg);
  raiseMonitoringAlarm(msg);
}

void
tcds::utils::Monitor::addInfoSpace(InfoSpaceHandler* const infoSpace)
{
  std::string name = infoSpace->name();
  std::tr1::unordered_map<std::string, InfoSpaceHandler*>::iterator iter =
    infoSpaceMap_.find(name);
  if (iter == infoSpaceMap_.end())
    {
      DEBUG("Adding InfoSpace '"
            << name
            << "' to monitoring list.");
      infoSpaceMap_.insert(std::make_pair(name, infoSpace));
    }
}

void
tcds::utils::Monitor::addItem(std::string const& itemSetName,
                              std::string const& itemName,
                              std::string const& itemDesc,
                              InfoSpaceHandler* const infoSpaceHandler,
                              std::string const& docString)
{
  // Find the ItemSet into which this item should go.
  typedef std::tr1::unordered_map<std::string, std::vector<std::pair<std::string, MonitorItem> > >::iterator SetListIter;
  SetListIter set = itemSets_.find(itemSetName);
  if (set == itemSets_.end())
    {
      std::string msg = "ItemSet with name '" +
        itemSetName +
        "' does not exist in the monitor.";
      FATAL(msg);
      XCEPT_RAISE(tcds::exception::SoftwareProblem, msg);
    }

  MonitorItem item(itemName, itemDesc, infoSpaceHandler, docString);
  (*set).second.push_back(std::make_pair(itemName, item));

  addInfoSpace(infoSpaceHandler);
}

void
tcds::utils::Monitor::newItemSet(std::string const& itemSetName)
{
  if (itemSets_.find(itemSetName) != itemSets_.end())
    {
      std::string msg = "ItemSet with name '" +
        itemSetName +
        "' already exists in the monitor.";
      FATAL(msg);
      XCEPT_RAISE(tcds::exception::SoftwareProblem, msg);
    }

  std::vector<std::pair<std::string, MonitorItem> > emptySet;
  itemSets_.insert(std::make_pair(itemSetName, emptySet));
}

void
tcds::utils::Monitor::timeExpired(toolbox::task::TimerEvent& event)
{
  updateAllInfoSpaces();
}

void
tcds::utils::Monitor::updateAllInfoSpaces()
{
  // Keep track of how long this takes.
  toolbox::TimeVal timeBegin = toolbox::TimeVal::gettimeofday();

  bool success = true;

  // NOTE: It is possible that this loop spans multiple instances of
  // the same type of InfoSpaceHandler. So it is not impossible to
  // encounter the same problem multiple times. Hence the fiddling
  // with the set.
  std::set<std::string> problems;
  std::tr1::unordered_map<std::string, InfoSpaceHandler*>::iterator iter;
  for (iter = infoSpaceMap_.begin(); iter != infoSpaceMap_.end(); ++iter)
    {
      // NOTE: One should be aware of the presence of this lock: any
      // monitoring update blocks anything else that depends on this
      // lock. It has been placed deliberately inside the loop over
      // InfoSpaces.
      std::unique_ptr<LockGuard<Lock> > guardedLock(new LockGuard<Lock>(getOwnerApplication().monitoringLock_));

      try
        {
          iter->second->update();
        }
      catch (xcept::Exception& err)
        {
          problems.insert(err.what());
          success = false;
        }
      catch (std::exception const& err)
        {
          problems.insert(err.what());
          success = false;
        }

      guardedLock.reset();
      ::usleep(kLoopRelaxTime);
    }

  if (success)
    {
      revokeMonitoringAlarm();
    }
  else
    {
      std::string const msgBase = "Problem" + std::string(problems.size() == 1 ? "" : "s") + " updating monitoring information";
      std::string msg = msgBase + ": ";
      for (std::set<std::string>::const_iterator it = problems.begin();
           it != problems.end();
           ++it)
        {
          if (it != problems.begin())
            {
              msg += ", ";
            }
          msg += "'" + *it + "'";
        }
      // Log the problem.
      ERROR(msg);
      raiseMonitoringAlarm(msg);
    }

  toolbox::TimeVal timeEnd = toolbox::TimeVal::gettimeofday();

  // Update the uptime estimate and the timestamp of the latest
  // monitoring update.
  toolbox::TimeVal timeNow(toolbox::TimeVal::gettimeofday());
  toolbox::TimeInterval upTime = timeNow - timeStart_;
  getOwnerApplication().appStateInfoSpace_.setDouble("upTime",
                                                     double(upTime));
  getOwnerApplication().appStateInfoSpace_.setTimeVal("latestMonitoringUpdate",
                                                      double(timeNow));
  getOwnerApplication().appStateInfoSpace_.setDouble("latestMonitoringUpdateDouble",
                                                     double(timeNow));
  getOwnerApplication().appStateInfoSpace_.setDouble("latestMonitoringDuration",
                                                     double(timeEnd - timeBegin));
  getOwnerApplication().appStateInfoSpace_.writeInfoSpace();

  // Tell everyone who registered as interested that another pass of
  // the monitoring has been completed.
  MonitorUpdateEvent event(this);
  dispatcher_.fireEvent(event);
}

void
tcds::utils::Monitor::raiseMonitoringAlarm(std::string const& problemDesc) const
{
  // Set our internal flag.
  // NOTE: We don't care if this was already set or not. If we
  // encounter a new monitoring problem, we just overwrite the
  // existing one.
  bool const wasInAlarm = isInAlarm_;
  bool const alarmHasChanged = (problemDesc != alarmDescription_);
  isInAlarm_ = true;
  alarmDescription_ = problemDesc;

  // Log the problem.
  ERROR("Encountered a monitoring alarm: '" << problemDesc << "'.");

  // Raise the alarm, unless we have already done so for the current
  // set of problems.
  // NOTE: This allows the alarm to be acknowledged in e.g. HotSpot.
  if (!wasInAlarm || alarmHasChanged)
    {
      if (!wasInAlarm) {
        ERROR("Raising monitoring alarm: '" << problemDesc << "'.");
      } else {
        ERROR("Updating monitoring alarm: '" << problemDesc << "'.");
      }
      XCEPT_DECLARE(tcds::exception::MonitoringFailureAlarm, alarmException, problemDesc);
      getOwnerApplication().raiseAlarm(alarmName_, alarmException);
    }

  // Let the application do whatever (else) it needs to do.
  getOwnerApplication().handleMonitoringProblem(problemDesc);
}

void
tcds::utils::Monitor::revokeMonitoringAlarm() const
{
  // Only do something if we're actually in an alarm condition.
  if (isInAlarm_)
    {
      // Log the action.
      INFO("Revoking monitoring alarm.");

      // Revoke the alarm.
      getOwnerApplication().revokeAlarm(alarmName_);
      getOwnerApplication().appStateInfoSpace_.removeMonitoringProblem();

      // Update our internal flags.
      isInAlarm_ = false;
      alarmDescription_ = "";

    }
}
