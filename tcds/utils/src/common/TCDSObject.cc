#include "tcds/utils/TCDSObject.h"

tcds::utils::TCDSObject::TCDSObject(tcds::utils::XDAQAppBase& owner) :
  owner_(owner)
{
}

tcds::utils::TCDSObject::~TCDSObject()
{
}

tcds::utils::XDAQAppBase&
tcds::utils::TCDSObject::getOwnerApplication() const
{
  return owner_;
}
