#include "tcds/utils/XDAQObject.h"

tcds::utils::XDAQObject::XDAQObject(xdaq::Application& owner) :
  owner_(owner)
{
}

tcds::utils::XDAQObject::~XDAQObject()
{
}

xdaq::Application&
tcds::utils::XDAQObject::getOwnerApplication() const
{
  return owner_;
}
