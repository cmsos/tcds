#include "tcds/utils/InfoSpaceItem.h"

#include <cassert>

tcds::utils::InfoSpaceItem::InfoSpaceItem(std::string const& name,
                                          ItemType const type,
                                          std::string const& format,
                                          UpdateType const updateType,
                                          bool const isValid) :
  name_(name),
  hwName_(name),
  type_(type),
  format_(format),
  updateType_(updateType),
  isValid_(isValid)
{
  assert (!hwName_.empty());
}

tcds::utils::InfoSpaceItem::InfoSpaceItem(std::string const& name,
                                          std::string const& hwName,
                                          ItemType const type,
                                          std::string const& format,
                                          UpdateType const updateType,
                                          bool const isValid) :
  name_(name),
  hwName_(hwName),
  type_(type),
  format_(format),
  updateType_(updateType),
  isValid_(isValid)
{
  assert (!hwName_.empty());
}

std::string
tcds::utils::InfoSpaceItem::name() const
{
  return name_;
}

std::string
tcds::utils::InfoSpaceItem::hwName() const
{
  return hwName_;
}

tcds::utils::InfoSpaceItem::ItemType
tcds::utils::InfoSpaceItem::type() const
{
  return type_;
}

std::string
tcds::utils::InfoSpaceItem::format() const
{
  return format_;
}

tcds::utils::InfoSpaceItem::UpdateType
tcds::utils::InfoSpaceItem::updateType() const
{
  return updateType_;
}

bool
tcds::utils::InfoSpaceItem::isValid() const
{
  return isValid_;
}

void
tcds::utils::InfoSpaceItem::setValid()
{
  isValid_ = true;
}

void
tcds::utils::InfoSpaceItem::setInvalid()
{
  isValid_ = false;
}

bool
tcds::utils::operator==(tcds::utils::InfoSpaceItem const& item, std::string const& name)
{
  return (item.name_ == name);
}

bool
tcds::utils::operator==(std::string const& name, tcds::utils::InfoSpaceItem const& item)
{
  return (item == name);
}
