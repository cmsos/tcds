#include "tcds/utils/AppHistItem.h"

tcds::utils::AppHistItem::AppHistItem(std::string const& msg) :
  msg_(msg),
  count_(1)
{
  timestamps_.push_back(toolbox::TimeVal::gettimeofday());
}

tcds::utils::AppHistItem::~AppHistItem()
{
}

void
tcds::utils::AppHistItem::addOccurrence()
{
  ++count_;
  timestamps_.push_back(toolbox::TimeVal::gettimeofday());
}

toolbox::TimeVal
tcds::utils::AppHistItem::timestamp() const
{
  return timestamps_.front();
}

toolbox::TimeVal
tcds::utils::AppHistItem::timestampLastOccurrence() const
{
  return timestamps_.back();
}

std::string
tcds::utils::AppHistItem::message() const
{
  return msg_;
}

unsigned int
tcds::utils::AppHistItem::count() const
{
  return count_;
}
