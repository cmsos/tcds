#include "tcds/utils/Utils.h"

#include <algorithm>
#include <cassert>
#include <cctype>
#include <cerrno>
#include <climits>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iomanip>
#include <list>
#include <sstream>
#include <cstddef>
#include <stdexcept>
#include <sys/stat.h>
#include <zconf.h>
#include <zlib.h>

#include "toolbox/Runtime.h"
#include "toolbox/TimeVal.h"
#include "toolbox/string.h"
#include "xcept/Exception.h"
#include "xdata/exception/Exception.h"
#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "xdata/exdr/Serializer.h"
#include "xdata/TableIterator.h"

#include "tcds/exception/Exception.h"
#include "tcds/utils/ConfigurationInfoSpaceHandler.h"
#include "tcds/utils/CurlGetter.h"
#include "tcds/utils/Definitions.h"

namespace toolbox {
  namespace exception {
  class Exception;
  }
}

long
tcds::utils::parseIntFromString(std::string const& numberAsString)
{
  errno = 0;
  char* tmp = 0;
  char const* const rawStr = numberAsString.c_str();

  long const res = strtol(rawStr, &tmp, 0);

  if ((tmp == rawStr) ||
      (*tmp != '\0') ||
      ((errno == ERANGE) && ((res == LONG_MIN) || (res == LONG_MAX))))
    {
      std::string const msg =
        "Failed to convert '" + numberAsString + "' to an integer";
      XCEPT_RAISE(tcds::exception::ValueError, msg);
    }

  return res;
}

unsigned int
tcds::utils::coerceBXNumber(int const bxNumber)
{
  // NOTE: This method assumes that the input BX number is at most one
  // orbit out of range.
  int tmp = bxNumber;
  if (tmp < int(tcds::definitions::kFirstBX))
    {
      tmp += tcds::definitions::kNumBXPerOrbit;
    }
  else if (tmp > int(tcds::definitions::kLastBX))
    {
      tmp -= tcds::definitions::kNumBXPerOrbit;
    }
  unsigned int res = tmp;
  return res;
}

unsigned int
tcds::utils::bucketToBX(unsigned int const bucketNumber)
{
  return (((bucketNumber - 1) / 10) + 1);
}

std::string
tcds::utils::expandPathName(std::string const& pathNameIn)
{
  std::vector<std::string> expandedPaths;
  try
    {
      expandedPaths = toolbox::getRuntime()->expandPathName(pathNameIn);
    }
  catch (toolbox::exception::Exception const&)
    {
      std::string const msgBase = "Cannot expand filename";
      XCEPT_RAISE(tcds::exception::ConfigurationProblem,
                  toolbox::toString("%s %s.", msgBase.c_str(), pathNameIn.c_str()));
    }

  if (expandedPaths.size() != 1)
    {
      XCEPT_RAISE(tcds::exception::ConfigurationProblem,
                  toolbox::toString("Expanding %s leads to ambiguities.",
                                    pathNameIn.c_str()));
    }

  return expandedPaths.at(0);
}

bool
tcds::utils::pathExists(std::string const& pathName)
{
  struct stat sb;
  memset(&sb, 0, sizeof(sb));
  return (stat(pathName.c_str(), &sb) == 0);
}

bool
tcds::utils::dirExists(std::string const& dirName)
{
  bool res = false;
  bool const exists = pathExists(dirName);
  if (exists)
    {
      res = pathIsDir(dirName);
    }
  return res;
}

bool
tcds::utils::pathIsDir(std::string const& pathName)
{
  // NOTE: This implementation is not super complete, nor pretty.
  bool res = false;
  if (pathExists(pathName))
    {
      struct stat sb;
      memset(&sb, 0, sizeof(sb));
      if (stat(pathName.c_str(), &sb) == 0)
        {
          res = S_ISDIR(sb.st_mode);
        }
    }
  return res;
}

void
tcds::utils::makeDir(std::string const& dirName)
{
  std::list<std::string> const pieces =
    toolbox::parseTokenList(dirName, "/");

  std::string tmp = "";
  for (std::list<std::string>::const_iterator it = pieces.begin();
       it != pieces.end();
       ++it)
    {
      tmp += ("/" + *it);
      if (mkdir(tmp.c_str(), S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH) != 0)
        {
          if (errno == EEXIST)
            {
              if (!pathIsDir(tmp))
                {
                  // Path already exists but is not a directory.
                  std::string const msg =
                    toolbox::toString("Could not create directory '%s': path '%s' already exists but is not a directory.",
                                      dirName.c_str(),
                                      tmp.c_str());
                  XCEPT_RAISE(tcds::exception::RuntimeProblem, msg.c_str());
                }
            }
          else
            {
              // Something went wrong, but what?
              std::string const msg =
                toolbox::toString("Could not create directory '%s': '%s'.",
                                  dirName.c_str(),
                                  std::strerror(errno));
              XCEPT_RAISE(tcds::exception::RuntimeProblem, msg.c_str());
            }
        }
    }
}
// BUG BUG BUG end

void
tcds::utils::rename(std::string const& from, std::string const& to)
{
  int const res = std::rename(from.c_str(), to.c_str());
  if (res != 0)
    {
      // Something went wrong...
      std::string const msg =
        toolbox::toString("Could not create rename '%s' to '%s': '%s'.",
                          from.c_str(),
                          to.c_str(),
                          std::strerror(errno));
      XCEPT_RAISE(tcds::exception::RuntimeProblem, msg.c_str());
    }
}

/**
 * Escape a string such that it's safe to embed as string in JSON
 * format. Takes care of slashes, backslashes, double quotes, and
 * special characters like tabs and line feeds.
 */
std::string
tcds::utils::escapeAsJSONString(std::string const& stringIn)
{
  return "\"" + toolbox::jsonquote(stringIn) + "\"";
}

std::string
tcds::utils::trimString(std::string const& stringToTrim,
                        std::string const& whitespace)
{
  size_t const strBegin = stringToTrim.find_first_not_of(whitespace);
  if (strBegin == std::string::npos)
    {
      // No content at all in this string.
      return "";
    }

  size_t const strEnd = stringToTrim.find_last_not_of(whitespace);
  size_t const strRange = strEnd - strBegin + 1;

  return stringToTrim.substr(strBegin, strRange);
}

std::string
tcds::utils::capitalizeString(std::string const& stringIn)
{
  std::string res = stringIn;
  if (!stringIn.empty())
    {
      // std::transform(res.begin(), res.end(), res.begin(), convertDown());
      // res[0] = convertUp()(res[0]);
      std::transform(res.begin(), res.end(), res.begin(), ::tolower);
      res[0] = ::toupper(res[0]);
    }
  return res;
}

std::string
tcds::utils::chunkifyFEDVector(std::string const& fedVecIn)
{
  // This thing can become annoyingly long. The idea here is to insert
  // some zero-width spaces in order to make it break into pieces when
  // formatted in the web interface.
  // NOTE: Not very efficient/pretty, but it works.

  std::string const zeroWidthSpace = "&#8203;";
  std::string const separator = "%";

  std::string fedVecOut = "";
  std::list<std::string> pieces = toolbox::parseTokenList(fedVecIn, separator);
  for (std::list<std::string>::const_iterator i = pieces.begin();
       i != pieces.end();
       ++i)
    {
      if (!fedVecOut.empty())
        {
          fedVecOut += zeroWidthSpace;
        }
      fedVecOut += (*i + separator);
    }

  return fedVecOut;
}

std::string
tcds::utils::formatBchannelNameString(tcds::definitions::BGO_NUM const bchannelNumber)
{
  std::string res;
  std::string bchannelName =
    tcds::utils::bgoNumberToName(static_cast<tcds::definitions::BGO_NUM>(bchannelNumber)).c_str();
  if (toolbox::startsWith(bchannelName, "Bgo"))
    {
      res = toolbox::toString("B-channel %d", bchannelNumber);
    }
  else
    {
      res = toolbox::toString("B-channel %d (%s)", bchannelNumber, bchannelName.c_str());
    }
  return res;
}

std::string
tcds::utils::formatBgoNameString(tcds::definitions::BGO_NUM const bgoNumber)
{
  std::string res;
  std::string bgoName =
    tcds::utils::bgoNumberToName(static_cast<tcds::definitions::BGO_NUM>(bgoNumber)).c_str();
  if (toolbox::startsWith(bgoName, "Bgo"))
    {
      res = toolbox::toString("B-go %d", bgoNumber);
    }
  else
    {
      res = toolbox::toString("B-go %d (%s)", bgoNumber, bgoName.c_str());
    }
  return res;
}

std::string
tcds::utils::formatSequenceNameString(tcds::definitions::SEQUENCE_NUM const sequenceNumber)
{
  std::string res;
  std::string sequenceName =
    tcds::utils::sequenceNumberToName(static_cast<tcds::definitions::SEQUENCE_NUM>(sequenceNumber)).c_str();
  if (toolbox::startsWith(sequenceName, "Sequence"))
    {
      res = toolbox::toString("Sequence %d", sequenceNumber);
    }
  else
    {
      res = toolbox::toString("Sequence %d (%s)", sequenceNumber, sequenceName.c_str());
    }
  return res;
}

std::string
tcds::utils::getTimestamp(bool const isoFormat,
                          bool const shortIsoFormat)
{
  std::string res;
  toolbox::TimeVal const timeVal = toolbox::TimeVal::gettimeofday();
  if (isoFormat)
    {
      if (shortIsoFormat)
        {
          res = tcds::utils::formatTimestampISO(timeVal, true);
        }
      else
        {
          res = tcds::utils::formatTimestampISO(timeVal);
        }
    }
  else
    {
      res = tcds::utils::formatTimestamp(timeVal);
    }

  return res;
}

std::string
tcds::utils::formatTimestamp(toolbox::TimeVal const timestamp,
                             toolbox::TimeVal::TimeZone const tz,
                             bool const longFormat)
{
  std::string res = timestamp.toString("%Y-%m-%d %H:%M:%S", tz);
  if (longFormat)
    {
      res += toolbox::toString(".%06u", timestamp.usec());
    }
  if (tz == toolbox::TimeVal::gmt)
    {
      res += " UTC";
    }
  return res;
}

std::string
tcds::utils::formatTimestampISO(toolbox::TimeVal const timestamp,
                                bool const shortIsoFormat)
{
  return formatTimestampISO(timestamp, toolbox::TimeVal::gmt, shortIsoFormat);
}

std::string
tcds::utils::formatTimestampISO(toolbox::TimeVal const timestamp,
                                toolbox::TimeVal::TimeZone const tz,
                                bool const shortIsoFormat)
{
  // NOTE: This code is not very elegant. But it does have the benefit
  // of sharing its core with the 'default' XDAQ TimeVal
  // implementation.
  std::string res = timestamp.toString("", tz);
  if (shortIsoFormat)
    {
      // Remove the decimal part.
      size_t const pos = res.find(".");
      res.erase(std::remove_if(res.begin() + pos, res.end(), &isdigit), res.end());
      // Remove the ':' and '.'.
      res.erase(std::remove_if(res.begin(), res.end(), &ispunct), res.end());
    }
  return res;
}

std::string
tcds::utils::formatTimestampDate(toolbox::TimeVal const timestamp,
                                 toolbox::TimeVal::TimeZone const tz)
{
  std::string const res = timestamp.toString("%Y-%m-%d", tz);
  return res;
}

std::string
tcds::utils::formatTimestampTime(toolbox::TimeVal const timestamp,
                                 toolbox::TimeVal::TimeZone const tz,
                                 bool const longFormat)
{
  std::string res = timestamp.toString("%H:%M:%S", tz);
  if (longFormat)
    {
      res += toolbox::toString(".%06u", timestamp.usec());
    }
  if (tz == toolbox::TimeVal::gmt)
    {
      res += " UTC";
    }
  return res;
}

std::string
tcds::utils::formatTimeRange(toolbox::TimeVal const timeBegin,
                             toolbox::TimeVal const timeEnd,
                             toolbox::TimeVal::TimeZone const tz,
                             bool const longFormat)
{
  std::stringstream result;

  time_t const secBegin = timeBegin.sec();
  struct tm tmpBegin;
  gmtime_r(&secBegin, &tmpBegin);
  time_t const secEnd = timeEnd.sec();
  struct tm tmpEnd;
  gmtime_r(&secEnd, &tmpEnd);

  // Case 1: begin and end times on the same day.
  if ((tmpBegin.tm_year == tmpEnd.tm_year) &&
      (tmpBegin.tm_mon == tmpEnd.tm_mon) &&
      (tmpBegin.tm_mday == tmpEnd.tm_mday))
    {
      // Show the day once, and the times individually.
      result << tcds::utils::formatTimestampDate(timeBegin, tz)
             << " "
             << tcds::utils::formatTimestampTime(timeBegin, tz, longFormat)
             << " - "
             << tcds::utils::formatTimestampTime(timeEnd, tz, longFormat);
    }
  // Case 2: begin and end times on different days.
  else
    {
      // Show the full begin and end dates plus times individually.
      result << tcds::utils::formatTimestamp(timeBegin, tz, longFormat)
             << " - "
             << tcds::utils::formatTimestamp(timeEnd, tz, longFormat);
    }

  return result.str();
}

std::string
tcds::utils::formatDeltaTString(toolbox::TimeVal const timeBegin,
                                toolbox::TimeVal const timeEnd,
                                bool const verbose)
{
  std::stringstream result;
  toolbox::TimeVal deltaT = timeEnd - timeBegin;
  if (deltaT.sec() != 0)
    {
      result << deltaT.sec() << " second";
      if(deltaT.sec() > 1)
        {
          result << "s";
        }
    }
  if (deltaT.millisec() != 0)
    {
      if (result.str().size() != 0)
        {
          result << " and ";
        }
      result << deltaT.millisec() << " millisecond";
      if (deltaT.millisec() > 1)
        {
          result << "s";
        }
    }
  if (result.str().size() == 0)
    {
      result << "negligible time";
    }

  // If requested: add the begin and end times explicitly.
  if (verbose)
    {
      result << " ("
             << "from: " << timeBegin.toString(toolbox::TimeVal::gmt)
             << ", "
             << "to: " << timeEnd.toString(toolbox::TimeVal::gmt)
             << ")";
    }

  return result.str();
}

std::map<std::string, tcds::definitions::BGO_NUM>
tcds::utils::bgoNameMap()
{
  std::map<std::string, tcds::definitions::BGO_NUM> bgoNames;

  bgoNames["LumiNibble"]        = tcds::definitions::BGO_BGO0;
  bgoNames["BC0"]               = tcds::definitions::BGO_BC0;
  bgoNames["TestEnable"]        = tcds::definitions::BGO_TESTENABLE;
  bgoNames["PrivateGap"]        = tcds::definitions::BGO_PRIVATEGAP;
  bgoNames["PrivateOrbit"]      = tcds::definitions::BGO_PRIVATEORBIT;
  bgoNames["Resync"]            = tcds::definitions::BGO_RESYNC;
  bgoNames["HardReset"]         = tcds::definitions::BGO_HARDRESET;
  bgoNames["EC0"]               = tcds::definitions::BGO_EC0;
  bgoNames["OC0"]               = tcds::definitions::BGO_OC0;
  bgoNames["Start"]             = tcds::definitions::BGO_START;
  bgoNames["Stop"]              = tcds::definitions::BGO_STOP;
  bgoNames["StartOfGap"]        = tcds::definitions::BGO_STARTOFGAP;
  bgoNames["Bgo12"]             = tcds::definitions::BGO_BGO12;
  bgoNames["WarningTestEnable"] = tcds::definitions::BGO_WARNINGTESTENABLE;
  bgoNames["Bgo14"]             = tcds::definitions::BGO_BGO14;
  bgoNames["Bgo15"]             = tcds::definitions::BGO_BGO15;

  // The new B-gos don't have any fancy names...
  for (int i = tcds::definitions::kBgoNumNewMin;
       i <= tcds::definitions::kBgoNumNewMax;
       ++i)
    {
      bgoNames[toolbox::toString("Bgo%u", unsigned(i))] =
        static_cast<tcds::definitions::BGO_NUM>(i);
    }

  return bgoNames;
}

tcds::definitions::BGO_NUM
tcds::utils::bgoNameToNumber(std::string const& bgoName)
{
  std::map<std::string, tcds::definitions::BGO_NUM> bgoMap = bgoNameMap();
  std::map<std::string, tcds::definitions::BGO_NUM>::const_iterator i = bgoMap.find(bgoName);
  if (i == bgoMap.end())
    {
      XCEPT_RAISE(tcds::exception::ValueError,
                  toolbox::toString("'%s' is not a valid B-go name.",
                                    bgoName.c_str()));
    }
  return bgoMap[bgoName];
}

std::string
tcds::utils::bgoNumberToName(tcds::definitions::BGO_NUM const& bgoNumber)
{
  static std::map<std::string, tcds::definitions::BGO_NUM> bgoMap = bgoNameMap();
  std::map<std::string, tcds::definitions::BGO_NUM>::const_iterator res = bgoMap.end();
  std::map<std::string, tcds::definitions::BGO_NUM>::const_iterator i;
  for (i = bgoMap.begin(); i != bgoMap.end(); ++i)
    {
      if (i->second == bgoNumber)
        {
          res = i;
          break;
        }
    }
  if (res == bgoMap.end())
    {
      XCEPT_RAISE(tcds::exception::ValueError,
                  toolbox::toString("'%d' is not a valid B-go number.",
                                    bgoNumber));
    }
  return res->first;
}

tcds::definitions::BCHANNEL_MODE
tcds::utils::bchannelModeFromString(std::string const bchannelModeString)
{
  if (bchannelModeString == "off")
    {
      return tcds::definitions::BCHANNEL_MODE_OFF;
    }
  else if (bchannelModeString == "single")
    {
      return tcds::definitions::BCHANNEL_MODE_SINGLE;
    }
  else if (bchannelModeString == "double")
    {
      return tcds::definitions::BCHANNEL_MODE_DOUBLE;
    }
  else if (bchannelModeString == "block")
    {
      return tcds::definitions::BCHANNEL_MODE_BLOCK;
    }
  else
    {
      XCEPT_RAISE(tcds::exception::ValueError,
                  toolbox::toString("'%s' is not a valid B-channel mode.",
                                    bchannelModeString.c_str()));
    }
}

std::string
tcds::utils::bchannelModeToString(tcds::definitions::BCHANNEL_MODE const bchannelMode)
{
  std::string res = "unknown";

  switch (bchannelMode)
    {
    case tcds::definitions::BCHANNEL_MODE_MISCONFIGURED:
      res = "misconfigured";
      break;
    case tcds::definitions::BCHANNEL_MODE_OFF:
      res = "off";
      break;
    case tcds::definitions::BCHANNEL_MODE_SINGLE:
      res = "single";
      break;
    case tcds::definitions::BCHANNEL_MODE_DOUBLE:
      res = "double";
      break;
    case tcds::definitions::BCHANNEL_MODE_BLOCK:
      res = "block";
      break;
    default:
      XCEPT_RAISE(tcds::exception::ValueError,
                  toolbox::toString("'%d' is not a valid B-channel mode.",
                                    bchannelMode));
      break;
    }
  return res;
}

std::map<std::string, tcds::definitions::SEQUENCE_NUM>
tcds::utils::sequenceNameMap()
{
  std::map<std::string, tcds::definitions::SEQUENCE_NUM> sequenceNames;

  sequenceNames["Start"]       = tcds::definitions::SEQUENCE_START;
  sequenceNames["Stop"]        = tcds::definitions::SEQUENCE_STOP;
  sequenceNames["Pause"]       = tcds::definitions::SEQUENCE_PAUSE;
  sequenceNames["Resume"]      = tcds::definitions::SEQUENCE_RESUME;
  sequenceNames["Resync"]      = tcds::definitions::SEQUENCE_RESYNC;
  sequenceNames["HardReset"]   = tcds::definitions::SEQUENCE_HARDRESET;
  sequenceNames["Calibration"] = tcds::definitions::SEQUENCE_CALIBRATION;
  sequenceNames["Sequence7"]   = tcds::definitions::SEQUENCE_SEQUENCE7;
  sequenceNames["Sequence8"]   = tcds::definitions::SEQUENCE_SEQUENCE8;
  sequenceNames["Sequence9"]   = tcds::definitions::SEQUENCE_SEQUENCE9;
  sequenceNames["Sequence10"]  = tcds::definitions::SEQUENCE_SEQUENCE10;
  sequenceNames["Sequence11"]  = tcds::definitions::SEQUENCE_SEQUENCE11;
  sequenceNames["Sequence12"]  = tcds::definitions::SEQUENCE_SEQUENCE12;
  sequenceNames["Sequence13"]  = tcds::definitions::SEQUENCE_SEQUENCE13;
  sequenceNames["Sequence14"]  = tcds::definitions::SEQUENCE_SEQUENCE14;
  sequenceNames["Sequence15"]  = tcds::definitions::SEQUENCE_SEQUENCE15;
  sequenceNames["Sequence16"]  = tcds::definitions::SEQUENCE_SEQUENCE16;
  sequenceNames["Sequence17"]  = tcds::definitions::SEQUENCE_SEQUENCE17;
  sequenceNames["Sequence18"]  = tcds::definitions::SEQUENCE_SEQUENCE18;
  sequenceNames["Sequence19"]  = tcds::definitions::SEQUENCE_SEQUENCE19;
  sequenceNames["Sequence20"]  = tcds::definitions::SEQUENCE_SEQUENCE20;
  sequenceNames["Sequence21"]  = tcds::definitions::SEQUENCE_SEQUENCE21;
  sequenceNames["Sequence22"]  = tcds::definitions::SEQUENCE_SEQUENCE22;
  sequenceNames["Sequence23"]  = tcds::definitions::SEQUENCE_SEQUENCE23;
  sequenceNames["Sequence24"]  = tcds::definitions::SEQUENCE_SEQUENCE24;
  sequenceNames["Sequence25"]  = tcds::definitions::SEQUENCE_SEQUENCE25;
  sequenceNames["Sequence26"]  = tcds::definitions::SEQUENCE_SEQUENCE26;
  sequenceNames["Sequence27"]  = tcds::definitions::SEQUENCE_SEQUENCE27;
  sequenceNames["Sequence28"]  = tcds::definitions::SEQUENCE_SEQUENCE28;
  sequenceNames["Sequence29"]  = tcds::definitions::SEQUENCE_SEQUENCE29;
  sequenceNames["Sequence30"]  = tcds::definitions::SEQUENCE_SEQUENCE30;
  sequenceNames["Sequence31"]  = tcds::definitions::SEQUENCE_SEQUENCE31;

  return sequenceNames;
}

tcds::definitions::SEQUENCE_NUM
tcds::utils::sequenceNameToNumber(std::string const& sequenceName)
{
  static std::map<std::string, tcds::definitions::SEQUENCE_NUM> sequenceMap = sequenceNameMap();
  std::map<std::string, tcds::definitions::SEQUENCE_NUM>::const_iterator i = sequenceMap.find(sequenceName);
  if (i == sequenceMap.end())
    {
      XCEPT_RAISE(tcds::exception::ValueError,
                  toolbox::toString("'%s' is not a valid sequence name.",
                                    sequenceName.c_str()));
    }
  return sequenceMap[sequenceName];
}

std::string
tcds::utils::sequenceNumberToName(tcds::definitions::SEQUENCE_NUM const& sequenceNumber)
{
  static std::map<std::string, tcds::definitions::SEQUENCE_NUM> sequenceMap = sequenceNameMap();
  std::map<std::string, tcds::definitions::SEQUENCE_NUM>::const_iterator res = sequenceMap.end();
  std::map<std::string, tcds::definitions::SEQUENCE_NUM>::const_iterator i;
  for (i = sequenceMap.begin(); i != sequenceMap.end(); ++i)
    {
      if (i->second == sequenceNumber)
        {
          res = i;
          break;
        }
    }
  if (res == sequenceMap.end())
    {
      XCEPT_RAISE(tcds::exception::ValueError,
                  toolbox::toString("'%d' is not a valid sequence number.",
                                    sequenceNumber));
    }
  return res->first;
}

tcds::definitions::BCOMMAND_TYPE
tcds::utils::bcommandTypeFromString(std::string const bcommandTypeString)
{
  if ((bcommandTypeString == "short") ||
      (bcommandTypeString == "broadcast"))
    {
      return tcds::definitions::BCOMMAND_TYPE_BROADCAST;
    }
  else if ((bcommandTypeString == "long") ||
           (bcommandTypeString == "addressed"))
    {
      return tcds::definitions::BCOMMAND_TYPE_ADDRESSED;
    }
  else
    {
      XCEPT_RAISE(tcds::exception::ValueError,
                  toolbox::toString("'%s' is not a valid B-command type.",
                                    bcommandTypeString.c_str()));
    }
}

tcds::definitions::BCOMMAND_ADDRESS_TYPE
tcds::utils::bcommandAddressTypeFromString(std::string const bcommandAddressTypeString)
{
  if (bcommandAddressTypeString == "internal")
    {
      return tcds::definitions::BCOMMAND_ADDRESS_TYPE_INTERNAL;
    }
  else if (bcommandAddressTypeString == "external")
    {
      return tcds::definitions::BCOMMAND_ADDRESS_TYPE_EXTERNAL;
    }
  else
    {
      XCEPT_RAISE(tcds::exception::ValueError,
                  toolbox::toString("'%s' is not a valid B-command address type.",
                                    bcommandAddressTypeString.c_str()));
    }
}

void
tcds::utils::checkBgoNumber(uint32_t const bgoNumber)
{
  if ((static_cast<int>(bgoNumber) < tcds::definitions::kBgoNumMin) ||
      (static_cast<int>(bgoNumber) > tcds::definitions::kBgoNumMax))
    {
      XCEPT_RAISE(tcds::exception::ValueError,
                  toolbox::toString("'%d' is not a valid B-go number.",
                                    bgoNumber));
    }
}

void
tcds::utils::checkBgoTrainNumber(uint32_t const bgoTrainNumber)
{
  if ((static_cast<int>(bgoTrainNumber) < tcds::definitions::kSequenceNumMin) ||
      (static_cast<int>(bgoTrainNumber) > tcds::definitions::kSequenceNumMax))
    {
      XCEPT_RAISE(tcds::exception::ValueError,
                  toolbox::toString("'%d' is not a valid B-go train/sequence number.",
                                    bgoTrainNumber));
    }
}

void
tcds::utils::checkBCommandData(uint32_t const bcommandData)
{
  // NOTE: The cast is to allow for the explicit comparison (which
  // makes the code clearer) without triggering a 'tautological
  // comparison' warning from the compiler.
  if ((static_cast<int64_t>(bcommandData) < tcds::definitions::BCOMMAND_DATA_MIN) &&
      (bcommandData > tcds::definitions::BCOMMAND_DATA_MAX))
    {
      XCEPT_RAISE(tcds::exception::ValueError,
                  toolbox::toString("The value '0x%x' is not valid as B-command data.",
                                    bcommandData));
    }
}

void
tcds::utils::checkBCommandAddress(uint32_t const address)
{
  // NOTE: The cast is to allow for the explicit comparison (which
  // makes the code clearer) without triggering a 'tautological
  // comparison' warning from the compiler.
  if ((static_cast<int64_t>(address) < tcds::definitions::BCOMMAND_ADDRESS_MIN) ||
      (address > tcds::definitions::BCOMMAND_ADDRESS_MAX))
    {
      XCEPT_RAISE(tcds::exception::ValueError,
                  toolbox::toString("The value '0x%x' is not valid as B-command address.",
                                    address));
    }
}

void
tcds::utils::checkBCommandSubAddress(uint32_t const address)
{
  // NOTE: The cast is to allow for the explicit comparison (which
  // makes the code clearer) without triggering a 'tautological
  // comparison' warning from the compiler.
  if ((static_cast<int64_t>(address) < tcds::definitions::BCOMMAND_SUBADDRESS_MIN) ||
      (address > tcds::definitions::BCOMMAND_SUBADDRESS_MAX))
    {
      XCEPT_RAISE(tcds::exception::ValueError,
                  toolbox::toString("The value '0x%x' is not valid as B-command sub-address.",
                                    address));
    }
}

std::map<uint8_t, std::pair<std::string, std::string> >
tcds::utils::ttsMapClassic()
{
  std::map<uint8_t, std::pair<std::string, std::string> > res;
  res[tcds::definitions::TTS_STATE_WARNING]            = std::make_pair("warning", "CW");
  res[tcds::definitions::TTS_STATE_OOS]                = std::make_pair("out-of-sync", "CS");
  res[tcds::definitions::TTS_STATE_BUSY]               = std::make_pair("busy", "CB");
  res[tcds::definitions::TTS_STATE_WARNING_DUE_TO_DAQ] = std::make_pair("warning (backpressured)", "CW");
  res[tcds::definitions::TTS_STATE_READY]              = std::make_pair("ready", "CR");
  res[tcds::definitions::TTS_STATE_ERROR]              = std::make_pair("error", "CE");
  res[tcds::definitions::TTS_STATE_DISCONNECTED_0]     = std::make_pair("disconnected (0x00)", "CD");
  res[tcds::definitions::TTS_STATE_DISCONNECTED_1]     = std::make_pair("disconnected (0x0f)", "CD");
  res[tcds::definitions::TTS_STATE_PRIV_REQ_1]         = std::make_pair("private request 1", "CP");
  res[tcds::definitions::TTS_STATE_PRIV_REQ_2]         = std::make_pair("private request 2", "CP");
  res[tcds::definitions::TTS_STATE_PRIV_REQ_3]         = std::make_pair("private request 3", "CP");
  res[tcds::definitions::TTS_STATE_INVALID_14]         = std::make_pair("invalid (FED)", "CI");

  return res;
}

std::map<uint8_t, std::pair<std::string, std::string> >
tcds::utils::ttsMapTCDS()
{
  std::map<uint8_t, std::pair<std::string, std::string> > res;
  res[tcds::definitions::TTS_STATE_LOST_INTO_PI]         = std::make_pair("link lost at PI input (unaligned)", "TP");
  res[tcds::definitions::TTS_STATE_LOST_INTO_PI_RXLOS]   = std::make_pair("link lost at PI input (no signal)", "TP");
  res[tcds::definitions::TTS_STATE_LOST_INTO_PI_MODABS]  = std::make_pair("link lost at PI input (no SFP)", "TP");
  res[tcds::definitions::TTS_STATE_LOST_INTO_LPM]        = std::make_pair("link lost at LPM input (unaligned)", "TL");
  res[tcds::definitions::TTS_STATE_LOST_INTO_LPM_RXLOS]  = std::make_pair("link lost at LPM input (no signal)", "TL");
  res[tcds::definitions::TTS_STATE_LOST_INTO_LPM_MODABS] = std::make_pair("link lost at LPM input (no SFP)", "TL");
  res[tcds::definitions::TTS_STATE_LOST_INTO_CPM]        = std::make_pair("link lost at CPM input", "TC");
  res[tcds::definitions::TTS_STATE_INVALID_TCDS]         = std::make_pair("invalid (TCDS)", "TI");
  // This is used internally in the TCDS firmware to flag inputs
  // that are masked/disabled/ignored.
  res[tcds::definitions::TTS_STATE_MASKED]               = std::make_pair("masked", "TR");
  // This is used internally in the TCDS to flag situations with
  // unknown TTS states. E.g., the initial state after application
  // start.
  res[tcds::definitions::TTS_STATE_UNKNOWN_TCDS]         = std::make_pair("unknown (TCDS)", "TU");
  return res;
}

std::string
tcds::utils::TTSStateToString(uint16_t const ttsIn,
                              bool const piMode,
                              bool const verbose)
{
  // Of course at the PI input we have to indicate whether we
  // (incorrectly) received a TCDS-like value (i.e. > 0xf) or if we
  // inserted the value ourselves (e.g., because the link
  // unaligned). We use a ninth bit to indicate the latter case.
  bool const isPIMode = piMode;

  // Values above 0xf are used by the TCDS internally.
  bool const isTCDSSpecial = (ttsIn > 0xf);

  // Extract the aforementioned nineth bit.
  bool const wasAddedByTCDS = (ttsIn > 0xff);

  bool const treatAsTCDS = isTCDSSpecial && (wasAddedByTCDS || !isPIMode);

  std::string res = "UNKNOWN";
  std::stringstream tmp;
  if (wasAddedByTCDS && !isPIMode)
    {
      // This ninth bit should only ever be set/used in the PI, at the
      // input from the FED. From there on the TCDS internally should
      // 'just do it right.'
      res = "IMPOSSIBLE";
      tmp << " (0x" << std::setw(2) << std::setfill('0') << std::hex << int(ttsIn) << ")";
      res += tmp.str();
    }
  else
    {
      uint8_t const ttsTmp = (ttsIn & 0xff);
      if (!treatAsTCDS)
        {
          static std::map<uint8_t, std::pair<std::string, std::string> > const ttsMapClassic =
            tcds::utils::ttsMapClassic();
          if (ttsMapClassic.find(ttsTmp) != ttsMapClassic.end())
            {
              res = ttsMapClassic.at(ttsTmp).first;
            }
          else
            {
              res = "invalid (FED";
              tmp << ", 0x" << std::setw(2) << std::setfill('0') << std::hex << int(ttsTmp);
              res += tmp.str();
              res += ")";
            }
        }
      else
        {
          static std::map<uint8_t, std::pair<std::string, std::string> > const ttsMapTCDS =
            tcds::utils::ttsMapTCDS();
          if (ttsMapTCDS.find(ttsTmp) != ttsMapTCDS.end())
            {
              res = ttsMapTCDS.at(ttsTmp).first;
            }
          else
            {
              res = "invalid (TCDS";
              tmp << ", 0x" << std::setw(2) << std::setfill('0') << std::hex << int(ttsTmp);
              res += tmp.str();
              res += ")";
            }
        }
    }

  return res;
}

std::string
tcds::utils::TTSStateToStringShort(uint8_t const ttsIn)
{
  // NOTE: This method should only be used for the PI TTS log,
  // really. So in this case we're always in PI mode.
  bool const isPIMode = true;

  // Values above 0xf are used by the TCDS internally.
  bool const isTCDSSpecial = (ttsIn > 0xf);

  // Of course at the PI input we have to indicate whether we
  // (incorrectly) received a TCDS-like value (i.e. > 0xf) or if we
  // inserted the value ourselves (e.g., because the link
  // unaligned). We use a ninth bit for that.
  bool const wasAddedByTCDS = (ttsIn > 0xff);

  bool const treatAsTCDS = isTCDSSpecial && (wasAddedByTCDS || !isPIMode);

  std::string res = "??";
  if (wasAddedByTCDS && !isPIMode)
    {
      // This ninth bit should only ever be set/used in the PI, at the
      // input from the FED. From there on the TCDS internally should
      // 'just do it right.'
      res = "!!";
    }
  else
    {
      uint8_t const ttsTmp = (ttsIn & 0xff);
      if (!treatAsTCDS)
        {
          static std::map<uint8_t, std::pair<std::string, std::string> > const ttsMapClassic =
            tcds::utils::ttsMapClassic();
          if (ttsMapClassic.find(ttsTmp) != ttsMapClassic.end())
            {
              res = ttsMapClassic.at(ttsTmp).second;
            }
          else
            {
              res = ttsMapClassic.at(tcds::definitions::TTS_STATE_INVALID_14).second;
            }
        }
      else
        {
          static std::map<uint8_t, std::pair<std::string, std::string> > const ttsMapTCDS =
            tcds::utils::ttsMapTCDS();
          if (ttsMapTCDS.find(ttsTmp) != ttsMapTCDS.end())
            {
              res = ttsMapTCDS.at(ttsTmp).second;
            }
          else
            {
              res = ttsMapTCDS.at(tcds::definitions::TTS_STATE_INVALID_TCDS).second;
            }
        }
    }

  // Now append the numeric TTS state itself.
  std::stringstream tmp;
  tmp << "_" << std::setw(2) << std::setfill('0') << std::hex << int(ttsIn);

  res += tmp.str();
  return res;
}

std::string
tcds::utils::TTSTriggerToString(uint8_t const ttsIn)
{
  std::string res = "UNKNOWN";
  switch (ttsIn)
    {
    case tcds::definitions::TTS_STATE_DISCONNECTED_0:
      // This is used to flag partitions that are to be ignored in the
      // TTS trigger-OR.
      // NOTE: When a FED sends a 0x0 state, the TTS OR maps it to the
      // 'generic' DISCONNECTED state 0xf.
      res = "x";
      break;
    case tcds::definitions::TTS_STATE_WARNING:
      res = "WRN";
      break;
    case tcds::definitions::TTS_STATE_OOS:
      res = "OOS";
      break;
    case tcds::definitions::TTS_STATE_BUSY:
      res = "BSY";
      break;
    case tcds::definitions::TTS_STATE_READY:
      res = "RDY";
      break;
    case tcds::definitions::TTS_STATE_ERROR:
      res = "ERR";
      break;
    case tcds::definitions::TTS_STATE_DISCONNECTED_1:
      res = "DIS";
      break;
    default:
      std::stringstream tmp;
      tmp << " 0x" << std::hex << int(ttsIn);
      res = tmp.str();
      break;
    }
  return res;
}

std::string
tcds::utils::ttsChannelTypeToString(tcds::definitions::TTS_CHANNEL_TYPE const type)
{
  // NOTE: Careful with these labels. They are published in the TCDS
  // flashlists and used by various third-party applications.

  std::string res;

  switch (type)
    {
    case tcds::definitions::TTS_CHANNEL_TOP_LEVEL:
      res = "tts_toplevel";
      break;
    case tcds::definitions::TTS_CHANNEL_ICI:
      res = "tts_ici";
      break;
    case tcds::definitions::TTS_CHANNEL_APVE:
      res = "tts_apve";
      break;
    case tcds::definitions::TTS_CHANNEL_RJ45:
      res = "tts_rj45";
      break;
    case tcds::definitions::TTS_CHANNEL_FED:
      res = "tts_fed";
      break;
    case tcds::definitions::TTS_CHANNEL_DAQ_BACKPRESSURE:
      res = "block_daq_backpressure";
      break;
    case tcds::definitions::TTS_CHANNEL_PM_APVE:
      res = "block_pm_apve";
      break;
    case tcds::definitions::TTS_CHANNEL_RETRI:
      res = "block_retri";
      break;
    case tcds::definitions::TTS_CHANNEL_BUNCH_MASK:
      res = "block_bx_mask";
      break;
    default:
      // ASSERT ASSERT ASSERT
      assert (false);
      // ASSERT ASSERT ASSERT end
      break;
    }

  return res;
}

std::map<std::string, int>
tcds::utils::ttsPriorities()
{
  // NOTE: Don't use zero, so a default value can still be inserted by
  // map::operator[]() in case of unknown keys.

  std::map<std::string, int> ttsPriorities;
  ttsPriorities["block_daq_backpressure"] = 1;
  ttsPriorities["block_pm_apve"] = 2;
  ttsPriorities["block_retri"] = 3;
  ttsPriorities["block_bx_mask"] = 4;
  ttsPriorities["tts_toplevel"] = 5;
  ttsPriorities["tts_ici"] = 6;
  ttsPriorities["tts_apve"] = 7;

  return ttsPriorities;
}

std::string
tcds::utils::BdataSourceToString(tcds::definitions::BDATA_SOURCE const bdataSourceIn)
{
  std::string res = "UNKNOWN";
  switch (bdataSourceIn)
    {
    case tcds::definitions::BDATA_SOURCE_APVE:
      res = "APVE";
      break;
    case tcds::definitions::BDATA_SOURCE_BRILDAQ:
      res = "BRILDAQ sync. data";
      break;
    default:
      // ASSERT ASSERT ASSERT
      assert (false);
      // ASSERT ASSERT ASSERT end
      break;
    }
  return res;
}

std::string
tcds::utils::BcommandTypeToString(tcds::definitions::BCOMMAND_TYPE const bcommandTypeIn)
{
  std::string res = "UNKNOWN";
  switch (bcommandTypeIn)
    {
    case tcds::definitions::BCOMMAND_TYPE_BROADCAST:
      res = "broadcast";
      break;
    case tcds::definitions::BCOMMAND_TYPE_ADDRESSED:
      res = "addressed";
      break;
    default:
      // ASSERT ASSERT ASSERT
      assert (false);
      // ASSERT ASSERT ASSERT end
      break;
    }
  return res;
}

std::string
tcds::utils::BcommandAddressingTypeToString(tcds::definitions::BCOMMAND_ADDRESS_TYPE const bcommandAddressTypeIn)
{
  std::string res = "UNKNOWN";
  switch (bcommandAddressTypeIn)
    {
    case tcds::definitions::BCOMMAND_ADDRESS_TYPE_INTERNAL:
      res = "internal";
      break;
    case tcds::definitions::BCOMMAND_ADDRESS_TYPE_EXTERNAL:
      res = "external";
      break;
    default:
      // ASSERT ASSERT ASSERT
      assert (false);
      // ASSERT ASSERT ASSERT end
      break;
    }
  return res;
}

std::string
tcds::utils::TriggerTypeToString(tcds::definitions::TRIG_TYPE const trigTypeIn)
{
  std::string res = "UNKNOWN";
  switch (trigTypeIn)
    {
    case tcds::definitions::TRIG_TYPE_0:
      res = "Trigger type 0";
      break;
    case tcds::definitions::TRIG_TYPE_PHYSICS:
      res = "Physics trigger";
      break;
    case tcds::definitions::TRIG_TYPE_SEQUENCE:
      res = "Sequence/calibration trigger";
      break;
    case tcds::definitions::TRIG_TYPE_RANDOM:
      res = "Random trigger";
      break;
    case tcds::definitions::TRIG_TYPE_AUX:
      res = "Auxiliary trigger";
      break;
    case tcds::definitions::TRIG_TYPE_5:
      res = "Trigger type 5";
      break;
    case tcds::definitions::TRIG_TYPE_6:
      res = "Trigger type 6";
      break;
    case tcds::definitions::TRIG_TYPE_7:
      res = "Trigger type 7";
      break;
    case tcds::definitions::TRIG_TYPE_CYCLIC:
      res = "Cyclic trigger";
      break;
    case tcds::definitions::TRIG_TYPE_BUNCHMASK:
      res = "Bunch-mask trigger";
      break;
    case tcds::definitions::TRIG_TYPE_SOFTWARE:
      res = "Software trigger";
      break;
    case tcds::definitions::TRIG_TYPE_TTS:
      res = "TTS-based trigger";
      break;
    case tcds::definitions::TRIG_TYPE_PATTERN:
      res = "Pattern trigger";
      break;
    case tcds::definitions::TRIG_TYPE_13:
      res = "Trigger type 13";
      break;
    case tcds::definitions::TRIG_TYPE_14:
      res = "Trigger type 14";
      break;
    case tcds::definitions::TRIG_TYPE_CPM_L1A_VIA_LPM:
      res = "'CPM L1A via LPM' trigger";
      break;
    default:
      // ASSERT ASSERT ASSERT
      assert (false);
      // ASSERT ASSERT ASSERT end
      break;
    }
  return res;
}

std::string
tcds::utils::compressString(std::string const stringIn)
{
  size_t srcSize = stringIn.size();
  Bytef const* src = reinterpret_cast<Bytef const*>(stringIn.c_str());
  // NOTE: The magic number below is a rounded-up result of reading
  // the zlib docs (for compress()).
  size_t dstSize = srcSize + (.1 * srcSize) + 16;
  std::vector<Bytef> dst(dstSize);

  z_stream zStream;
  // Not pretty, but necessary here.
  zStream.next_in = const_cast<Bytef*>(src);
  zStream.avail_in = srcSize;
  zStream.next_out = &dst[0];
  zStream.avail_out = dstSize;
  zStream.zalloc = Z_NULL;
  zStream.zfree = Z_NULL;
  zStream.opaque = Z_NULL;

  // NOTE: The '8' below is the value of DEF_MEM_LEVEL, the default
  // memory-usage level. This is defined in zutil.h, which is not
  // installed (for whatever reason) by the zlib-devel RPM.
  // NOTE: The '+ 16' changes the behaviour from zlib to gzip format.
  int res = deflateInit2(&zStream,
                         Z_DEFAULT_COMPRESSION,
                         Z_DEFLATED,
                         MAX_WBITS + 16,
                         8,
                         Z_DEFAULT_STRATEGY);

  if (res != Z_OK)
    {
      std::string const msg = "Failed to initialize zlib.";
      XCEPT_RAISE(tcds::exception::RuntimeProblem, msg.c_str());
    }
  else
    {
      res = deflate(&zStream, Z_FINISH);
      if ((res != Z_STREAM_END) && (res != Z_OK))
        {
          deflateEnd(&zStream);
          std::string const msg = "Failed to compress string using zlib "
            "(looks like a buffer size problem).";
          XCEPT_RAISE(tcds::exception::RuntimeProblem, msg.c_str());
        }
      dstSize = zStream.total_out;
      res = deflateEnd(&zStream);
      if (res != Z_OK)
        {
          std::string const msg = "Failed to compress string using zlib.";
          XCEPT_RAISE(tcds::exception::RuntimeProblem, msg.c_str());
        }
    }

  // Turn the result back into an std::string.
  std::string const stringOut(dst.begin(), dst.begin() + dstSize);
  return stringOut;
}

std::vector<std::pair<uint16_t, uint16_t> >
tcds::utils::groupBXListIntoRanges(std::vector<uint16_t> const bxList)
{
  std::vector<std::pair<uint16_t, uint16_t> > bxRanges;
  if (bxList.size() > 0)
    {
      std::vector<uint16_t>::const_iterator bx = bxList.begin() + 1;
      uint16_t first = *bxList.begin();
      uint16_t last = *bxList.begin();
      while (bx != bxList.end())
        {
          if (*bx == (last + 1))
            {
              // Part of the current group.
              last = *bx;
            }
          else
            {
              // Not part of the current group.
              bxRanges.push_back(std::make_pair(first, last));
              first = *bx;
              last = *bx;
            }
          ++bx;
        }
      bxRanges.push_back(std::make_pair(first, last));
    }
  return bxRanges;
}

std::string
tcds::utils::formatBXRangeList(std::vector<std::pair<uint16_t, uint16_t> > const bxRanges)
{
  std::string res = "";
  for (std::vector<std::pair<uint16_t, uint16_t> >::const_iterator i = bxRanges.begin();
       i != bxRanges.end();
       ++i)
    {
      if (!res.empty())
        {
          res += ", ";
        }
      uint16_t first = i->first;
      uint16_t last = i->second;
      if (first == last)
        {
          res += toolbox::toString("%d", first);
        }
      else
        {
          res += toolbox::toString("%d-%d", first, last);
        }
    }
  if (res.empty())
    {
      res = "none";
    }
  return res;
}

std::string
tcds::utils::beamModeToString(tcds::definitions::BEAM_MODE const beamMode)
{
  std::string res = "unknown";

  switch (beamMode)
    {
    case tcds::definitions::BEAM_MODE_UNKNOWN:
      res = "unknown";
      break;
    case tcds::definitions::BEAM_MODE_NO_MODE:
      res = "no mode";
      break;
    case tcds::definitions::BEAM_MODE_SETUP:
      res = "setup";
      break;
    case tcds::definitions::BEAM_MODE_INJECTION_PROBE_BEAM:
      res = "injection probe beam";
      break;
    case tcds::definitions::BEAM_MODE_INJECTION_SETUP_BEAM:
      res = "injection setup beam";
      break;
    case tcds::definitions::BEAM_MODE_INJECTION_PHYSICS_BEAM:
      res = "injection physics beam";
      break;
    case tcds::definitions::BEAM_MODE_PREPARE_RAMP:
      res = "prepare ramp";
      break;
    case tcds::definitions::BEAM_MODE_RAMP:
      res = "ramp";
      break;
    case tcds::definitions::BEAM_MODE_FLAT_TOP:
      res = "flat top";
      break;
    case tcds::definitions::BEAM_MODE_SQUEEZE:
      res = "squeeze";
      break;
    case tcds::definitions::BEAM_MODE_ADJUST:
      res = "adjust";
      break;
    case tcds::definitions::BEAM_MODE_STABLE_BEAMS:
      res = "stable beams";
      break;
    case tcds::definitions::BEAM_MODE_UNSTABLE_BEAMS:
      res = "unstable beams";
      break;
    case tcds::definitions::BEAM_MODE_BEAM_DUMP:
      res = "beam dump";
      break;
    case tcds::definitions::BEAM_MODE_RAMP_DOWN:
      res = "ramp down";
      break;
    case tcds::definitions::BEAM_MODE_RECOVERY:
      res = "recovery";
      break;
    case tcds::definitions::BEAM_MODE_INJECT_AND_DUMP:
      res = "inject and dump";
      break;
    case tcds::definitions::BEAM_MODE_CIRCULATE_AND_DUMP:
      res = "circulate and dump";
      break;
    case tcds::definitions::BEAM_MODE_ABORT:
      res = "abort";
      break;
    case tcds::definitions::BEAM_MODE_CYCLING:
      res = "cycling";
      break;
    case tcds::definitions::BEAM_MODE_BEAM_DUMP_WARNING:
      res = "beam dump warning";
      break;
    case tcds::definitions::BEAM_MODE_NO_BEAM:
      res = "no beam";
      break;
    default:
      XCEPT_RAISE(tcds::exception::ValueError,
                  toolbox::toString("Received an invalid LHC beam mode: %d.",
                                    beamMode));
      break;
    }
  return res;
}

unsigned int
tcds::utils::countTrailingZeros(uint32_t const val)
{
  unsigned int numZeros = 0;
  uint32_t tmp = val;
  while ((tmp & 0x1) == 0)
    {
      numZeros += 1;
      tmp >>= 1;
    }
  return numZeros;
}

std::string
tcds::utils::formatLPMLabel(unsigned int const lpmNumber,
                            tcds::utils::ConfigurationInfoSpaceHandler const& cfgInfoSpace)
{
  std::string res = "";
  std::string const lpmLabel =
    cfgInfoSpace.getString(toolbox::toString("partitionLabelLPM%d", lpmNumber));
  if (lpmLabel.empty())
    {
      res = toolbox::toString("LPM %d", lpmNumber);
    }
  else
    {
      res = toolbox::toString("LPM %d (%s)", lpmNumber, lpmLabel.c_str());
    }
  return res;
}

std::string
tcds::utils::formatICILabel(unsigned int const lpmNumber,
                            unsigned int const iciNumber,
                            tcds::utils::ConfigurationInfoSpaceHandler const& cfgInfoSpace,
                            bool const labelForExternal)
{
  return formatLabel("ICI", lpmNumber, iciNumber, cfgInfoSpace, labelForExternal);
}

std::string
tcds::utils::formatAPVELabel(unsigned int const lpmNumber,
                             unsigned int const apveNumber,
                             tcds::utils::ConfigurationInfoSpaceHandler const& cfgInfoSpace,
                             bool const labelForExternal)
{
  return formatLabel("APVE", lpmNumber, apveNumber, cfgInfoSpace, labelForExternal);
}

std::string
tcds::utils::formatLabel(std::string const& type,
                         unsigned int const lpmNumber,
                         unsigned int const number,
                         tcds::utils::ConfigurationInfoSpaceHandler const& cfgInfoSpace,
                         bool const labelForExternal)
{
  std::string const typeUp = toolbox::toupper(type);
  std::string res = "";
  std::string parName;

  if (lpmNumber == 0)
    {
      // LPMController etc.
      parName = toolbox::toString("partitionLabel%s%d", typeUp.c_str(), number);
    }
  else
    {
      // CPMController.
      parName = toolbox::toString("partitionLabelLPM%d%s%d",
                                  lpmNumber, typeUp.c_str(), number);
    }

  std::string const label = cfgInfoSpace.getString(parName);
  if (label.empty())
    {
      if (!labelForExternal)
        {
          res = toolbox::toString("%s%d", typeUp.c_str(), number);
        }
      else
        {
          if (lpmNumber == 0)
            {
              res = toolbox::toString("%s%d", typeUp.c_str(), number);
            }
          else
            {
              res = toolbox::toString("LPM%d:%s%d", lpmNumber, typeUp.c_str(), number);
            }
        }
    }
  else
    {
      if (!labelForExternal)
        {
          res = toolbox::toString("%s%d (%s)", typeUp.c_str(), number, label.c_str());
        }
      else
        {
          res = toolbox::toString("%s", label.c_str());
        }
    }

  return res;
}

xdata::Table
tcds::utils::getLASCatalog(std::string const& lasURL,
                           bool const staticCatalog)
{
  std::string lasPath;
  if (staticCatalog)
    {
      lasPath = "retrieveStaticCatalog";
    }
  else
    {
      lasPath = "retrieveCatalog";
    }
  // The data we get will have (E)XDR format
  std::string const lasQuery = "fmt=exdr";

  tcds::utils::CurlGetter getter;
  std::string rawTableData = getter.get(lasURL + "/" + lasPath, "", lasQuery, "");

  xdata::exdr::FixedSizeInputStreamBuffer
    inBuffer(static_cast<char*>(&rawTableData[0]), rawTableData.size());

  xdata::Table table;
  xdata::exdr::Serializer serializer;
  try
    {
      serializer.import(&table, &inBuffer);
    }
  catch (xdata::exception::Exception& err)
    {
      std::string const fullURL =
        getter.buildFullURL(lasURL + "/" + lasPath, "", lasQuery, "");
      std::string const msgBase =
        toolbox::toString("Failed to deserialize incoming LAS catalog table "
                          "from LAS '%s' (full query URL: '%s')",
                          lasURL.c_str(),
                          fullURL.c_str());
      std::string const msg =
        toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
      XCEPT_RETHROW(tcds::exception::RuntimeProblem, msg, err);
    }

  return table;
}

xdata::Table
tcds::utils::getFlashList(std::string const& lasURL,
                          std::string const& flashlistName)
{
  std::string const lasPath = "retrieveCollection";
  // The data we get will have (E)XDR format
  std::string lasQuery =
    "fmt=exdr&flash=" + flashlistName;

  tcds::utils::CurlGetter getter;
  std::string rawTableData =
    getter.get(lasURL + "/" + lasPath, "", lasQuery, "");

  // // NOTE: This is a bit of a short-cut. One should retrieve and check
  // // the catalog, really. On the other hand, this reduces the amount
  // // of required network traffic.
  // std::string const tmp = toolbox::tolower(rawTableData);
  // std::string const failMsg = "failed to find flashlist";
  // if (tmp.find(failMsg) != std::string::npos)
  //   {
  //     std::string const fullURL =
  //       getter.buildFullURL(lasURL + "/" + lasPath, "", lasQuery, "");
  //     std::string const msg =
  //       toolbox::toString("Failed to find flashlist '%s' on LAS '%s' (full query URL: '%s').",
  //                         flashlistName.c_str(),
  //                         lasURL.c_str(),
  //                         fullURL.c_str());
  //     XCEPT_RAISE(tcds::exception::HTTPProblem, msg);
  //   }

  xdata::exdr::FixedSizeInputStreamBuffer
    inBuffer(static_cast<char*>(&rawTableData[0]), rawTableData.size());

  xdata::Table table;
  xdata::exdr::Serializer serializer;
  try
    {
      serializer.import(&table, &inBuffer);
    }
  catch (xdata::exception::Exception& err)
    {
      std::string const fullURL =
        getter.buildFullURL(lasURL + "/" + lasPath, "", lasQuery, "");
      std::string const msgBase =
        toolbox::toString("Failed to deserialize incoming flashlist table "
                          "for flashlist '%s' on LAS '%s' (full query URL: '%s') ",
                          flashlistName.c_str(),
                          lasURL.c_str(),
                          fullURL.c_str());
      std::string const msg =
        toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
      XCEPT_RETHROW(tcds::exception::XDataProblem, msg, err);
    }

  return table;
}

bool
tcds::utils::isFlashListHostedByLAS(std::string const& lasURL,
                                    std::string const& flashlistName)
{
  // NOTE: To see if a flashlist is hosted by a certain LAS,
  // independent of whether any data has been received yet, we need to
  // check the _static_ catalogue.

  xdata::Table catalogStatic = getLASCatalog(lasURL, true);
  return isFlashListInCatalog(catalogStatic, flashlistName);
}

bool
tcds::utils::isFlashListPresentInLAS(std::string const& lasURL,
                                     std::string const& flashlistName)
{
  // NOTE: To see if a flashlist is present in a certain LAS, we need
  // to check the _dynamic_ catalogue.

  xdata::Table catalogDynamic = getLASCatalog(lasURL, false);
  return isFlashListInCatalog(catalogDynamic, flashlistName);
}

bool
tcds::utils::isFlashListInCatalog(xdata::Table& catalog,
                                  std::string const& flashlistName)
{
  bool flashlistFound = false;
  for (xdata::TableIterator it = catalog.begin();
       it != catalog.end();
       ++it)
    {
      if (it->getField("name")->toString() == flashlistName)
        {
          flashlistFound = true;
          break;
        }
    }

  return flashlistFound;
}

std::vector<std::string>::const_iterator
tcds::utils::findFlashList(std::string const& flashlistName,
                           std::vector<std::string> const& lasURLs)
{
  std::vector<std::string>::const_iterator res = lasURLs.end();

  for (std::vector<std::string>::const_iterator it = lasURLs.begin();
       it != lasURLs.end();
       ++it)
    {
      if (tcds::utils::isFlashListHostedByLAS(*it, flashlistName))
        {
          res = it;
          break;
        }
    }

  return res;
}

void
tcds::utils::verifyBgoNumber(unsigned int const bgoNumber)
{
  if ((bgoNumber < tcds::definitions::kBgoNumMin) &&
      (bgoNumber > tcds::definitions::kBgoNumMax))
    {
      std::string const msg = toolbox::toString("B-go number %d falls outside "
                                                "the allowed range of [%d, %d].",
                                                bgoNumber,
                                                tcds::definitions::kBgoNumMin,
                                                tcds::definitions::kBgoNumMax);
      XCEPT_RAISE(tcds::exception::ValueError, msg.c_str());
    }
}

void
tcds::utils::verifySequenceNumber(unsigned int const sequenceNumber)
{
  if ((sequenceNumber < tcds::definitions::kSequenceNumMin) &&
      (sequenceNumber > tcds::definitions::kSequenceNumMax))
    {
      std::string const msg = toolbox::toString("Sequence number %d falls outside "
                                                "the allowed range of [%d, %d].",
                                                sequenceNumber,
                                                tcds::definitions::kSequenceNumMin,
                                                tcds::definitions::kSequenceNumMax);
      XCEPT_RAISE(tcds::exception::ValueError, msg.c_str());
    }
}

tcds::utils::fedEnableMask
tcds::utils::parseFedEnableMask(std::string const& fedEnableMask)
{
  // We expect the FED enable mask string to be non-empty. Otherwise:
  // complain.
  if (fedEnableMask.empty())
    {
      std::string const msg =
        "Failed to parse the FED enable mask. Expected a non-empty string.";
      XCEPT_RAISE(tcds::exception::ConfigurationProblem, msg);
    }

  // We expect the FED enable mask string to end with '%'. Otherwise:
  // complain.
  if (!toolbox::endsWith(fedEnableMask, "%"))
    {
      std::string const msg =
        "Failed to parse the FED enable mask. Expected a '%' at the end.";
      XCEPT_RAISE(tcds::exception::ConfigurationProblem, msg);
    }

  std::list<std::string> const pieces =
    toolbox::parseTokenList(fedEnableMask, "%");

  tcds::utils::fedEnableMask res;
  for (std::list<std::string>::const_iterator piece = pieces.begin();
       piece != pieces.end();
       ++piece)
    {
      std::list<std::string> const tmp = toolbox::parseTokenList(*piece, "&");

      // The above splitting on '%' and '&' should have left us with
      // two pieces. If not, raise a stink.
      if (tmp.size() != 2)
        {
          std::string const msg =
            "Failed to parse the FED enable mask. Something appears to be wrong with its structure.";
          XCEPT_RAISE(tcds::exception::ConfigurationProblem, msg);
        }

      uint16_t fedId;
      std::stringstream tmpFedId(toolbox::trim(tmp.front()));
      tmpFedId >> fedId;
      if (tmpFedId.fail() || !(tmpFedId >> std::ws).eof())
        {
          std::string const msg =
            toolbox::toString("Failed to parse the FED enable mask. "
                              "Could not turn '%s' into a FED ID.",
                              tmpFedId.str().c_str());
          XCEPT_RAISE(tcds::exception::ConfigurationProblem, msg);
        }

      uint16_t mask;
      std::stringstream tmpMask(toolbox::trim(tmp.back()));
      tmpMask >> mask;
      if (tmpMask.fail() || !(tmpMask >> std::ws).eof())
        {
          std::string const msg =
            toolbox::toString("Failed to parse the FED enable mask. "
                              "Could not turn '%s' into a mask value.",
                              tmpMask.str().c_str());
          XCEPT_RAISE(tcds::exception::ConfigurationProblem, msg);
        }

      res[fedId] = mask;
    }
  return res;
}

bool
tcds::utils::isFedInFedEnableMask(uint32_t const fedId, std::string const& fedEnableMask)
{
  tcds::utils::fedEnableMask fedVector = parseFedEnableMask(fedEnableMask);
  bool const res = (fedVector.find(fedId) != fedVector.end());
  return res;
}

bool
tcds::utils::doesFedDoDAQ(uint32_t const fedId, std::string const& fedEnableMask)
{
  // NOTE: Non-existent FED ids never do anything.
  bool fedDoesDAQ = false;

  if (fedId != tcds::definitions::kImpossibleFEDId)
    {
      tcds::utils::fedEnableMask fedVector = parseFedEnableMask(fedEnableMask);
      uint8_t mask = 0x0;
      try
        {
          mask = fedVector.at(fedId);
        }
      catch (std::out_of_range const&)
        {
          std::string msg =
            toolbox::toString("Expected FED ID %d in the FED vector but could not find it.",
                              fedId);
          XCEPT_RAISE(tcds::exception::ValueError, msg.c_str());
        }
      fedDoesDAQ = ((mask & 0x5) == 0x1);
    }

  return fedDoesDAQ;
}

bool
tcds::utils::doesFedDoTTS(uint32_t const fedId, std::string const& fedEnableMask)
{
  // NOTE: Non-existent FED ids never do anything.
  bool fedDoesTTS = false;

  if (fedId != tcds::definitions::kImpossibleFEDId)
    {
      tcds::utils::fedEnableMask fedVector = parseFedEnableMask(fedEnableMask);
      uint8_t mask = 0x0;
      try
        {
          mask = fedVector.at(fedId);
        }
      catch (std::out_of_range const&)
        {
          std::string msg =
            toolbox::toString("Expected FED ID %d in the FED vector but could not find it.",
                              fedId);
          XCEPT_RAISE(tcds::exception::ValueError, msg.c_str());
        }
      fedDoesTTS = ((mask & 0xa) == 0x2);
    }

  return fedDoesTTS;
}

bool
tcds::utils::isFedInRun(uint32_t const fedId, std::string const& fedEnableMask)
{
  // The 'common' understanding in CMS is that a FED is 'not in the
  // run' when it does not contribute to the DAQ, nor to the TTS.
  bool const fedDoesDAQ = doesFedDoDAQ(fedId, fedEnableMask);
  bool const fedDoesTTS = doesFedDoTTS(fedId, fedEnableMask);
  bool const fedIsInRun = fedDoesDAQ || fedDoesTTS;
  return fedIsInRun;
}

tcds::utils::ttcPartitionMap
tcds::utils::parseTTCPartitionMap(std::string const& ttcPartitionMap)
{
  // We expect the TTC partition map string to be
  // non-empty. Otherwise: complain.
  if (ttcPartitionMap.empty())
    {
      std::string const msg =
        "Failed to parse the TTC partitition map. Expected a non-empty string.";
      XCEPT_RAISE(tcds::exception::ConfigurationProblem, msg);
    }

  // We expect the TTC partition map string to end with
  // '%'. Otherwise: complain.
  if (!toolbox::endsWith(ttcPartitionMap, "%"))
    {
      std::string const msg =
        "Failed to parse the TTC partition map. Expected a '%' at the end.";
      XCEPT_RAISE(tcds::exception::ConfigurationProblem, msg);
    }

  std::list<std::string> const pieces =
    toolbox::parseTokenList(ttcPartitionMap, "%");

  tcds::utils::ttcPartitionMap res;
  for (std::list<std::string>::const_iterator piece = pieces.begin();
       piece != pieces.end();
       ++piece)
    {
      std::list<std::string> const tmp = toolbox::parseTokenList(*piece, "&");

      // The above splitting on '%' and '&' should have left us with
      // two pieces. If not, raise a stink.
      if (tmp.size() != 2)
        {
          std::string const msg =
            "Failed to parse the TTC partition map. Something appears to be wrong with its structure.";
          XCEPT_RAISE(tcds::exception::ConfigurationProblem, msg);
        }

      std::string const partitionName(toolbox::trim(tmp.front()));

      uint16_t mask;
      std::stringstream tmpMask(toolbox::trim(tmp.back()));
      tmpMask >> mask;
      if (tmpMask.fail() || !(tmpMask >> std::ws).eof())
        {
          std::string const msg =
            toolbox::toString("Failed to parse the TTC partition map. "
                              "Could not turn '%s' into a mask value.",
                              tmpMask.str().c_str());
          XCEPT_RAISE(tcds::exception::ConfigurationProblem, msg);
        }

      res[partitionName] = mask;
    }
  return res;
}

bool
tcds::utils::doesPartitionDoTTS(std::string const partitionName, std::string const& ttcPartitionMap)
{
  // NOTE: Unused partitions never do anything.
  bool partitionDoesTTS = false;

  if (toolbox::tolower(partitionName) != toolbox::tolower(tcds::definitions::kUnusedPartitionName))
    {
      tcds::utils::ttcPartitionMap const partitionMap = parseTTCPartitionMap(ttcPartitionMap);
      uint8_t mask = 0x0;
      try
        {
          mask = partitionMap.at(partitionName);
        }
      catch (std::out_of_range const&)
        {
          std::string msg =
            toolbox::toString("Expected partition '%s' in the TTC partition map but could not find it.",
                              partitionName.c_str());
          XCEPT_RAISE(tcds::exception::ValueError, msg.c_str());
        }
      partitionDoesTTS = ((mask & 0x1) != 0x0);
    }

  return partitionDoesTTS;
}

std::string
tcds::utils::bstStatusToString(tcds::definitions::BST_SIGNAL_STATUS const bstStatus)
{
  std::string res;

  switch (bstStatus)
    {
    case tcds::definitions::BST_SIGNAL_STATUS_UNKNOWN:
      res = "unknown";
      break;
    case tcds::definitions::BST_SIGNAL_STATUS_RESET:
      res = "permanently under reset";
      break;
    case tcds::definitions::BST_SIGNAL_STATUS_NO_SIGNAL:
      res = "no signal";
      break;
    case tcds::definitions::BST_SIGNAL_STATUS_NO_DATA:
      res = "no data received";
      break;
    case tcds::definitions::BST_SIGNAL_STATUS_GOOD:
      res = "good";
      break;
    default:
      // ASSERT ASSERT ASSERT
      assert (false);
      // ASSERT ASSERT ASSERT end
      break;
    }

  return res;
}
