#include "tcds/utils/HwInfoSpaceUpdater.h"

#include "tcds/utils/InfoSpaceItem.h"

tcds::utils::HwInfoSpaceUpdater::HwInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                                    tcds::hwlayer::DeviceBase const& hw) :
  tcds::utils::HwInfoSpaceUpdaterBase(xdaqApp, hw)
{
}

tcds::utils::HwInfoSpaceUpdater::~HwInfoSpaceUpdater()
{
}
