#include "tcds/utils/WebServer.h"

#include <algorithm>
#include <sstream>

#include "cgicc/HTTPResponseHeader.h"
#include "toolbox/Event.h"
#include "toolbox/string.h"
#include "xcept/Exception.h"
#include "xdaq/ApplicationDescriptor.h"
#include "xgi/Output.h"

#include "tcds/exception/Exception.h"
#include "tcds/utils/LockGuard.h"
#include "tcds/utils/LogMacros.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/WebSpacer.h"
#include "tcds/utils/WebTab.h"
#include "tcds/utils/WebTable.h"
#include "tcds/utils/XDAQAppBase.h"
#include "tcds/utils/XGIMethod.h"

tcds::utils::WebServer::WebServer(tcds::utils::XDAQAppBase& xdaqApp,
                                  tcds::utils::Monitor& monitor) :
  xgi::framework::UIManager(&xdaqApp),
  TCDSObject(xdaqApp),
  monitor_(monitor),
  logger_(xdaqApp.getApplicationLogger()),
  jsonContents_(""),
  lock_(toolbox::BSem::FULL)
{
  // Direct binding for the JSON access to the InfoSpace contents.
  tcds::utils::helper0::deferredbind(&xdaqApp, this, &WebServer::jsonUpdate, "update");

  // Binding for the HTML pages, via the XGI framework so we benefit
  // from the HyperDAQ header and footer.
  // xgi::framework::deferredbind(xdaqApp, this, &WebServer::monitoringWebPage, "Default");
  // xgi::framework::deferredbind(xdaqApp, this, &WebServer::monitoringWebPage, "monitoring");
  tcds::utils::helper1::deferredbind(&xdaqApp, this, &WebServer::monitoringWebPage, "Default");
  tcds::utils::helper1::deferredbind(&xdaqApp, this, &WebServer::monitoringWebPage, "monitoring");

  // NOTE: This call replaces the default layout for the HyperDAQ
  // pages for _all_ applications in the XDAQ executive. Not just for
  // the TCDS applications.
  setLayout(&layout_);

  monitor_.addActionListener(this);
}

tcds::utils::WebServer::~WebServer()
{
}

void
tcds::utils::WebServer::actionPerformed(toolbox::Event& event)
{
  if (event.type() == "MonitoringUpdated")
  {
    updateJSONContents();
  }
}

std::string
tcds::utils::WebServer::getApplicationName() const
{
  std::string xmlClassName = getOwnerApplication().getApplicationDescriptor()->getClassName();
  size_t lastColon = xmlClassName.find_last_of(":");
  std::string appName = xmlClassName;
  if (lastColon != std::string::npos)
    {
      appName = xmlClassName.substr(lastColon + 1);
    }
  return appName;
}

std::string
tcds::utils::WebServer::getServiceName() const
{
  std::string const res = getOwnerApplication().getApplicationDescriptor()->getAttribute("service");
  return res;
}

tcds::utils::WebTab&
tcds::utils::WebServer::getTab(std::string const& tabName) const
{
  std::vector<WebTab*>::const_iterator tab =
    std::find_if(tabs_.begin(), tabs_.end(), WebTab::FindByName(tabName));
  if (tab == tabs_.end())
    {
      std::string msg = "No tab with name '" +
        tabName +
        "' exists in the webserver.";
      ERROR(msg);
      XCEPT_RAISE(tcds::exception::SoftwareProblem, msg);
    }
  return *(*tab);
}

bool
tcds::utils::WebServer::tabExists(std::string const& tabName) const
{
  std::vector<WebTab*>::const_iterator tab =
    std::find_if(tabs_.begin(), tabs_.end(), WebTab::FindByName(tabName));
  return (tab != tabs_.end());
}

void
tcds::utils::WebServer::registerTab(std::string const& name,
                                    std::string const& description,
                                    size_t const numColumns)
{
  // NOTE: It's not a problem if the requested tab already exists, of
  // course. Less work.
  if (!tabExists(name))
    {
      WebTab* tab = new WebTab(name, description, numColumns);
      tabs_.push_back(tab);
    }
}

void
tcds::utils::WebServer::registerTable(std::string const& name,
                                      std::string const& description,
                                      Monitor const& monitor,
                                      std::string const& itemSetName,
                                      std::string const& tabName,
                                      size_t const colSpan)
{
  registerWebObject<WebTable>(name, description, monitor, itemSetName, tabName, colSpan);
}

void
tcds::utils::WebServer::registerSpacer(std::string const& name,
                                       std::string const& description,
                                       Monitor const& monitor,
                                       std::string const& itemSetName,
                                       std::string const& tabName,
                                       size_t const colSpan)
{
  registerWebObject<WebSpacer>(name, description, monitor, itemSetName, tabName, colSpan);
}

void
tcds::utils::WebServer::jsonUpdate(xgi::Input* const in, xgi::Output* const out)
{
  try
    {
      serveJSONUpdate(in, out);
    }
  catch (xcept::Exception& err)
    {
      std::string const msg =
        toolbox::toString("Failed to serve the JSON update. "
                          "Caught an exception: '%s'.", err.what());
      ERROR(msg);
      XCEPT_DECLARE(tcds::exception::RuntimeProblem, top, msg);
      getOwnerApplication().notifyQualified("error", top);
    }
}

void
tcds::utils::WebServer::serveJSONUpdate(xgi::Input* const in, xgi::Output* const out) const
{
  // This should really only ever happen once per 'application life
  // cycle'. It can only happen if a request for a JSON update comes
  // in before the first iteration of the monitoring loop has
  // finished. In that case WebServer::actionPerformed() has not yet
  // been called, and we have nothing to show. In that case we simply
  // show the current situation.
  if (isJSONContentsEmpty())
    {
      updateJSONContents();
    }

  {
    tcds::utils::LockGuard<tcds::utils::Lock> guardedLock(lock_);
    // Stuff everything into the output.
    out->getHTTPResponseHeader().addHeader("Content-Type", "application/json");
    *out << jsonContents_;
  }

}

void
tcds::utils::WebServer::updateJSONContents() const
{
  // Prepare the actual JSON contents.
  std::stringstream tmp("");
  bool caughtError = false;
  std::string errMsg;
  try
    {
      tcds::utils::LockGuard<Lock>(getOwnerApplication().monitoringLock_);
      std::vector<WebTab*>::const_iterator iter;
      for (iter = tabs_.begin(); iter != tabs_.end(); ++iter)
        {
          std::string const jsonTmp = (*iter)->getJSONString();
          if (!jsonTmp.empty())
            {
              if (!tmp.str().empty())
              {
                tmp << ",\n";
              }
              tmp << jsonTmp;
            }
        }
    }
  catch (xcept::Exception& err)
    {
      tmp.str("");
      caughtError = true;
      errMsg = toolbox::toString("Failed to update the monitoring JSON contents: '%s'.",
                                 err.what());
    }

  {
    tcds::utils::LockGuard<tcds::utils::Lock> guardedLock(lock_);
    jsonContents_ = "{\n" + tmp.str() + "\n}";
  }

  if (caughtError)
    {
      ERROR(errMsg);
      XCEPT_RAISE(tcds::exception::SoftwareProblem, errMsg);
    }
}

void
tcds::utils::WebServer::monitoringWebPage(xgi::Input* const in, xgi::Output* const out)
{
  xgi::Output tmpOut;
  tmpOut.str("");
  tmpOut.setHTTPResponseHeader(out->getHTTPResponseHeader());
  try
    {
      monitoringWebPageCore(in, &tmpOut);
      *out << tmpOut.str();
      out->setHTTPResponseHeader(tmpOut.getHTTPResponseHeader());
    }
  catch (xcept::Exception& err)
    {
      std::string const msg =
        toolbox::toString("Failed to serve the monitoring web page. "
                          "Caught an exception: '%s'.", err.what());
      *out << msg;
      ERROR(msg);
      XCEPT_DECLARE(tcds::exception::RuntimeProblem, top, msg);
      getOwnerApplication().notifyQualified("error", top);
    }
}

bool
tcds::utils::WebServer::isJSONContentsEmpty() const
{
  tcds::utils::LockGuard<tcds::utils::Lock> guardedLock(lock_);
  bool const res = jsonContents_.empty();
  return res;
}

void
tcds::utils::WebServer::monitoringWebPageCore(xgi::Input* const in, xgi::Output* const out) const
{
  // NOTE: Since we are using the deferred binding for HTTP calls, the
  // XDAQ framework will take care of the HTML skeleton. We only have
  // to fill in the meat.

  // NOSCRIPT handling. Not very nice, but descriptive it is.
  *out << "<noscript>\n"
       << "<div style=\"position: fixed; top: 0px; left: 0px; z-index: 3000;"
       << "height: 100%; width: 100%; background-color: #FFFFFF\">"
       << "<p style=\"margin-left: 10px\">"
       << "Sorry. The TCDS control applications just don't work without JavaScript, "
       << "and JavaScript seems to be disabled."
       << "</p>"
       << "</div>\n"
       << "</noscript>\n";

  // And then this is the real meat.
  printTabs(out);
}

void
tcds::utils::WebServer::printTabs(xgi::Output* out) const
{
  // NOTE: This has been written to work with the new XDAQ12-style
  // HyperDAQ tabs and doT.js.

  // Start with an overall application title.
  // NOTE: Mix in a bit of templating to show the FSM state name in
  // the title.
  // NOTE: Only show the state name if the state name string is
  // defined and not set to 'n/a'.
  std::string const stateNameSrc =
    "[\"Application state\"][\"Application FSM state\"]";
  *out << "<div class=\"tcds-application-title-wrapper\">"
       << "\n";
  std::string tmpServiceName = getServiceName();
  if (!tmpServiceName.empty())
    {
      tmpServiceName = " '" + tmpServiceName + "'";
    }
  *out << "<h1 id=\"tcds-application-title\">" << getApplicationName() << tmpServiceName << "</h1>"
       << "\n";
  *out << "<script type=\"text/x-dot-template\">"
       << "{{? it" << stateNameSrc << " != 'n/a'}}"
       << "<h2 id=\"tcds-application-subtitle\">" << "({{=it" << stateNameSrc << "}})</h2>"
       << "\n"
       << "{{?}}"
       << "</script>"
       << "\n"
       << "<div class=\"target\"></div>";
  *out << "</div>"
       << "\n";

  // Now plug in the main tab wrapper.
  *out << "<div class=\"xdaq-tab-wrapper\">";
  *out << "\n";

  for (std::vector<WebTab*>::const_iterator tab = tabs_.begin();
       tab != tabs_.end();
       ++tab)
    {
      *out << "<div class=\"xdaq-tab\" "
           << "title=\"" << (*tab)->getName() << "\">"
           << "\n"
           << (*tab)->getHTMLString()
           << "\n"
           << "</div>"
           << "\n";
    }

  *out << "</div>";
  *out << "\n";
}
