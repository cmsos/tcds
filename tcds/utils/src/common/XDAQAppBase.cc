#include "tcds/utils/XDAQAppBase.h"

#include <algorithm>
#include <cassert>
#include <exception>
#include <list>
#include <ostream>
#include <cstddef>
#include <stdint.h>
#include <utility>
#include <vector>

#include "hyperdaq/framework/Layout.h"
#include "sentinel/utils/Alarm.h"
#include "tcds/utils/FSMSOAPParHelper.h"
#include "toolbox/BSem.h"
#include "toolbox/Event.h"
#include "toolbox/TimeInterval.h"
#include "toolbox/TimeVal.h"
#include "toolbox/exception/Exception.h"
#include "toolbox/net/UUID.h"
#include "toolbox/string.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerFactory.h"
#include "xcept/Exception.h"
#include "xcept/tools.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationDescriptor.h"
#include "xdaq/ContextDescriptor.h"
#include "xdaq/InstantiateApplicationEvent.h"
#include "xdaq/NamespaceURI.h"
#include "xdaq/exception/ConfigurationError.h"
#include "xdaq2rc/ClassnameAndInstance.h"
#include "xdata/Bag.h"
#include "xdata/Event.h"
#include "xdata/InfoSpace.h"
#include "xdata/InfoSpaceFactory.h"
#include "xdata/Serializable.h"
#include "xdata/String.h"
#include "xgi/Output.h"
#include "xoap/CountingPtr.h"
#include "xoap/MessageFactory.h"
#include "xoap/Method.h"
#include "xoap/SOAPElement.h"
#include "xoap/SOAPMessage.h"
#include "xoap/SOAPName.h"

#include "tcds/exception/Exception.h"
#include "tcds/hwlayer/ConfigurationProcessor.h"
#include "tcds/hwlayer/DeviceBase.h"
#include "tcds/hwlayer/RegDumpConfigurationProcessor.h"
#include "tcds/utils/ConfigurationInfoSpaceHandler.h"
#include "tcds/utils/Lock.h"
#include "tcds/utils/LockGuard.h"
#include "tcds/utils/LogMacros.h"
#include "tcds/utils/SOAPUtils.h"
#include "tcds/utils/Utils.h"

namespace toolbox {
  namespace task {
    namespace exception {
      class Exception;
    }
  }
}

tcds::utils::XDAQAppBase::XDAQAppBase(xdaq::ApplicationStub* const stub,
                                      std::unique_ptr<tcds::hwlayer::DeviceBase> hw) :
  xdaq::Application(stub),
  appStateInfoSpaceUpdater_(*this),
  appStateInfoSpace_(*this, &appStateInfoSpaceUpdater_),
  hwP_(std::move(hw)),
  logger_(this->getApplicationLogger()),
  monitor_(*this),
  webServer_(*this, monitor_),
  maintenanceMode_(false),
  monitoringLock_(toolbox::BSem::FULL, false),
  timerName_(""),
  timerJobName_("HwLeaseExpiryTimerJob"),
  sentinelInfoSpaceName_("urn:xdaq-sentinel:alarms")
{
  std::string const iconPath = buildIconPathName();
  editApplicationDescriptor()->setAttribute("icon", iconPath);

  // Binding of the ParameterSet SOAP method. The TCDS control
  // applications do not allow direct setting of parameters.
  xoap::bind<XDAQAppBase>(this, &XDAQAppBase::parameterSet, "ParameterSet", XDAQ_NS_URI);

  // Bind the hardware lease renewal.
  xoap::bind<XDAQAppBase>(this, &XDAQAppBase::renewHwLease, "RenewHardwareLease", XDAQ_NS_URI);

  // Binding of two methods to handle entering and releasing of
  // 'maintenance mode.'
  xoap::bind<XDAQAppBase>(this,
                          &XDAQAppBase::maintenanceModeOn,
                          "EnterMaintenanceMode",
                          XDAQ_NS_URI);
  xoap::bind<XDAQAppBase>(this,
                          &XDAQAppBase::maintenanceModeOff,
                          "ReleaseMaintenanceMode",
                          XDAQ_NS_URI);

  //----------

  // Create the auto-expiry timer for the hardware lease.
  timerName_ = toolbox::toString("HwLeaseExpiryTimer_lid%d",
                                 getApplicationDescriptor()->getLocalId());
  hwLeaseExpiryTimerP_ = toolbox::task::getTimerFactory()->createTimer(timerName_);

  //----------

  // Register to be notified when the XDAQ framework loads the
  // configuration values from the XML file.
  getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");

  // Register to be notified when the XDAQ framework has instantiated
  // an application.
  getApplicationContext()->addActionListener(this);

  appStateInfoSpace_.initialize();
  appStateInfoSpace_.addHistoryItem("Application started");

  //----------

  revokeHwLease();
}

tcds::utils::XDAQAppBase::~XDAQAppBase()
{
  monitor_.stopMonitoring();

  revokeHwLease();
  if (hwLeaseExpiryTimerP_)
    {
      try
        {
          toolbox::task::getTimerFactory()->removeTimer(timerName_);
        }
      catch (toolbox::exception::Exception const& err)
        {
          // Nothing to do. Let it go...
        }
      hwLeaseExpiryTimerP_ = 0;
    }
}

void
tcds::utils::XDAQAppBase::redirect(xgi::Input* in, xgi::Output* out, std::string const& msg)
{
  hyperdaq::framework::Layout layout;
  layout.getHTMLHeader(&webServer_, in, out);

  // Show message, if any.
  if (!msg.empty())
    {
      *out << "<div>"
           << "<div style=\"width:30%;margin-left:auto;margin-right:auto\">"
           << "<p>"
           << msg
           << "</p>"
           << "</div>"
           << "</div>";
    }

  // Redirect back to the default page.
  *out << "<script language=\"javascript\">"
       << "setTimeout(function(){window.location=\"" << getFullURL() << "\"} , 3000);"
       << "</script>";
  *out << "<div>"
       << "<div style=\"width:30%;margin-left:auto;margin-right:auto\">"
       << "<p>"
       << "You will be redirected back to the main page soon."
       << "</p>"
       << "</div>"
       << "</div>";

  // Show footer, for completeness.
  layout.getHTMLFooter(&webServer_, in, out);
}

void
tcds::utils::XDAQAppBase::raiseAlarm(std::string const& baseName,
                                     xcept::Exception& err)
{
  // NOTE: There is a bit of jiggery-pokery going on in this
  // method. The idea behind this is to try and keep the alarm
  // reporting and the application history both complete and clean,
  // without imposing constraints on the code that calls
  // raiseAlarm(). (This code is already handling a problem, so let's
  // not stress it further.)

  std::string const name = buildAlarmName(baseName);
  std::string const severity = "alarm";

  //----------

  // We always mark the problem in the application state as well.
  // NOTE: Here we simply keep the latest message (i.e., the current
  // one).
  appStateInfoSpace_.addProblem(name, err.what());

  //----------

  xdata::InfoSpace* alarmInfoSpace =
    xdata::getInfoSpaceFactory()->get(sentinelInfoSpaceName_);

  bool isNewAlarm = !alarmInfoSpace->hasItem(name);
  bool alarmsEqual = false;
  if (!isNewAlarm)
    {
      // Find out what the previous alarm severity and message were.
      sentinel::utils::Alarm* const alarmOld =
        dynamic_cast<sentinel::utils::Alarm*>(alarmInfoSpace->find(name));
      alarmsEqual = err.equals(alarmOld->getException());
    }

  bool const haveToRaiseAlarm = isNewAlarm;
  bool const haveToUpdateAlarm = !isNewAlarm && !alarmsEqual;

  // We only do alarm manipulations if we need to.
  if (haveToRaiseAlarm || haveToUpdateAlarm)
    {
      // NOTE: Some of the exception messages are very verbose. For
      // the history list we reduce this a little bit by cutting off
      // overything after the first newline.
      std::string const msg = err.message();
      std::string const shortMsg = msg.substr(0, msg.find('\n'));
      std::string const verb = haveToRaiseAlarm ? "Raising" : "Updating";
      std::string const histMsg = verb + " alarm '" + name + "'. " + shortMsg;

      // Raise the alarm, if needed.
      if (haveToRaiseAlarm)
        {
          sentinel::utils::Alarm* alarm = new sentinel::utils::Alarm(severity, err, this);
          alarmInfoSpace->fireItemAvailable(name, alarm);
        }
      else if (haveToUpdateAlarm)
        {
          sentinel::utils::Alarm* const alarmOld =
            dynamic_cast<sentinel::utils::Alarm*>(alarmInfoSpace->find(name));
          alarmInfoSpace->fireItemRevoked(name, this);
          delete alarmOld;
          sentinel::utils::Alarm* alarm = new sentinel::utils::Alarm(severity, err, this);
          alarmInfoSpace->fireItemAvailable(name, alarm);
        }

      //----------

      // Mark the raising/updating of the alarm in the history record.
      appStateInfoSpace_.addHistoryItem(histMsg);
    }
}

void
tcds::utils::XDAQAppBase::revokeAlarm(std::string const& baseName,
                                      std::string const& reason)
{
  // Revoke the specified alarm (if it exists).
  std::string const name = buildAlarmName(baseName);

  xdata::InfoSpace* alarmInfoSpace =
    xdata::getInfoSpaceFactory()->get(sentinelInfoSpaceName_);
  if (alarmInfoSpace->hasItem(name))
    {
      sentinel::utils::Alarm* const alarm =
        dynamic_cast<sentinel::utils::Alarm*>(alarmInfoSpace->find(name));
      alarmInfoSpace->fireItemRevoked(name, this);
      delete alarm;

      //----------

      // Mark the revoking of the alarm in the history record.
      std::string histMsg = "Revoking alarm '" + name + "'";
      if (reason.size())
        {
          histMsg += ". " + reason;
        }
      appStateInfoSpace_.addHistoryItem(histMsg);
    }

  //----------

  // Remove the problem from the  application state as well.
  appStateInfoSpace_.removeProblem(name);
}

std::string
tcds::utils::XDAQAppBase::buildAlarmName(std::string const& baseName)
{
  // In principle the service name combined with the instance number
  // should be unique. So let's specialize the alarm names like that.

  std::string const serviceName = getApplicationDescriptor()->getAttribute("service");
  unsigned int const localId = getApplicationDescriptor()->getLocalId();

  // NOTE: Use escape() just in case someone thought it a good idea to
  // have spaces etc. in the service name.
  std::string const alarmName = toolbox::toString("%s:%s:%d",
                                                  baseName.c_str(),
                                                  toolbox::escape(serviceName).c_str(),
                                                  localId);

  return alarmName;
}

void
tcds::utils::XDAQAppBase::handleMonitoringProblem(std::string const& problemDesc)
{
  ERROR("Detected a monitoring problem '" << problemDesc << "'.");
}

xoap::MessageReference
tcds::utils::XDAQAppBase::executeSOAPCommand(xoap::MessageReference& cmd,
                                             xdaq::ApplicationDescriptor const& dest) const
{
  try
    {
      xoap::MessageReference reply = sendSOAP(cmd, dest);
      if (tcds::utils::soap::hasFault(reply))
        {
          std::string msg("");
          std::string const faultString =
            tcds::utils::soap::extractFaultString(reply);
          if (tcds::utils::soap::hasFaultDetail(reply))
            {
              std::string const faultDetail =
                tcds::utils::soap::extractFaultDetail(reply);
              msg =
                toolbox::toString("Received a SOAP fault as reply: '%s: %s'.",
                                  faultString.c_str(),
                                  faultDetail.c_str());
            }
          else
            {
              msg =
                toolbox::toString("Received a SOAP fault as reply: '%s'.",
                                  faultString.c_str());
            }
          XCEPT_RAISE(tcds::exception::RuntimeProblem, msg);
        }
      else
        {
          return reply;
        }
    }
  catch (xcept::Exception const& err)
    {
      std::string const msg = toolbox::toString("Problem executing SOAP command: '%s'.",
                                                err.what());
      XCEPT_RETHROW(tcds::exception::RuntimeProblem,
                    msg,
                    err);
    }
}

xoap::MessageReference
tcds::utils::XDAQAppBase::sendSOAP(xoap::MessageReference& msg,
                                   xdaq::ApplicationDescriptor const& dest) const
{
  xoap::MessageReference const reply = postSOAP(msg, dest);
  return reply;
}

xoap::MessageReference
tcds::utils::XDAQAppBase::postSOAP(xoap::MessageReference& msg,
                                   xdaq::ApplicationDescriptor const& destination) const
{
  xoap::MessageReference const tmp =
    getApplicationContext()->postSOAP(msg,
                                      *getApplicationDescriptor(),
                                      destination);

  // NOTE: The following is necessary to make things work with replies
  // from XDAQ applications in the same executive as the receiving
  // application.
  // Details: https://svnweb.cern.ch/trac/cmsos/ticket/3255
  xoap::MessageReference const reply = xoap::createMessage(tmp);
  return reply;
}

void
tcds::utils::XDAQAppBase::setupInfoSpaces()
{
}

void
tcds::utils::XDAQAppBase::actionPerformed(xdata::Event& event)
{
  // This is called after all default configuration values have been
  // loaded (from the XDAQ configuration file).
  if (event.type() == "urn:xdaq-event:setDefaultValues")
    {
      if (cfgInfoSpaceP_.get() != 0)
        {
          // Make sure the configuration settings are up-to-date.
          cfgInfoSpaceP_->readInfoSpace();

          // Fake an update in order to trigger the necessary update
          // callbacks. Among other things, these callbacks trigger
          // XMAS flashlist updates, etc.
          cfgInfoSpaceP_->writeInfoSpace(true);
        }
    }
}

void
tcds::utils::XDAQAppBase::actionPerformed(toolbox::Event& event)
{
  // This is called after an application has been fully
  // instantiated. Here it is used to detect when our application is
  // fully up and running (according to XDAQ).
  if (event.type() == "urn:xdaq-event:InstantiateApplication")
    {
      xdaq::InstantiateApplicationEvent& de =
        dynamic_cast<xdaq::InstantiateApplicationEvent&>(event);
      DEBUG("DEBUG JGH Application has been instantiated:");
      DEBUG("DEBUG JGH   " << de.getApplicationDescriptor()->getClassName());
      DEBUG("DEBUG JGH   " << de.getApplicationDescriptor()->getLocalId());
      DEBUG("DEBUG JGH   " << de.getApplicationDescriptor()->getUUID().toString());
      if (de.getApplicationDescriptor() == getApplicationDescriptor())
        {
          try
            {
              setupInfoSpaces();
            }
          catch (xcept::Exception const& err)
            {
              ERROR("DEBUG JGH callxxx ERROR TYPE 0");
              std::string const msgBase = "Failed to finish the application configuration";
              std::string const msg = toolbox::toString("%s: '%s'.",
                                                        msgBase.c_str(),
                                                        err.what());
              FATAL(msg);
              XCEPT_RAISE(xdaq::exception::ConfigurationError, msg);
            }
          catch (std::exception const& err)
            {
              ERROR("DEBUG JGH callxxx ERROR TYPE 1");
              std::string const msgBase = "Failed to finish the application configuration";
              std::string const msg = toolbox::toString("%s: '%s'.",
                                                        msgBase.c_str(),
                                                        err.what());
              FATAL(msg);
              XCEPT_RAISE(xdaq::exception::ConfigurationError, msg);
            }

          //----------

          try
            {
              monitor_.startMonitoring();
            }
          catch (xcept::Exception const& err)
            {
              std::string const msgBase = "Failed to start the application monitoring loop";
              std::string const msg = toolbox::toString("%s: '%s'.",
                                                        msgBase.c_str(),
                                                        err.what());
              ERROR(msg);
              appStateInfoSpace_.addMonitoringProblem(msg);
            }
          catch (std::exception const& err)
            {
              std::string const msgBase = "Failed to start the application monitoring loop";
              std::string const msg = toolbox::toString("%s: '%s'.",
                                                        msgBase.c_str(),
                                                        err.what());
              ERROR(msg);
              appStateInfoSpace_.addMonitoringProblem(msg);
            }
        }
    }
}

std::string
tcds::utils::XDAQAppBase::getFullURL()
{
  std::string url = getApplicationDescriptor()->getContextDescriptor()->getURL();
  std::string urn = getApplicationDescriptor()->getURN();
  std::string fullURL = toolbox::toString("%s/%s", url.c_str(), urn.c_str());
  return fullURL;
}

bool
tcds::utils::XDAQAppBase::isHwLeased() const
{
  // This method returns true in case the hardware is currently leased
  // by a session.
  return (hwLeaseOwnerId_.isValid());
}

bool
tcds::utils::XDAQAppBase::wasHwLeased() const
{
  // This method returns true in case the hardware is not currently
  // leased by a session, but it was previously, and the lease
  // expired.
  // NOTE: In case the lease was explicitly revoked (by a Halt
  // command), this method returns false.
  return (!hwLeaseOwnerId_.isValid() && expiredHwLeaseOwnerId_.isValid());
}

tcds::utils::OwnerId
tcds::utils::XDAQAppBase::getHwLeaseOwnerId() const
{
  return hwLeaseOwnerId_;
}

tcds::utils::OwnerId
tcds::utils::XDAQAppBase::getExpiredHwLeaseOwnerId() const
{
  return expiredHwLeaseOwnerId_;
}

void
tcds::utils::XDAQAppBase::assignHwLease(tcds::utils::OwnerId const& leaseOwnerId)
{
  // NOTE: The assumption is that this never gets called with an
  // invalid leaseOwnerId. In order to expire/revoke the lease, call
  // revokeHwLease() instead.

  //----------

  // NOTE: In maintenance mode, lease manipulations are not honored.
  if (maintenanceMode_)
    {
      return;
    }

  //----------

  // ASSERT ASSERT ASSERT
  assert (leaseOwnerId.isValid());
  // ASSERT ASSERT ASSERT end

  // First figure out how long the lease is supposed to last.
  toolbox::TimeInterval leaseDuration = toolbox::TimeVal(10.);
  std::string const tmp = cfgInfoSpaceP_->getString("hardwareLeaseDuration");
  try
    {
      leaseDuration.fromString(tmp);
    }
  catch (toolbox::exception::Exception& err)
    {
      // std::string const state = "Failed to interpret .";
      std::string const msg =
        toolbox::toString("Failed to interpret '%s' as hardware lease duration. "
                          "Using 10 seconds instead.",
                          tmp.c_str());
      ERROR(msg);
      notifyQualified("error", err);
    }
  toolbox::TimeVal endOfLeaseTime =
    toolbox::TimeVal::gettimeofday() + leaseDuration;

  // Check if we are assigning, renewing, or re-assigning the hardware
  // lease.
  bool const renewing = (hwLeaseOwnerId_ == leaseOwnerId);
  bool const reassigning = ((hwLeaseOwnerId_ != leaseOwnerId) &&
                            (expiredHwLeaseOwnerId_ == leaseOwnerId));

  hwLeaseOwnerId_ = leaseOwnerId;
  appStateInfoSpace_.setOwnerId(leaseOwnerId);
  expiredHwLeaseOwnerId_ = tcds::utils::OwnerId();

  // Start the auto-expiry timer.
  try
    {
      // NOTE: This should not be necessary, but it does not hurt
      // either.
      hwLeaseExpiryTimerP_->remove(timerJobName_);
    }
  catch (toolbox::task::exception::Exception const& err)
    {
      // No problem.
    }
  hwLeaseExpiryTimerP_->schedule(this, endOfLeaseTime, 0, timerJobName_);

  std::string msgBase = "";
  if (renewing)
    {
      msgBase = "Renewing hardware lease for";
    }
  else if (reassigning)
    {
      msgBase = "Re-assigning hardware lease to";
    }
  else
    {
      msgBase = "Assigning hardware lease to";
    }
  std::string const histMsg =
    toolbox::toString("%s %s.",
                      msgBase.c_str(),
                      leaseOwnerId.asString().c_str());
  appStateInfoSpace_.addHistoryItem(histMsg);
}

void
tcds::utils::XDAQAppBase::renewHwLease()
{
  assignHwLease(hwLeaseOwnerId_);
}

void
tcds::utils::XDAQAppBase::revokeHwLease(bool const isExpiry)
{
  // NOTE: The isExpiry parameter distinguishes between
  // (unintentional) lease expiry and (intentional) lease revocation.

  //----------

  // NOTE: In maintenance mode, lease manipulations are not honored.
  if (maintenanceMode_)
    {
      return;
    }

  //----------

  // Keep track of what happened.
  tcds::utils::OwnerId const leaseOwnerId = getHwLeaseOwnerId();
  if (leaseOwnerId.isValid())
    {
      std::string tmp = "Releasing";
      if (isExpiry)
        {
          tmp = "Expiring";
        }
      std::string const histMsg =
        toolbox::toString("%s hardware lease of %s.",
                          tmp.c_str(),
                          leaseOwnerId.asString().c_str());
      appStateInfoSpace_.addHistoryItem(histMsg);
    }

  if (hwLeaseExpiryTimerP_)
    {
      try
        {
          hwLeaseExpiryTimerP_->remove(timerJobName_);
        }
      catch (toolbox::task::exception::Exception const& err)
        {
          // This should not happen here, but okay...
        }
    }

  // In case of lease expiry, keep track of the lease owner who lost
  // his lease.
  if (isExpiry)
    {
      expiredHwLeaseOwnerId_ = hwLeaseOwnerId_;
    }
  else
    {
      expiredHwLeaseOwnerId_.clear();
    }

  // Set the lease owner id to an empty owner id to indicate that the
  // lease has expired.
  hwLeaseOwnerId_ = tcds::utils::OwnerId();
  appStateInfoSpace_.setOwnerId(hwLeaseOwnerId_);
}

void
tcds::utils::XDAQAppBase::hwConnect()
{
  if (haveHw() && !getHw().isReadyForUse())
    {
      hwConnectImpl();
    }
}

void
tcds::utils::XDAQAppBase::hwRelease()
{
  hwReleaseImpl();
}

std::string
tcds::utils::XDAQAppBase::readHardwareConfiguration()
{
  return readHardwareConfigurationImpl();
}

std::string
tcds::utils::XDAQAppBase::readHardwareState()
{
  return readHardwareStateImpl();
}

std::string
tcds::utils::XDAQAppBase::readHardwareConfigurationImpl()
{
  // Read the register contents from the hardware.
  tcds::hwlayer::DeviceBase::RegContentsVec hwContents;
  hwContents = hwReadHardwareConfiguration();

  tcds::hwlayer::ConfigurationProcessor::RegValVec hwContentsVec;
  for (tcds::hwlayer::DeviceBase::RegContentsVec::const_iterator i = hwContents.begin();
       i != hwContents.end();
       ++i)
    {
      hwContentsVec.push_back(std::make_pair(i->first.name(), i->second));
    }

  // Now turn the register contents we found into something that we
  // can stuff into a SOAP message.
  tcds::hwlayer::RegDumpConfigurationProcessor cfgProcessor;
  std::string const res = cfgProcessor.compose(hwContentsVec);

  return res;
}

std::string
tcds::utils::XDAQAppBase::readHardwareStateImpl()
{
  // NOTE: Things like hwConnect(), hwRelease(), etc., are protected
  // by a lock in the FSM state transitions. In this case we have to
  // take care of locking ourselves. This ensures that nothing fiddles
  // with the hardware while we're dumping the state.
  LockGuard<Lock> guardedLock(monitoringLock_);

  bool const hwWasConnected = getHw().isReadyForUse();

  if (!hwWasConnected)
    {
      hwConnect();
    }

  tcds::hwlayer::ConfigurationProcessor::RegValVec hwContentsVec = getHw().dumpRegisterContents();

  if (!hwWasConnected)
    {
      hwRelease();
    }

  // Now turn the register contents we found into something that we
  // can stuff into a SOAP message.
  tcds::hwlayer::RegDumpConfigurationProcessor cfgProcessor;
  std::string const res = cfgProcessor.compose(hwContentsVec);
  return res;
}

std::string
tcds::utils::XDAQAppBase::buildIconPathName(xdaq::ApplicationDescriptor const* const app)
{
  // The XDAQ application icon is set based on the (XML) class name.
  // NOTE: This assumes that all TCDS packages are structured the same
  // way using the following application class naming:
  // tcds::packageName::applicationName. (And the same thing with
  // single colons in XML.)
  xdaq::ApplicationDescriptor const* tmp = app;
  if (tmp == 0)
    {
      tmp = getApplicationDescriptor();
    }
  std::string const className = tmp->getClassName();
  std::list<std::string> const classNamePiecesTmp =
    toolbox::parseTokenList(className, ":");
  std::vector<std::string> const classNamePieces =
    std::vector<std::string>(classNamePiecesTmp.begin(), classNamePiecesTmp.end());
  std::string iconFileName = "/tcds/utils/images/tcds_generic_icon.png";
  if (classNamePieces.size() > 2)
    {
      size_t const index = classNamePieces.size() - 2;
      std::string const packageName = toolbox::tolower(classNamePieces.at(index));
      std::string const applicationName = toolbox::tolower(classNamePieces.back());
      iconFileName = "/tcds/" + packageName + "/images/" + applicationName + "_icon.png";
    }
  return iconFileName;
}

tcds::utils::ConfigurationInfoSpaceHandler const&
tcds::utils::XDAQAppBase::getConfigurationInfoSpaceHandler() const
{
  return static_cast<tcds::utils::ConfigurationInfoSpaceHandler&>(*cfgInfoSpaceP_.get());
}

xoap::MessageReference
tcds::utils::XDAQAppBase::parameterSet(xoap::MessageReference msg)
{
  // It is not allowed to set parameters straight from SOAP: log and
  // send error back.
  std::string errMsg = "Someone is trying to set an InfoSpace parameter via SOAP. "
    "This is not allowed, and will be ignored.";
  appStateInfoSpace_.addHistoryItem(errMsg);
  ERROR(errMsg);
  XCEPT_DECLARE(tcds::exception::SOAPCommandProblem, top, errMsg);
  notifyQualified("error", top);

  std::string soapErrMsg = "Direct setting of InfoSpace parameters via SOAP is not allowed.";
  tcds::utils::soap::SOAPFaultCodeType faultCode = tcds::utils::soap::SOAPFaultCodeReceiver;
  std::string faultString = soapErrMsg;
  xoap::MessageReference reply =
    tcds::utils::soap::makeSOAPFaultReply(this,
                                          msg,
                                          faultCode,
                                          faultString);
  return reply;
}

bool
tcds::utils::XDAQAppBase::haveHw() const
{
  return (hwP_.get() != 0);
}

tcds::hwlayer::DeviceBase&
tcds::utils::XDAQAppBase::getHw() const
{
  return static_cast<tcds::hwlayer::DeviceBase&>(*hwP_.get());
}

xoap::MessageReference
tcds::utils::XDAQAppBase::renewHwLease(xoap::MessageReference msg)
{
  // First question: Is there a lease owner currently?
  if (!isHwLeased() && !wasHwLeased())
    {
      // No lease owner, and there was no owner who lost his lease. So
      // there is nothing to renew. Send an error back.
      std::string const histMsg = "Ignoring spurious request for hardware lease renewal.";
      appStateInfoSpace_.addHistoryItem(histMsg);
      std::string msgBase = "There is no hardware lease to renew.";
      ERROR(msgBase);
      XCEPT_DECLARE(tcds::exception::SOAPCommandProblem, top, msgBase);
      notifyQualified("error", top);
      tcds::utils::soap::SOAPFaultCodeType faultCode = tcds::utils::soap::SOAPFaultCodeReceiver;
      std::string faultString = msgBase;
      xoap::MessageReference reply =
        tcds::utils::soap::makeSOAPFaultReply(this,
                                              msg,
                                              faultCode,
                                              faultString);
      return reply;
    }
  else
    {
      // Second question: Who is the current lease owner? Or who was
      // the lease owner who lost his lease?
      tcds::utils::OwnerId leaseOwnerId;
      if (isHwLeased())
        {
          leaseOwnerId = getHwLeaseOwnerId();
        }
      else if (wasHwLeased())
        {
          leaseOwnerId = getExpiredHwLeaseOwnerId();
        }

      // Third question: Does the current request come from our
      // hardware owner?
      std::string commandName = "undefined";
      tcds::utils::OwnerId requestorId;
      try
        {
          commandName = tcds::utils::soap::extractSOAPCommandName(msg);
          requestorId = tcds::utils::soap::extractSOAPCommandOwnerId(msg);
        }
      catch(tcds::exception::Exception const& err)
        {
          // Somehow we don't understand the SOAP message. Log an
          // error and flag that we are having trouble.
          std::string const histMsg = "Received an ununderstood SOAP command."
            " See the SOAP reply for details.";
          appStateInfoSpace_.addHistoryItem(histMsg);
          std::string msgBase = "SOAP command not understood";
          ERROR(toolbox::toString("%s: '%s'.",
                                  msgBase.c_str(),
                                  err.message().c_str()));
          XCEPT_DECLARE_NESTED(tcds::exception::HwLeaseRenewalProblem, top,
                               toolbox::toString("%s.", msgBase.c_str()), err);
          notifyQualified("error", top);
          tcds::utils::soap::SOAPFaultCodeType faultCode = tcds::utils::soap::SOAPFaultCodeSender;
          std::string faultString = msgBase;
          std::string faultDetail = toolbox::toString("%s: '%s'.",
                                                      msgBase.c_str(),
                                                      err.message().c_str());
          xoap::MessageReference reply =
            tcds::utils::soap::makeSOAPFaultReply(this,
                                                  msg,
                                                  faultCode,
                                                  faultString,
                                                  faultDetail);
          return reply;
        }

      if (requestorId != leaseOwnerId)
        {
          // No match -> send error back.
          std::string const histMsg =
            toolbox::toString("Hardware lease renewal not allowed: "
                              "hardware lease owned by %s.",
                              leaseOwnerId.asString().c_str());
          appStateInfoSpace_.addHistoryItem(histMsg);
          std::string msgBase = histMsg;
          ERROR(msgBase);
          XCEPT_DECLARE(tcds::exception::SOAPCommandProblem, top, msgBase);
          notifyQualified("error", top);
          tcds::utils::soap::SOAPFaultCodeType faultCode = tcds::utils::soap::SOAPFaultCodeReceiver;
          std::string faultString = msgBase;
          xoap::MessageReference reply =
            tcds::utils::soap::makeSOAPFaultReply(this,
                                                  msg,
                                                  faultCode,
                                                  faultString);
          return reply;
        }
      else
        {
          // Ok, everything checks out. Do what we were asked to do.
          assignHwLease(leaseOwnerId);

          //----------

          // Reply that all is well.
          std::string soapProtocolVersion = msg->getImplementationFactory()->getProtocolVersion();
          try
            {
              xoap::MessageReference reply =
                tcds::utils::soap::makeCommandSOAPReply(soapProtocolVersion,
                                                        commandName,
                                                        "");
              return reply;
            }
          catch(xcept::Exception const& err)
            {
              std::string msgBase =
                toolbox::toString("Failed to create SOAP reply for command '%s'",
                                  commandName.c_str());
              ERROR(toolbox::toString("%s: %s.",
                                      msgBase.c_str(),
                                      err));
              XCEPT_DECLARE_NESTED(tcds::exception::RuntimeProblem, top,
                                   toolbox::toString("%s.", msgBase.c_str()), err);
              notifyQualified("error", top);
              XCEPT_RETHROW(xoap::exception::Exception, msgBase, err);
            }
        }
    }

  //----------

  // Should only get here in case the above SOAP reply fails. In that
  // case: return an empty message.
  return xoap::createMessage();
}

xoap::MessageReference
tcds::utils::XDAQAppBase::maintenanceModeOn(xoap::MessageReference msg)
{
  // NOTE: This could be improved a bit with a more sophisticated
  // handling of the maintenance mode switching.
  tcds::utils::OwnerId requestorId;
  try
    {
      requestorId = tcds::utils::soap::extractSOAPCommandOwnerId(msg);
    }
  catch(xcept::Exception const& err)
    {
      std::string const histMsg =
        toolbox::toString("Ignoring 'enter maintenance mode' request: "
                          "'%s'.",
                          err.what());
      appStateInfoSpace_.addHistoryItem(histMsg);
      std::string msgBase = histMsg;
      ERROR(msgBase);
      XCEPT_DECLARE(tcds::exception::RuntimeProblem, top, msgBase);
      notifyQualified("error", top);
      tcds::utils::soap::SOAPFaultCodeType faultCode = tcds::utils::soap::SOAPFaultCodeReceiver;
      std::string faultString = msgBase;
      xoap::MessageReference reply =
        tcds::utils::soap::makeSOAPFaultReply(this,
                                              msg,
                                              faultCode,
                                              faultString);
      return reply;
    }

  switchMaintenanceMode(true, requestorId);

  // Reply.
  xoap::MessageReference reply = tcds::utils::soap::makeCommandSOAPReply(msg);
  return reply;
}

xoap::MessageReference
tcds::utils::XDAQAppBase::maintenanceModeOff(xoap::MessageReference msg)
{
  // NOTE: This could be improved a bit with a more sophisticated
  // handling of the maintenance mode switching.
  tcds::utils::OwnerId requestorId;
  try
    {
      requestorId = tcds::utils::soap::extractSOAPCommandOwnerId(msg);
    }
  catch(xcept::Exception const& err)
    {
      std::string const histMsg =
        toolbox::toString("Ignoring 'release maintenance mode' request: "
                          "'%s'.",
                          err.what());
      appStateInfoSpace_.addHistoryItem(histMsg);
      std::string msgBase = histMsg;
      ERROR(msgBase);
      XCEPT_DECLARE(tcds::exception::RuntimeProblem, top, msgBase);
      notifyQualified("error", top);
      tcds::utils::soap::SOAPFaultCodeType faultCode = tcds::utils::soap::SOAPFaultCodeReceiver;
      std::string faultString = msgBase;
      xoap::MessageReference reply =
        tcds::utils::soap::makeSOAPFaultReply(this,
                                              msg,
                                              faultCode,
                                              faultString);
      return reply;
    }

  switchMaintenanceMode(false, requestorId);

  // Reply.
  xoap::MessageReference reply = tcds::utils::soap::makeCommandSOAPReply(msg);
  return reply;
}

void
tcds::utils::XDAQAppBase::switchMaintenanceMode(bool const on,
                                                tcds::utils::OwnerId const& maintainerId)
{
  if (on && !maintenanceMode_)
    {
      std::string const msg = "Switching from operations to maintenance mode";
      INFO(msg);
      appStateInfoSpace_.addHistoryItem(msg);

      // Keep track of the 'true' owner if we are switching into
      // maintenance mode.
      hwLeaseOwnerIdPrev_ = hwLeaseOwnerId_;
      shouldHaveALeaseOwner_ = hwLeaseOwnerIdPrev_.isValid();

      // Assign the hardware lease to the maintainer.
      // NOTE: First assign owner, then switch modes.
      assignHwLease(maintainerId);
      setMaintenanceMode(on);
    }
  else if (!on && maintenanceMode_)
    {
      std::string const msg = "Switching from maintenance to operations mode";
      INFO(msg);
      appStateInfoSpace_.addHistoryItem(msg);

      // Restore the 'true' lease owner if we're switching out of
      // maintenance mode.
      // NOTE: First switch modes, then assign owner.
      setMaintenanceMode(on);
      if (shouldHaveALeaseOwner_)
        {
          assignHwLease(hwLeaseOwnerIdPrev_);
        }
      else
        {
          revokeHwLease();
        }
    }
}

void
tcds::utils::XDAQAppBase::setMaintenanceMode(bool const on)
{
  // Handle the flag.
  maintenanceMode_ = on;
  appStateInfoSpace_.setBool("maintenanceMode", maintenanceMode_);
}

void
tcds::utils::XDAQAppBase::timeExpired(toolbox::task::TimerEvent& event)
{
  // NOTE: In maintenance mode the hardware lease does not expire.
  if (!maintenanceMode_)
    {
      INFO("The time has come: expiring hardware lease.");
      revokeHwLease(true);
    }
}

tcds::hwlayer::DeviceBase::RegContentsVec
tcds::utils::XDAQAppBase::hwReadHardwareConfiguration() const
{
  tcds::hwlayer::RegisterInfo::RegInfoVec regInfos = hwP_->getRegisterInfos();
  tcds::hwlayer::RegisterInfo::RegInfoVec regInfosFiltered = filterRegInfo(regInfos);
  return hwP_->readHardwareConfiguration(regInfosFiltered);
}

tcds::hwlayer::RegisterInfo::RegInfoVec
tcds::utils::XDAQAppBase::filterRegInfo(tcds::hwlayer::RegisterInfo::RegInfoVec const& regInfosIn) const
{
  // Filter the full hardware contents to remove 'the things that
  // should not be.'
  tcds::hwlayer::RegisterInfo::RegInfoVec regInfosOut;

  for (tcds::hwlayer::RegisterInfo::RegInfoVec::const_iterator regInfo = regInfosIn.begin();
       regInfo != regInfosIn.end();
       ++regInfo)
    {
      // Only registers that can be read _and_ written are useful in a
      // configuration dump.
      if (regInfo->isReadable() && regInfo->isWritable())
        {
          // Not all registers are allowed to be used as configuration
          // settings. Let's filter out the disallowed ones.
          if (isRegisterAllowed(*regInfo))
            {
              regInfosOut.push_back(*regInfo);
            }
        }
    }

  return regInfosOut;
}

tcds::utils::RegCheckResult
tcds::utils::XDAQAppBase::isRegisterAllowed(tcds::hwlayer::RegisterInfo const& regInfo) const
{
  return tcds::utils::kRegCheckResultOK;
}

std::vector<tcds::utils::FSMSOAPParHelper>
tcds::utils::XDAQAppBase::expectedFSMSoapPars(std::string const& commandName) const
{
  return expectedFSMSoapParsImpl(commandName);
}

std::vector<tcds::utils::FSMSOAPParHelper>
tcds::utils::XDAQAppBase::expectedFSMSoapParsImpl(std::string const& commandName) const
{
  // Define what we expect in terms of parameters for each SOAP FSM
  // command.
  std::vector<FSMSOAPParHelper> params;
  if ((commandName == "Configure") ||
      (commandName == "Reconfigure"))
    {
      params.push_back(FSMSOAPParHelper(commandName, "hardwareConfigurationString", true));
    }
  else if (commandName == "Enable")
    {
      params.push_back(FSMSOAPParHelper(commandName, "runNumber", true));
    }

  return params;
}

void
tcds::utils::XDAQAppBase::loadSOAPCommandParameters(xoap::MessageReference const& msg)
{
  // Any command can take (optionally) an xdaq::rcmsURL
  // parameter. This is used for asynchronous reporting of FSM state
  // changes.
  // NOTE: An empty string as rcmsURL is used to disable the
  // asynchronous state change notifications.
  std::string rcmsURL = "";
  bool urlFound = false;
  try
    {
      rcmsURL = tcds::utils::soap::extractSOAPCommandRCMSURL(msg);
      urlFound = true;
    }
  catch(tcds::exception::Exception const& err)
    {
      // This is okay. If no RCMS url is specified, we continue
      // without.
    }
  if (urlFound)
    {
      std::string noteStr = "";
      if (rcmsURL.empty())
        {
          noteStr = " (I.e., disabling asynchronous state change notifications.)";
        }
      INFO(toolbox::toString("Updating RCMS state notification listener URL to '%s'.%s",
                             rcmsURL.c_str(),
                             noteStr.c_str()));
      xdata::Serializable* tmp = getApplicationInfoSpace()->find("rcmsStateListener");
      xdata::Bag<xdaq2rc::ClassnameAndInstance>* bag = dynamic_cast<xdata::Bag<xdaq2rc::ClassnameAndInstance>*>(tmp);
      bag->getField("url")->setValue(xdata::String(rcmsURL));
      getApplicationInfoSpace()->fireItemValueChanged("rcmsStateListener");
    }

  //----------

  loadSOAPCommandParametersImpl(msg);
}

void
tcds::utils::XDAQAppBase::loadSOAPCommandParametersImpl(xoap::MessageReference const& msg)
{
  std::string const commandName = tcds::utils::soap::extractSOAPCommandName(msg);

  //----------

  // Figure out what to expect in terms of parameters for this FSM
  // SOAP command.
  std::vector<FSMSOAPParHelper> paramsExpected = expectedFSMSoapPars(commandName);
  std::vector<std::string> paramNamesExpected;
  for (std::vector<FSMSOAPParHelper>::const_iterator param = paramsExpected.begin();
       param != paramsExpected.end();
       ++param)
    {
      paramNamesExpected.push_back(param->parameterName());
    }

  //----------

  // Figure out which parameter we received.
  xoap::SOAPElement commandNode = tcds::utils::soap::extractBodyNode(msg);
  std::vector<xoap::SOAPElement> paramsFound = commandNode.getChildElements();

  //----------

  // Check if all required parameters are present.
  for (std::vector<FSMSOAPParHelper>::const_iterator param = paramsExpected.begin();
       param != paramsExpected.end();
       ++param)
    {
      if (param->isRequired())
        {
          std::string const parName = param->parameterName();
          if (!tcds::utils::soap::hasSOAPCommandParameter(msg, parName))
            {
              // Missing a required parameter.
              std::string const msg =
                toolbox::toString("Missing a SOAP command parameter "
                                  "that is required for '%s': '%s.'",
                                  commandName.c_str(),
                                  parName.c_str());
              ERROR(msg);
              XCEPT_DECLARE(tcds::exception::SOAPCommandProblem, err, msg);
              notifyQualified("error", err);
              XCEPT_RAISE(tcds::exception::SOAPCommandProblem, msg);
            }
        }
    }

  //----------

  // Check for unexpected/unallowed parameters.
  for (std::vector<xoap::SOAPElement>::iterator param = paramsFound.begin();
       param != paramsFound.end();
       ++param)
    {
      std::string const parName = param->getElementName().getLocalName();
      std::vector<std::string>::iterator i =
        std::find(paramNamesExpected.begin(), paramNamesExpected.end(), parName);
      if (i == paramNamesExpected.end())
        {
          // Found a parameter that we did not expect.
          std::string const msg =
            toolbox::toString("Found a SOAP command parameter "
                              "that is not accepted for '%s': '%s.'",
                              commandName.c_str(),
                              parName.c_str());
          ERROR(msg);
          XCEPT_DECLARE(tcds::exception::SOAPCommandProblem, err, msg);
          notifyQualified("error", err);
          XCEPT_RAISE(tcds::exception::SOAPCommandProblem, msg);
        }
    }

  //----------

  // Loop through all expected parameters and (try to) load them.
  for (std::vector<FSMSOAPParHelper>::const_iterator param = paramsExpected.begin();
       param != paramsExpected.end();
       ++param)
    {
      loadSOAPCommandParameter(msg, *param);
    }
}

void
tcds::utils::XDAQAppBase::loadSOAPCommandParameter(xoap::MessageReference const& msg,
                                                   FSMSOAPParHelper const& param)
{
  loadSOAPCommandParameterImpl(msg, param);
}

void
tcds::utils::XDAQAppBase::loadSOAPCommandParameterImpl(xoap::MessageReference const& msg,
                                                       FSMSOAPParHelper const& param)
{
  // NOTE: The assumption is that when entering this method, the
  // command-to-parameter mapping, required parameter presence,
  // etc. have all been checked already. This method only concerns the
  // actual loading of the parameters.

  std::string const parName = param.parameterName();

  if (parName == "hardwareConfigurationString")
    {
      // Import 'xdaq:hardwareConfigurationString'.
      std::string hwCfgString =
        tcds::utils::soap::extractSOAPCommandParameterString(msg, parName);
      // NOTE: explicitly mark empty strings, simply for clarity.
      if (tcds::utils::trimString(hwCfgString, " \t\n").empty())
        {
          hwCfgString =
            toolbox::toString("%c Empty string received. "
                              "This message was inserted on the receiving side for clarity.",
                              tcds::hwlayer::RegDumpConfigurationProcessor::kCommentChar);
        }
      cfgInfoSpaceP_->setString("hardwareConfigurationStringReceived",
                                hwCfgString);
    }
  else if (parName == "runNumber")
    {
      // Import 'xdaq:runNumber'.
      uint32_t const runNumber =
        tcds::utils::soap::extractSOAPCommandParameterUnsignedInteger(msg, parName);
      cfgInfoSpaceP_->setUInt32(parName, runNumber);
    }
}
