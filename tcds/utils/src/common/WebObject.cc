#include "tcds/utils/WebObject.h"

#include <sstream>

#include "tcds/utils/Monitor.h"
#include "tcds/utils/Utils.h"

std::string const tcds::utils::WebObject::kStringInvalidItem("-");
std::string const tcds::utils::WebObject::kStringUnknownItem("?");

std::string const tcds::utils::WebObject::kDefaultValueString = "-";

tcds::utils::WebObject::WebObject(std::string const& name,
                                  std::string const& description,
                                  Monitor const& monitor,
                                  std::string const& itemSetName,
                                  std::string const& tabName,
                                  size_t const colSpan) :
  name_(name),
  description_(description),
  monitor_(monitor),
  itemSetName_(itemSetName),
  tabName_(tabName),
  colSpan_(colSpan)
{
}

tcds::utils::WebObject::~WebObject()
{
}

std::string
tcds::utils::WebObject::getName() const
{
  return name_;
}

std::string
tcds::utils::WebObject::getDescription() const
{
  return description_;
}

std::string
tcds::utils::WebObject::getItemSetName() const
{
  return itemSetName_;
}

std::string
tcds::utils::WebObject::getTabName() const
{
  return tabName_;
}

size_t
tcds::utils::WebObject::getColSpan() const
{
  return colSpan_;
}

std::string
tcds::utils::WebObject::getJSONString() const
{
  Monitor::StringPairVector items = monitor_.getFormattedItemSet(itemSetName_);
  Monitor::StringPairVector::const_iterator iter;

  std::stringstream tmp("");
  for (iter = items.begin(); iter != items.end(); ++iter)
    {
      std::string name = escapeAsJSONString(iter->first);
      std::string value = iter->second;
      if (!tmp.str().empty())
        {
          tmp << ",\n";
        }
      tmp << name << ": " << value;
    }

  std::string res =
    escapeAsJSONString(itemSetName_) +
    ": {\n" +
    tmp.str() +
    "\n}";

  return res;
}
