#include "tcds/utils/Lock.h"

tcds::utils::Lock::Lock(toolbox::BSem::State state, bool recursive) :
  semaphore_(state, recursive)
{
}

tcds::utils::Lock::~Lock()
{
  unlock();
}

void
tcds::utils::Lock::lock()
{
  semaphore_.take();
}

void
tcds::utils::Lock::unlock()
{
  semaphore_.give();
}
