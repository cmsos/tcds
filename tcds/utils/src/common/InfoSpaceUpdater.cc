#include "tcds/utils/InfoSpaceUpdater.h"

#include <string>
#include <vector>

#include "toolbox/string.h"
#include "xcept/Exception.h"

#include "tcds/exception/Exception.h"
#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/InfoSpaceItem.h"

tcds::utils::InfoSpaceUpdater::InfoSpaceUpdater(xdaq::Application& xdaqApp) :
  XDAQObject(xdaqApp)
{
}

tcds::utils::InfoSpaceUpdater::~InfoSpaceUpdater()
{
}

void
tcds::utils::InfoSpaceUpdater::updateInfoSpace(InfoSpaceHandler* const infoSpaceHandler)
{
  try
    {
      updateInfoSpaceImpl(infoSpaceHandler);
    }
  catch (...)
    {
      infoSpaceHandler->setInvalid();
      throw;
    }
}

void
tcds::utils::InfoSpaceUpdater::updateInfoSpaceImpl(InfoSpaceHandler* const infoSpaceHandler)
{
  InfoSpaceHandler::ItemVec& items = infoSpaceHandler->getItems();
  InfoSpaceHandler::ItemVec::iterator iter;

  for (iter = items.begin(); iter != items.end(); ++iter)
    {
    try
      {
        updateInfoSpaceItem(*iter, infoSpaceHandler);
      }
    catch (xcept::Exception const& err)
      {
        iter->setInvalid();
        std::string msg =
          toolbox::toString("Updating failed for item with name '%s': '%s'.",
                            iter->name().c_str(),
                            err.what());
        XCEPT_RAISE(tcds::exception::SoftwareProblem, msg);
      }
    catch (...)
      {
        iter->setInvalid();
        std::string msg =
          toolbox::toString("Updating failed for item with name '%s'.",
                            iter->name().c_str());
        XCEPT_RAISE(tcds::exception::SoftwareProblem, msg);
      }
    }

  // Now sync everything from the cache to the InfoSpace itself.
  infoSpaceHandler->writeInfoSpace();
}

bool
tcds::utils::InfoSpaceUpdater::updateInfoSpaceItem(InfoSpaceItem& item,
                                                   InfoSpaceHandler* const infoSpaceHandler)
{
  bool isUpToDate = false;

  // Nothing is implemented here. It's simply a catch-all which raises
  // an exception to show that something is missing.
  tcds::utils::InfoSpaceItem::UpdateType updateType = item.updateType();
  if (updateType == tcds::utils::InfoSpaceItem::NOUPDATE)
    {
      isUpToDate = true;
    }
  else
    {
      std::string msg =
        toolbox::toString("Updating not implemented for item with name '%s'.",
                          item.name().c_str());
      item.setInvalid();
      XCEPT_RAISE(tcds::exception::SoftwareProblem, msg);
      isUpToDate = false;
    }

  return isUpToDate;
}
