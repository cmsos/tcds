#include "tcds/utils/InfoSpaceHandlerLocker.h"

#include "xdata/InfoSpace.h"

#include "tcds/utils/InfoSpaceHandler.h"

tcds::utils::InfoSpaceHandlerLocker::InfoSpaceHandlerLocker(tcds::utils::InfoSpaceHandler const* const infoSpaceHandler) :
  infoSpaceHandler_(infoSpaceHandler)
{
  lock();
}

tcds::utils::InfoSpaceHandlerLocker::~InfoSpaceHandlerLocker()
{
  unlock();
}

void
tcds::utils::InfoSpaceHandlerLocker::lock() const
{
  infoSpaceHandler_->infoSpaceP_->lock();
  if (infoSpaceHandler_->mirrorInfoSpaceP_)
    {
      try
        {
          infoSpaceHandler_->mirrorInfoSpaceP_->lock();
        }
      catch (...)
        {
          infoSpaceHandler_->infoSpaceP_->unlock();
          throw;
        }
    }
}

void
tcds::utils::InfoSpaceHandlerLocker::unlock() const
{
  try
    {
      infoSpaceHandler_->infoSpaceP_->unlock();
    }
  catch (...)
    {
      // Stumble on.
    }

  if (infoSpaceHandler_->mirrorInfoSpaceP_)
    {
      try
        {
          infoSpaceHandler_->mirrorInfoSpaceP_->unlock();
        }
      catch (...)
        {
          // Stumble on.
        }
    }
}
