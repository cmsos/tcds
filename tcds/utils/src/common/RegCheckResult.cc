#include "tcds/utils/RegCheckResult.h"

tcds::utils::RegCheckResult::RegCheckResult(tcds::definitions::REG_FAIL_REASON const reason) :
  reason_(reason)
{
}

tcds::utils::RegCheckResult::~RegCheckResult()
{
}

bool
tcds::utils::RegCheckResult::operator==(tcds::utils::RegCheckResult const& rhs) const
{
  return (reason_ == rhs.reason_);
}

bool
tcds::utils::RegCheckResult::operator!=(tcds::utils::RegCheckResult const& rhs) const
{
  return !(*this == rhs);
}

bool
tcds::utils::RegCheckResult::booleanTest() const
{
  return (reason_ == tcds::definitions::REG_FAIL_REASON_ALL_OK);
}
