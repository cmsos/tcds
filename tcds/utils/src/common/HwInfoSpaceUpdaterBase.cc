#include "tcds/utils/HwInfoSpaceUpdaterBase.h"

#include <stdint.h>
#include <string>
#include <vector>

#include "toolbox/string.h"
#include "xcept/Exception.h"

#include "tcds/exception/Exception.h"
#include "tcds/hwlayer/DeviceBase.h"
#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/XDAQAppBase.h"

tcds::utils::HwInfoSpaceUpdaterBase::HwInfoSpaceUpdaterBase(tcds::utils::XDAQAppBase& xdaqApp,
                                                            tcds::hwlayer::DeviceBase const& hw) :
  tcds::utils::InfoSpaceUpdater(xdaqApp),
  tcds::utils::TCDSObject(xdaqApp),
  hw_(hw)
{
}

tcds::utils::HwInfoSpaceUpdaterBase::~HwInfoSpaceUpdaterBase()
{
}

void
tcds::utils::HwInfoSpaceUpdaterBase::updateInfoSpaceImpl(InfoSpaceHandler* const infoSpaceHandler)
{
  tcds::hwlayer::DeviceBase const& hw = getHw();
  if (hw.isReadyForUse())
    {
      InfoSpaceHandler::ItemVec& items = infoSpaceHandler->getItems();
      InfoSpaceHandler::ItemVec::iterator iter;

      for (iter = items.begin(); iter != items.end(); ++iter)
        {
          try
            {
              updateInfoSpaceItem(*iter, infoSpaceHandler);
            }
          catch (xcept::Exception const& err)
            {
              iter->setInvalid();
              std::string msg =
                toolbox::toString("Updating failed for item with name '%s': '%s'.",
                                  iter->name().c_str(),
                                  err.what());
              XCEPT_RAISE(tcds::exception::SoftwareProblem, msg);
            }
          catch (...)
            {
              iter->setInvalid();
              std::string msg =
                toolbox::toString("Updating failed for item with name '%s'.",
                                  iter->name().c_str());
              XCEPT_RAISE(tcds::exception::SoftwareProblem, msg);
            }
        }

      // Now sync everything from the cache to the InfoSpace itself.
      infoSpaceHandler->writeInfoSpace();
    }
  else
    {
      infoSpaceHandler->setInvalid();
    }
}

bool
tcds::utils::HwInfoSpaceUpdaterBase::updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                                         tcds::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  bool updated = false;
  tcds::hwlayer::DeviceBase const& hw = getHw();
  if (hw.isReadyForUse())
    {
      std::string name = item.name();
      tcds::utils::InfoSpaceItem::UpdateType updateType = item.updateType();
      tcds::utils::InfoSpaceItem::ItemType itemType = item.type();
      if (updateType == tcds::utils::InfoSpaceItem::HW32)
        {
          uint32_t const tmp = hw_.readRegister(item.hwName());
          if (itemType == tcds::utils::InfoSpaceItem::UINT32)
            {
              infoSpaceHandler->setUInt32(name, tmp);
              updated = true;
            }
          else if (itemType == tcds::utils::InfoSpaceItem::BOOL)
            {
              infoSpaceHandler->setBool(name, (tmp != 0));
              updated = true;
            }
        }
      else if (updateType == tcds::utils::InfoSpaceItem::HW32BLOCK)
        {
          if (itemType == tcds::utils::InfoSpaceItem::UINT32VEC)
            {
              std::vector<uint32_t> const tmp = hw_.readBlock(item.hwName());
              infoSpaceHandler->setUInt32Vec(name, tmp);
              updated = true;
            }
        }
      if (!updated)
        {
          updated = tcds::utils::InfoSpaceUpdater::updateInfoSpaceItem(item, infoSpaceHandler);
        }
    }

  if (updated)
    {
      item.setValid();
    }
  else
    {
      item.setInvalid();
    }

  return updated;
}

tcds::hwlayer::DeviceBase const&
tcds::utils::HwInfoSpaceUpdaterBase::getHw() const
{
  return hw_;
}
