#include "tcds/utils/WebSpacer.h"

#include <sstream>
#include <string>

tcds::utils::WebSpacer::WebSpacer(std::string const& name,
                                  std::string const& description,
                                  Monitor const& monitor,
                                  std::string const& itemSetName,
                                  std::string const& tabName,
                                  size_t const colSpan) :
  WebObject(name, description, monitor, itemSetName, tabName, colSpan)
{
}

std::string
tcds::utils::WebSpacer::getHTMLString() const
{
  std::stringstream res;

  res << "<div class=\"tcds-item-table-wrapper\">";
  res << "</div>";
  res << "\n";

  return res.str();
}

std::string
tcds::utils::WebSpacer::getJSONString() const
{
  return "";
}
