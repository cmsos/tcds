#include "tcds/utils/PSXReply.h"

#include <cassert>
#include <stdint.h>

#include "toolbox/string.h"

#include "tcds/utils/DIPUtils.h"
#include "tcds/utils/SOAPUtils.h"

tcds::utils::PSXReply::PSXReply(xoap::MessageReference& rawReply) :
  rawReply_(rawReply)
{
}

tcds::utils::PSXReply::~PSXReply()
{
}

bool
tcds::utils::PSXReply::isValid(std::string const& dpName)
{
  std::string tmp = tcds::utils::extractPSXDPGetReply(rawReply_, dpName + ":_online.._invalid");
  bool const invalid = (toolbox::tolower(tmp) == "true");
  return !invalid;
}

bool
tcds::utils::PSXReply::isGood(std::string const& dpName)
{
  bool const valid = isValid(dpName);

  std::string const dipQual = extractDIPQuality(dpName);

  return (valid && (dipQual == "GOOD"));
}

std::string
tcds::utils::PSXReply::extractRawResult(std::string const& dpName)
{
  return tcds::utils::extractPSXDPGetReply(rawReply_, dpName + ":_online.._value");
}

std::string
tcds::utils::PSXReply::extractDIPQuality(std::string const& dpName)
{
  // This is how the DIP data point quality is translated into the
  // user bits of the PVSS data point:
  //   https://readthedocs.web.cern.ch/display/ICKB/DIP+FAQ#DIPFAQ-HowistheDIPqualityinformationmappedtoWinCCOAuserandinvalidbitsinWinCCOA?

  std::string tmp;

  tmp = tcds::utils::extractPSXDPGetReply(rawReply_, dpName + ":_online.._userbit1");
  bool const userBit1 = (toolbox::tolower(tmp) == "true");

  tmp = tcds::utils::extractPSXDPGetReply(rawReply_, dpName + ":_online.._userbit2");
  bool const userBit2 = (toolbox::tolower(tmp) == "true");

  DIP_QUAL const qualVal = dipQualFromUserBits(userBit1, userBit2);
  return dipQualToString(qualVal);
}
