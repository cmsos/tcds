#include "tcds/utils/Resolver.h"

#include <sstream>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>

#include "toolbox/string.h"
#include "xcept/Exception.h"

#include "tcds/exception/Exception.h"

tcds::utils::Resolver::Resolver() :
  nodeInfo_(0)
{
}

tcds::utils::Resolver::~Resolver()
{
  reset();
}

std::vector<struct addrinfo>
tcds::utils::Resolver::resolve(std::string const& node,
                               unsigned int const portNumber)
{
  std::stringstream portNumberStr;
  portNumberStr << portNumber;
  return resolve(node, portNumberStr.str());
}

std::vector<struct addrinfo>
tcds::utils::Resolver::resolve(std::string const& node,
                               std::string const& service)
{
  // Cleanup, just in case.
  reset();

  char const* serviceP = 0;
  if (!service.empty())
    {
      serviceP = service.c_str();
    }
  struct addrinfo hints;
  memset(&hints, 0, sizeof(struct addrinfo));
  hints.ai_family = AF_UNSPEC;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_flags = AI_PASSIVE;
  hints.ai_protocol = 0;
  hints.ai_canonname = 0;
  hints.ai_addr = 0;
  hints.ai_next = 0;

  int status = getaddrinfo(node.c_str(), serviceP, &hints, &nodeInfo_);

  if (status != 0)
    {
      std::string const msg =
        toolbox::toString("Failed to resolve '%s:%s': '%s'.",
                          node.c_str(), service.c_str(), gai_strerror(status));
      XCEPT_RAISE(tcds::exception::RuntimeProblem, msg.c_str());
    }

  std::vector<struct addrinfo> res;
  struct addrinfo* p;
  for (p = nodeInfo_; p != 0; p = p->ai_next)
    {
      res.push_back(*p);
    }

  // Cleanup, and return.
  reset();
  return res;
}

void
tcds::utils::Resolver::reset()
{
  if (nodeInfo_)
    {
      freeaddrinfo(nodeInfo_);
      nodeInfo_ = 0;
    }
}
