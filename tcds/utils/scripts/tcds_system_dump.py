#!/usr/bin/env python

###############################################################################
## Script to perform a full dump of all diagnostic information in the TCDS.
###############################################################################

# These could be considered configuration parameters.
# - The output directory where all dumped information should go.
TARGET_DIR = "/tmp"
# - The sender email address to use for the notification mail.
ADDRESS_FROM = "jeroen.hegeman@cern.ch"
# - The recipient email addresses to use for the notification mail.
ADDRESSES_TO = ["jeroen.hegeman@cern.ch"]
# - The address of the CERN anonymous mail server to be used to send
# - the notification mail.
ADDRESS_SMTP = "cernmx.cern.ch"

###############################################################################

import argparse
import commands
import datetime
import errno
import json
import logging
import os
import re
import shutil
import smtplib
import socket
import sys
import tempfile
import urllib2
import urlparse
from xml.dom import minidom

from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate

###############################################################################

# Fixed constants.
URI_NS_XSD = "http://www.w3.org/2001/XMLSchema"
URI_NS_XSI = "http://www.w3.org/2001/XMLSchema-instance"
SOAP_PREFIX_XDAQ = "xdaq"
URI_NS_XDAQ = "urn:xdaq-soap:3.0"

###############################################################################

def build_xdaq_soap_command_message(rc_session_id,
                                    command_name,
                                    rcms_url=None,
                                    parameters=None):

    if parameters is None:
        parameters = {}

    uri_ns_soap_envelope = "http://www.w3.org/2003/05/soap-envelope"
    soap_envelope_prefix = "env"

    # Create the main DOM document.
    doc = minidom.Document()

    # Create and attach the SOAP envelope.
    envelope = doc.createElement("{0:s}:Envelope".format(soap_envelope_prefix))
    envelope.setAttribute("xmlns:{0:s}".format(soap_envelope_prefix),
                          uri_ns_soap_envelope)
    envelope.setAttribute("xmlns:xsd", URI_NS_XSD)
    envelope.setAttribute("xmlns:xsi", URI_NS_XSI)
    doc.appendChild(envelope)

    # Create the header and attach it to the envelope.
    header = doc.createElement("{0:s}:Header".format(soap_envelope_prefix))
    envelope.appendChild(header)

    # Create the SOAP body and attach it to the envelope.
    body = doc.createElement("{0:s}:Body".format(soap_envelope_prefix))
    envelope.appendChild(body)

    # Create and attach the body element.
    cmd_element = doc.createElement("{0:s}:{1:s}".format(SOAP_PREFIX_XDAQ,
                                                         command_name))
    cmd_element.setAttribute("xmlns:{0:s}".format(SOAP_PREFIX_XDAQ),
                             URI_NS_XDAQ)
    body.appendChild(cmd_element)

    # Attach the RunControl session identifier as attribute of the
    # command node.
    cmd_element.setAttribute("{0:s}:actionRequestorId".format(SOAP_PREFIX_XDAQ),
                             rc_session_id)

    # Attach the RCMS state notification listener URL (if given) as
    # attribute of the command node.
    if not rcms_url is None:
        cmd_element.setAttribute("{0:s}:rcmsURL".format(SOAP_PREFIX_XDAQ),
                                 rcms_url)

    # If any parameters were given, attach those to the command node.
    if len(parameters):
        # cmd_element.setAttribute("xsi:type", "soapenc:Struct")
        parameter_names = parameters.keys()
        parameter_names.sort()
        for par_name in parameter_names:
            (par_type, par_val) = parameters[par_name]
            param_element = doc.createElement("{0:s}:{1:s}".format(SOAP_PREFIX_XDAQ,
                                                                   par_name))
            param_element.setAttribute("xsi:type", "xsd:{0:s}".format(par_type))
            value_element = doc.createTextNode(str(par_val))
            param_element.appendChild(value_element)
            cmd_element.appendChild(param_element)

    # End of build_xdaq_soap_command_message().
    return doc

###############################################################################

def send_xdaq_soap_message(host, port, lid, msg, verbose=False):

    if verbose:
        print "Sending SOAP message:"
        print msg.toprettyxml()

    soap_message_txt = msg.toxml()
    headers = {
        "Content-Location" : "urn:xdaq-application:lid={0:d}".format(lid),
        "Content-type" : "application/soap+xml",
        "Content-length" :len(soap_message_txt)
    }
    url = "http://{0:s}:{1:d}".format(host, port)
    response_tmp = send_message(url, soap_message_txt, headers, verbose)

    response = minidom.parseString(response_tmp)
    if verbose:
        print "Received SOAP response:"
        print response.toprettyxml()

    # End of send_xdaq_soap_message().
    return response

###############################################################################

def send_message(url, content=None, headers={}, verbose=False):

    req = urllib2.Request(url, content, headers)
    response = None

    try:
        reply = urllib2.urlopen(req)
        response = reply.read()
    except httplib.BadStatusLine, err:
        msg = "It looks like the application crashed before responding."
        print >> sys.stderr, msg
        sys.exit(1)
    except urllib2.HTTPError, err:
        msg = "Problem sending message, " \
              "the server could not fullfill the request. " \
              "Error code: {0:d}"
        print >> sys.stderr, msg.format(err.code)
        sys.exit(1)
    except urllib2.URLError, err:
        msg =  "Problem sending message, " \
               "failed to reach the server: '{0:s}'."
        print >> sys.stderr, msg.format(str(err.reason))
        sys.exit(1)

    # End of send_message().
    return response

###############################################################################

def extract_xdaq_soap_fault(soap_response):
    # Note: Some of this is a bit of a hack. But we somehow have to
    # recognize both SOAP v1.1 and v1.2.

    error_msg = None
    envelope = find_child_node_by_name(soap_response, "envelope", True)
    body = find_child_node_by_name(envelope, "body", True)
    fault = find_child_node_by_name(body, "fault", True)
    if fault:
        error_msg = None

        #----------
        # The fault string itself.
        #----------
        # This should work for SOAP v1.1.
        reason = find_child_node_by_name(fault, "faultstring", True)
        if reason:
            error_msg = reason.firstChild.nodeValue
        else:
            # This should work for SOAP v1.2.
            reason = find_child_node_by_name(fault, "reason", True)
            if reason:
                error_msg = reason.firstChild.firstChild.nodeValue

        #----------
        # The fault detail, if it exists.
        #----------
        # This should work for SOAP v1.1.
        detail = find_child_node_by_name(fault, "detail", True)
        if detail and detail.firstChild.nodeValue:
            error_msg = "{0:s} {1:s}".format(error_msg, detail.firstChild.nodeValue)
        else:
            # This should work for SOAP v1.2.
            detail = find_child_node_by_name(fault, "Detail", True)
            if detail and detail.firstChild.nodeValue:
                error_msg = "{0:s} {1:s}".format(error_msg, detail.firstChild.firstChild.nodeValue)

    # End of extract_xdaq_soap_fault().
    return error_msg

###############################################################################

def extract_reg_dump_from_soap_reply(soap_response):
    # Note: Some of this is a bit of a hack. But we somehow have to
    # recognize both SOAP v1.1 and v1.2.

    envelope = find_child_node_by_name(soap_response, "envelope", True)
    body = find_child_node_by_name(envelope, "body", True)
    response_element = find_child_node_by_name(body, "DumpHardwareStateResponse", True)
    par_element = find_child_node_by_name(response_element, "registerDump", True)
    par_val = par_element.firstChild.nodeValue

    # End of extract_reg_dump_from_soap_reply().
    return par_val

###############################################################################

def find_child_node_by_name(parent_node, child_node_name, case_insensitive=False):
    res = None
    if parent_node:
        child_node_name_tmp = child_node_name
        if case_insensitive:
            child_node_name_tmp = child_node_name_tmp.lower()
        for node in parent_node.childNodes:
            tmp = node.nodeName
            index = tmp.find(":")
            if index > -1:
                tmp = tmp[index + 1:]
            if case_insensitive:
                tmp = tmp.lower()
            if tmp == child_node_name_tmp:
                res = node
                break
    # End of find_child_node_by_name().
    return res

###############################################################################

if __name__ == "__main__":

    #----------

    # Generate timestamp.
    timestamp_begin = datetime.datetime.utcnow()

    #----------

    parser = argparse.ArgumentParser()
    parser.add_argument("--tcdscentral-host",
                        help="The host of the TCDSCentral instance of the system to dump.",
                        type=str,
                        default="localhost")
    parser.add_argument("--tcdscentral-port",
                        help="The port number of the TCDSCentral instance of the system to dump.",
                        type=int,
                        default=2000)
    parser.add_argument("--dump-reason",
                        help="The reason to request the system dump.",
                        type=str,
                        default="")
    parser.add_argument("-v",
                        "--verbose",
                        help="Increase output verbosity.",
                        action="store_true")
    args = parser.parse_args()

    #----------

    # Setup logging to both file and console.
    (tmpfile_fd, tmpfile_name) = tempfile.mkstemp()
    logger = logging.getLogger("tcds_system_dump")
    logger.setLevel(logging.INFO)
    fh = logging.FileHandler(tmpfile_name)
    ch = logging.StreamHandler()
    logger.addHandler(fh)
    logger.addHandler(ch)

    #----------

    # Figure out the XDAQ zone we're trying to dump.

    logger.info("Determining XDAQ zone.")

    tmp = "http://{0:s}:{1:d}/urn:xdaq-application:service=hyperdaq/processInformation"
    proc_info_url = tmp.format(args.tcdscentral_host, args.tcdscentral_port)

    html = None
    try:
        response = urllib2.urlopen(proc_info_url)
        html = response.read()
    except Exception, err:
        logger.error("!!! Failed to connect to URL '{0:s}': '{1:s}'. !!!".format(proc_info_url, err))
        sys.exit(1)

    regex = re.compile("<td>XDAQ_ZONE</td><td>(.*?)</td>")
    match = regex.search(html)
    if not match:
        logger.error("!!! Failed to extract XDAQ zone name from URL '{0:s}'. !!!".format(proc_info_url))
        sys.exit(1)
    xdaq_zone = match.group(1)

    logger.info("  XDAQ zone is '{0:s}'.".format(xdaq_zone))

    #----------

    # Get the lists (yes, multiple!) from the TCDSCentral application.

    logger.info("Obtaining TCDS control application lists.")

    tmp = "http://{0:s}:2000/urn:xdaq-application:service=tcds-central"
    tcdscentral_url = tmp.format(args.tcdscentral_host)

    raw_json = None
    try:
        response = urllib2.urlopen("{0:s}/update".format(tcdscentral_url))
        raw_json = response.read()
    except Exception, err:
        logger.error("!!! Failed to connect to URL '{0:s}': '{1:s}'. !!!".format(tcdscentral_url, err))
        sys.exit(1)

    tcdscentral_data = json.loads(raw_json)

    # Filter out all keys that sound like itemset-appsinfo-XXX.
    regex = re.compile("^itemset-appsinfo-.*$")
    keys = tcdscentral_data.keys()
    app_list_keys = filter(regex.match, keys)

    apps = {}
    for app_list_key in app_list_keys:
        raw = tcdscentral_data[app_list_key]
        vals = raw["Status of applications"]
        for val in vals:
            service = val["service"]
            url = val["url"]
            apps[service] = url

    logger.info("  Found {0:d} applications.".format(len(apps)))

    services = apps.keys()
    services.sort()

    #----------

    # Create output directory etc.

    logger.info("Creating output directory.")

    out_dir_name_base = "tcds_system_dump_{0:s}_{1:s}".format(xdaq_zone, timestamp_begin.strftime("%Y%m%dT%H%M%S"))
    out_dir_name = os.path.join(TARGET_DIR, out_dir_name_base)

    try:
        os.mkdir(out_dir_name)
    except Exception, err:
        msg = "!!! Failed to create output directory '{0:s}': '{1:s}'. !!!"
        logger.error(msg.format(out_dir_name, err))
        sys.exit(1)

    logger.info("  Output directory: '{0:s}'.".format(out_dir_name))

    #----------

    # Collect and store all the control applications' JSON data.

    logger.info("Collecting and storing JSON data from TCDS control applications.")

    dir_name = os.path.join(out_dir_name, "app_json_data")
    try:
        os.mkdir(dir_name)
    except Exception, err:
        msg = "Failed to create output directory '{0:s}': '{1:s}'."
        logger.error(msg.format(dir_name, err))

    for service in services:
        if args.verbose:
            logger.info("  {0:s}".format(service))
        url = apps[service]
        try:
            local_url = "{0:s}/update".format(url)
            response = urllib2.urlopen(local_url)
            json_data = response.read()
        except Exception, err:
            logger.error("Failed to connect to URL '{0:s}': '{1:s}'.".format(local_url, err))

        out_file_name = os.path.join(dir_name, "{0:s}.json".format(service))
        try:
            out_file = open(out_file_name, "w")
            out_file.write(json_data)
            out_file.close()
        except Exception, err:
            msg = "Failed to store output file: '{0:s}'."
            logger.error(msg.format(err))

    #----------

    # Collect and store all the control applications' log files.

    logger.info("Collecting and storing log files from executives hosting TCDS control applications.")

    dir_name = os.path.join(out_dir_name, "app_logs")
    try:
        os.mkdir(dir_name)
    except Exception, err:
        msg = "Failed to create output directory '{0:s}': '{1:s}'."
        logger.error(msg.format(dir_name, err))

    logs_stored = {}
    for service in services:
        if args.verbose:
            logger.info("  {0:s}".format(service))
        url = apps[service]
        url_split = urlparse.urlsplit(url)

        tmp = "http://{0:s}/urn:xdaq-application:service=hyperdaq/processInformation"
        proc_info_url = tmp.format(url_split.netloc)
        host_name = url_split.hostname

        dir_name_local = os.path.join(dir_name, host_name)
        try:
            os.mkdir(dir_name_local)
        except Exception, err:
            if err.args != (17, 'File exists'):
                msg = "Failed to create output directory '{0:s}': '{1:s}'."
                logger.error(msg.format(dir_name_local, err))

        html = None
        try:
            response = urllib2.urlopen(proc_info_url)
            html = response.read()
        except Exception, err:
            logger.error("!!! Failed to connect to URL '{0:s}': '{1:s}'. !!!".format(proc_info_url, err))
            sys.exit(1)

        regex = re.compile("<td>XDAQ_LOG</td><td>(.*?)</td>")
        match = regex.search(html)
        if not match:
            logger.error("!!! Failed to extract XDAQ log file name from URL '{0:s}'. !!!".format(proc_info_url))
            sys.exit(1)
        log_file_name = match.group(1)

        try:
            if log_file_name in logs_stored[host_name]:
                continue
        except KeyError:
            pass

        path = "urn:xdaq-application:service=hyperdaq/getLogFile"
        query = "url=file://{0:s}".format(log_file_name)
        fragment = ""
        url_split_mod = urlparse.SplitResult(url_split.scheme,
                                             url_split.netloc,
                                             path,
                                             query,
                                             fragment)
        local_url = urlparse.urlunsplit(url_split_mod)
        response = None
        try:
            response = urllib2.urlopen(local_url)
            log_data = response.read()
        except Exception, err:
            logger.error("Failed to connect to URL '{0:s}': '{1:s}'.".format(local_url, err))

        log_file_base_name = os.path.basename(log_file_name)
        out_file_name = os.path.join(dir_name_local, log_file_base_name)
        try:
            out_file = open(out_file_name, "w")
            out_file.write(log_data)
            out_file.close()
        except Exception, err:
            msg = "Failed to store output file: '{0:s}'."
            logger.error(msg.format(err))

        try:
            logs_stored[host_name].append(log_file_name)
        except KeyError:
            logs_stored[host_name] = [log_file_name]

    #----------

    # Now cycle through all applications, read back the current
    # hardware configuration and state, and store the results.

    logger.info("Collecting and storing hardware register dumps.")

    dir_name = os.path.join(out_dir_name, "hw_reg_dumps")
    try:
        os.mkdir(dir_name)
    except Exception, err:
        msg = "Failed to create output directory '{0:s}': '{1:s}'."
        logger.error(msg.format(dir_name, err))

    for service in services:
        if args.verbose:
            logger.info("  {0:s}".format(service))
        url = apps[service]
        url_split = urlparse.urlsplit(url)
        host_name = url_split.hostname
        port_number = url_split.port
        lid_number = int(url_split.path.split("lid=")[1])
        command_name = "DumpHardwareState"
        parameters = []
        soap_msg = build_xdaq_soap_command_message("dummy",
                                                   command_name,
                                                   None,
                                                   parameters)
        soap_reply = send_xdaq_soap_message(host_name,
                                            port_number,
                                            lid_number,
                                            soap_msg,
                                            verbose=False)
        soap_fault = extract_xdaq_soap_fault(soap_reply)
        if soap_fault:
            # It could of course be that the application is 'Halted'
            # and thus not connected to the hardware.
            if ((soap_fault.find("the XDAQ control application is not (yet) connected to its hardware") > -1)):
                msg = "  Application '{0:s}' is not connected to its hardware.".format(service)
                logger.info(msg)
            elif ((soap_fault.find("No callback method found for incoming request [urn:xdaq-soap:3.0:DumpHardwareState]") > -1)):
                msg = "  Application '{0:s}' has no hardware, or does not support dumping its hardware state.".format(service)
                logger.info(msg)
            else:
                msg = "!!! SOAP reply indicates a problem: '{0:s}'. !!!".format(soap_fault)
                logger.error(msg)
        else:
            # Store the returned register dump to file.
            reg_dump = extract_reg_dump_from_soap_reply(soap_reply)
            reg_dump_lines = reg_dump.split("\n")
            reg_dump_lines = [i for i in reg_dump_lines if len(i)]
            reg_dump_lines.sort()
            dump_file_name = os.path.join(dir_name, "{0:s}.txt".format(service))
            try:
                dump_file = open(dump_file_name, "w")
                dump_file.writelines(os.linesep.join(reg_dump_lines))
                dump_file.close()
            except Exception, err:
                logger.error("!!! Failed to write register dump to file '{0:s}': '{1:s}'. !!!".format(dump_file_name, err))

    #----------

    # Don't forget to move the log file in there as well.

    logger.info("Storing dump log file.")

    os.close(tmpfile_fd)
    os.chmod(tmpfile_name, 0o644)
    shutil.move(tmpfile_name, os.path.join(out_dir_name, "tcds_system_dump.log"))

    #----------

    # Tar up the results and see if we can get them small enough to
    # email them.

    logger.info("Tarring up everything.")

    # Create a temporary directory for the tar-ball.
    tmp_dir = tempfile.mkdtemp()

    try:
        # Create the tar-ball.
        tar_ball_name_base = "{0:s}.tar.xz".format(out_dir_name_base)
        tar_ball_name = os.path.join(tmp_dir, tar_ball_name_base)
        cmd = "tar -cJf {0:s} {1:s}".format(tar_ball_name, out_dir_name)
        (status, output) = commands.getstatusoutput(cmd)
        if (status != 0):
            msg = "Failed to create tar-ball '{0:s}': '{1:s}'."
            logger.error(msg.format(tar_ball_name, output))
        else:
            # Move tar-ball to output directory.
            shutil.move(tar_ball_name, out_dir_name)
    finally:
        # Remove the temporary directory.
        try:
            shutil.rmtree(tmp_dir)
        except OSError as exc:
            if exc.errno != errno.ENOENT:
                raise

    #----------

    # Let's keep track of how long this takes.
    timestamp_end = datetime.datetime.utcnow()

    #----------

    # Write some basic info into a README file.

    logger.info("Writing README file.")

    out_file_name = os.path.join(out_dir_name, "README")
    readme_lines = []
    readme_lines.append("TCDS system dump for XDAQ zone '{0:s}'.".format(xdaq_zone))
    readme_lines.append("Timestamp of start of dump: {0:s}.".format(timestamp_begin.isoformat()))
    readme_lines.append("Timestamp of end of dump: {0:s}.".format(timestamp_end.isoformat()))
    if args.dump_reason:
        readme_lines.append("Reason for dump: {0:s}.".format(args.dump_reason))
    else:
        readme_lines.append("No reason given for dump.")
    readme_lines.append("Original dump location: {0:s}:{1:s}.".format(socket.gethostname(), os.path.abspath(out_dir_name)))
    readme_lines.append("")
    try:
        out_file = open(out_file_name, "w")
        out_file.writelines(os.linesep.join(readme_lines))
        out_file.close()
    except Exception, err:
        msg = "Failed to create README file: '{0:s}'."
        logger.error(msg.format(err))

    #----------

    # Send a notification email.

    logger.info("Sending notification email.")

    subject = "TCDS system dump notification"
    msg = MIMEMultipart("mixed")
    msg["From"] = ADDRESS_FROM
    msg["To"] = COMMASPACE.join(ADDRESSES_TO)
    msg["Subject"] = subject

    msg_text = MIMEText(os.linesep.join(readme_lines), "text")
    tar_ball_data = open(os.path.join(out_dir_name, tar_ball_name_base), "rb").read()
    msg_attachment = MIMEApplication(tar_ball_data, "x-xz")
    msg_attachment.add_header("Content-ID", "<pdf1>")
    msg_attachment.add_header("Content-Disposition", "attachment", filename=os.path.basename(tar_ball_name))
    msg_attachment.add_header("Content-Disposition", "inline", filename=os.path.basename(tar_ball_name))

    msg.attach(msg_text)
    msg.attach(msg_attachment)

    s = smtplib.SMTP(ADDRESS_SMTP)
    s.sendmail(ADDRESS_FROM, ADDRESSES_TO, msg.as_string())
    s.quit()

    #----------

    logger.info("Done")

    # All done.
    sys.exit(0)

###############################################################################
