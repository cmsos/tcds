#ifndef _tcds_utils_Resolver_h_
#define _tcds_utils_Resolver_h_

#include <netdb.h>
#include <string>
#include <vector>

namespace tcds {
  namespace utils {

    class Resolver
    {

    public:
      Resolver();
      ~Resolver();

      std::vector<struct addrinfo> resolve(std::string const& node,
                                           unsigned int const portNumber);
      std::vector<struct addrinfo> resolve(std::string const& node,
                                           std::string const& service="");

    private:
      void reset();

      struct addrinfo* nodeInfo_;

    };

  } // namespace utils
} // namespace tcds

#endif // _tcds_utils_Utils_h_
