#include <stdint.h>
#include <string>

#include "tcds/utils/Definitions.h"
#include "tcds/utils/SOAPUtils.h"
#include "tcds/utils/Utils.h"

template <class T>
tcds::utils::SOAPCmdSendBCommand<T>::SOAPCmdSendBCommand(T& controller) :
SOAPCmdBase<T>(controller, "SendBCommand")
{
}

template <class T>
xoap::MessageReference
tcds::utils::SOAPCmdSendBCommand<T>::executeImpl(xoap::MessageReference const& msg)
{
  // Extract the command parameters and check their values.

  // - The B-command data.
  uint32_t const tmpData =
    tcds::utils::soap::extractSOAPCommandParameterUnsignedInteger(msg, "bcommandData");
  // Check the range.
  tcds::utils::checkBCommandData(tmpData);
  tcds::definitions::bcommand_data_t bcommandData = tmpData;

  // - The B-command type.
  std::string const tmpType =
    tcds::utils::soap::extractSOAPCommandParameterString(msg, "bcommandType");
  tcds::definitions::BCOMMAND_TYPE bcommandType =
    tcds::utils::bcommandTypeFromString(tmpType);

  tcds::definitions::bcommand_address_t address = 0x0;
  tcds::definitions::bcommand_address_t subAddress = 0x0;
  tcds::definitions::BCOMMAND_ADDRESS_TYPE addressType =
    tcds::definitions::BCOMMAND_ADDRESS_TYPE_INTERNAL;
  uint32_t tmpAddress;
  uint32_t tmpSubAddress;
  std::string tmpAddressType;
  if (bcommandType == tcds::definitions::BCOMMAND_TYPE_ADDRESSED)
    {
      // In this case we also need the address, sub-address, etc.

      // - The TTCrx address.
      tmpAddress =
        tcds::utils::soap::extractSOAPCommandParameterUnsignedInteger(msg, "bcommandAddress");
      // Check the range.
      tcds::utils::checkBCommandAddress(tmpAddress);
      address = tmpAddress;

      // - The sub-address.
      tmpSubAddress =
        tcds::utils::soap::extractSOAPCommandParameterUnsignedInteger(msg, "bcommandSubAddress");
      // Check the range.
      tcds::utils::checkBCommandSubAddress(tmpSubAddress);
      subAddress = tmpSubAddress;

      // - The address type.
      tmpAddressType =
        tcds::utils::soap::extractSOAPCommandParameterString(msg, "bcommandAddressType");
      addressType = tcds::utils::bcommandAddressTypeFromString(tmpAddressType);
    }

  // Keep track of history.
  std::string histMsg = "unknown";
  if (bcommandType == tcds::definitions::BCOMMAND_TYPE_BROADCAST)
    {
      histMsg = toolbox::toString("%s: "
                                  "bcommandType = '%s', "
                                  "bcommandData = 0x%02x",
                                  this->commandName().c_str(),
                                  tmpType.c_str(),
                                  bcommandData);
    }
  else
    {
      histMsg = toolbox::toString("%s: "
                                  "bcommandType = '%s', "
                                  "bcommandAddress = 0x%04x, "
                                  "bcommandSubAddress = 0x%02x, "
                                  "bcommandAddressType = '%s', "
                                  "bcommandData = 0x%02x",
                                  this->commandName().c_str(),
                                  tmpType.c_str(),
                                  address,
                                  subAddress,
                                  tmpAddressType.c_str(),
                                  bcommandData);
    }
  this->controller_.appStateInfoSpace_.addHistoryItem(histMsg);

  // Do what we were asked to do.
  this->controller_.getHw().sendBCommand(bcommandType, bcommandData,
                                         address, subAddress, addressType);

  // Send reply.
  xoap::MessageReference reply = tcds::utils::soap::makeCommandSOAPReply(msg);
  return reply;
}
