#ifndef _tcds_utils_InfoSpaceItem_h_
#define _tcds_utils_InfoSpaceItem_h_

#include <string>

namespace tcds {
  namespace utils {

    class InfoSpaceItem
    {

      friend bool operator==(InfoSpaceItem const& item, std::string const& name);
      friend bool operator==(std::string const& name, InfoSpaceItem const& item);

    public:
      enum ItemType {STRING, TIMEVAL, UINT32, UINT64, BOOL, DOUBLE, FLOAT, DOUBLEVEC, STRINGVEC, TIMEVALVEC, UINT32VEC};
      enum UpdateType {HW32, HW32BLOCK, HW64, HWBOOL, PROCESS, TRACKER, CONFIGURATION, NOUPDATE};

      InfoSpaceItem(std::string const& name,
                    ItemType const type,
                    std::string const& format,
                    UpdateType const updateType,
                    bool const isValid);

      // BUG BUG BUG
      // This is still a bit ugly. Could use some work.
      InfoSpaceItem(std::string const& name,
                    std::string const& hwName,
                    ItemType const type,
                    std::string const& format,
                    UpdateType const updateType,
                    bool const isValid);
      // BUG BUG BUG end

      std::string name() const;
      std::string hwName() const;
      ItemType type() const;
      std::string format() const;
      UpdateType updateType() const;
      bool isValid() const;
      void setValid();
      void setInvalid();

    private:
      std::string name_;
      std::string hwName_;
      ItemType type_;
      std::string format_;
      UpdateType updateType_;
      bool isValid_;

    };

    bool operator==(InfoSpaceItem const& item, std::string const& name);
    bool operator==(std::string const& name, InfoSpaceItem const& item);

  } // namespace utils
} // namespace tcds

#endif // _tcds_utils_InfoSpaceItem_h_
