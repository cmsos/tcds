#ifndef _tcds_utils_FSM_h_
#define _tcds_utils_FSM_h_

#include <map>
#include <string>

#include "log4cplus/logger.h"

#include "toolbox/Event.h"
#include "toolbox/lang/Class.h"
#include "xdaq2rc/RcmsStateNotifier.h"
#include "xoap/MessageReference.h"

namespace toolbox {
  namespace fsm {
    class AsynchronousFiniteStateMachine;
    class FiniteStateMachine;
  }
}

namespace xcept {
  class Exception;
}

namespace tcds {
  namespace utils {

    class XDAQAppWithFSMBase;

    /**
     * TCDS software state machine implementation. The state machines
     * used for the partition manager controllers differ slightly from
     * the state machines used elsewhere. This class implements the
     * most extensive version of the two. In the the
     * non-partition-manager controllers the same FSM class is
     * instantiated but not all state transitions are exposed to the
     * outside world (i.e., are not bound to anything).
     */
    class FSM : public toolbox::lang::Class
    {

    public:
      FSM(XDAQAppWithFSMBase* const xdaqAppP);
      virtual ~FSM();

      // SOAP-based access to state transitions.
      xoap::MessageReference changeState(xoap::MessageReference msg);

      // Direct access to state transitions.
      void coldReset();
      void configure();
      void reconfigure();
      void enable();
      void pause();
      void resume();
      void stop();
      void halt();
      void fail();

      std::string getCurrentStateName() const;

      void gotoFailed(std::string const& reason="No further information available");
      void gotoFailed(xcept::Exception& err);

    protected:
      void stateChangedWithNotification(toolbox::fsm::FiniteStateMachine& fsm);
      void stateChangedToFailedWithNotification(toolbox::fsm::FiniteStateMachine& fsm);

    private:
      toolbox::fsm::AsynchronousFiniteStateMachine* fsmP_;
      XDAQAppWithFSMBase* xdaqAppP_;
      log4cplus::Logger logger_;
      xdaq2rc::RcmsStateNotifier rcmsNotifier_;

      std::map<std::string, std::string> automaticTransitions_;

      void invalidStateTransitionAction(toolbox::Event::Reference event);
      void notifyRCMS(std::string const& stateName, std::string const& msg);
      void reset(std::string const& msg);

      void logStateChangeDetails() const;

    };

  } // namespace utils
} // namespace tcds

#endif // _tcds_utils_FSM_h_
