#ifndef _tcds_utils_LockGuard_h_
#define _tcds_utils_LockGuard_h_

#include <iostream>

namespace tcds {
  namespace utils {

    template <class L>
    class LockGuard
    {

    public:
      LockGuard(L& lock);
      ~LockGuard();

    private:
      L& lock_;

      // Prevent copying.
      LockGuard(LockGuard const&);
      LockGuard& operator=(LockGuard const&);

    };

  } // namespace utils
} // namespace tcds

template <class L>
tcds::utils::LockGuard<L>::LockGuard(L& lock) :
lock_(lock)
{
  lock_.lock();
}

template <class L>
tcds::utils::LockGuard<L>::~LockGuard()
{
  lock_.unlock();
}

#endif // _tcds_utils_LockGuard_h_
