#include "tcds/utils/SOAPUtils.h"

template <class T>
tcds::utils::SOAPCmdDisableTTCSpy<T>::SOAPCmdDisableTTCSpy(T& controller) :
  SOAPCmdBase<T>(controller, "DisableTTCSpy", false)
{
}

template <class T>
xoap::MessageReference
tcds::utils::SOAPCmdDisableTTCSpy<T>::executeImpl(xoap::MessageReference const& msg)
{
  this->controller_.getHw().disableTTCSpy();
  xoap::MessageReference reply = tcds::utils::soap::makeCommandSOAPReply(msg);
  return reply;
}
