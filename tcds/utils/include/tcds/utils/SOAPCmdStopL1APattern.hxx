#include "tcds/utils/SOAPUtils.h"

template <class T>
tcds::utils::SOAPCmdStopL1APattern<T>::SOAPCmdStopL1APattern(T& controller) :
  SOAPCmdBase<T>(controller, "StopL1APattern", false)
{
}

template <class T>
xoap::MessageReference
tcds::utils::SOAPCmdStopL1APattern<T>::executeImpl(xoap::MessageReference const& msg)
{
  this->controller_.getHw().stopL1APattern();
  xoap::MessageReference reply = tcds::utils::soap::makeCommandSOAPReply(msg);
  return reply;
}
