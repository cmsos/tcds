#ifndef _tcds_utils_SOAPCmdDisableCyclicGenerator_h_
#define _tcds_utils_SOAPCmdDisableCyclicGenerator_h_

#include "xoap/MessageReference.h"

#include "tcds/utils/SOAPCmdBase.h"

namespace tcds {
  namespace utils {

    class XDAQAppBase;

    template <class T>
      class SOAPCmdDisableCyclicGenerator : public SOAPCmdBase<T>
      {

      public:
        SOAPCmdDisableCyclicGenerator(T& controller);

        /**
         * Disables the specified cyclic generator. Bound to the SOAP
         * command DisableCyclicGenerator.
         * - SOAP command parameters:
         *   -  xsd:unsignedInt `genNumber': the cyclic generator number (e.g., '1').
         * - SOAP command return value: none.
         */
        virtual xoap::MessageReference executeImpl(xoap::MessageReference const& msg);

      };

  } // namespace utils
} // namespace tcds

#include "tcds/utils/SOAPCmdDisableCyclicGenerator.hxx"

#endif // _tcds_utils_SOAPCmdDisableCyclicGenerator_h_
