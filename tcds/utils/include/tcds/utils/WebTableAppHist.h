#ifndef _tcds_utils_WebTableAppHist_h
#define _tcds_utils_WebTableAppHist_h

#include <cstddef>
#include <string>

#include "tcds/utils/WebObject.h"

namespace tcds {
  namespace utils {
    class Monitor;
  }
}

namespace tcds {
  namespace utils {

    class WebTableAppHist : public tcds::utils::WebObject
    {

    public:
      WebTableAppHist(std::string const& name,
                      std::string const& description,
                      tcds::utils::Monitor const& monitor,
                      std::string const& itemSetName,
                      std::string const& tabName,
                      size_t const colSpan);

      std::string getHTMLString() const;

    };

  } // namespace utils
} // namespace tcds

#endif // _tcds_utils_WebTableAppHist_h
