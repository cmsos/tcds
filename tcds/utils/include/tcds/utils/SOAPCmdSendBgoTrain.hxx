#include <stdint.h>
#include <string>
#include <vector>

#include "toolbox/string.h"
#include "xcept/Exception.h"
#include "xoap/SOAPName.h"
#include "xoap/SOAPElement.h"

#include "tcds/exception/Exception.h"
#include "tcds/utils/Definitions.h"
#include "tcds/utils/SOAPUtils.h"
#include "tcds/utils/Utils.h"

template <class T>
tcds::utils::SOAPCmdSendBgoTrain<T>::SOAPCmdSendBgoTrain(T& controller) :
SOAPCmdBase<T>(controller, "SendBgoTrain")
{
}

template <class T>
xoap::MessageReference
tcds::utils::SOAPCmdSendBgoTrain<T>::executeImpl(xoap::MessageReference const& msg)
{
  // Extract the command parameter.
  // NOTE: In this case a bit tricky. It accepts either 'bgoTrainName' or
  // 'bgoTrainNumber'.
  xoap::SOAPName soapNameName("bgoTrainName", "xdaq", "");
  xoap::SOAPName soapNameNumber("bgoTrainNumber", "xdaq", "");
  xoap::SOAPElement commandNode = tcds::utils::soap::extractBodyNode(msg);
  std::vector<xoap::SOAPElement> childrenName =
    commandNode.getChildElements(soapNameName);
  std::vector<xoap::SOAPElement> childrenNumber =
    commandNode.getChildElements(soapNameNumber);

  // We should have found one of the two...
  if ((childrenName.size() == 0) && (childrenNumber.size() == 0))
    {
      XCEPT_RAISE(tcds::exception::SOAPFormatProblem,
                  toolbox::toString("Expected either 'bgoTrainName' or 'bgoTrainNumber' "
                                    "in SOAP command '%s'.",
                                    this->commandName().c_str()));
    }

  // And we should only have found one of the two...
  if ((childrenName.size() != 0) && (childrenNumber.size() != 0))
    {
      XCEPT_RAISE(tcds::exception::SOAPFormatProblem,
                  toolbox::toString("Expected either 'bgoTrainName' or 'bgoTrainNumber' "
                                    "in SOAP command '%s' but found both.",
                                    this->commandName().c_str()));
    }

  // Whatever we found: translate it into a valid B-go train number.
  std::string bgoTrainName;
  tcds::definitions::SEQUENCE_NUM bgoTrainNumber = tcds::definitions::SEQUENCE_START;
  if (childrenName.size() != 0)
    {
      bgoTrainName = tcds::utils::soap::extractSOAPCommandParameterString(msg, "bgoTrainName");
      bgoTrainNumber = tcds::utils::sequenceNameToNumber(bgoTrainName);
    }
  else if (childrenNumber.size() != 0)
    {
      uint32_t const tmp =
        tcds::utils::soap::extractSOAPCommandParameterUnsignedInteger(msg, "bgoTrainNumber");
      bgoTrainNumber = tcds::definitions::SEQUENCE_NUM(tmp);
      // Check the range.
      tcds::utils::checkBgoTrainNumber(bgoTrainNumber);
    }

  // Keep track of history.
  std::string histMsg = "unknown";
  if (childrenName.size() != 0)
    {
      histMsg = toolbox::toString("%s: bgoTrainName = '%s'",
                                  this->commandName().c_str(),
                                  bgoTrainName.c_str());
    }
  else if (childrenNumber.size() != 0)
    {
      histMsg = toolbox::toString("%s: bgoTrainNumber = %d",
                                  this->commandName().c_str(),
                                  bgoTrainNumber);
      std::string const bgoTrainName = sequenceNumberToName(bgoTrainNumber);
      if (!toolbox::startsWith(bgoTrainName, "Sequence"))
        {
          histMsg += " (" + bgoTrainName + ")";
        }
    }
  this->controller_.appStateInfoSpace_.addHistoryItem(histMsg);

  // Do what we were asked to do.
  this->controller_.getHw().sendBgoTrain(bgoTrainNumber);

  // Send reply.
  xoap::MessageReference reply = tcds::utils::soap::makeCommandSOAPReply(msg);
  return reply;
}
