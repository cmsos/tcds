#include "tcds/utils/SOAPUtils.h"

template <class T>
tcds::utils::SOAPCmdResetTTCSpyLog<T>::SOAPCmdResetTTCSpyLog(T& controller) :
  SOAPCmdBase<T>(controller, "ResetTTCSpyLog", false)
{
}

template <class T>
xoap::MessageReference
tcds::utils::SOAPCmdResetTTCSpyLog<T>::executeImpl(xoap::MessageReference const& msg)
{
  this->controller_.getHw().resetTTCSpy();
  xoap::MessageReference reply = tcds::utils::soap::makeCommandSOAPReply(msg);
  return reply;
}
