#ifndef _tcds_utils_DIPDP_h_
#define _tcds_utils_DIPDP_h_

#include <string>
#include <vector>

#include "tcds/utils/DIPUtils.h"

namespace tcds {
  namespace utils {

    class DIPDP
    {
    public:
      DIPDP(std::string const& dpName,
            std::string const& dpValue,
            tcds::utils::DIP_QUAL const dpQual);

      std::string dpName() const;
      std::string dpValue() const;
      tcds::utils::DIP_QUAL dpQual() const;

    private:
      std::string dpName_;
      std::string dpValue_;
      tcds::utils::DIP_QUAL dpQual_;
    };

  } // namespace utils
} // namespace tcds

#endif // _tcds_utils_DIPDP_h_
