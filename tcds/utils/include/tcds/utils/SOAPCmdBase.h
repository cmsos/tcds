#ifndef _tcds_utils_SOAPCmdBase_h_
#define _tcds_utils_SOAPCmdBase_h_

#include <string>

#include "toolbox/lang/Class.h"
#include "xoap/MessageReference.h"

namespace tcds {
  namespace utils {

    template <class T>
    class SOAPCmdBase : public toolbox::lang::Class
    {

    public:
      virtual ~SOAPCmdBase();

      std::string commandName() const;
      xoap::MessageReference execute(xoap::MessageReference msg);

      virtual xoap::MessageReference executeImpl(xoap::MessageReference const& msg) = 0;

    protected:
      SOAPCmdBase(T& controller,
                  std::string const& commandName,
                  bool const requiresHwLease=true);

      T& controller_;
      std::string const commandName_;
      bool const requiresHwLease_;

    };

  } // namespace utils
} // namespace tcds

#include "tcds/utils/SOAPCmdBase.hxx"

#endif // _tcds_utils_SOAPCmdBase_h_
