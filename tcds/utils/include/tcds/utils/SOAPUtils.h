#ifndef _tcds_utils_SOAPUtils_h
#define _tcds_utils_SOAPUtils_h

#include <stdint.h>
#include <string>
#include <vector>

#include "xoap/MessageReference.h"
#include "xoap/SOAPElement.h"

#include "tcds/utils/DIPUtils.h"
#include "tcds/utils/OwnerId.h"

namespace xdaq {
  class Application;
}

namespace xoap {
  class SOAPName;
}

namespace tcds {
  namespace utils {

    namespace soap {

      // NOTE: The 'xdaq' namespace is determined by the fact that
      // this is the namespace used by RunControl.

      // Enum definitions for SOAP fault-code types. Defined in SOAP v1.2-speak:
      // - SOAPFaultSender for v1.1 'Client' and v1.2 'Sender',
      // - SOAPFaultReceiver for v1.1 'Server' and v1.2 'Receiver'.
      enum SOAPFaultCodeType {SOAPFaultCodeSender, SOAPFaultCodeReceiver};

      xoap::MessageReference makeParameterGetSOAPCmd(std::string const& className,
                                                     std::string const& parName,
                                                     std::string const& parType);

      // The following three methods prepare SOAP replies to SOAP
      // messages. The protocol version of the reply is based on the
      // protocol version of the original message.
      xoap::MessageReference makeSOAPFaultReply(xdaq::Application* const xdaqApp,
                                                xoap::MessageReference const& msg,
                                                SOAPFaultCodeType const faultCode,
                                                std::string const& faultString,
                                                std::string const& faultDetail="");
      xoap::MessageReference makeCommandSOAPReply(xoap::MessageReference const& msg,
                                                  std::string const& fsmState="");
      xoap::MessageReference makeCommandSOAPReply(xoap::MessageReference const& msg,
                                                  std::string const& replyMsg,
                                                  std::string const& fsmState="");

      // The following four methods prepare SOAP replies to SOAP
      // messages. These are used internally by the methods above.
      xoap::MessageReference makeSOAPFaultReply(std::string const& soapProtocolVersion,
                                                SOAPFaultCodeType const faultCode,
                                                std::string const& faultString,
                                                std::string const& faultDetail);
      xoap::MessageReference makeCommandSOAPReply(std::string const& soapProtocolVersion,
                                                  std::string const& command,
                                                  std::string const& fsmState);
      xoap::MessageReference makeCommandSOAPReply(std::string const& soapProtocolVersion,
                                                  std::string const& command,
                                                  std::string const& replyMsg,
                                                  std::string const& fsmState);

      std::string extractSOAPCommandName(xoap::MessageReference const& msg);
      std::string extractSOAPCommandRCMSURL(xoap::MessageReference const& msg);
      uint32_t extractSOAPCommandRCMSSessionId(xoap::MessageReference const& msg);
      std::string extractSOAPCommandRequestorId(xoap::MessageReference const& msg);
      tcds::utils::OwnerId extractSOAPCommandOwnerId(xoap::MessageReference const& msg);

      bool hasFault(xoap::MessageReference const& msg);
      bool hasFaultDetail(xoap::MessageReference const& msg);
      std::string extractFaultString(xoap::MessageReference const& msg);
      std::string extractFaultDetail(xoap::MessageReference const& msg);

      bool hasSOAPCommandAttribute(xoap::MessageReference const& msg,
                                   std::string const& attributeName);
      bool hasSOAPCommandParameter(xoap::MessageReference const& msg,
                                   std::string const& parameterName);
      bool extractSOAPCommandParameterBool(xoap::MessageReference const& msg,
                                           std::string const& parameterName);
      std::string extractSOAPCommandParameterString(xoap::MessageReference const& msg,
                                                    std::string const& parameterName);
      uint32_t extractSOAPCommandParameterUnsignedInteger(xoap::MessageReference const& msg,
                                                          std::string const& parameterName);

      xoap::SOAPElement extractSOAPCommandParameterElement(xoap::MessageReference const& msg,
                                                           std::string const& parameterName);
      void addSOAPReplyParameter(xoap::MessageReference const& msg,
                                 std::string const& parameterName,
                                 bool const val);
      void addSOAPReplyParameter(xoap::MessageReference const& msg,
                                 std::string const& parameterName,
                                 std::string const& val);
      void addSOAPReplyParameter(xoap::MessageReference const& msg,
                                 std::string const& parameterName,
                                 uint32_t const val);

      /**
       * Extract all child nodes of the body element of a message. Two
       * methods:
       * - extractBodyNode() for when only a single node is allowed,
       * - extractBodyNodes() for when more than one node is allowed.
       */
      xoap::SOAPElement extractBodyNode(xoap::MessageReference const& msg);
      std::vector<xoap::SOAPElement> extractBodyNodes(xoap::MessageReference const& msg);

      /**
       * Starting from a given node, extract its child node with the
       * specified name. Only one such child node is allowed.
       */
      xoap::SOAPElement extractChildNode(xoap::SOAPElement& node,
                                         xoap::SOAPName& childName);

    } // namespace soap

  } // namespace utils
} // namespace tcds

#endif // _tcds_utils_SOAPUtils_h
