#ifndef _tcds_utils_SafeBoolBase_h_
#define _tcds_utils_SafeBoolBase_h_

namespace tcds {
  namespace utils {

    class SafeBoolBase
    {
      // See here for what lies behind the curtain:
      //   https://www.artima.com/cppsource/safebool.html
    protected:
      SafeBoolBase() {};
      SafeBoolBase(SafeBoolBase const& base) {};
      virtual ~SafeBoolBase() {};

      typedef void (SafeBoolBase::*boolType)() const;
      void thisTypeDoesNotSupportComparisons() const {};

      SafeBoolBase& operator=(SafeBoolBase const& base) {return *this;};
    };

    template <typename T=void>
    class SafeBool : public SafeBoolBase
    {
    public:
      operator boolType() const {
        return (static_cast<const T*>(this))->booleanTest() ?
          &SafeBoolBase::thisTypeDoesNotSupportComparisons : 0;
      }
    protected:
      virtual ~SafeBool() {};
    };

    template<>
    class SafeBool<void> : public SafeBoolBase
    {
    public:
      operator boolType() const
      {
        return (booleanTest() == true) ?
          &SafeBool::thisTypeDoesNotSupportComparisons : 0;
      }
    protected:
      virtual bool booleanTest() const = 0;
      virtual ~SafeBool() {};
    };

    // Avoid conversion to bool.
    template <typename T, typename U>
    void operator==(SafeBool<T> const& lhs, SafeBool<U> const& rhs) {
      lhs.thisTypeDoesNotSupportComparisons();
    }

    // Avoid conversion to bool.
    template <typename T, typename U>
    void operator!=(SafeBool<T> const& lhs, SafeBool<U> const& rhs) {
      lhs.thisTypeDoesNotSupportComparisons();
    }

  } // namespace utils
} // namespace tcds

#endif // _tcds_utils_SafeBoolBase_h_
