#ifndef _tcds_utils_version_h_
#define _tcds_utils_version_h_

#include <functional>
#include <set>
#include <string>

#include "config/PackageInfo.h"

// !!! Edit these lines to reflect the latest package version. !!!
#define TCDS_UTILS_VERSION_MAJOR 4
#define TCDS_UTILS_VERSION_MINOR 15
#define TCDS_UTILS_VERSION_PATCH 0

// If any previous versions available:
// #define UTILS_PREVIOUS_VERSIONS "3.8.0,3.8.1"
// else:
#undef TCDS_UTILS_PREVIOUS_VERSIONS

//
// Template macros and boilerplate code.
//
#define TCDS_UTILS_VERSION_CODE PACKAGE_VERSION_CODE(TCDS_UTILS_VERSION_MAJOR,TCDS_UTILS_VERSION_MINOR,TCDS_UTILS_VERSION_PATCH)
#ifndef TCDS_UTILS_PREVIOUS_VERSIONS
#define TCDS_UTILS_FULL_VERSION_LIST PACKAGE_VERSION_STRING(TCDS_UTILS_VERSION_MAJOR,TCDS_UTILS_VERSION_MINOR,TCDS_UTILS_VERSION_PATCH)
#else
#define TCDS_UTILS_FULL_VERSION_LIST TCDS_UTILS_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(TCDS_UTILS_VERSION_MAJOR,TCDS_UTILS_VERSION_MINOR,TCDS_UTILS_VERSION_PATCH)
#endif

namespace tcds {
  namespace utils {

    const std::string project = "tcds";
    const std::string package = "utils";
    const std::string versions = TCDS_UTILS_FULL_VERSION_LIST;
    const std::string description = "CMS TCDS helper software.";
    const std::string authors = "Jeroen Hegeman";
    const std::string summary = "CMS TCDS helper software.";
    const std::string link = "https://twiki.cern.ch/twiki/bin/view/CMS/TcdsNotes";
    config::PackageInfo getPackageInfo();
    void checkPackageDependencies();
    std::set<std::string, std::less<std::string> > getPackageDependencies();

  } // namespace utils
} // namespace tcds

#endif
