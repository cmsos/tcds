#ifndef _tcds_utils_SOAPCmdEnableRandomTriggers_h_
#define _tcds_utils_SOAPCmdEnableRandomTriggers_h_

#include "xoap/MessageReference.h"

#include "tcds/utils/SOAPCmdBase.h"

namespace tcds {
  namespace utils {

    class XDAQAppBase;

    template <class T>
      class SOAPCmdEnableRandomTriggers : public SOAPCmdBase<T>
      {

      public:
        SOAPCmdEnableRandomTriggers(T& controller);

        /**
         * Sends a single B-command. Bound to the SOAP command EnableRandomTriggers.
         * - SOAP command parameters:
         *   - xsd:unsignedInt 'frequency': the desired random-trigger frequency (Hz).
         * - SOAP command return value:
         *   - xsd:unsignedInt 'frequency': the realized random-trigger frequency (Hz).
         *
         * NOTE: This command has to be able to be executed from
         * 'random' web browsers, so it does not require a requestor
         * ID.
         */
        virtual xoap::MessageReference executeImpl(xoap::MessageReference const& msg);

      };

  } // namespace utils
} // namespace tcds

#include "tcds/utils/SOAPCmdEnableRandomTriggers.hxx"

#endif // _tcds_utils_SOAPCmdEnableRandomTriggers_h_
