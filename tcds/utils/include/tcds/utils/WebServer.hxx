#include "tcds/utils/WebTab.h"

#include <utility>

template <class O>
void
tcds::utils::WebServer::registerWebObject(std::string const& name,
                                          std::string const& description,
                                          Monitor const& monitor,
                                          std::string const& itemSetName,
                                          std::string const& tabName,
                                          size_t const colSpan)
{
  WebTab& tab = getTab(tabName);

  // NOTE: This is a bit tricky, but from here the WebTab takes
  // ownership of the created object. (Hence the std::move().)
  std::unique_ptr<O> object = std::unique_ptr<O>(new O(name, description, monitor, itemSetName, tabName, colSpan));
  tab.addWebObject(std::unique_ptr<WebObject>(std::move(object)));
}
