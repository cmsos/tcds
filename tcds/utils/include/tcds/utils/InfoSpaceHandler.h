#ifndef _tcds_utils_InfoSpaceHandler_h_
#define _tcds_utils_InfoSpaceHandler_h_

#include <list>
#include <stdint.h>
#include <string>
#include <vector>
#include <tr1/memory>
#include <tr1/unordered_map>

#include "toolbox/TimeVal.h"
#include "xdata/Boolean.h"
#include "xdata/Double.h"
#include "xdata/Float.h"
#include "xdata/String.h"
#include "xdata/TimeVal.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/UnsignedInteger64.h"
#include "xdata/Vector.h"

#include "tcds/utils/InfoSpaceHandlerIF.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Uncopyable.h"
#include "tcds/utils/XDAQObject.h"

namespace log4cplus {
  class Logger;
}

namespace xdata {
  class InfoSpace;
}

namespace xdaq {
  class Application;
}

namespace tcds {
  namespace utils {

    class InfoSpaceUpdater;
    class Monitor;
    class WebServer;

    class InfoSpaceHandler :
      public InfoSpaceHandlerIF,
      public XDAQObject,
      private Uncopyable
    /**
     * This class implements a thread-safe wrapper around the XDAQ
     * xdata::InfoSpace class. Should not be copied, for safety
     * reasons. The InfoSpaceHandler basically provides a read/write
     * buffer around the actual InfoSpace. The readInfoSpace() and
     * writeInfoSpace() methods can be used to synchronize the buffer
     * with the underlying InfoSpace.
     *
     * @note
     * If needed, this class could be made a bit more efficient by
     * keeping the items_ sorted and then instead of using std::find()
     * using a find() method based on lower_bound(). Something along
     * the lines of:
     *
     * const_iterator find(const T& t) const
     * {
     *   const_iterator i = lower_bound(begin(), end(), t, cmp);
     *   return i == end() || cmp(t, *i) ? end() : i;
     * }
     */
    {

    private:
      /**
       * The InfoSpacePairX classes are implemented as private nested
       * classes inside InfoSpaceHandler to hide them from the
       * outside. (They should only be used by the InfoSpaceHandler
       * class itself.)
       */
      template <typename T, typename U>
        class InfoSpacePair
      {
        friend class InfoSpaceHandler;
      public:
        InfoSpacePair(T value)
          {
            cppVar_ = value;
            xdaqVarP_ = std::tr1::shared_ptr<U>(new U(cppVar_));
          }
        ~InfoSpacePair()
          {
            xdaqVarP_.reset();
          }
        std::tr1::shared_ptr<xdata::Serializable> ptr() const
          {
            return xdaqVarP_;
          }
        T value() const
        {
          return cppVar_;
        }
        // BUG BUG BUG
        // Maybe this should be the next generation? Tricky, though,
        // since it is no longer const.
        /* T value(bool const pull=false) */
        /* { */
        /*   if (pull) */
        /*     { */
        /*       readFromInfoSpace(); */
        /*     } */
        /*   return cppVar_; */
        /* } */
        // BUG BUG BUG end
        void set(T const newVal, bool const push=false)
        {
          cppVar_ = newVal;
          if (push)
            {
              writeToInfoSpace();
            }
        }
      private:
        void readFromInfoSpace()
        {
          cppVar_ = *xdaqVarP_;
        }
        void writeToInfoSpace()
        {
          *xdaqVarP_ = cppVar_;
        }
        T cppVar_;
        std::tr1::shared_ptr<U> xdaqVarP_;
      };

      template <typename T, typename U>
        class InfoSpacePairV
      {
      public:
        InfoSpacePairV(std::vector<T> value)
          {
            cppVar_ = value;
            xdaqVarP_ = std::tr1::shared_ptr<xdata::Vector<U> >(new xdata::Vector<U>);
            typename std::vector<T>::iterator it;
            for (it = cppVar_.begin(); it != cppVar_.end(); ++it)
              {
                xdaqVarP_->push_back(*it);
              }
          }
        ~InfoSpacePairV()
          {
            xdaqVarP_.reset();
          }
        std::vector<T> value() const
        {
          return cppVar_;
        }
        std::tr1::shared_ptr<xdata::Serializable> ptr() const
          {
            return xdaqVarP_;
          }
        void set(std::vector<T> const newVal, bool const push=false)
        {
          cppVar_ = newVal;
          if (push)
            {
              xdaqVarP_->clear();
              typename std::vector<T>::iterator it;
              for (it = cppVar_.begin(); it != cppVar_.end(); ++it)
                {
                  xdaqVarP_->push_back(*it);
                }
            }
        }
        void readFromInfoSpace()
        {
          cppVar_.clear();
          typename xdata::Vector<U>::iterator it;
          for (it = xdaqVarP_->begin(); it != xdaqVarP_->end(); ++it)
            {
              cppVar_.push_back(*it);
            }
        }
        void writeToInfoSpace()
        {
          xdaqVarP_->clear();
          typename std::vector<T>::iterator it;
          for (it = cppVar_.begin(); it != cppVar_.end(); ++it)
            {
              xdaqVarP_->push_back(*it);
            }
        }
      private:
        std::vector<T> cppVar_;
        std::tr1::shared_ptr<xdata::Vector<U> > xdaqVarP_;
      };

      typedef InfoSpacePair<bool, xdata::Boolean> BoolInfoSpacePair;
      typedef InfoSpacePair<double, xdata::Double> DoubleInfoSpacePair;
      typedef InfoSpacePair<float, xdata::Float> FloatInfoSpacePair;
      typedef InfoSpacePair<std::string, xdata::String> StringInfoSpacePair;
      typedef InfoSpacePair<toolbox::TimeVal, xdata::TimeVal> TimeValInfoSpacePair;
      typedef InfoSpacePair<uint32_t, xdata::UnsignedInteger32> UInt32InfoSpacePair;
      typedef InfoSpacePair<uint64_t, xdata::UnsignedInteger64> UInt64InfoSpacePair;
      typedef InfoSpacePairV<double, xdata::Double> DoubleVecInfoSpacePair;
      typedef InfoSpacePairV<std::string, xdata::String> StringVecInfoSpacePair;
      typedef InfoSpacePairV<toolbox::TimeVal, xdata::TimeVal> TimeValVecInfoSpacePair;
      typedef InfoSpacePairV<uint32_t, xdata::UnsignedInteger32> UInt32VecInfoSpacePair;

    public:

      friend class InfoSpaceHandlerLocker;

      // NOTE: This is not super-pretty, but it allows the
      // MultiInfoSpaceHandler just a bit more control over the
      // InfoSpaceHandler (in particular: the name of the tab used in
      // the web interface) without exposing anything to the whole
      // wide world.
      friend class MultiInfoSpaceHandler;

      typedef std::vector<InfoSpaceItem> ItemVec;
      typedef std::tr1::unordered_map<std::string, BoolInfoSpacePair> BoolMap;
      typedef std::tr1::unordered_map<std::string, DoubleInfoSpacePair> DoubleMap;
      typedef std::tr1::unordered_map<std::string, FloatInfoSpacePair> FloatMap;
      typedef std::tr1::unordered_map<std::string, StringInfoSpacePair> StringMap;
      typedef std::tr1::unordered_map<std::string, TimeValInfoSpacePair> TimeValMap;
      typedef std::tr1::unordered_map<std::string, UInt32InfoSpacePair> UInt32Map;
      typedef std::tr1::unordered_map<std::string, UInt64InfoSpacePair> UInt64Map;
      typedef std::tr1::unordered_map<std::string, DoubleVecInfoSpacePair> DoubleVecMap;
      typedef std::tr1::unordered_map<std::string, StringVecInfoSpacePair> StringVecMap;
      typedef std::tr1::unordered_map<std::string, TimeValVecInfoSpacePair> TimeValVecMap;
      typedef std::tr1::unordered_map<std::string, UInt32VecInfoSpacePair> UInt32VecMap;

      virtual ~InfoSpaceHandler();

      /**
       * Synchronize the InfoSpaceHandler buffer to the underlying
       * InfoSpace.
       */
      void readInfoSpace();

      /**
       * Synchronize the underlying InfoSpace to the InfoSpaceHandler
       * buffer. Normally only calls fireItemGroupChanged() when
       * something actually changed, unless force is set to true.
       */
      void writeInfoSpace(bool const force=false);

      std::string name() const;

      /**
       * The various InfoSpace item 'creation' methods.
       */
      void createBool(std::string const& name,
                      bool const value=false,
                      std::string const& format="",
                      InfoSpaceItem::UpdateType const updateType=InfoSpaceItem::HW32,
                      bool const isValid=false,
                      // BUG BUG BUG
                      // This is still a bit ugly.
                      std::string const& hwName="");
                      // BUG BUG BUG end
      void createDouble(std::string const& name,
                        double const value=0.,
                        std::string const& format="",
                        InfoSpaceItem::UpdateType const updateType=InfoSpaceItem::PROCESS,
                        bool const isValid=false,
                        // BUG BUG BUG
                        // This is still a bit ugly.
                        std::string const& hwName="");
                        // BUG BUG BUG end
      void createFloat(std::string const& name,
                       float const value=0.,
                       std::string const& format="",
                       InfoSpaceItem::UpdateType const updateType=InfoSpaceItem::PROCESS,
                       bool const isValid=false,
                        // BUG BUG BUG
                        // This is still a bit ugly.
                        std::string const& hwName="");
                        // BUG BUG BUG end
      void createString(std::string const& name,
                        std::string const& value="",
                        std::string const& format="",
                        InfoSpaceItem::UpdateType const updateType=InfoSpaceItem::PROCESS,
                        bool const isValid=false,
                        // BUG BUG BUG
                        // This is still a bit ugly.
                        std::string const& hwName="");
                        // BUG BUG BUG end
      void createTimeVal(std::string const& name,
                         toolbox::TimeVal const& value=toolbox::TimeVal(0),
                         std::string const& format="",
                         InfoSpaceItem::UpdateType const updateType=InfoSpaceItem::PROCESS,
                         bool const isValid=false);
      void createUInt32(std::string const& name,
                        uint32_t const value=0,
                        std::string const& format="",
                        InfoSpaceItem::UpdateType const updateType=InfoSpaceItem::HW32,
                        bool const isValid=false,
                        // BUG BUG BUG
                        // This is still a bit ugly.
                        std::string const& hwName="");
                        // BUG BUG BUG end
      void createUInt64(std::string const& name,
                        uint64_t const value=0,
                        std::string const& format="",
                        InfoSpaceItem::UpdateType const updateType=InfoSpaceItem::HW64,
                        bool const isValid=false);
      void createDoubleVec(std::string const& name,
                           std::vector<double> const value=std::vector<double>(),
                           std::string const& format="",
                           InfoSpaceItem::UpdateType const updateType=InfoSpaceItem::PROCESS,
                           bool const isValid=false);
      void createStringVec(std::string const& name,
                           std::vector<std::string> const value=std::vector<std::string>(),
                           std::string const& format="",
                           InfoSpaceItem::UpdateType const updateType=InfoSpaceItem::PROCESS,
                           bool const isValid=false);
      void createTimeValVec(std::string const& name,
                            std::vector<toolbox::TimeVal> const value=std::vector<toolbox::TimeVal>(),
                            std::string const& format="",
                            InfoSpaceItem::UpdateType const updateType=InfoSpaceItem::PROCESS,
                            bool const isValid=false);
      void createUInt32Vec(std::string const& name,
                           std::vector<uint32_t> const value=std::vector<uint32_t>(),
                           std::string const& format="",
                           InfoSpaceItem::UpdateType const updateType=InfoSpaceItem::HW32BLOCK,
                           bool const isValid=false);

      std::string getFormatted(std::string const& name) const;
      ItemVec& getItems();

      /**
       * Getters for various types of InfoSpace variables.
       */
      bool getBool(std::string const& name) const;
      double getDouble(std::string const& name) const;
      float getFloat(std::string const& name) const;
      std::string getString(std::string const& name) const;
      toolbox::TimeVal getTimeVal(std::string const& name) const;
      uint32_t getUInt32(std::string const& name) const;
      uint64_t getUInt64(std::string const& name) const;
      std::vector<double> getDoubleVec(std::string const& name) const;
      std::vector<std::string> getStringVec(std::string const& name) const;
      std::vector<toolbox::TimeVal> getTimeValVec(std::string const& name) const;
      std::vector<uint32_t> getUInt32Vec(std::string const& name) const;

      /**
       * Setters for various types of InfoSpace variables.
       *
       * NOTE: The 'push' parameters immediately push the change into
       * the underlying InfoSpace. This does _not_ play nice with the
       * XMAS monitoring: every 'push' will force an update of the
       * whole InfoSpace in the XMAS monitoring.
       */
      void setBool(std::string const& name,
                   bool const newVal,
                   bool const push=false);
      void setDouble(std::string const& name,
                     double const newVal,
                     bool const push=false);
      void setFloat(std::string const& name,
                    float const newVal,
                    bool const push=false);
      void setString(std::string const& name,
                     std::string const& newVal,
                     bool const push=false);
      void setTimeVal(std::string const& name,
                      toolbox::TimeVal const& newVal,
                      bool const push=false);
      void setUInt32(std::string const& name,
                     uint32_t const newVal,
                     bool const push=false);
      void setUInt64(std::string const& name,
                     uint64_t const newVal,
                     bool const push=false);
      void setDoubleVec(std::string const& name,
                        std::vector<double> const newVal,
                        bool const push=false);
      void setStringVec(std::string const& name,
                        std::vector<std::string> const newVal,
                        bool const push=false);
      void setTimeValVec(std::string const& name,
                         std::vector<toolbox::TimeVal> const newVal,
                         bool const push=false);
      void setUInt32Vec(std::string const& name,
                        std::vector<uint32_t> const newVal,
                        bool const push=false);

      /**
       * Update all items in this InfoSpace using the associated
       * InfoSpaceUpdater.
       */
      void update();

      /**
       * Set all InfoSpace items to valid/invalid.
       */
      void setValid();
      void setInvalid();

      /**
       * Register itemsets in this InfoSpace with a monitor/webserver
       * pair for remote access to the monitoring info.
       */
      virtual void registerItemSets(Monitor& monitor,
                                    WebServer& webServer);

      bool exists(std::string const& itemName) const;

      // BUG BUG BUG
      // This needs follow-up. Maybe a true InfoSpaceHandler base class?
      InfoSpaceHandler(xdaq::Application& xdaqApp,
                       std::string const& name,
                       InfoSpaceUpdater* const updater=0,
                       xdata::InfoSpace* const mirrorInfoSpace=0,
                       bool const forceSync=false);
      // BUG BUG BUG end

      // The default value shown for 'invalid' and uninitialized
      // items.
      static std::string const kInvalidItemString;
      static std::string const kUninitializedString;

    protected:
      /**
       * Methods to lock/unlock the underlying InfoSpace.
       * These methods use the lock of the underlying InfoSpace
       * itself.
       * @note Technically speaking these are not const, but
       * practically speaking they are (since they only touch the
       * mutable lock itself).
       */
      /* void lock() const; */
      /* void unlock() const; */

      /**
       * Register itemsets in this InfoSpace with a monitor
       * object. Obviously needs to be implemented by the derived
       * class itself.
       */
      // BUG BUG BUG
      // This needs follow-up. We should not need these method. It
      // doesn't actually do anything... Maybe we need a true
      // InfoSpaceHandler base class?
      virtual void registerItemSetsWithMonitor(Monitor& monitor);
      // BUG BUG BUG end

      /**
       * Register itemsets in this InfoSpace with a webserver
       * object. Obviously needs to be implemented by the derived
       * class itself.
       */
      // BUG BUG BUG
      // This needs follow-up. We should not need this method. It
      // doesn't actually do anything... Maybe we need a true
      // InfoSpaceHandler base class?
      virtual void registerItemSetsWithWebServer(WebServer& webServer,
                                                 Monitor& monitor,
                                                 std::string const& tabName="");
      // BUG BUG BUG end

      virtual std::string formatItem(ItemVec::const_iterator const& item) const;

      log4cplus::Logger& logger_;

    private:
      xdata::InfoSpace* infoSpaceP_;
      std::string name_;
      InfoSpaceUpdater* updaterP_;
      xdata::InfoSpace* mirrorInfoSpaceP_;
      bool forceSync_;
      bool somethingHasChanged_;

      ItemVec items_;
      BoolMap boolMap_;
      DoubleMap doubleMap_;
      FloatMap floatMap_;
      StringMap stringMap_;
      TimeValMap timevalMap_;
      UInt32Map uint32Map_;
      UInt64Map uint64Map_;
      DoubleVecMap doubleVecMap_;
      StringVecMap stringVecMap_;
      TimeValVecMap timevalVecMap_;
      UInt32VecMap uint32VecMap_;

      void throwItemExists(std::string const& name);

      // Helper methods interfacing to the underlying
      // xdata::InfoSpace.
      void fireItemAvailable(std::string const& name,
                             xdata::Serializable* serializable);
      void fireItemValueChanged(std::string const& name);
      void fireItemGroupChanged(std::list<std::string>& names);

    };

  } // namespace utils
} // namespace tcds

#endif // _tcds_utils_InfoSpaceHandler_h_
