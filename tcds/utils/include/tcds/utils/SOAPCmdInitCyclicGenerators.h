#ifndef _tcds_utils_SOAPCmdInitCyclicGenerators_h_
#define _tcds_utils_SOAPCmdInitCyclicGenerators_h_

#include "xoap/MessageReference.h"

#include "tcds/utils/SOAPCmdBase.h"

namespace tcds {
  namespace utils {

    class XDAQAppBase;

    template <class T>
      class SOAPCmdInitCyclicGenerators : public SOAPCmdBase<T>
    {

    public:
      SOAPCmdInitCyclicGenerators(T& controller);

      /**
       * (Re)Initializes all cyclic generators (i.e., resets the
       * pre-/postscale counters, etc.).
       * - SOAP command parameters: none.
       * - SOAP command return value: none.
       */
      virtual xoap::MessageReference executeImpl(xoap::MessageReference const& msg);

    };

  } // namespace utils
} // namespace tcds

#include "tcds/utils/SOAPCmdInitCyclicGenerators.hxx"

#endif // _tcds_utils_SOAPCmdInitCyclicGenerators_h_
