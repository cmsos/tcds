#ifndef _tcds_utils_PSXReply_h_
#define _tcds_utils_PSXReply_h_

#include <string>

#include "xoap/MessageReference.h"

namespace tcds {
  namespace utils {

    class PSXReply
    /**
     * Helper class to disentangle SOAP replies from the XDAQ PSX
     * server.
     */
    {

    public:

      PSXReply(xoap::MessageReference& rawReply);
      ~PSXReply();

      bool isValid(std::string const& dpName);
      bool isGood(std::string const& dpName);

      std::string extractRawResult(std::string const& dpName);
      std::string extractDIPQuality(std::string const& dpName);

    private:
      xoap::MessageReference rawReply_;

    };

  } // namespace utils
} // namespace tcds

#endif // _tcds_utils_PSXReply_h_
