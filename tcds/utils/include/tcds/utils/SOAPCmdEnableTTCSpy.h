#ifndef _tcds_utils_SOAPCmdEnableTTCSpy_h_
#define _tcds_utils_SOAPCmdEnableTTCSpy_h_

#include "xoap/MessageReference.h"

#include "tcds/utils/SOAPCmdBase.h"

namespace tcds {
  namespace utils {

    class XDAQAppBase;

    template <class T>
      class SOAPCmdEnableTTCSpy : public SOAPCmdBase<T>
    {

    public:
      SOAPCmdEnableTTCSpy(T& controller);

      /**
       * Enables the TTCSpy logger. Bound to the SOAP command
       * EnableTTCSpy.
       * - SOAP command parameters: none.
       * - SOAP command return value: none.
       */
      virtual xoap::MessageReference executeImpl(xoap::MessageReference const& msg);

    };

  } // namespace utils
} // namespace tcds

#include "tcds/utils/SOAPCmdEnableTTCSpy.hxx"

#endif // _tcds_utils_SOAPCmdEnableTTCSpy_h_
