#ifndef _tcds_utils_FSMDriver_h_
#define _tcds_utils_FSMDriver_h_

#include <memory>
#include <string>
#include <time.h>

#include "toolbox/exception/Listener.h"
#include "toolbox/task/TimerListener.h"

namespace toolbox {
  namespace task {
    class Timer;
    class TimerEvent;
  }
}

namespace xcept {
  class Exception;
}

namespace tcds {
  namespace utils {

    class XDAQAppWithFSMAutomatic;

    class FSMDriver :
      public toolbox::task::TimerListener,
      public toolbox::exception::Listener
    {

    public:
      FSMDriver(XDAQAppWithFSMAutomatic* const xdaqApp);
      ~FSMDriver();

      void start();
      void stop();

      virtual void timeExpired(toolbox::task::TimerEvent& e);
      virtual void onException(xcept::Exception& err);

    private:
      static unsigned int const kGracePeriod_ = 3;
      static time_t const kCheckInterval_ = 5;

      std::string const fsmDriverAlarmName_;
      XDAQAppWithFSMAutomatic* const xdaqAppP_;
      std::string timerName_;
      std::unique_ptr<toolbox::task::Timer> timerP_;

    };

  } // namespace utils
} // namespace tcds

#endif // _tcds_utils_FSMDriver_h_
