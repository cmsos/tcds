#include "tcds/utils/SOAPUtils.h"

template <class T>
tcds::utils::SOAPCmdSendL1APattern<T>::SOAPCmdSendL1APattern(T& controller) :
  SOAPCmdBase<T>(controller, "SendL1APattern", false)
{
}

template <class T>
xoap::MessageReference
tcds::utils::SOAPCmdSendL1APattern<T>::executeImpl(xoap::MessageReference const& msg)
{
  this->controller_.getHw().sendL1APattern();
  xoap::MessageReference reply = tcds::utils::soap::makeCommandSOAPReply(msg);
  return reply;
}
