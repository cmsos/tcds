#ifndef _tcds_utils_SOAPCmdSendBgoTrain_h_
#define _tcds_utils_SOAPCmdSendBgoTrain_h_

#include "xoap/MessageReference.h"

#include "tcds/utils/SOAPCmdBase.h"

namespace tcds {
  namespace utils {

    class XDAQAppBase;

    template <class T>
      class SOAPCmdSendBgoTrain : public SOAPCmdBase<T>
    {

    public:
      SOAPCmdSendBgoTrain(T& controller);

      /**
       * Triggers a B-go train. Bound to the SOAP command SendBgoTrain.
       * - SOAP command parameters:
       *   either one of the following two
       *   -  xsd:string `bgoTrainName': the B-go train name (e.g., 'Sequence10', 'Start').
       *   -  xsd:unsignedInt `bgoTrainNumber': the B-go train number (e.g., '1').
       * - SOAP command return value: none.
       */
      virtual xoap::MessageReference executeImpl(xoap::MessageReference const& msg);

    };

  } // namespace utils
} // namespace tcds

#include "tcds/utils/SOAPCmdSendBgoTrain.hxx"

#endif // _tcds_utils_SOAPCmdSendBgoTrain_h_
