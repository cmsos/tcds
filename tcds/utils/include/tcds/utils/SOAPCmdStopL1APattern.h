#ifndef _tcds_utils_SOAPCmdStopL1APattern_h_
#define _tcds_utils_SOAPCmdStopL1APattern_h_

#include "xoap/MessageReference.h"

#include "tcds/utils/SOAPCmdBase.h"

namespace tcds {
  namespace utils {

    class XDAQAppBase;

    template <class T>
      class SOAPCmdStopL1APattern : public SOAPCmdBase<T>
    {

    public:
      SOAPCmdStopL1APattern(T& controller);

      /**
       * Starts the RAM-based trigger pattern generator. Bound to the
       * SOAP command StopL1APattern.
       * - SOAP command parameters: none.
       * - SOAP command return value: none.
       *
       * NOTE: This command has to be able to be executed from
       * 'random' web browsers, so it does not require a requestor
       * ID.
       */
      virtual xoap::MessageReference executeImpl(xoap::MessageReference const& msg);

    };

  } // namespace utils
} // namespace tcds

#include "tcds/utils/SOAPCmdStopL1APattern.hxx"

#endif // _tcds_utils_SOAPCmdStopL1APattern_h_
