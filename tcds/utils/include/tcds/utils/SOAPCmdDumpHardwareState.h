#ifndef _tcds_utils_SOAPCmdDumpHardwareState_h_
#define _tcds_utils_SOAPCmdDumpHardwareState_h_

#include "xoap/MessageReference.h"

#include "tcds/utils/SOAPCmdBase.h"

namespace tcds {
  namespace utils {

    class XDAQAppBase;

    template <class T>
      class SOAPCmdDumpHardwareState : public SOAPCmdBase<T>
      {

      public:
        SOAPCmdDumpHardwareState(T& controller);

        /**
         * Reads back the hardware configuration. Bound to the SOAP
         * command DumpHardwareState.
         * - SOAP command parameters:
         *   None.
         * - SOAP command return value:
         *   A big string containing a full register dump.
         *
         * NOTE: This command has to be able to be executed from
         * 'random' web browsers, so it does not require a requestor
         * ID.
         */
        virtual xoap::MessageReference executeImpl(xoap::MessageReference const& msg);

      };

  } // namespace utils
} // namespace tcds

#include "tcds/utils/SOAPCmdDumpHardwareState.hxx"

#endif // _tcds_utils_SOAPCmdDumpHardwareState_h_
