#ifndef _tcds_utils_MonitorUpdateEvent_h_
#define _tcds_utils_MonitorUpdateEvent_h_

#include <string>

#include "toolbox/Event.h"

namespace tcds {
  namespace utils {

    class MonitorUpdateEvent : public toolbox::Event
    {
    public:
      MonitorUpdateEvent(void* originator);
    };

  } // namespace utils
} // namespace tcds

#endif // _tcds_utils_MonitorUpdateEvent_h_
