#ifndef _tcds_utils_SOAPCmdSendBCommand_h_
#define _tcds_utils_SOAPCmdSendBCommand_h_

#include "xoap/MessageReference.h"

#include "tcds/utils/SOAPCmdBase.h"

namespace tcds {
  namespace utils {

    class XDAQAppBase;

    template <class T>
      class SOAPCmdSendBCommand : public SOAPCmdBase<T>
    {

    public:
      SOAPCmdSendBCommand(T& controller);

      /**
       * Sends a single B-command. Bound to the SOAP command SendBCommand.
       * - SOAP command parameters:
       *   - xsd:unsignedInt 'bcommandData': the B-command data (8 bits).
       *   - xsd:string 'bcommandType': the B-command type (i.e., long or short).
       *   - xsd:unsignedInt 'bcommandAddress': the TTCrx address (14 bits).
       *   - xsd:unsignedInt 'bcommandSubAddress': the TTCrx register
       *     address/external address (8 bits).
       *   - xsd:string 'addressType': the addressing mode
       *     (i.e., internal or external).
       * - SOAP command return value: none.
       */
      virtual xoap::MessageReference executeImpl(xoap::MessageReference const& msg);

    };

  } // namespace utils
} // namespace tcds

#include "tcds/utils/SOAPCmdSendBCommand.hxx"

#endif // _tcds_utils_SOAPCmdSendBCommand_h_
