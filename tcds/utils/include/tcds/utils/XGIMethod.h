#ifndef _tcds_utils_XGIMethod_h
#define _tcds_utils_XGIMethod_h

#include "xgi/framework/Method.h"
#include "xgi/Method.h"

#include "tcds/utils/Utils.h"

namespace xgi {
  class Input;
  class Output;
  namespace exception {
    class Exception;
  }
}

// These are more of less temporary replacements of the xgi::Method
// and xgi::framework::Method classes, together with the corresponding
// xgi::bind() and xgi::framework::deferredbind() methods. These
// implementations support gzip encoding, until this makes its way
// into XDAQ itself.
//   See also: https://svnweb.cern.ch/trac/cmsos/ticket/3896

// NOTE: The namespacing here is a bit special! I basically just
// replaced everything in its namespace. Not very pretty, but this
// represents the smallest possible changes to the original code.

namespace tcds {
  namespace utils {

    namespace helper0 {

      template<class LISTENER>
        class Method : public xgi::Method<LISTENER>
        {
        public:
          Method()
            {
            }

          virtual ~Method()
            {
            }

          virtual void invoke(xgi::Input* in, xgi::Output* out)
          {
            try
              {
                (this->obj_->*this->func_)(in, out);
              }
            catch (xgi::exception::Exception& e)
              {
                out->str("");
                out->clear();
                return;
              }

            // Now compress, if allowed.
            bool doGZIP = false;
            std::string const acceptEncoding = in->getenv("ACCEPT_ENCODING");
            if (!acceptEncoding.empty() && (acceptEncoding.find("gzip") != std::string::npos))
              {
                doGZIP = true;
              }
            if (doGZIP)
              {
                out->getHTTPResponseHeader().addHeader("Content-Encoding", "gzip");
                std::string const tmp = tcds::utils::compressString(out->str());
                out->str("");
                *out << tmp;
              }
          }
        };

      template <class LISTENER>
        void bind(LISTENER* obj, void (LISTENER::*func)(xgi::Input*, xgi::Output*), const std::string & messageName)
        {
          Method<LISTENER>* f = new Method<LISTENER>;
          f->obj_ = obj;
          f->func_ = func;
          f->name_ = messageName;
          obj->addMethod(f, messageName);
        }

      template<class APPLICATION, class LISTENER>
        void deferredbind(APPLICATION* application, LISTENER* obj, void (LISTENER::*func) (xgi::Input*, xgi::Output*), const std::string & messageName)
      {
        Method<LISTENER>* f = new Method<LISTENER>();
        f->obj_ = obj;
        f->func_ = func;
        f->name_ = messageName;
        application->addMethod(f, messageName);
      }

    } // namespace helper0

    namespace helper1 {

      template<class LISTENER>
        class Method : public xgi::framework::Method<LISTENER>
        {
        public:
          Method()
            {
            }

          virtual ~Method()
            {
            }

          virtual void invoke(xgi::Input* in, xgi::Output* out)
          {
            this->obj_->getHTMLHeader(in, out);
            try
              {
                (this->obj_->*this->func_)(in, out);
              }
            catch (xgi::exception::Exception& e)
              {
                out->str("");
                out->clear();
                this->obj_->errorPage(in, out, e);
                return;
              }
            this->obj_->getHTMLFooter(in, out);

            // Now compress, if allowed.
            bool doGZIP = false;
            std::string const acceptEncoding = in->getenv("ACCEPT_ENCODING");
            if (!acceptEncoding.empty() && (acceptEncoding.find("gzip") != std::string::npos))
              {
                doGZIP = true;
              }
            if (doGZIP)
              {
                out->getHTTPResponseHeader().addHeader("Content-Encoding", "gzip");
                std::string const tmp = tcds::utils::compressString(out->str());
                out->str("");
                *out << tmp;
              }
          }
        };

      template<class LISTENER>
        void deferredbind(xdaq::Application* application, LISTENER* obj,
                          void (LISTENER::*func) (xgi::Input*, xgi::Output*),
                          const std::string & messageName)
        {
          Method<LISTENER>* f = new Method<LISTENER>();
          f->obj_ = obj;
          f->func_ = func;
          f->name_ = messageName;
          application->addMethod(f, messageName);
        }

    } // namespace helper1

  } // namespace utils
} // namespace tcds

#endif // _tcds_utils_XGIMethod_h
