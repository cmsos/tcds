#ifndef _tcds_utils_AppHist_h_
#define _tcds_utils_AppHist_h_

#include <deque>
#include <string>

#include "tcds/utils/AppHistItem.h"
#include "tcds/utils/Lock.h"

namespace tcds {
  namespace utils {

    class AppHist
    {

    public:
      AppHist();
      ~AppHist();

      void addItem(std::string const& msg);

      std::string getJSONString() const;

    private:
      static unsigned const kMaxHistSize = 1000;
      std::deque<tcds::utils::AppHistItem> history_;
      mutable Lock lock_;

    };

  } // namespace utils
} // namespace tcds

#endif // _tcds_utils_AppHistItem_h_
