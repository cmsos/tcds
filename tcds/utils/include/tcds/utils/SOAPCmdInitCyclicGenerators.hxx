#include "tcds/utils/SOAPUtils.h"

template <class T>
tcds::utils::SOAPCmdInitCyclicGenerators<T>::SOAPCmdInitCyclicGenerators(T& controller) :
  SOAPCmdBase<T>(controller, "InitCyclicGenerators")
{
}

template <class T>
xoap::MessageReference
tcds::utils::SOAPCmdInitCyclicGenerators<T>::executeImpl(xoap::MessageReference const& msg)
{
  // Do what we were asked to do.
  this->controller_.getHw().initCyclicGenerators();

  // Send reply.
  xoap::MessageReference reply = tcds::utils::soap::makeCommandSOAPReply(msg);
  return reply;
}
