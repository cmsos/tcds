#ifndef _tcds_utils_SOAPCmdSendL1A_h_
#define _tcds_utils_SOAPCmdSendL1A_h_

#include "xoap/MessageReference.h"

#include "tcds/utils/SOAPCmdBase.h"

namespace tcds {
  namespace utils {

    class XDAQAppBase;

    template <class T>
      class SOAPCmdSendL1A : public SOAPCmdBase<T>
    {

    public:
      SOAPCmdSendL1A(T& controller);

      /**
       * Sends a single L1A. Bound to the SOAP command SendL1A.
       * - SOAP command parameters: none.
       * - SOAP command return value: none.
       */
      virtual xoap::MessageReference executeImpl(xoap::MessageReference const& msg);

    };

  } // namespace utils
} // namespace tcds

#include "tcds/utils/SOAPCmdSendL1A.hxx"

#endif // _tcds_utils_SOAPCmdSendL1A_h_
