#ifndef _tcds_utils_XDAQAppWithFSMForPMs_h_
#define _tcds_utils_XDAQAppWithFSMForPMs_h_

#include <memory>

#include "tcds/utils/XDAQAppWithFSMBase.h"

namespace xdaq {
  class ApplicationStub;
}

namespace tcds {
  namespace hwlayer {
    class DeviceBase;
  }
}

namespace tcds {
  namespace utils {

    class XDAQAppWithFSMForPMs : public XDAQAppWithFSMBase
    {

      // NOTE: All TCDS control applications (at least the ones that
      // have a state machine) internally contain the same FSM
      // (implemented in XDAQAppWithFSMBase). The difference between
      // classes like XDAQAppWithFSMAutomatic, XDAQAppWithFSMBasic,
      // etc. lies in the fact that different transitions are exposed
      // to the outside world (i.e., bound to SOAP commands).

    public:
      virtual ~XDAQAppWithFSMForPMs();

    protected:
      XDAQAppWithFSMForPMs(xdaq::ApplicationStub* const stub,
                           std::unique_ptr<tcds::hwlayer::DeviceBase> hw);

    };

  } // namespace utils
} // namespace tcds

#endif // _tcds_utils_XDAQAppWithFSMForPMs_h_
