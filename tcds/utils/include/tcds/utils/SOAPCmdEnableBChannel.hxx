#include <stdint.h>
#include <string>
#include <vector>

#include "toolbox/string.h"
#include "xcept/Exception.h"
#include "xoap/SOAPName.h"
#include "xoap/SOAPElement.h"

#include "tcds/exception/Exception.h"
#include "tcds/utils/Definitions.h"
#include "tcds/utils/SOAPUtils.h"
#include "tcds/utils/Utils.h"

template <class T>
tcds::utils::SOAPCmdEnableBChannel<T>::SOAPCmdEnableBChannel(T& controller) :
  SOAPCmdBase<T>(controller, "EnableBChannel")
{
}

template <class T>
xoap::MessageReference
tcds::utils::SOAPCmdEnableBChannel<T>::executeImpl(xoap::MessageReference const& msg)
{
  // Extract the command parameter.
  // NOTE: In this case a bit tricky. It accepts either 'bgoName' or
  // 'bgoNumber'.
  xoap::SOAPName soapNameName("bgoName", "xdaq", "");
  xoap::SOAPName soapNameNumber("bgoNumber", "xdaq", "");
  xoap::SOAPElement commandNode = tcds::utils::soap::extractBodyNode(msg);
  std::vector<xoap::SOAPElement> childrenName =
    commandNode.getChildElements(soapNameName);
  std::vector<xoap::SOAPElement> childrenNumber =
    commandNode.getChildElements(soapNameNumber);

  // We should have found one of the two...
  if ((childrenName.size() == 0) && (childrenNumber.size() == 0))
    {
      XCEPT_RAISE(tcds::exception::SOAPFormatProblem,
                  toolbox::toString("Expected either a child element 'xdaq:bgoName' "
                                    "or a child element 'xdaq:bgoNumber' "
                                    "in SOAP command '%s'.",
                                    this->commandName().c_str()));
    }

  // And we should only have found one of the two...
  if ((childrenName.size() != 0) && (childrenNumber.size() != 0))
    {
      XCEPT_RAISE(tcds::exception::SOAPFormatProblem,
                  toolbox::toString("Expected either a child element 'xdaq:bgoName' "
                                    "or a child element 'xdaq:bgoNumber' "
                                    "in SOAP command '%s' but found both.",
                                    this->commandName().c_str()));
    }

  // Whatever we found: translate it into a valid B-go number.
  std::string bgoName;
  tcds::definitions::BGO_NUM bgoNumber = tcds::definitions::BGO_BC0;
  if (childrenName.size() != 0)
    {
      bgoName = tcds::utils::soap::extractSOAPCommandParameterString(msg, "bgoName");
      bgoNumber = tcds::utils::bgoNameToNumber(bgoName);
    }
  else if (childrenNumber.size() != 0)
    {
      uint32_t const tmp =
        tcds::utils::soap::extractSOAPCommandParameterUnsignedInteger(msg, "bgoNumber");
      bgoNumber = tcds::definitions::BGO_NUM(tmp);
      // Check the range.
      tcds::utils::checkBgoNumber(bgoNumber);
    }

  // Extract the B-channel mode (single or double).
  std::string const tmp =
    tcds::utils::soap::extractSOAPCommandParameterString(msg, "bchannelMode");
  tcds::definitions::BCHANNEL_MODE bchannelMode = tcds::definitions::BCHANNEL_MODE_SINGLE;
  bchannelMode = tcds::utils::bchannelModeFromString(tmp);

  // Keep track of history.
  std::string histMsg = "unknown";
  if (childrenName.size() != 0)
    {
      histMsg = toolbox::toString("%s: bgoName = '%s', bchannelMode = '%s'",
                                  this->commandName().c_str(),
                                  bgoName.c_str(),
                                  tmp.c_str());
    }
  else if (childrenNumber.size() != 0)
    {
      histMsg = toolbox::toString("%s: bgoNumber = %d, bchannelMode = '%s'",
                                  this->commandName().c_str(),
                                  bgoNumber,
                                  tmp.c_str());
    }
  this->controller_.appStateInfoSpace_.addHistoryItem(histMsg);

  // Do what we were asked to do.
  this->controller_.getHw().enableBchannel(bgoNumber, bchannelMode);

  // Send reply.
  xoap::MessageReference reply = tcds::utils::soap::makeCommandSOAPReply(msg);
  return reply;
}
