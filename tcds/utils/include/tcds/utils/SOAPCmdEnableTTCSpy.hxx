#include "tcds/utils/SOAPUtils.h"

template <class T>
tcds::utils::SOAPCmdEnableTTCSpy<T>::SOAPCmdEnableTTCSpy(T& controller) :
  SOAPCmdBase<T>(controller, "EnableTTCSpy", false)
{
}

template <class T>
xoap::MessageReference
tcds::utils::SOAPCmdEnableTTCSpy<T>::executeImpl(xoap::MessageReference const& msg)
{
  this->controller_.getHw().resetTTCSpy();
  this->controller_.getHw().enableTTCSpy();
  xoap::MessageReference reply = tcds::utils::soap::makeCommandSOAPReply(msg);
  return reply;
}
