#ifndef _tcds_utils_Utils_h_
#define _tcds_utils_Utils_h_

/** @file
 * This is just a bunch of helper routines that don't fit in their own
 * little boxes.
 */

#include <cctype>
#include <map>
#include <stdint.h>
#include <string>
#include <utility>
#include <vector>

#include "toolbox/TimeVal.h"
#include "xdata/Table.h"

#include "tcds/utils/Definitions.h"

namespace tcds {
  namespace utils {

    class ConfigurationInfoSpaceHandler;

    // Convert a string representing an integer number to an integer
    // number.
    long parseIntFromString(std::string const& numberAsString);

    // Restrain a BX number inside the allowed range.
    unsigned int coerceBXNumber(int const bxNumber);

    // Convert an LHC bucket number to the corresponding BX number.
    unsigned int bucketToBX(unsigned int const bucketNumber);

    // This expands environment variables in path names.
    std::string expandPathName(std::string const& pathNameIn);

    // Check if a certain path exists.
    // NOTE: Can be either a file or a directory.
    bool pathExists(std::string const& pathName);

    // Check if a certain directory exists.
    bool dirExists(std::string const& dirName);

    // Check if a certain path is a directory or not.
    bool pathIsDir(std::string const& pathName);

    // Create a directory (recursively).
    void makeDir(std::string const& dirName);

    // Rename a file/directory.
    void rename(std::string const& from, std::string const& to);

    // // Some misc. string methods.
    // struct convertUp {
    //   unsigned char operator()(unsigned char c) { return ::toupper(c); }
    // };

    // struct convertDown {
    //   unsigned char operator()(unsigned char c) { return ::tolower(c); }
    // };

    std::string escapeAsJSONString(std::string const& stringIn);
    std::string trimString(std::string const& stringToTrim,
                           std::string const& whitespace=" \t\r\n");

    std::string capitalizeString(std::string const& stringIn);

    // A helper method to insert 'invisible spaces' into a FED vector
    // (which are strings that tend to be incredibly long).
    std::string chunkifyFEDVector(std::string const& fedVecIn);

    // A method to nicely write B-go names with all available detail.
    std::string formatBchannelNameString(tcds::definitions::BGO_NUM const bchannelNumber);
    std::string formatBgoNameString(tcds::definitions::BGO_NUM const bgoNumber);

    // A method to nicely write sequence names with all available detail.
    std::string formatSequenceNameString(tcds::definitions::SEQUENCE_NUM const sequenceNumber);

    // A method to get the current timestamp, nicely formatted.
    std::string getTimestamp(bool const isoFormat=false,
                             bool const shortIsoFormat=false);

    // A few methods to nicely write time values.
    std::string formatTimestamp(toolbox::TimeVal const timestamp,
                                toolbox::TimeVal::TimeZone const tz=toolbox::TimeVal::gmt,
                                bool const longFormat=false);
    std::string formatTimestampISO(toolbox::TimeVal const timestamp,
                                   bool const shortIsoFormat=false);
    std::string formatTimestampISO(toolbox::TimeVal const timestamp,
                                   toolbox::TimeVal::TimeZone const tz,
                                   bool const shortIsoFormat=false);
    std::string formatTimestampDate(toolbox::TimeVal const timestamp,
                                    toolbox::TimeVal::TimeZone const tz=toolbox::TimeVal::gmt);
    std::string formatTimestampTime(toolbox::TimeVal const timestamp,
                                    toolbox::TimeVal::TimeZone const tz=toolbox::TimeVal::gmt,
                                    bool const longFormat=false);
    // A method to nicely format time ranges.
    std::string formatTimeRange(toolbox::TimeVal const timeBegin,
                                toolbox::TimeVal const timeEnd,
                                toolbox::TimeVal::TimeZone const tz=toolbox::TimeVal::gmt,
                                bool const longFormat=false);

    // A method to nicely write time differences.
    std::string formatDeltaTString(toolbox::TimeVal const timeBegin,
                                   toolbox::TimeVal const timeEnd,
                                   bool const verbose=false);

    // Mapping between B-go names and numbers.
    std::map<std::string, tcds::definitions::BGO_NUM> bgoNameMap();
    tcds::definitions::BGO_NUM bgoNameToNumber(std::string const& bgoName);
    std::string bgoNumberToName(tcds::definitions::BGO_NUM const& bgoNumber);

    // Mapping between sequence names and numbers.
    std::map<std::string, tcds::definitions::SEQUENCE_NUM> sequenceNameMap();
    tcds::definitions::SEQUENCE_NUM sequenceNameToNumber(std::string const& sequenceName);
    std::string sequenceNumberToName(tcds::definitions::SEQUENCE_NUM const& sequenceNumber);

    // Mapping of B-channel mode strings to enums.
    tcds::definitions::BCHANNEL_MODE bchannelModeFromString(std::string const bchannelModeString);
    // Mapping of B-channel mode enums to strings.
    std::string bchannelModeToString(tcds::definitions::BCHANNEL_MODE const bchannelMode);

    // Mapping of B-command type strings to enums.
    tcds::definitions::BCOMMAND_TYPE bcommandTypeFromString(std::string const bcommandTypeString);
    // Mapping of B-command address type strings to enums.
    tcds::definitions::BCOMMAND_ADDRESS_TYPE bcommandAddressTypeFromString(std::string const bcommandAddressTypeString);

    // Some boundary checking functions.
    // NOTE: These are intended to check the parameter values from
    // SOAP commands, so they all start out with 32-bit numbers.
    void checkBgoNumber(uint32_t const bgoNumber);
    void checkBgoTrainNumber(uint32_t const bgoTrainNumber);
    void checkBCommandData(uint32_t const bcommandData);
    void checkBCommandAddress(uint32_t const address);
    void checkBCommandSubAddress(uint32_t const address);

    //----------

    // Mapping of TTS states and action-triggers to strings.

    std::map<uint8_t, std::pair<std::string, std::string> > ttsMapClassic();
    std::map<uint8_t, std::pair<std::string, std::string> > ttsMapTCDS();

    // NOTE: TTS values are 8-bit, really. But in the PI we use a
    // ninth bit to flag 'internal stuff.'
    std::string TTSStateToString(uint16_t const ttsIn,
                                 bool const piMode=false,
                                 bool const verbose=false);

    // Something similar to the above, but very 'space
    // efficient'. Only used by the PI TTSLogWriter, really.
    std::string TTSStateToStringShort(uint8_t const ttsIn);

    // The following is again similar to the above, but used to
    // describe the 'TTS state triggers' configuration matrix in the
    // CPM/LPM.
    std::string TTSTriggerToString(uint8_t const ttsIn);

    //----------

    // Mapping of TTS channel types to strings.
    std::string ttsChannelTypeToString(tcds::definitions::TTS_CHANNEL_TYPE const type);

    // TTS channel priority ordering.
    std::map<std::string, int> ttsPriorities();

    //----------

    // Mapping of B-data sources to strings.
    std::string BdataSourceToString(tcds::definitions::BDATA_SOURCE const bdataSourceIn);

    // Mapping of B-command types to strings.
    std::string BcommandTypeToString(tcds::definitions::BCOMMAND_TYPE const bcommandTypeIn);
    // Mapping of B-command addressing types to strings.
    std::string BcommandAddressingTypeToString(tcds::definitions::BCOMMAND_ADDRESS_TYPE const bcommandAddressTypeIn);

    // Mapping of trigger types to strings.
    std::string TriggerTypeToString(tcds::definitions::TRIG_TYPE const trigTypeIn);

    // A little helper to (zlib) compress a string.
    std::string compressString(std::string const stringIn);

    // A helper method to group a list of BXs into ranges like '1-5,
    // 11, 13, 20-14'.
    std::vector<std::pair<uint16_t, uint16_t> > groupBXListIntoRanges(std::vector<uint16_t> const bxList);

    std::string formatBXRangeList(std::vector<std::pair<uint16_t, uint16_t> > const bxRanges);

    // Mapping of LHC beam modes to strings.
    std::string beamModeToString(tcds::definitions::BEAM_MODE const beamMode);

    unsigned int countTrailingZeros(uint32_t const val);

    // Formatting of labels etc.
    std::string formatLPMLabel(unsigned int const lpmNumber,
                               tcds::utils::ConfigurationInfoSpaceHandler const& cfgInfoSpace);
    std::string formatICILabel(unsigned int const lpmNumber,
                               unsigned int const iciNumber,
                               tcds::utils::ConfigurationInfoSpaceHandler const& cfgInfoSpace,
                               bool const labelForExternal);
    std::string formatAPVELabel(unsigned int const lpmNumber,
                                unsigned int const apveNumber,
                                tcds::utils::ConfigurationInfoSpaceHandler const& cfgInfoSpace,
                                bool const labelForExternal);
    std::string formatLabel(std::string const& type,
                            unsigned int const lpmNumber,
                            unsigned int const number,
                            tcds::utils::ConfigurationInfoSpaceHandler const& cfgInfoSpace,
                            bool const labelForExternal);

    // Extract the LAS flashlist catalog from a LAS.
    xdata::Table getLASCatalog(std::string const& lasURL,
                               bool const staticCatalog=true);

    // Extract a single flashlist from a LAS.
    xdata::Table getFlashList(std::string const& lasURL,
                              std::string const& flashlistName);

    // Check if a LAS hosts a certain flashlist.
    bool isFlashListHostedByLAS(std::string const& lasURL,
                                std::string const& flashlistName);
    // Check if a LAS holds data for a certain flashlist.
    bool isFlashListPresentInLAS(std::string const& lasURL,
                                 std::string const& flashlistName);
    bool isFlashListInCatalog(xdata::Table& catalog,
                              std::string const& flashlistName);

    std::vector<std::string>::const_iterator findFlashList(std::string const& flashlistName,
                                                           std::vector<std::string> const& lasURLs);

    void verifyBgoNumber(unsigned int const bgoNumber);
    void verifySequenceNumber(unsigned int const sequenceNumber);

    //----------

    // Instructions on how to interpret the fedEnableMask string can
    // be found in the RCMS documentation:
    // http://cmsdoc.cern.ch/cms/TRIDAS/RCMS/Docs/Manuals/manuals/level1FMFSM_1_10_0.pdf

    // Helper method to parse a fedEnableMask string into an easy map.
    typedef std::map<uint16_t, uint8_t> fedEnableMask;
    fedEnableMask parseFedEnableMask(std::string const& fedEnableMask);

    // Helper methods to figure out what the fedEnableMask is telling
    // us about a certain FED ID.
    bool isFedInFedEnableMask(uint32_t const fedId, std::string const& fedEnableMask);
    bool doesFedDoDAQ(uint32_t const fedId, std::string const& fedEnableMask);
    bool doesFedDoTTS(uint32_t const fedId, std::string const& fedEnableMask);
    bool isFedInRun(uint32_t const fedId, std::string const& fedEnableMask);

    // Helper method to parse a ttcPartitionMap into an easy map.
    typedef std::map<std::string, uint8_t> ttcPartitionMap;
    ttcPartitionMap parseTTCPartitionMap(std::string const& ttcPartitionMap);

    // Helper methods to figure out what the ttcPartitionMap is
    // telling us about a certain partition.
    bool doesPartitionDoTTS(std::string const partitionName, std::string const& ttcPartitionMap);

    // Mapping of BST signal status codes to strings.
    std::string bstStatusToString(tcds::definitions::BST_SIGNAL_STATUS const bstStatus);

  } // namespace utils
} // namespace tcds

#endif // _tcds_utils_Utils_h_
