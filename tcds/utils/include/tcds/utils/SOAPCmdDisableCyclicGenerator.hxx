#include <stdint.h>
#include <string>
#include <vector>

#include "toolbox/string.h"
#include "xcept/Exception.h"
#include "xoap/SOAPName.h"
#include "xoap/SOAPElement.h"

#include "tcds/exception/Exception.h"
#include "tcds/utils/SOAPUtils.h"
#include "tcds/utils/Utils.h"

template <class T>
tcds::utils::SOAPCmdDisableCyclicGenerator<T>::SOAPCmdDisableCyclicGenerator(T& controller) :
  SOAPCmdBase<T>(controller, "DisableCyclicGenerator")
{
}

template <class T>
xoap::MessageReference
tcds::utils::SOAPCmdDisableCyclicGenerator<T>::executeImpl(xoap::MessageReference const& msg)
{
  // Extract the command parameter.
  xoap::SOAPName soapNameNumber("genNumber", "xdaq", "");
  xoap::SOAPElement commandNode = tcds::utils::soap::extractBodyNode(msg);
  std::vector<xoap::SOAPElement> childrenNumber =
    commandNode.getChildElements(soapNameNumber);

  if (childrenNumber.size() != 1)
    {
      XCEPT_RAISE(tcds::exception::SOAPFormatProblem,
                  toolbox::toString("Expected exactly one child element "
                                    "with name 'xdaq:genNumber' "
                                    "in SOAP command '%s' "
                                    "but found %d.",
                                    this->commandName_.c_str(),
                                    childrenNumber.size()));
    }

  // Translate it into a number.
  uint32_t const genNumber =
    tcds::utils::soap::extractSOAPCommandParameterUnsignedInteger(msg, "genNumber");

  // Keep track of history.
  std::string histMsg = toolbox::toString("%s: genNumber = %d",
                                          this->commandName().c_str(),
                                          genNumber);
  this->controller_.appStateInfoSpace_.addHistoryItem(histMsg);

  // Do what we were asked to do.
  this->controller_.getHw().disableCyclicGenerator(genNumber);

  // Send reply.
  xoap::MessageReference reply = tcds::utils::soap::makeCommandSOAPReply(msg);
  return reply;
}
