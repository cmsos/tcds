#ifndef _tcds_utils_XDAQAppBase_h_
#define _tcds_utils_XDAQAppBase_h_

#include <memory>
#include <string>
#include <vector>

#include "toolbox/ActionListener.h"
#include "toolbox/task/TimerListener.h"
#include "xdaq/Application.h"
#include "xdata/ActionListener.h"
#include "xoap/MessageReference.h"

#include "tcds/hwlayer/DeviceBase.h"
#include "tcds/hwlayer/RegisterInfo.h"
#include "tcds/utils/ApplicationStateInfoSpaceHandler.h"
#include "tcds/utils/InfoSpaceUpdaterPlain.h"
#include "tcds/utils/Lock.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/OwnerId.h"
#include "tcds/utils/RegCheckResult.h"
#include "tcds/utils/WebServer.h"

namespace log4cplus {
  class Logger;
}

namespace tcds {
  namespace utils {
    class FSMSOAPParHelper;
  }
}

namespace toolbox {
  class Event;
  namespace task {
    class Timer;
    class TimerEvent;
  }
}

namespace xcept {
  class Exception;
}

namespace xdaq {
  class ApplicationDescriptor;
  class ApplicationStub;
}

namespace xdata {
  class Event;
}

namespace xgi {
  class Input;
  class Output;
}

namespace tcds {
  namespace utils {

    class ConfigurationInfoSpaceHandler;

    class XDAQAppBase :
      public toolbox::task::TimerListener,
      public toolbox::ActionListener,
      public xdaq::Application,
      public xdata::ActionListener
      /**
       * Abstract base class for all TCDS top-level XDAQ applications
       * (or 'Controllers').
       */
    {
      // The Monitor class is a friend so it has access to the
      // monitoringLock_.
      friend class Monitor;
      friend class WebServer;

    public:
      virtual ~XDAQAppBase();

      virtual void actionPerformed(xdata::Event& event);
      virtual void actionPerformed(toolbox::Event& event);

      std::string getFullURL();

      bool isHwLeased() const;
      bool wasHwLeased() const;
      tcds::utils::OwnerId getHwLeaseOwnerId() const;
      tcds::utils::OwnerId getExpiredHwLeaseOwnerId() const;
      void assignHwLease(tcds::utils::OwnerId const& leaseOwnerId);
      void renewHwLease();
      void revokeHwLease(bool const isExpiry=false);

      /**
       * Connect to hardware/release hardware.
       */
      void hwConnect();
      void hwRelease();

      /**
       * Setting parameters straight from SOAP messages is not
       * allowed. This method is only implemented to send a SOAP fault
       * back in case someone tries it anyway.
       */
      xoap::MessageReference parameterSet(xoap::MessageReference msg);

      std::string readHardwareConfiguration();
      std::string readHardwareState();

      // Utility method to determine the application icon file path
      // for an application specified by the ApplicationDescriptor (or
      // the current application).
      std::string buildIconPathName(xdaq::ApplicationDescriptor const* const app=0);

      // Access to inside information.
      virtual ConfigurationInfoSpaceHandler const& getConfigurationInfoSpaceHandler() const;

    protected:
      XDAQAppBase(xdaq::ApplicationStub* const stub,
                  std::unique_ptr<tcds::hwlayer::DeviceBase> hw);

      // Some alarm handling helpers.
      void raiseAlarm(std::string const& baseName,
                      xcept::Exception& err);
      void revokeAlarm(std::string const& baseName,
                       std::string const& reason="");
      std::string buildAlarmName(std::string const& baseName);

      // A helper to take action upon monitoring problems. Not all
      // applications may take these equally seriously, depending on
      // their core business (e.g., monitoring vs. control).
      virtual void handleMonitoringProblem(std::string const& problemDesc);

      // A little helper method for HTML action handling.
      void redirect(xgi::Input* in, xgi::Output* out, std::string const& msg="");

      // Several methods for SOAP messaging back and forth.
      xoap::MessageReference executeSOAPCommand(xoap::MessageReference& cmd,
                                                xdaq::ApplicationDescriptor const& dest) const;
      xoap::MessageReference sendSOAP(xoap::MessageReference& msg,
                                      xdaq::ApplicationDescriptor const& dest) const;
      xoap::MessageReference postSOAP(xoap::MessageReference& msg,
                                      xdaq::ApplicationDescriptor const& destination) const;

      virtual void setupInfoSpaces();

      virtual void hwConnectImpl() = 0;
      virtual void hwReleaseImpl() = 0;

      /**
       * Access the hardware pointer as DeviceBase&.
       */
      bool haveHw() const;
      virtual tcds::hwlayer::DeviceBase& getHw() const;

      InfoSpaceUpdaterPlain appStateInfoSpaceUpdater_;
      ApplicationStateInfoSpaceHandler appStateInfoSpace_;
      /**
       * @note
       *
       * The following two variables should be instantiated by each
       * and every inheriting class in the constructor.
       * So even though they are pointers, we should be able to simply
       * use them everywhere outside the constructor.
       */
      std::unique_ptr<ConfigurationInfoSpaceHandler> cfgInfoSpaceP_;
      std::unique_ptr<tcds::hwlayer::DeviceBase> hwP_;

      log4cplus::Logger& logger_;
      Monitor monitor_;
      WebServer webServer_;

      // A string identifying the RunControl session owning the
      // hardware associated with this control application.
      tcds::utils::OwnerId hwLeaseOwnerId_;
      // And the previous one, in case it expired...
      tcds::utils::OwnerId expiredHwLeaseOwnerId_;

      // A flag to show that the application is in 'maintenance mode.'
      bool maintenanceMode_;

      // A 'stashed' version of the true lease owner(s) for use in
      // maintenance mode.
      tcds::utils::OwnerId hwLeaseOwnerIdPrev_;
      tcds::utils::OwnerId expiredHwLeaseOwnerIdPrev_;
      // And a little helper to keep track of lease manipulations
      // during the time spent in maintenance mode. (Ugly...)
      bool shouldHaveALeaseOwner_;

      // A lock to allow scheduling between the monitoring updates
      // (which potentially access the hardware) and other actions on
      // the hardware.
      Lock monitoringLock_;

      // TODO TODO TODO
      // Maybe all the hardware-level state transition methods should
      // be moved here too.
      virtual tcds::hwlayer::DeviceBase::RegContentsVec hwReadHardwareConfiguration() const;
      // TODO TODO TODO end

      virtual tcds::utils::RegCheckResult isRegisterAllowed(tcds::hwlayer::RegisterInfo const& regInfo) const;

      // Several methods involved in loading parameters from an FSM
      // SOAP command message into the ConfigurationInfoSpace.
      std::vector<FSMSOAPParHelper> expectedFSMSoapPars(std::string const& commandName) const;
      virtual std::vector<FSMSOAPParHelper> expectedFSMSoapParsImpl(std::string const& commandName) const;
      void loadSOAPCommandParameters(xoap::MessageReference const& msg);
      virtual void loadSOAPCommandParametersImpl(xoap::MessageReference const& msg);
      void loadSOAPCommandParameter(xoap::MessageReference const& msg,
                                    FSMSOAPParHelper const& param);
      virtual void loadSOAPCommandParameterImpl(xoap::MessageReference const& msg,
                                                FSMSOAPParHelper const& param);

    private:
      // This method renews the hardware lease from a SOAP command.
      xoap::MessageReference renewHwLease(xoap::MessageReference msg);

      // Several methods to handle entering and releasing of
      // 'maintenance mode.'
      xoap::MessageReference maintenanceModeOn(xoap::MessageReference msg);
      xoap::MessageReference maintenanceModeOff(xoap::MessageReference msg);

      void switchMaintenanceMode(bool const on, tcds::utils::OwnerId const& maintainerId);
      void setMaintenanceMode(bool const on);

      virtual std::string readHardwareConfigurationImpl();
      virtual std::string readHardwareStateImpl();

      // This method is used to auto-expire the hardware lease after a
      // given time.
      void timeExpired(toolbox::task::TimerEvent& event);

      /**
       * A filtering method to select which part of the hardware
       * configuration is exposed to the outside world via
       * hwReadHardwareConfiguration().
       */
      tcds::hwlayer::RegisterInfo::RegInfoVec filterRegInfo(tcds::hwlayer::RegisterInfo::RegInfoVec const& regInfosIn) const;

      // The auto-expiry timer for the hardware lease.
      std::string timerName_;
      std::string const timerJobName_;
      toolbox::task::Timer* hwLeaseExpiryTimerP_;

      // The name of the XDAQ InfoSpace that alarms live in, and where
      // the Sentinel probe picks them up.
      std::string const sentinelInfoSpaceName_;

    };

  } // namespace utils
} // namespace tcds

#endif // _tcds_utils_XDAQAppBase_h_
