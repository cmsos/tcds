#ifndef _tcds_utils_Monitor_h_
#define _tcds_utils_Monitor_h_

#include <memory>
#include <string>
#include <sys/types.h>
#include <tr1/unordered_map>
#include <utility>
#include <vector>

#include "toolbox/ActionListener.h"
#include "toolbox/EventDispatcher.h"
#include "toolbox/exception/Listener.h"
#include "toolbox/task/TimerListener.h"
#include "toolbox/TimeVal.h"

#include "tcds/utils/MonitorItem.h"
#include "tcds/utils/TCDSObject.h"
#include "tcds/utils/Uncopyable.h"

namespace log4cplus {
  class Logger;
}

namespace toolbox {
  namespace task {
    class Timer;
    class TimerEvent;
  }
}

namespace xcept {
  class Exception;
}

namespace tcds {
  namespace utils {

    class InfoSpaceHandler;
    class XDAQAppBase;

    class Monitor :
      public toolbox::task::TimerListener,
      public toolbox::exception::Listener,
      public TCDSObject,
      private Uncopyable
    /**
     * Driving class behind all monitoring going on in the TCDS XDAQ
     * framework. Access from 'outside' is via the WebServer
     * class. This class is uncopyable since it holds all kinds of
     * pointers to InfoSpaces.
     */
    {

    public:
      Monitor(tcds::utils::XDAQAppBase& xdaqApp, std::string const& name="");
      ~Monitor();

      void addActionListener(toolbox::ActionListener* const listener);
      void removeActionListener(toolbox::ActionListener* const listener);

      void startMonitoring();
      void stopMonitoring();

      typedef std::vector<std::pair<std::string, std::string> > StringPairVector;
      StringPairVector getFormattedItemSet(std::string const& itemSetName) const;
      StringPairVector getItemSetDocStrings(std::string const& itemSetName) const;

      /**
       * Add the item with itemName to the itemset with itemSetName.
       */
      void addItem(std::string const& itemSetName,
                   std::string const& itemName,
                   std::string const& itemDesc,
                   InfoSpaceHandler* const infoSpaceHandler,
                   std::string const& docString="");

      /**
         Create a new itemset with the name given.
       */
      void newItemSet(std::string const& name);

      // The toolbox::exception::Listener callback.
      virtual void onException(xcept::Exception& err);

     protected:
      log4cplus::Logger& logger_;

      void addInfoSpace(InfoSpaceHandler* const infoSpace);

      void timeExpired(toolbox::task::TimerEvent& event);

    private:
      static const useconds_t kLoopRelaxTime = 100;

      // This is the name of any monitoring alarms raised.
      std::string const alarmName_;
      // And this flag is used to keep track of existing alarm
      // conditions. Just to ease the book keeping, really.
      // NOTE: This is mutable because it 'hides' an internal
      // implementation detail. The Monitor does not change by the
      // presence/absence of the alarm.
      mutable bool isInAlarm_;
      mutable std::string alarmDescription_;
      std::string timerName_;

      std::tr1::unordered_map<std::string, InfoSpaceHandler*> infoSpaceMap_;
      typedef std::tr1::unordered_map<std::string,
                                      std::vector<std::pair<std::string,
                                                            tcds::utils::MonitorItem> > > MonitorItemMap;
      MonitorItemMap itemSets_;
      std::unique_ptr<toolbox::task::Timer> timerP_;

      // The start time of the monitor. Just for a simplistic estimate
      // of the application uptime.
      toolbox::TimeVal timeStart_;

      /**
       * The main update method: crawls over all registered InfoSpaces
       * and updates all of them.
       */
      void updateAllInfoSpaces();

      void handleException(xcept::Exception& err);

      void raiseMonitoringAlarm(std::string const& problemDesc) const;
      void revokeMonitoringAlarm() const;

      toolbox::EventDispatcher dispatcher_;

    };

  } // namespace utils
} // namespace tcds

#endif // _tcds_utils_Monitor_h_
