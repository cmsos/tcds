#ifndef _tcds_utils_SOAPCmdEnableBChannel_h_
#define _tcds_utils_SOAPCmdEnableBChannel_h_

#include "xoap/MessageReference.h"

#include "tcds/utils/SOAPCmdBase.h"

namespace tcds {
  namespace utils {

    class XDAQAppBase;

    template <class T>
      class SOAPCmdEnableBChannel : public SOAPCmdBase<T>
      {

      public:
        SOAPCmdEnableBChannel(T& controller);

        /**
         * Enables the specified B-channel. Bound to the SOAP command
         * EnableBChannel.
         * - SOAP command parameters:
         *   - either one of the following two
         *     -  xsd:string `bgoName': the B-go name (e.g., 'Bgo1', 'Start').
         *     -  xsd:unsignedInt `bgoNumber': the B-go number (e.g., '1').
         *   - xsd:string `bchannelMode': the B-channel mode, 'single' or 'double'.
         * - SOAP command return value: none.
         */
        virtual xoap::MessageReference executeImpl(xoap::MessageReference const& msg);

      };

  } // namespace utils
} // namespace tcds

#include "tcds/utils/SOAPCmdEnableBChannel.hxx"

#endif // _tcds_utils_SOAPCmdEnableBChannel_h_
