#ifndef _tcds_utils_WebServer_h
#define _tcds_utils_WebServer_h

#include <cstddef>
#include <string>
#include <vector>

#include "toolbox/ActionListener.h"
#include "xgi/framework/UIManager.h"

#include "tcds/utils/Layout.h"
#include "tcds/utils/Lock.h"
#include "tcds/utils/TCDSObject.h"
#include "tcds/utils/Uncopyable.h"

namespace log4cplus {
  class Logger;
}

namespace xgi {
  class Input;
  class Output;
}

namespace tcds {
  namespace utils {

    class Monitor;
    class WebTab;
    class XDAQAppBase;

    class WebServer :
      public xgi::framework::UIManager,
      public toolbox::ActionListener,
      public TCDSObject,
      private Uncopyable

      /**
       * Webserver class: allows access to all monitorable content in
       * the TCDS XDAQ applications.
       *
       * The HTML produced by this class is based on HTML5 BoilerPlate
       * (http://html5boilerplate.com/), via a bit of playing and
       * creative copy-paste from initializr
       * (http://www.initializr.com/).
       */
    {

    public:
      WebServer(tcds::utils::XDAQAppBase& xdaqApp,
                tcds::utils::Monitor& monitor);
      ~WebServer();

      void actionPerformed(toolbox::Event& event);

      void registerTab(std::string const& name,
                       std::string const& description,
                       size_t const numColumns);
      template <class O>
      void registerWebObject(std::string const& name,
                             std::string const& description,
                             Monitor const& monitor,
                             std::string const& itemSetName,
                             std::string const& tabName,
                             size_t const colSpan=1);
      void registerTable(std::string const& name,
                         std::string const& description,
                         Monitor const& monitor,
                         std::string const& itemSetName,
                         std::string const& tabName,
                         size_t const colSpan=1);
      void registerSpacer(std::string const& name,
                          std::string const& description,
                          Monitor const& monitor,
                          std::string const& itemSetName,
                          std::string const& tabName,
                          size_t const colSpan=1);

      void jsonUpdate(xgi::Input* const in, xgi::Output* const out);
      void monitoringWebPage(xgi::Input* const in, xgi::Output* const out);

    private:
      /**
       * Returns the class name of the owner XDAQ application (without
       * namespaces).
       */
      std::string getApplicationName() const;

      std::string getServiceName() const;

      WebTab& getTab(std::string const& tabName) const;
      bool tabExists(std::string const& tabName) const;

      void printTabs(xgi::Output* const out) const;

      void serveJSONUpdate(xgi::Input* const in, xgi::Output* const out) const;
      void updateJSONContents() const;
      void monitoringWebPageCore(xgi::Input* const in, xgi::Output* const out) const;

      bool isJSONContentsEmpty() const;

      tcds::utils::Monitor& monitor_;
      log4cplus::Logger& logger_;
      std::vector<WebTab*> tabs_;
      tcds::utils::Layout layout_;

      mutable std::string jsonContents_;
      mutable tcds::utils::Lock lock_;

    };

  } // namespace utils
} // namespace tcds

#include "tcds/utils/WebServer.hxx"

#endif // _tcds_utils_WebServer_h
