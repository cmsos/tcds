#ifndef _tcds_utils_SOAPCmdDisableTTCSpy_h_
#define _tcds_utils_SOAPCmdDisableTTCSpy_h_

#include "xoap/MessageReference.h"

#include "tcds/utils/SOAPCmdBase.h"

namespace tcds {
  namespace utils {

    class XDAQAppBase;

    template <class T>
      class SOAPCmdDisableTTCSpy : public SOAPCmdBase<T>
    {

    public:
      SOAPCmdDisableTTCSpy(T& controller);

      /**
       * Disables the TTCSpy logger. Bound to the SOAP command
       * DisableTTCSpy.
       * - SOAP command parameters: none.
       * - SOAP command return value: none.
       */
      virtual xoap::MessageReference executeImpl(xoap::MessageReference const& msg);

    };

  } // namespace utils
} // namespace tcds

#include "tcds/utils/SOAPCmdDisableTTCSpy.hxx"

#endif // _tcds_utils_SOAPCmdDisableTTCSpy_h_
