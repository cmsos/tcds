#include "log4cplus/loggingmacros.h"

#include "toolbox/string.h"
#include "xcept/Exception.h"
#include "xdaq/NamespaceURI.h"
#include "xoap/Method.h"

#include "tcds/exception/Exception.h"
#include "tcds/utils/OwnerId.h"
#include "tcds/utils/SOAPUtils.h"

template <class T>
tcds::utils::SOAPCmdBase<T>::SOAPCmdBase(T& controller,
                                         std::string const& commandName,
                                         bool const requiresHwLease) :
  controller_(controller),
  commandName_(commandName),
  requiresHwLease_(requiresHwLease)
{
  // Bind this as SOAP command.
  xoap::deferredbind(&(this->controller_), this, &SOAPCmdBase<T>::execute,
                     commandName, XDAQ_NS_URI);
}

template <class T>
tcds::utils::SOAPCmdBase<T>::~SOAPCmdBase()
{
}

template <class T>
std::string
tcds::utils::SOAPCmdBase<T>::commandName() const
{
  return commandName_;
}

template <class T>
xoap::MessageReference
tcds::utils::SOAPCmdBase<T>::execute(xoap::MessageReference msg)
{
  xoap::MessageReference reply;
  bool executeCommand = false;

  tcds::utils::OwnerId requestorId;
  if (requiresHwLease_)
    {
      // First check if the request comes from the hardware owner or
      // not.
      requestorId = tcds::utils::soap::extractSOAPCommandOwnerId(msg);
      if (!requestorId.isValid())
        {
          // No requestor ID -> send error back.
          std::string msgBase =
            toolbox::toString("SOAP command '%s' not allowed "
                              "without requestor ID.",
                              commandName().c_str());
          LOG4CPLUS_ERROR(controller_.getApplicationLogger(), msgBase);
          XCEPT_DECLARE(tcds::exception::FSMTransitionProblem, top, msgBase);
          controller_.notifyQualified("error", top);
          tcds::utils::soap::SOAPFaultCodeType faultCode =
            tcds::utils::soap::SOAPFaultCodeReceiver;
          std::string faultString = msgBase;
          reply = tcds::utils::soap::makeSOAPFaultReply(&controller_,
                                                        msg,
                                                        faultCode,
                                                        faultString);

          // Keep track of history.
          std::string const histMsg =
            toolbox::toString("Ignored SOAP command '%s' without requestor ID",
                              commandName().c_str());
          controller_.appStateInfoSpace_.addHistoryItem(histMsg);
        }
      else if (!controller_.isHwLeased())
        {
          // Hardware is not owned by anyone -> send error back.
          std::string msgBase =
            toolbox::toString("SOAP command '%s' not allowed "
                              "without valid hardware lease.",
                              commandName().c_str());
          LOG4CPLUS_ERROR(controller_.getApplicationLogger(), msgBase);
          XCEPT_DECLARE(tcds::exception::FSMTransitionProblem, top, msgBase);
          controller_.notifyQualified("error", top);
          tcds::utils::soap::SOAPFaultCodeType faultCode =
            tcds::utils::soap::SOAPFaultCodeReceiver;
          std::string faultString = msgBase;
          reply = tcds::utils::soap::makeSOAPFaultReply(&controller_,
                                                        msg,
                                                        faultCode,
                                                        faultString);

          // Keep track of history.
          std::string const histMsg =
            toolbox::toString("Ignored SOAP command '%s' from %s "
                              "because it requires a valid hardware lease",
                              commandName().c_str(),
                              requestorId.asString().c_str());
          controller_.appStateInfoSpace_.addHistoryItem(histMsg);
        }
      else
        {
          tcds::utils::OwnerId const leaseOwnerId = controller_.getHwLeaseOwnerId();
          if (requestorId != leaseOwnerId)
            {
              // No match -> send error back.
              std::string msgBase =
                toolbox::toString("SOAP command '%s' not allowed: "
                                  "hardware lease owned by %s.",
                                  commandName().c_str(),
                                  leaseOwnerId.asString().c_str());
              LOG4CPLUS_ERROR(controller_.getApplicationLogger(), msgBase);
              XCEPT_DECLARE(tcds::exception::FSMTransitionProblem, top, msgBase);
              controller_.notifyQualified("error", top);
              tcds::utils::soap::SOAPFaultCodeType faultCode =
                tcds::utils::soap::SOAPFaultCodeReceiver;
              std::string faultString = msgBase;
              reply = tcds::utils::soap::makeSOAPFaultReply(&controller_,
                                                            msg,
                                                            faultCode,
                                                            faultString);

              // Keep track of history.
              std::string const histMsg =
                toolbox::toString("Ignored SOAP command '%s' from %s since hardware is owned by %s",
                                  commandName().c_str(),
                                  requestorId.asString().c_str(),
                                  leaseOwnerId.asString().c_str());
              controller_.appStateInfoSpace_.addHistoryItem(histMsg);
            }
          else
            {
              executeCommand = true;
            }
        }
    }
  else
    {
      // No valid hardware lease required.
      executeCommand = true;
    }

  // The command has been accepted. Renew the hardware lease and
  // execute the command.
  if (executeCommand)
    {
      if (requiresHwLease_)
        {
          controller_.renewHwLease();
        }

      try
        {
          // Keep track of history.
          std::string const histMsg =
            toolbox::toString("Executing SOAP command '%s'",
                              commandName().c_str());
          controller_.appStateInfoSpace_.addHistoryItem(histMsg);
          // Execute command.
          reply = executeImpl(msg);
        }
      catch (tcds::exception::Exception& err)
        {
          tcds::utils::soap::SOAPFaultCodeType const faultCode =
            tcds::utils::soap::SOAPFaultCodeReceiver;
          std::string const msgBase =
            toolbox::toString("Failed to execute SOAP command '%s'.",
                              commandName().c_str());
          std::string const faultString = msgBase;
          std::string const faultDetail = err.message().c_str();
          reply = tcds::utils::soap::makeSOAPFaultReply(&controller_,
                                                        msg,
                                                        faultCode,
                                                        faultString,
                                                        faultDetail);

          // Keep track of history.
          std::string histMsg = "";
          if (requestorId.isValid())
            {
              histMsg = toolbox::toString("Failed to execute SOAP command '%s' from %s: '%s'",
                                          commandName().c_str(),
                                          requestorId.asString().c_str(),
                                          faultDetail.c_str());
            }
          else
            {
              histMsg = toolbox::toString("Failed to execute SOAP command '%s': '%s'",
                                          commandName().c_str(),
                                          faultDetail.c_str());
            }
          controller_.appStateInfoSpace_.addHistoryItem(histMsg);
        }
    }

  return reply;
}
