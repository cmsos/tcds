#ifndef _tcds_utils_AppHistItem_h_
#define _tcds_utils_AppHistItem_h_

#include <string>
#include <vector>

#include "toolbox/TimeVal.h"

namespace tcds {
  namespace utils {

    class AppHistItem
    {

    public:
      AppHistItem(std::string const& msg);
      ~AppHistItem();
      void addOccurrence();

      // This method returns the first timestamp we have: the
      // timestamp of the first occurrence.
      toolbox::TimeVal timestamp() const;
      // This method returns the last timestamp we have: the timestamp
      // of the last occurrence.
      toolbox::TimeVal timestampLastOccurrence() const;

      std::string message() const;
      unsigned int count() const;

    private:
      std::vector<toolbox::TimeVal> timestamps_;
      std::string const msg_;
      unsigned int count_;

    };

  } // namespace utils
} // namespace tcds

#endif // _tcds_utils_AppHistItem_h_
