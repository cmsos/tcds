#ifndef _tcds_utils_HwInfoSpaceUpdater_h_
#define _tcds_utils_HwInfoSpaceUpdater_h_

#include "tcds/utils/HwInfoSpaceUpdaterBase.h"

namespace tcds {
  namespace hwlayer {
    class DeviceBase;
  }
}

namespace tcds {
  namespace utils {
    class InfoSpaceHandler;
    class InfoSpaceItem;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace utils {

    class HwInfoSpaceUpdater : public tcds::utils::HwInfoSpaceUpdaterBase
    {

    public:
      HwInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                         tcds::hwlayer::DeviceBase const& hw);
      virtual ~HwInfoSpaceUpdater();

    };

  } // namespace utils
} // namespace tcds

#endif // _tcds_utils_HwInfoSpaceUpdater_h_
