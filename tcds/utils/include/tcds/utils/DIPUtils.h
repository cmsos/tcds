#ifndef _tcds_utils_DIPUtils_h_
#define _tcds_utils_DIPUtils_h_

#include <map>
#include <string>
#include <utility>
#include <vector>

#include "xoap/MessageReference.h"

namespace tcds {
  namespace utils {

    class DIPDP;

    // From here:
    //   https://readthedocs.web.cern.ch/display/ICKB/DIP+FAQ
    enum DIP_QUAL {
      DIP_QUAL_UNINITIALIZED = 1,
      DIP_QUAL_BAD = 0,
      DIP_QUAL_GOOD = 3,
      DIP_QUAL_UNCERTAIN = 2
    };

    std::map<std::pair<bool, bool>, DIP_QUAL> dipQualMap();
    DIP_QUAL dipQualFromUserBits(bool const userBit1, bool const userBit2);
    std::pair<bool, bool> userBitsFromDipQual(DIP_QUAL const dipQual);

    std::map<DIP_QUAL, std::string> dipQualStringMap();
    std::string dipQualToString(DIP_QUAL const dipQual);

    xoap::MessageReference makePSXDPGetSOAPCmd(std::string const& dpName);
    xoap::MessageReference makePSXDPSetSOAPCmd(std::string const& dpName,
                                               std::string const& dpValue,
                                               tcds::utils::DIP_QUAL const dpQual);
    xoap::MessageReference makePSXDPSetSOAPCmd(std::vector<tcds::utils::DIPDP> const& dpInfo);

    std::string extractPSXDPGetReply(xoap::MessageReference& msg,
                                     std::string const& dpName);
    std::string extractPSXDPSetReply(xoap::MessageReference& msg,
                                     std::string const& dpName);


  } // namespace utils
} // namespace tcds

#endif // _tcds_utils_DIPUtils_h_
