#ifndef _tcds_utils_XDAQAppWithFSMBase_h_
#define _tcds_utils_XDAQAppWithFSMBase_h_

#include <memory>
#include <string>

#include "toolbox/Event.h"
#include "xoap/MessageReference.h"

#include "tcds/utils/FSM.h"
#include "tcds/utils/XDAQAppBase.h"

namespace tcds {
  namespace hwlayer {
    class DeviceBase;
  }
}

namespace xdaq {
  class ApplicationStub;
}

namespace tcds {
  namespace utils {

    class XDAQAppWithFSMBase : public XDAQAppBase
    {

      // NOTE: All TCDS control applications (at least the ones that
      // have a state machine) internally contain the same FSM
      // (implemented in XDAQAppWithFSMBase). The difference between
      // classes like XDAQAppWithFSMAutomatic, XDAQAppWithFSMBasic,
      // etc. lies in the fact that different transitions are exposed
      // to the outside world (i.e., bound to SOAP commands).

      /**
       * The FSM is a friend so it has access to the
       * appStateInfoSpace_;
       */
      friend class FSM;

    public:
      virtual ~XDAQAppWithFSMBase();

      /**
       * Access to the state-machine transitions for ourselves.
       */
      void coldReset();
      void configure();
      void enable();
      void fail();
      void halt();
      void pause();
      void reconfigure();
      void resume();
      void stop();

      std::string getCurrentStateName() const;

      /**
       * These are the usual state transitions.
       */
      void coldResetAction(toolbox::Event::Reference event);
      void configureAction(toolbox::Event::Reference event);
      void enableAction(toolbox::Event::Reference event);
      void failAction(toolbox::Event::Reference event);
      void haltAction(toolbox::Event::Reference event);
      void pauseAction(toolbox::Event::Reference event);
      void resumeAction(toolbox::Event::Reference event);
      void stopAction(toolbox::Event::Reference event);

      /**
       * These two are special TTC-action state transitions from
       * Paused via an intermediate action state back to Paused.
       */
      void ttcHardResetAction(toolbox::Event::Reference event);
      void ttcResyncAction(toolbox::Event::Reference event);

      void zeroAction(toolbox::Event::Reference event);

      xoap::MessageReference changeState(xoap::MessageReference msg);

    protected:
      /**
       * Abstract class, so protected constructor.
       */
      XDAQAppWithFSMBase(xdaq::ApplicationStub* const stub,
                         std::unique_ptr<tcds::hwlayer::DeviceBase> hw);

      virtual void coldResetActionImpl(toolbox::Event::Reference event);
      virtual void configureActionImpl(toolbox::Event::Reference event);
      virtual void enableActionImpl(toolbox::Event::Reference event);
      virtual void failActionImpl(toolbox::Event::Reference event);
      virtual void haltActionImpl(toolbox::Event::Reference event);
      virtual void pauseActionImpl(toolbox::Event::Reference event);
      virtual void resumeActionImpl(toolbox::Event::Reference event);
      virtual void stopActionImpl(toolbox::Event::Reference event);

      virtual void ttcHardResetActionImpl(toolbox::Event::Reference event);
      virtual void ttcResyncActionImpl(toolbox::Event::Reference event);

      virtual void zeroActionImpl(toolbox::Event::Reference event);

      virtual xoap::MessageReference changeStateImpl(xoap::MessageReference msg);

      /**
       * The hardware-level equivalents of the state transitions.
       */
      void hwColdReset();
      virtual void hwColdResetImpl();
      void hwConfigure();
      virtual void hwConfigureImpl();
      // BUG BUG BUG
      // Missing a few here.
      // BUG BUG BUG end

      /**
       * Some configuration helpers. These methods are called just
       * before and just after applying a configuration to the
       * hardware.
       */
      void hwCfgInitialize();
      void hwCfgFinalize();
      virtual void hwCfgInitializeImpl();
      virtual void hwCfgFinalizeImpl();

    private:
      FSM fsm_;

    };

  } // namespace utils
} // namespace tcds

#endif // _tcds_utils_XDAQAppWithFSMBase_h_
