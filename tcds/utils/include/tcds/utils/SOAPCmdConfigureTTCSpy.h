#ifndef _tcds_utils_SOAPCmdConfigureTTCSpy_h_
#define _tcds_utils_SOAPCmdConfigureTTCSpy_h_

#include "xoap/MessageReference.h"

#include "tcds/utils/SOAPCmdBase.h"

namespace tcds {
  namespace utils {

    class XDAQAppBase;

    template <class T>
      class SOAPCmdConfigureTTCSpy : public SOAPCmdBase<T>
    {

    public:
      SOAPCmdConfigureTTCSpy(T& controller);

      /**
       * Configures the TTCSpy trigger mask. Bound to the SOAP command
       * ConfigureTTCSpy.
       * - SOAP command parameters:
       *   - xsd:string 'loggingMode':
       *     The logging mode. Either 'LogOnly' or 'LogAllExcept'.
       *   - xsd:string 'triggerTermCombinationOperator':
       *     The logic operator used to combine the various trigger
       *     terms. Either 'OR' or 'AND'.
       *   - xsd:boolean 'brcBC0':
       *     Trigger on broadcast B-commands with the
       *     event-counter-reset bit set.
       *   - xsd:boolean 'brcEC0':
       *     Trigger on broadcast B-commands with the
       *     event-counter-reset bit set.
       *   - xsd:unsignedInt 'brcVal0':
       *     Trigger on broadcast B-commands with this pattern in the
       *     eight data-bits.
       *   - xsd:unsignedInt 'brcVal1':
       *     Trigger on broadcast B-commands with this pattern in the
       *     eight data-bits.
       *   - xsd:unsignedInt 'brcDDDD':
       *     Trigger on broadcast B-commands with this pattern in the
       *     four user-bits.
       *   - xsd:unsignedInt 'brcTT':
       *     Trigger on broadcast B-commands with this pattern in the
       *     two test-bits.
       *   - xsd:boolean 'brcDDDDAll'
       *     Trigger on all broadcast B-commands with non-zero
       *     user-bits.
       *   - xsd:boolean 'brcTTAll':
       *     Trigger on all broadcast B-commands with non-zero
       *     test-bits.
       *   - xsd:boolean 'brcAll':
       *     Trigger on all broadcast B-commands.
       *   - xsd:boolean 'addAll':
       *     Trigger on all addressed B-commands.
       *   - xsd:boolean 'l1a'
       *     Trigger on L1As.
       *   - xsd:boolean 'brcZeroData':
       *     Trigger on all broadcast B-commands with 0x0 data payload.
       *   - xsd:boolean 'adrZeroData':
       *     Trigger on all addressed B-commands with 0x0 data payload.
       *   - xsd:boolean 'errCom':
       *     Trigger on transmission errors.
       * - SOAP command return value: none.
       *
       * NOTE: This command has to be able to be executed from
       * 'random' web browsers, so it does not require a requestor
       * ID.
       */
      virtual xoap::MessageReference executeImpl(xoap::MessageReference const& msg);

    };

  } // namespace utils
} // namespace tcds

#include "tcds/utils/SOAPCmdConfigureTTCSpy.hxx"

#endif // _tcds_utils_SOAPCmdConfigureTTCSpy_h_
