#ifndef _tcds_utils_XDAQAppWithFSMAutomatic_h_
#define _tcds_utils_XDAQAppWithFSMAutomatic_h_

#include <memory>
#include <string>

#include "tcds/utils/FSMDriver.h"
#include "tcds/utils/XDAQAppWithFSMBase.h"

namespace tcds {
  namespace hwlayer {
    class DeviceBase;
  }
}

namespace toolbox {
  class Event;
}

namespace xcept {
  class Exception;
}

namespace xdaq {
  class ApplicationStub;
}

namespace tcds {
  namespace utils {

    class XDAQAppWithFSMAutomatic : public XDAQAppWithFSMBase
    {

      friend class FSMDriver;

      // NOTE: All TCDS control applications (at least the ones that
      // have a state machine) internally contain the same FSM
      // (implemented in XDAQAppWithFSMBase). The difference between
      // classes like XDAQAppWithFSMAutomatic, XDAQAppWithFSMBasic,
      // etc. lies in the fact that different transitions are exposed
      // to the outside world (i.e., bound to SOAP commands).

    public:
      virtual ~XDAQAppWithFSMAutomatic();

      virtual void actionPerformed(toolbox::Event& event);

    protected:
      /**
       * Abstract class, so protected constructor.
       */
      XDAQAppWithFSMAutomatic(xdaq::ApplicationStub* const stub,
                              std::unique_ptr<tcds::hwlayer::DeviceBase> hw);

      void raiseAlarm(std::string const& name,
                      xcept::Exception& err);
      void revokeAlarm(std::string const& name);

      virtual void handleMonitoringProblem(std::string const& problemDesc);

    private:
      FSMDriver fsmDriver_;

    };

  } // namespace utils
} // namespace tcds

#endif // _tcds_utils_XDAQAppWithFSMAutomatic_h_
