#ifndef _tcds_utils_Definitions_h_
#define _tcds_utils_Definitions_h_

#include <stdint.h>
#include <string>

namespace tcds {
  namespace definitions {

    // LHC-related definitions.
    unsigned int const kFirstBX = 1;
    unsigned int const kLastBX = 3564;
    unsigned int const kNumBXPerOrbit = kLastBX - kFirstBX + 1;
    unsigned int const kLHCOrbitFreq = 11246;

    // LHC beam-modes.
    enum BEAM_MODE {
      BEAM_MODE_UNKNOWN = 0,
      BEAM_MODE_NO_MODE = 1,
      BEAM_MODE_SETUP = 2,
      BEAM_MODE_INJECTION_PROBE_BEAM = 3,
      BEAM_MODE_INJECTION_SETUP_BEAM = 4,
      BEAM_MODE_INJECTION_PHYSICS_BEAM = 5,
      BEAM_MODE_PREPARE_RAMP = 6,
      BEAM_MODE_RAMP = 7,
      BEAM_MODE_FLAT_TOP = 8,
      BEAM_MODE_SQUEEZE = 9,
      BEAM_MODE_ADJUST = 10,
      BEAM_MODE_STABLE_BEAMS = 11,
      BEAM_MODE_UNSTABLE_BEAMS = 12,
      BEAM_MODE_BEAM_DUMP = 13,
      BEAM_MODE_RAMP_DOWN = 14,
      BEAM_MODE_RECOVERY = 15,
      BEAM_MODE_INJECT_AND_DUMP = 16,
      BEAM_MODE_CIRCULATE_AND_DUMP = 17,
      BEAM_MODE_ABORT = 18,
      BEAM_MODE_CYCLING = 19,
      BEAM_MODE_BEAM_DUMP_WARNING = 20,
      BEAM_MODE_NO_BEAM = 21,
    };
    int const kBeamModeMin = BEAM_MODE_NO_MODE;
    int const kBeamModeMax = BEAM_MODE_NO_BEAM;

    // Definitions related to B-gos.
    enum BGO_NUM {
      // Original CMS B-gos.
      BGO_BGO0=0,
      BGO_BC0=1,
      BGO_TESTENABLE=2,
      BGO_PRIVATEGAP=3,
      BGO_PRIVATEORBIT=4,
      BGO_RESYNC=5,
      BGO_HARDRESET=6,
      BGO_EC0=7,
      BGO_OC0=8,
      BGO_START=9,
      BGO_STOP=10,
      BGO_STARTOFGAP=11,
      BGO_BGO12=12,
      BGO_WARNINGTESTENABLE=13,
      BGO_BGO14=14,
      BGO_BGO15=15,
      // New, TCDS-era B-gos.
      BGO_BGO16=16,
      BGO_BGO17=17,
      BGO_BGO18=18,
      BGO_BGO19=19,
      BGO_BGO20=20,
      BGO_BGO21=21,
      BGO_BGO22=22,
      BGO_BGO23=23,
      BGO_BGO24=24,
      BGO_BGO25=25,
      BGO_BGO26=26,
      BGO_BGO27=27,
      BGO_BGO28=28,
      BGO_BGO29=29,
      BGO_BGO30=30,
      BGO_BGO31=31,
      BGO_BGO32=32,
      BGO_BGO33=33,
      BGO_BGO34=34,
      BGO_BGO35=35,
      BGO_BGO36=36,
      BGO_BGO37=37,
      BGO_BGO38=38,
      BGO_BGO39=39,
      BGO_BGO40=40,
      BGO_BGO41=41,
      BGO_BGO42=42,
      BGO_BGO43=43,
      BGO_BGO44=44,
      BGO_BGO45=45,
      BGO_BGO46=46,
      BGO_BGO47=47,
      BGO_BGO48=48,
      BGO_BGO49=49,
      BGO_BGO50=50,
      BGO_BGO51=51,
      BGO_BGO52=52,
      BGO_BGO53=53,
      BGO_BGO54=54,
      BGO_BGO55=55,
      BGO_BGO56=56,
      BGO_BGO57=57,
      BGO_BGO58=58,
      BGO_BGO59=59,
      BGO_BGO60=60,
      BGO_BGO61=61,
      BGO_BGO62=62,
      BGO_BGO63=63
    };
    int const kBgoNumMin = BGO_BGO0;
    int const kBgoNumMax = BGO_BGO63;
    int const kBgoNumOriMin = BGO_BGO0;
    int const kBgoNumOriMax = BGO_BGO15;
    int const kBgoNumNewMin = BGO_BGO16;
    int const kBgoNumNewMax = BGO_BGO63;

    // Sequence numbers.
    enum SEQUENCE_NUM {
      SEQUENCE_START=0,
      SEQUENCE_STOP=1,
      SEQUENCE_PAUSE=2,
      SEQUENCE_RESUME=3,
      SEQUENCE_RESYNC=4,
      SEQUENCE_HARDRESET=5,
      SEQUENCE_CALIBRATION = 6,
      SEQUENCE_SEQUENCE7 = 7,
      SEQUENCE_SEQUENCE8 = 8,
      SEQUENCE_SEQUENCE9 = 9,
      SEQUENCE_SEQUENCE10 = 10,
      SEQUENCE_SEQUENCE11 = 11,
      SEQUENCE_SEQUENCE12 = 12,
      SEQUENCE_SEQUENCE13 = 13,
      SEQUENCE_SEQUENCE14 = 14,
      SEQUENCE_SEQUENCE15 = 15,
      SEQUENCE_SEQUENCE16 = 16,
      SEQUENCE_SEQUENCE17 = 17,
      SEQUENCE_SEQUENCE18 = 18,
      SEQUENCE_SEQUENCE19 = 19,
      SEQUENCE_SEQUENCE20 = 20,
      SEQUENCE_SEQUENCE21 = 21,
      SEQUENCE_SEQUENCE22 = 22,
      SEQUENCE_SEQUENCE23 = 23,
      SEQUENCE_SEQUENCE24 = 24,
      SEQUENCE_SEQUENCE25 = 25,
      SEQUENCE_SEQUENCE26 = 26,
      SEQUENCE_SEQUENCE27 = 27,
      SEQUENCE_SEQUENCE28 = 28,
      SEQUENCE_SEQUENCE29 = 29,
      SEQUENCE_SEQUENCE30 = 30,
      SEQUENCE_SEQUENCE31 = 31,
    };
    int const kSequenceNumMin = SEQUENCE_START;
    int const kSequenceNumMax = SEQUENCE_SEQUENCE31;

    // B-channel modes: misconfigured, off, single, or double.
    enum BCHANNEL_MODE {
      BCHANNEL_MODE_MISCONFIGURED=0,
      BCHANNEL_MODE_OFF=1,
      BCHANNEL_MODE_SINGLE=2,
      BCHANNEL_MODE_DOUBLE=3,
      BCHANNEL_MODE_BLOCK=4
    };

    // Definitions related to B-data.
    enum BDATA_SOURCE {BDATA_SOURCE_APVE=0,
                       BDATA_SOURCE_BRILDAQ=1};

    // Definitions related to B-commands.
    enum BCOMMAND_TYPE {BCOMMAND_TYPE_BROADCAST=0,
                        BCOMMAND_TYPE_ADDRESSED=1};
    enum BCOMMAND_ADDRESS_TYPE {BCOMMAND_ADDRESS_TYPE_INTERNAL=0,
                                BCOMMAND_ADDRESS_TYPE_EXTERNAL=1};
    typedef uint8_t bcommand_data_t;
    typedef uint16_t bcommand_address_t;
    // B-command data spans 8 bits.
    bcommand_data_t const BCOMMAND_DATA_MIN = 0x0;
    bcommand_data_t const BCOMMAND_DATA_MAX = 0xff;
    // B-command addresses span 14 bits.
    bcommand_address_t const BCOMMAND_ADDRESS_MIN = 0x0;
    bcommand_address_t const BCOMMAND_ADDRESS_MAX = 0x3fff;
    // B-command sub-addresses span 8 bits.
    bcommand_address_t const BCOMMAND_SUBADDRESS_MIN = 0x0;
    bcommand_address_t const BCOMMAND_SUBADDRESS_MAX = 0xff;

    enum SOFTWARE_REQUEST_TYPE {
      SOFTWARE_REQUEST_TYPE_L1A = 0x100,
      SOFTWARE_REQUEST_TYPE_BGO = 0x200,
      SOFTWARE_REQUEST_TYPE_BGO_TRAIN = 0x400
    };

    // Definitions of trigger types.
    enum TRIG_TYPE {TRIG_TYPE_0=0,
                    TRIG_TYPE_PHYSICS=1,
                    TRIG_TYPE_SEQUENCE=2,
                    TRIG_TYPE_RANDOM=3,
                    TRIG_TYPE_AUX=4,
                    TRIG_TYPE_5=5,
                    TRIG_TYPE_6=6,
                    TRIG_TYPE_7=7,
                    TRIG_TYPE_CYCLIC=8,
                    TRIG_TYPE_BUNCHMASK=9,
                    TRIG_TYPE_SOFTWARE=10,
                    TRIG_TYPE_TTS=11,
                    TRIG_TYPE_PATTERN=12,
                    TRIG_TYPE_13=13,
                    TRIG_TYPE_14=14,
                    TRIG_TYPE_CPM_L1A_VIA_LPM=15};
    int const kTrigTypeMin = TRIG_TYPE_0;
    int const kTrigTypeMax = TRIG_TYPE_CPM_L1A_VIA_LPM;
    int const kNumTrigTypes = kTrigTypeMax - kTrigTypeMin + 1;

    // Definitions of TTS states.
    enum TTS_STATE {
      // The first states (< 16) are the 'legacy' four-bit TTS states.
      // NOTE: In the firmware (in the TTS intelligent OR) all invalid
      // states get mapped to 0xe, and both disconnected states get
      // mapped to 0xff.
      TTS_STATE_DISCONNECTED_0 = 0,
      TTS_STATE_WARNING = 1,
      TTS_STATE_OOS = 2,
      TTS_STATE_INVALID_3 = 3,
      TTS_STATE_BUSY = 4,
      TTS_STATE_INVALID_5 = 5,
      TTS_STATE_WARNING_DUE_TO_DAQ = 6,
      TTS_STATE_INVALID_7 = 7,
      TTS_STATE_READY = 8,
      TTS_STATE_PRIV_REQ_1 = 9,
      TTS_STATE_PRIV_REQ_2 = 10,
      TTS_STATE_PRIV_REQ_3 = 11,
      TTS_STATE_ERROR = 12,
      TTS_STATE_INVALID_13 = 13,
      TTS_STATE_INVALID_14 = 14,
      TTS_STATE_DISCONNECTED_1 = 15,
      // The following are the TCDS-special TTS states.
      TTS_STATE_LOST_INTO_PI = 0x10,
      TTS_STATE_LOST_INTO_PI_RXLOS = 0x11,
      TTS_STATE_LOST_INTO_PI_MODABS = 0x12,
      TTS_STATE_LOST_INTO_LPM = 0x20,
      TTS_STATE_LOST_INTO_LPM_RXLOS = 0x21,
      TTS_STATE_LOST_INTO_LPM_MODABS = 0x22,
      TTS_STATE_LOST_INTO_CPM = 0x30,
      TTS_STATE_MASKED = 0x98,
      TTS_STATE_INVALID_TCDS = 0xee,
      TTS_STATE_UNKNOWN_TCDS = 0xff
    };
    // BUG BUG BUG
    // These names could probably be improved a bit.
    /* unsigned int const kTTSStateMin = TTS_STATE_DISCONNECTED0; */
    /* unsigned int const kTTSStateMax = TTS_STATE_DISCONNECTED1; */
    // BUG BUG BUG end

    // An 'easy enum' for the various TTS sources/types/channels.
    enum TTS_CHANNEL_TYPE {
      TTS_CHANNEL_TOP_LEVEL,
      TTS_CHANNEL_ICI,
      TTS_CHANNEL_APVE,
      TTS_CHANNEL_RJ45,
      TTS_CHANNEL_FED,
      TTS_CHANNEL_DAQ_BACKPRESSURE,
      TTS_CHANNEL_RETRI,
      TTS_CHANNEL_PM_APVE,
      TTS_CHANNEL_BUNCH_MASK
    };

    // Define an 'impossible' FED ID which can be used to recognize
    // unused PI inputs.
    // NOTE: This is not strictly speaking an impossible FED ID, just
    // very far from any current CMS FED IDs.
    unsigned int const kImpossibleFEDId = 0xfff;

    // Define an 'unused' partition name which can be used to
    // recognize unused partitions.
    std::string const kUnusedPartitionName = "Unused";

    // BST signal reception state definitions.
    enum BST_SIGNAL_STATUS {
      BST_SIGNAL_STATUS_UNKNOWN = 0x0,
      BST_SIGNAL_STATUS_RESET = 0x0000dead,
      BST_SIGNAL_STATUS_NO_SIGNAL = 0xfa11010c,
      BST_SIGNAL_STATUS_NO_DATA = 0xfa110acc,
      BST_SIGNAL_STATUS_GOOD = 0x0000bea0
    };

    // BRILDAQ nibble status/quality.
    enum LUMI_NIBBLE_QUALITY {
      LUMI_NIBBLE_QUALITY_UNKNOWN = 0,
      LUMI_NIBBLE_QUALITY_CORRUPTED,
      LUMI_NIBBLE_QUALITY_INVALID,
      LUMI_NIBBLE_QUALITY_GOOD
    };

    enum REG_FAIL_REASON {
      REG_FAIL_REASON_UNKNOWN = 0,
      REG_FAIL_REASON_ALL_OK,
      REG_FAIL_REASON_UNAVAILABLE,
      REG_FAIL_REASON_DISALLOWED
    };

   } // namespace definitions
} // namespace tcds

#endif // _tcds_utils_Definitions_h_
