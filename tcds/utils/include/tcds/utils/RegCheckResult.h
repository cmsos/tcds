#ifndef _tcds_utils_RegCheckResult_h_
#define _tcds_utils_RegCheckResult_h_

#include "tcds/utils/Definitions.h"
#include "tcds/utils/SafeBool.h"

namespace tcds {
  namespace utils {

    class RegCheckResult : public tcds::utils::SafeBool<>
    {

    public:
      RegCheckResult(tcds::definitions::REG_FAIL_REASON const reason);
      virtual ~RegCheckResult();

      bool operator==(RegCheckResult const& rhs) const;
      bool operator!=(RegCheckResult const& rhs) const;

    protected:
      bool booleanTest() const;

    private:
      tcds::definitions::REG_FAIL_REASON reason_;

    };

    tcds::utils::RegCheckResult const kRegCheckResultOK =
      tcds::utils::RegCheckResult(tcds::definitions::REG_FAIL_REASON_ALL_OK);
    tcds::utils::RegCheckResult const kRegCheckResultUnavailable =
      tcds::utils::RegCheckResult(tcds::definitions::REG_FAIL_REASON_UNAVAILABLE);
    tcds::utils::RegCheckResult const kRegCheckResultDisallowed =
      tcds::utils::RegCheckResult(tcds::definitions::REG_FAIL_REASON_DISALLOWED);

  } // namespace utils
} // namespace tcds

#endif // _tcds_utils_RegCheckResult_h_
