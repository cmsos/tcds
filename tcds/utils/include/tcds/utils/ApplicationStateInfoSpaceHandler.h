#ifndef ApplicationStateInfoSpaceHandler_h_
#define ApplicationStateInfoSpaceHandler_h_

#include <map>
#include <string>

#include "tcds/utils/AppHist.h"
#include "tcds/utils/InfoSpaceHandler.h"

namespace xdaq {
  class Application;
}

namespace tcds {
  namespace utils {

    class InfoSpaceUpdater;
    class Monitor;
    class OwnerId;
    class WebServer;

    class ApplicationStateInfoSpaceHandler : public InfoSpaceHandler
    /**
     * This class holds the basic information about the top-level
     * state of the whole application. For example:
     * - The current state of the software state machine (if present).
     * - The state of the monitoring (i.e., running or
         stopped-because-of-a-problem).
     * NOTE: The variables in this InfoSpace are all a little
     * different from 'normal' InfoSpace items in that they are not
     * updated by an updater, but directly (i.e., from some other
     * place in the code).
     */
    {

    public:
      static std::string const kAllOKString;
      static std::string const kNoProblemString;

      ApplicationStateInfoSpaceHandler(xdaq::Application& xdaqApp,
                                       tcds::utils::InfoSpaceUpdater* updater);
      virtual ~ApplicationStateInfoSpaceHandler();

      void initialize();

      void addProblem(std::string const& keyword,
                      std::string const& problemDescription);
      void removeProblem(std::string const& keyword);

      void addHistoryItem(std::string const& desc);

      void setFSMState(std::string const& stateDescription,
                       std::string const& problemDescription=kNoProblemString);

      void setOwnerId(tcds::utils::OwnerId const& ownerId);

      void addMonitoringProblem(std::string const& problemDescription);
      void removeMonitoringProblem();

    protected:
      virtual void registerItemSetsWithMonitor(Monitor& monitor);
      virtual void registerItemSetsWithWebServer(WebServer& webServer,
                                                 Monitor& monitor,
                                                 std::string const& forceTabName="");

      virtual std::string formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const;

    private:
      void setApplicationState(std::string const& stateDescription);
      void setProblemDescription(std::string const& problemDescription);
      void updateStatusDescription();

      void updateSessionIdInApplicationDescriptor() const;

      std::map<std::string, std::string> problems_;
      tcds::utils::AppHist history_;

    };

  } // namespace utils
} // namespace tcds

#endif // ApplicationStateInfoSpaceHandler_h_
