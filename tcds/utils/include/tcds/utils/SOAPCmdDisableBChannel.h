#ifndef _tcds_utils_SOAPCmdDisableBChannel_h_
#define _tcds_utils_SOAPCmdDisableBChannel_h_

#include "xoap/MessageReference.h"

#include "tcds/utils/SOAPCmdBase.h"

namespace tcds {
  namespace utils {

    class XDAQAppBase;

    template <class T>
      class SOAPCmdDisableBChannel : public SOAPCmdBase<T>
      {

      public:
        SOAPCmdDisableBChannel(T& controller);

        /**
         * Disables the specified B-channel. Bound to the SOAP command
         * DisableBChannel.
         * - SOAP command parameters:
         *   - either one of the following two
         *     -  xsd:string `bgoName': the B-go name (e.g., 'Bgo1', 'Start').
         *     -  xsd:unsignedInt `bgoNumber': the B-go number (e.g., '1').
         * - SOAP command return value: none.
         */
        virtual xoap::MessageReference executeImpl(xoap::MessageReference const& msg);

      };

  } // namespace utils
} // namespace tcds

#include "tcds/utils/SOAPCmdDisableBChannel.hxx"

#endif // _tcds_utils_SOAPCmdDisableBChannel_h_
