#ifndef _tcds_utils_WebObject_h
#define _tcds_utils_WebObject_h

#include <cstddef>
#include <string>

namespace tcds {
  namespace utils {

    class Monitor;

    class WebObject
    {

    public:
      static std::string const kDefaultValueString;

      virtual ~WebObject();

      std::string getName() const;
      std::string getDescription() const;
      std::string getItemSetName() const;
      std::string getTabName() const;
      size_t getColSpan() const;

      virtual std::string getHTMLString() const = 0;
      virtual std::string getJSONString() const;

    protected:
      WebObject(std::string const& name,
                std::string const& description,
                Monitor const& monitor,
                std::string const& itemSetName,
                std::string const& tabName,
                size_t const colSpan);

      static std::string const kStringInvalidItem;
      static std::string const kStringUnknownItem;

      std::string const name_;
      std::string const description_;
      Monitor const& monitor_;
      std::string const itemSetName_;
      std::string const tabName_;
      size_t const colSpan_;

    };

  } // namespace utils
} // namespace tcds

#endif // _tcds_utils_WebObject_h
