#ifndef _tcds_utils_WebTab_h
#define _tcds_utils_WebTab_h

#include <cstddef>
#include <functional>
#include <memory>
#include <string>
#include <vector>

namespace tcds {
  namespace utils {

    class WebObject;

    class WebTab
    {

    public:
      WebTab(std::string const& name,
             std::string const& description,
             size_t const numColumns);
      ~WebTab();

      // Predicate to find a WebTab based on its name.
      class FindByName : public std::unary_function<WebTab const* const, bool> {
      public:
        FindByName(std::string const& nameToMatch) : nameToMatch_(nameToMatch)
        {
        };
        bool operator() (WebTab const* const tab) const
        {
          return (tab->name_ == nameToMatch_);
        }
      private:
        std::string const nameToMatch_;
      };

      std::string getName() const;
      std::string getDescription() const;
      size_t getNumColumns() const;

      std::string getHTMLString() const;
      std::string getJSONString() const;

      // NOTE: the WebTab takes ownership of the object passed in.
      void addWebObject(std::unique_ptr<WebObject> object);

    private:
      std::string const name_;
      std::string const description_;
      size_t const numColumns_;

      std::vector<WebObject*> webObjects_;

      // NOTE: WebTabs contain pointers to WebObjects and should
      // therefore not be copied (or assigned).
      WebTab(WebTab& src);
      WebTab& operator=(WebTab const& src);

    };

  } // namespace utils
} // namespace tcds

#endif // _tcds_utils_WebTab_h
