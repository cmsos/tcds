//-----------------------------------------------------------------------------

// Load some (more) stuff we need.

// NOTE: Only do this for TCDS control applications. The check here is
// necessary because the call to
// xgi::framework::UIManager::setLayout() in the WebServer globally
// replaces the HyperDAQ layout for all applications in the XDAQ
// executive.

if (isTCDSApplication())
{
    // The splash screen.
    getScript(baseUrl + "/tcds/utils/html/please-wait/please-wait.min.js", true);
    getScript(baseUrl + "/tcds/utils/html/js/tcds_loading_screen.js", true);
    getCSS(baseUrl + "/tcds/utils/html/please-wait/please-wait.css", true);

    // JQuery friends.
    // NOTE: JQuery itself is already loaded by the XDAQ/HyperDAQ
    // framework JS.
    getScript(baseUrl + "/tcds/utils/html/jquery-ui/jquery-ui.min.js");
    getCSS(baseUrl + "/tcds/utils/html/jquery-ui/jquery-ui.css");

    // SlickGrid-related.
    getScript(baseUrl + "/tcds/utils/html/slickgrid/lib/jquery.event.drag-2.3.0.js", true);
    // Individual, plain files for development
    // getScript(baseUrl + "/tcds/utils/html/slickgrid/slick.core.js", true);
    // getScript(baseUrl + "/tcds/utils/html/slickgrid/slick.grid.js", true);
    // getScript(baseUrl + "/tcds/utils/html/slickgrid/plugins/slick.rowselectionmodel.js", true);
    // getScript(baseUrl + "/tcds/utils/html/slickgrid/plugins/slick.autotooltips.js", true);
    // Combined, minified file for deployment.
    getScript(baseUrl + "/tcds/utils/html/slickgrid/minified/slick.combined.min.js", true);
    getCSS(baseUrl + "/tcds/utils/html/slickgrid/slick.grid.css");

    // PNotify.
    // Individual, plain files for development
    // getScript(baseUrl + "/tcds/utils/html/nonblock/NonBlock.js");
    // getScript(baseUrl + "/tcds/utils/html/pnotify/PNotify.js");
    // Combined, minified file for deployment.
    getScript(baseUrl + "/tcds/utils/html/nonblock/minified/nonblock.min.js");
    getScript(baseUrl + "/tcds/utils/html/pnotify/minified/pnotify.min.js");
    getCSS(baseUrl + "/tcds/utils/html/pnotify/PNotify.css");

    // Chart and friends.
    getScript(baseUrl + "/tcds/utils/html/hammerjs/hammer.min.js", true);
    getScript(baseUrl + "/tcds/utils/html/chartjs/chart.min.js", true);
    getScript(baseUrl + "/tcds/utils/html/chartjs-plugin-zoom/chartjs-plugin-zoom.min.js", true);
    getScript(baseUrl + "/tcds/utils/html/chartjs-adapter-date-fns/dist/chartjs-adapter-date-fns.bundle.min.js", true);

    // doT.
    getScript(baseUrl + "/tcds/utils/html/dot/doT.js");

    // TCDS stuff.
    getCSS(baseUrl + "/tcds/utils/html/css/tcds.css");
}

//-----------------------------------------------------------------------------
