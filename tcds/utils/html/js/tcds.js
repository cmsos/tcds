//-----------------------------------------------------------------------------
//
// NOTE: This has been written to work with the XDAQ12-style HyperDAQ
// and doT.js.
//
//-----------------------------------------------------------------------------

// HyperDAQ JS pre-load callback method.
$(document).on("xdaq-pre-load", function() {
    console.debug("xdaq-pre-load");
    if (isTCDSApplication())
    {
        showLoadingScreen();
    }
});

//-----------------------------------------------------------------------------

// HyperDAQ JS post-load callback method.
$(document).on("xdaq-post-load", function() {
    console.debug("xdaq-post-load");
    if (isTCDSApplication())
    {
        // NOTE: At some point we may want to clean up this
        // button-business a bit.

        // Add the 'Modify TTCSpy configuration' button if we're
        // dealing with a PIController.
        var tmp = jQuery(".tcds-tab-name:contains('TTCSpy')");
        if (tmp.length)
        {
            var hook = tmp.parent();
            fixupTTCSpyConfigButton(hook);
        }

        // Add the 'Modify random-trigger rate' button if needed.
        tmp = jQuery("p:contains('Random trigger')");
        if (tmp.length)
        {
            var hook = tmp.first();
            fixupRandomRateConfigButton(hook);
        }
        // Add the 'Launch L1A trigger pattern' and 'Halt L1A trigger
        // pattern' buttons if needed.
        tmp = jQuery("p:contains('Pattern trigger')");
        if (tmp.length)
        {
            var hook = tmp.first();
            fixupPatternTriggerLaunchButton(hook);
            fixupPatternTriggerHaltButton(hook);
        }

        // Add the 'Dump system state' button if we're
        // dealing with a TCDSCentral.
        tmp = jQuery("h1:contains('TCDSCentral')");
        if (tmp.length)
        {
            var hook = tmp;
            fixupSystemDumpButton(hook);
        }

        // Make sure that data gets updated when switching tabs (since
        // normally only visible data, and not the data in the
        // background tabs) gets updated regularly from the AJAX
        // requests.
        jQuery(".xdaq-tab-nav").click(function() {
            applyData();
        });

        //----------

        startUpdate();

        //----------

        finishLoadingScreen();
    }
});

//-----------------------------------------------------------------------------

function determineNextUpdateInterval()
{
    // This method implements a basic truncated binary exponential
    // backoff for the AJAX update interval.

    if (tcds.updateFailCount)
    {
        var N = (1 << tcds.updateFailCount) - 1;
        var rnd = Math.random();
        var factor = Math.round(rnd * N);
        tcds.updateInterval = (1 + factor ) * tcds.defaultUpdateInterval;
    }
    else
    {
        tcds.updateInterval = tcds.defaultUpdateInterval;
    }
}

//-----------------------------------------------------------------------------

function startUpdate()
{
    console.debug("Starting TCDS AJAX update loop");
    updateLoop();
}

//-----------------------------------------------------------------------------

function updateLoop()
{
    console.debug("Iteration of updateLoop()");

    setTimeout(function() {
        determineNextUpdateInterval();
        ajaxForUpdate();
    }, tcds.updateInterval);
};

//-----------------------------------------------------------------------------

function ajaxForUpdate()
{
    var data;
    jQuery.ajax({
        url : tcds.updateUrl,
        dataType : "json",
        data : data,
        timeout : tcds.ajaxTimeout,
        success : function(data, textStatus, jqXHR) {
            // clearErrors();
            clearAjaxStatus();

            tcds.updateFailCount = Math.max(0, tcds.updateFailCount - 1);
            addAjaxStatus("AJAX success at " + timestampStr());

            processAJAXSuccess(data, textStatus, jqXHR);

            // //----------
            // // A little hacky, but it can be useful. Enable
            // // the below to check for duplicate HTML IDs.
            // $('[id]').each(function() {
            //     var ids = $('[id="' + this.id + '"]');
            //     if ((ids.length > 1) && (ids[0] == this))
            //     {
            //         console.warn("Multiple IDs #" + this.id);
            //     }
            // });
            // //----------

            if (tcds.updateFailCount)
            {
                addError("Connection with the application is shaky."
                         + " Information below may be stale."
                         + " Updating throttled to level " + tcds.updateFailCount
                         + " of " + tcds.maxUpdateFailCount + ".");
                if (tcds.updateFailCount == tcds.maxUpdateFailCount)
                {
                    addError("(Level " + tcds.updateFailCount
                             + " pretty much means 'connection lost',"
                             + " with periodic retries.)");
                }
            }

            showErrors();
            showProblems();
            showAjaxStatus();

            // finishLoadingScreen();
            updateLoop();
        },
        error : function(jqXHR, textStatus, errorThrown)
        {
            clearErrors();
            clearAjaxStatus();

            tcds.updateFailCount = Math.min(tcds.maxUpdateFailCount, tcds.updateFailCount + 1);
            addAjaxStatus("AJAX failure at " + timestampStr());

            processAJAXError(jqXHR, textStatus, errorThrown);

            if (tcds.updateFailCount)
            {
                addError("Connection with the application is shaky."
                         + " Information below may be stale."
                         + " Updating throttled to level " + tcds.updateFailCount
                         + " of " + tcds.maxUpdateFailCount + ".");
                if (tcds.updateFailCount == tcds.maxUpdateFailCount)
                {
                    addError("(Level " + tcds.updateFailCount
                             + " pretty much means 'connection lost',"
                             + " with periodic retries.)");
                }
            }

            showErrors();
            showProblems();
            showAjaxStatus();

            updateLoop();
        }
    });
}

//-----------------------------------------------------------------------------

function processAJAXSuccess(data, textStatus, jqXHR)
{
    console.debug("processAJAXSuccess()");

    //----------

    // Check if we really received something. In case something went
    // really bad, we will receive an empty string.
    if (jQuery.isEmptyObject(data))
    {
        // Clear errors before adding an important one.
        clearErrors();
        addError("Received an empty JSON update. " +
                 "\n" +
                 "Something must have gone horribly wrong " +
                 "on the application side. " +
                 "\n" +
                 "Please have a look at the application log " +
                 "for more information.");
    }
    else
    {
        // This is a bit hacky, but it does save a lot of CPU cycles.
        var tmp = data["Application state"]["Latest monitoring update time"];
        if ((tcds.update_timestamp !== undefined) &&
            (tcds.update_timestamp == tmp))
        {
            // NOTE: This is the case where we can't call
            // clearErrors().
            console.warn("Skipping an AJAX update"
                         + " that delivered data we already had"
                         + " (data from " + tmp + ").");
            addAjaxStatus("Update has timestamp " + timestampStr(tmp) + " -> ignoring old data.");
            return;
        }

        clearErrors();
        addAjaxStatus("Update has timestamp " + timestampStr(tmp) + ".");
        tcds.update_timestamp = tmp;

        //----------

        // And now we can get started actually doing something useful.
        try
        {
            // Process and apply the data to the DOM.
            // tcds.dataPrev = tcds.data;
            tcds.data = data;
            applyData();

            //----------

            // Replace the HyperDAQ framework page title with our own one
            // (which is of course better).
            const title = jQuery("#tcds-application-title").text();
            const subTitle = jQuery("#tcds-application-subtitle").text();
            const ourTitle = title + " " + subTitle;
            document.title = ourTitle;

            // A special case: we always expect an entry called 'Application
            // State' - 'Problem description'. If that does not exist, log an
            // error. If it does exist and is not "-", announce that there has
            // apparently been some problem in the XDAQ application.
            var tmp0 = "Application state";
            var tmp1 = "Problem description";
            var tmp2 = "Uptime";
            // Verify that this element exists in the JSON data we received in
            // response to our AJAX call.
            var tmpProbDesc = tcds.invalidDataStr;
            try
            {
                tmpProbDesc = data[tmp0][tmp1];
            }
            catch (err)
            {
                addError("Expected (but could not find) an item called " +
                         "'" + tmp0 + "' - '" + tmp1 + "'" +
                         " in the JSON update data.");
            }
            if (tmpProbDesc != tcds.invalidDataStr)
            {
                addError("This application requires attention."
                         + "\n"
                         + " Please see"
                         + " 'Application status'"
                         + " -> 'Application state'"
                         + " -> 'Problem description'"
                         + " for details.");
            }

            //----------

            // BUG BUG BUG
            // This should be somewhere else, really.

            // Manipulation of the various buttons that should not
            // always be enabled.
            tmp0 = "Application state";
            tmp1 = "Application FSM state";
            var fsmState = tcds.data[tmp0][tmp1];
            if ((fsmState == "Halted") ||
                (fsmState == "Configuring") ||
                (fsmState == "Zeroing"))
            {
                // In these states we are not (yet fully) connected to the
                // hardware.
                jQuery("#button_configure_ttcspy").prop("disabled", true);
                jQuery("#button_configure_randomrate").prop("disabled", true);
                jQuery("#button_launch_pattern_trigger").prop("disabled", true);
                jQuery("#button_halt_pattern_trigger").prop("disabled", true);
            }
            else
            {
                // In anything but the above we are connected to the hardware.
                jQuery("#button_configure_ttcspy").prop("disabled", false);
                // Argh! Only enable the button if random-triggers are
                // actually enabled.
                var button = jQuery("#button_configure_randomrate");
                if (button.length)
                {
                    var tmp = tcds.data["itemset-inputs"]["Random-trigger generator"];
                    var randomsEnabled = (tmp == "enabled");
                    button.prop("disabled", !randomsEnabled);
                }
                // And similar for the pattern trigger button.
                var buttonLaunch = jQuery("#button_launch_pattern_trigger");
                var buttonHalt = jQuery("#button_halt_pattern_trigger");
                if (buttonLaunch.length || buttonHalt.length)
                {
                    var tmp = tcds.data["itemset-inputs"]["Pattern trigger generator"];
                    var patternTriggerEnabled = (tmp == "enabled");
                    if (buttonLaunch.length)
                    {
                        buttonLaunch.prop("disabled", !patternTriggerEnabled);
                    }
                    if (buttonHalt.length)
                    {
                        buttonHalt.prop("disabled", !patternTriggerEnabled);
                    }
                }
            }
            // BUG BUG BUG end

        }
        catch(err)
        {
            addError("Caught an exception: " + err);
        }

    }
}

//-----------------------------------------------------------------------------

function processAJAXError(jqXHR, textStatus, errorThrown)
{
    console.debug("processAJAXError()");

    var reasonString = "";
    var baseString = "";

    // Let's at least catch the usual problems. If nothing known
    // matches show a blanket fail message.
    if (textStatus == "parsererror")
    {
        baseString = "Something went wrong with the AJAX update.";
        reasonString = "An error occurred parsing the received JSON data ('" + errorThrown.message + "').";
    }
    else if (textStatus == "error")
    {
        if (errorThrown == "Bad Request")
        {
            reasonString = "Cannot connect to the XDAQ application ('Bad Request').";
        }
        else if ((jqXHR.status == 404) || (errorThrown == "Not Found"))
        {
            reasonString = "Cannot find the remote location ('Not Found').";
        }
        else
        {
            reasonString = "Lost connection to the XDAQ application ('Connection Failed').";
        }
    }
    else if (textStatus == "timeout")
    {
        reasonString = "Lost connection to the XDAQ application (connection timed out).";
    }
    else
    {
        baseString = "Something went wrong with the AJAX update.";
        reasonString = "No clue what though.";
    }
    var msg = "";
    if (baseString.length > 0)
    {
        msg = baseString;
    }
    if (reasonString.length > 0)
    {
        if (msg.length > 0)
        {
            msg += "\n";
        }
        msg += reasonString;
    }
    if (msg.length > 0)
    {
        msg += "\n";
    }
    console.error("Error while obtaining/parsing JSON data: " + msg);
}

//-----------------------------------------------------------------------------

function applyData()
{
    var data = tcds.data;

    //----------

    // // This is a bit hacky, but it does save a lot of CPU cycles.
    // var tmp = data["Application state"]["Latest monitoring update time"];
    // if ((tcds.update_timestamp !== undefined) &&
    //     (tcds.update_timestamp == tmp))
    // {
    //     console.warn("Skipping an AJAX update"
    //                  + " that delivered data we already had"
    //                  + " (data from " + tmp + ").");
    //     addAjaxStatus("Update has timestamp " + timestampStr(tmp) + " -> ignoring old data.");
    //     return;
    // }
    // else
    // {
    //     addAjaxStatus("Update has timestamp " + timestampStr(tmp) + ".");
    // }
    // tcds.update_timestamp = tmp;

    tcds.update_timestamp = data["Application state"]["Latest monitoring update time"];

    var scripts;
    if (!tcds.initialised)
    {
        // The first time around, when things have not yet been
        // initialised, simply use all dot template scripts.
        scripts = jQuery("script[type='text/x-dot-template']");
    }
    else
    {
        // After initialisation, only use those dot template scripts
        // that do not have an invisible .xdaq-tab as parent.
        var tmp_scripts_0 = jQuery(".xdaq-tab").not(".xdaq-tab-hide").find("script[type='text/x-dot-template']");
        // NOTE: Don't forget to include those dot template scripts
        // that don't have an .xdaq-tab as parent at all. E.g., the
        // page subtitle...
        var tmp_scripts_1 = jQuery("script[type='text/x-dot-template']:not(.xdaq-tab script)");
        scripts = tmp_scripts_0;
        scripts = scripts.add(tmp_scripts_1);
    }
    jQuery(scripts).each(function() {
        var thisScript = jQuery(this);
        if (thisScript.parent().is(":visible") || !tcds.initialised)
        {
            var templateStr = thisScript.text();
            var template = tcds.templateCache[templateStr];
            if (template == undefined)
            {
                template = doT.template(templateStr);
                tcds.templateCache[templateStr] = template;
            }
            // NOTE: The following is an attempt to handle both real
            // HTML and strings with HTML-like characters.
            var tmp = template(data);

            var html = hackIntoHTML(tmp);
            var target = thisScript.siblings(".target")[0];
            if (jQuery(target).html() != html)
            {
                jQuery(target).html(html);
            }
        }
    });

    //----------

    // Fill the application state history table.
    var placeholderName = "#application-status-history-placeholder";
    var placeholder = jQuery(placeholderName);
    if (placeholder.length)
    {
        // If the placeholder exists, this must be some sort of TCDS application.
        var data = tcds.data["itemset-application-status-history"]["Application status history"];
        var columns = [
            {id: "timestamp", name: "Timestamp", field: "Timestamp", width: 200},
            {id: "msg", name: "Message", field: "Message", width: 400}
        ];
        updateGrid("apphist-grid", placeholder, data, columns, true);
    }

    //----------

    // Fill the TTCSpy log table.
    var placeholderName = "#ttcspylog-placeholder";
    var placeholder = jQuery(placeholderName);
    if (placeholder.length)
    {
        // If the placeholder exists, this must be a PIController.
        var tmp = tcds.data["itemset-ttcspylog"]["TTCSpy log contents"];
        var spyData = tmp;
        var columns = [];
        if (tmp !== tcds.invalidDataStr)
        {
            var logColumns = tmp["columns"];
            var columnIds = Object.keys(logColumns);
            for (var id in logColumns)
            {
                columns.push({
                    "id": id,
                    "field": id,
                    "name": logColumns[id]
                    // "formatter": ttsLogFormatter
                });
            }
            spyData = tmp["data"];
            spyData.getItemMetadata = function(row) {
                var thisRowData = this[row];
                var res = {};

                //----------
                // CSS coloring of TTC spy entries based on their types.
                //----------

                // Mark errors in red.
                var flags = thisRowData['flags'];
                if (flags.indexOf("error") > -1)
                {
                    res['cssClasses'] = 'ttc_spy_row_error';
                }
                else
                {
                    // Mark everything else based on the entry type.
                    var type = thisRowData['type'];
                    if (type == "L1A")
                    {
                        res['cssClasses'] = 'ttc_spy_row_l1a';
                    }
                    else if (type == "Broadcast command")
                    {
                        res['cssClasses'] = 'ttc_spy_row_bcommand_broadcast';
                    }
                    else if (type == "Addressed command")
                    {
                        res['cssClasses'] = 'ttc_spy_row_bcommand_addressed';
                    }
                }

                //----------

                return res;
            };
        }
        updateGrid("ttcspylog-grid", placeholder, spyData, columns, false);
    }

    //----------

    // Fill the TTS history table.

    function ttsLogFormatter(row, cell, value, columnDef, dataContext)
    {
        return "<span tts_val='" + value + "'>" + value + "</span>";
    }

    var placeholderName = "#ttslog-placeholder";
    var placeholder = jQuery(placeholderName);
    if (placeholder.length)
    {
        // If the placeholder exists, this must be a PIController.
        var tmp = tcds.data["itemset-ttslog"]["TTS log contents"];
        var logData = tmp;
        var columns = [];
        if (tmp !== tcds.invalidDataStr)
        {
            var logColumns = tmp["columns"];
            var columnIds = Object.keys(logColumns);
            for (var id in logColumns)
            {
                columns.push({
                    "id": id,
                    "field": id,
                    "name": logColumns[id],
                    "formatter": ttsLogFormatter
                });
            }
            logData = tmp["data"];
            logData.getItemMetadata = function(row) {
                var thisRowData = this[row];
                var res = {};

                //----------

                // NOTE: Each entry can be accompanied by a simple
                // text comment. Entries with orbit number 0 and BX
                // number 0 are used as 'pure comments.'

                // If this is a pure comment, put it widely across all
                // columns.
                if ((thisRowData.orbit == 0) && (thisRowData.bx == 0))
                {
                    var options = {
                        groupCssClass: "slick-group",
                        groupTitleCssClass: "slick-group-title",
                        totalsCssClass: "slick-group-totals",
                        groupFocusable: true,
                        totalsFocusable: false,
                        toggleCssClass: "slick-group-toggle",
                        toggleExpandedCssClass: "expanded",
                        toggleCollapsedCssClass: "collapsed",
                        enableExpandCollapse: true
                    };
                    function defaultGroupCellFormatter(row, cell, value, columnDef, item) {
                        if (!options.enableExpandCollapse) {
                            return item.title;
                        }

                        var indentation = item.level * 15 + "px";

                        return "<span class='" + options.toggleCssClass + " " +
                            (item.collapsed ? options.toggleCollapsedCssClass : options.toggleExpandedCssClass) +
                            "' style='margin-left:" + indentation +"'>" +
                            "</span>" +
                            "<span class='" + options.groupTitleCssClass + "' level='" + item.level + "'>" +
                            thisRowData.comment +
                            "</span>";
                    }
                    res = {
                        "columns": {
                            0: {
                                colspan: "*",
                                formatter: defaultGroupCellFormatter,
                                editor: null
                            }
                        }
                    }
                }
                else
                {
                    //----------
                    // CSS coloring of 'normal' TTS log entries.
                    //----------
                    // Let's mark entries with the output TTS state in
                    // anything but READY.
                    var outputState = thisRowData['tts_out'];
                    if (outputState != "ready")
                    {
                        res['cssClasses'] = 'tts_log_row_not_ready';
                    }
                    else
                    {
                        res['cssClasses'] = 'tts_log_row_ready';
                    }
                }

                //----------

                return res;
            };
        }
        updateGrid("ttslog-grid", placeholder, logData, columns, false);
    }

    //----------

    // Fill the recent-nibbles history table.
    var placeholderName = "#nibblehistory-placeholder";
    var placeholder = jQuery(placeholderName);
    if (placeholder.length)
    {
        // If the placeholder exists, this must be a CPMController.
        var historyData = tcds.data["itemset-cpm-nibble-history"]["Recent nibbles"];
        var columns = [
            {id: "runnum", name: "Run number", field: "RunNumber"},
            {id: "sectionnum", name: "Section number", field: "SectionNumber"},
            {id: "nibblenum", name: "Nibble number", field: "NibbleNumber"},
            {id: "runactive", name: "Run active", field: "RunActive"},
            {id: "numorbits", name: "# orbits", field: "NumOrbits"},
            {id: "triggerrate", name: "Trigger rate (Hz)", field: "TriggerRate"},
            {id: "suppressedtriggerrate", name: "Suppressed-trigger rate (Hz)", field: "SuppressedTriggerRate"},
            {id: "deadtime", name: "Deadtime (% of time)", field: "Deadtime"},
            {id: "deadtimebeamactive", name: "Deadtime with beam active (% of time)", field: "DeadtimeBeamActive"}
        ];
        updateGrid("nibblehistory-grid", placeholder, historyData, columns, true);
    }

    //----------

    // Fill the recent-sections history table.
    var placeholderName = "#sectionhistory-placeholder";
    var placeholder = jQuery(placeholderName);
    if (placeholder.length)
    {
        // If the placeholder exists, this must be a CPMController.
        var historyData = tcds.data["itemset-cpm-section-history"]["Recent sections"];
        var columns = [
            {id: "runnum", name: "Run number", field: "RunNumber"},
            {id: "sectionnum", name: "Section number", field: "SectionNumber"},
            {id: "numnibbles", name: "# nibbles", field: "NumNibbles"},
            {id: "triggerrate", name: "Trigger rate (Hz)", field: "TriggerRate"},
            {id: "suppressedtriggerrate", name: "Suppressed-trigger rate (Hz)", field: "SuppressedTriggerRate"},
            {id: "deadtime", name: "Deadtime (% of time)", field: "Deadtime"},
            {id: "deadtimebeamactive", name: "Deadtime with beam active (% of time)", field: "DeadtimeBeamActive"}
        ];
        updateGrid("sectionhistory-grid", placeholder, historyData, columns, true);
    }

    //----------

    // Fill the recent-BRILDAQ history table.
    var placeholderName = "#brildaqhistory-placeholder";
    var placeholder = jQuery(placeholderName);
    if (placeholder.length)
    {
        // If the placeholder exists, this must be a BRILDAQChecker.
        var historyData = tcds.data["itemset-brildaq-history"]["Recent TCDS BRILDAQ data"];
        var columns = [
            {id: "source_id", name: "Source ID", field: "DataSourceId"},
            {id: "format_version", name: "Data format version", field: "FormatVersion"},
            {id: "bst_signal_status", name: "BST status", field: "BSTSignalStatus"},
            {id: "timestamp_begin", name: "Begin", field: "BSTTimestampBegin"},
            {id: "timestamp_end", name: "End", field: "BSTTimestampEnd"},
            {id: "fillnum", name: "Fill number", field: "FillNumber"},
            {id: "runnum", name: "Run number", field: "RunNumber"},
            {id: "sectionnum", name: "Section number", field: "SectionNumber"},
            {id: "nibblenum", name: "Nibble number", field: "NibbleNumber"},
            {id: "runactive", name: "Run active", field: "CMSRunActive"},
            {id: "deadtime", name: "Deadtime (% of time)", field: "Deadtime"},
            {id: "deadtimebeamactive", name: "Beam-active deadtime (% of time)", field: "DeadtimeBeamActive"}
        ];
        updateGrid("brildaqhistory-grid", placeholder, historyData, columns, true);
    }

    //----------

    // Fill the APVE pipeline history table.
    var placeholderName = "#apvesimhistory-placeholder";
    var placeholder = jQuery(placeholderName);
    if (placeholder.length)
    {
        // If the placeholder exists, this must be an APVEController.
        var historyData = tcds.data["itemset-simhist"]["Simulated APV pipeline history"];
        var columns = [
            {id: "eventnumber", name: "Event number", field: "EventNumber"},
            {id: "address", name: "Pipeline address", field: "PipelineAddress"},
            {id: "graycode", name: "Gray code", field: "PipelineAddressGrayCode"},
            {id: "deltaaddress", name: "Address difference", field: "PipelineAddressDelta"}
        ];
        updateGrid("apvesimhistory-grid", placeholder, historyData, columns, false);
    }

    //----------

    // Fill the APVE status history table.
    var placeholderName = "#apvestatushistory-placeholder";
    var placeholder = jQuery(placeholderName);
    if (placeholder.length)
    {
        // If the placeholder exists, this must be an APVEController.
        var historyData = tcds.data["itemset-statushist"]["TTS and APVE status history"];
        var columns = [
            {id: "orbitnumber", name: "Orbit number", field: "OrbitNumber"},
            {id: "bxnumber", name: "BX number", field: "BXNumber"},
            {id: "ttsstate", name: "Output TTS state", field: "TTSState"},
            {id: "fmmttsstate", name: "FMM TTS", field: "FMMTTSState"},
            {id: "apvttsstate", name: "APV TTS", field: "APVTTSState"},
            {id: "apvoosreason", name: "APV 'Out-of-sync' reason", field: "APVOOSReason"},
            {id: "apverrorreason", name: "APV 'Error' reason", field: "APVErrorReason"},
            {id: "flags", name: "Flags", field: "Flags"}
        ];
        updateGrid("apvestatushistory-grid", placeholder, historyData, columns, false);
    }

    //----------

    placeholderName = "#l1ahistos-placeholder";
    placeholder = jQuery(placeholderName);
    if (placeholder.length && placeholder.is(":visible"))
    {
        // First extract the data from the JSON update and remodel it
        // a little.
        const histoDataTmp = tcds.data["itemset-l1a-histos"];
        var histoData = [];
        const labels = Object.keys(histoDataTmp);
        for (var i = 0; i != labels.length; ++i)
        {
            var label = labels[i];
            var dataTmp = histoDataTmp[label];
            //if ((dataTmp != "-") && (Math.max.apply(Math, dataTmp) != 0))
            if (dataTmp != tcds.invalidDataStr)
            {
                histoData.push({label: label,
                                data: dataTmp});
            }
        }

        if (!histoData.length)
        {
            placeholder.text(tcds.invalidDataStr);
            delete tcds.l1ahistos;
            tcds.l1ahistos = undefined;
        }
        else
        {
            if (tcds.l1ahistos === undefined)
            {
                const chartCanvasId = "chart-container-l1ahistos";
                jQuery(placeholderName).html(jQuery("<canvas></canvas>")
                                             .attr("id", chartCanvasId)
                                             .attr("class", "chart-container-l1ahistos"));
                tcds.l1ahistos = new L1AHistos(chartCanvasId);
                tcds.l1ahistos.init();
                tcds.l1ahistos.setData(histoData);
                tcds.l1ahistos.update();

                // Fix the vertical position of the XDAQ page footer.
                xdaqHeaderFooterStick();
            }
            else
            {
                tcds.l1ahistos.setData(histoData);
                tcds.l1ahistos.update();
            }
        }
    }

    //----------

    placeholderName = ".freqmon-trend-placeholder";
    placeholders = jQuery(placeholderName);
    for (var i = 0; i < placeholders.length; ++i)
    {
        var placeholder = jQuery(placeholders[i]);
        if (placeholder.length && placeholder.is(":visible"))
        {
            // First extract the data from the JSON update and remodel it
            // a little.
            const itemSetNameRaw = placeholder.attr("itemsetname");
            const itemSetName = itemSetNameRaw.replace(/^[^a-z]+|[^\w:.-]+/gi, "");

            const trendDataTmp = tcds.data[itemSetNameRaw];
            var trendData = undefined;
            var dataTmp = trendDataTmp["Trend"];
            if (dataTmp != tcds.invalidDataStr)
            {
                const label = itemSetNameRaw;
                const data = dataTmp.map(i => {return [Date.parse(i["Timestamp"]),
                                                       parseFloat(i["Frequency"])]});
                trendData = {label: label,
                             data: data};
            }

            if (!trendData)
            {
                placeholder.text(tcds.invalidDataStr);
                delete tcds.freqmontrends[itemSetName];
            }
            else
            {
                if (tcds.freqmontrends[itemSetName] === undefined)
                {
                    const trendCanvasId = "chart-container-" + itemSetName;
                    jQuery(placeholder).html(jQuery("<canvas></canvas>")
                                             .attr("id", trendCanvasId)
                                             .attr("class", "chart-container-freqmon"));
                    tcds.freqmontrends[itemSetName] = new FreqMonTrend(trendCanvasId);
                    tcds.freqmontrends[itemSetName].init();
                    tcds.freqmontrends[itemSetName].setData(trendData);
                    tcds.freqmontrends[itemSetName].update();

                    // Fix the vertical position of the XDAQ page footer.
                    xdaqHeaderFooterStick();
                }
                else
                {
                    tcds.freqmontrends[itemSetName].setData(trendData);
                    tcds.freqmontrends[itemSetName].update();
                }
            }
        }
    }

    //----------

    tcds.initialised = true;
}

//-----------------------------------------------------------------------------

function updateGrid(id, container, gridData, columns, reverse)
{
    if (jQuery(container).length && jQuery(container).is(":visible"))
    {
        if (gridData == tcds.invalidDataStr)
        {
            jQuery(container).text(tcds.invalidDataStr);
            delete tcds.grids[id];
            tcds.grids[id] = undefined;
            xdaqHeaderFooterStick();
        }
        else
        {
            if (reverse)
            {
                gridData.reverse();
            }

            var grid = tcds.grids[id];
            if (grid === undefined)
            {
                var options = {
                    enableCellNavigation: true,
                    enableColumnReorder: false,
                    forceFitColumns: true,
                    fullWidthRows: true,
                    multiSelect: true
                };
                jQuery(container).html(jQuery("<div></div>").attr("id", id).attr("class", "grid"));
                grid = new Slick.Grid("#" + id, gridData, columns, options);
                grid.setSelectionModel(new Slick.RowSelectionModel());
                grid.registerPlugin(new Slick.AutoTooltips({enableForHeaderCells: true}));
                grid.render();
                tcds.grids[id] = grid;
                xdaqHeaderFooterStick();

                if (!reverse)
                {
                    // NOTE: This is not particularly pretty, no...
                    jQuery(container).prepend("<div>"
                                              + "<input type=\"checkbox\""
                                              + " id=\"tail_button_" + id + "\""
                                              + " style=\"display: inline-block;\"/>"
                                              + "<label for=\"tail_button_" + id + "\""
                                              + " style=\"display: inline-block;\">"
                                              + "Follow tail of table"
                                              + "</label>"
                                              + "</div>");
                }
                jQuery(container).prepend("<div>"
                                          + "<input type=\"checkbox\""
                                          + " id=\"freeze_button_" + id + "\""
                                          + " style=\"display: inline-block;\"/>"
                                          + "<label for=\"freeze_button_" + id + "\""
                                          + " style=\"display: inline-block;\">"
                                          + "Freeze table"
                                          + "</label>"
                                          + "</div>");
            }
            else
            {
                var button = jQuery("#freeze_button_" + id);
                if (!button.is(":checked"))
                {
                    grid.setData(gridData);
                    grid.invalidate();
                    grid.render();
                }
            }

            // Scroll to bottom if requested.
            var button = jQuery("#tail_button_" + id);
            if (button.is(":checked"))
            {
                grid.scrollRowIntoView(grid.getDataLength());
            }
        }
    }
}

//-----------------------------------------------------------------------------

function clearErrors(htmlText)
{
    tcds.errors = [];
}

//-----------------------------------------------------------------------------

function addError(htmlText)
{
    tcds.errors.push(htmlText);
}

//-----------------------------------------------------------------------------

function showErrors()
{
    var numErrors = tcds.errors.length;
    if (numErrors > 0)
    {
        // Show the errors.
        var tmp = "";
        for (var i=0; i < numErrors; ++i)
        {
            if (i != 0)
            {
                tmp += "\n";
            }
            tmp += tcds.errors[i];
        }

        if (typeof tcds.logThing === 'undefined')
        {
            var stack_bar_top = new PNotify.Stack({modal: false,
                                                   dir1: 'down',
                                                   firstpos1: 0,
                                                   spacing1: 0,
                                                   push: 'top'});
            var opts = {
                type: 'error',
                title: '',
                text: tmp,
                stack: stack_bar_top,
                addClass: 'stack-bar-top bigandproud',
                addModelessClass: 'nonblock',
                hide: false,
                shadow: false,
                icon: false,
                styling: 'tcdstheme',
                width: '100%'
            };
            tcds.logThing = PNotify.alert(opts);
        }
        else
        {
            tcds.logThing.update({text: tmp});
        }

        // Mark the page title with a warning.
        var tmp = document.title;
        var ind = tmp.indexOf("(");
        if (ind >= 0)
        {
            tmp = tmp.substring(0, ind);
        }
        tmp = tmp + " (!)";
        document.title = tmp;

        // Add a (slightly hacky) handler to the page header to help
        // us restore AJAX updates more quickly than done
        // automatically if we need to.
        jQuery("#xdaq-header").off("click");
        jQuery("#xdaq-header").on("click", function() {
            ajaxForUpdate();
        });
    }
    else
    {
        console.debug("No problem(s) to report");
        if (typeof tcds.logThing !== 'undefined')
        {
            tcds.logThing.close();
            delete tcds.logThing;
            tcds.logThing = undefined;
        }

        // Remove the hacky handler from the header.
        jQuery("#xdaq-header").off("click");
    }
}

//-----------------------------------------------------------------------------

function showProblems()
{
    // NOTE: This is something that only applies to the TCDSCentral, really.

    // As long as there are any issues with the TCDSCentral
    // application itself, we don't confuse the situation any further.
    const numErrors = tcds.errors.length;
    if (numErrors == 0)
    {
        var problems = [];
        const appGroupStringStart = 'itemset-appsinfo-';
        const appGroups = Object.keys(tcds.data).filter(a => a.startsWith(appGroupStringStart));
        appGroups.forEach(function(group) {
            const numProblems = tcds.data[group]['Status of applications'].filter(a => a['applicationState'] != "All OK").length;
            if (numProblems != 0)
            {
                const groupName = group.replace(appGroupStringStart, '');
                const verb = (numProblems == 1) ? "is" : "are";
                const s = (numProblems == 1) ? "" : "s";
                const msg = "There " + verb + " " + numProblems + " '" + groupName + "' application" + s + " with problems";
                problems.push(msg);
            }
        });

        if (problems.length > 0)
        {
            // Gather all problems.
            var tmp = "";
            for (var i=0; i < problems.length; ++i)
            {
                if (i != 0)
                {
                    tmp += "\n";
                }
                tmp += problems[i];
            }
            if (typeof tcds.logThing === 'undefined')
            {
                var stack_bar_top = new PNotify.Stack({modal: false,
                                                       dir1: 'down',
                                                       firstpos1: 0,
                                                       spacing1: 0,
                                                       push: 'top'});
                var opts = {
                    type: 'warning',
                    title: '',
                    text: tmp,
                    stack: stack_bar_top,
                    addClass: 'stack-bar-top bigandproud',
                    addModelessClass: 'nonblock',
                    hide: false,
                    shadow: false,
                    icon: false,
                    styling: 'tcdstheme',
                    width: '100%'
                };
                tcds.logThing = PNotify.alert(opts);
            }
            else
            {
                tcds.logThing.update({text: tmp});
            }

        // // Mark the page title with a warning.
        // var tmp = document.title;
        // var ind = tmp.indexOf("(");
        // if (ind >= 0)
        // {
        //     tmp = tmp.substring(0, ind);
        // }
        // tmp = tmp + " (!)";
        // document.title = tmp;

        }
        else
        {
            if (typeof tcds.logThing !== 'undefined')
            {
                tcds.logThing.close();
                delete tcds.logThing;
                tcds.logThing = undefined;
            }

        }
    }
}

//-----------------------------------------------------------------------------

function clearAjaxStatus(htmlText)
{
    tcds.ajaxStatus = [];
}

//-----------------------------------------------------------------------------

function addAjaxStatus(htmlText)
{
    tcds.ajaxStatus.push(htmlText);
}

//-----------------------------------------------------------------------------

function showAjaxStatus()
{
    var msgBase = tcds.ajaxStatus[0];
    var type = "notice";
    if (msgBase.indexOf("success") > -1)
    {
        type = "success";
    }
    else if (msgBase.indexOf("failure") > -1)
    {
        type = "error";
    }

    var num = tcds.ajaxStatus.length;
    if (num > 0)
    {
        var tmp = "";
        for (var i=0; i < num; ++i)
        {
            if (i != 0)
            {
                tmp += "\n";
            }
            tmp += tcds.ajaxStatus[i];
        }
    }

    if (typeof tcds.ajaxStatusThing === 'undefined')
    {
        var stack_bottom_right = new PNotify.Stack({dir1: "up",
                                                    dir2: "left",
                                                    firstpos1: 1,
                                                    firstpos2: 1});
        var opts = {
            type: type,
            title: "",
            text: tmp,
            stack: stack_bottom_right,
            addClass: "stack-bottom-right smallandtimid ajax_status_thing",
            width: "auto",
            hide: false,
            shadow: false,
            icon: false,
            styling: "tcdstheme"
        };
        tcds.ajaxStatusThing = PNotify.alert(opts);
    }
    else
    {
        tcds.ajaxStatusThing.update({
            type: type,
            text: tmp
        });
    }
}

//-----------------------------------------------------------------------------

function configureTTCSpy()
{
    // Extract the parameters from the form.
    var loggingMode = jQuery("#loggingMode").val();
    var triggerTermCombinationOperator = jQuery("#triggerTermCombinationOperator").val();
    var l1a = jQuery("#l1a").is(":checked");
    var brcAll = jQuery("#brcAll").is(":checked");
    var addAll = jQuery("#addAll").is(":checked");
    var brcBC0 = jQuery("#brcBC0").is(":checked");
    var brcEC0 = jQuery("#brcEC0").is(":checked");
    var brcDDDDAll = jQuery("#brcDDDDAll").is(":checked");
    var brcTTAll = jQuery("#brcTTAll").is(":checked");
    var brcZeroData = jQuery("#brcZeroData").is(":checked");
    var adrZeroData = jQuery("#adrZeroData").is(":checked");
    var errCom = jQuery("#errCom").is(":checked");
    var brcDDDD = jQuery("#brcDDDD").val();
    var brcTT = jQuery("#brcTT").val();
    var brcVal0 = jQuery("#brcVal0").val();
    var brcVal1 = jQuery("#brcVal1").val();
    var brcVal2 = jQuery("#brcVal2").val();
    var brcVal3 = jQuery("#brcVal3").val();
    var brcVal4 = jQuery("#brcVal4").val();
    var brcVal5 = jQuery("#brcVal5").val();
    var parameters = [
        {
            "name" : "loggingMode",
            "type" : "string",
            "value" : loggingMode
        },
        {
            "name" : "triggerTermCombinationOperator",
            "type" : "string",
            "value" : triggerTermCombinationOperator
        },
        {
            "name" : "l1a",
            "type" : "boolean",
            "value" : l1a
        },
        {
            "name" : "brcAll",
            "type" : "boolean",
            "value" : brcAll
        },
        {
            "name" : "addAll",
            "type" : "boolean",
            "value" : addAll
        },
        {
            "name" : "brcBC0",
            "type" : "boolean",
            "value" : brcBC0
        },
        {
            "name" : "brcEC0",
            "type" : "boolean",
            "value" : brcEC0
        },
        {
            "name" : "brcDDDDAll",
            "type" : "boolean",
            "value" : brcDDDDAll
        },
        {
            "name" : "brcTTAll",
            "type" : "boolean",
            "value" : brcTTAll
        },
        {
            "name" : "brcZeroData",
            "type" : "boolean",
            "value" : brcZeroData
        },
        {
            "name" : "adrZeroData",
            "type" : "boolean",
            "value" : adrZeroData
        },
        {
            "name" : "errCom",
            "type" : "boolean",
            "value" : errCom
        },
        {
            "name" : "brcDDDD",
            "type" : "unsignedInt",
            "value" : brcDDDD
        },
        {
            "name" : "brcTT",
            "type" : "unsignedInt",
            "value" : brcTT
        },
        {
            "name" : "brcVal0",
            "type" : "unsignedInt",
            "value" : brcVal0
        },
        {
            "name" : "brcVal1",
            "type" : "unsignedInt",
            "value" : brcVal1
        },
        {
            "name" : "brcVal2",
            "type" : "unsignedInt",
            "value" : brcVal2
        },
        {
            "name" : "brcVal3",
            "type" : "unsignedInt",
            "value" : brcVal3
        },
        {
            "name" : "brcVal4",
            "type" : "unsignedInt",
            "value" : brcVal4
        },
        {
            "name" : "brcVal5",
            "type" : "unsignedInt",
            "value" : brcVal5
        }
    ];

    // Build the SOAP message and send it off.
    var soapMsg = buildSOAPCommand("ConfigureTTCSpy",
                                   parameters);
    sendSOAPCommand(soapMsg);
}

//-----------------------------------------------------------------------------

function configureRandomRate()
{
    // Extract the parameters from the form.
    var frequency = jQuery("#frequency").val();
    var cmdName = "";
    var parameters = [];

    if (frequency != 0)
    {
        cmdName = "EnableRandomTriggers"
        parameters = [
            {
                "name" : "frequency",
                "type" : "unsignedInt",
                "value" : frequency
            },
        ];
    }
    else
    {
        cmdName = "DisableRandomTriggers"
        parameters = [];
    }

    // Build the SOAP message and send it off.
    var soapMsg = buildSOAPCommand(cmdName, parameters);
    sendSOAPCommand(soapMsg);
}

//-----------------------------------------------------------------------------

function launchPatternTrigger()
{
    var cmdName = "SendL1APattern"
    parameters = [];

    // Build the SOAP message and send it off.
    var soapMsg = buildSOAPCommand(cmdName, parameters);
    sendSOAPCommand(soapMsg);
}

//-----------------------------------------------------------------------------

function haltPatternTrigger()
{
    var cmdName = "StopL1APattern"
    parameters = [];

    // Build the SOAP message and send it off.
    var soapMsg = buildSOAPCommand(cmdName, parameters);
    sendSOAPCommand(soapMsg);
}

//-----------------------------------------------------------------------------

function requestSystemDump()
{
    // Extract the parameters from the form.
    var reason = jQuery("#reason").val();
    var cmdName = "DumpSystemState";
    var parameters = [
        {
            "name" : "reason",
            "type" : "string",
            "value" : reason
        }
    ]

    // Build the SOAP message and send it off.
    var soapMsg = buildSOAPCommand(cmdName, parameters);
    sendSOAPCommand(soapMsg);
}

//-----------------------------------------------------------------------------

function buildSOAPCommand(cmdName, params)
{
    var soapMsg = "<?xml version=\"1.0\" ?>" +
        "<env:Envelope xmlns:env=\"http://www.w3.org/2003/05/soap-envelope\"" +
        "              xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"" +
        "              xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">" +
        "<env:Header/>" +
        "<env:Body>" +
        "<xdaq:" + cmdName + " xmlns:xdaq=\"urn:xdaq-soap:3.0\">";

    for (var i = 0; i < params.length; ++i)
    {
        var par = params[i];
        soapMsg += "<xdaq:" + par.name + " xsi:type=\"xsd:" + par.type + "\">" +
            par.value +
            "</xdaq:" + par.name + ">";
    }

    soapMsg += "</xdaq:" + cmdName + ">" +
        "</env:Body>" +
        "</env:Envelope>";

    soapMsg = jQuery.trim(soapMsg);
    return soapMsg;
}

//-----------------------------------------------------------------------------

function sendSOAPCommand(soapMsg)
{
    jQuery.ajax({
        type: "post",
        url: tcds.applicationUrl,
        contentType: "text/xml",
        dataType: "xml",
        data: soapMsg,
        processData: false,
        success: function(data, textStatus, req) {
            var fault = jQuery(data).find("env\\:Fault");
            if (fault.length)
            {
                var msg = "Something went wrong";
                var tmpFaultString = fault.find("faultstring");
                var tmpDetail = fault.find("detail");
                if (!tmpFaultString.size() && !tmpDetail)
                {
                    msg = msg + ", but it is not clear exactly what."
                }
                else
                {
                    if (tmpFaultString.size())
                    {
                        msg = msg + ": " + tmpFaultString.text();
                    }
                    if (tmpDetail.size())
                    {
                        if (tmpFaultString.size())
                        {
                            msg = msg + ": ";
                        }
                        msg += tmpDetail.text();
                    }
                }
                alert(msg);
            }
        },
        error: function(data, status, req) {
            alert(req.responseText + " " + status);
        }
    });
}

//-----------------------------------------------------------------------------

function fixupTTCSpyConfigButton(hook)
{
    hook.append("<button id=\"button_configure_ttcspy\">Modify TTCSpy configuration</button>");

    jQuery("body").append("<div id=\"dialog_form_ttcspy\" title=\"Modify TTCSpy configuration\">" +
                          "<form>" +

                          "<fieldset>" +
                          "<legend>Logging configuration:</legend>" +

                          // Logging mode.
                          "<label for=\"loggingMode\">Logging mode</label>" +
                          "<select id=\"loggingMode\" name=\"loggingMode\">" +
                          "<option value=\"LogOnly\">LogOnly</option>" +
                          "<option value=\"LogAllExcept\">LogAllExcept</option>" +
                          "</select>" +

                          "</fieldset>" +

                          "<fieldset>" +
                          "<legend>Trigger configuration:</legend>" +

                          // Trigger-term combination operator.
                          "<label for=\"triggerTermCombinationOperator\">Logging logic operator</label>" +
                          "<select id=\"triggerTermCombinationOperator\" name=\"triggerTermCombinationOperator\">" +
                          "<option value=\"OR\">OR</option>" +
                          "<option value=\"AND\">AND</option>" +
                          "</select>" +

                          // L1As.
                          "<label for=\"bcrL1A\">Trigger on L1As</label>" +
                          "<input type=\"checkbox\" id=\"l1a\" name=\"l1a\"/>" +

                          // brcAll.
                          "<label for=\"bcrAll\">Trigger on all broadcast B-commands</label>" +
                          "<input type=\"checkbox\" id=\"brcAll\" name=\"brcAll\"/>" +

                          // addAll.
                          "<label for=\"bcrAll\">Trigger on all addressed B-commands</label>" +
                          "<input type=\"checkbox\" id=\"addAll\" name=\"addAll\"/>" +

                          // brcBC0.
                          "<label for=\"bcrBC0\">Trigger on broadcast B-commands with the bunch-counter reset bit set</label>" +
                          "<input type=\"checkbox\" id=\"brcBC0\" name=\"brcBC0\"/>" +

                          // brcEC0.
                          "<label for=\"bcrEC0\">Trigger on broadcast B-commands with the event-counter reset bit set</label>" +
                          "<input type=\"checkbox\" id=\"brcEC0\" name=\"brcEC0\"/>" +

                          // brcDDDDAll.
                          "<label for=\"bcrDDDDAll\">Trigger on all broadcast B-commands with non-zero user-data bits</label>" +
                          "<input type=\"checkbox\" id=\"brcDDDDAll\" name=\"brcDDDDAll\"/>" +

                          // brcTTAll.
                          "<label for=\"bcrTTAll\">Trigger on all broadcast B-commands with non-zero test-data bits</label>" +
                          "<input type=\"checkbox\" id=\"brcTTAll\" name=\"brcTTAll\"/>" +

                          // brcZeroData.
                          "<label for=\"brcZeroData\">Trigger on all broadcast B-commands with zero data payload</label>" +
                          "<input type=\"checkbox\" id=\"brcZeroData\" name=\"brcZeroData\"/>" +

                          // adrZeroData.
                          "<label for=\"adrZeroData\">Trigger on all addressed B-commands with zero data payload</label>" +
                          "<input type=\"checkbox\" id=\"adrZeroData\" name=\"adrZeroData\"/>" +

                          // errCom.
                          "<label for=\"errComm\">Trigger on all communication errors</label>" +
                          "<input type=\"checkbox\" id=\"errCom\" name=\"errCom\"/>" +

                          // brcDDDD.
                          "<label for=\"bcrDDDD\">Trigger on broadcast B-commands with this value in the user-data bits (if non-zero)</label>" +
                          "<input type=\"text\" id=\"brcDDDD\" name=\"brcDDDD\"/>" +

                          // brcTT.
                          "<label for=\"bcrTT\">Trigger on broadcast B-commands with this value in the test-data bits (if non-zero)</label>" +
                          "<input type=\"text\" id=\"brcTT\" name=\"brcTT\"/>" +

                          // brcVal0.
                          "<label for=\"bcrVal0\">Trigger on broadcast B-commands with this value in the (8-bit) data (1 of 6)</label>" +
                          "<input type=\"text\" id=\"brcVal0\" name=\"brcVal0\"/>" +

                          // brcVal1.
                          "<label for=\"bcrVal1\">Trigger on broadcast B-commands with this value in the (8-bit) data (2 of 6)</label>" +
                          "<input type=\"text\" id=\"brcVal1\" name=\"brcVal1\"/>" +

                          // brcVal2.
                          "<label for=\"bcrVal2\">Trigger on broadcast B-commands with this value in the (8-bit) data (3 of 6)</label>" +
                          "<input type=\"text\" id=\"brcVal2\" name=\"brcVal2\"/>" +

                          // brcVal3.
                          "<label for=\"bcrVal3\">Trigger on broadcast B-commands with this value in the (8-bit) data (4 of 6)</label>" +
                          "<input type=\"text\" id=\"brcVal3\" name=\"brcVal3\"/>" +

                          // brcVal4.
                          "<label for=\"bcrVal4\">Trigger on broadcast B-commands with this value in the (8-bit) data (5 of 6)</label>" +
                          "<input type=\"text\" id=\"brcVal4\" name=\"brcVal4\"/>" +

                          // brcVal5.
                          "<label for=\"bcrVal5\">Trigger on broadcast B-commands with this value in the (8-bit) data (6 of 6)</label>" +
                          "<input type=\"text\" id=\"brcVal5\" name=\"brcVal5\"/>" +

                          "</fieldset>" +

                          "<!-- Allow form submission with keyboard without duplicating the dialog button -->" +
                          "<input type=\"submit\" tabindex=\"-1\" style=\"position:absolute; top:-1000px\">" +

                          "</form>" +
                          "</div>");

    var dialog = jQuery("#dialog_form_ttcspy").dialog({
        autoOpen: false,
        autoResize: true,
        height: "auto",
        width: "40%",
        modal: true,
        resizable: false,
        buttons: {
            "Submit": function() {
                configureTTCSpy();
                dialog.dialog("close");
            },
            Cancel: function() {
                dialog.dialog("close");
            }
        },
        close: function() {
            form[0].reset();
            //allFields.removeClass("ui-state-error");
        }
    });

    var form = dialog.find("form").on("submit", function(event) {
        event.preventDefault();
        event.stopPropagation();
        configureTTCSpy();
        dialog.dialog("close");
    });

    jQuery("#button_configure_ttcspy").button().on("click", function() {
        // Fill in the current values, based on the data we have.
        const data = tcds.data;

        var loggingMode = data['itemset-ttcspylogging']['Logging mode'];

        var triggerTermCombinationOperator = data['itemset-ttcspytrigger']['Logging trigger-term combination operator'];
        var l1a = data['itemset-ttcspytrigger']['Trigger on L1As'];
        var brcAll = data['itemset-ttcspytrigger']['Trigger on all broadcast B-commands'];
        var addAll = data['itemset-ttcspytrigger']['Trigger on all addressed B-commands'];
        var brcBC0 = data['itemset-ttcspytrigger']['Trigger on broadcast B-commands with the BC0 bit set'];
        var brcEC0 = data['itemset-ttcspytrigger']['Trigger on broadcast B-commands with the EC0 bit set'];
        var brcDDDD = data['itemset-ttcspytrigger']['Trigger on broadcast B-commands with this value in the user-data bits (if non-zero)'];
        var brcTT = data['itemset-ttcspytrigger']['Trigger on broadcast B-commands with this value in the test-data bits (if non-zero)'];
        var brcDDDDAll = data['itemset-ttcspytrigger']['Trigger on all broadcast B-commands with non-zero user-data bits'];
        var brcTTAll = data['itemset-ttcspytrigger']['Trigger on all broadcast B-commands with non-zero test-data bits'];
        var brcZeroData = data['itemset-ttcspytrigger']['Trigger on all broadcast B-commands with zero data payload'];
        var adrZeroData = data['itemset-ttcspytrigger']['Trigger on all addressed B-commands with zero data payload'];
        var errCom = data['itemset-ttcspytrigger']['Trigger on all communication errors'];
        var brcVal0 = data['itemset-ttcspytrigger']['Trigger on broadcast B-commands with this value in the (8-bit) data (1 of 6)'];
        var brcVal1 = data['itemset-ttcspytrigger']['Trigger on broadcast B-commands with this value in the (8-bit) data (2 of 6)'];
        var brcVal2 = data['itemset-ttcspytrigger']['Trigger on broadcast B-commands with this value in the (8-bit) data (3 of 6)'];
        var brcVal3 = data['itemset-ttcspytrigger']['Trigger on broadcast B-commands with this value in the (8-bit) data (4 of 6)'];
        var brcVal4 = data['itemset-ttcspytrigger']['Trigger on broadcast B-commands with this value in the (8-bit) data (5 of 6)'];
        var brcVal5 = data['itemset-ttcspytrigger']['Trigger on broadcast B-commands with this value in the (8-bit) data (6 of 6)'];
        jQuery("#loggingMode").val(loggingMode);
        jQuery("#triggerTermCombinationOperator").val(triggerTermCombinationOperator);
        jQuery("#l1a").prop("checked", l1a=="true");
        jQuery("#brcAll").prop("checked", brcAll=="true");
        jQuery("#addAll").prop("checked", addAll=="true");
        jQuery("#brcBC0").prop("checked", brcBC0=="true");
        jQuery("#brcEC0").prop("checked", brcEC0=="true");
        jQuery("#brcDDDD").val(brcDDDD);
        jQuery("#brcTT").val(brcTT);
        jQuery("#brcDDDDAll").prop("checked", brcDDDDAll=="true");
        jQuery("#brcTTAll").prop("checked", brcTTAll=="true");
        jQuery("#brcZeroData").prop("checked", brcZeroData=="true");
        jQuery("#adrZeroData").prop("checked", adrZeroData=="true");
        jQuery("#errCom").prop("checked", errCom=="true");
        jQuery("#brcVal0").val(brcVal0);
        jQuery("#brcVal1").val(brcVal1);
        jQuery("#brcVal2").val(brcVal2);
        jQuery("#brcVal3").val(brcVal3);
        jQuery("#brcVal4").val(brcVal4);
        jQuery("#brcVal5").val(brcVal5);

        // Open the dialog.
        dialog.dialog("open");
    });
}

//-----------------------------------------------------------------------------

function fixupRandomRateConfigButton(hook)
{
    hook.append("<br/>");
    hook.append("<button id=\"button_configure_randomrate\">Modify random-trigger rate</button>");

    jQuery("body").append("<div id=\"dialog_form_randomrate\" title=\"Modify random-trigger rate\">" +
                          "<form>" +

                          "<fieldset>" +

                          "<label for=\"frequency\">Requested rate (Hz)</label>" +
                          "<input type=\"number\" min=\"0\" id=\"frequency\" name=\"frequency\"/>" +

                          "</fieldset>" +

                          "<!-- Allow form submission with keyboard without duplicating the dialog button -->" +
                          "<input type=\"submit\" tabindex=\"-1\" style=\"position:absolute; top:-1000px\">" +

                          "</form>" +
                          "</div>");

    var dialog = jQuery("#dialog_form_randomrate").dialog({
        autoOpen: false,
        autoResize: true,
        height: "auto",
        width: "40%",
        modal: true,
        resizable: false,
        buttons: {
            "Submit": function() {
                configureRandomRate();
                dialog.dialog("close");
            },
            Cancel: function() {
                dialog.dialog("close");
            }
        },
        close: function() {
            form[0].reset();
        }
    });

    var form = dialog.find("form").on("submit", function(event) {
        event.preventDefault();
        event.stopPropagation();
        configureRandomRate();
        dialog.dialog("close");
    });

    jQuery("#button_configure_randomrate").button().on("click", function() {
        // Fill in the current values.
        var frequency = tcds.data['itemset-random-trigger']['Requested random-trigger rate (Hz)'];
        jQuery("#frequency").val(frequency);

        // Open the dialog.
        dialog.dialog("open");
    });
}

//-----------------------------------------------------------------------------

function fixupPatternTriggerLaunchButton(hook)
{
    hook.append("<br/>");
    hook.append("<button id=\"button_launch_pattern_trigger\">Launch L1A pattern</button>");

    jQuery("#button_launch_pattern_trigger").button().on("click", function() {
        launchPatternTrigger();
    });
}

//-----------------------------------------------------------------------------

function fixupPatternTriggerHaltButton(hook)
{
    hook.append("<br/>");
    hook.append("<button id=\"button_halt_pattern_trigger\">Halt L1A pattern</button>");

    jQuery("#button_halt_pattern_trigger").button().on("click", function() {
        haltPatternTrigger();
    });
}

//-----------------------------------------------------------------------------

function fixupSystemDumpButton(hook)
{
    hook.after("<br><button id=\"button_system_dump\">Dump system state</button>");

    jQuery("body").append("<div id=\"dialog_form_dumpreason\" title=\"Specify reason for system-state dump\">" +

                          "<p>NOTE: A full system dump takes quite a long time, " +
                          "creates lots of output, " +
                          "and generates several very large emails. " +
                          "Please only use when necessary.</p>" +

                          "<form>" +

                          "<fieldset>" +

                          "<label for=\"reason\">Reason for dump:</label>" +
                          "<input type=\"string\" id=\"reason\" name=\"reason\" size=\"90%\"/>" +

                          "</fieldset>" +

                          "<!-- Allow form submission with keyboard without duplicating the dialog button -->" +
                          "<input type=\"submit\" tabindex=\"-1\" style=\"position:absolute; top:-1000px\">" +

                          "</form>" +
                          "</div>");

    var dialog = jQuery("#dialog_form_dumpreason").dialog({
        autoOpen: false,
        autoResize: true,
        height: "auto",
        width: "40%",
        modal: true,
        resizable: false,
        buttons: {
            "Submit": function() {
                requestSystemDump();
                dialog.dialog("close");
            },
            Cancel: function() {
                dialog.dialog("close");
            }
        },
        close: function() {
            form[0].reset();
            //allFields.removeClass("ui-state-error");
        }
    });

    var form = dialog.find("form").on("submit", function(event) {
        event.preventDefault();
        event.stopPropagation();
        requestSystemDump();
        dialog.dialog("close");
    });

    jQuery("#button_system_dump").button().on("click", function() {
        // Open the dialog.
        dialog.dialog("open");
    });
}

//-----------------------------------------------------------------------------

function L1AHistos(containerId)
{
    const bxMin = 1;
    const bxMax = 3564;
    const maxNumBins = 100;
    const colors = tcds.colors;

    var bxNumbers = [];
    for (var i = bxMin; i <= bxMax; ++i) {
        bxNumbers.push(i);
    }

    const options = {
        scales: {
            x: {
                title: {
                    display: true,
                    text: 'BX',
                    font: {
                        weight: 'bold'
                    }
                },
                type: 'linear',
                min: bxMin,
                max: bxMax,
                offset: 0,
                stacked: true,
                grid: {
                    display: false
                },
                ticks: {
                    // stepSize: 100,
                    precision: 0,
                    callback(value, index, values) {
                        return value.toString();
                    }
                }
            },
            y: {
                title: {
                    display: true,
                    text: 'L1A count',
                    font: {
                        weight: 'bold'
                    }
                },
                stacked: true,
                grid: {
                    display: false
                },
                ticks: {
                    stepSize: 1,
                    precision: 0
                }
            }
        },
        plugins: {
            zoom: {
                zoom: {
                    wheel: {
                        enabled: false
                    },
                    pinch: {
                        enabled: false
                    },
                    drag: {
                        enabled: true
                    },
                    mode: 'x',
                    onZoomComplete({chart}) {
                        var tmpLabelMin = chart.scales.x.ticks[0].label;
                        var tmpMin = tmpLabelMin.indexOf(':') > -1 ? tmpLabelMin.split(':')[0] : tmpLabelMin;
                        chart.xMin = parseInt(tmpMin);
                        var tmpLabelMax = chart.scales.x.ticks[chart.scales.x.ticks.length - 1].label;
                        var tmpMax = tmpLabelMax.indexOf(':') > -1 ? tmpLabelMax.split(':')[1] : tmpLabelMax;
                        chart.xMax = parseInt(tmpMax);
                    }
                },
                pan: {
                    enabled: true,
                    mode: 'x',
                    modifierKey: 'alt'
                },
                limits: {
                    x: {
                        min: bxMin,
                        max: bxMax
                    }
                }
            },
            tooltip: {
                mode: 'x',
                callbacks: {
                    title: function(context) {
                        // return "BX " + context[0].label;
                        return "";
                    }
                }
            }
        },
        onClick: (e) => {
            this.chart.resetZoom('none');
        }
    }

    this.containerId = containerId;
    this.chart = undefined;
    this.labels = undefined;
    this.data = undefined;

    this.init = function()
    {
        const config = {
            type: 'bar',
            data: null,
            options: options,
            histos: this
        };

        this.chart = new Chart(
            document.getElementById(this.containerId),
            config
        );
        this.chart.xMin = bxMin;
        this.chart.xMax = bxMax;
    }

    this.setData = function(dataRaw)
    {
        // These are not real histograms, but rather category bar
        // plots. And the category labels are the same for all data
        // series.
        this.labels = bxNumbers;

        // Filter out all data series that have empty histograms (as
        // in: no L1As of that type at all).
        this.data = [];
        for (var i = 0; i != dataRaw.length; ++i)
        {
            if (this.sum(dataRaw[i].data) > 0)
            {
                var tmp = [];
                for (var j = 0; j != dataRaw[i].data.length; ++j)
                {
                    tmp.push(dataRaw[i].data[j]);
                }
                this.data.push({label: dataRaw[i].label,
                                data: tmp,
                                backgroundColor: colors[i],
                                hoverBackgroundColor: Chart.helpers.color(colors[i]).darken(0.2).rgbString(),
                                borderColor: Chart.helpers.color(colors[i]).darken(0.2).rgbString(),
                                hoverBorderColor: Chart.helpers.color(colors[i]).darken(0.2).rgbString(),
                                borderWidth: 1,
                                hoverBorderWidth: 1,
                                barThickness: 'flex',
                                barPercentage: 1.0,
                                categoryPercentage: 1.0});
            }
        }
    }

    this.update = function()
    {
        var xMin = this.chart.xMin;
        var xMax = this.chart.xMax;

        var rebinFactor = this.getRebinFactor(xMin, xMax);

        var plotLabels = this.rebinLabels(bxNumbers, rebinFactor);
        var plotData = this.rebinData(this.data, rebinFactor);

        this.chart.data.labels = plotLabels;
        this.chart.data.datasets = plotData;
        // Update the chart, without transition animation.
        this.chart.update('none');
    }

    this.getRebinFactor = function(xMin, xMax)
    {
        // Determine if we need to rebin the data or not.
        const numBins = (xMax - xMin + 1);
        var rebinFactor;
        if (numBins > maxNumBins)
        {
            const ratio = numBins / maxNumBins;
            rebinFactor = this.findDivisor(ratio, numBins);
        }
        else
        {
            rebinFactor = 1;
        }
        return rebinFactor;
    }

    this.getData = function(xMin, xMax)
    {
        // Create a sliced copy of the part of the data we want.
        var dataTmp = [];
        var tmpObj;
        var tmpDat;
        for (var i = 0; i != this.data.length; ++i)
        {
            tmpDat = this.data[i]["data"].slice(xMin - 1, xMax);
            tmpObj = jQuery.extend({}, this.data[i]);
            tmpObj.data = tmpDat;
            dataTmp.push(tmpObj);
        }
    }

    this.rebinLabels = function(labels, rebinFactor)
    {
        // NOTE: The assumption is that rebinFactor is a divisor of
        // the range spanned by the labels.

        var tmp = this.rebinHelper(labels, rebinFactor);
        var labelsCropped = tmp.map(function(item) {return item / rebinFactor});
        return labelsCropped;
    }

    this.rebinData = function(data, rebinFactor)
    {
        // NOTE: The assumption is that rebinFactor is a divisor of
        // data.length.
        var dataCropped = [];
        for (var i = 0; i != data.length; ++i)
        {
            tmpDat = this.rebinHelper(data[i]["data"], rebinFactor);
            tmpObj = jQuery.extend({}, data[i]);
            tmpObj.data = tmpDat;
            dataCropped.push(tmpObj);
        }
        return dataCropped;
    }

    this.rebinHelper = function(data, n)
    {
        // NOTE: The assumption is that n is a divisor of data.length.
        var dataRebinned = [];
        for (var i = 0; i < data.length; i += n)
        {
            var slice = data.slice(i, i + n);
            var tmp = slice.reduce(function(total, entry) {return total + entry;}, 0);
            dataRebinned.push(tmp);
        }
        return dataRebinned;
    }

    this.sum = function(values)
    {
        return values.reduce(function(a, b) {return a + b;})
    }

    this.findDivisor = function(val, n)
    {
        const divisors = this.getDivisors(n);
        var res;
        if (divisors.length > 2)
        {
            for (var i = 0; i < divisors.length; ++i)
            {
                if ((divisors[i + 1] >= val) && (divisors[i] < val))
                {
                    var tmpLo = Math.abs(val - divisors[i]);
                    var tmpHi = Math.abs(val - divisors[i + 1]);
                    if (tmpLo < tmpHi)
                    {
                        res = divisors[i];
                    }
                    else
                    {
                        res = divisors[i + 1];
                    }
                    break;
                }
            }
        }
        else
        {
            res = 1;
        }
        return res;
    }

    this.getDivisors = function(n)
    {
        if (n < 1)
        {
            throw "ParameterError: expected N >= 1 but received N = " + n + ".";
        }
        var small = [];
        var large = [];
        const stop = Math.floor(Math.sqrt(n));
        for (var i = 1; i <= stop; ++i)
        {
            if ((n % i) == 0)
            {
                small.push(i);
                if ((i * i) != n)
                {
                    large.push(n / i);
                }
            }
        }
        large.reverse();
        var res = small.concat(large);
        return res
    }
}

//-----------------------------------------------------------------------------

function FreqMonTrend(containerId)
{
    const options = {
        plugins: {
            legend: {
                display: false
            }
        },
        maintainAspectRatio: false,
        scales: {
            x: {
                type: 'time',
                time: {
                    minUnit: 'second',
                    displayFormats: {second: 'HH:mm:ss',
                                     minute: 'HH:mm:ss',
                                     hour: 'HH:mm:ss'},
                    tooltipFormat: 'HH:mm:ss'
                }
            },
            y: {
                ticks: {
                    precision: 0
                }
            }
        }
    };

    this.containerId = containerId
    this.chart = undefined;
    this.labels = undefined;
    this.data = undefined;

    this.init = function()
    {
        const config = {
            type: 'line',
            data: null,
            options: options
        };

        this.chart = new Chart(
            document.getElementById(this.containerId),
            config
        );
    }

    this.setData = function(dataRaw)
    {
        var tmpLabels = [];
        var tmpData = [];
        for (var i = 0; i != dataRaw.data.length; ++i)
        {
            tmpLabels.push(dataRaw.data[i][0]);
            tmpData.push(dataRaw.data[i][1]);
        }
        this.labels = tmpLabels;
        this.data = [{label: dataRaw.label,
                      data: tmpData,
                      color: tcds.colors[1],
                      backgroundColor: tcds.colors[1],
                      tension: 0.1}];
    }

    this.update = function()
    {
        var plotLabels = this.labels;
        var plotData = this.data;

        this.chart.data.labels = plotLabels;
        this.chart.data.datasets = plotData;
        // Update the chart, without transition animation.
        this.chart.update('none');
    }
}

//-----------------------------------------------------------------------------

// A string cleaner copy-pasted from here:
//   http://phpjs.org/functions/htmlspecialchars

function htmlspecialchars(string, quote_style, charset, double_encode) {
    //       discuss at: http://phpjs.org/functions/htmlspecialchars/
    //      original by: Mirek Slugen
    //      improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    //      bugfixed by: Nathan
    //      bugfixed by: Arno
    //      bugfixed by: Brett Zamir (http://brett-zamir.me)
    //      bugfixed by: Brett Zamir (http://brett-zamir.me)
    //       revised by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    //         input by: Ratheous
    //         input by: Mailfaker (http://www.weedem.fr/)
    //         input by: felix
    // reimplemented by: Brett Zamir (http://brett-zamir.me)
    //             note: charset argument not supported
    //        example 1: htmlspecialchars("<a href='test'>Test</a>", 'ENT_QUOTES');
    //        returns 1: '&lt;a href=&#039;test&#039;&gt;Test&lt;/a&gt;'
    //        example 2: htmlspecialchars("ab\"c'd", ['ENT_NOQUOTES', 'ENT_QUOTES']);
    //        returns 2: 'ab"c&#039;d'
    //        example 3: htmlspecialchars('my "&entity;" is still here', null, null, false);
    //        returns 3: 'my &quot;&entity;&quot; is still here'

    var optTemp = 0,
        i = 0,
        noquotes = false;
    if (typeof quote_style === 'undefined' || quote_style === null) {
        quote_style = 2;
    }
    string = string.toString();
    if (double_encode !== false) { // Put this first to avoid double-encoding
        string = string.replace(/&/g, '&amp;');
    }
    string = string.replace(/</g, '&lt;')
        .replace(/>/g, '&gt;');

    var OPTS = {
        'ENT_NOQUOTES': 0,
        'ENT_HTML_QUOTE_SINGLE': 1,
        'ENT_HTML_QUOTE_DOUBLE': 2,
        'ENT_COMPAT': 2,
        'ENT_QUOTES': 3,
        'ENT_IGNORE': 4
    };
    if (quote_style === 0) {
        noquotes = true;
    }
    if (typeof quote_style !== 'number') { // Allow for a single string or an array of string flags
        quote_style = [].concat(quote_style);
        for (i = 0; i < quote_style.length; i++) {
            // Resolve string input to bitwise e.g. 'ENT_IGNORE' becomes 4
            if (OPTS[quote_style[i]] === 0) {
                noquotes = true;
            } else if (OPTS[quote_style[i]]) {
                optTemp = optTemp | OPTS[quote_style[i]];
            }
        }
        quote_style = optTemp;
    }
    if (quote_style & OPTS.ENT_HTML_QUOTE_SINGLE) {
        string = string.replace(/'/g, '&#039;');
    }
    if (!noquotes) {
        string = string.replace(/"/g, '&quot;');
    }

    return string;
}

//-----------------------------------------------------------------------------

// Check if the browser considers this proper HTML. Overly strict, but
// okay. For details, see:
//   http://stackoverflow.com/questions/10026626/check-if-html-snippet-is-valid-with-javascript
function isProperHTML(html)
{
    var doc = document.createElement("div");
    doc.innerHTML = html;
    return (doc.innerHTML === html);
}

//-----------------------------------------------------------------------------

function hackIntoHTML(text)
{
    var res = jQuery("<div></div>").html(text).html();
    return res;
}

//-----------------------------------------------------------------------------

// Some debug helper methods (copy-pasted from the internet
// somewhere).

function syntaxHighlight(json)
{
    if (typeof json != 'string') {
        json = JSON.stringify(json, undefined, 2);
    }
    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
        var cls = 'number';
        if (/^"/.test(match)) {
            if (/:$/.test(match)) {
                cls = 'key';
            } else {
                cls = 'string';
            }
        } else if (/true|false/.test(match)) {
            cls = 'boolean';
        } else if (/null/.test(match)) {
            cls = 'null';
        }
        return '<span class="' + cls + '">' + match + '</span>';
    });
}

function prettyPrint(obj)
{
    return JSON.stringify(obj);
}

function getRandomInt(min, max)
{
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

//-----------------------------------------------------------------------------
