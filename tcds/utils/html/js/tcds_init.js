//-----------------------------------------------------------------------------

function getBaseURL()
{
    // Figure out the base URL of the application server.
    var pathName = window.location.pathname;
    if (pathName[0] == "/")
    {
        pathName = pathName.substr(1);
    }
    if (pathName[-1] == "/")
    {
        pathName = pathName.substr(0, -1);
    }
    var pathArray = pathName.split("/");
    var newPathName = pathArray[0];
    var baseUrl = window.location.protocol + "//" + window.location.host;
    return baseUrl;
}

//-----------------------------------------------------------------------------

function getApplicationURL()
{
    // Figure out the application URL.
    var pathName = window.location.pathname;
    if (pathName[0] == "/")
    {
        pathName = pathName.substr(1);
    }
    if (pathName[-1] == "/")
    {
        pathName = pathName.substr(0, -1);
    }
    var pathArray = pathName.split("/");
    var newPathName = pathArray[0];
    var appUrl = window.location.protocol + "//" + window.location.host + "/" + newPathName;
    return appUrl;
}

//-----------------------------------------------------------------------------

function getScript(scriptUrl, forceOrder=false)
{
    console.log("Going to load script " + scriptUrl);
    var elem = document.body.appendChild(document.createElement("script"));
    if (forceOrder)
    {
        elem.async = false;
    }
    elem.setAttribute("src", scriptUrl);
    console.log("Done loading script " + scriptUrl);
}

//-----------------------------------------------------------------------------

function getCSS(cssUrl)
{
    console.log("Going to load stylesheet " + cssUrl);
    var elem = document.body.appendChild(document.createElement("link"));
    elem.setAttribute("rel", "stylesheet");
    elem.setAttribute("href", cssUrl);
    console.log("Done loading stylesheet " + cssUrl);
}

//-----------------------------------------------------------------------------

function isTCDSApplication()
{
    // Here we can use a feature of the XDAQ HyperDAQ framework and
    // check for the 'data-app-name' attribute of the HTML body.

    // NOTE: This of course only works _after_ the DOM has been
    // loaded.

    var res = false;
    var appName = jQuery("body").attr("data-app-name");
    if (typeof appName !== typeof undefined)
    {
        res = appName.startsWith("tcds::");
    }
    return res;
}

//-----------------------------------------------------------------------------

function zeroFill(number, width)
{
    width -= number.toString().length;
    if (width > 0)
    {
        return new Array(width + (/\./.test( number ) ? 2 : 1)).join('0') + number;
    }
    return number + "";
}

//-----------------------------------------------------------------------------

function timestamp()
{
    return new Date();
}

//----------

function timestampStr(stamp=undefined)
{
    if (typeof stamp === 'undefined')
    {
        stamp = timestamp();
    }
    else if (typeof stamp == 'string')
    {
        stamp = new Date(stamp.replace(/-/g, '/'));
    }
    else
    {
        stamp = timestamp();
    }
    return zeroFill(stamp.getHours(), 2)
        + ":"
        + zeroFill(stamp.getMinutes(), 2)
        + ":"
        + zeroFill(stamp.getSeconds() ,2);
}

//-----------------------------------------------------------------------------

// NOTE: All global variables are stuffed into the global 'tcds'
// container.
var tcds = {};
tcds.baseUrl = undefined;
tcds.applicationUrl = undefined;
tcds.updateUrl = undefined;

// How long do we want to wait for an AJAX request before we call it
// timed out?
tcds.ajaxTimeout = 1000;

tcds.defaultUpdateInterval = 1000;
tcds.updateInterval = tcds.defaultUpdateInterval;
tcds.updateFailCount = 0;
tcds.maxUpdateFailCount = 6;

tcds.data = undefined;
tcds.grids = {};
tcds.l1ahistos = undefined;
tcds.freqmontrends = {};
tcds.initialised = false;
tcds.templateCache = {};

// Invalid/unavailable data items in the JSON updates will look like
// this:
tcds.invalidDataStr = "-";

tcds.errors = [];
tcds.logThing = undefined;
tcds.ajaxStatus = [];
tcds.ajaxStatusThing = undefined;

// Source: http://vrl.cs.brown.edu/color.
tcds.colors = ['rgb(243,192,17)', 'rgb(244,109,58)', 'rgb(86,235,211)', 'rgb(17,94,65)', 'rgb(154,232,113)', 'rgb(35,158,179)', 'rgb(179,217,250)', 'rgb(62,71,86)', 'rgb(217,143,244)', 'rgb(95,50,126)', 'rgb(132,130,149)', 'rgb(54,147,242)', 'rgb(191,17,175)', 'rgb(44,165,89)', 'rgb(202,219,165)', 'rgb(130,153,81)']

//----------

// Figure out some URLs.
var baseUrl = getBaseURL();
var applicationUrl = getApplicationURL();
var updateUrl = applicationUrl + "/update";

tcds.baseUrl = baseUrl;
tcds.applicationUrl = applicationUrl;
tcds.updateUrl = updateUrl;

//-----------------------------------------------------------------------------
