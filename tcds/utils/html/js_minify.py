#!/usr/bin/python

import httplib
import urllib
import sys

js_code_pieces = []
for file_name in sys.argv[1:]:
    with open(file_name, "r") as in_file:
        tmp = in_file.read()
        js_code_pieces.append(tmp)
js_code_combined = "".join(js_code_pieces)

# Define the parameters for the POST request and encode them in a
# URL-safe format.
params = urllib.urlencode([
    ('js_code', js_code_combined),
    # ('compilation_level', 'WHITESPACE_ONLY'),
    ('compilation_level', 'SIMPLE_OPTIMIZATIONS'),
    ('output_format', 'text'),
    ('output_info', 'compiled_code'),
])

# Always use the following value for the Content-type header.
headers = {
    "Content-type" : "application/x-www-form-urlencoded"
}
conn = httplib.HTTPSConnection('closure-compiler.appspot.com')
conn.request('POST', '/compile', params, headers)
response = conn.getresponse()
data = response.read()
print data
conn.close()
