#ifndef _tcds_rf2ttc_RF2TTCController_h_
#define _tcds_rf2ttc_RF2TTCController_h_

#include <memory>

#include "toolbox/Event.h"
#include "xdaq/Application.h"

#include "tcds/rf2ttc/VMEDeviceRF2TTC.h"
#include "tcds/utils/RegCheckResult.h"
#include "tcds/utils/SOAPCmdBase.h"
#include "tcds/utils/SOAPCmdDumpHardwareState.h"
#include "tcds/utils/XDAQAppWithFSMBasic.h"

namespace xdaq {
  class ApplicationStub;
}

namespace tcds {
  namespace hwlayer {
    class RegisterInfo;
  }
}

namespace tcds {
  namespace hwutilsvme {
    class HwIDInfoSpaceHandlerVME;
    class HwIDInfoSpaceUpdaterVME;
  }
}

namespace tcds {
  namespace utils {
    class HwInfoSpaceUpdater;
  }
}

namespace tcds {
  namespace rf2ttc {

    class AutoModeInfoSpaceHandler;
    class BunchClockInfoSpaceHandler;
    class BunchClockInfoSpaceUpdater;
    class HwStatusInfoSpaceHandler;
    class HwStatusInfoSpaceUpdater;
    class OrbitInfoSpaceHandler;
    class OrbitInfoSpaceUpdater;

    class RF2TTCController : public tcds::utils::XDAQAppWithFSMBasic
    {

    public:
      XDAQ_INSTANTIATOR();

      RF2TTCController(xdaq::ApplicationStub* stub);
      virtual ~RF2TTCController();

    protected:
      virtual void setupInfoSpaces();

      /**
       * Access the hardware pointer as VMEDeviceRF2TTC&.
       */
      virtual VMEDeviceRF2TTC& getHw() const;

      virtual void hwConnectImpl();
      virtual void hwReleaseImpl();

      virtual void hwCfgInitializeImpl();
      virtual void hwCfgFinalizeImpl();

      virtual void coldResetActionImpl(toolbox::Event::Reference event);
      virtual void zeroActionImpl(toolbox::Event::Reference event);

      virtual tcds::utils::RegCheckResult isRegisterAllowed(tcds::hwlayer::RegisterInfo const& regInfo) const;

    private:
      // Various InfoSpaces and their InfoSpaceUpdaters.
      std::unique_ptr<tcds::utils::HwInfoSpaceUpdater> autoModeInfoSpaceUpdaterP_;
      std::unique_ptr<tcds::rf2ttc::AutoModeInfoSpaceHandler> autoModeInfoSpaceP_;
      std::unique_ptr<tcds::rf2ttc::BunchClockInfoSpaceUpdater> bunchClockInfoSpaceUpdaterP_;
      std::unique_ptr<tcds::rf2ttc::BunchClockInfoSpaceHandler> bunchClockInfoSpaceP_;
      std::unique_ptr<tcds::hwutilsvme::HwIDInfoSpaceUpdaterVME> hwIDInfoSpaceUpdaterP_;
      std::unique_ptr<tcds::hwutilsvme::HwIDInfoSpaceHandlerVME> hwIDInfoSpaceP_;
      std::unique_ptr<tcds::rf2ttc::HwStatusInfoSpaceUpdater> hwStatusInfoSpaceUpdaterP_;
      std::unique_ptr<tcds::rf2ttc::HwStatusInfoSpaceHandler> hwStatusInfoSpaceP_;
      std::unique_ptr<tcds::rf2ttc::OrbitInfoSpaceUpdater> orbitInfoSpaceUpdaterP_;
      std::unique_ptr<tcds::rf2ttc::OrbitInfoSpaceHandler> orbitInfoSpaceP_;

      // The SOAP commands.
      template<typename> friend class tcds::utils::SOAPCmdBase;
      template<typename> friend class tcds::utils::SOAPCmdDumpHardwareState;
      tcds::utils::SOAPCmdDumpHardwareState<RF2TTCController> soapCmdDumpHardwareState_;

    };

  } // namespace rf2ttc
} // namespace tcds

#endif // _tcds_rf2ttc_RF2TTCController_h_
