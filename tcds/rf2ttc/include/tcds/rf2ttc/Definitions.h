#ifndef _tcds_rf2ttc_Definitions_h_
#define _tcds_rf2ttc_Definitions_h_

#include <stdint.h>

namespace tcds {
  namespace definitions {

    enum TTCRX_STATUS {
      TTCRX_NOT_READY = 0,
      TTCRX_READY = 1
    };

    // Working mode for RF2TTC outputs: manual/automatic.
    enum WORKING_MODE {
      WORKING_MODE_MANUAL = 0,
      WORKING_MODE_AUTOMATIC = 1
    };

    // Beam/no-beam modes for RF2TTC outputs.
    enum AUTOMATIC_MODE_SOURCE {
      AUTOMATIC_MODE_SOURCE_NOBEAM = 0,
      AUTOMATIC_MODE_SOURCE_BEAM = 1
    };

    // QPLL relocking modes: manual/automatic.
    enum QPLL_RELOCKING_MODE {
      QPLL_RELOCKING_MANUAL = 0,
      QPLL_RELOCKING_AUTOMATIC = 1
    };

    // QPLL error and locking status enums.
    enum QPLL_ERROR_STATUS {
      QPLL_ERROR_STATUS_OK = 0,
      QPLL_ERROR_STATUS_ERROR = 1
    };
    enum QPLL_LOCK_STATUS {
      QPLL_LOCK_STATUS_UNLOCKED = 0,
      QPLL_LOCK_STATUS_LOCKED = 1
    };

    // Orbit 'locking' status.
    enum ORBIT_LOCK_STATUS {
      ORBIT_LOCK_STATUS_OK = 0,
      ORBIT_LOCK_STATUS_WRONG_LENGTH = 1,
      ORBIT_LOCK_STATUS_NO_ORBIT = 2
    };

    // Enums for the various RF2TTC outputs.
    enum BC_SIGNAL {
      BC_SIGNAL_BC1,
      BC_SIGNAL_BC2,
      BC_SIGNAL_BCREF,
      BC_SIGNAL_BCMAIN
    };
    int const kBCSignalMin = BC_SIGNAL_BC1;
    int const kBCSignalMax = BC_SIGNAL_BCMAIN;

    enum ORB_SIGNAL {
      ORB_SIGNAL_ORB1,
      ORB_SIGNAL_ORB2,
      ORB_SIGNAL_ORBMAIN
    };
    int const kORBSignalMin = ORB_SIGNAL_ORB1;
    int const kORBSignalMax = ORB_SIGNAL_ORBMAIN;

    // Bunch-clock source enums.
    // - For BC1/BC2/BC-ref.
    enum BC_SOURCE {
      BC_SOURCE_INTERNAL = 0,
      BC_SOURCE_EXTERNAL = 1
    };
    // - For BC-main.
    enum BC_MAIN_SOURCE {
      BC_MAIN_SOURCE_INTERNAL = 0,
      BC_MAIN_SOURCE_BCREF = 1,
      BC_MAIN_SOURCE_BC2 = 2,
      BC_MAIN_SOURCE_BC1 = 3
    };

    // Orbit source enums.
    // - For Orbit1/Orbit2.
    enum ORB_SOURCE {
      ORB_SOURCE_EXTERNAL = 0,
      ORB_SOURCE_INTERNAL = 1
    };
    // - For Orbit-main.
    enum ORB_MAIN_SOURCE {
      ORB_MAIN_SOURCE_ORB1 = 0,
      ORB_MAIN_SOURCE_ORB2 = 1,
      ORB_MAIN_SOURCE_INTERNAL = 2
    };

  } // namespace definitions
} // namespace tcds

#endif // _tcds_rf2ttc_Definitions_h_
