#ifndef _tcds_apve_VMEDeviceRF2TTC_h_
#define _tcds_apve_VMEDeviceRF2TTC_h_

#include <stdint.h>
#include <string>

#include "tcds/hwlayervme/VMEDeviceBase.h"
#include "tcds/rf2ttc/Definitions.h"

namespace tcds {
  namespace rf2ttc {

    /**
     * Implementation of the VME RF2TTC functionality.
     */
    class VMEDeviceRF2TTC : public tcds::hwlayervme::VMEDeviceBase
    {

    public:
      VMEDeviceRF2TTC();
      virtual ~VMEDeviceRF2TTC();

      virtual uint32_t getBaseAddress(uint32_t const slotNumber) const;

      // Reset the whole board.
      void resetBoard() const;
      // Reset a single QPLL.
      void resetQPLL(tcds::definitions::BC_SIGNAL const bcSignal) const;

      // Configure the TTCrx chip to enable its Dout bus carrying the
      // BST machine mode.
      void enableBST() const;

      tcds::definitions::BC_SOURCE getCurrentBCSource(tcds::definitions::BC_SIGNAL const bc) const;
      tcds::definitions::ORB_SOURCE getCurrentOrbitSource(tcds::definitions::ORB_SIGNAL const orb) const;
      tcds::definitions::BC_MAIN_SOURCE getCurrentBCMainSource() const;
      tcds::definitions::ORB_MAIN_SOURCE getCurrentOrbitMainSource() const;

      bool isQPLLError(tcds::definitions::BC_SIGNAL const bcSignal) const;
      bool isQPLLLocked(tcds::definitions::BC_SIGNAL const bcSignal) const;
      bool isOrbitLocked(tcds::definitions::ORB_SIGNAL const orbSignal) const;
      tcds::definitions::ORBIT_LOCK_STATUS getOrbitStatus(tcds::definitions::ORB_SIGNAL const orbSignal) const;

    protected:
      virtual std::string readFirmwareVersionImpl() const;

      // Specialized reads/writes for the I2C-based registers.
      virtual uint32_t readRegisterImpl(std::string const& regName) const;
      virtual void writeRegisterImpl(std::string const& regName,
                                     uint32_t const regVal) const;
      uint32_t readDelay25Register(std::string const& regName) const;
      void writeDelay25Register(std::string const& regName,
                                uint32_t const regVal) const;

      // The (I2C) registers in the Delay25 and TTCrx chips are
      // handled in a slightly special way.
      bool isDelay25Register(std::string const& regName) const;

      uint32_t getCurrentSource(std::string const& regNameBase) const;

    };

  } // namespace rf2ttc
} // namespace tcds

#endif // _tcds_rf2ttc_VMEDeviceRF2TTC_h_
