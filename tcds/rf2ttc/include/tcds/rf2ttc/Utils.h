#ifndef _tcds_rf2ttc_Utils_h_
#define _tcds_rf2ttc_Utils_h_

#include <string>

#include "tcds/rf2ttc/Definitions.h"

namespace tcds {
  namespace rf2ttc {

    // Mapping of TTCrx status enums to strings.
    std::string ttcrxStatusToString(tcds::definitions::TTCRX_STATUS const status);

    // Mapping of (output) working mode enums to strings.
    std::string workingModeToString(tcds::definitions::WORKING_MODE const mode);

    // Mapping of QPLL relocking mode enums to strings.
    std::string qpllRelockingModeToString(tcds::definitions::QPLL_RELOCKING_MODE const mode);

    // Mapping of QPLL error/locking status enums to strings.
    std::string qpllErrorStatusToString(tcds::definitions::QPLL_ERROR_STATUS const status);
    std::string qpllLockStatusToString(tcds::definitions::QPLL_LOCK_STATUS const status);

    // Mapping of orbit 'locking' status enums to strings.
    std::string orbitLockStatusToString(tcds::definitions::ORBIT_LOCK_STATUS const status);

    // Mapping of bunch-clock and orbit signal names to strings.
    std::string bcSignalToString(tcds::definitions::BC_SIGNAL const signal);
    std::string orbitSignalToString(tcds::definitions::ORB_SIGNAL const signal);

    // Mapping(s) of bunch-clock input source enums to strings.
    std::string bcSourceToString(tcds::definitions::BC_SOURCE const src);
    std::string bcMainSourceToString(tcds::definitions::BC_MAIN_SOURCE const src);

    // Mapping(s) of orbit signal input source enums to strings.
    std::string orbSourceToString(tcds::definitions::ORB_SOURCE const src);
    std::string orbMainSourceToString(tcds::definitions::ORB_MAIN_SOURCE const src);

    // Mapping(s) between clock signal enums and register name suffixes.
    std::string bcSignalRegSuffix(tcds::definitions::BC_SIGNAL const src);
    tcds::definitions::BC_SIGNAL bcSignalFromRegSuffix(std::string const& suffix);

    // Mapping(s) between orbit signal enums and register name suffixes.
    std::string orbSignalRegSuffix(tcds::definitions::ORB_SIGNAL const src);
    tcds::definitions::ORB_SIGNAL orbSignalFromRegSuffix(std::string const& suffix);

  } // namespace rf2ttc
} // namespace tcds

#endif // _tcds_rf2ttc_Utils_h_
