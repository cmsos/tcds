#ifndef _tcds_rf2ttc_OrbitInfoSpaceHandler_h_
#define _tcds_rf2ttc_OrbitInfoSpaceHandler_h_

#include <string>

#include "tcds/utils/InfoSpaceHandler.h"

namespace xdaq {
  class Application;
}

namespace tcds {
  namespace utils {
    class InfoSpaceUpdater;
    class Monitor;
    class WebServer;
  }
}

namespace tcds {
  namespace rf2ttc {

    class OrbitInfoSpaceHandler : public tcds::utils::InfoSpaceHandler
    {

    public:
      OrbitInfoSpaceHandler(xdaq::Application& xdaqApp,
                            tcds::utils::InfoSpaceUpdater* updater);
      virtual ~OrbitInfoSpaceHandler();

    protected:
      virtual void registerItemSetsWithMonitor(tcds::utils::Monitor& monitor);
      virtual void registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                 tcds::utils::Monitor& monitor,
                                                 std::string const& forceTabName="");

      virtual std::string formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const;

    };

  } // namespace rf2ttc
} // namespace tcds

#endif // _tcds_rf2ttc_OrbitInfoSpaceHandler_h_
