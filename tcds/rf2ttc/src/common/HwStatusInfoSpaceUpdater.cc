#include "tcds/rf2ttc/HwStatusInfoSpaceUpdater.h"

#include <stdint.h>
#include <string>

#include "tcds/rf2ttc/Definitions.h"
#include "tcds/rf2ttc/VMEDeviceRF2TTC.h"
#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/InfoSpaceItem.h"

tcds::rf2ttc::HwStatusInfoSpaceUpdater::HwStatusInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                                                 tcds::rf2ttc::VMEDeviceRF2TTC const& hw) :
  tcds::utils::HwInfoSpaceUpdaterBase(xdaqApp, hw)
{
}

tcds::rf2ttc::HwStatusInfoSpaceUpdater::~HwStatusInfoSpaceUpdater()
{
}

bool
tcds::rf2ttc::HwStatusInfoSpaceUpdater::updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                                             tcds::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  bool updated = false;
  tcds::rf2ttc::VMEDeviceRF2TTC const& hw = getHw();
  if (hw.isReadyForUse())
    {
      std::string name = item.name();
      tcds::utils::InfoSpaceItem::UpdateType updateType = item.updateType();
      if (updateType == tcds::utils::InfoSpaceItem::PROCESS)
        {
          // The 'PROCESS' update type means that there is something
          // special to the variable. Figure out what to do based on the
          // variable name.
          if (name == "curr_select_bcmain")
            {
              uint32_t const newVal = hw.getCurrentBCMainSource();
              infoSpaceHandler->setUInt32(name, newVal);
              updated = true;
            }
          else if (name == "curr_select_orbmain")
            {
              uint32_t const newVal = hw.getCurrentOrbitMainSource();
              infoSpaceHandler->setUInt32(name, newVal);
              updated = true;
            }
          else if (name == "orbit_lock_status_orbmain")
            {
              tcds::definitions::ORBIT_LOCK_STATUS const newVal = hw.getOrbitStatus(tcds::definitions::ORB_SIGNAL_ORBMAIN);
              infoSpaceHandler->setUInt32(name, static_cast<uint32_t>(newVal));
              updated = true;
            }
        }
      if (!updated)
        {
          updated = tcds::utils::HwInfoSpaceUpdaterBase::updateInfoSpaceItem(item, infoSpaceHandler);
        }
    }

  if (updated)
    {
      item.setValid();
    }
  else
    {
      item.setInvalid();
    }

  return updated;
}

tcds::rf2ttc::VMEDeviceRF2TTC const&
tcds::rf2ttc::HwStatusInfoSpaceUpdater::getHw() const
{
  return dynamic_cast<tcds::rf2ttc::VMEDeviceRF2TTC const&>(tcds::utils::HwInfoSpaceUpdaterBase::getHw());
}
