#include "tcds/rf2ttc/OrbitInfoSpaceHandler.h"

#include <stdint.h>

#include "toolbox/string.h"

#include "tcds/rf2ttc/Definitions.h"
#include "tcds/rf2ttc/Utils.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/Utils.h"
#include "tcds/utils/WebServer.h"

tcds::rf2ttc::OrbitInfoSpaceHandler::OrbitInfoSpaceHandler(xdaq::Application& xdaqApp,
                                                           tcds::utils::InfoSpaceUpdater* updater) :
  InfoSpaceHandler(xdaqApp, "tcds-rf2ttc-orbits", updater)
{
  // The working mode of each of the orbit outputs (manual/automatic).
  for (int orbSignal = tcds::definitions::kORBSignalMin;
       orbSignal <= tcds::definitions::kORBSignalMax;
       ++orbSignal)
    {
      tcds::definitions::ORB_SIGNAL tmp =
        static_cast<tcds::definitions::ORB_SIGNAL>(orbSignal);
      std::string const regNameSuffix = orbSignalRegSuffix(tmp);
      createUInt32("working_mode_" + regNameSuffix);
    }

  //----------

  // The source settings for the different RF2TTC modes for each of
  // the orbit outputs.
  for (int orbSignal = tcds::definitions::kORBSignalMin;
       orbSignal <= tcds::definitions::kORBSignalMax;
       ++orbSignal)
    {
      tcds::definitions::ORB_SIGNAL tmp =
        static_cast<tcds::definitions::ORB_SIGNAL>(orbSignal);
      std::string const regNameSuffix = orbSignalRegSuffix(tmp);
      // Source for 'manual' mode.
      createUInt32("man_select_" + regNameSuffix);
      // Source for 'beam' mode.
      createUInt32("beam_select_" + regNameSuffix);
      // Source for 'no-beam' mode.
      createUInt32("nobeam_select_" + regNameSuffix);
    }

  //----------

  // The currently-selected sources.
  for (int orbSignal = tcds::definitions::kORBSignalMin;
       orbSignal <= tcds::definitions::kORBSignalMax;
       ++orbSignal)
    {
      tcds::definitions::ORB_SIGNAL tmp =
        static_cast<tcds::definitions::ORB_SIGNAL>(orbSignal);
      std::string const regNameSuffix = orbSignalRegSuffix(tmp);
      createUInt32("curr_select_" + regNameSuffix,
                   0,
                   "",
                   tcds::utils::InfoSpaceItem::PROCESS);
    }

  //----------

  // Input DAC settings.
  for (int orbSignal = tcds::definitions::kORBSignalMin;
       orbSignal <= tcds::definitions::kORBSignalMax;
       ++orbSignal)
    {
      tcds::definitions::ORB_SIGNAL tmp =
        static_cast<tcds::definitions::ORB_SIGNAL>(orbSignal);
      std::string const regNamePrefix = orbSignalRegSuffix(tmp);
      // NOTE: Orbit-main is not an input, so it has no input DAC.
      if (tmp != tcds::definitions::ORB_SIGNAL_ORBMAIN)
        {
          createUInt32(regNamePrefix + "_dac");
        }
    }

  //----------

  // The delay25 chip settings for each of the orbit input phase
  // adjustments.
  for (int orbSignal = tcds::definitions::kORBSignalMin;
       orbSignal <= tcds::definitions::kORBSignalMax;
       ++orbSignal)
    {
      tcds::definitions::ORB_SIGNAL tmp =
        static_cast<tcds::definitions::ORB_SIGNAL>(orbSignal);
      std::string const regNameSuffix = orbSignalRegSuffix(tmp);
      // NOTE: Orbit-main is not an input, so it has no input delay.
      if (tmp != tcds::definitions::ORB_SIGNAL_ORBMAIN)
        {
          createBool("orbin_delay25_" + regNameSuffix + "_enable",
                     false,
                     "enabled/disabled");
          createUInt32("orbin_delay25_" + regNameSuffix + "_val");
        }
    }

  // Orbit output coarse delay settings.
  for (int orbSignal = tcds::definitions::kORBSignalMin;
       orbSignal <= tcds::definitions::kORBSignalMax;
       ++orbSignal)
    {
      tcds::definitions::ORB_SIGNAL tmp =
        static_cast<tcds::definitions::ORB_SIGNAL>(orbSignal);
      std::string const regNamePrefix = orbSignalRegSuffix(tmp);
      createUInt32(regNamePrefix + "_coarse_delay");
    }

  // The delay25 chip settings for each of the orbit output phase
  // adjustments.
  for (int orbSignal = tcds::definitions::kORBSignalMin;
       orbSignal <= tcds::definitions::kORBSignalMax;
       ++orbSignal)
    {
      tcds::definitions::ORB_SIGNAL tmp =
        static_cast<tcds::definitions::ORB_SIGNAL>(orbSignal);
      std::string const regNameSuffix = orbSignalRegSuffix(tmp);
      createBool("orbout_delay25_" + regNameSuffix + "_enable",
                 false,
                 "enabled/disabled");
      createUInt32("orbout_delay25_" + regNameSuffix + "_val");
    }

  //----------

  // Orbit output polarity inversion flags.
  for (int orbSignal = tcds::definitions::kORBSignalMin;
       orbSignal <= tcds::definitions::kORBSignalMax;
       ++orbSignal)
    {
      tcds::definitions::ORB_SIGNAL tmp =
        static_cast<tcds::definitions::ORB_SIGNAL>(orbSignal);
      std::string const regNamePrefix = orbSignalRegSuffix(tmp);
      createBool(regNamePrefix + "_polarity",
                 false,
                 "true/false");
    }

  //----------

  // Internal-orbit length configuration.
  for (int orbSignal = tcds::definitions::kORBSignalMin;
       orbSignal <= tcds::definitions::kORBSignalMax;
       ++orbSignal)
    {
      tcds::definitions::ORB_SIGNAL tmp =
        static_cast<tcds::definitions::ORB_SIGNAL>(orbSignal);
      std::string const regNamePrefix = orbSignalRegSuffix(tmp);
      createUInt32(regNamePrefix + "_int_period_set");
    }

  //----------

  // Length of most recent orbit.
  for (int orbSignal = tcds::definitions::kORBSignalMin;
       orbSignal <= tcds::definitions::kORBSignalMax;
       ++orbSignal)
    {
      tcds::definitions::ORB_SIGNAL tmp =
        static_cast<tcds::definitions::ORB_SIGNAL>(orbSignal);
      std::string const regNamePrefix = orbSignalRegSuffix(tmp);
      createUInt32(regNamePrefix + "_period_rd");
    }

  //----------

  // Orbit 'locking' status, unlock count, and latest unlock
  // timestamp.
  for (int orbSignal = tcds::definitions::kORBSignalMin;
       orbSignal <= tcds::definitions::kORBSignalMax;
       ++orbSignal)
    {
      tcds::definitions::ORB_SIGNAL tmp =
        static_cast<tcds::definitions::ORB_SIGNAL>(orbSignal);
      std::string const regNamePrefix = orbSignalRegSuffix(tmp);
      createUInt32("orbit_lock_status_" + regNamePrefix,
                   0,
                   "",
                   tcds::utils::InfoSpaceItem::PROCESS);
      createUInt32("orbit_lock_status_count_" + regNamePrefix,
                   0,
                   "",
                   tcds::utils::InfoSpaceItem::PROCESS);
      createTimeVal("orbit_latest_unlock_time_" + regNamePrefix,
                    0,
                    "time_val_no_zero",
                    tcds::utils::InfoSpaceItem::PROCESS);
    }
}

tcds::rf2ttc::OrbitInfoSpaceHandler::~OrbitInfoSpaceHandler()
{
}

void
tcds::rf2ttc::OrbitInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  for (int orbSignal = tcds::definitions::kORBSignalMin;
       orbSignal <= tcds::definitions::kORBSignalMax;
       ++orbSignal)
    {
      tcds::definitions::ORB_SIGNAL tmp =
        static_cast<tcds::definitions::ORB_SIGNAL>(orbSignal);
      std::string const regNameSuffix = orbSignalRegSuffix(tmp);
      monitor.newItemSet("itemset-orbit-" + regNameSuffix);
      monitor.addItem("itemset-orbit-" + regNameSuffix,
                      "working_mode_" + regNameSuffix,
                      "Working mode",
                      this);
      monitor.addItem("itemset-orbit-" + regNameSuffix,
                      "man_select_" + regNameSuffix,
                      "Source for mode 'manual'",
                      this);
      monitor.addItem("itemset-orbit-" + regNameSuffix,
                      "beam_select_" + regNameSuffix,
                      "Source for mode 'beam'",
                      this);
      monitor.addItem("itemset-orbit-" + regNameSuffix,
                      "nobeam_select_" + regNameSuffix,
                      "Source for mode 'no-beam'",
                      this);
      monitor.addItem("itemset-orbit-" + regNameSuffix,
                      "curr_select_" + regNameSuffix,
                      "Selected source",
                      this);
      if (tmp != tcds::definitions::ORB_SIGNAL_ORBMAIN)
        {
          monitor.addItem("itemset-orbit-" + regNameSuffix,
                          regNameSuffix + "_dac",
                          "Input DAC threshold setting",
                          this);
          monitor.addItem("itemset-orbit-" + regNameSuffix,
                          "orbin_delay25_" + regNameSuffix + "_enable",
                          "Input phase adjustment Delay25 chip",
                          this);
          monitor.addItem("itemset-orbit-" + regNameSuffix,
                          "orbin_delay25_" + regNameSuffix + "_val",
                          "Input phase delay (0.5 ns steps)",
                          this);
        }
      monitor.addItem("itemset-orbit-" + regNameSuffix,
                      regNameSuffix + "_coarse_delay",
                      "Output delay (25 ns steps)",
                      this);
      monitor.addItem("itemset-orbit-" + regNameSuffix,
                      "orbout_delay25_" + regNameSuffix + "_enable",
                      "Output phase adjustment Delay25 chip",
                      this);
      monitor.addItem("itemset-orbit-" + regNameSuffix,
                      "orbout_delay25_" + regNameSuffix + "_val",
                      "Output phase delay (0.5 ns steps)",
                      this);
      monitor.addItem("itemset-orbit-" + regNameSuffix,
                      regNameSuffix + "_polarity",
                      "Output polarity inverted",
                      this);
      monitor.addItem("itemset-orbit-" + regNameSuffix,
                      regNameSuffix + "_int_period_set",
                      "Internal-orbit length (BX)",
                      this);
      monitor.addItem("itemset-orbit-" + regNameSuffix,
                      regNameSuffix + "_period_rd",
                      "Measured length of most recent orbit (BX)",
                      this);
      monitor.addItem("itemset-orbit-" + regNameSuffix,
                      "orbit_lock_status_" + regNameSuffix,
                      "'Locking' status",
                      this);
      monitor.addItem("itemset-orbit-" + regNameSuffix,
                      "orbit_lock_status_count_" + regNameSuffix,
                      "'Unlock' count",
                      this);
      monitor.addItem("itemset-orbit-" + regNameSuffix,
                      "orbit_latest_unlock_time_" + regNameSuffix,
                      "Timestamp of latest 'unlock'",
                      this);
    }
}

void
tcds::rf2ttc::OrbitInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                   tcds::utils::Monitor& monitor,
                                                                   std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "Orbit signals" : forceTabName;

  webServer.registerTab(tabName,
                        "Information related to the orbit signal(s)",
                        3);

  for (int orbSignal = tcds::definitions::kORBSignalMin;
       orbSignal <= tcds::definitions::kORBSignalMax;
       ++orbSignal)
    {
      tcds::definitions::ORB_SIGNAL tmp =
        static_cast<tcds::definitions::ORB_SIGNAL>(orbSignal);
      std::string const signalLabel = orbitSignalToString(tmp);
      std::string const regNameSuffix = orbSignalRegSuffix(tmp);
      webServer.registerTable(signalLabel + " info",
                              signalLabel + "-related info",
                              monitor,
                              "itemset-orbit-" + regNameSuffix,
                              tabName);
    }
}

std::string
tcds::rf2ttc::OrbitInfoSpaceHandler::formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const
{
  std::string res = tcds::utils::escapeAsJSONString(tcds::utils::InfoSpaceHandler::kInvalidItemString);
  if (item->isValid())
    {
      std::string name = item->name();
      if (toolbox::startsWith(name, "working_mode"))
        {
          uint32_t const value = getUInt32(name);
          tcds::definitions::WORKING_MODE const valueEnum =
            static_cast<tcds::definitions::WORKING_MODE>(value);
          res = tcds::utils::escapeAsJSONString(tcds::rf2ttc::workingModeToString(valueEnum));
        }
      else if (toolbox::startsWith(name, "man_select") ||
               toolbox::startsWith(name, "beam_select") ||
               toolbox::startsWith(name, "nobeam_select"))
        {
          uint32_t const value = getUInt32(name);
          if (name.find("orbmain") != std::string::npos)
            {
              tcds::definitions::ORB_MAIN_SOURCE const valueEnum =
                static_cast<tcds::definitions::ORB_MAIN_SOURCE>(value);
              res = tcds::utils::escapeAsJSONString(tcds::rf2ttc::orbMainSourceToString(valueEnum));
            }
          else
            {
              tcds::definitions::ORB_SOURCE const valueEnum =
                static_cast<tcds::definitions::ORB_SOURCE>(value);
              res = tcds::utils::escapeAsJSONString(tcds::rf2ttc::orbSourceToString(valueEnum));
            }
        }
      else if (toolbox::startsWith(name, "orbit_lock_status") &&
               (name.find("count") == std::string::npos))
        {
          uint32_t const value = getUInt32(name);
          tcds::definitions::ORBIT_LOCK_STATUS const valueEnum =
            static_cast<tcds::definitions::ORBIT_LOCK_STATUS>(value);
          res = tcds::utils::escapeAsJSONString(tcds::rf2ttc::orbitLockStatusToString(valueEnum));
        }
     else if (name == "curr_select_orbmain")
        {
          uint32_t const value = getUInt32(name);
          tcds::definitions::ORB_MAIN_SOURCE const valueEnum =
            static_cast<tcds::definitions::ORB_MAIN_SOURCE>(value);
          res = tcds::utils::escapeAsJSONString(tcds::rf2ttc::orbMainSourceToString(valueEnum));
        }
      else if (toolbox::startsWith(name, "curr_select_orb"))
        {
          uint32_t const value = getUInt32(name);
          tcds::definitions::ORB_SOURCE const valueEnum =
            static_cast<tcds::definitions::ORB_SOURCE>(value);
          res = tcds::utils::escapeAsJSONString(tcds::rf2ttc::orbSourceToString(valueEnum));
        }
       else
        {
          // For everything else simply call the generic formatter.
          res = InfoSpaceHandler::formatItem(item);
        }
    }
  return res;
}
