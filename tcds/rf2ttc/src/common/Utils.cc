#include "tcds/rf2ttc/Utils.h"

#include "toolbox/string.h"
#include "xcept/Exception.h"

#include "tcds/exception/Exception.h"

std::string
tcds::rf2ttc::ttcrxStatusToString(tcds::definitions::TTCRX_STATUS const status)
{
  std::string res = "";

  switch (status)
    {
    case tcds::definitions::TTCRX_NOT_READY:
      res = "not ready";
      break;
    case tcds::definitions::TTCRX_READY:
      res = "ready";
      break;
    default:
      XCEPT_RAISE(tcds::exception::ValueError,
                  toolbox::toString("'%d' is not a valid TTCrx status.",
                                    status));
      break;
    }
  return res;
}

std::string
tcds::rf2ttc::workingModeToString(tcds::definitions::WORKING_MODE const mode)
{
  std::string res = "";

  switch (mode)
    {
    case tcds::definitions::WORKING_MODE_MANUAL:
      res = "manual";
      break;
    case tcds::definitions::WORKING_MODE_AUTOMATIC:
      res = "automatic";
      break;
    default:
      XCEPT_RAISE(tcds::exception::ValueError,
                  toolbox::toString("'%d' is not a valid working mode.",
                                    mode));
      break;
    }
  return res;
}

std::string
tcds::rf2ttc::qpllRelockingModeToString(tcds::definitions::QPLL_RELOCKING_MODE const mode)
{
  std::string res = "";

  switch (mode)
    {
    case tcds::definitions::QPLL_RELOCKING_MANUAL:
      res = "manual";
      break;
    case tcds::definitions::QPLL_RELOCKING_AUTOMATIC:
      res = "automatic";
      break;
    default:
      XCEPT_RAISE(tcds::exception::ValueError,
                  toolbox::toString("'%d' is not a valid QPLL relocking mode.",
                                    mode));
      break;
    }
  return res;
}

std::string
tcds::rf2ttc::qpllErrorStatusToString(tcds::definitions::QPLL_ERROR_STATUS const status)
{
  std::string res = "";

  switch (status)
    {
    case tcds::definitions::QPLL_ERROR_STATUS_ERROR:
      res = "error";
      break;
    case tcds::definitions::QPLL_ERROR_STATUS_OK:
      res = "ok";
      break;
    default:
      XCEPT_RAISE(tcds::exception::ValueError,
                  toolbox::toString("'%d' is not a valid QPLL error status code.",
                                    status));
      break;
    }
  return res;
}

std::string
tcds::rf2ttc::qpllLockStatusToString(tcds::definitions::QPLL_LOCK_STATUS const status)
{
  std::string res = "";

  switch (status)
    {
    case tcds::definitions::QPLL_LOCK_STATUS_UNLOCKED:
      res = "not locked";
      break;
    case tcds::definitions::QPLL_LOCK_STATUS_LOCKED:
      res = "locked";
      break;
    default:
      XCEPT_RAISE(tcds::exception::ValueError,
                  toolbox::toString("'%d' is not a valid QPLL error locking code.",
                                    status));
      break;
    }
  return res;
}

std::string
tcds::rf2ttc::orbitLockStatusToString(tcds::definitions::ORBIT_LOCK_STATUS const status)
{
  std::string res = "";

  switch (status)
    {
    case tcds::definitions::ORBIT_LOCK_STATUS_OK:
      res = "OK";
      break;
    case tcds::definitions::ORBIT_LOCK_STATUS_WRONG_LENGTH:
      res = "wrong orbit length";
      break;
    case tcds::definitions::ORBIT_LOCK_STATUS_NO_ORBIT:
      res = "no orbit signal";
      break;
    default:
      XCEPT_RAISE(tcds::exception::ValueError,
                  toolbox::toString("'%d' is not a valid QPLL error locking code.",
                                    status));
      break;
    }
  return res;
}

std::string
tcds::rf2ttc::bcSignalToString(tcds::definitions::BC_SIGNAL const signal)
{
  std::string res = "";

  switch (signal)
    {
    case tcds::definitions::BC_SIGNAL_BC1:
      res = "BC1";
      break;
    case tcds::definitions::BC_SIGNAL_BC2:
      res = "BC2";
      break;
    case tcds::definitions::BC_SIGNAL_BCREF:
      res = "BC-ref";
      break;
    case tcds::definitions::BC_SIGNAL_BCMAIN:
      res = "BC-main";
      break;
    default:
      XCEPT_RAISE(tcds::exception::ValueError,
                  toolbox::toString("'%d' is not a valid bunch-clock signal.",
                                    signal));
      break;
    }
  return res;
}

std::string
tcds::rf2ttc::orbitSignalToString(tcds::definitions::ORB_SIGNAL const signal)
{
  std::string res = "";

  switch (signal)
    {
    case tcds::definitions::ORB_SIGNAL_ORB1:
      res = "Orbit1";
      break;
    case tcds::definitions::ORB_SIGNAL_ORB2:
      res = "Orbit2";
      break;
    case tcds::definitions::ORB_SIGNAL_ORBMAIN:
      res = "Orbit-main";
      break;
    default:
      XCEPT_RAISE(tcds::exception::ValueError,
                  toolbox::toString("'%d' is not a valid bunch-clock signal.",
                                    signal));
      break;
    }
  return res;
}

std::string
tcds::rf2ttc::bcSourceToString(tcds::definitions::BC_SOURCE const src)
{
  std::string res = "";

  switch (src)
    {
    case tcds::definitions::BC_SOURCE_INTERNAL:
      res = "internal";
      break;
    case tcds::definitions::BC_SOURCE_EXTERNAL:
      res = "external";
      break;
    default:
      XCEPT_RAISE(tcds::exception::ValueError,
                  toolbox::toString("'%d' is not a valid BC1/BC2/BCref source.",
                                    src));
      break;
    }
  return res;
}

std::string
tcds::rf2ttc::bcMainSourceToString(tcds::definitions::BC_MAIN_SOURCE const src)
{
  std::string res = "";

  switch (src)
    {
    case tcds::definitions::BC_MAIN_SOURCE_INTERNAL:
      res = "internal";
      break;
    case tcds::definitions::BC_MAIN_SOURCE_BC1:
      res = "BC1";
      break;
    case tcds::definitions::BC_MAIN_SOURCE_BC2:
      res = "BC2";
      break;
    case tcds::definitions::BC_MAIN_SOURCE_BCREF:
      res = "BCref";
      break;
    default:
      XCEPT_RAISE(tcds::exception::ValueError,
                  toolbox::toString("'%d' is not a valid BCmain source.",
                                    src));
      break;
    }
  return res;
}

std::string
tcds::rf2ttc::orbSourceToString(tcds::definitions::ORB_SOURCE const src)
{
  std::string res = "";

  switch (src)
    {
    case tcds::definitions::ORB_SOURCE_INTERNAL:
      res = "internal";
      break;
    case tcds::definitions::ORB_SOURCE_EXTERNAL:
      res = "external";
      break;
    default:
      XCEPT_RAISE(tcds::exception::ValueError,
                  toolbox::toString("'%d' is not a valid Orbit1/Orbit2 source.",
                                    src));
      break;
    }
  return res;
}

std::string
tcds::rf2ttc::orbMainSourceToString(tcds::definitions::ORB_MAIN_SOURCE const src)
{
  std::string res = "";

  switch (src)
    {
    case tcds::definitions::ORB_MAIN_SOURCE_INTERNAL:
      res = "internal";
      break;
    case tcds::definitions::ORB_MAIN_SOURCE_ORB1:
      res = "Orbit1";
      break;
    case tcds::definitions::ORB_MAIN_SOURCE_ORB2:
      res = "Orbit2";
      break;
    default:
      XCEPT_RAISE(tcds::exception::ValueError,
                  toolbox::toString("'%d' is not a valid Orbit-main source.",
                                    src));
      break;
    }
  return res;
}

std::string
tcds::rf2ttc::bcSignalRegSuffix(tcds::definitions::BC_SIGNAL const src)
{
  std::string res = "";

  switch (src)
    {
    case tcds::definitions::BC_SIGNAL_BCMAIN:
      res = "bcmain";
      break;
    case tcds::definitions::BC_SIGNAL_BCREF:
      res = "bcref";
      break;
    case tcds::definitions::BC_SIGNAL_BC1:
      res = "bc1";
      break;
    case tcds::definitions::BC_SIGNAL_BC2:
      res = "bc2";
      break;
    default:
      XCEPT_RAISE(tcds::exception::ValueError,
                  toolbox::toString("'%d' is not a valid bunch-clock signal enum.",
                                    src));
      break;
    }
  return res;
}

tcds::definitions::BC_SIGNAL
tcds::rf2ttc::bcSignalFromRegSuffix(std::string const& suffix)
{
  tcds::definitions::BC_SIGNAL res = tcds::definitions::BC_SIGNAL_BCMAIN;

  if (suffix == "bcmain")
    {
      res = tcds::definitions::BC_SIGNAL_BCMAIN;
    }
  else if (suffix == "bcref")
    {
      res = tcds::definitions::BC_SIGNAL_BCREF;
    }
  else if (suffix == "bc1")
    {
      res = tcds::definitions::BC_SIGNAL_BC1;
    }
  else if (suffix == "bc2")
    {
      res = tcds::definitions::BC_SIGNAL_BC2;
    }
  else
    {
      XCEPT_RAISE(tcds::exception::ValueError,
                  toolbox::toString("'%s' is not a valid bunch-clock signal register suffix.",
                                    suffix.c_str()));
    }
  return res;
}

std::string
tcds::rf2ttc::orbSignalRegSuffix(tcds::definitions::ORB_SIGNAL const src)
{
  std::string res = "";

  switch (src)
    {
    case tcds::definitions::ORB_SIGNAL_ORBMAIN:
      res = "orbmain";
      break;
    case tcds::definitions::ORB_SIGNAL_ORB1:
      res = "orb1";
      break;
    case tcds::definitions::ORB_SIGNAL_ORB2:
      res = "orb2";
      break;
    default:
      XCEPT_RAISE(tcds::exception::ValueError,
                  toolbox::toString("'%d' is not a valid orbit signal enum.",
                                    src));
      break;
    }
  return res;
}

tcds::definitions::ORB_SIGNAL
tcds::rf2ttc::orbSignalFromRegSuffix(std::string const& suffix)
{
  tcds::definitions::ORB_SIGNAL res = tcds::definitions::ORB_SIGNAL_ORBMAIN;

  if (suffix == "orbmain")
    {
      res = tcds::definitions::ORB_SIGNAL_ORBMAIN;
    }
  else if (suffix == "orb1")
    {
      res = tcds::definitions::ORB_SIGNAL_ORB1;
    }
  else if (suffix == "orb2")
    {
      res = tcds::definitions::ORB_SIGNAL_ORB2;
    }
  else
    {
      XCEPT_RAISE(tcds::exception::ValueError,
                  toolbox::toString("'%s' is not a valid orbit signal register suffix.",
                                    suffix.c_str()));
    }
  return res;
}
