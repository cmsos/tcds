#include "tcds/rf2ttc/AutoModeInfoSpaceHandler.h"

#include <stdint.h>

#include "toolbox/string.h"

#include "tcds/utils/Definitions.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/Utils.h"
#include "tcds/utils/WebServer.h"

tcds::rf2ttc::AutoModeInfoSpaceHandler::AutoModeInfoSpaceHandler(xdaq::Application& xdaqApp,
                                                                 tcds::utils::InfoSpaceUpdater* updater) :
  InfoSpaceHandler(xdaqApp, "tcds-automode", updater)
{
  // NOTE: Explicitly add the (impossible) 'unknown' 0 mode.
  int mode = 0;
  std::string const name = toolbox::toString("beam_no_beam_def_mode%d", mode);
  createUInt32(name);

  for (int mode = tcds::definitions::kBeamModeMin;
       mode <= tcds::definitions::kBeamModeMax;
       ++mode)
    {
      std::string const name = toolbox::toString("beam_no_beam_def_mode%d", mode);
      createUInt32(name);
    }
}

tcds::rf2ttc::AutoModeInfoSpaceHandler::~AutoModeInfoSpaceHandler()
{
}

void
tcds::rf2ttc::AutoModeInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  monitor.newItemSet("itemset-automode");

  // NOTE: Explicitly add the 'unknown' 0 mode.
  int mode = 0;
  std::string const name = toolbox::toString("beam_no_beam_def_mode%d", mode);
  std::string const tmp =
    tcds::utils::beamModeToString(static_cast<tcds::definitions::BEAM_MODE>(mode));
  std::string const description = tcds::utils::capitalizeString(tmp);
  monitor.addItem("itemset-automode",
                  name,
                  description,
                  this);

  for (int mode = tcds::definitions::kBeamModeMin;
       mode <= tcds::definitions::kBeamModeMax;
       ++mode)
    {
      std::string const name = toolbox::toString("beam_no_beam_def_mode%d", mode);
      std::string const tmp =
        tcds::utils::beamModeToString(static_cast<tcds::definitions::BEAM_MODE>(mode));
      std::string const description = tcds::utils::capitalizeString(tmp);
      monitor.addItem("itemset-automode",
                      name,
                      description,
                      this);
    }
}

void
tcds::rf2ttc::AutoModeInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                      tcds::utils::Monitor& monitor,
                                                                      std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "Automatic mode" : forceTabName;

  std::string const description =
    "In automatic mode, the RF2TTC will automatically switch between 'beam' "
    "and 'no-beam' modes based on the LHC beam mode (received via the BST). "
    "The source of each of the RF2TTC outputs can be made dependent "
    "on the 'beam' vs. 'no-beam' mode.";
  webServer.registerTab(tabName,
                        description,
                        3);
  webServer.registerTable("Beam mode mapping",
                          "Mapping between LHC beam modes and RF2TTC working modes",
                          monitor,
                          "itemset-automode",
                          tabName);
}

std::string
tcds::rf2ttc::AutoModeInfoSpaceHandler::formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const
{
  std::string res = tcds::utils::escapeAsJSONString(tcds::utils::InfoSpaceHandler::kInvalidItemString);
  if (item->isValid())
    {
      std::string name = item->name();
      if (toolbox::startsWith(name, "beam_no_beam_def"))
        {
          uint32_t const value = getUInt32(name);
          std::string tmp = "";
          if (value == 0)
            {
              tmp = "no-beam";
            }
          else
            {
              tmp = "beam";
            }
          res = tcds::utils::escapeAsJSONString(tmp);
        }
      else
        {
          // For everything else simply call the generic formatter.
          res = InfoSpaceHandler::formatItem(item);
        }
    }
  return res;
}
