#include "tcds/rf2ttc/VMEDeviceRF2TTC.h"

#include <cstddef>
#include <sstream>

#include "toolbox/string.h"
#include "xcept/Exception.h"

#include "tcds/exception/Exception.h"
#include "tcds/hwlayer/Utils.h"
#include "tcds/rf2ttc/Utils.h"
#include "tcds/utils/Definitions.h"

tcds::rf2ttc::VMEDeviceRF2TTC::VMEDeviceRF2TTC() :
  VMEDeviceBase()
{
}

tcds::rf2ttc::VMEDeviceRF2TTC::~VMEDeviceRF2TTC()
{
}

uint32_t
tcds::rf2ttc::VMEDeviceRF2TTC::getBaseAddress(uint32_t const slotNumber) const
{
  return (slotNumber << 20);
}

void
tcds::rf2ttc::VMEDeviceRF2TTC::resetBoard() const
{
  // See the manual: set bset, then set bclear.
  writeRegister("bset_board", 0x1);
  writeRegister("bclear_board", 0x1);
}

void
tcds::rf2ttc::VMEDeviceRF2TTC::resetQPLL(tcds::definitions::BC_SIGNAL const bcSignal) const
{
  // See the manual: set bset, then set bclear.
  std::string regNameSuffix = "unknown";
  if (bcSignal == tcds::definitions::BC_SIGNAL_BC1)
    {
      regNameSuffix = "bc1";
    }
  else if (bcSignal == tcds::definitions::BC_SIGNAL_BC2)
    {
      regNameSuffix = "bc2";
    }
  else if (bcSignal == tcds::definitions::BC_SIGNAL_BCREF)
    {
      regNameSuffix = "bcref";
    }
  else if (bcSignal == tcds::definitions::BC_SIGNAL_BCMAIN)
    {
      regNameSuffix = "bcmain";
    }
  writeRegister(toolbox::toString("bset_qpll_%s", regNameSuffix.c_str()), 0x1);
  writeRegister(toolbox::toString("bclear_qpll_%s", regNameSuffix.c_str()), 0x1);
}

void
tcds::rf2ttc::VMEDeviceRF2TTC::enableBST() const
{
  // Write to the TTCrx control register (at address 0x3).
  // NOTE: For details on the procedure: refer to the RF2TTC and TTCrx
  // manuals.
  // NOTE: The 0xb3 value comes from the RF2TTC manual.
  uint32_t const ttcrxControlReg = 0x3;
  writeRegister("ttcrx_address", ttcrxControlReg);
  uint32_t const ttcrxControlVal = 0xb3;
  writeRegister("ttcrx_data", ttcrxControlVal);
}

tcds::definitions::BC_SOURCE
tcds::rf2ttc::VMEDeviceRF2TTC::getCurrentBCSource(tcds::definitions::BC_SIGNAL const bc) const
{
  uint32_t const currSrc = getCurrentSource(bcSignalRegSuffix(bc));
  tcds::definitions::BC_SOURCE res =
    static_cast<tcds::definitions::BC_SOURCE>(currSrc);
  return res;
}

tcds::definitions::ORB_SOURCE
tcds::rf2ttc::VMEDeviceRF2TTC::getCurrentOrbitSource(tcds::definitions::ORB_SIGNAL const orb) const
{
  uint32_t const currSrc = getCurrentSource(orbSignalRegSuffix(orb));
  tcds::definitions::ORB_SOURCE res =
    static_cast<tcds::definitions::ORB_SOURCE>(currSrc);
  return res;
}

tcds::definitions::BC_MAIN_SOURCE
tcds::rf2ttc::VMEDeviceRF2TTC::getCurrentBCMainSource() const
{
  uint32_t const currSrc =
    getCurrentSource(bcSignalRegSuffix(tcds::definitions::BC_SIGNAL_BCMAIN));
  tcds::definitions::BC_MAIN_SOURCE res =
    static_cast<tcds::definitions::BC_MAIN_SOURCE>(currSrc);
  return res;
}

tcds::definitions::ORB_MAIN_SOURCE
tcds::rf2ttc::VMEDeviceRF2TTC::getCurrentOrbitMainSource() const
{
  uint32_t const currSrc =
    getCurrentSource(orbSignalRegSuffix(tcds::definitions::ORB_SIGNAL_ORBMAIN));
  tcds::definitions::ORB_MAIN_SOURCE res =
    static_cast<tcds::definitions::ORB_MAIN_SOURCE>(currSrc);
  return res;
}

bool
tcds::rf2ttc::VMEDeviceRF2TTC::isQPLLError(tcds::definitions::BC_SIGNAL const bcSignal) const
{
  std::string regName = "unkown";
  if (bcSignal == tcds::definitions::BC_SIGNAL_BC1)
    {
      regName = "qpll_error_status_bc1";
    }
  else if (bcSignal == tcds::definitions::BC_SIGNAL_BC2)
    {
      regName = "qpll_error_status_bc2";
    }
  else if (bcSignal == tcds::definitions::BC_SIGNAL_BCREF)
    {
      regName = "qpll_error_status_bcref";
    }
  else if (bcSignal == tcds::definitions::BC_SIGNAL_BCMAIN)
    {
      regName = "qpll_error_status_bcmain";
    }
  bool const tmp = readRegister(regName);
  return (tmp != 0x0);
}

bool
tcds::rf2ttc::VMEDeviceRF2TTC::isQPLLLocked(tcds::definitions::BC_SIGNAL const bcSignal) const
{
  std::string regName = "unkown";
  if (bcSignal == tcds::definitions::BC_SIGNAL_BC1)
    {
      regName = "qpll_lock_status_bc1";
    }
  else if (bcSignal == tcds::definitions::BC_SIGNAL_BC2)
    {
      regName = "qpll_lock_status_bc2";
    }
  else if (bcSignal == tcds::definitions::BC_SIGNAL_BCREF)
    {
      regName = "qpll_lock_status_bcref";
    }
  else if (bcSignal == tcds::definitions::BC_SIGNAL_BCMAIN)
    {
      regName = "qpll_lock_status_bcmain";
    }
  bool const tmp = readRegister(regName);
  return (tmp != 0x0);
}

bool
tcds::rf2ttc::VMEDeviceRF2TTC::isOrbitLocked(tcds::definitions::ORB_SIGNAL const orbSignal) const
{
  tcds::definitions::ORBIT_LOCK_STATUS const tmp = getOrbitStatus(orbSignal);
  return (tmp == tcds::definitions::ORBIT_LOCK_STATUS_OK);
}

tcds::definitions::ORBIT_LOCK_STATUS
tcds::rf2ttc::VMEDeviceRF2TTC::getOrbitStatus(tcds::definitions::ORB_SIGNAL const orbSignal) const
{
  std::string regNameLength = "unkown";
  std::string regNameCount = "unkown";
  if (orbSignal == tcds::definitions::ORB_SIGNAL_ORB1)
    {
      regNameLength = "orb1_period_rd";
      regNameCount = "orb1_counter";
    }
  else if (orbSignal == tcds::definitions::ORB_SIGNAL_ORB2)
    {
      regNameLength = "orb2_period_rd";
      regNameCount = "orb2_counter";
    }
  else if (orbSignal == tcds::definitions::ORB_SIGNAL_ORBMAIN)
    {
      regNameLength = "orbmain_period_rd";
      regNameCount = "orbmain_counter";
    }

  // Check if the orbit counter is increasing.
  // NOTE: Kinda brute-force, but it seems to do the trick.
  uint32_t count0 = readRegister(regNameCount);
  tcds::hwlayer::nanosleep(100000);
  uint32_t count1 = readRegister(regNameCount);
  tcds::hwlayer::nanosleep(100000);
  uint32_t count2 = readRegister(regNameCount);
  tcds::definitions::ORBIT_LOCK_STATUS res = tcds::definitions::ORBIT_LOCK_STATUS_OK;
  if ((count1 == count0) && (count2 == count0))
    {
      res = tcds::definitions::ORBIT_LOCK_STATUS_NO_ORBIT;
    }
  else
    {
      // Check the length of the last orbit. This should match the
      // expected LHC orbit length.
      uint32_t const lastOrbitLength = readRegister(regNameLength);
      if (lastOrbitLength != tcds::definitions::kNumBXPerOrbit)
        {
          res = tcds::definitions::ORBIT_LOCK_STATUS_WRONG_LENGTH;
        }
    }
  return res;
}

std::string
tcds::rf2ttc::VMEDeviceRF2TTC::readFirmwareVersionImpl() const
{
  // NOTE: The hexadecimal printed format of the firmware version
  // spells the firmware date as ddmmyyyy.
  uint32_t const tmpVal = hwDevice_.readRegister("firmware_id");
  std::stringstream tmp;
  tmp << std::hex << tmpVal;
  std::string const tmpStr = tmp.str();

  size_t const len = tmpStr.size();
  std::string const res =
    tmpStr.substr(len - 4, 4) +
    "-" +
    tmpStr.substr(len - 6, 2) +
    "-" +
    tmpStr.substr(len - 8, 2);
  return res;
}

uint32_t
tcds::rf2ttc::VMEDeviceRF2TTC::readRegisterImpl(std::string const& regName) const
{
  uint32_t res = 0;
  if (isDelay25Register(regName))
    {
      // Delay25 chip registers are treated in a special way.
      res = readDelay25Register(regName);
    }
  else
    {
      res = tcds::hwlayervme::VMEDeviceBase::readRegisterImpl(regName);
    }
  return res;
}

void
tcds::rf2ttc::VMEDeviceRF2TTC::writeRegisterImpl(std::string const& regName,
                                                 uint32_t const regVal) const
{
  if (isDelay25Register(regName))
    {
      // Delay25 chip registers are treated in a special way.
      writeDelay25Register(regName, regVal);
    }
  else
    {
      tcds::hwlayervme::VMEDeviceBase::writeRegisterImpl(regName, regVal);
    }
}

uint32_t
tcds::rf2ttc::VMEDeviceRF2TTC::readDelay25Register(std::string const& regName) const
{
  std::string const responseRegName = "delay25_reg";
  // Dummy read first, to trigger the i2C transaction.
  hwDevice_.readRegister(regName);
  // Wait at least 2 ms (to follow the prescription in the RF2TTC
  // manual).
  tcds::hwlayer::nanosleep(3000000);
  // And now a proper read from the response register to get the
  // result.
  uint32_t tmp = hwDevice_.readRegister(responseRegName);
  // Make sure to apply the correct mask corresponding to the original
  // register.
  uint32_t const mask = hwDevice_.getMask(regName);
  tmp &= mask;
  uint32_t helper = mask;
  while ((helper & 0x1) == 0x0)
    {
      tmp >>= 1;
      helper >>= 1;
    }
  uint32_t const res = tmp;
  return res;
}

void
tcds::rf2ttc::VMEDeviceRF2TTC::writeDelay25Register(std::string const& regName,
                                                    uint32_t const regVal) const
{
  // Writing the Delay25 registers is special in that we need to make
  // sure the enable bit does not inadvertently switch off.

  // The masking here ORs in the enable bit.
  uint32_t const enableBit = 0x40;
  if ((regName.find("delay25") != std::string::npos) &&
      (toolbox::endsWith(regName, "_val")))
    {
      std::string regNameTmp = regName;
      regNameTmp.erase(regName.size() - 4);
      hwDevice_.writeRegister(regNameTmp, regVal | enableBit);
    }
  else
    {
      hwDevice_.writeRegister(regName, regVal);
    }

  // NOTE: It appears that for some reason we need to give the
  // hardware a little bit of time to process the write. Without the
  // following sleep statement we sometimes read back zeros instead of
  // the correct value of the Delay25 register.
  tcds::hwlayer::nanosleep(3000000);
}

bool
tcds::rf2ttc::VMEDeviceRF2TTC::isDelay25Register(std::string const& regName) const
{
  // In the RF2TTC all Delay25 and all TTCrx registers are accessed
  // via i2C.
  bool res = (regName.find("delay25") != std::string::npos);
  res = res && (regName != "delay25_reg");
  return res;
}

uint32_t
tcds::rf2ttc::VMEDeviceRF2TTC::getCurrentSource(std::string const& regNameBase) const
{
  // First check the mode: manual or automatic.
  std::string regNameTmp = toolbox::toString("working_mode_%s", regNameBase.c_str());
  uint32_t const tmp = readRegister(regNameTmp);
  tcds::definitions::WORKING_MODE workingMode =
    static_cast<tcds::definitions::WORKING_MODE>(tmp);

  std::string regName = "";
  if (workingMode == tcds::definitions::WORKING_MODE_MANUAL)
    {
      // This case is straightforward.
      regName = toolbox::toString("man_select_%s", regNameBase.c_str());
    }
  else
    {
      // Automatic mode. Need to figure out the beam mode.
      uint32_t const beamModeNumberTmp = readRegister("bst_beam_mode");
      int beamModeNumber = beamModeNumberTmp;

      // Check the beam mode number we found. This should never be a
      // problem, but still.
      if ((beamModeNumber < tcds::definitions::kBeamModeMin) ||
          (beamModeNumber > tcds::definitions::kBeamModeMax))
        {
          std::string const msg =
            toolbox::toString("Received an impossible LHC beam mode via the BST: %d",
                              beamModeNumber);
          XCEPT_RAISE(tcds::exception::RuntimeProblem, msg);
        }

      // Then figure out if for this mode we're using the 'beam' or
      // the 'no-beam' definition.
      uint32_t const tmp =
        readRegister(toolbox::toString("beam_no_beam_def_mode%d", beamModeNumber));
      tcds::definitions::AUTOMATIC_MODE_SOURCE sourceDef =
        static_cast<tcds::definitions::AUTOMATIC_MODE_SOURCE>(tmp);

      if (sourceDef == tcds::definitions::AUTOMATIC_MODE_SOURCE_BEAM)
        {
          regName = toolbox::toString("beam_select_%s", regNameBase.c_str());
        }
      else
        {
          regName = toolbox::toString("nobeam_select_%s", regNameBase.c_str());
        }
    }
  uint32_t const currSrc = readRegister(regName);
  return currSrc;
}
