#include "tcds/rf2ttc/HwStatusInfoSpaceHandler.h"

#include <stdint.h>

#include "tcds/rf2ttc/Definitions.h"
#include "tcds/rf2ttc/Utils.h"
#include "tcds/utils/Definitions.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/Utils.h"
#include "tcds/utils/WebServer.h"

tcds::rf2ttc::HwStatusInfoSpaceHandler::HwStatusInfoSpaceHandler(xdaq::Application& xdaqApp,
                                                                       tcds::utils::InfoSpaceUpdater* updater) :
  InfoSpaceHandler(xdaqApp, "tcds-hw-status-rf2ttc", updater)
{
  // General status info.
  createUInt32("ttcrx_status");
  createUInt32("bst_beam_mode");

  // The currently-selected clock/orbit sources.
  createUInt32("curr_select_bcmain",
               0,
               "",
               tcds::utils::InfoSpaceItem::PROCESS);
  createUInt32("curr_select_orbmain",
               0,
               "",
               tcds::utils::InfoSpaceItem::PROCESS);
  createUInt32("qpll_lock_status_bcmain");
  createUInt32("orbit_lock_status_orbmain",
               0,
               "",
               tcds::utils::InfoSpaceItem::PROCESS);
}

tcds::rf2ttc::HwStatusInfoSpaceHandler::~HwStatusInfoSpaceHandler()
{
}

void
tcds::rf2ttc::HwStatusInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  // General status info.
  monitor.newItemSet("itemset-hw-status");
  monitor.addItem("itemset-hw-status",
                  "ttcrx_status",
                  "BST TTCrx status",
                  this);
  monitor.addItem("itemset-hw-status",
                  "bst_beam_mode",
                  "LHC beam mode (from BST)",
                  this);

  // An itemset for the currently-selected clock/orbit sources.
  monitor.newItemSet("itemset-current-sources");
  monitor.addItem("itemset-current-sources",
                  "curr_select_bcmain",
                  "Current BC-main source",
                  this);
  monitor.addItem("itemset-current-sources",
                  "curr_select_orbmain",
                  "Current Orbit-main source",
                  this);
  monitor.addItem("itemset-current-sources",
                  "qpll_lock_status_bcmain",
                  "BC-main QPLL locking status",
                  this);
  monitor.addItem("itemset-current-sources",
                  "orbit_lock_status_orbmain",
                  "Orbit-main 'locking' status",
                  this);
}

void
tcds::rf2ttc::HwStatusInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                      tcds::utils::Monitor& monitor,
                                                                      std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "Hardware status" : forceTabName;

  webServer.registerTab(tabName,
                        "Hardware information",
                        3);
  webServer.registerTable("BST status info",
                          "BST-related status info",
                          monitor,
                          "itemset-hw-status",
                          tabName);
  webServer.registerTable("CMS clock and orbit sources",
                          "Currently-selected clock and orbit sources",
                          monitor,
                          "itemset-current-sources",
                          tabName);
}

std::string
tcds::rf2ttc::HwStatusInfoSpaceHandler::formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const
{
  std::string res = tcds::utils::escapeAsJSONString(tcds::utils::InfoSpaceHandler::kInvalidItemString);
  if (item->isValid())
    {
      std::string name = item->name();
      if (name == "ttcrx_status")
        {
          uint32_t const value = getUInt32(name);
          tcds::definitions::TTCRX_STATUS const valueEnum =
            static_cast<tcds::definitions::TTCRX_STATUS>(value);
          res = tcds::utils::escapeAsJSONString(tcds::rf2ttc::ttcrxStatusToString(valueEnum));
        }
      else if (name == "bst_beam_mode")
        {
          uint32_t const value = getUInt32(name);
          tcds::definitions::BEAM_MODE const valueEnum =
            static_cast<tcds::definitions::BEAM_MODE>(value);
          res = tcds::utils::escapeAsJSONString(tcds::utils::beamModeToString(valueEnum));
        }
      else if (name == "curr_select_bcmain")
        {
          uint32_t const value = getUInt32(name);
          tcds::definitions::BC_MAIN_SOURCE const valueEnum =
            static_cast<tcds::definitions::BC_MAIN_SOURCE>(value);
          res = tcds::utils::escapeAsJSONString(tcds::rf2ttc::bcMainSourceToString(valueEnum));
        }
      else if (name == "curr_select_orbmain")
        {
          uint32_t const value = getUInt32(name);
          tcds::definitions::ORB_MAIN_SOURCE const valueEnum =
            static_cast<tcds::definitions::ORB_MAIN_SOURCE>(value);
          res = tcds::utils::escapeAsJSONString(tcds::rf2ttc::orbMainSourceToString(valueEnum));
        }
      else if (name == "qpll_lock_status_bcmain")
        {
          uint32_t const value = getUInt32(name);
          tcds::definitions::QPLL_LOCK_STATUS const valueEnum =
            static_cast<tcds::definitions::QPLL_LOCK_STATUS>(value);
          res = tcds::utils::escapeAsJSONString(tcds::rf2ttc::qpllLockStatusToString(valueEnum));
        }
      else if (name == "orbit_lock_status_orbmain")
        {
          uint32_t const value = getUInt32(name);
          tcds::definitions::ORBIT_LOCK_STATUS const valueEnum =
            static_cast<tcds::definitions::ORBIT_LOCK_STATUS>(value);
          res = tcds::utils::escapeAsJSONString(tcds::rf2ttc::orbitLockStatusToString(valueEnum));
        }
      else
        {
          // For everything else simply call the generic formatter.
          res = InfoSpaceHandler::formatItem(item);
        }
    }
  return res;
}
