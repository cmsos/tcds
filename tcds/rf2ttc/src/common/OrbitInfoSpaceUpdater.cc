#include "tcds/rf2ttc/OrbitInfoSpaceUpdater.h"

#include <stdint.h>
#include <string>

#include "toolbox/string.h"
#include "toolbox/TimeVal.h"

#include "tcds/rf2ttc/Definitions.h"
#include "tcds/rf2ttc/Utils.h"
#include "tcds/rf2ttc/VMEDeviceRF2TTC.h"
#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/InfoSpaceItem.h"

tcds::rf2ttc::OrbitInfoSpaceUpdater::OrbitInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                                           tcds::rf2ttc::VMEDeviceRF2TTC const& hw) :
  tcds::utils::HwInfoSpaceUpdaterBase(xdaqApp, hw),
  nextUpdateIsReset_(true)
{
}

tcds::rf2ttc::OrbitInfoSpaceUpdater::~OrbitInfoSpaceUpdater()
{
}

void
tcds::rf2ttc::OrbitInfoSpaceUpdater::updateInfoSpaceImpl(tcds::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  tcds::utils::HwInfoSpaceUpdaterBase::updateInfoSpaceImpl(infoSpaceHandler);
  nextUpdateIsReset_ = false;
}

bool
tcds::rf2ttc::OrbitInfoSpaceUpdater::updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                                             tcds::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  bool updated = false;
  tcds::rf2ttc::VMEDeviceRF2TTC const& hw = getHw();
  if (hw.isReadyForUse())
    {
      std::string name = item.name();
      tcds::utils::InfoSpaceItem::UpdateType updateType = item.updateType();
      if (updateType == tcds::utils::InfoSpaceItem::PROCESS)
        {
          // The 'PROCESS' update type means that there is something
          // special to the variable. Figure out what to do based on
          // the variable name.
          if (toolbox::startsWith(name, "orbit_lock_status_orb"))
            {
              // Step 1: handle the flag.
              uint32_t const oldVal = infoSpaceHandler->getUInt32(name);
              std::string const suffix = name.substr(name.find("_orb") + 1);
              tcds::definitions::ORBIT_LOCK_STATUS const newVal =
                hw.getOrbitStatus(orbSignalFromRegSuffix(suffix));
              infoSpaceHandler->setUInt32(name, static_cast<uint32_t>(newVal));
              updated = true;

              // Step 2: handle the counter.
              std::string counterName = name;
              counterName.insert(counterName.find("status") + 6, "_count");
              uint32_t const oldCounterVal = infoSpaceHandler->getUInt32(counterName);
              uint32_t newCounterVal = oldCounterVal;
              if (nextUpdateIsReset_)
                {
                  newCounterVal = 0;
                }
              // If the error flag changed from 'good' to 'bad':
              // update the corresponding counter.
              // NOTE: This means from zero to anything-but-zero.
              if ((oldVal == 0) && (newVal != 0))
                {
                  newCounterVal += 1;
                }
              infoSpaceHandler->setUInt32(counterName, newCounterVal);

              // Step 3: handle the unlock time if applicable.
              std::string timestampName =
                "orbit_latest_unlock_time_" + name.substr(name.find("_orb") + 1);
              toolbox::TimeVal const oldTimeVal = infoSpaceHandler->getTimeVal(timestampName);
              toolbox::TimeVal newTimeVal = oldTimeVal;
              if (nextUpdateIsReset_)
                {
                  newTimeVal = 0;
                }
              if (toolbox::startsWith(name, "orbit_lock_") && ((oldVal == 0) && (newVal != 0)))
                {
                  newTimeVal = toolbox::TimeVal::gettimeofday();
                }
              infoSpaceHandler->setTimeVal(timestampName, newTimeVal);
            }
          else if (toolbox::startsWith(name, "orbit_lock_status_count_") ||
                   toolbox::startsWith(name, "orbit_latest_unlock_time_"))
            {
              // These are already updated together with the
              // corresponding flags (see above).
              updated = true;
            }
          // TODO TODO TODO
          // Not very pretty...
          else if (name == "curr_select_orbmain")
            {
              uint32_t const newVal = hw.getCurrentOrbitMainSource();
              infoSpaceHandler->setUInt32(name, newVal);
              updated = true;
            }
          else if (name == "curr_select_orb1")
            {
              uint32_t const newVal = hw.getCurrentOrbitSource(tcds::definitions::ORB_SIGNAL_ORB1);
              infoSpaceHandler->setUInt32(name, newVal);
              updated = true;
            }
          else if (name == "curr_select_orb2")
            {
              uint32_t const newVal = hw.getCurrentOrbitSource(tcds::definitions::ORB_SIGNAL_ORB2);
              infoSpaceHandler->setUInt32(name, newVal);
              updated = true;
            }
          // TODO TODO TODO end
        }
      if (!updated)
        {
          updated = tcds::utils::HwInfoSpaceUpdaterBase::updateInfoSpaceItem(item, infoSpaceHandler);
        }
    }

  if (updated)
    {
      item.setValid();
    }
  else
    {
      item.setInvalid();
    }

  return updated;
}

void
tcds::rf2ttc::OrbitInfoSpaceUpdater::resetCounters()
{
  // Reset the unlock counters upon the next update.
  nextUpdateIsReset_ = true;
}

tcds::rf2ttc::VMEDeviceRF2TTC const&
tcds::rf2ttc::OrbitInfoSpaceUpdater::getHw() const
{
  return dynamic_cast<tcds::rf2ttc::VMEDeviceRF2TTC const&>(tcds::utils::HwInfoSpaceUpdaterBase::getHw());
}
