#include "tcds/rf2ttc/BunchClockInfoSpaceUpdater.h"

#include <stdint.h>
#include <string>

#include "toolbox/string.h"
#include "toolbox/TimeVal.h"

#include "tcds/rf2ttc/Definitions.h"
#include "tcds/rf2ttc/VMEDeviceRF2TTC.h"
#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/InfoSpaceItem.h"

tcds::rf2ttc::BunchClockInfoSpaceUpdater::BunchClockInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                                                     tcds::rf2ttc::VMEDeviceRF2TTC const& hw) :
  tcds::utils::HwInfoSpaceUpdaterBase(xdaqApp, hw),
  nextUpdateIsReset_(true)
{
}

tcds::rf2ttc::BunchClockInfoSpaceUpdater::~BunchClockInfoSpaceUpdater()
{
}

void
tcds::rf2ttc::BunchClockInfoSpaceUpdater::updateInfoSpaceImpl(tcds::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  tcds::utils::HwInfoSpaceUpdaterBase::updateInfoSpaceImpl(infoSpaceHandler);
  nextUpdateIsReset_ = false;
}

bool
tcds::rf2ttc::BunchClockInfoSpaceUpdater::updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                                              tcds::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  bool updated = false;
  tcds::rf2ttc::VMEDeviceRF2TTC const& hw = getHw();
  if (hw.isReadyForUse())
    {
      std::string const name = item.name();
      tcds::utils::InfoSpaceItem::UpdateType updateType = item.updateType();
      if (updateType == tcds::utils::InfoSpaceItem::PROCESS)
        {
          // The 'PROCESS' update type means that there is something
          // special to the variable. Figure out what to do based on
          // the variable name.
          if (toolbox::startsWith(name, "qpll_error_status_bc") ||
              toolbox::startsWith(name, "qpll_lock_status_bc"))
            {
              // Step 1: handle the flag.
              uint32_t const oldVal = infoSpaceHandler->getUInt32(name);
              uint32_t const newVal = hw.readRegister(name);
              infoSpaceHandler->setUInt32(name, newVal);
              updated = true;

              // Step 2: handle the counter.
              std::string counterName = name;
              counterName.insert(counterName.find("status") + 6, "_count");
              uint32_t const oldCounterVal = infoSpaceHandler->getUInt32(counterName);
              uint32_t newCounterVal = oldCounterVal;
              if (nextUpdateIsReset_)
                {
                  newCounterVal = 0;
                }
              // If the error flag changed from 'good' to 'bad':
              // update the corresponding counter.

              // NOTE: For the error flag 'good' is 0, for the locking
              // flag 'good' is 1.
              if ((toolbox::startsWith(name, "qpll_error_") && ((oldVal == 0) && (newVal == 1))) ||
                  (toolbox::startsWith(name, "qpll_lock_") && ((oldVal == 1) && (newVal == 0))))
                {
                  newCounterVal += 1;
                }
              infoSpaceHandler->setUInt32(counterName, newCounterVal);

              // Step 3: handle the unlock time if applicable.
              std::string const timestampName =
                "qpll_latest_unlock_time_" + name.substr(name.find("bc"));
              toolbox::TimeVal const oldTimeVal = infoSpaceHandler->getTimeVal(timestampName);
              toolbox::TimeVal newTimeVal = oldTimeVal;
              if (nextUpdateIsReset_)
                {
                  newTimeVal = 0;
                }
              if (toolbox::startsWith(name, "qpll_lock_") && ((oldVal == 1) && (newVal == 0)))
                {
                  newTimeVal = toolbox::TimeVal::gettimeofday();
                }
              infoSpaceHandler->setTimeVal(timestampName, newTimeVal);
            }
          else if (toolbox::startsWith(name, "qpll_error_status_count_bc") ||
                   toolbox::startsWith(name, "qpll_lock_status_count_bc") ||
                   toolbox::startsWith(name, "qpll_latest_unlock_time_bc"))
            {
              // These are already updated together with the
              // corresponding flags (see above).
              updated = true;
            }
          // TODO TODO TODO
          // Not very pretty...
          else if (name == "curr_select_bcmain")
            {
              uint32_t const newVal = hw.getCurrentBCMainSource();
              infoSpaceHandler->setUInt32(name, newVal);
              updated = true;
            }
          else if (name == "curr_select_bcref")
            {
              uint32_t const newVal = hw.getCurrentBCSource(tcds::definitions::BC_SIGNAL_BCREF);
              infoSpaceHandler->setUInt32(name, newVal);
              updated = true;
            }
          else if (name == "curr_select_bc1")
            {
              uint32_t const newVal = hw.getCurrentBCSource(tcds::definitions::BC_SIGNAL_BC1);
              infoSpaceHandler->setUInt32(name, newVal);
              updated = true;
            }
          else if (name == "curr_select_bc2")
            {
              uint32_t const newVal = hw.getCurrentBCSource(tcds::definitions::BC_SIGNAL_BC2);
              infoSpaceHandler->setUInt32(name, newVal);
              updated = true;
            }
          // TODO TODO TODO end
          else if (toolbox::startsWith(name, "qpll_error_status_count_bc"))
            {
              updated = true;
            }
        }
      if (!updated)
        {
          updated = tcds::utils::HwInfoSpaceUpdaterBase::updateInfoSpaceItem(item, infoSpaceHandler);
        }
    }

  if (updated)
    {
      item.setValid();
    }
  else
    {
      item.setInvalid();
    }

  return updated;
}

void
tcds::rf2ttc::BunchClockInfoSpaceUpdater::resetCounters()
{
  // Reset the QPLL error and unlock counters of each of the BC
  // outputs upon the next update.
  nextUpdateIsReset_ = true;
}

tcds::rf2ttc::VMEDeviceRF2TTC const&
tcds::rf2ttc::BunchClockInfoSpaceUpdater::getHw() const
{
  return dynamic_cast<tcds::rf2ttc::VMEDeviceRF2TTC const&>(tcds::utils::HwInfoSpaceUpdaterBase::getHw());
}
