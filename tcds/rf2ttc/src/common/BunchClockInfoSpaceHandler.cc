#include "tcds/rf2ttc/BunchClockInfoSpaceHandler.h"

#include <stdint.h>

#include "toolbox/string.h"

#include "tcds/rf2ttc/Definitions.h"
#include "tcds/rf2ttc/Utils.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/Utils.h"
#include "tcds/utils/WebServer.h"

tcds::rf2ttc::BunchClockInfoSpaceHandler::BunchClockInfoSpaceHandler(xdaq::Application& xdaqApp,
                                                                     tcds::utils::InfoSpaceUpdater* updater) :
  InfoSpaceHandler(xdaqApp, "tcds-rf2ttc-bunchclocks", updater)
{
  // The working mode of each of the BC outputs (manual/automatic).
  for (int bcSignal = tcds::definitions::kBCSignalMin;
       bcSignal <= tcds::definitions::kBCSignalMax;
       ++bcSignal)
    {
      tcds::definitions::BC_SIGNAL tmp =
        static_cast<tcds::definitions::BC_SIGNAL>(bcSignal);
      std::string const regNameSuffix = bcSignalRegSuffix(tmp);
      createUInt32("working_mode_" + regNameSuffix);
    }

  //----------

  // The source settings for the different RF2TTC modes for each of
  // the BC outputs.
  for (int bcSignal = tcds::definitions::kBCSignalMin;
       bcSignal <= tcds::definitions::kBCSignalMax;
       ++bcSignal)
    {
      tcds::definitions::BC_SIGNAL tmp =
        static_cast<tcds::definitions::BC_SIGNAL>(bcSignal);
      std::string const regNameSuffix = bcSignalRegSuffix(tmp);
      // Source for 'manual' mode.
      createUInt32("man_select_" + regNameSuffix);
      // Source for 'beam' mode.
      createUInt32("beam_select_" + regNameSuffix);
      // Source for 'no-beam' mode.
      createUInt32("nobeam_select_" + regNameSuffix);
    }

  //----------

  // The currently-selected sources.
  for (int bcSignal = tcds::definitions::kBCSignalMin;
       bcSignal <= tcds::definitions::kBCSignalMax;
       ++bcSignal)
    {
      tcds::definitions::BC_SIGNAL tmp =
        static_cast<tcds::definitions::BC_SIGNAL>(bcSignal);
      std::string const regNameSuffix = bcSignalRegSuffix(tmp);
      createUInt32("curr_select_" + regNameSuffix,
                   0,
                   "",
                   tcds::utils::InfoSpaceItem::PROCESS);
    }

  //----------

  // Input DAC settings.
  for (int bcSignal = tcds::definitions::kBCSignalMin;
       bcSignal <= tcds::definitions::kBCSignalMax;
       ++bcSignal)
    {
      tcds::definitions::BC_SIGNAL tmp =
        static_cast<tcds::definitions::BC_SIGNAL>(bcSignal);
      // NOTE: BC-main is not an input, so it has no input DAC.
      if (tmp != tcds::definitions::BC_SIGNAL_BCMAIN)
        {
          std::string const regNamePrefix = bcSignalRegSuffix(tmp);
          createUInt32(regNamePrefix + "_dac");
        }
    }

  //----------

  // The delay25 chip settings for each of the BC outputs.
  for (int bcSignal = tcds::definitions::kBCSignalMin;
       bcSignal <= tcds::definitions::kBCSignalMax;
       ++bcSignal)
    {
      tcds::definitions::BC_SIGNAL tmp =
        static_cast<tcds::definitions::BC_SIGNAL>(bcSignal);
      std::string const regNameSuffix = bcSignalRegSuffix(tmp);
      createBool("bc_delay25_" + regNameSuffix + "_enable",
                 false,
                 "enabled/disabled");
      createUInt32("bc_delay25_" + regNameSuffix + "_val");
    }

  //----------

  // The QPLL relocking mode (auto/manual) for each of the BC inputs.
  for (int bcSignal = tcds::definitions::kBCSignalMin;
       bcSignal <= tcds::definitions::kBCSignalMax;
       ++bcSignal)
    {
      tcds::definitions::BC_SIGNAL tmp =
        static_cast<tcds::definitions::BC_SIGNAL>(bcSignal);
      std::string const regNameSuffix = bcSignalRegSuffix(tmp);
      createUInt32("qpll_mode_" + regNameSuffix);
    }

  // NOTE: The QPLL error and locking status registers are
  // latched. Once an error or an unlock is detected, these bits stay
  // latched, and are cleared upon read. For this reason we add
  // 'status counters' to each of these latched registers. These
  // counters will be updated automatically, together with the
  // corresponding latched bits.

  // The QPLL error status of each of the BC outputs.
  for (int bcSignal = tcds::definitions::kBCSignalMin;
       bcSignal <= tcds::definitions::kBCSignalMax;
       ++bcSignal)
    {
      tcds::definitions::BC_SIGNAL tmp =
        static_cast<tcds::definitions::BC_SIGNAL>(bcSignal);
      std::string const regNameSuffix = bcSignalRegSuffix(tmp);
      createUInt32("qpll_error_status_" + regNameSuffix,
                   0,
                   "",
                   tcds::utils::InfoSpaceItem::PROCESS);
      createUInt32("qpll_error_status_count_" + regNameSuffix,
                   0,
                   "",
                   tcds::utils::InfoSpaceItem::PROCESS);
    }

  // The QPLL locking status of each of the BC outputs.
  for (int bcSignal = tcds::definitions::kBCSignalMin;
       bcSignal <= tcds::definitions::kBCSignalMax;
       ++bcSignal)
    {
      tcds::definitions::BC_SIGNAL tmp =
        static_cast<tcds::definitions::BC_SIGNAL>(bcSignal);
      std::string const regNameSuffix = bcSignalRegSuffix(tmp);
      createUInt32("qpll_lock_status_" + regNameSuffix,
                   0,
                   "",
                   tcds::utils::InfoSpaceItem::PROCESS);
      createUInt32("qpll_lock_status_count_" + regNameSuffix,
                   0,
                   "",
                   tcds::utils::InfoSpaceItem::PROCESS);
    }

  // The timestamp of the latest QPLL unlock for each of the BC outputs.
  for (int bcSignal = tcds::definitions::kBCSignalMin;
       bcSignal <= tcds::definitions::kBCSignalMax;
       ++bcSignal)
    {
      tcds::definitions::BC_SIGNAL tmp =
        static_cast<tcds::definitions::BC_SIGNAL>(bcSignal);
      std::string const suffix = bcSignalRegSuffix(tmp);
      createTimeVal("qpll_latest_unlock_time_" + suffix,
                    0,
                    "time_val_no_zero",
                    tcds::utils::InfoSpaceItem::PROCESS);
    }
}

tcds::rf2ttc::BunchClockInfoSpaceHandler::~BunchClockInfoSpaceHandler()
{
}

void
tcds::rf2ttc::BunchClockInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  for (int bcSignal = tcds::definitions::kBCSignalMin;
       bcSignal <= tcds::definitions::kBCSignalMax;
       ++bcSignal)
    {
      tcds::definitions::BC_SIGNAL tmp =
        static_cast<tcds::definitions::BC_SIGNAL>(bcSignal);
      std::string const regNameSuffix = bcSignalRegSuffix(tmp);
      monitor.newItemSet("itemset-bunchclock-" + regNameSuffix);
      monitor.addItem("itemset-bunchclock-" + regNameSuffix,
                      "working_mode_" + regNameSuffix,
                      "Working mode",
                      this);
      monitor.addItem("itemset-bunchclock-" + regNameSuffix,
                      "man_select_" + regNameSuffix,
                      "Source for mode 'manual'",
                      this);
      monitor.addItem("itemset-bunchclock-" + regNameSuffix,
                      "beam_select_" + regNameSuffix,
                      "Source for mode 'beam'",
                      this);
      monitor.addItem("itemset-bunchclock-" + regNameSuffix,
                      "nobeam_select_" + regNameSuffix,
                      "Source for mode 'no-beam'",
                      this);
      monitor.addItem("itemset-bunchclock-" + regNameSuffix,
                      "curr_select_" + regNameSuffix,
                      "Selected source",
                      this);
      // BC-main is not an input, so has no input DAC setting.
      if (tmp != tcds::definitions::BC_SIGNAL_BCMAIN)
        {
          monitor.addItem("itemset-bunchclock-" + regNameSuffix,
                          regNameSuffix + "_dac",
                          "Input DAC threshold setting",
                          this);
        }

      monitor.addItem("itemset-bunchclock-" + regNameSuffix,
                      "bc_delay25_" + regNameSuffix + "_enable",
                      "Output phase adjustment Delay25 chip",
                      this);
      monitor.addItem("itemset-bunchclock-" + regNameSuffix,
                      "bc_delay25_" + regNameSuffix + "_val",
                      "Output phase delay (0.5 ns steps)",
                      this);

      monitor.addItem("itemset-bunchclock-" + regNameSuffix,
                      "qpll_mode_" + regNameSuffix,
                      "QPLL relocking mode",
                      this);
      monitor.addItem("itemset-bunchclock-" + regNameSuffix,
                      "qpll_error_status_" + regNameSuffix,
                      "QPLL error status",
                      this);
      monitor.addItem("itemset-bunchclock-" + regNameSuffix,
                      "qpll_error_status_count_" + regNameSuffix,
                      "QPLL error count",
                      this);
      monitor.addItem("itemset-bunchclock-" + regNameSuffix,
                      "qpll_lock_status_" + regNameSuffix,
                      "QPLL locking status",
                      this);
      monitor.addItem("itemset-bunchclock-" + regNameSuffix,
                      "qpll_lock_status_count_" + regNameSuffix,
                      "QPLL unlock count",
                      this);
      monitor.addItem("itemset-bunchclock-" + regNameSuffix,
                      "qpll_latest_unlock_time_" + regNameSuffix,
                      "Timestamp of latest QPLL unlock",
                      this);
    }
}

void
tcds::rf2ttc::BunchClockInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                         tcds::utils::Monitor& monitor,
                                                                        std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "Bunch clocks" : forceTabName;

  webServer.registerTab(tabName,
                        "Information related to the 40 MHz bunch clock(s)",
                        4);

  for (int bcSignal = tcds::definitions::kBCSignalMin;
       bcSignal <= tcds::definitions::kBCSignalMax;
       ++bcSignal)
    {
      tcds::definitions::BC_SIGNAL tmp =
        static_cast<tcds::definitions::BC_SIGNAL>(bcSignal);
      std::string const regNameSuffix = bcSignalRegSuffix(tmp);
      std::string const signalLabel = bcSignalToString(tmp);
      webServer.registerTable(signalLabel + " info",
                              signalLabel + "-related info",
                              monitor,
                              "itemset-bunchclock-" + regNameSuffix,
                              tabName);
    }
}

std::string
tcds::rf2ttc::BunchClockInfoSpaceHandler::formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const
{
  std::string res = tcds::utils::escapeAsJSONString(tcds::utils::InfoSpaceHandler::kInvalidItemString);
  if (item->isValid())
    {
      std::string name = item->name();
      if (toolbox::startsWith(name, "working_mode"))
        {
          uint32_t const value = getUInt32(name);
          tcds::definitions::WORKING_MODE const valueEnum =
            static_cast<tcds::definitions::WORKING_MODE>(value);
          res = tcds::utils::escapeAsJSONString(tcds::rf2ttc::workingModeToString(valueEnum));
        }
      else if (toolbox::startsWith(name, "qpll_mode"))
        {
          uint32_t const value = getUInt32(name);
          tcds::definitions::QPLL_RELOCKING_MODE const valueEnum =
            static_cast<tcds::definitions::QPLL_RELOCKING_MODE>(value);
          res = tcds::utils::escapeAsJSONString(tcds::rf2ttc::qpllRelockingModeToString(valueEnum));
        }
      else if (toolbox::startsWith(name, "qpll_error_status") &&
               (name.find("count") == std::string::npos))
        {
          uint32_t const value = getUInt32(name);
          tcds::definitions::QPLL_ERROR_STATUS const valueEnum =
            static_cast<tcds::definitions::QPLL_ERROR_STATUS>(value);
          res = tcds::utils::escapeAsJSONString(tcds::rf2ttc::qpllErrorStatusToString(valueEnum));
        }
      else if (toolbox::startsWith(name, "qpll_lock_status") &&
               (name.find("count") == std::string::npos))
        {
          uint32_t const value = getUInt32(name);
          tcds::definitions::QPLL_LOCK_STATUS const valueEnum =
            static_cast<tcds::definitions::QPLL_LOCK_STATUS>(value);
          res = tcds::utils::escapeAsJSONString(tcds::rf2ttc::qpllLockStatusToString(valueEnum));
        }
      else if (toolbox::startsWith(name, "man_select") ||
               toolbox::startsWith(name, "beam_select") ||
               toolbox::startsWith(name, "nobeam_select"))
        {
          uint32_t const value = getUInt32(name);
          if (name.find("bcmain") != std::string::npos)
            {
              tcds::definitions::BC_MAIN_SOURCE const valueEnum =
                static_cast<tcds::definitions::BC_MAIN_SOURCE>(value);
              res = tcds::utils::escapeAsJSONString(tcds::rf2ttc::bcMainSourceToString(valueEnum));
            }
          else
            {
              tcds::definitions::BC_SOURCE const valueEnum =
                static_cast<tcds::definitions::BC_SOURCE>(value);
              res = tcds::utils::escapeAsJSONString(tcds::rf2ttc::bcSourceToString(valueEnum));
            }
        }
      else if (name == "curr_select_bcmain")
        {
          uint32_t const value = getUInt32(name);
          tcds::definitions::BC_MAIN_SOURCE const valueEnum =
            static_cast<tcds::definitions::BC_MAIN_SOURCE>(value);
          res = tcds::utils::escapeAsJSONString(tcds::rf2ttc::bcMainSourceToString(valueEnum));
        }
      else if (toolbox::startsWith(name, "curr_select_bc"))
        {
          uint32_t const value = getUInt32(name);
          tcds::definitions::BC_SOURCE const valueEnum =
            static_cast<tcds::definitions::BC_SOURCE>(value);
          res = tcds::utils::escapeAsJSONString(tcds::rf2ttc::bcSourceToString(valueEnum));
        }
      else
        {
          // For everything else simply call the generic formatter.
          res = InfoSpaceHandler::formatItem(item);
        }
    }
  return res;
}
