#include "tcds/hwlayerscpi/HwDeviceSCPI.h"

#include <cerrno>
#include <cstring>
#include <netdb.h>
#include <cstddef>
#include <sys/socket.h>
#include <sys/time.h>
#include <unistd.h>

#include "toolbox/string.h"
#include "xcept/Exception.h"

#include "tcds/exception/Exception.h"
#include "tcds/utils/Resolver.h"

tcds::hwlayerscpi::HwDeviceSCPI::HwDeviceSCPI()  :
  socket_(-1)
{
}

tcds::hwlayerscpi::HwDeviceSCPI::~HwDeviceSCPI()
{
}

void
tcds::hwlayerscpi::HwDeviceSCPI::hwConnect(std::string const& hostName,
                                           uint32_t const portNumber)
{
  // Resolve the host name.
  tcds::utils::Resolver resolver;
  std::vector<struct addrinfo> hostInfos;
  try
    {
      hostInfos = resolver.resolve(hostName, portNumber);
    }
  catch (tcds::exception::Exception const& err)
    {
      std::string const msg =
        toolbox::toString("Failed to resolve SCPI device host name '%s'.",
                          hostName.c_str());
      XCEPT_RAISE(tcds::exception::RuntimeProblem, msg.c_str());
    }
  struct addrinfo const hostInfo = hostInfos.at(0);

  // Create the network socket.
  socket_ = ::socket(hostInfo.ai_family, hostInfo.ai_socktype, hostInfo.ai_protocol);
  if (socket_ < 0)
    {
      std::string const msg =
        toolbox::toString("Failed to create network socket (to connect to the SCPI device): '%s'.",
                          std::strerror(errno));
      XCEPT_RAISE(tcds::exception::RuntimeProblem, msg.c_str());
    }

  // Set the timeouts of this socket to 10 seconds.
  struct timeval tv;
  tv.tv_sec = 10;
  tv.tv_usec = 0;
  setsockopt(socket_, SOL_SOCKET, SO_SNDTIMEO, (void*)&tv, sizeof(tv));
  setsockopt(socket_, SOL_SOCKET, SO_RCVTIMEO, (void*)&tv, sizeof(tv));

  // Build connection object and connect.
  if (::connect(socket_, hostInfo.ai_addr, hostInfo.ai_addrlen) < 0)
    {
      std::string const msg =
        toolbox::toString("Failed to connect to the SCPI device '%s:%d': '%s'.",
                          hostName.c_str(),
                          portNumber,
                          std::strerror(errno));

      // Don't forget to clean up the socket!
      ::close(socket_);
      socket_ = -1;

      XCEPT_RAISE(tcds::exception::HardwareProblem, msg.c_str());
    }
}

void
tcds::hwlayerscpi::HwDeviceSCPI::hwRelease()
{
  if (socket_ >= 0)
    {
      ::close(socket_);
    }
  socket_ = -1;
}

bool
tcds::hwlayerscpi::HwDeviceSCPI::isHwConnected() const
{
  return (socket_ >= 0);
}

uint32_t
tcds::hwlayerscpi::HwDeviceSCPI::readRegister(std::string const& regName) const
{
  // BUG BUG BUG
  // Lacks implementation.
  return 0;
  // BUG BUG BUG end
}

void
tcds::hwlayerscpi::HwDeviceSCPI::writeRegister(std::string const& regName,
                                               uint32_t const regVal) const
{
  // BUG BUG BUG
  // Lacks implementation.
  // BUG BUG BUG end
}

std::vector<uint32_t>
tcds::hwlayerscpi::HwDeviceSCPI::readBlock(std::string const& regName,
                                           uint32_t const nWords) const
{
  // BUG BUG BUG
  // Lacks implementation.
  std::vector<uint32_t> const res;
  return res;
  // BUG BUG BUG end
}

std::vector<uint32_t>
tcds::hwlayerscpi::HwDeviceSCPI::readBlockOffset(std::string const& regName,
                                                 uint32_t const nWords,
                                                 uint32_t const offset) const
{
  // BUG BUG BUG
  // Lacks implementation.
  std::vector<uint32_t> const res;
  return res;
  // BUG BUG BUG end
}

void
tcds::hwlayerscpi::HwDeviceSCPI::writeBlock(std::string const& regName,
                                            std::vector<uint32_t> const regVal) const
{
  // BUG BUG BUG
  // Lacks implementation.
  // BUG BUG BUG end
}

uint32_t
tcds::hwlayerscpi::HwDeviceSCPI::getBlockSize(std::string const& regName) const
{
  // Dummy.
  return 0;
}

std::vector<std::string>
tcds::hwlayerscpi::HwDeviceSCPI::getRegisterNames() const
{
  // Dummy.
  std::vector<std::string> const res;
  return res;
}

tcds::hwlayer::RegisterInfo::RegInfoVec
tcds::hwlayerscpi::HwDeviceSCPI::getRegisterInfos() const
{
  // Dummy.
  tcds::hwlayer::RegisterInfo::RegInfoVec const regInfos;
  return regInfos;
}

tcds::hwlayer::RegDumpVec
tcds::hwlayerscpi::HwDeviceSCPI::dumpRegisterContents() const
{
  tcds::hwlayer::RegDumpVec res;

  // BUG BUG BUG
  // Lacks implementation.
  return res;
  // BUG BUG BUG end
}

// HAL::VMEDevice&
// tcds::hwlayerscpi::HwDeviceSCPI::getHwDevice() const
// {
//   if (!isHwConnected())
//     {
//       std::string msg = "Trying to control the hardware, "
//         "but the XDAQ control application is not (yet) connected "
//         "to the hardware. (Unless something is very wrong, "
//         "this simply means you have to 'Configure' first.)";
//       XCEPT_RAISE(tcds::exception::SoftwareProblem, msg);
//     }
//   else
//     {
//       HAL::VMEDevice& hw = static_cast<HAL::VMEDevice&>(*hwP_);
//       return hw;
//     }
// }

std::string
tcds::hwlayerscpi::HwDeviceSCPI::execCommand(std::string const& cmd) const
{
  // If this is a query, we expect to get a reply. Otherwise it's a
  // 'fire and forget' command.
  bool const expectAnswer = toolbox::endsWith(cmd, "?");
  std::string res = "";
  sendCommand(cmd);
  if (expectAnswer)
    {
      std::string rawReply = readRawData();
      res = rawReply;
      // NOTE: If the reply ends with ';1', this must be (or at least
      // we hope so) the result of an '*OPC?' query added to the
      // command.
      if (rawReply.size() > 2)
        {
          std::string const ending = rawReply.substr(rawReply.size() - 2, 2);
          if (ending == ";1")
            {
              res = rawReply.substr(0, rawReply.size() - 2);
            }
        }
    }
  return res;
}

void
tcds::hwlayerscpi::HwDeviceSCPI::sendCommand(std::string const& cmd,
                                             bool const appendOPC) const
{
  // NOTE: Appending a *OPC? leads to problems with 'long-running'
  // commands, like *IDN?. Typically, this results in a '-440, "Query
  // UNTERMINATED after indefinite response"' error.
  std::string tmp = cmd;
  if (appendOPC)
    {
      tmp = tmp + ";" + "*OPC?";
    }
  tmp += "\r\n";
  send(socket_, tmp.c_str(), tmp.size(), 0);
}

std::string
tcds::hwlayerscpi::HwDeviceSCPI::readRawData() const
{
  size_t const bufSize = 1024;
  char buf[bufSize];
  int numBytes = recv(socket_, buf, bufSize, 0);
  if (numBytes < 1)
    {
      std::string msg = "Failed to read from the SCPI device";
      if (errno < 0)
        {
          msg = toolbox::toString("%s: '%s'", msg.c_str(), std::strerror(errno));
        }
      msg += ".";
      XCEPT_RAISE(tcds::exception::RuntimeProblem, msg.c_str());
    }

  std::string const rawData(buf, numBytes);
  std::string const res = toolbox::trim(rawData);
  return res;
}
