#include "tcds/hwlayerscpi/SCPIDeviceBase.h"

#include <unistd.h>
#include <sstream>

tcds::hwlayerscpi::SCPIDeviceBase::SCPIDeviceBase()
{
}

tcds::hwlayerscpi::SCPIDeviceBase::~SCPIDeviceBase()
{
}

bool
tcds::hwlayerscpi::SCPIDeviceBase::isReadyForUseImpl() const
{
  return isHwConnected();
}

void
tcds::hwlayerscpi::SCPIDeviceBase::hwConnect(std::string const& hostName,
                                             uint32_t const portNumber)
{
  hwConnectImpl(hostName, portNumber);
}

void
tcds::hwlayerscpi::SCPIDeviceBase::hwConnectImpl(std::string const& hostName,
                                                 uint32_t const portNumber)
{
  hwDevice_.hwConnect(hostName, portNumber);
}

void
tcds::hwlayerscpi::SCPIDeviceBase::hwRelease()
{
  hwReleaseImpl();
}

void
tcds::hwlayerscpi::SCPIDeviceBase::hwReleaseImpl()
{
  hwDevice_.hwRelease();
}

tcds::hwlayer::RegDumpVec
tcds::hwlayerscpi::SCPIDeviceBase::dumpRegisterContentsImpl() const
{
  return hwDevice_.dumpRegisterContents();
}

bool
tcds::hwlayerscpi::SCPIDeviceBase::isHwConnected() const
{
  return hwDevice_.isHwConnected();
}

void
tcds::hwlayerscpi::SCPIDeviceBase::reset() const
{
  return resetImpl();
}

void
tcds::hwlayerscpi::SCPIDeviceBase::resetImpl() const
{
  execCommand("*RST");
}

void
tcds::hwlayerscpi::SCPIDeviceBase::clearStatus() const
{
  return clearStatusImpl();
}

void
tcds::hwlayerscpi::SCPIDeviceBase::clearStatusImpl() const
{
  execCommand("*CLS");
}

std::string
tcds::hwlayerscpi::SCPIDeviceBase::readDeviceID() const
{
  return readDeviceIDImpl();
}

std::string
tcds::hwlayerscpi::SCPIDeviceBase::readDeviceIDImpl() const
{
  std::string const deviceId = execCommand("*IDN?");
  return deviceId;
}

std::string
tcds::hwlayerscpi::SCPIDeviceBase::readDeviceOptions() const
{
  return readDeviceOptionsImpl();
}

std::string
tcds::hwlayerscpi::SCPIDeviceBase::readDeviceOptionsImpl() const
{
  std::string const deviceOpts = execCommand("*OPT?");
  return deviceOpts;
}

uint32_t
tcds::hwlayerscpi::SCPIDeviceBase::readStatusByte() const
{
  return readStatusByteImpl();
}

std::string
tcds::hwlayerscpi::SCPIDeviceBase::readLatestError() const
{
  return readLatestErrorImpl();
}

tcds::definitions::REF_SOURCE
tcds::hwlayerscpi::SCPIDeviceBase::getReferenceSource() const
{
  tcds::definitions::REF_SOURCE res = tcds::definitions::REF_SOURCE_UNKNOWN;

  return res;
}

uint32_t
tcds::hwlayerscpi::SCPIDeviceBase::readStatusByteImpl() const
{
  std::string const statusRaw = execCommand("*STB?");
  uint32_t status;
  std::stringstream(statusRaw) >> status;
  return status;
}

std::string
tcds::hwlayerscpi::SCPIDeviceBase::readLatestErrorImpl() const
{
  std::string const res = execCommand("SYSTEM:ERROR?");
  return res;
}

std::string
tcds::hwlayerscpi::SCPIDeviceBase::execCommand(std::string const& cmd) const
{
  waitUntilReady();
  std::string const res = hwDevice_.execCommand(cmd);
  return res;
}

bool
tcds::hwlayerscpi::SCPIDeviceBase::isReady() const
{
  // NOTE: The following _has to be_ a direct call to
  // hwDevice_::execCommand(). Otherwise it turns into a recursion
  // from our own execCommand().
  std::string const tmp = hwDevice_.execCommand("*OPC?");
  bool const res = (tmp == "1");
  return res;
}

void
tcds::hwlayerscpi::SCPIDeviceBase::waitUntilReady() const
{
  // NOTE: This method typically never enters the loop. Software
  // interactions are so slow, compared to the hardware, that
  // operations tend to finish before we can check for them finishing.
  while (!isReady())
    {
      ::sleep(1);
    }
}

std::vector<std::string>
tcds::hwlayerscpi::SCPIDeviceBase::getRegisterNamesImpl() const
{
  std::vector<std::string> res = hwDevice_.getRegisterNames();
  return res;
}

tcds::hwlayer::RegisterInfo::RegInfoVec
tcds::hwlayerscpi::SCPIDeviceBase::getRegisterInfosImpl() const
{
  tcds::hwlayer::RegisterInfo::RegInfoVec res = hwDevice_.getRegisterInfos();
  return res;
}

uint32_t
tcds::hwlayerscpi::SCPIDeviceBase::readRegisterImpl(std::string const& regName) const
{
  return hwDevice_.readRegister(regName);
}

void
tcds::hwlayerscpi::SCPIDeviceBase::writeRegisterImpl(std::string const& regName,
                                                   uint32_t const regVal) const
{
  hwDevice_.writeRegister(regName, regVal);
}

std::vector<uint32_t>
tcds::hwlayerscpi::SCPIDeviceBase::readBlockImpl(std::string const& regName,
                                               uint32_t const nWords) const
{
  std::vector<uint32_t> const res = hwDevice_.readBlock(regName, nWords);
  return res;
}

std::vector<uint32_t>
tcds::hwlayerscpi::SCPIDeviceBase::readBlockOffsetImpl(std::string const& regName,
                                                     uint32_t const nWords,
                                                     uint32_t const offset) const
{
  std::vector<uint32_t> const res = hwDevice_.readBlockOffset(regName, nWords, offset);
  return res;
}

void
tcds::hwlayerscpi::SCPIDeviceBase::writeBlockImpl(std::string const& regName,
                                                std::vector<uint32_t> const& regVals) const
{
  hwDevice_.writeBlock(regName, regVals);
}

uint32_t
tcds::hwlayerscpi::SCPIDeviceBase::getBlockSizeImpl(std::string const& regName) const
{
  return hwDevice_.getBlockSize(regName);
}

void
tcds::hwlayerscpi::SCPIDeviceBase::writeHardwareConfigurationImpl(tcds::hwlayer::ConfigurationProcessor::RegValVec const& cfg) const
{
  // BUG BUG BUG
  // Lacks implementation.
  // BUG BUG BUG end

//   for (tcds::hwlayer::ConfigurationProcessor::RegValVec::const_iterator it = cfg.begin();
//        it != cfg.end();
//        ++it)
//     {
//       // NOTE: Some care is required here. For single-word writes the
//       // appropriate mask (specified in the address table) is
//       // applied. Block writes don't know about masks.
//       if (it->second.size() == 1)
//         {
//           writeRegister(it->first, it->second.at(0));
//         }
//       else
//         {
//           writeBlock(it->first, it->second);
//         }
//     }
}

tcds::hwlayer::DeviceBase::RegContentsVec
tcds::hwlayerscpi::SCPIDeviceBase::readHardwareConfigurationImpl(tcds::hwlayer::RegisterInfo::RegInfoVec const& regInfos) const
{
  // BUG BUG BUG
  // Lacks implementation.
  // BUG BUG BUG end

  tcds::hwlayer::DeviceBase::RegContentsVec res;
//   for (tcds::hwlayer::RegisterInfo::RegInfoVec::const_iterator regInfo = regInfos.begin();
//        regInfo != regInfos.end();
//        ++regInfo)
//     {
//       std::vector<uint32_t> regVals;
//       // NOTE: Some care is required here. For single-word reads the
//       // appropriate mask (specified in the address table) is
//       // applied. Block reads don't know about masks.
//       uint32_t const size = getBlockSize(regInfo->name());
//       if (size == 1)
//         {
//           regVals.push_back(readRegister(regInfo->name()));
//         }
//       else
//         {
//           regVals = readBlock(regInfo->name());
//         }

//       res.push_back(std::make_pair(*regInfo, regVals));
//     }

  return res;
}
