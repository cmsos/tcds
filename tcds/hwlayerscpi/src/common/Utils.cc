#include "tcds/hwlayerscpi/Utils.h"

#include <sstream>

#include "toolbox/string.h"
#include "xcept/Exception.h"

#include "tcds/exception/Exception.h"

std::string
tcds::hwlayerscpi::statusByteToString(uint32_t const value)
{
  std::stringstream tmp;
  tmp << "0x" << std::hex << value;
  // Bit 2 seems to indicate a generic 'there are errors in the error
  // queue' condition.
  if (value & 0x4)
    {
      tmp << " --> error(s) detected";
    }
  return tmp.str();
}

std::string
tcds::hwlayerscpi::refClkSrcToString(tcds::definitions::REF_SOURCE const src)
{
  std::string res = "";

  switch (src)
    {
    case tcds::definitions::REF_SOURCE_UNKNOWN:
      res = "unknown/none";
      break;
    case tcds::definitions::REF_SOURCE_INTERNAL:
      res = "internal";
      break;
    case tcds::definitions::REF_SOURCE_EXTERNAL_1MHZ:
      res = "external (1 MHz)";
      break;
    case tcds::definitions::REF_SOURCE_EXTERNAL_5MHZ:
      res = "external (5 MHz)";
      break;
    case tcds::definitions::REF_SOURCE_EXTERNAL_10MHZ:
      res = "external (10 MHz)";
      break;
    default:
      XCEPT_RAISE(tcds::exception::ValueError,
                  toolbox::toString("'%d' is not a valid reference clock source value.",
                                    src));
      break;
    }
  return res;
}
