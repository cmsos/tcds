#include "tcds/hwlayerscpi/version.h"

#include "config/version.h"
#include "toolbox/version.h"
#include "xcept/version.h"

#include "tcds/exception/version.h"
#include "tcds/hwlayer/version.h"
#include "tcds/utils/version.h"

GETPACKAGEINFO(tcds::hwlayerscpi)

void
tcds::hwlayerscpi::checkPackageDependencies()
{
  CHECKDEPENDENCY(config);
  CHECKDEPENDENCY(toolbox);
  CHECKDEPENDENCY(xcept);

  CHECKDEPENDENCY(tcds::exception);
  CHECKDEPENDENCY(tcds::hwlayer);
  CHECKDEPENDENCY(tcds::utils);
}

std::set<std::string, std::less<std::string> >
tcds::hwlayerscpi::getPackageDependencies()
{
  std::set<std::string, std::less<std::string> > dependencies;

  ADDDEPENDENCY(dependencies, config);
  ADDDEPENDENCY(dependencies, toolbox);
  ADDDEPENDENCY(dependencies, xcept);

  ADDDEPENDENCY(dependencies, tcds::exception);
  ADDDEPENDENCY(dependencies, tcds::hwlayer);
  ADDDEPENDENCY(dependencies, tcds::utils);

  return dependencies;
}
