#ifndef _tcds_hwlayerscpi_Utils_h_
#define _tcds_hwlayerscpi_Utils_h_

#include <cstdint>
#include <string>

#include "tcds/hwlayerscpi/Definitions.h"

namespace tcds {
  namespace hwlayerscpi {

    // Formatting of some generic information contained in the *STB?
    // output.
    std::string statusByteToString(uint32_t const value);

    // Mapping of reference clock source enums to strings.
    std::string refClkSrcToString(tcds::definitions::REF_SOURCE const src);

  } // namespace hwlayerscpi
} // namespace tcds

#endif // _tcds_hwlayerscpi_Utils_h_
