#ifndef _tcds_hwlayerscpi_SCPIDeviceBase_h_
#define _tcds_hwlayerscpi_SCPIDeviceBase_h_

#include <stdint.h>
#include <string>
#include <vector>

#include "tcds/hwlayer/ConfigurationProcessor.h"
#include "tcds/hwlayer/DeviceBase.h"
#include "tcds/hwlayer/IHwDevice.h"
#include "tcds/hwlayer/RegisterInfo.h"
#include "tcds/hwlayerscpi/Definitions.h"
#include "tcds/hwlayerscpi/HwDeviceSCPI.h"

namespace tcds {
  namespace hwlayerscpi {

    /**
     * Abstract base class for all TCP/socket-based SCPI
     * devices. (I.e., 'normal lab instruments'.)
     */
    class SCPIDeviceBase : public tcds::hwlayer::DeviceBase
    {

    public:
      virtual ~SCPIDeviceBase();

      /**
       * Connect to the hardware. Does whatever is necessary to find
       * the hardware and setup the connection.
       */
      void hwConnect(std::string const& hostName,
                     uint32_t const portNumber);

      /**
       * The inverse of hwConnect. Disconnects from the hardware,
       * releasing it for use by someone/something else.
       */
      void hwRelease();

      bool isHwConnected() const;

      void reset() const;
      void clearStatus() const;
      std::string readDeviceID() const;
      std::string readDeviceOptions() const;
      uint32_t readStatusByte() const;
      std::string readLatestError() const;

      virtual tcds::definitions::REF_SOURCE getReferenceSource() const;

      std::string execCommand(std::string const& cmd) const;
      bool isReady() const;
      void waitUntilReady() const;

    protected:
      /**
       * @note
       * Protected constructor since this is an abstract base class.
       */
      SCPIDeviceBase();

      virtual bool isReadyForUseImpl() const;

      virtual void hwConnectImpl(std::string const& hostName,
                                 uint32_t const portNumber);
      virtual void hwReleaseImpl();

      virtual void resetImpl() const;
      virtual void clearStatusImpl() const;
      virtual std::string readDeviceIDImpl() const;
      virtual std::string readDeviceOptionsImpl() const;
      virtual uint32_t readStatusByteImpl() const;
      virtual std::string readLatestErrorImpl() const;

      virtual std::vector<std::string> getRegisterNamesImpl() const;
      virtual tcds::hwlayer::RegisterInfo::RegInfoVec getRegisterInfosImpl() const;

      virtual uint32_t readRegisterImpl(std::string const& regName) const;
      virtual void writeRegisterImpl(std::string const& regName,
                                     uint32_t const regVal) const;
      virtual std::vector<uint32_t> readBlockImpl(std::string const& regName,
                                                  uint32_t const nWords) const;
      virtual std::vector<uint32_t> readBlockOffsetImpl(std::string const& regName,
                                                        uint32_t const nWords,
                                                        uint32_t const offset) const;
      virtual void writeBlockImpl(std::string const& regName,
                                  std::vector<uint32_t> const& regVals) const;

      virtual uint32_t getBlockSizeImpl(std::string const& regName) const;

      virtual void writeHardwareConfigurationImpl(tcds::hwlayer::ConfigurationProcessor::RegValVec const& cfg) const;
      virtual tcds::hwlayer::DeviceBase::RegContentsVec readHardwareConfigurationImpl(tcds::hwlayer::RegisterInfo::RegInfoVec const& regInfos) const;

      virtual tcds::hwlayer::RegDumpVec dumpRegisterContentsImpl() const;

      HwDeviceSCPI hwDevice_;

    };

  } // namespace hwlayerscpi
} // namespace tcds

#endif // _tcds_hwlayerscpi_SCPIDeviceBase_h_
