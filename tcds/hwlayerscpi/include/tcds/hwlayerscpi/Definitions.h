#ifndef _tcds_hwlayerscpi_Definitions_h_
#define _tcds_hwlayerscpi_Definitions_h_

namespace tcds {
  namespace definitions {

    enum REF_SOURCE {REF_SOURCE_UNKNOWN=0,
                     REF_SOURCE_INTERNAL=1,
                     REF_SOURCE_EXTERNAL_1MHZ=2,
                     REF_SOURCE_EXTERNAL_5MHZ=3,
                     REF_SOURCE_EXTERNAL_10MHZ=4};

  } // namespace definitions
} // namespace tcds

#endif // _tcds_hwlayerscpi_Definitions_h_
