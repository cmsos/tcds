#ifndef _tcds_hwlayerscpi_HwDeviceSCPI_h_
#define _tcds_hwlayerscpi_HwDeviceSCPI_h_

#include <stdint.h>
#include <string>
#include <vector>

#include "tcds/hwlayer/IHwDevice.h"
#include "tcds/hwlayer/RegisterInfo.h"

namespace tcds {
  namespace hwlayerscpi {

    /**
     * Hardware access class for TCP/socket-based SCPI devices. (I.e.,
     * 'normal lab instruments'.)
     */
    class HwDeviceSCPI : public tcds::hwlayer::IHwDevice
    {

    public:
      HwDeviceSCPI();
      virtual ~HwDeviceSCPI();

      void hwConnect(std::string const& hostName,
                     uint32_t const portNumber);
      void hwRelease();

      bool isHwConnected() const;

      uint32_t readRegister(std::string const& regName) const;
      void writeRegister(std::string const& regName,
                         uint32_t const regVal) const;
      std::vector<uint32_t> readBlock(std::string const& regName,
                                      uint32_t const nWords) const;
      std::vector<uint32_t> readBlockOffset(std::string const& regName,
                                            uint32_t const nWords,
                                            uint32_t const offset) const;
      void writeBlock(std::string const& regName,
                      std::vector<uint32_t> const regVal) const;
      uint32_t getBlockSize(std::string const& regName) const;

      std::vector<std::string> getRegisterNames() const;
      tcds::hwlayer::RegisterInfo::RegInfoVec getRegisterInfos() const;

      virtual tcds::hwlayer::RegDumpVec dumpRegisterContents() const;

      std::string execCommand(std::string const& cmd) const;
      void sendCommand(std::string const& cmd, bool const appendOPC=false) const;
      std::string readRawData() const;

    private:
      int mutable socket_;

    };

  } // namespace hwlayerscpi
} // namespace tcds

#endif // _tcds_hwlayerscpi_HwDeviceSCPI_h_
