#ifndef _tcds_apve_WebTableStatusHist_h
#define _tcds_apve_WebTableStatusHist_h

#include <cstddef>
#include <string>

#include "tcds/utils/WebObject.h"

namespace tcds {
  namespace utils {
    class Monitor;
  }
}

namespace tcds {
  namespace apve {

    class WebTableStatusHist : public tcds::utils::WebObject
    {

    public:
      WebTableStatusHist(std::string const& name,
                         std::string const& description,
                         tcds::utils::Monitor const& monitor,
                         std::string const& itemSetName,
                         std::string const& tabName,
                         size_t const colSpan);

      std::string getHTMLString() const;

    };

  } // namespace apve
} // namespace tcds

#endif // _tcds_apve_WebTableStatusHist_h
