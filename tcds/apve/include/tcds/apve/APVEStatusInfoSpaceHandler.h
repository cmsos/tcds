#ifndef _tcds_apve_APVEStatusInfoSpaceHandler_h_
#define _tcds_apve_APVEStatusInfoSpaceHandler_h_

#include <string>

#include "tcds/utils/InfoSpaceHandler.h"

namespace xdaq {
  class Application;
}

namespace tcds {
  namespace utils {
    class InfoSpaceUpdater;
    class Monitor;
    class WebServer;
  }
}

namespace tcds {
  namespace apve {

    class APVEStatusInfoSpaceHandler : public tcds::utils::InfoSpaceHandler
    {

    public:
      APVEStatusInfoSpaceHandler(xdaq::Application& xdaqApp,
                                 tcds::utils::InfoSpaceUpdater* updater);

    protected:
      virtual void registerItemSetsWithMonitor(tcds::utils::Monitor& monitor);
      virtual void registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                 tcds::utils::Monitor& monitor,
                                                 std::string const& forceTabName="");

    };

  } // namespace apve
} // namespace tcds

#endif // _tcds_apve_APVEStatusInfoSpaceHandler_h_
