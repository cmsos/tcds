#ifndef _tcds_apve_APVHistContents_h_
#define _tcds_apve_APVHistContents_h_

#include <stdint.h>
#include <string>
#include <vector>

#include "tcds/apve/APVHistEntry.h"

namespace tcds {
  namespace apve {

    class APVHistContents
    {

    public:
      // Constructor that takes a block of memory read from the APVE.
      APVHistContents(std::vector<uint32_t> const dataIn);

      std::string getJSONString() const;

      static int addressDifference(uint8_t const address0, uint8_t const address1);

    private:
      // This constructor is not supposed to be used.
      APVHistContents();

      /* std::string formatTimestamp(uint64_t const timestamp) const; */

      // These are the raw entries from the hardware.
      std::vector<APVHistEntry> entries_;

    };

  } // namespace apve
} // namespace tcds

#endif // _tcds_apve_APVHistContents_h_
