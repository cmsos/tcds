#ifndef _tcds_apve_Definitions_h_
#define _tcds_apve_Definitions_h_

#include <stdint.h>

namespace tcds {
  namespace definitions {

    // The trigger-veto mask is made up 0f 280 bits, spread across 17
    // 16-bit words and one 8-bit word.
    size_t const kNumWords = 18;
    size_t const kNumFullWords = kNumWords - 1;

    // The number of entries in the APV pipeline.
    size_t const kAPVPipelineLength = 192;
    /* uint8_t const kAPVPipelineAddressLo = 0; */
    /* uint8_t const kAPVPipelineAddressHi = (kAPVPipelineLength - 1); */

    enum APVE_READOUT_MODE {
      APVE_READOUT_MODE_DECONVOLUTION=0,
      APVE_READOUT_MODE_PEAK=1
    };

    enum APVE_TRIGGER_MODE {
      APVE_TRIGGER_MODE_DECONVOLUTION=0,
      APVE_TRIGGER_MODE_PEAK_SINGLE=1
    };

  } // namespace definitions
} // namespace tcds

#endif // _tcds_apve_Definitions_h_
