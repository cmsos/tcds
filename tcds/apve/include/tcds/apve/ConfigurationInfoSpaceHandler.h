#ifndef _tcds_apve_ConfigurationInfoSpaceHandler_h_
#define _tcds_apve_ConfigurationInfoSpaceHandler_h_

#include "tcds/hwutilstca/ConfigurationInfoSpaceHandlerTCA.h"

namespace xdaq {
  class Application;
}

namespace tcds {
  namespace utils {
    class Monitor;
  }
}

namespace tcds {
  namespace apve {

    class ConfigurationInfoSpaceHandler :
      public tcds::hwutilstca::ConfigurationInfoSpaceHandlerTCA
    {

    public:
      ConfigurationInfoSpaceHandler(xdaq::Application& xdaqApp);

    private:
      virtual void registerItemSetsWithMonitor(tcds::utils::Monitor& monitor);

    };

  } // namespace apve
} // namespace tcds

#endif // _tcds_apve_ConfigurationInfoSpaceHandler_h_
