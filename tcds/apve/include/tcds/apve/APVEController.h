#ifndef _tcds_apve_APVEController_h_
#define _tcds_apve_APVEController_h_

#include <memory>

#include "toolbox/Event.h"
#include "xdaq/Application.h"

#include "tcds/apve/TCADeviceAPVE.h"
#include "tcds/utils/RegCheckResult.h"
#include "tcds/utils/SOAPCmdBase.h"
#include "tcds/utils/SOAPCmdDumpHardwareState.h"
#include "tcds/utils/SOAPCmdReadHardwareConfiguration.h"
#include "tcds/utils/XDAQAppWithFSMBasic.h"

namespace xdaq {
  class ApplicationStub;
}

namespace tcds {
  namespace hwlayer {
    class RegisterInfo;
  }
}

namespace tcds {
  namespace hwutilstca {
    class HwIDInfoSpaceHandlerTCA;
    class HwIDInfoSpaceUpdaterTCA;
  }
}

namespace tcds {
  namespace apve {

    class APVEInfoSpaceHandler;
    class APVEInfoSpaceUpdater;
    class APVECountersInfoSpaceHandler;
    class APVECountersInfoSpaceUpdater;
    class APVESimHistInfoSpaceHandler;
    class APVESimHistInfoSpaceUpdater;
    class APVEStatusInfoSpaceHandler;
    class APVEStatusInfoSpaceUpdater;
    class APVEStatusHistInfoSpaceHandler;
    class APVEStatusHistInfoSpaceUpdater;
    class APVETriggerMaskInfoSpaceHandler;
    class APVETriggerMaskInfoSpaceUpdater;
    class HwStatusInfoSpaceHandler;
    class HwStatusInfoSpaceUpdater;

    class APVEController : public tcds::utils::XDAQAppWithFSMBasic
    {

    public:
      XDAQ_INSTANTIATOR();

      APVEController(xdaq::ApplicationStub* stub);
      virtual ~APVEController();

      unsigned short apveNumber() const;

    protected:
      virtual void setupInfoSpaces();

      /**
       * Access the hardware pointer as TCADeviceAPVE&.
       */
      virtual TCADeviceAPVE& getHw() const;

      virtual void configureActionImpl(toolbox::Event::Reference event);
      virtual void zeroActionImpl(toolbox::Event::Reference event);

      virtual tcds::utils::RegCheckResult isRegisterAllowed(tcds::hwlayer::RegisterInfo const& regInfo) const;

      virtual void hwConnectImpl();
      virtual void hwReleaseImpl();

      virtual void hwCfgInitializeImpl();
      virtual void hwCfgFinalizeImpl();

    private:
      // Various InfoSpaces and their InfoSpaceUpdaters.
      std::unique_ptr<APVEInfoSpaceUpdater> apveInfoSpaceUpdaterP_;
      std::unique_ptr<APVEInfoSpaceHandler> apveInfoSpaceP_;
      std::unique_ptr<APVECountersInfoSpaceUpdater> apveCountersInfoSpaceUpdaterP_;
      std::unique_ptr<APVECountersInfoSpaceHandler> apveCountersInfoSpaceP_;
      std::unique_ptr<APVESimHistInfoSpaceUpdater> apveSimHistInfoSpaceUpdaterP_;
      std::unique_ptr<APVESimHistInfoSpaceHandler> apveSimHistInfoSpaceP_;
      std::unique_ptr<APVEStatusInfoSpaceUpdater> apveStatusInfoSpaceUpdaterP_;
      std::unique_ptr<APVEStatusInfoSpaceHandler> apveStatusInfoSpaceP_;
      std::unique_ptr<APVEStatusHistInfoSpaceUpdater> apveStatusHistInfoSpaceUpdaterP_;
      std::unique_ptr<APVEStatusHistInfoSpaceHandler> apveStatusHistInfoSpaceP_;
      std::unique_ptr<APVETriggerMaskInfoSpaceUpdater> apveTriggerMaskInfoSpaceUpdaterP_;
      std::unique_ptr<APVETriggerMaskInfoSpaceHandler> apveTriggerMaskInfoSpaceP_;
      std::unique_ptr<tcds::hwutilstca::HwIDInfoSpaceUpdaterTCA> hwIDInfoSpaceUpdaterP_;
      std::unique_ptr<tcds::hwutilstca::HwIDInfoSpaceHandlerTCA> hwIDInfoSpaceP_;
      std::unique_ptr<tcds::apve::HwStatusInfoSpaceUpdater> hwStatusInfoSpaceUpdaterP_;
      std::unique_ptr<tcds::apve::HwStatusInfoSpaceHandler> hwStatusInfoSpaceP_;

      // The SOAP commands.
      template<typename> friend class tcds::utils::SOAPCmdBase;
      template<typename> friend class tcds::utils::SOAPCmdDumpHardwareState;
      template<typename> friend class tcds::utils::SOAPCmdReadHardwareConfiguration;
      tcds::utils::SOAPCmdDumpHardwareState<APVEController> soapCmdDumpHardwareState_;
      tcds::utils::SOAPCmdReadHardwareConfiguration<APVEController> soapCmdReadHardwareConfiguration_;

    };

  } // namespace apve
} // namespace tcds

#endif // _tcds_apve_APVEController_h_
