#ifndef _tcds_apve_HwStatusInfoSpaceUpdater_h_
#define _tcds_apve_HwStatusInfoSpaceUpdater_h_

#include "tcds/hwutilstca/HwStatusInfoSpaceUpdaterTCA.h"

namespace tcds {
  namespace utils {
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace apve {

    class TCADeviceAPVE;

    class HwStatusInfoSpaceUpdater : public tcds::hwutilstca::HwStatusInfoSpaceUpdaterTCA
    {

    public:
      HwStatusInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                               tcds::apve::TCADeviceAPVE const& hw);
      virtual ~HwStatusInfoSpaceUpdater();

    private:
      tcds::apve::TCADeviceAPVE const& getHw() const;

      tcds::apve::TCADeviceAPVE const& hw_;

    };

  } // namespace apve
} // namespace tcds

#endif // _tcds_apve_HwStatusInfoSpaceUpdater_h_
