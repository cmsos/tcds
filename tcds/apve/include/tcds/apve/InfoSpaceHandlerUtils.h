#ifndef _tcds_apve_InfoSpaceHandlerUtils_h_
#define _tcds_apve_InfoSpaceHandlerUtils_h_

#include <stddef.h>
#include <string>

namespace tcds {
  namespace utils {
    class InfoSpaceHandler;
    class Monitor;
    class WebServer;
  }
}

namespace tcds {
  namespace apve {

    // NOTE: This approach is not very pretty, but it allows sharing
    // of the code between all the different APVEController
    // InfoSpaces, and the CPMController/LPMController single APVE
    // InfoSpaces.

    //----------

    // APVEInfoSpaceHandler.
    void createItemsAPVE(tcds::utils::InfoSpaceHandler* const infoSpaceHandler,
                         std::string const& prefix="");
    void registerItemSetsWithMonitorAPVE(tcds::utils::Monitor& monitor,
                                         tcds::utils::InfoSpaceHandler* const infoSpaceHandler,
                                         std::string const& prefix="");
    void registerItemSetsWithWebServerAPVE(tcds::utils::WebServer& webServer,
                                           tcds::utils::Monitor& monitor,
                                           std::string const& tabName);

    //----------

    // APVETriggerMaskInfoSpaceHandler.
    void createItemsAPVETriggerMask(tcds::utils::InfoSpaceHandler* const infoSpaceHandler,
                                    std::string const& prefix="");
    void registerItemSetsWithMonitorAPVETriggerMask(tcds::utils::Monitor& monitor,
                                                    tcds::utils::InfoSpaceHandler* const infoSpaceHandler,
                                                    std::string const& prefix="");
    void registerItemSetsWithWebServerAPVETriggerMask(tcds::utils::WebServer& webServer,
                                                      tcds::utils::Monitor& monitor,
                                                      std::string const& tabName);
    //----------

    // APVESimHistInfoSpaceHandler.
    void createItemsAPVESimHist(tcds::utils::InfoSpaceHandler* const infoSpaceHandler,
                                std::string const& prefix="");
    void registerItemSetsWithMonitorAPVESimHist(tcds::utils::Monitor& monitor,
                                                tcds::utils::InfoSpaceHandler* const infoSpaceHandler,
                                                std::string const& prefix="");
    void registerItemSetsWithWebServerAPVESimHist(tcds::utils::WebServer& webServer,
                                                  tcds::utils::Monitor& monitor,
                                                  std::string const& tabName,
                                                  size_t const colSpan);

    //----------

    // APVEStatusInfoSpaceHandler.
    void createItemsAPVEStatus(tcds::utils::InfoSpaceHandler* const infoSpaceHandler,
                               std::string const& prefix="");
    void registerItemSetsWithMonitorAPVEStatus(tcds::utils::Monitor& monitor,
                                               tcds::utils::InfoSpaceHandler* const infoSpaceHandler,
                                               std::string const& prefix="");
    void registerItemSetsWithWebServerAPVEStatus(tcds::utils::WebServer& webServer,
                                                 tcds::utils::Monitor& monitor,
                                                 std::string const& tabName);

    //----------

    // APVECountersInfoSpaceHandler.
    void createItemsAPVECounters(tcds::utils::InfoSpaceHandler* const infoSpaceHandler,
                                 std::string const& prefix="");
    void registerItemSetsWithMonitorAPVECounters(tcds::utils::Monitor& monitor,
                                                 tcds::utils::InfoSpaceHandler* const infoSpaceHandler,
                                                 std::string const& prefix="");
    void registerItemSetsWithWebServerAPVECounters(tcds::utils::WebServer& webServer,
                                                   tcds::utils::Monitor& monitor,
                                                   std::string const& tabName);

    //----------

  } // namespace apve
} // namespace tcds

#endif // _tcds_apve_InfoSpaceHandlerUtils_h_
