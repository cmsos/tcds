#ifndef _tcds_apve_Utils_h_
#define _tcds_apve_Utils_h_

#include <stdint.h>
#include <string>

#include "tcds/apve/Definitions.h"

namespace tcds {
  namespace apve {

    // Mapping of APVE readout/trigger mode enums to strings.
    std::string APVEReadoutModeToString(tcds::definitions::APVE_READOUT_MODE const modeIn);
    std::string APVETriggerModeToString(tcds::definitions::APVE_TRIGGER_MODE const modeIn);

    // Convert pipeline addresses to Gray codes.
    uint8_t pipelineAddressToGrayCode(uint8_t const address);

  } // namespace apve
} // namespace tcds

#endif // _tcds_apve_Utils_h_
