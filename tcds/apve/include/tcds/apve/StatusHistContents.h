#ifndef _tcds_apve_StatusHistContents_h_
#define _tcds_apve_StatusHistContents_h_

#include <stdint.h>
#include <string>
#include <vector>

#include "tcds/apve/StatusHistEntry.h"

namespace tcds {
  namespace apve {

    class StatusHistContents
    {

    public:
      // Constructor that takes a block of memory read from the APVE.
      StatusHistContents(std::vector<uint32_t> const dataIn);

      std::string getJSONString() const;

    private:
      // This constructor is not supposed to be used.
      StatusHistContents();

      // These are the raw entries from the hardware.
      std::vector<StatusHistEntry> entries_;

    };

  } // namespace apve
} // namespace tcds

#endif // _tcds_apve_StatusHistContents_h_
