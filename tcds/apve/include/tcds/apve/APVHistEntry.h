#ifndef _tcds_apve_APVHistEntry_h_
#define _tcds_apve_APVHistEntry_h_

#include <stdint.h>

namespace tcds {
  namespace apve {

    class APVHistEntry
    {

    public:

      // The APV pipeline history is organized in entries of 32-bits,
      // mapped as follows:
      // - bits [7:0] -> APV pipeline address,
      // - bits [31:8] -> event number bits.

      APVHistEntry(uint32_t const dataIn);

      uint32_t eventNumber() const;
      uint8_t pipelineAddress() const;

    private:
      // This constructor is not supposed to be used.
      APVHistEntry();

      uint32_t rawData_;
      uint8_t pipelineAddress_;
      uint32_t eventNumber_;

    };

  } // namespace apve
} // namespace tcds

#endif // _tcds_apve_APVHistEntry_h_
