#ifndef _tcds_apve_TCADeviceAPVE_h_
#define _tcds_apve_TCADeviceAPVE_h_

#include <stdint.h>
#include <string>
#include <vector>

#include "tcds/hwlayertca/TCADeviceBase.h"

namespace tcds {
  namespace apve {

    /**
     * Implementation of the internal APVE functionality. The APVE is
     * implemented as a firmware block inside the LPM (which is based
     * on an FC7 carrier board).
     */
    class TCADeviceAPVE : public tcds::hwlayertca::TCADeviceBase
    {

    public:

      /**
       * @note
       * The TCADeviceAPVE need an apve number. The TCADeviceAPVE
       * connects to the physical LPM hardware, but only acts upon the
       * apve (firmware) instance with the given number. The caveat is
       * that at the time the TCADeviceAPVE object is instantiated in
       * the APVEController, the XDAQ configuration has not yet been
       * loaded, so the apve number is not yet known. Hence the (ugly)
       * setAPVENumber() method.
       */
      TCADeviceAPVE();
      virtual ~TCADeviceAPVE();

      unsigned short apveNumber() const;
      void setAPVENumber(unsigned short const apveNumber);

      /**
       * Enable the parallel-load buffer register for the trigger-veto
       * shift register.
       */
      void enableTriggerVetoMaskLoadBuffer() const;

      /**
       * Readout the simulated pipeline history.
       */
      std::vector<uint32_t> readSimPipelineHistory() const;

      /**
       * Readout the TTS/status history.
       */
      std::vector<uint32_t> readStatusHistory() const;

      /**
       * Control over the simulated pipeline history.
       */
      void lockSimPipelineHistory() const;
      void unlockSimPipelineHistory() const;

      /**
       * Control over the TTS/status history.
       */
      void lockStatusHistory() const;
      void unlockStatusHistory() const;

      /**
       * Control over the statistics counters.
       */
      void latchStatisticsCounters() const;
      void resetStatisticsCounters() const;

    protected:
      virtual std::string regNamePrefixImpl() const;

      virtual bool bootstrapDoneImpl() const;
      virtual void runBootstrapImpl() const;

    private:
      unsigned short apveNumber_;

    };

  } // namespace apve
} // namespace tcds

#endif // _tcds_apve_TCADeviceAPVE_h_
