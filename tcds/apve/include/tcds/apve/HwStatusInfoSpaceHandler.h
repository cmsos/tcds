#ifndef _tcds_apve_HwStatusInfoSpaceHandler_h_
#define _tcds_apve_HwStatusInfoSpaceHandler_h_

#include "tcds/hwutilstca/HwStatusInfoSpaceHandlerTCA.h"

namespace tcds {
  namespace utils {
    class InfoSpaceUpdater;
  }
}

namespace xdaq {
  class Application;
}

namespace tcds {
  namespace apve {

    class HwStatusInfoSpaceHandler : public tcds::hwutilstca::HwStatusInfoSpaceHandlerTCA
    {

    public:
      HwStatusInfoSpaceHandler(xdaq::Application& xdaqApp,
                               tcds::utils::InfoSpaceUpdater* updater);
      virtual ~HwStatusInfoSpaceHandler();

    };

  } // namespace apve
} // namespace tcds

#endif // _tcds_apve_HwStatusInfoSpaceHandler_h_
