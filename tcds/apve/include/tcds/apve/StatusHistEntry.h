#ifndef _tcds_apve_StatusHistEntry_h_
#define _tcds_apve_StatusHistEntry_h_

#include <stddef.h>
#include <stdint.h>
#include <string>
#include <vector>

namespace tcds {
  namespace apve {

    class StatusHistEntry
    {

    public:

      // The APVE status history is organized in entries of
      // 128-bits. In terms of 16-bit VME words, the data mapping is
      // as follows:
      // - word 0:
      //   - bits [15:0] -> Sources of 'ERROR' due to the APV.
      // - word 1:
      //   - bits [15:0] -> Sources of 'ERROR' due to the FMM.
      // - word 2:
      //   - bits [15:0] -> Sources of 'OOS' due to the APV.
      // - word 3:
      //   - bits [15:0] -> Sources of 'OOS' due to the FMM.
      // - word 4:
      //   - bits [3:0] -> Output status sent to the LTCS.
      //   - bits [7:4] -> Output status sent to the GTCS.
      //   - bits [11:8] -> FMM status.
      //   - bits [15:12] -> APV status.
      // - word 5:
      //   - bit 1: L1 reset flag.
      //   - bit 2: history-stop flag.
      //   - bit 3: history-start flag.
      //   - bits [15:4] -> Bunch-crossing counter.
      // - word 6:
      //   - bits [15:0] -> Orbit counter bits [15:0].
      // - word 7:
      //   - bits [15:0] -> Orbit counter bits [31:16].

      // NOTE: The number of words per entry is based on the 16-bit
      // definition of a (16-bit) VME word.
      static size_t const kNumWordsPerEntry = 8;

      StatusHistEntry(std::vector<uint32_t> const& dataIn);

      std::string rawData() const;

      bool isHistStart() const;
      bool isHistStop() const;

      uint16_t bxNumber() const;
      uint32_t orbitNumber() const;

      uint8_t fmmStatus() const;
      uint8_t apvStatus() const;
      uint8_t ttsState() const;

      std::string apvErrorReason() const;
      std::string apvOutOfSyncReason() const;

    private:
      // This constructor is not supposed to be used.
      StatusHistEntry();

      std::vector<uint32_t> rawData_;
      std::vector<uint16_t> data_;
      bool isHistStart_;
      bool isHistStop_;
      uint16_t bxNumber_;
      uint32_t orbitNumber_;
      uint8_t fmmStatus_;
      uint8_t apvStatus_;
      uint8_t ttsState_;
      uint16_t apvErrorReason_;
      uint16_t fmmErrorReason_;
      uint16_t apvOutOfSyncReason_;
      uint16_t fmmOutOfSyncReason_;

    };

  } // namespace apve
} // namespace tcds

#endif // _tcds_apve_StatusHistEntry_h_
