#include "tcds/apve/InfoSpaceHandlerUtils.h"

#include "toolbox/string.h"

#include "tcds/apve/Definitions.h"
#include "tcds/apve/WebTableSimHist.h"
#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/WebServer.h"

void
tcds::apve::createItemsAPVE(tcds::utils::InfoSpaceHandler* const infoSpaceHandler,
                            std::string const& prefix)
{
  // Overall APVE enable flag.
  infoSpaceHandler->createBool(prefix + "config.apv_enable", false, "enabled/disabled");

  // Emulation switch. Should be kept to 1 since in the TCDS
  // implementation there is no real APV.
  infoSpaceHandler->createBool(prefix + "config.apv_sim_select", true, "enabled/disabled");

  // APV latency.
  infoSpaceHandler->createUInt32(prefix + "config.apv_latency");

  // Readout and trigger modes.
  infoSpaceHandler->createUInt32(prefix + "config.apv_read_mode");
  infoSpaceHandler->createUInt32(prefix + "config.apv_trigger_mode");

  // WARNING/BUSY thresholds.
  infoSpaceHandler->createUInt32(prefix + "config.apv_threshold_busy");
  infoSpaceHandler->createUInt32(prefix + "config.apv_threshold_warning");

  // APSP/L1A offsets.
  infoSpaceHandler->createUInt32(prefix + "config.apsp_offset");
  infoSpaceHandler->createUInt32(prefix + "config.l1a_offset");

  // Pipeline offsets.
  infoSpaceHandler->createUInt32(prefix + "pipeline_offsets.l1a");
  infoSpaceHandler->createUInt32(prefix + "pipeline_offsets.resync");
  infoSpaceHandler->createUInt32(prefix + "pipeline_offsets.bc0");
  infoSpaceHandler->createUInt32(prefix + "pipeline_offsets.tcs_error");
  infoSpaceHandler->createUInt32(prefix + "pipeline_offsets.ec0");
  infoSpaceHandler->createUInt32(prefix + "pipeline_offsets.oc0");

  // Misc.
  infoSpaceHandler->createBool(prefix + "config.fmm_enable", false, "enabled/disabled");
  infoSpaceHandler->createBool(prefix + "debug_tts.force_tts_error", false, "enabled/disabled");
  infoSpaceHandler->createBool(prefix + "debug_tts.force_tts_busy", false, "enabled/disabled");
  infoSpaceHandler->createBool(prefix + "debug_tts.force_tts_warning", false, "enabled/disabled");
  infoSpaceHandler->createBool(prefix + "debug_tts.force_tts_oos", false, "enabled/disabled");
}

void
tcds::apve::registerItemSetsWithMonitorAPVE(tcds::utils::Monitor& monitor,
                                            tcds::utils::InfoSpaceHandler* const infoSpaceHandler,
                                            std::string const& prefix)
{
  // These are the important settings.
  monitor.newItemSet("itemset-settings");
  monitor.addItem("itemset-settings",
                  prefix + "config.apv_enable",
                  "Overall APVE enable",
                  infoSpaceHandler);
  monitor.addItem("itemset-settings",
                  prefix + "config.apv_sim_select",
                  "APV emulation mode",
                  infoSpaceHandler);
  monitor.addItem("itemset-settings",
                  prefix + "config.apv_latency",
                  "APV latency (BX)",
                  infoSpaceHandler);
  monitor.addItem("itemset-settings",
                  prefix + "config.apv_read_mode",
                  "APV readout mode",
                  infoSpaceHandler);
  monitor.addItem("itemset-settings",
                  prefix + "config.apv_trigger_mode",
                  "APV trigger mode",
                  infoSpaceHandler);
  monitor.addItem("itemset-settings",
                  prefix + "config.apv_threshold_busy",
                  "APV BUSY threshold",
                  infoSpaceHandler);
  monitor.addItem("itemset-settings",
                  prefix + "config.apv_threshold_warning",
                  "APV WARNING threshold",
                  infoSpaceHandler);
  monitor.addItem("itemset-settings",
                  prefix + "config.apsp_offset",
                  "APSP offset (BX)",
                  infoSpaceHandler);
  monitor.addItem("itemset-settings",
                  prefix + "config.l1a_offset",
                  "L1A offset (BX)",
                  infoSpaceHandler);

  // Pipeline offsets.
  monitor.newItemSet("itemset-offsets");
  monitor.addItem("itemset-offsets",
                  prefix + "pipeline_offsets.l1a",
                  "Pipeline offset for L1As (BX)",
                  infoSpaceHandler);
  monitor.addItem("itemset-offsets",
                  prefix + "pipeline_offsets.oc0",
                  "Pipeline offset for OC0 B-gos (BX)",
                  infoSpaceHandler);
  monitor.addItem("itemset-offsets",
                  prefix + "pipeline_offsets.ec0",
                  "Pipeline offset for EC0 B-gos (BX)",
                  infoSpaceHandler);
  monitor.addItem("itemset-offsets",
                  prefix + "pipeline_offsets.bc0",
                  "Pipeline offset for BC0 B-gos (BX)",
                  infoSpaceHandler);
  monitor.addItem("itemset-offsets",
                  prefix + "pipeline_offsets.resync",
                  "Pipeline offset for Resync B-gos (BX)",
                  infoSpaceHandler);
  monitor.addItem("itemset-offsets",
                  prefix + "pipeline_offsets.tcs_error",
                  "Pipeline offset for TCS errors (BX)",
                  infoSpaceHandler);

  // Misc.
  monitor.newItemSet("itemset-misc");
  monitor.addItem("itemset-misc",
                  prefix + "config.fmm_enable",
                  "FMM input (which does not exist)",
                  infoSpaceHandler);
  monitor.addItem("itemset-misc",
                  prefix + "debug_tts.force_tts_error",
                  "Simulated TCS ERROR",
                  infoSpaceHandler);
  monitor.addItem("itemset-misc",
                  prefix + "debug_tts.force_tts_busy",
                  "Simulated TCS BUSY",
                  infoSpaceHandler);
  monitor.addItem("itemset-misc",
                  prefix + "debug_tts.force_tts_warning",
                  "Simulated TCS WARNING",
                  infoSpaceHandler);
  monitor.addItem("itemset-misc",
                  prefix + "debug_tts.force_tts_oos",
                  "Simulated TCS OUT-OF-SYNC",
                  infoSpaceHandler);
}

void
tcds::apve::registerItemSetsWithWebServerAPVE(tcds::utils::WebServer& webServer,
                                              tcds::utils::Monitor& monitor,
                                              std::string const& tabName)
{
  webServer.registerTable("APV emulation",
                          "APV emulation settings",
                          monitor,
                          "itemset-settings",
                          tabName);
  webServer.registerTable("Pipeline offsets",
                          "APV emulation pipeline offsets",
                          monitor,
                          "itemset-offsets",
                          tabName);
  webServer.registerTable("Misc",
                          "Miscellaneous settings",
                          monitor,
                          "itemset-misc",
                          tabName);
}

void
tcds::apve::createItemsAPVETriggerMask(tcds::utils::InfoSpaceHandler* const infoSpaceHandler,
                                       std::string const& prefix)
{
  // All 17.5 16-bit words (i.e., 280 bits) of the trigger-veto mask
  for (size_t i = 0; i != tcds::definitions::kNumFullWords; ++i)
    {
      std::string const name = toolbox::toString("trigger_veto_mask.word%d", i);
      infoSpaceHandler->createUInt32(prefix + name, 0x0, "x4");
    }
  std::string const name = toolbox::toString("trigger_veto_mask.word%d", tcds::definitions::kNumFullWords);
  infoSpaceHandler->createUInt32(prefix + name, 0x0, "x2");
}

void
tcds::apve::registerItemSetsWithMonitorAPVETriggerMask(tcds::utils::Monitor& monitor,
                                                       tcds::utils::InfoSpaceHandler* const infoSpaceHandler,
                                                       std::string const& prefix)
{
  monitor.newItemSet("itemset-apve-veto-mask");
  for (size_t i = 0; i != tcds::definitions::kNumWords; ++i)
    {
      std::string const name = toolbox::toString("trigger_veto_mask.word%d", i);
      std::string const desc = toolbox::toString("Trigger-veto mask word %d", i);
      monitor.addItem("itemset-apve-veto-mask", prefix + name, desc, infoSpaceHandler);
    }
}

void
tcds::apve::registerItemSetsWithWebServerAPVETriggerMask(tcds::utils::WebServer& webServer,
                                                         tcds::utils::Monitor& monitor,
                                                         std::string const& tabName)
{
  webServer.registerTable("Veto mask",
                          "The trigger suppression mask",
                          monitor,
                          "itemset-apve-veto-mask",
                          tabName);
}

void
tcds::apve::createItemsAPVESimHist(tcds::utils::InfoSpaceHandler* const infoSpaceHandler,
                                   std::string const& prefix)
{
  infoSpaceHandler->createString(prefix + "apv_simulated_pipeline_history");
}

void
tcds::apve::registerItemSetsWithMonitorAPVESimHist(tcds::utils::Monitor& monitor,
                                                   tcds::utils::InfoSpaceHandler* const infoSpaceHandler,
                                                   std::string const& prefix)
{
  monitor.newItemSet("itemset-simhist");
  monitor.addItem("itemset-simhist",
                  prefix + "apv_simulated_pipeline_history",
                  "Simulated APV pipeline history",
                  infoSpaceHandler);
}

void
tcds::apve::registerItemSetsWithWebServerAPVESimHist(tcds::utils::WebServer& webServer,
                                                     tcds::utils::Monitor& monitor,
                                                     std::string const& tabName,
                                                     size_t const colSpan)
{
  webServer.registerWebObject<WebTableSimHist>("Pipeline history",
                                               "Simulated APV pipeline history. Note that this is cleared upon each Resync.",
                                               monitor,
                                               "itemset-simhist",
                                               tabName,
                                               colSpan);
}

void
tcds::apve::createItemsAPVEStatus(tcds::utils::InfoSpaceHandler* const infoSpaceHandler,
                                  std::string const& prefix)
{
  // APV Out-Of-Sync explanation.
  infoSpaceHandler->createBool(prefix + "diagnostics.apv_oos_reason.software",
                               false,
                               "true/false");
  infoSpaceHandler->createBool(prefix + "diagnostics.apv_oos_reason.error_latch",
                               false,
                               "true/false");
  infoSpaceHandler->createBool(prefix + "diagnostics.apv_oos_reason.bc0_sync_loss",
                               false,
                               "true/false");
  infoSpaceHandler->createBool(prefix + "diagnostics.apv_oos_reason.apv_i2c_configuration_change",
                               false,
                               "true/false");
  infoSpaceHandler->createBool(prefix + "diagnostics.apv_oos_reason.apv_configuration_change",
                               false,
                               "true/false");
  infoSpaceHandler->createBool(prefix + "diagnostics.apv_oos_reason.apv_buffer_overflow",
                               false,
                               "true/false");
  infoSpaceHandler->createBool(prefix + "diagnostics.apv_oos_reason.apv_reset_in_progress",
                               false,
                               "true/false");
  infoSpaceHandler->createBool(prefix + "diagnostics.apv_oos_reason.real_apv_error_bit",
                               false,
                               "true/false");
  infoSpaceHandler->createBool(prefix + "diagnostics.apv_oos_reason.real_apv_tick_error",
                               false,
                               "true/false");

  // APV Error explanation.
  infoSpaceHandler->createBool(prefix + "diagnostics.apv_error_reason.software",
                               false,
                               "true/false");
  infoSpaceHandler->createBool(prefix + "diagnostics.apv_error_reason.apv_latency",
                               false,
                               "true/false");
  infoSpaceHandler->createBool(prefix + "diagnostics.apv_error_reason.apv_busy_threshold_set_incorrectly",
                               false,
                               "true/false");
  infoSpaceHandler->createBool(prefix + "diagnostics.apv_error_reason.apv_error_threshold_set_incorrectly",
                               false,
                               "true/false");
  infoSpaceHandler->createBool(prefix + "diagnostics.apv_error_reason.no_tcs_source_enabled",
                               false,
                               "true/false");
  infoSpaceHandler->createBool(prefix + "diagnostics.apv_error_reason.tcs_control_error",
                               false,
                               "true/false");
  infoSpaceHandler->createBool(prefix + "diagnostics.apv_error_reason.tcs_clock_error",
                               false,
                               "true/false");
}

void
tcds::apve::registerItemSetsWithMonitorAPVEStatus(tcds::utils::Monitor& monitor,
                                                  tcds::utils::InfoSpaceHandler* const infoSpaceHandler,
                                                  std::string const& prefix)
{
  // APV Out-Of-Sync explanation.
  monitor.newItemSet("itemset-apve-status-oos");
  monitor.addItem("itemset-apve-status-oos",
                  prefix + "diagnostics.apv_oos_reason.software",
                  "Forced by software",
                  infoSpaceHandler);
  monitor.addItem("itemset-apve-status-oos",
                  prefix + "diagnostics.apv_oos_reason.error_latch",
                  "Error latch",
                  infoSpaceHandler);
  monitor.addItem("itemset-apve-status-oos",
                  prefix + "diagnostics.apv_oos_reason.bc0_sync_loss",
                  "BC0 synchronisation lost",
                  infoSpaceHandler);
  monitor.addItem("itemset-apve-status-oos",
                  prefix + "diagnostics.apv_oos_reason.apv_i2c_configuration_change",
                  "APV I2C configuration changed (should not occur in the TCDS)",
                  infoSpaceHandler);
  monitor.addItem("itemset-apve-status-oos",
                  prefix + "diagnostics.apv_oos_reason.apv_configuration_change",
                  "APV configuration changed",
                  infoSpaceHandler);
  monitor.addItem("itemset-apve-status-oos",
                  prefix + "diagnostics.apv_oos_reason.apv_buffer_overflow",
                  "APV buffer overflow",
                  infoSpaceHandler);
  monitor.addItem("itemset-apve-status-oos",
                  prefix + "diagnostics.apv_oos_reason.apv_reset_in_progress",
                  "APV reset in progress",
                  infoSpaceHandler);
  monitor.addItem("itemset-apve-status-oos",
                  prefix + "diagnostics.apv_oos_reason.real_apv_error_bit",
                  "Real APV in error (should not occur in the TCDS)",
                  infoSpaceHandler);
  monitor.addItem("itemset-apve-status-oos",
                  prefix + "diagnostics.apv_oos_reason.real_apv_tick_error",
                  "Real APV tick error detected (should not occur in the TCDS)",
                  infoSpaceHandler);

  // APV Error explanation.
  monitor.newItemSet("itemset-apve-status-error");
  monitor.addItem("itemset-apve-status-error",
                  prefix + "diagnostics.apv_error_reason.software",
                  "Forced by software",
                  infoSpaceHandler);
  monitor.addItem("itemset-apve-status-error",
                  prefix + "diagnostics.apv_error_reason.apv_latency",
                  "APV latency set to an incompatible value",
                  infoSpaceHandler);
  monitor.addItem("itemset-apve-status-error",
                  prefix + "diagnostics.apv_error_reason.apv_busy_threshold_set_incorrectly",
                  "APV Busy threshold set to an incompatible value",
                  infoSpaceHandler);
  monitor.addItem("itemset-apve-status-error",
                  prefix + "diagnostics.apv_error_reason.apv_error_threshold_set_incorrectly",
                  "APV Error threshold set to an incompatible value",
                  infoSpaceHandler);
  monitor.addItem("itemset-apve-status-error",
                  prefix + "diagnostics.apv_error_reason.no_tcs_source_enabled",
                  "No TCS source enabled",
                  infoSpaceHandler);
  monitor.addItem("itemset-apve-status-error",
                  prefix + "diagnostics.apv_error_reason.tcs_control_error",
                  "TCS control error",
                  infoSpaceHandler);
  monitor.addItem("itemset-apve-status-error",
                  prefix + "diagnostics.apv_error_reason.tcs_clock_error",
                  "TCS clock error",
                  infoSpaceHandler);
}

void
tcds::apve::registerItemSetsWithWebServerAPVEStatus(tcds::utils::WebServer& webServer,
                                                    tcds::utils::Monitor& monitor,
                                                    std::string const& tabName)
{
  webServer.registerTable("APV out-of-sync reason",
                          "Explains why the APV is 'Out-Of-Sync'",
                          monitor,
                          "itemset-apve-status-oos",
                          tabName);
  webServer.registerTable("APV error reason",
                          "Explains why the APV is in 'Error'",
                          monitor,
                          "itemset-apve-status-error",
                          tabName);
}

void
tcds::apve::createItemsAPVECounters(tcds::utils::InfoSpaceHandler* const infoSpaceHandler,
                                    std::string const& prefix)
{
  // TTC-related counters.
  infoSpaceHandler->createUInt64(prefix + "ttc_stats.clk_count");
  infoSpaceHandler->createUInt64(prefix + "ttc_stats.bc0_count");
  infoSpaceHandler->createUInt64(prefix + "ttc_stats.l1a_count");
  infoSpaceHandler->createUInt64(prefix + "ttc_stats.resync_count");

  // TTS-related counters.
  infoSpaceHandler->createUInt64(prefix + "ttc_stats.tcs_warn_count");
  infoSpaceHandler->createUInt64(prefix + "ttc_stats.tcs_busy_count");
  infoSpaceHandler->createUInt64(prefix + "ttc_stats.fmm_warn_count");
  infoSpaceHandler->createUInt64(prefix + "ttc_stats.fmm_busy_count");
  infoSpaceHandler->createUInt64(prefix + "ttc_stats.apv_warn_count");
  infoSpaceHandler->createUInt64(prefix + "ttc_stats.apv_busy_count");
}

void
tcds::apve::registerItemSetsWithMonitorAPVECounters(tcds::utils::Monitor& monitor,
                                                    tcds::utils::InfoSpaceHandler* const infoSpaceHandler,
                                                    std::string const& prefix)
{
  // TTC-related counters.
  std::string itemSetName = "itemset-apve-counters-ttc";
  monitor.newItemSet(itemSetName);
  monitor.addItem(itemSetName,
                  prefix + "ttc_stats.clk_count",
                  "Clock-tick count",
                  infoSpaceHandler);
  monitor.addItem(itemSetName,
                  prefix + "ttc_stats.l1a_count",
                  "L1A count",
                  infoSpaceHandler,
                  "NOTE: Since the APVE does not know about EC0s,"
                  " this is the total number of L1As received since the start of the run");
  monitor.addItem(itemSetName,
                  prefix + "ttc_stats.bc0_count",
                  "BC0 count",
                  infoSpaceHandler);
  monitor.addItem(itemSetName,
                  prefix + "ttc_stats.resync_count",
                  "Resync count",
                  infoSpaceHandler);

  // TTS-related counters.
  itemSetName = "itemset-apve-counters-tts";
  monitor.newItemSet(itemSetName);
  monitor.addItem(itemSetName,
                  prefix + "ttc_stats.tcs_warn_count",
                  "TCS WARN count",
                  infoSpaceHandler);
  monitor.addItem(itemSetName,
                  prefix + "ttc_stats.tcs_busy_count",
                  "TCS BUSY count",
                  infoSpaceHandler);
  monitor.addItem(itemSetName,
                  prefix + "ttc_stats.fmm_warn_count",
                  "FMM WARN count",
                  infoSpaceHandler);
  monitor.addItem(itemSetName,
                  prefix + "ttc_stats.fmm_busy_count",
                  "FMM BUSY count",
                  infoSpaceHandler);
  monitor.addItem(itemSetName,
                  prefix + "ttc_stats.apv_warn_count",
                  "APV WARN count",
                  infoSpaceHandler);
  monitor.addItem(itemSetName,
                  prefix + "ttc_stats.apv_busy_count",
                  "APV BUSY count",
                  infoSpaceHandler);
}

void
tcds::apve::registerItemSetsWithWebServerAPVECounters(tcds::utils::WebServer& webServer,
                                                      tcds::utils::Monitor& monitor,
                                                      std::string const& tabName)
{
  // TTC-related counters.
  webServer.registerTable("TTC-related counters",
                          "",
                          monitor,
                          "itemset-apve-counters-ttc",
                          tabName);

  // TTS-related counters.
  webServer.registerTable("TTS-related counters",
                          "",
                          monitor,
                          "itemset-apve-counters-tts",
                          tabName);
}
