#include "tcds/apve/StatusHistEntry.h"

#include <algorithm>
#include <iomanip>
#include <sstream>

#include "toolbox/string.h"
#include "xcept/Exception.h"

#include "tcds/exception/Exception.h"

tcds::apve::StatusHistEntry::StatusHistEntry(std::vector<uint32_t> const& dataIn)
{
  // Check if the number of words matches what we expect.
  if (dataIn.size() != kNumWordsPerEntry)
    {
      XCEPT_RAISE(tcds::exception::SoftwareProblem,
                  toolbox::toString("Got more or less data than expected "
                                    "(%d words instead of %d) "
                                    "for a single APVE status history entry.",
                                    dataIn.size(), kNumWordsPerEntry));
    }

  // Store the raw data vector.
  rawData_ = dataIn;
  std::reverse(rawData_.begin(), rawData_.end());

  // We have to reshuffle the data contents 'a bit'. Don't ask.
  std::vector<uint8_t> tmpData(2 * kNumWordsPerEntry, 0);
  for (size_t i = 0; i != tmpData.size(); ++i)
    {
      for (size_t j = 0; j != rawData_.size(); ++j)
        {
          uint8_t tmp = tmpData.at(i);
          tmp += (((rawData_.at(j) & (0x1 << i)) >> i) << j);
          tmpData.at(i) = tmp;
        }
    }

  data_.clear();
  data_ = std::vector<uint16_t>(rawData_.size(), 0);
  for (size_t i = 0; i != data_.size(); ++i)
    {
      data_.at(i) = tmpData.at(2 * i) + (tmpData.at(2 * i + 1) << 8);
    }

  //----------

  // Carve the pieces that we want from the raw data. For details see
  // StatusHistEntry.h.
  isHistStart_ = ((data_.at(5) & 0x8) != 0);
  isHistStop_ = ((data_.at(5) & 0x4) != 0);

  bxNumber_ = (data_.at(5) & 0x0000fff0) >> 4;
  orbitNumber_ = (data_.at(7) << 16) + data_.at(6);

  fmmStatus_ = (data_.at(4) & 0x00000f00) >> 8;
  apvStatus_ = (data_.at(4) & 0x0000f000) >> 12;
  ttsState_ = (data_.at(4) & 0x000000f0) >> 4;

  apvErrorReason_ = data_.at(0);
  fmmErrorReason_ = data_.at(1);
  apvOutOfSyncReason_ = data_.at(2);
  fmmOutOfSyncReason_ = data_.at(3);
}

std::string
tcds::apve::StatusHistEntry::rawData() const
{
  std::stringstream res;
  for (std::vector<uint32_t>::const_iterator i = rawData_.begin();
       i != rawData_.end();
       ++i)
    {
      if (!res.str().empty())
        {
          res << ", ";
        }
      res << "0x" << std::setfill('0') << std::setw(4) << std::hex << *i;
    }
  // ------
  res << " | ";
  for (std::vector<uint16_t>::const_iterator i = data_.begin();
       i != data_.end();
       ++i)
    {
      if (!res.str().empty())
        {
          res << ", ";
        }
      res << "0x" << std::setfill('0') << std::setw(4) << std::hex << int(*i);
    }
  return res.str();
}

bool
tcds::apve::StatusHistEntry::isHistStart() const
{
  return isHistStart_;
}

bool
tcds::apve::StatusHistEntry::isHistStop() const
{
  return isHistStop_;
}

uint16_t
tcds::apve::StatusHistEntry::bxNumber() const
{
  return bxNumber_;
}

uint32_t
tcds::apve::StatusHistEntry::orbitNumber() const
{
  return orbitNumber_;
}

uint8_t
tcds::apve::StatusHistEntry::fmmStatus() const
{
  return fmmStatus_;
}

uint8_t
tcds::apve::StatusHistEntry::apvStatus() const
{
  return apvStatus_;
}

uint8_t
tcds::apve::StatusHistEntry::ttsState() const
{
  return ttsState_;
}

std::string
tcds::apve::StatusHistEntry::apvErrorReason() const
{
  std::string res;

  if (apvErrorReason_ & 0x1)
    {
      res = "Forced by software";
    }
  else if (apvErrorReason_ & 0x1)
    {
      res = "APV latency misconfigured";
    }
  else if (apvErrorReason_ & 0x1)
    {
      res = "APV threshold set for BUSY incompatible";
    }
  else if (apvErrorReason_ & 0x1)
    {
      res = "APV threshold set for WARNING incompatible";
    }
  else if (apvErrorReason_ & 0x1)
    {
      res = "No TCS source enabled";
    }
  else if (apvErrorReason_ & 0x1)
    {
      res = "TCS control error";
    }
  else if (apvErrorReason_ & 0x1)
    {
      res = "TCS clock error";
    }

  if (res.empty())
    {
      res = "-";
    }

  return res;
}

std::string
tcds::apve::StatusHistEntry::apvOutOfSyncReason() const
{
  std::string res;

  if (apvOutOfSyncReason_ & 0x1)
    {
      res = "Latched error";
    }
  else if (apvOutOfSyncReason_ & 0x2)
    {
      res = "BC0 sync loss";
    }
  else if (apvOutOfSyncReason_ & 0x4)
    {
      res = "Forced by software";
    }
  else if (apvOutOfSyncReason_ & 0x8)
    {
      res = "APV I2C config change";
    }
  else if (apvOutOfSyncReason_ & 0x10)
    {
      res = "APV config change";
    }
  else if (apvOutOfSyncReason_ & 0x20)
    {
      res = "APV buffer overflow";
    }
  else if (apvOutOfSyncReason_ & 0x40)
    {
      res = "APV resetting";
    }
  else if (apvOutOfSyncReason_ & 0x80)
    {
      res = "Real-APV error";
    }
  else if (apvOutOfSyncReason_ & 0x100)
    {
      res = "Real-APV tick error";
    }

  if (res.empty())
    {
      res = "-";
    }

  return res;
}
