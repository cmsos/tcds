#include "tcds/apve/APVEInfoSpaceUpdater.h"

#include "tcds/apve/TCADeviceAPVE.h"
#include "tcds/utils/InfoSpaceItem.h"

tcds::apve::APVEInfoSpaceUpdater::APVEInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                                       TCADeviceAPVE const& hw) :
  tcds::utils::HwInfoSpaceUpdaterBase(xdaqApp, hw)
{
}

tcds::apve::APVEInfoSpaceUpdater::~APVEInfoSpaceUpdater()
{
}

bool
tcds::apve::APVEInfoSpaceUpdater::updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                                      tcds::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  bool updated = false;
  tcds::apve::TCADeviceAPVE const& hw = getHw();
  if (hw.isHwConnected())
    {
      if (!updated)
        {
          updated = tcds::utils::HwInfoSpaceUpdaterBase::updateInfoSpaceItem(item, infoSpaceHandler);
        }
    }

  if (updated)
    {
      item.setValid();
    }
  else
    {
      item.setInvalid();
    }

  return updated;
}

tcds::apve::TCADeviceAPVE const&
tcds::apve::APVEInfoSpaceUpdater::getHw()
{
  return dynamic_cast<tcds::apve::TCADeviceAPVE const&>(tcds::utils::HwInfoSpaceUpdaterBase::getHw());
}
