#include "tcds/apve/TCADeviceAPVE.h"

#include <algorithm>
#include <memory>
#include <sstream>
#include <vector>

#include "tcds/hwlayertca/BootstrapHelper.h"
#include "tcds/hwlayertca/HwDeviceTCA.h"
#include "tcds/hwlayertca/TCACarrierBase.h"
#include "tcds/hwlayertca/TCACarrierFC7.h"

tcds::apve::TCADeviceAPVE::TCADeviceAPVE() :
  TCADeviceBase(std::unique_ptr<tcds::hwlayertca::TCACarrierBase>(new tcds::hwlayertca::TCACarrierFC7(hwDevice_))),
  apveNumber_(0)
{
}

tcds::apve::TCADeviceAPVE::~TCADeviceAPVE()
{
}

unsigned short
tcds::apve::TCADeviceAPVE::apveNumber() const
{
  return apveNumber_;
}

void
tcds::apve::TCADeviceAPVE::setAPVENumber(unsigned short const apveNumber)
{
  apveNumber_ = apveNumber;
}

void
tcds::apve::TCADeviceAPVE::enableTriggerVetoMaskLoadBuffer() const
{
  // We just need to set one bit here. After that we can use the
  // trigger_veto_mask as a normal register and the loading of the
  // actual shift register used for the trigger veto will happen in
  // the background.
  writeRegister("trigger_veto_shift_register", 0x4);
}

std::vector<uint32_t>
tcds::apve::TCADeviceAPVE::readSimPipelineHistory() const
{
  // Lock the history.
  lockSimPipelineHistory();

  // Reset the read pointer to the start of the buffer.
  writeRegister("apv_sim_history.reset_read_pointer", 0x1);

  // Read the history.
  uint32_t const numEntries = readRegister("apv_sim_history.num_entries");
  uint32_t const blockSize = getBlockSize("apv_sim_history.data");
  std::vector<uint32_t> const res = readBlock("apv_sim_history.data",
                                              std::min(numEntries, blockSize));

  // Unlock the history.
  unlockSimPipelineHistory();

  return res;
}

std::vector<uint32_t>
tcds::apve::TCADeviceAPVE::readStatusHistory() const
{
  std::vector<uint32_t> res;

  // Lock the history.
  lockStatusHistory();

  // Reset the read pointer to the start of the buffer.
  writeRegister("status_history.reset_read_pointer", 0x1);

  // Read the history.
  res = readBlock("status_history.data");

  // Unlock the history.
  unlockStatusHistory();

  return res;
}

void
tcds::apve::TCADeviceAPVE::lockSimPipelineHistory() const
{
  writeRegister("apv_sim_history.locked", 0x1);
}

void
tcds::apve::TCADeviceAPVE::unlockSimPipelineHistory() const
{
  writeRegister("apv_sim_history.locked", 0x0);
}

void
tcds::apve::TCADeviceAPVE::lockStatusHistory() const
{
  writeRegister("status_history.locked", 0x1);
}

void
tcds::apve::TCADeviceAPVE::unlockStatusHistory() const
{
  writeRegister("status_history.locked", 0x0);
}

void
tcds::apve::TCADeviceAPVE::latchStatisticsCounters() const
{
  writeRegister("ttc_stats.latch", 0x1);
}

void
tcds::apve::TCADeviceAPVE::resetStatisticsCounters() const
{
  writeRegister("ttc_stats.reset", 0x1);
}

std::string
tcds::apve::TCADeviceAPVE::regNamePrefixImpl() const
{
  // All the apve registers start with 'apveX.', where 'X' is the apve
  // number in the LPM (i.e., [1, 4]).
  std::stringstream regNamePrefix;
  regNamePrefix << "apve" << apveNumber() << ".";
  return regNamePrefix.str();
}

bool
tcds::apve::TCADeviceAPVE::bootstrapDoneImpl() const
{
  tcds::hwlayertca::BootstrapHelper h(*this);
  return h.bootstrapDone();
}

void
tcds::apve::TCADeviceAPVE::runBootstrapImpl() const
{
  tcds::hwlayertca::BootstrapHelper h(*this);
  h.runBootstrap();
  // Reset the backplane link to the CPM.
  hwDevice_.writeRegister("lpm_main.backplane_tts_control.gtx_reset", 0x0);
  hwDevice_.writeRegister("lpm_main.backplane_tts_control.gtx_reset", 0x1);
  hwDevice_.writeRegister("lpm_main.backplane_tts_control.gtx_reset", 0x0);
}
