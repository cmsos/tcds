#include "tcds/apve/APVESimHistInfoSpaceHandler.h"

#include "tcds/apve/InfoSpaceHandlerUtils.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Utils.h"
#include "tcds/utils/WebServer.h"

tcds::apve::APVESimHistInfoSpaceHandler::APVESimHistInfoSpaceHandler(xdaq::Application& xdaqApp,
                                                                     tcds::utils::InfoSpaceUpdater* updater) :
  InfoSpaceHandler(xdaqApp, "tcds-apvecontroller-simhist", updater)
{
  tcds::apve::createItemsAPVESimHist(this);
}

void
tcds::apve::APVESimHistInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  tcds::apve::registerItemSetsWithMonitorAPVESimHist(monitor, this);
}

void
tcds::apve::APVESimHistInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                       tcds::utils::Monitor& monitor,
                                                                       std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "Pipeline" : forceTabName;

  webServer.registerTab(tabName,
                        "Simulated APV pipeline history",
                        1);
  tcds::apve::registerItemSetsWithWebServerAPVESimHist(webServer, monitor, tabName, 1);
}

std::string
tcds::apve::APVESimHistInfoSpaceHandler::formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const
{
  // Specialized formatting rules for the pipeline history.
  std::string res = tcds::utils::escapeAsJSONString(tcds::utils::InfoSpaceHandler::kInvalidItemString);
  if (item->isValid())
    {
      std::string name = item->name();
      if (name == "apv_simulated_pipeline_history")
        {
          // Special in the sense that this is something that needs to
          // be interpreted as a proper JavaScript object, so let's
          // not add any more double quotes.
          res = getString(name);
        }
      else
        {
          // For everything else simply call the generic formatter.
          res = InfoSpaceHandler::formatItem(item);
        }
    }
  return res;
}
