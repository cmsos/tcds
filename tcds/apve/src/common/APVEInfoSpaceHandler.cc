#include "tcds/apve/APVEInfoSpaceHandler.h"

#include <stdint.h>

#include "tcds/apve/Definitions.h"
#include "tcds/apve/InfoSpaceHandlerUtils.h"
#include "tcds/apve/Utils.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Utils.h"
#include "tcds/utils/WebServer.h"

tcds::apve::APVEInfoSpaceHandler::APVEInfoSpaceHandler(xdaq::Application& xdaqApp,
                                                       tcds::utils::InfoSpaceUpdater* updater) :
  InfoSpaceHandler(xdaqApp, "tcds-apvecontroller-main", updater)
{
  tcds::apve::createItemsAPVE(this);
}

void
tcds::apve::APVEInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  tcds::apve::registerItemSetsWithMonitorAPVE(monitor, this);
}

void
tcds::apve::APVEInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                tcds::utils::Monitor& monitor,
                                                                std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "Emulation" : forceTabName;

  webServer.registerTab(tabName,
                        "APVE information",
                        3);
  tcds::apve::registerItemSetsWithWebServerAPVE(webServer, monitor, tabName);
}

std::string
tcds::apve::APVEInfoSpaceHandler::formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const
{
  // Specialized formatting rules for the enums.
  std::string res = tcds::utils::escapeAsJSONString(tcds::utils::InfoSpaceHandler::kInvalidItemString);
  if (item->isValid())
    {
      std::string name = item->name();
      if (name == "config.apv_read_mode")
        {
          uint32_t const value = getUInt32(name);
          tcds::definitions::APVE_READOUT_MODE valueEnum =
            static_cast<tcds::definitions::APVE_READOUT_MODE>(value);
          res = tcds::utils::escapeAsJSONString(tcds::apve::APVEReadoutModeToString(valueEnum));
        }
      else if (name == "config.apv_trigger_mode")
        {
          uint32_t const value = getUInt32(name);
          tcds::definitions::APVE_TRIGGER_MODE valueEnum =
            static_cast<tcds::definitions::APVE_TRIGGER_MODE>(value);
          res = tcds::utils::escapeAsJSONString(tcds::apve::APVETriggerModeToString(valueEnum));
        }
      else
        {
          // For everything else simply call the generic formatter.
          res = InfoSpaceHandler::formatItem(item);
        }
    }
  return res;
}
