#include "tcds/apve/APVHistContents.h"

#include <iomanip>
#include <cstddef>
#include <sstream>

#include "toolbox/string.h"

#include "tcds/apve/Definitions.h"
#include "tcds/apve/Utils.h"
#include "tcds/utils/Utils.h"

tcds::apve::APVHistContents::APVHistContents(std::vector<uint32_t> const dataIn)
{
  size_t numEntries = dataIn.size();
  entries_.reserve(numEntries);
  entries_.clear();
  for (std::vector<uint32_t>::const_iterator it = dataIn.begin();
       it != dataIn.end();
       ++it)
    {
      APVHistEntry newEntry(*it);
      entries_.push_back(newEntry);
    }
}

std::string
tcds::apve::APVHistContents::getJSONString() const
{
  std::stringstream res;

  res << "[";
  for (std::vector<APVHistEntry>::const_iterator it = entries_.begin();
       it != entries_.end();
       ++it)
    {
      res << "{";

      // The event number.
      res << tcds::utils::escapeAsJSONString("EventNumber")
          << ": "
          << tcds::utils::escapeAsJSONString(toolbox::toString("%u", it->eventNumber()));
      res << ", ";

      // The corresponding pipeline address.
      res << tcds::utils::escapeAsJSONString("PipelineAddress")
          << ": "
          << tcds::utils::escapeAsJSONString(toolbox::toString("%u", int(it->pipelineAddress())));
      res << ", ";

      // The Gray code of the pipeline address.
      res << tcds::utils::escapeAsJSONString("PipelineAddressGrayCode")
          << ": "
          << tcds::utils::escapeAsJSONString(toolbox::toString("0x%02x", int(pipelineAddressToGrayCode(it->pipelineAddress()))));
      res << ", ";

      // The difference in pipeline address with respect to the
      // previous entry.
      // NOTE: The entries are logged in reverse chronological order.
      std::stringstream deltaStr;
      if (it != (entries_.end() - 1))
        {
          int const delta = addressDifference(it->pipelineAddress(), (it + 1)->pipelineAddress());
          deltaStr << std::dec << delta;
        }
      else
        {
          deltaStr << "-";
        }
      res << tcds::utils::escapeAsJSONString("PipelineAddressDelta")
          << ": "
          << tcds::utils::escapeAsJSONString(deltaStr.str());

      res << "}";

      if (it != (entries_.end() - 1))
        {
          res << ", ";
        }
    }
  res << "]";

  return res.str();
}

int
tcds::apve::APVHistContents::addressDifference(uint8_t const address0, uint8_t const address1)
{
  int res = address0 - address1;
  if (res < 0)
    {
      res += tcds::definitions::kAPVPipelineLength;
    }
  return res;
}
