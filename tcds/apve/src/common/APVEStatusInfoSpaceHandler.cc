#include "tcds/apve/APVEStatusInfoSpaceHandler.h"

#include "tcds/apve/InfoSpaceHandlerUtils.h"
#include "tcds/utils/WebServer.h"

tcds::apve::APVEStatusInfoSpaceHandler::APVEStatusInfoSpaceHandler(xdaq::Application& xdaqApp,
                                                                   tcds::utils::InfoSpaceUpdater* updater) :
  InfoSpaceHandler(xdaqApp, "tcds-apvecontroller-status", updater)
{
  tcds::apve::createItemsAPVEStatus(this);
}

void
tcds::apve::APVEStatusInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  tcds::apve::registerItemSetsWithMonitorAPVEStatus(monitor, this);
}

void
tcds::apve::APVEStatusInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                      tcds::utils::Monitor& monitor,
                                                                      std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "Counters" : forceTabName;

  webServer.registerTab(tabName,
                        "APVE status information",
                        4);
  tcds::apve::registerItemSetsWithWebServerAPVEStatus(webServer, monitor, tabName);
}
