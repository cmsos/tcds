#include "tcds/apve/APVECountersInfoSpaceHandler.h"

#include "tcds/apve/InfoSpaceHandlerUtils.h"
#include "tcds/utils/WebServer.h"

tcds::apve::APVECountersInfoSpaceHandler::APVECountersInfoSpaceHandler(xdaq::Application& xdaqApp,
                                                                       tcds::utils::InfoSpaceUpdater* updater) :
  InfoSpaceHandler(xdaqApp, "tcds-apve-counters", updater)
{
  tcds::apve::createItemsAPVECounters(this);
}

void
tcds::apve::APVECountersInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  tcds::apve::registerItemSetsWithMonitorAPVECounters(monitor, this);
}

void
tcds::apve::APVECountersInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                        tcds::utils::Monitor& monitor,
                                                                        std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "Counters" : forceTabName;

  webServer.registerTab(tabName,
                        "The 'TTC statistics' from the pre-LS1 APVE",
                        2);
  tcds::apve::registerItemSetsWithWebServerAPVECounters(webServer, monitor, tabName);
}
