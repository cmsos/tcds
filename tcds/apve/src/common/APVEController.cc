#include "tcds/apve/APVEController.h"

#include <string>

#include "log4cplus/loggingmacros.h"

#include "toolbox/string.h"
#include "xcept/Exception.h"
#include "xdaq/exception/ApplicationInstantiationFailed.h"

#include "tcds/apve/APVECountersInfoSpaceHandler.h"
#include "tcds/apve/APVECountersInfoSpaceUpdater.h"
#include "tcds/apve/APVEInfoSpaceHandler.h"
#include "tcds/apve/APVEInfoSpaceUpdater.h"
#include "tcds/apve/APVESimHistInfoSpaceHandler.h"
#include "tcds/apve/APVESimHistInfoSpaceUpdater.h"
#include "tcds/apve/APVEStatusHistInfoSpaceHandler.h"
#include "tcds/apve/APVEStatusHistInfoSpaceUpdater.h"
#include "tcds/apve/APVEStatusInfoSpaceHandler.h"
#include "tcds/apve/APVEStatusInfoSpaceUpdater.h"
#include "tcds/apve/APVETriggerMaskInfoSpaceHandler.h"
#include "tcds/apve/APVETriggerMaskInfoSpaceUpdater.h"
#include "tcds/apve/ConfigurationInfoSpaceHandler.h"
#include "tcds/apve/HwStatusInfoSpaceHandler.h"
#include "tcds/apve/HwStatusInfoSpaceUpdater.h"
#include "tcds/apve/TCADeviceAPVE.h"
#include "tcds/exception/Exception.h"
#include "tcds/hwlayer/DeviceBase.h"
#include "tcds/hwlayer/RegisterInfo.h"
#include "tcds/hwutilstca/HwIDInfoSpaceHandlerTCA.h"
#include "tcds/hwutilstca/HwIDInfoSpaceUpdaterTCA.h"
#include "tcds/hwutilstca/Utils.h"
#include "tcds/utils/ApplicationStateInfoSpaceHandler.h"
#include "tcds/utils/ConfigurationInfoSpaceHandler.h"
#include "tcds/utils/LogMacros.h"

XDAQ_INSTANTIATOR_IMPL(tcds::apve::APVEController)

tcds::apve::APVEController::APVEController(xdaq::ApplicationStub* stub)
try
  :
  tcds::utils::XDAQAppWithFSMBasic(stub, std::unique_ptr<tcds::hwlayer::DeviceBase>(new tcds::apve::TCADeviceAPVE())),
    soapCmdDumpHardwareState_(*this),
    soapCmdReadHardwareConfiguration_(*this)
  {
    // Create the InfoSpace holding all configuration information.
    cfgInfoSpaceP_ =
      std::unique_ptr<tcds::apve::ConfigurationInfoSpaceHandler>(new tcds::apve::ConfigurationInfoSpaceHandler(*this));

    // Make sure the correct default hardware configuration file is found.
    cfgInfoSpaceP_->setString("defaultHwConfigurationFilePath",
                              "${XDAQ_ROOT}/etc/tcds/apve/hw_cfg_default_apve.txt");
  }
catch (tcds::exception::Exception const& err)
  {
    std::string msgBase = "Something went wrong instantiating the APVEController application";
    std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.what());
    XCEPT_RAISE(xdaq::exception::ApplicationInstantiationFailed, msg.c_str());
  }

tcds::apve::APVEController::~APVEController()
{
  hwRelease();
}

void
tcds::apve::APVEController::setupInfoSpaces()
{
  // Make sure the configuration settings are up-to-date.
  cfgInfoSpaceP_->readInfoSpace();

  // Instantiate all hardware-related InfoSpaceHandlers and InfoSpaceUpdaters.
  apveInfoSpaceUpdaterP_ =
    std::unique_ptr<APVEInfoSpaceUpdater>(new APVEInfoSpaceUpdater(*this, getHw()));
  apveInfoSpaceP_ =
    std::unique_ptr<APVEInfoSpaceHandler>(new APVEInfoSpaceHandler(*this, apveInfoSpaceUpdaterP_.get()));
  apveCountersInfoSpaceUpdaterP_ =
    std::unique_ptr<APVECountersInfoSpaceUpdater>(new APVECountersInfoSpaceUpdater(*this, getHw()));
  apveCountersInfoSpaceP_ =
    std::unique_ptr<APVECountersInfoSpaceHandler>(new APVECountersInfoSpaceHandler(*this, apveCountersInfoSpaceUpdaterP_.get()));
  apveSimHistInfoSpaceUpdaterP_ =
    std::unique_ptr<APVESimHistInfoSpaceUpdater>(new APVESimHistInfoSpaceUpdater(*this, getHw()));
  apveSimHistInfoSpaceP_ =
    std::unique_ptr<APVESimHistInfoSpaceHandler>(new APVESimHistInfoSpaceHandler(*this, apveSimHistInfoSpaceUpdaterP_.get()));
  apveStatusInfoSpaceUpdaterP_ =
    std::unique_ptr<APVEStatusInfoSpaceUpdater>(new APVEStatusInfoSpaceUpdater(*this, getHw()));
  apveStatusInfoSpaceP_ =
    std::unique_ptr<APVEStatusInfoSpaceHandler>(new APVEStatusInfoSpaceHandler(*this, apveStatusInfoSpaceUpdaterP_.get()));
  apveStatusHistInfoSpaceUpdaterP_ =
    std::unique_ptr<APVEStatusHistInfoSpaceUpdater>(new APVEStatusHistInfoSpaceUpdater(*this, getHw()));
  apveStatusHistInfoSpaceP_ =
    std::unique_ptr<APVEStatusHistInfoSpaceHandler>(new APVEStatusHistInfoSpaceHandler(*this, apveStatusHistInfoSpaceUpdaterP_.get()));
  apveTriggerMaskInfoSpaceUpdaterP_ =
    std::unique_ptr<APVETriggerMaskInfoSpaceUpdater>(new APVETriggerMaskInfoSpaceUpdater(*this, getHw()));
  apveTriggerMaskInfoSpaceP_ =
    std::unique_ptr<APVETriggerMaskInfoSpaceHandler>(new APVETriggerMaskInfoSpaceHandler(*this, apveTriggerMaskInfoSpaceUpdaterP_.get()));
  hwIDInfoSpaceUpdaterP_ =
    std::unique_ptr<tcds::hwutilstca::HwIDInfoSpaceUpdaterTCA>(new tcds::hwutilstca::HwIDInfoSpaceUpdaterTCA(*this, getHw()));
  hwIDInfoSpaceP_ =
    std::unique_ptr<tcds::hwutilstca::HwIDInfoSpaceHandlerTCA>(new tcds::hwutilstca::HwIDInfoSpaceHandlerTCA(*this, hwIDInfoSpaceUpdaterP_.get()));
  hwStatusInfoSpaceUpdaterP_ =
    std::unique_ptr<tcds::apve::HwStatusInfoSpaceUpdater>(new tcds::apve::HwStatusInfoSpaceUpdater(*this, getHw()));
  hwStatusInfoSpaceP_ =
    std::unique_ptr<tcds::apve::HwStatusInfoSpaceHandler>(new tcds::apve::HwStatusInfoSpaceHandler(*this, hwStatusInfoSpaceUpdaterP_.get()));

  // Register all InfoSpaceItems with the Monitor and the WebServer.
  cfgInfoSpaceP_->registerItemSets(monitor_, webServer_);
  appStateInfoSpace_.registerItemSets(monitor_, webServer_);
  hwIDInfoSpaceP_->registerItemSets(monitor_, webServer_);
  hwStatusInfoSpaceP_->registerItemSets(monitor_, webServer_);
  apveInfoSpaceP_->registerItemSets(monitor_, webServer_);
  apveTriggerMaskInfoSpaceP_->registerItemSets(monitor_, webServer_);
  apveStatusInfoSpaceP_->registerItemSets(monitor_, webServer_);
  apveSimHistInfoSpaceP_->registerItemSets(monitor_, webServer_);
  apveStatusHistInfoSpaceP_->registerItemSets(monitor_, webServer_);
  apveCountersInfoSpaceP_->registerItemSets(monitor_, webServer_);
}

tcds::apve::TCADeviceAPVE&
tcds::apve::APVEController::getHw() const
{
  return static_cast<tcds::apve::TCADeviceAPVE&>(*hwP_.get());
}

void
tcds::apve::APVEController::configureActionImpl(toolbox::Event::Reference event)
{
  // Extract the apve number from the configuration and propagate that
  // into our hardware device instance.
  unsigned short const apveNumber = cfgInfoSpaceP_->getUInt32("apveNumber");
  getHw().setAPVENumber(apveNumber);

  // Then do what we normally do.
  tcds::utils::XDAQAppWithFSMBasic::configureActionImpl(event);
}

void
tcds::apve::APVEController::zeroActionImpl(toolbox::Event::Reference event)
{
  getHw().resetStatisticsCounters();
}

tcds::utils::RegCheckResult
tcds::apve::APVEController::isRegisterAllowed(tcds::hwlayer::RegisterInfo const& regInfo) const
{
  // Start by applying the default selection.
  tcds::utils::RegCheckResult res = tcds::utils::XDAQAppWithFSMBasic::isRegisterAllowed(regInfo);

  // There is no FMM, so this register should not be messed with.
  res = (res && !toolbox::endsWith(regInfo.name(), "config.fmm_enable"))
    ? res : tcds::utils::kRegCheckResultDisallowed;

  // There is no real APV, so this register should not be messed with.
  res = (res && !toolbox::endsWith(regInfo.name(), "config.apv_sim_select"))
    ? res : tcds::utils::kRegCheckResultDisallowed;

  return res;
}

void
tcds::apve::APVEController::hwConnectImpl()
{
  tcds::hwutilstca::tcaDeviceHwConnectImpl(*cfgInfoSpaceP_, getHw());
}

void
tcds::apve::APVEController::hwReleaseImpl()
{
  getHw().hwRelease();
}

void
tcds::apve::APVEController::hwCfgInitializeImpl()
{
  // Check for the presence of the TTC clock. Without TTC clock it is
  // no use continuing (and apart from that, some registers will not
  // be accessible).
  if (!(getHw().isTTCClockUp() && getHw().isTTCClockStable()))
    {
      std::string const msg = "Could not configure the hardware: no TTC clock present, or clock not stable.";
      ERROR(msg);
      XCEPT_RAISE(tcds::exception::TTCClockProblem, msg.c_str());
    }

  //----------

  // Configure and enable the 'TTC-stream phase monitoring.'
  // NOTE: This really measures phases between clocks, not w.r.t. the
  // real TTC stream, so this can be enabled before selecting the TTC
  // stream source.
  getHw().enablePhaseMonitoring();

  //----------

  // Configure and enable the 'TTC-stream phase monitoring.'
  // NOTE: This really measures phases between clocks, not the real
  // TTC stream.
  getHw().enablePhaseMonitoring();
}

void
tcds::apve::APVEController::hwCfgFinalizeImpl()
{
  // Make sure the histories are unlocked for writing.
  getHw().unlockSimPipelineHistory();
  getHw().unlockStatusHistory();

  // Enable the parallel-load buffer for the trigger-veto shift
  // register.
  getHw().enableTriggerVetoMaskLoadBuffer();

  // These are required settings. Other values make no sense for the
  // APVE in the TCDS.
  getHw().writeRegister("config.apv_sim_select", 0x1);
  getHw().writeRegister("config.fmm_enable", 0x0);
}

unsigned short
tcds::apve::APVEController::apveNumber() const
{
  return getHw().apveNumber();
}
