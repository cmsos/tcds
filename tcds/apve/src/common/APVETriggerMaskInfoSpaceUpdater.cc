#include "tcds/apve/APVETriggerMaskInfoSpaceUpdater.h"

#include "tcds/apve/TCADeviceAPVE.h"
#include "tcds/utils/InfoSpaceItem.h"

tcds::apve::APVETriggerMaskInfoSpaceUpdater::APVETriggerMaskInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                                                             TCADeviceAPVE const& hw) :
  tcds::utils::HwInfoSpaceUpdaterBase(xdaqApp, hw)
{
}

tcds::apve::APVETriggerMaskInfoSpaceUpdater::~APVETriggerMaskInfoSpaceUpdater()
{
}

bool
tcds::apve::APVETriggerMaskInfoSpaceUpdater::updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                                                 tcds::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  bool updated = false;
  tcds::apve::TCADeviceAPVE const& hw = getHw();
  if (hw.isHwConnected())
    {
      if (!updated)
        {
          updated = tcds::utils::HwInfoSpaceUpdaterBase::updateInfoSpaceItem(item, infoSpaceHandler);
        }
    }

  if (updated)
    {
      item.setValid();
    }
  else
    {
      item.setInvalid();
    }

  return updated;
}

tcds::apve::TCADeviceAPVE const&
tcds::apve::APVETriggerMaskInfoSpaceUpdater::getHw()
{
  return dynamic_cast<tcds::apve::TCADeviceAPVE const&>(tcds::utils::HwInfoSpaceUpdaterBase::getHw());
}
