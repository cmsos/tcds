#include "tcds/apve/APVEStatusHistInfoSpaceHandler.h"

#include "tcds/apve/WebTableStatusHist.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/Utils.h"
#include "tcds/utils/WebServer.h"

tcds::apve::APVEStatusHistInfoSpaceHandler::APVEStatusHistInfoSpaceHandler(xdaq::Application& xdaqApp,
                                                                           tcds::utils::InfoSpaceUpdater* updater) :
  InfoSpaceHandler(xdaqApp, "tcds-apvecontroller-statushist", updater)
{
  createString("apv_status_history");
}

void
tcds::apve::APVEStatusHistInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  monitor.newItemSet("itemset-statushist");
  monitor.addItem("itemset-statushist",
                  "apv_status_history",
                  "TTS and APVE status history",
                  this);
}

void
tcds::apve::APVEStatusHistInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                          tcds::utils::Monitor& monitor,
                                                                          std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "TTS" : forceTabName;

  webServer.registerTab(tabName,
                        "TTS and APVE status history",
                        1);
  webServer.registerWebObject<WebTableStatusHist>("Status history",
                                                  "TTS and status history",
                                                  monitor,
                                                  "itemset-statushist",
                                                  tabName);
}

std::string
tcds::apve::APVEStatusHistInfoSpaceHandler::formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const
{
  // Specialized formatting rules for the pipeline history.
  std::string res = tcds::utils::escapeAsJSONString(tcds::utils::InfoSpaceHandler::kInvalidItemString);
  if (item->isValid())
    {
      std::string name = item->name();
      if (name == "apv_status_history")
        {
          // Special in the sense that this is something that needs to
          // be interpreted as a proper JavaScript object, so let's
          // not add any more double quotes.
          res = getString(name);
        }
      else
        {
          // For everything else simply call the generic formatter.
          res = InfoSpaceHandler::formatItem(item);
        }
    }
  return res;
}
