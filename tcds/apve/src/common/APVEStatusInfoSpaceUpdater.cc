#include "tcds/apve/APVEStatusInfoSpaceUpdater.h"

#include "tcds/apve/TCADeviceAPVE.h"
#include "tcds/utils/InfoSpaceItem.h"

tcds::apve::APVEStatusInfoSpaceUpdater::APVEStatusInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                                                   TCADeviceAPVE const& hw) :
  tcds::utils::HwInfoSpaceUpdaterBase(xdaqApp, hw)
{
}

tcds::apve::APVEStatusInfoSpaceUpdater::~APVEStatusInfoSpaceUpdater()
{
}

bool
tcds::apve::APVEStatusInfoSpaceUpdater::updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                                            tcds::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  bool updated = false;
  tcds::apve::TCADeviceAPVE const& hw = getHw();
  if (hw.isHwConnected())
    {
      if (!updated)
        {
          updated = tcds::utils::HwInfoSpaceUpdaterBase::updateInfoSpaceItem(item, infoSpaceHandler);
        }
    }

  if (updated)
    {
      item.setValid();
    }
  else
    {
      item.setInvalid();
    }

  return updated;
}

tcds::apve::TCADeviceAPVE const&
tcds::apve::APVEStatusInfoSpaceUpdater::getHw()
{
  return dynamic_cast<tcds::apve::TCADeviceAPVE const&>(tcds::utils::HwInfoSpaceUpdaterBase::getHw());
}
