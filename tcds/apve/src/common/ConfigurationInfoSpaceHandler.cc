#include "tcds/apve/ConfigurationInfoSpaceHandler.h"

#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Monitor.h"

tcds::apve::ConfigurationInfoSpaceHandler::ConfigurationInfoSpaceHandler(xdaq::Application& xdaqApp) :
  tcds::hwutilstca::ConfigurationInfoSpaceHandlerTCA(xdaqApp)
{
  createUInt32("apveNumber", 1, "", tcds::utils::InfoSpaceItem::NOUPDATE, true);
}

void
tcds::apve::ConfigurationInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  tcds::hwutilstca::ConfigurationInfoSpaceHandlerTCA::registerItemSetsWithMonitor(monitor);
  monitor.addItem("Application configuration",
                  "apveNumber",
                  "apve number",
                  this);
}
