#include "tcds/apve/APVETriggerMaskInfoSpaceHandler.h"

#include "tcds/apve/InfoSpaceHandlerUtils.h"
#include "tcds/utils/WebServer.h"

tcds::apve::APVETriggerMaskInfoSpaceHandler::APVETriggerMaskInfoSpaceHandler(xdaq::Application& xdaqApp,
                                                                             tcds::utils::InfoSpaceUpdater* updater) :
  InfoSpaceHandler(xdaqApp, "tcds-apvecontroller-triggermask", updater)
{
  tcds::apve::createItemsAPVETriggerMask(this);
}

void
tcds::apve::APVETriggerMaskInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  tcds::apve::registerItemSetsWithMonitorAPVETriggerMask(monitor, this);
}

void
tcds::apve::APVETriggerMaskInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                           tcds::utils::Monitor& monitor,
                                                                           std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "Trigger mask" : forceTabName;

  webServer.registerTab(tabName,
                        "Veto mask to suppress high-rate trigger-noise in the APV",
                        1);
  tcds::apve::registerItemSetsWithWebServerAPVETriggerMask(webServer, monitor, tabName);
}
