#include "tcds/apve/APVEStatusHistInfoSpaceUpdater.h"

#include <stdint.h>
#include <string>
#include <vector>

#include "tcds/apve/StatusHistContents.h"
#include "tcds/apve/TCADeviceAPVE.h"
#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/InfoSpaceItem.h"

tcds::apve::APVEStatusHistInfoSpaceUpdater::APVEStatusHistInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                                                           TCADeviceAPVE const& hw) :
  tcds::utils::HwInfoSpaceUpdaterBase(xdaqApp, hw)
{
}

tcds::apve::APVEStatusHistInfoSpaceUpdater::~APVEStatusHistInfoSpaceUpdater()
{
}

bool
tcds::apve::APVEStatusHistInfoSpaceUpdater::updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                                                tcds::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  bool updated = false;
  tcds::apve::TCADeviceAPVE const& hw = getHw();
  if (hw.isHwConnected())
    {
      std::string const name = item.name();
      tcds::utils::InfoSpaceItem::UpdateType const updateType = item.updateType();
      if (updateType == tcds::utils::InfoSpaceItem::PROCESS)
        {
          // The 'PROCESS' update type means that there is something
          // special to the variable. Figure out what to do based on
          // the variable name.
          if (name == "apv_status_history")
            {
              std::string newVal = "[]";
              std::vector<uint32_t> const histContents = hw.readStatusHistory();
              if (!histContents.empty())
                {
                  StatusHistContents tmp(histContents);
                  newVal = tmp.getJSONString();
                }
              infoSpaceHandler->setString(name, newVal);
              updated = true;
            }
        }
      if (!updated)
        {
          updated = tcds::utils::HwInfoSpaceUpdaterBase::updateInfoSpaceItem(item, infoSpaceHandler);
        }
    }

  if (updated)
    {
      item.setValid();
    }
  else
    {
      item.setInvalid();
    }

  return updated;
}

tcds::apve::TCADeviceAPVE const&
tcds::apve::APVEStatusHistInfoSpaceUpdater::getHw()
{
  return dynamic_cast<tcds::apve::TCADeviceAPVE const&>(tcds::utils::HwInfoSpaceUpdaterBase::getHw());
}
