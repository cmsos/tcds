#include "tcds/apve/APVECountersInfoSpaceUpdater.h"

#include <stdint.h>
#include <string>

#include "toolbox/string.h"

#include "tcds/apve/TCADeviceAPVE.h"
#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/InfoSpaceItem.h"

tcds::apve::APVECountersInfoSpaceUpdater::APVECountersInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                                                       TCADeviceAPVE const& hw) :
  tcds::utils::HwInfoSpaceUpdaterBase(xdaqApp, hw)
{
}

tcds::apve::APVECountersInfoSpaceUpdater::~APVECountersInfoSpaceUpdater()
{
}

void
tcds::apve::APVECountersInfoSpaceUpdater::updateInfoSpaceImpl(tcds::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  // Latch the APVE statistics counters.
  tcds::apve::TCADeviceAPVE const& hw = getHw();
  if (hw.isHwConnected())
    {
      hw.latchStatisticsCounters();
    }
  tcds::utils::HwInfoSpaceUpdaterBase::updateInfoSpaceImpl(infoSpaceHandler);
}

bool
tcds::apve::APVECountersInfoSpaceUpdater::updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                                              tcds::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  bool updated = false;
  tcds::apve::TCADeviceAPVE const& hw = getHw();
  if (hw.isHwConnected())
    {
      // NOTE: The following are all 64-bit counters. They are
      // 'clocked-out' in four successive 16-bit reads.
      // NOTE: The assumption for the below is that nothing read these
      // registers after the latching in updateInfoSpaceImpl().
      std::string const name = item.name();
      if (toolbox::endsWith(name, "_count"))
        {
          uint32_t const tmp0 = hw.readRegister(name);
          uint32_t const tmp1 = hw.readRegister(name);
          uint32_t const tmp2 = hw.readRegister(name);
          uint32_t const tmp3 = hw.readRegister(name);
          uint64_t const newVal =
            uint64_t(tmp0 & 0x0000ffff) +
            (uint64_t(tmp1 & 0x0000ffff) << 16) +
            (uint64_t(tmp2 & 0x0000ffff) << 32) +
            (uint64_t(tmp3 & 0x0000ffff) << 48);
          infoSpaceHandler->setUInt64(name, newVal);
          updated = true;
        }
      if (!updated)
        {
          updated = tcds::utils::HwInfoSpaceUpdaterBase::updateInfoSpaceItem(item, infoSpaceHandler);
        }
    }

  if (updated)
    {
      item.setValid();
    }
  else
    {
      item.setInvalid();
    }

  return updated;
}

tcds::apve::TCADeviceAPVE const&
tcds::apve::APVECountersInfoSpaceUpdater::getHw()
{
  return dynamic_cast<tcds::apve::TCADeviceAPVE const&>(tcds::utils::HwInfoSpaceUpdaterBase::getHw());

}
