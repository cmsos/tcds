#include "tcds/apve/HwStatusInfoSpaceHandler.h"

tcds::apve::HwStatusInfoSpaceHandler::HwStatusInfoSpaceHandler(xdaq::Application& xdaqApp,
                                                               tcds::utils::InfoSpaceUpdater* updater) :
  HwStatusInfoSpaceHandlerTCA(xdaqApp, "tcds-hw-status-apve", updater)
{
}

tcds::apve::HwStatusInfoSpaceHandler::~HwStatusInfoSpaceHandler()
{
}
