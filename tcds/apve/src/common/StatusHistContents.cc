#include "tcds/apve/StatusHistContents.h"

#include <cstddef>
#include <sstream>

#include "tcds/utils/Utils.h"

tcds::apve::StatusHistContents::StatusHistContents(std::vector<uint32_t> const dataIn)
{
  size_t const numWordsPerEntry = StatusHistEntry::kNumWordsPerEntry;
  size_t const numEntries = dataIn.size() / numWordsPerEntry;
  entries_.reserve(numEntries);
  entries_.clear();
  std::vector<uint32_t>::const_iterator it = dataIn.begin();
  while (it < dataIn.end())
    {
      std::vector<uint32_t> tmp(it, it + numWordsPerEntry);
      StatusHistEntry newEntry(tmp);
      entries_.push_back(newEntry);
      it += numWordsPerEntry;
    }
}

std::string
tcds::apve::StatusHistContents::getJSONString() const
{
  std::stringstream res;

  res << "[";
  for (std::vector<StatusHistEntry>::const_iterator it = entries_.begin();
       it != entries_.end();
       ++it)
    {
      res << "{";

      // // The raw data.
      // res << "\"RawData\": \"" << std::dec << it->rawData() << "\"";
      // res << ", ";

      // The orbit number.
      res << "\"OrbitNumber\": \"" << std::dec << it->orbitNumber() << "\"";
      res << ", ";

      // The bunch-crossing number.
      res << "\"BXNumber\": \"" << std::dec << it->bxNumber() << "\"";
      res << ", ";

      // The output TTS state.
      res << "\"TTSState\": \"" << std::dec << tcds::utils::TTSStateToString(it->ttsState()) << "\"";
      res << ", ";

      // The FMM TTS state.
      res << "\"FMMTTSState\": \"" << std::dec << tcds::utils::TTSStateToString(it->fmmStatus()) << "\"";
      res << ", ";

      // The APV TTS state.
      res << "\"APVTTSState\": \"" << std::dec << tcds::utils::TTSStateToString(it->apvStatus()) << "\"";
      res << ", ";

      // The reason the APV is out-of-sync.
      res << "\"APVOOSReason\": \"" << it->apvOutOfSyncReason() << "\"";
      res << ", ";

      // The reason the APV is in error.
      res << "\"APVErrorReason\": \"" << it->apvErrorReason() << "\"";
      res << ", ";

      // History start/stop flags, if set.
      std::string flagsStr;
      if (it->isHistStart())
        {
          flagsStr = "First entry after BC0 sync loss (or Resync).";
        }
      if (it->isHistStop())
        {
          if (!flagsStr.empty())
            {
              flagsStr += ", ";
            }
          flagsStr += "Last entry (due to BC0 sync loss).";
        }
      res << "\"Flags\": \"" << flagsStr << "\"";

      res << "}";

      if (it != (entries_.end() - 1))
        {
          res << ", ";
        }
    }
  res << "]";

  return res.str();
}
