<?xml version="1.0" encoding="UTF-8"?>

<node id="phasemon">

  <!-- The phase monitoring is based on the White-Rabbit phase detector. -->

  <!-- The reference clock can be selected between FCLKA and a 40 MHz
       clock signal on SFP8 RX. -->

  <!-- The reference clock selection. -->
  <node id="refclk_select">
    <node id="master" address="0x00000010" mask="0x00000001" permission="rw"
          description="0: fabric clock, 1: external ref. clock." />
    <node id="external" address="0x00000010" mask="0x00000008" permission="rw"
          description="0: L8 SFP8 RX, 1: ECL from L12 clk0." />
  </node>

  <!-- TTC FMC monitoring. -->
  <node id="ttc_fmc_mon" address="0x00000001">
    <node id="ttc_fmc_pll_locked" address="0x00000000" mask="0x00000008" permission="r" />
    <node id="ttc_fmc_divider_reset" address="0x00000000" mask="0x00000020" permission="r" />
    <node id="ttc_fmc_ready" address="0x00000000" mask="0x00000040" permission="r" />
  </node>

  <!-- Common. -->
  <node id="meas_common">
    <node id="mmcm1_reset" address="0x00000011" mask="0x00000001" permission="rw" />
    <node id="mmcm2_reset" address="0x00000011" mask="0x00000002" permission="rw" />
    <node id="mmcm1_locked" address="0x00000003" mask="0x00000001" permission="r" />
    <node id="mmcm2_locked" address="0x00000003" mask="0x00000002" permission="r" />
    <node id="measurement_duration" address="0x00000011" mask="0x0fff0000" permission="rw"
          description="How many times to measure (for averaging)." />
  </node>

  <!-- Signal input: L8 SFP1 RX. -->
  <node id="meas_sfp1">
    <node id="reset" address="0x00000012" mask="0x00000001" permission="rw" />
    <node id="enable" address="0x00000013" mask="0x00000001" permission="rw" />
    <node id="disconnected" address="0x00000015" mask="0x00000001" permission="rw" />
    <node id="data_valid" address="0x00000003" mask="0x00000004" permission="rw" />
    <node id="result" address="0x00000004" mask="0xffffffff" permission="r" />
  </node>

  <!-- Signal input: L8 SFP2 RX. -->
  <node id="meas_sfp2">
    <node id="reset" address="0x00000012" mask="0x00000002" permission="rw" />
    <node id="enable" address="0x00000013" mask="0x00000002" permission="rw" />
    <node id="disconnected" address="0x00000015" mask="0x00000002" permission="rw" />
    <node id="data_valid" address="0x00000003" mask="0x00000008" permission="rw" />
    <node id="result" address="0x00000005" mask="0xffffffff" permission="r" />
  </node>

  <!-- Signal input: L8 SFP3 RX. -->
  <node id="meas_sfp3">
    <node id="reset" address="0x00000012" mask="0x00000004" permission="rw" />
    <node id="enable" address="0x00000013" mask="0x00000004" permission="rw" />
    <node id="disconnected" address="0x00000015" mask="0x00000004" permission="rw" />
    <node id="data_valid" address="0x00000003" mask="0x00000010" permission="rw" />
    <node id="result" address="0x00000006" mask="0xffffffff" permission="r" />
  </node>

  <!-- Signal input: L8 SFP4 RX. -->
  <node id="meas_sfp4">
    <node id="reset" address="0x00000012" mask="0x00000008" permission="rw" />
    <node id="enable" address="0x00000013" mask="0x00000008" permission="rw" />
    <node id="disconnected" address="0x00000015" mask="0x00000008" permission="rw" />
    <node id="data_valid" address="0x00000003" mask="0x00000020" permission="rw" />
    <node id="result" address="0x00000007" mask="0xffffffff" permission="r" />
  </node>

  <!-- Signal input: L8 SFP5 RX. -->
  <node id="meas_sfp5">
    <node id="reset" address="0x00000012" mask="0x00000010" permission="rw" />
    <node id="enable" address="0x00000013" mask="0x00000010" permission="rw" />
    <node id="disconnected" address="0x00000015" mask="0x00000010" permission="rw" />
    <node id="data_valid" address="0x00000003" mask="0x00000040" permission="rw" />
    <node id="result" address="0x00000008" mask="0xffffffff" permission="r" />
  </node>

  <!-- Signal input: L8 SFP6 RX. -->
  <node id="meas_sfp6">
    <node id="reset" address="0x00000012" mask="0x00000020" permission="rw" />
    <node id="enable" address="0x00000013" mask="0x00000020" permission="rw" />
    <node id="disconnected" address="0x00000015" mask="0x00000020" permission="rw" />
    <node id="data_valid" address="0x00000003" mask="0x00000080" permission="rw" />
    <node id="result" address="0x00000009" mask="0xffffffff" permission="r" />
  </node>

  <!-- Signal input: L8 SFP7 RX. -->
  <node id="meas_sfp7">
    <node id="reset" address="0x00000012" mask="0x00000040" permission="rw" />
    <node id="enable" address="0x00000013" mask="0x00000040" permission="rw" />
    <node id="disconnected" address="0x00000015" mask="0x00000040" permission="rw" />
    <node id="data_valid" address="0x00000003" mask="0x00000100" permission="rw" />
    <node id="result" address="0x0000000a" mask="0xffffffff" permission="r" />
  </node>

  <!-- Signal input: FCLKA. -->
  <node id="meas_fclka">
    <node id="reset" address="0x00000012" mask="0x00000080" permission="rw" />
    <node id="enable" address="0x00000013" mask="0x00000080" permission="rw" />
    <node id="disconnected" address="0x00000015" mask="0x00000080" permission="rw" />
    <node id="data_valid" address="0x00000003" mask="0x00000200" permission="rw" />
    <node id="result" address="0x0000000b" mask="0xffffffff" permission="r" />
  </node>

  <!-- Signal input: TTC FMC decoded clock output. -->
  <node id="meas_ttc_fmc">
    <node id="reset" address="0x00000012" mask="0x00000100" permission="rw" />
    <node id="enable" address="0x00000013" mask="0x00000100" permission="rw" />
    <node id="disconnected" address="0x00000015" mask="0x00000100" permission="rw" />
    <node id="data_valid" address="0x00000003" mask="0x00000400" permission="rw" />
    <node id="result" address="0x0000000c" mask="0xffffffff" permission="r" />
  </node>

  <!-- Signal input: ECL clock input connected to the TTC orbit input
       on the 6-lemo TCDS FMC on L12. -->
  <node id="meas_ecl_ttcorb">
    <node id="reset" address="0x00000012" mask="0x00000200" permission="rw" />
    <node id="enable" address="0x00000013" mask="0x00000200" permission="rw" />
    <node id="disconnected" address="0x00000015" mask="0x00000200" permission="rw" />
    <node id="data_valid" address="0x00000003" mask="0x00000800" permission="rw" />
    <node id="result" address="0x00000019" mask="0xffffffff" permission="r" />
  </node>

  <!-- Signal input: ECL clock input connected to the modified (!) TTC
       tr0 input on the 6-lemo TCDS FMC on L12. -->
  <node id="meas_ecl_tr0">
    <node id="reset" address="0x00000012" mask="0x00000400" permission="rw" />
    <node id="enable" address="0x00000013" mask="0x00000400" permission="rw" />
    <node id="disconnected" address="0x00000015" mask="0x00000400" permission="rw" />
    <node id="data_valid" address="0x00000003" mask="0x00001000" permission="rw" />
    <node id="result" address="0x0000001a" mask="0xffffffff" permission="r" />
  </node>

  <!-- Signal input: ECL clock input connected to the modified (!) TTC
       tr1 input on the 6-lemo TCDS FMC on L12. -->
  <node id="meas_ecl_tr1">
    <node id="reset" address="0x00000012" mask="0x00000800" permission="rw" />
    <node id="enable" address="0x00000013" mask="0x00000800" permission="rw" />
    <node id="disconnected" address="0x00000015" mask="0x00000800" permission="rw" />
    <node id="data_valid" address="0x00000003" mask="0x00002000" permission="rw" />
    <node id="result" address="0x0000001b" mask="0xffffffff" permission="r" />
  </node>

  <!-- Signal input: ECL reference clock input connected to the TTC
       clock input on the 6-lemo TCDS FMC on L12. -->
  <node id="meas_ecl_refclk">
    <node id="reset" address="0x00000012" mask="0x00001000" permission="rw" />
    <node id="enable" address="0x00000013" mask="0x00001000" permission="rw" />
    <node id="disconnected" address="0x00000015" mask="0x00001000" permission="rw" />
    <node id="data_valid" address="0x00000003" mask="0x00004000" permission="rw" />
    <node id="result" address="0x0000001c" mask="0xffffffff" permission="r" />
  </node>

  <!-- Signal input: optical reference clock connected to SFP8 RX on
       the 8-SFP TCDS FMC on L8. -->
  <node id="meas_sfp_refclk">
    <node id="reset" address="0x00000012" mask="0x00002000" permission="rw" />
    <node id="enable" address="0x00000013" mask="0x00002000" permission="rw" />
    <node id="disconnected" address="0x00000015" mask="0x00002000" permission="rw" />
    <node id="data_valid" address="0x00000003" mask="0x00008000" permission="rw" />
    <node id="result" address="0x0000001d" mask="0xffffffff" permission="r" />
  </node>

  <!-- One of the signals from SFP1-7 RX can be sent to SFP8 TX for
       decoding on the TTC FMC. This register controls the signal
       selection. -->
  <node id="ttc_fmc_signal_select" address="0x00000014" mask="0x00000007" permission="rw"
        description="Signal selection from SFP1-7 RX to SFP8 TX to be forwarded to the TTC FMC input. Values 0-6 respectively forward SFP1-7 RX." />

  <!-- Configure which of the SFP1-7 RX signals are 40 MHz clocks, and
       which are TTC streams. This determines the phase measurement
       method. -->
  <node id="signal_type_config" address="0x00000014" permission="rw"
        description="Signal type selection (determines the phase measurement method): 0 -> 40 MHz clock, 1 -> TTC stream.">
    <node id="meas_sfp1" address="0x00000000" mask="0x00000010" permission="rw" />
    <node id="meas_sfp2" address="0x00000000" mask="0x00000020" permission="rw" />
    <node id="meas_sfp3" address="0x00000000" mask="0x00000040" permission="rw" />
    <node id="meas_sfp4" address="0x00000000" mask="0x00000080" permission="rw" />
    <node id="meas_sfp5" address="0x00000000" mask="0x00000100" permission="rw" />
    <node id="meas_sfp6" address="0x00000000" mask="0x00000200" permission="rw" />
    <node id="meas_sfp7" address="0x00000000" mask="0x00000400" permission="rw" />
  </node>

  <!-- NOTE: SFP1-8 are on the 8-SFP TCDS FMC in the L8 position. -->
  <node id="autoscan" address="0x00000000">
    <node id="enable" address="0x00000014" mask="0x00010000" permission="rw" />
    <node id="meas_sfp1">
      <node id="result" address="0x00000101" mask="0xffffffff" permission="r" />
      <node id="timeout_ttc_fmc" address="0x00000100" mask="0x00000001" permission="r" />
      <node id="timeout_dmdt" address="0x00000100" mask="0x00000002" permission="r" />
      <node id="data_valid" address="0x00000100" mask="0x00000004" permission="r" />
      <node id="new_value" address="0x00000100" mask="0x00000008" permission="r" />
    </node>
    <node id="meas_sfp2">
      <node id="result" address="0x00000105" mask="0xffffffff" permission="r" />
      <node id="timeout_ttc_fmc" address="0x00000104" mask="0x00000001" permission="r" />
      <node id="timeout_dmdt" address="0x00000104" mask="0x00000002" permission="r" />
      <node id="data_valid" address="0x00000104" mask="0x00000004" permission="r" />
      <node id="new_value" address="0x00000104" mask="0x00000008" permission="r" />
    </node>
    <node id="meas_sfp3">
      <node id="result" address="0x00000109" mask="0xffffffff" permission="r" />
      <node id="timeout_ttc_fmc" address="0x00000108" mask="0x00000001" permission="r" />
      <node id="timeout_dmdt" address="0x00000108" mask="0x00000002" permission="r" />
      <node id="data_valid" address="0x00000108" mask="0x00000004" permission="r" />
      <node id="new_value" address="0x00000108" mask="0x00000008" permission="r" />
    </node>
    <node id="meas_sfp4">
      <node id="result" address="0x0000010d" mask="0xffffffff" permission="r" />
      <node id="timeout_ttc_fmc" address="0x0000010c" mask="0x00000001" permission="r" />
      <node id="timeout_dmdt" address="0x0000010c" mask="0x00000002" permission="r" />
      <node id="data_valid" address="0x0000010c" mask="0x00000004" permission="r" />
      <node id="new_value" address="0x0000010c" mask="0x00000008" permission="r" />
    </node>
    <node id="meas_sfp5">
      <node id="result" address="0x00000111" mask="0xffffffff" permission="r" />
      <node id="timeout_ttc_fmc" address="0x00000110" mask="0x00000001" permission="r" />
      <node id="timeout_dmdt" address="0x00000110" mask="0x00000002" permission="r" />
      <node id="data_valid" address="0x00000110" mask="0x00000004" permission="r" />
      <node id="new_value" address="0x00000110" mask="0x00000008" permission="r" />
    </node>
    <node id="meas_sfp6">
      <node id="result" address="0x00000115" mask="0xffffffff" permission="r" />
      <node id="timeout_ttc_fmc" address="0x00000114" mask="0x00000001" permission="r" />
      <node id="timeout_dmdt" address="0x00000114" mask="0x00000002" permission="r" />
      <node id="data_valid" address="0x00000114" mask="0x00000004" permission="r" />
      <node id="new_value" address="0x00000114" mask="0x00000008" permission="r" />
    </node>
    <node id="meas_sfp7">
      <node id="result" address="0x00000119" mask="0xffffffff" permission="r" />
      <node id="timeout_ttc_fmc" address="0x00000118" mask="0x00000001" permission="r" />
      <node id="timeout_dmdt" address="0x00000118" mask="0x000000002" permission="r" />
      <node id="data_valid" address="0x00000118" mask="0x00000004" permission="r" />
      <node id="new_value" address="0x00000118" mask="0x00000008" permission="r" />
    </node>

    <node id="meas_fclka">
      <node id="result" address="0x0000011d" mask="0xffffffff" permission="r" />
      <node id="timeout_dmdt" address="0x0000011c" mask="0x00000002" permission="r" />
      <node id="data_valid" address="0x0000011c" mask="0x00000004" permission="r" />
      <node id="new_value" address="0x0000011c" mask="0x00000008" permission="r" />
    </node>

    <!-- NOTE: There is a gap here. This is where, in the firmware,
         the registers corresponding to the TTC FMC phase measurement
         live. -->

    <node id="meas_ecl_ttcorb">
      <node id="result" address="0x00000125" mask="0xffffffff" permission="r" />
      <node id="timeout_dmdt" address="0x00000124" mask="0x00000002" permission="r" />
      <node id="data_valid" address="0x00000124" mask="0x00000004" permission="r" />
      <node id="new_value" address="0x00000124" mask="0x00000008" permission="r" />
    </node>

    <node id="meas_ecl_tr0">
      <node id="result" address="0x00000129" mask="0xffffffff" permission="r" />
      <node id="timeout_dmdt" address="0x00000128" mask="0x00000002" permission="r" />
      <node id="data_valid" address="0x00000128" mask="0x00000004" permission="r" />
      <node id="new_value" address="0x00000128" mask="0x00000008" permission="r" />
    </node>

    <node id="meas_ecl_tr1">
      <node id="result" address="0x0000012d" mask="0xffffffff" permission="r" />
      <node id="timeout_dmdt" address="0x0000012c" mask="0x00000002" permission="r" />
      <node id="data_valid" address="0x0000012c" mask="0x00000004" permission="r" />
      <node id="new_value" address="0x0000012c" mask="0x00000008" permission="r" />
    </node>

    <node id="meas_ecl_refclk">
      <node id="result" address="0x00000131" mask="0xffffffff" permission="r" />
      <node id="timeout_dmdt" address="0x00000130" mask="0x00000002" permission="r" />
      <node id="data_valid" address="0x00000130" mask="0x00000004" permission="r" />
      <node id="new_value" address="0x00000130" mask="0x00000008" permission="r" />
    </node>

    <node id="meas_sfp_refclk">
      <node id="result" address="0x00000135" mask="0xffffffff" permission="r" />
      <node id="timeout_dmdt" address="0x00000134" mask="0x00000002" permission="r" />
      <node id="data_valid" address="0x00000134" mask="0x00000004" permission="r" />
      <node id="new_value" address="0x00000134" mask="0x00000008" permission="r" />
    </node>

  </node>

  <!-- The phase shifting control of the TTC PLL. -->
  <node id="phase_shift">
    <!-- Phase-shift control signals. -->
    <node id="direction" address="0x00000010" mask="0x02000000" permission="rw"
          description="Direction of the phase shift: 1=increase, 0=decrease." />
    <node id="strobe" address="0x00000010" mask="0x01000000" permission="rw"
          description="Each 0->1 transition shifts the clocks by approx. 22.3 ps." />
    <!-- Phase-shift status signals. -->
    <node id="done" address="0x00000001" mask="0x00000010" permission="r"
          description="A 1 declares that the phase shift was successful." />
  </node>

</node>
