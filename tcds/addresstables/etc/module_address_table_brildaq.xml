<?xml version="1.0" encoding="UTF-8"?>

<!-- NOTE: For efficiency reasons, this should probably only be read
out as a big block, and decoded in software. Having the
individual pieces is easy for debugging though. -->
<node id="brildaq" mode="block" size="448" permission="r"
      description="The basic CPM BRILDAQ data block.">

  <!-- BRILDAQ packet header. -->
  <node id="system_id" address="0x00000000" permission="r"
        description="Should say 'TCDS'." />
  <node id="lumi_section_number" address="0x00000001" permission="r"
        description="The lumi-section number of the current data packet." />
  <node id="lumi_nibble_number" address="0x00000002" mask="0x000000ff" permission="r"
        description="The lumi-nibble number of the current data packet." />
  <node id="cms_run_active" address="0x00000002" mask="0x00000100" permission="r"
        description="The lumi-nibble number of the current data packet." />
  <node id="num_nibbles_per_section" address="0x00000002" mask="0x0000f000" permission="r"
        description="The number of lumi-nibbles that go into each lumi section." />
  <node id="num_orbits" address="0x00000002" mask="0xffff0000" permission="r"
        description="The number of orbits that were integrated into this nibble ." />
  <node id="orbit_number" address="0x00000003" permission="r"
        description="The orbit number in which the last lumi-nibble B-go was received." />
  <node id="run_number" address="0x00000004" permission="r"
        description="The CMS run number.">
    <node id="lo" address="0x00000000" permission="r" />
    <node id="hi" address="0x00000001" permission="r" />
  </node>
  <node id="deadtime_bx_count_total" address="0x00000006" permission="r"
        description="The overall, total deadtime BX count." />
  <node id="deadtime_bx_count_tts" address="0x00000007" permission="r"
        description="The total TTS deadtime BX count." />

  <!-- Event counts that passed all rules and selections, by trigger type. -->
  <node id="event_count_trigtype0" address="0x00000008" permission="r" />
  <node id="event_count_trigtype1" address="0x00000009" permission="r" />
  <node id="event_count_trigtype2" address="0x0000000a" permission="r" />
  <node id="event_count_trigtype3" address="0x0000000b" permission="r" />
  <node id="event_count_trigtype4" address="0x0000000c" permission="r" />
  <node id="event_count_trigtype5" address="0x0000000d" permission="r" />
  <node id="event_count_trigtype6" address="0x0000000e" permission="r" />
  <node id="event_count_trigtype7" address="0x0000000f" permission="r" />
  <node id="event_count_trigtype8" address="0x00000010" permission="r" />
  <node id="event_count_trigtype9" address="0x00000011" permission="r" />
  <node id="event_count_trigtype10" address="0x00000012" permission="r" />
  <node id="event_count_trigtype11" address="0x00000013" permission="r" />
  <node id="event_count_trigtype12" address="0x00000014" permission="r" />
  <node id="event_count_trigtype13" address="0x00000015" permission="r" />
  <node id="event_count_trigtype14" address="0x00000016" permission="r" />
  <node id="event_count_trigtype15" address="0x00000017" permission="r" />

  <!-- Event counts that did not pass all rules and selections, by trigger type. -->
  <node id="suppressed_event_count_trigtype0" address="0x0000018" permission="r" />
  <node id="suppressed_event_count_trigtype1" address="0x0000019" permission="r" />
  <node id="suppressed_event_count_trigtype2" address="0x0000001a" permission="r" />
  <node id="suppressed_event_count_trigtype3" address="0x0000001b" permission="r" />
  <node id="suppressed_event_count_trigtype4" address="0x0000001c" permission="r" />
  <node id="suppressed_event_count_trigtype5" address="0x0000001d" permission="r" />
  <node id="suppressed_event_count_trigtype6" address="0x0000001e" permission="r" />
  <node id="suppressed_event_count_trigtype7" address="0x0000001f" permission="r" />
  <node id="suppressed_event_count_trigtype8" address="0x00000020" permission="r" />
  <node id="suppressed_event_count_trigtype9" address="0x00000021" permission="r" />
  <node id="suppressed_event_count_trigtype10" address="0x00000022" permission="r" />
  <node id="suppressed_event_count_trigtype11" address="0x00000023" permission="r" />
  <node id="suppressed_event_count_trigtype12" address="0x00000024" permission="r" />
  <node id="suppressed_event_count_trigtype13" address="0x00000025" permission="r" />
  <node id="suppressed_event_count_trigtype14" address="0x00000026" permission="r" />
  <node id="suppressed_event_count_trigtype15" address="0x00000027" permission="r" />

  <!--
  The timestamp lives in the first 8 bytes of the BST message:
    - The first four bytes contain the number of microseconds.
    - The second four bytes contain the integer number of seconds
      since 01-01-1970.
  -->
  <node id="bst_timestamp" address="0x00000028">
    <node id="microseconds" address="0x00000000" mask="0xffffffff" permission="r" />
    <node id="seconds" address="0x00000001" mask="0xffffffff" permission="r" />
  </node>

  <node id="bst_byte0" address="0x00000028" mask="0x000000ff" permission="r" />
  <node id="bst_byte1" address="0x00000028" mask="0x0000ff00" permission="r" />
  <node id="bst_byte2" address="0x00000028" mask="0x00ff0000" permission="r" />
  <node id="bst_byte3" address="0x00000028" mask="0xff000000" permission="r" />
  <node id="bst_byte4" address="0x00000029" mask="0x000000ff" permission="r" />
  <node id="bst_byte5" address="0x00000029" mask="0x0000ff00" permission="r" />
  <node id="bst_byte6" address="0x00000029" mask="0x00ff0000" permission="r" />
  <node id="bst_byte7" address="0x00000029" mask="0xff000000" permission="r" />
  <node id="bst_byte8" address="0x0000002a" mask="0x000000ff" permission="r" />
  <node id="bst_byte9" address="0x0000002a" mask="0x0000ff00" permission="r" />
  <node id="bst_byte10" address="0x0000002a" mask="0x00ff0000" permission="r" />
  <node id="bst_byte11" address="0x0000002a" mask="0xff000000" permission="r" />
  <node id="bst_byte12" address="0x0000002b" mask="0x000000ff" permission="r" />
  <node id="bst_byte13" address="0x0000002b" mask="0x0000ff00" permission="r" />
  <node id="bst_byte14" address="0x0000002b" mask="0x00ff0000" permission="r" />
  <node id="bst_byte15" address="0x0000002b" mask="0xff000000" permission="r" />
  <node id="bst_byte16" address="0x0000002c" mask="0x000000ff" permission="r" />
  <node id="bst_byte17" address="0x0000002c" mask="0x0000ff00" permission="r" />
  <node id="bst_byte18" address="0x0000002c" mask="0x00ff0000" permission="r" />
  <node id="bst_byte19" address="0x0000002c" mask="0xff000000" permission="r" />
  <node id="bst_byte20" address="0x0000002d" mask="0x000000ff" permission="r" />
  <node id="bst_byte21" address="0x0000002d" mask="0x0000ff00" permission="r" />
  <node id="bst_byte22" address="0x0000002d" mask="0x00ff0000" permission="r" />
  <node id="bst_byte23" address="0x0000002d" mask="0xff000000" permission="r" />
  <node id="bst_byte24" address="0x0000002e" mask="0x000000ff" permission="r" />
  <node id="bst_byte25" address="0x0000002e" mask="0x0000ff00" permission="r" />
  <node id="bst_byte26" address="0x0000002e" mask="0x00ff0000" permission="r" />
  <node id="bst_byte27" address="0x0000002e" mask="0xff000000" permission="r" />
  <node id="bst_byte28" address="0x0000002f" mask="0x000000ff" permission="r" />
  <node id="bst_byte29" address="0x0000002f" mask="0x0000ff00" permission="r" />
  <node id="bst_byte30" address="0x0000002f" mask="0x00ff0000" permission="r" />
  <node id="bst_byte31" address="0x0000002f" mask="0xff000000" permission="r" />
  <node id="bst_byte32" address="0x00000030" mask="0x000000ff" permission="r" />
  <node id="bst_byte33" address="0x00000030" mask="0x0000ff00" permission="r" />
  <node id="bst_byte34" address="0x00000030" mask="0x00ff0000" permission="r" />
  <node id="bst_byte35" address="0x00000030" mask="0xff000000" permission="r" />
  <node id="bst_byte36" address="0x00000031" mask="0x000000ff" permission="r" />
  <node id="bst_byte37" address="0x00000031" mask="0x0000ff00" permission="r" />
  <node id="bst_byte38" address="0x00000031" mask="0x00ff0000" permission="r" />
  <node id="bst_byte39" address="0x00000031" mask="0xff000000" permission="r" />
  <node id="bst_byte40" address="0x00000032" mask="0x000000ff" permission="r" />
  <node id="bst_byte41" address="0x00000032" mask="0x0000ff00" permission="r" />
  <node id="bst_byte42" address="0x00000032" mask="0x00ff0000" permission="r" />
  <node id="bst_byte43" address="0x00000032" mask="0xff000000" permission="r" />
  <node id="bst_byte44" address="0x00000033" mask="0x000000ff" permission="r" />
  <node id="bst_byte45" address="0x00000033" mask="0x0000ff00" permission="r" />
  <node id="bst_byte46" address="0x00000033" mask="0x00ff0000" permission="r" />
  <node id="bst_byte47" address="0x00000033" mask="0xff000000" permission="r" />
  <node id="bst_byte48" address="0x00000034" mask="0x000000ff" permission="r" />
  <node id="bst_byte49" address="0x00000034" mask="0x0000ff00" permission="r" />
  <node id="bst_byte50" address="0x00000034" mask="0x00ff0000" permission="r" />
  <node id="bst_byte51" address="0x00000034" mask="0xff000000" permission="r" />
  <node id="bst_byte52" address="0x00000035" mask="0x000000ff" permission="r" />
  <node id="bst_byte53" address="0x00000035" mask="0x0000ff00" permission="r" />
  <node id="bst_byte54" address="0x00000035" mask="0x00ff0000" permission="r" />
  <node id="bst_byte55" address="0x00000035" mask="0xff000000" permission="r" />
  <node id="bst_byte56" address="0x00000036" mask="0x000000ff" permission="r" />
  <node id="bst_byte57" address="0x00000036" mask="0x0000ff00" permission="r" />
  <node id="bst_byte58" address="0x00000036" mask="0x00ff0000" permission="r" />
  <node id="bst_byte59" address="0x00000036" mask="0xff000000" permission="r" />
  <node id="bst_byte60" address="0x00000037" mask="0x000000ff" permission="r" />
  <node id="bst_byte61" address="0x00000037" mask="0x0000ff00" permission="r" />
  <node id="bst_byte62" address="0x00000037" mask="0x00ff0000" permission="r" />
  <node id="bst_byte63" address="0x00000037" mask="0xff000000" permission="r" />

  <!-- The BST reception status is appended by the CPM firmware, and
       not part of the BST message itself. -->
  <node id="bst_status" address="0x00000038" mask="0xffffffff" permission="r" />

  <node id="deadtime_bx_counts_tts" address="0x00000039" permission="r">
    <node id="lpm1" address="0x00000000" permission="r">
      <node id="ici1" address="0x00000000" permission="r" />
      <node id="ici2" address="0x00000001" permission="r" />
      <node id="ici3" address="0x00000002" permission="r" />
      <node id="ici4" address="0x00000003" permission="r" />
      <node id="ici5" address="0x00000004" permission="r" />
      <node id="ici6" address="0x00000005" permission="r" />
      <node id="ici7" address="0x00000006" permission="r" />
      <node id="ici8" address="0x00000007" permission="r" />
      <node id="apve1" address="0x00000008" permission="r" />
      <node id="apve2" address="0x00000009" permission="r" />
      <node id="apve3" address="0x0000000a" permission="r" />
      <node id="apve4" address="0x0000000b" permission="r" />
    </node>
    <node id="lpm2" address="0x00000010" permission="r">
      <node id="ici1" address="0x00000000" permission="r" />
      <node id="ici2" address="0x00000001" permission="r" />
      <node id="ici3" address="0x00000002" permission="r" />
      <node id="ici4" address="0x00000003" permission="r" />
      <node id="ici5" address="0x00000004" permission="r" />
      <node id="ici6" address="0x00000005" permission="r" />
      <node id="ici7" address="0x00000006" permission="r" />
      <node id="ici8" address="0x00000007" permission="r" />
      <node id="apve1" address="0x00000008" permission="r" />
      <node id="apve2" address="0x00000009" permission="r" />
      <node id="apve3" address="0x0000000a" permission="r" />
      <node id="apve4" address="0x0000000b" permission="r" />
    </node>
    <node id="lpm3" address="0x00000020" permission="r">
      <node id="ici1" address="0x00000000" permission="r" />
      <node id="ici2" address="0x00000001" permission="r" />
      <node id="ici3" address="0x00000002" permission="r" />
      <node id="ici4" address="0x00000003" permission="r" />
      <node id="ici5" address="0x00000004" permission="r" />
      <node id="ici6" address="0x00000005" permission="r" />
      <node id="ici7" address="0x00000006" permission="r" />
      <node id="ici8" address="0x00000007" permission="r" />
      <node id="apve1" address="0x00000008" permission="r" />
      <node id="apve2" address="0x00000009" permission="r" />
      <node id="apve3" address="0x0000000a" permission="r" />
      <node id="apve4" address="0x0000000b" permission="r" />
    </node>
    <node id="lpm4" address="0x00000030" permission="r">
      <node id="ici1" address="0x00000000" permission="r" />
      <node id="ici2" address="0x00000001" permission="r" />
      <node id="ici3" address="0x00000002" permission="r" />
      <node id="ici4" address="0x00000003" permission="r" />
      <node id="ici5" address="0x00000004" permission="r" />
      <node id="ici6" address="0x00000005" permission="r" />
      <node id="ici7" address="0x00000006" permission="r" />
      <node id="ici8" address="0x00000007" permission="r" />
      <node id="apve1" address="0x00000008" permission="r" />
      <node id="apve2" address="0x00000009" permission="r" />
      <node id="apve3" address="0x0000000a" permission="r" />
      <node id="apve4" address="0x0000000b" permission="r" />
    </node>
    <node id="lpm5" address="0x00000040" permission="r">
      <node id="ici1" address="0x00000000" permission="r" />
      <node id="ici2" address="0x00000001" permission="r" />
      <node id="ici3" address="0x00000002" permission="r" />
      <node id="ici4" address="0x00000003" permission="r" />
      <node id="ici5" address="0x00000004" permission="r" />
      <node id="ici6" address="0x00000005" permission="r" />
      <node id="ici7" address="0x00000006" permission="r" />
      <node id="ici8" address="0x00000007" permission="r" />
      <node id="apve1" address="0x00000008" permission="r" />
      <node id="apve2" address="0x00000009" permission="r" />
      <node id="apve3" address="0x0000000a" permission="r" />
      <node id="apve4" address="0x0000000b" permission="r" />
    </node>
    <node id="lpm6" address="0x00000050" permission="r">
      <node id="ici1" address="0x00000000" permission="r" />
      <node id="ici2" address="0x00000001" permission="r" />
      <node id="ici3" address="0x00000002" permission="r" />
      <node id="ici4" address="0x00000003" permission="r" />
      <node id="ici5" address="0x00000004" permission="r" />
      <node id="ici6" address="0x00000005" permission="r" />
      <node id="ici7" address="0x00000006" permission="r" />
      <node id="ici8" address="0x00000007" permission="r" />
      <node id="apve1" address="0x00000008" permission="r" />
      <node id="apve2" address="0x00000009" permission="r" />
      <node id="apve3" address="0x0000000a" permission="r" />
      <node id="apve4" address="0x0000000b" permission="r" />
    </node>
    <node id="lpm7" address="0x00000060" permission="r">
      <node id="ici1" address="0x00000000" permission="r" />
      <node id="ici2" address="0x00000001" permission="r" />
      <node id="ici3" address="0x00000002" permission="r" />
      <node id="ici4" address="0x00000003" permission="r" />
      <node id="ici5" address="0x00000004" permission="r" />
      <node id="ici6" address="0x00000005" permission="r" />
      <node id="ici7" address="0x00000006" permission="r" />
      <node id="ici8" address="0x00000007" permission="r" />
      <node id="apve1" address="0x00000008" permission="r" />
      <node id="apve2" address="0x00000009" permission="r" />
      <node id="apve3" address="0x0000000a" permission="r" />
      <node id="apve4" address="0x0000000b" permission="r" />
    </node>
    <node id="lpm8" address="0x00000070" permission="r">
      <node id="ici1" address="0x00000000" permission="r" />
      <node id="ici2" address="0x00000001" permission="r" />
      <node id="ici3" address="0x00000002" permission="r" />
      <node id="ici4" address="0x00000003" permission="r" />
      <node id="ici5" address="0x00000004" permission="r" />
      <node id="ici6" address="0x00000005" permission="r" />
      <node id="ici7" address="0x00000006" permission="r" />
      <node id="ici8" address="0x00000007" permission="r" />
      <node id="apve1" address="0x00000008" permission="r" />
      <node id="apve2" address="0x00000009" permission="r" />
      <node id="apve3" address="0x0000000a" permission="r" />
      <node id="apve4" address="0x0000000b" permission="r" />
    </node>
    <node id="lpm9" address="0x00000080" permission="r">
      <node id="ici1" address="0x00000000" permission="r" />
      <node id="ici2" address="0x00000001" permission="r" />
      <node id="ici3" address="0x00000002" permission="r" />
      <node id="ici4" address="0x00000003" permission="r" />
      <node id="ici5" address="0x00000004" permission="r" />
      <node id="ici6" address="0x00000005" permission="r" />
      <node id="ici7" address="0x00000006" permission="r" />
      <node id="ici8" address="0x00000007" permission="r" />
      <node id="apve1" address="0x00000008" permission="r" />
      <node id="apve2" address="0x00000009" permission="r" />
      <node id="apve3" address="0x0000000a" permission="r" />
      <node id="apve4" address="0x0000000b" permission="r" />
    </node>
    <node id="lpm10" address="0x00000090" permission="r">
      <node id="ici1" address="0x00000000" permission="r" />
      <node id="ici2" address="0x00000001" permission="r" />
      <node id="ici3" address="0x00000002" permission="r" />
      <node id="ici4" address="0x00000003" permission="r" />
      <node id="ici5" address="0x00000004" permission="r" />
      <node id="ici6" address="0x00000005" permission="r" />
      <node id="ici7" address="0x00000006" permission="r" />
      <node id="ici8" address="0x00000007" permission="r" />
      <node id="apve1" address="0x00000008" permission="r" />
      <node id="apve2" address="0x00000009" permission="r" />
      <node id="apve3" address="0x0000000a" permission="r" />
      <node id="apve4" address="0x0000000b" permission="r" />
    </node>
    <node id="lpm11" address="0x000000a0" permission="r">
      <node id="ici1" address="0x00000000" permission="r" />
      <node id="ici2" address="0x00000001" permission="r" />
      <node id="ici3" address="0x00000002" permission="r" />
      <node id="ici4" address="0x00000003" permission="r" />
      <node id="ici5" address="0x00000004" permission="r" />
      <node id="ici6" address="0x00000005" permission="r" />
      <node id="ici7" address="0x00000006" permission="r" />
      <node id="ici8" address="0x00000007" permission="r" />
      <node id="apve1" address="0x00000008" permission="r" />
      <node id="apve2" address="0x00000009" permission="r" />
      <node id="apve3" address="0x0000000a" permission="r" />
      <node id="apve4" address="0x0000000b" permission="r" />
    </node>
    <node id="lpm12" address="0x000000b0" permission="r">
      <node id="ici1" address="0x00000000" permission="r" />
      <node id="ici2" address="0x00000001" permission="r" />
      <node id="ici3" address="0x00000002" permission="r" />
      <node id="ici4" address="0x00000003" permission="r" />
      <node id="ici5" address="0x00000004" permission="r" />
      <node id="ici6" address="0x00000005" permission="r" />
      <node id="ici7" address="0x00000006" permission="r" />
      <node id="ici8" address="0x00000007" permission="r" />
      <node id="apve1" address="0x00000008" permission="r" />
      <node id="apve2" address="0x00000009" permission="r" />
      <node id="apve3" address="0x0000000a" permission="r" />
      <node id="apve4" address="0x0000000b" permission="r" />
    </node>
  </node>

  <!-- Dead-time split out by source. -->
  <node id="deadtime_bx_count_trigger_rules" address="0x000000101" permission="r" />
  <node id="deadtime_bx_count_bunch_mask_veto" address="0x0000000fa" permission="r" />
  <node id="deadtime_bx_count_retri" address="0x0000000fb" permission="r" />
  <node id="deadtime_bx_count_apve" address="0x0000000fc" permission="r" />
  <node id="deadtime_bx_count_daq_backpressure" address="0x0000000fd" permission="r" />

  <node id="deadtime_bx_count_calibration" address="0x0000000fe" permission="r" />
  <node id="deadtime_bx_count_sw_pause" address="0x0000000ff" permission="r" />
  <node id="deadtime_bx_count_fw_pause" address="0x000000100" permission="r" />

  <!-- Dead-time due to trigger rules split out by rule. -->
  <node id="deadtime_bx_count_trigger_rule1" address="0x000000102" permission="r" />
  <node id="deadtime_bx_count_trigger_rule2" address="0x000000103" permission="r" />
  <node id="deadtime_bx_count_trigger_rule3" address="0x000000104" permission="r" />
  <node id="deadtime_bx_count_trigger_rule4" address="0x000000105" permission="r" />
  <node id="deadtime_bx_count_trigger_rule5" address="0x000000106" permission="r" />
  <node id="deadtime_bx_count_trigger_rule6" address="0x000000107" permission="r" />
  <node id="deadtime_bx_count_trigger_rule7" address="0x000000108" permission="r" />
  <node id="deadtime_bx_count_trigger_rule8" address="0x000000109" permission="r" />
  <node id="deadtime_bx_count_trigger_rule9" address="0x00000010a" permission="r" />
  <node id="deadtime_bx_count_trigger_rule10" address="0x00000010b" permission="r" />
  <node id="deadtime_bx_count_trigger_rule11" address="0x00000010c" permission="r" />
  <node id="deadtime_bx_count_trigger_rule12" address="0x00000010d" permission="r" />
  <node id="deadtime_bx_count_trigger_rule13" address="0x00000010e" permission="r" />
  <node id="deadtime_bx_count_trigger_rule14" address="0x00000010f" permission="r" />
  <node id="deadtime_bx_count_trigger_rule15" address="0x000000110" permission="r" />

  <!-- A bunch-crossing counter. Handy to have this here in the same packet. -->
  <node id="bx_count" address="0x000000114" permission="r" />

  <!-- The suppressed trigger counts, split out by deadtime source. -->
  <node id="suppressed_event_counts_by_deadtime_source" address="0x0000115" permission="r">
    <node id="tts"
          module="file://${XDAQ_ROOT}/etc/tcds/addresstables/module_address_table_counts_by_trigtype.xml"
          address="0x00000000" />
    <node id="trigger_rules"
          module="file://${XDAQ_ROOT}/etc/tcds/addresstables/module_address_table_counts_by_trigtype.xml"
          address="0x00000010" />
    <node id="bunch_mask"
          module="file://${XDAQ_ROOT}/etc/tcds/addresstables/module_address_table_counts_by_trigtype.xml"
          address="0x00000020" />
    <node id="retri"
          module="file://${XDAQ_ROOT}/etc/tcds/addresstables/module_address_table_counts_by_trigtype.xml"
          address="0x00000030" />
    <node id="apve"
          module="file://${XDAQ_ROOT}/etc/tcds/addresstables/module_address_table_counts_by_trigtype.xml"
          address="0x00000040" />
    <node id="daq_backpressure"
          module="file://${XDAQ_ROOT}/etc/tcds/addresstables/module_address_table_counts_by_trigtype.xml"
          address="0x00000050" />
    <node id="daq_backpressure"
          module="file://${XDAQ_ROOT}/etc/tcds/addresstables/module_address_table_counts_by_trigtype.xml"
          address="0x00000050" />
    <node id="calibration"
          module="file://${XDAQ_ROOT}/etc/tcds/addresstables/module_address_table_counts_by_trigtype.xml"
          address="0x00000060" />
    <node id="sw_pause"
          module="file://${XDAQ_ROOT}/etc/tcds/addresstables/module_address_table_counts_by_trigtype.xml"
          address="0x00000070" />
    <node id="fw_pause"
          module="file://${XDAQ_ROOT}/etc/tcds/addresstables/module_address_table_counts_by_trigtype.xml"
          address="0x00000080" />
  </node>

  <!-- BRILDAQ packet footer. -->
  <!-- NOTE: These variables are repeated here (from the header) to
       enable checking that the whole block has been read
       correctly. -->
  <!-- <node id="system_id_check" address="0x0000013c" permission="r" /> -->
  <!-- <node id="lumi_section_number_check" address="0x0000013d" permission="r" /> -->
  <!-- <node id="lumi_nibble_number_check" address="0x0000013e" mask="0x000000ff" permission="r" /> -->
  <!-- <node id="orbit_number_check" address="0x0000013f" permission="r" /> -->
  <node id="system_id_check" address="0x000001bc" permission="r" />
  <node id="lumi_section_number_check" address="0x000001bd" permission="r" />
  <node id="lumi_nibble_number_check" address="0x000001be" mask="0x000000ff" permission="r" />
  <node id="orbit_number_check" address="0x000001bf" permission="r" />

</node>
