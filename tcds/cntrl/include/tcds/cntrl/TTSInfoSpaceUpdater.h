#ifndef _tcds_cntrl_TTSInfoSpaceUpdater_h_
#define _tcds_cntrl_TTSInfoSpaceUpdater_h_

#include <map>
#include <string>

#include "xdata/Serializable.h"
#include "xdata/String.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/Table.h"
#include "xdata/TableIterator.h"

#include "tcds/utils/InfoSpaceUpdater.h"
#include "tcds/utils/Utils.h"

namespace tcds {
  namespace utils {
    class InfoSpaceHandler;
    class InfoSpaceItem;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace cntrl {

    class TCDSCentral;

    class TTSInfoSpaceUpdater : public tcds::utils::InfoSpaceUpdater
    {

    public:
      TTSInfoSpaceUpdater(tcds::cntrl::TCDSCentral& xdaqApp,
                          std::string const& groupName);
      virtual ~TTSInfoSpaceUpdater();

    protected:
      virtual void updateInfoSpaceImpl(tcds::utils::InfoSpaceHandler* const infoSpaceHandler);
      virtual bool updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                       tcds::utils::InfoSpaceHandler* const infoSpaceHandler);

      tcds::utils::XDAQAppBase& getOwnerApplication() const;

    private:

      //----------

      // Helper to sort the TTS info from all the TCDS applications.
      // NOTE: This basically works straight on the rows of the
      // flashlist table. This means it strongly depends on the
      // definition of the flashlist itself.
      class compareHelper {

      public:
        compareHelper()
        {
          ttsPriorities_ = tcds::utils::ttsPriorities();
        }

        bool operator() (xdata::TableIterator lhs,
                         xdata::TableIterator rhs) const
        {
          // First preference: sort by 'TTS source type.'
          xdata::Serializable* tmpLhs = lhs->getField("type");
          xdata::Serializable* tmpRhs = rhs->getField("type");
          std::string const lhType = *dynamic_cast<xdata::String*>(tmpLhs);
          std::string const rhType = *dynamic_cast<xdata::String*>(tmpRhs);
          if (lhType != rhType)
            {
              return ttsPriorities_[lhType] < ttsPriorities_[rhType];
            }

          // Second option: sort by PM number and ID number.
          // NOTE: Ideally one would sort by application context, but
          // I have not found a way to obtain that information here.
          tmpLhs = lhs->getField("pm_number");
          tmpRhs = rhs->getField("pm_number");
          unsigned int const lhPMNumber = *dynamic_cast<xdata::UnsignedInteger32*>(tmpLhs);
          unsigned int const rhPMNumber = *dynamic_cast<xdata::UnsignedInteger32*>(tmpRhs);
          if (lhPMNumber != rhPMNumber)
            {
              return lhPMNumber < rhPMNumber;
            }
          else
            {
              tmpLhs = lhs->getField("id_number");
              tmpRhs = rhs->getField("id_number");
              unsigned int const lhIDNumber = *dynamic_cast<xdata::UnsignedInteger32*>(tmpLhs);
              unsigned int const rhIDNumber = *dynamic_cast<xdata::UnsignedInteger32*>(tmpRhs);
              if (lhIDNumber != rhIDNumber)
                {
                  return lhIDNumber < rhIDNumber;
                }
            }

          // Last resort: sort by label. (Should not be needed, really.)
          tmpLhs = lhs->getField("label");
          tmpRhs = rhs->getField("label");
          std::string const lhLabel = *dynamic_cast<xdata::String*>(tmpLhs);
          std::string const rhLabel = *dynamic_cast<xdata::String*>(tmpRhs);
          return (lhLabel < rhLabel);
        }

      private:
        // NOTE: The mutable is so that we can call operator[]() on
        // the map from a const method. This does not truly affect the
        // state of the object itself.
        mutable std::map<std::string, int> ttsPriorities_;

      };

      //----------

      xdata::Table getTTSInfo() const;
      std::string getTTSString(std::string const& serviceName);

      std::string const groupName_;
      xdata::Table ttsInfo_;
      bool shouldCheckConfig_;
      std::string lasURL_;
      std::string flashlistName_;

    };

  } // namespace cntrl
} // namespace tcds

#endif // _tcds_cntrl_TTSInfoSpaceUpdater_h_
