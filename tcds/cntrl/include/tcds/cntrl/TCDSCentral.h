#ifndef _tcds_cntrl_TCDSCentral_h_
#define _tcds_cntrl_TCDSCentral_h_

#include <memory>
#include <string>
#include <vector>

#include "toolbox/exception/Listener.h"
#include "xdaq/Application.h"
#include "xoap/MessageReference.h"

#include "tcds/utils/XDAQAppBase.h"

namespace toolbox {
  namespace task {
    class WorkLoop;
  }
}

namespace xcept {
  class Exception;
}

namespace xdaq {
  class ApplicationStub;
}

namespace tcds {
  namespace cntrl {

    class AppsInfoSpaceHandler;
    class AppsInfoSpaceUpdater;
    class InfraInfoSpaceHandler;
    class InfraInfoSpaceUpdater;
    class TTSInfoSpaceHandler;
    class TTSInfoSpaceUpdater;

    /**
     * The TCDSCentral is the central monitoring application to keep
     * an eye on all TCDS control applications. The XDAQ application
     * itself is really only used to provide a common interface.
     */
    class TCDSCentral :
      public toolbox::exception::Listener,
      public tcds::utils::XDAQAppBase
    {

    public:
      XDAQ_INSTANTIATOR();

      TCDSCentral(xdaq::ApplicationStub* const stub);
      virtual ~TCDSCentral();

      // The toolbox::exception::Listener callback.
      virtual void onException(xcept::Exception& err);

      xoap::MessageReference dumpSystemState(xoap::MessageReference const msg);

    protected:
      virtual void setupInfoSpaces();

      // Dummy methods in this case.
      virtual void hwConnectImpl();
      virtual void hwReleaseImpl();
      virtual void hwConfigureImpl();

      bool dumpSystemStateCore(toolbox::task::WorkLoop* wl);

    private:
      // Various InfoSpaces and their InfoSpaceUpdaters.
      // NOTE: We can't use unique_ptrs here, since we want to stuff
      // things into an STL vector.
      std::vector<AppsInfoSpaceUpdater*> appsInfoSpaceUpdaterPs_;
      std::vector<AppsInfoSpaceHandler*> appsInfoSpacePs_;
      std::unique_ptr<InfraInfoSpaceUpdater> infraInfoSpaceUpdaterP_;
      std::unique_ptr<InfraInfoSpaceHandler> infraInfoSpaceP_;
      std::vector<TTSInfoSpaceUpdater*> ttsInfoSpaceUpdaterPs_;
      std::vector<TTSInfoSpaceHandler*> ttsInfoSpacePs_;

      // A helper variable for the 'system dumps.'
      std::string dumpReason_;

    };

  } // namespace cntrl
} // namespace tcds

#endif // _tcds_cntrl_TCDSCentral_h_
