#ifndef _tcds_cntrl_InfraInfoSpaceUpdater_h_
#define _tcds_cntrl_InfraInfoSpaceUpdater_h_

#include <string>

#include "xdata/Table.h"

#include "tcds/utils/InfoSpaceUpdater.h"

namespace tcds {
  namespace utils {
    class InfoSpaceHandler;
    class InfoSpaceItem;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace cntrl {

    class TCDSCentral;

    class InfraInfoSpaceUpdater : public tcds::utils::InfoSpaceUpdater
    {

    public:
      InfraInfoSpaceUpdater(tcds::cntrl::TCDSCentral& xdaqApp);
      virtual ~InfraInfoSpaceUpdater();

    protected:
      virtual void updateInfoSpaceImpl(tcds::utils::InfoSpaceHandler* const infoSpaceHandler);
      virtual bool updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                       tcds::utils::InfoSpaceHandler* const infoSpaceHandler);

      tcds::utils::XDAQAppBase& getOwnerApplication() const;

    private:
      xdata::Table getInfraInfo() const;
      std::string getInfraInfoString();

      xdata::Table parseHeartbeatJSON(std::string const& json) const;

    //   std::string const groupName_;
      xdata::Table infraInfo_;
    //   bool shouldCheckConfig_;
    //   std::string lasURL_;
    //   std::string flashlistName_;

    };

  } // namespace cntrl
} // namespace tcds

#endif // _tcds_cntrl_InfraInfoSpaceUpdater_h_
