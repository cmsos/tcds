#ifndef _tcds_cntrl_TTSInfoSpaceHandler_h_
#define _tcds_cntrl_TTSInfoSpaceHandler_h_

#include <string>
#include <vector>

#include "tcds/utils/InfoSpaceHandler.h"

namespace xdaq {
  class Application;
}

namespace tcds {
  namespace utils {
    class InfoSpaceUpdater;
    class Monitor;
    class WebServer;
  }
}

namespace tcds {
  namespace cntrl {

    class TTSInfoSpaceHandler : public tcds::utils::InfoSpaceHandler
    {

    public:
      TTSInfoSpaceHandler(xdaq::Application& xdaqApp,
                           tcds::utils::InfoSpaceUpdater* updater,
                           std::string const& groupName);
      virtual ~TTSInfoSpaceHandler();

    protected:
      virtual void registerItemSetsWithMonitor(tcds::utils::Monitor& monitor);
      virtual void registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                 tcds::utils::Monitor& monitor,
                                                 std::string const& forceTabName="");

      virtual std::string formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const;

    private:
      std::string const groupName_;
      /* std::vector<std::string> itemsetNames_; */
      std::vector<std::string> serviceNames_;

    };

  } // namespace cntrl
} // namespace tcds

#endif // _tcds_cntrl_TTSInfoSpaceHandler_h_
