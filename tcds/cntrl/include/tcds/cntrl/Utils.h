#ifndef _tcds_cntrl_Utils_h_
#define _tcds_cntrl_Utils_h_

namespace toolbox {
  class Properties;
}

namespace xdaq {
  class ApplicationDescriptor;
}

namespace tcds {
  namespace cntrl {

    // Helpers to sort XDAQ applications.

    // Sort XDAQ applications by port number, host, and then local-id.
    // NOTE: This sounds a bit counter-intuitive, but it works out
    // quite well with the naming/numbering scheme used in the TCDS.
    class compareApplications
    {

    public:
      bool operator() (xdaq::ApplicationDescriptor const* const& lhs,
                       xdaq::ApplicationDescriptor const* const& rhs) const;
    };

    // Sort XDAQ applications by class name, port number, host, and
    // then local-id. This works better than the above for the XDAQ
    // infrastructure applications.
    class compareApplicationsGrouped
    {

    public:
      bool operator() (toolbox::Properties const& lhs,
                       toolbox::Properties const& rhs) const;

    };

  } // namespace cntrl
} // namespace tcds

#endif // _tcds_cntrl_Utils_h_
