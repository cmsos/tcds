#ifndef _tcds_cntrl_InfraInfoSpaceHandler_h_
#define _tcds_cntrl_InfraInfoSpaceHandler_h_

#include <string>

#include "tcds/utils/InfoSpaceHandler.h"

namespace xdaq {
  class Application;
}

namespace tcds {
  namespace utils {
    class InfoSpaceUpdater;
    class Monitor;
    class WebServer;
  }
}

namespace tcds {
  namespace cntrl {

    class InfraInfoSpaceHandler : public tcds::utils::InfoSpaceHandler
    {

    public:
      InfraInfoSpaceHandler(xdaq::Application& xdaqApp,
                            tcds::utils::InfoSpaceUpdater* updater);
      virtual ~InfraInfoSpaceHandler();

    protected:
      virtual void registerItemSetsWithMonitor(tcds::utils::Monitor& monitor);
      virtual void registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                 tcds::utils::Monitor& monitor,
                                                 std::string const& forceTabName="");

      virtual std::string formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const;

    };

  } // namespace cntrl
} // namespace tcds

#endif // _tcds_cntrl_InfraInfoSpaceHandler_h_
