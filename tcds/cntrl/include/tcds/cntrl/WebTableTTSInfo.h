#ifndef _tcds_cntrl_WebTableTTSInfo_h
#define _tcds_cntrl_WebTableTTSInfo_h

#include <cstddef>
#include <string>

#include "tcds/utils/WebObject.h"

namespace tcds {
  namespace utils {
    class Monitor;
  }
}

namespace tcds {
  namespace cntrl {

    class WebTableTTSInfo : public tcds::utils::WebObject
    {

    public:
      WebTableTTSInfo(std::string const& name,
                       std::string const& description,
                       tcds::utils::Monitor const& monitor,
                       std::string const& itemSetName,
                       std::string const& tabName,
                       size_t const colSpan);

      std::string getHTMLString() const;

    };

  } // namespace cntrl
} // namespace tcds

#endif // _tcds_cntrl_WebTableTTSInfo_h
