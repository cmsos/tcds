#include "tcds/cntrl/Utils.h"

#include <string>

#include "toolbox/net/URL.h"
#include "toolbox/Properties.h"
#include "xdaq/ApplicationDescriptor.h"
#include "xdaq/ContextDescriptor.h"

bool
tcds::cntrl::compareApplications::operator() (xdaq::ApplicationDescriptor const* const& lhs,
                                              xdaq::ApplicationDescriptor const* const& rhs) const
{
  toolbox::net::URL const lhu = toolbox::net::URL(lhs->getContextDescriptor()->getURL());
  toolbox::net::URL const rhu = toolbox::net::URL(rhs->getContextDescriptor()->getURL());
  unsigned int const lhp = lhu.getPort();
  unsigned int const rhp = rhu.getPort();
  if (lhp != rhp)
    {
      return (lhp < rhp);
    }
  else
    {
      std::string const lhh = lhu.getHost();
      std::string const rhh = rhu.getHost();
      if (lhh != rhh)
        {
          return (lhh < rhh);
        }
      else
        {
          if (lhs->getLocalId() != rhs->getLocalId())
            {
              return (lhs->getLocalId() < rhs->getLocalId());
            }
          else
            {
              // If nothing else works: sort the raw pointers
              // by value.
              return (lhs < rhs);
            }
        }
    }
}

bool
tcds::cntrl::compareApplicationsGrouped::operator() (toolbox::Properties const& lhs,
                                                     toolbox::Properties const& rhs) const
{
  // NOTE: On has to make copies here, because
  // toolbox::Properties::getProperty() is not const.
  toolbox::Properties lhsLocal(lhs);
  toolbox::Properties rhsLocal(rhs);

  std::string const lhc = lhsLocal.getProperty("class");
  std::string const rhc = rhsLocal.getProperty("class");
  if (lhc != rhc)
    {
      return (lhc < rhc);
    }
  else
    {
      toolbox::net::URL const lhu = toolbox::net::URL(lhsLocal.getProperty("url"));
      toolbox::net::URL const rhu = toolbox::net::URL(rhsLocal.getProperty("url"));

      unsigned int const lhp = lhu.getPort();
      unsigned int const rhp = rhu.getPort();
      if (lhp != rhp)
        {
          return (lhp < rhp);
        }
      else
        {
          std::string const lhh = lhu.getHost();
          std::string const rhh = rhu.getHost();
          if (lhh != rhh)
            {
              return (lhh < rhh);
            }
          else
            {
              std::string const lhi = lhsLocal.getProperty("id");
              std::string const rhi = rhsLocal.getProperty("id");
              return (lhi < rhi);
            }
        }
    }
}
