#include "tcds/cntrl/InfraInfoSpaceUpdater.h"

#include <set>
#include <cstddef>
#include <map>
#include <vector>

#include "jansson.h"

#include "toolbox/Properties.h"
#include "toolbox/string.h"
#include "toolbox/TimeVal.h"
#include "xcept/Exception.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationDescriptor.h"
#include "xdaq/ApplicationGroup.h"
#include "xdaq/ContextDescriptor.h"
#include "xdaq/Zone.h"
#include "xdata/Double.h"
#include "xdata/Serializable.h"
#include "xdata/String.h"
#include "xdata/Table.h"
#include "xdata/TableIterator.h"
#include "xdata/TimeVal.h"
#include "xplore/Advertisement.h"
#include "xplore/Interface.h"

#include "tcds/utils/CurlGetter.h"
#include "tcds/utils/XDAQAppBase.h"
#include "tcds/cntrl/TCDSCentral.h"
#include "tcds/cntrl/Utils.h"
#include "tcds/exception/Exception.h"
#include "tcds/utils/ConfigurationInfoSpaceHandler.h"
#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Utils.h"

tcds::cntrl::InfraInfoSpaceUpdater::InfraInfoSpaceUpdater(tcds::cntrl::TCDSCentral& xdaqApp) :
  tcds::utils::InfoSpaceUpdater(xdaqApp)
{
}

tcds::cntrl::InfraInfoSpaceUpdater::~InfraInfoSpaceUpdater()
{
}

tcds::utils::XDAQAppBase&
tcds::cntrl::InfraInfoSpaceUpdater::getOwnerApplication() const
{
  return static_cast<tcds::utils::XDAQAppBase&>(tcds::utils::InfoSpaceUpdater::getOwnerApplication());
}

void
tcds::cntrl::InfraInfoSpaceUpdater::updateInfoSpaceImpl(tcds::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  infraInfo_ = getInfraInfo();

  // Then do what we normally do.
  tcds::utils::InfoSpaceUpdater::updateInfoSpaceImpl(infoSpaceHandler);
}

bool
tcds::cntrl::InfraInfoSpaceUpdater::updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                                        tcds::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  bool updated = false;
  std::string const name = item.name();
  tcds::utils::InfoSpaceItem::UpdateType const updateType = item.updateType();
  if (updateType == tcds::utils::InfoSpaceItem::PROCESS)
    {
      // The 'PROCESS' update type means that there is something
      // special to the variable. Figure out what to do based on
      // the variable name.
      if (name == "infra_info")
        {
          std::string const newVal = getInfraInfoString();
          infoSpaceHandler->setString(name, newVal);
          updated = true;
        }
    }
  if (!updated)
    {
      updated = tcds::utils::InfoSpaceUpdater::updateInfoSpaceItem(item, infoSpaceHandler);
    }
  if (updated)
    {
      item.setValid();
    }
  return updated;
}

xdata::Table
tcds::cntrl::InfraInfoSpaceUpdater::getInfraInfo() const
{
  // Get the information about all XDAQ applications from the
  // heartbeat server.
  std::string const heartbeatURL =
    getOwnerApplication().getConfigurationInfoSpaceHandler().getString("heartbeatURL")
    + "/retrieveHeartbeatTable";

  // Get the list of all XDAQ applications from the heartbeat server.
  std::string appJson;
  tcds::utils::CurlGetter getter;
  try
    {
      appJson = getter.get(heartbeatURL);
    }
  catch (xdaq::exception::Exception& err)
    {
      std::string const msg =
        toolbox::toString("Failed to contact the xmas heartbeat application: '%s'.",
                          err.message().c_str());
      XCEPT_RAISE(tcds::exception::RuntimeProblem, msg);
    }

  // Convert the above JSON reply into something easily traversible.
  xdata::Table appTable;
  try
    {
      appTable = parseHeartbeatJSON(appJson);
    }
  catch (xdaq::exception::Exception& err)
    {
      std::string const msg =
        toolbox::toString("Failed to parse the JSON reply from the xmas heartbeat application: '%s'.",
                          err.message().c_str());
      XCEPT_RAISE(tcds::exception::RuntimeProblem, msg);
    }

  return appTable;
}

std::string
tcds::cntrl::InfraInfoSpaceUpdater::getInfraInfoString()
{

  // Extract a list of 'infrastructure applications' from the full
  // application list.
  std::set<toolbox::Properties, tcds::cntrl::compareApplicationsGrouped> infraProperties;
  for (xdata::Table::iterator row = infraInfo_.begin();
       row != infraInfo_.end();
       ++row)
    {
      toolbox::Properties properties;

      std::string const className = row->getField("class")->toString();
      std::string const context = row->getField("context")->toString();
      std::string const service = row->getField("service")->toString();
      std::string const id = row->getField("id")->toString();
      std::string const urn = "urn:xdaq-application:lid=" + id;
      std::string const url = context + "/" + urn;
      std::string const iconURL = context + "/" + row->getField("icon")->toString();

      properties.setProperty("class", className);
      properties.setProperty("context", context);
      properties.setProperty("id", id);
      properties.setProperty("service", service);
      properties.setProperty("url", url);
      properties.setProperty("icon", iconURL);

      infraProperties.insert(properties);
    }

  //----------

  // Loop over all found applications and collect some info.
  std::string res;
  for (std::set<toolbox::Properties>::iterator i = infraProperties.begin();
       i != infraProperties.end();
       ++i)
    {
      // NOTE: One has to make a copy here, since
      // toolbox::Properties::getProperty() is not const...
      toolbox::Properties prop(*i);

      // Filter out the TCDS applications. They have their own
      // AppsInfoSpaceHandler. Also filter out all '::probe::'
      // applications. There are many of these, and they are not of
      // prime interest.
      std::string const className = prop.getProperty("class");
      if (!toolbox::startsWith(className, "tcds::")
          && (className.find("::probe::") == std::string::npos))
        {
          // If we're not on the first line, close the previous line
          // with a comma.
          if (!res.empty())
            {
              res += ", ";
            }

          // Start of JSON object.
          res += "{";

          // Application/class name.
          res +=
            tcds::utils::escapeAsJSONString("class") +
            ": " +
            tcds::utils::escapeAsJSONString(className);

          // Application service name.
          std::string const serviceName = prop.getProperty("service");
          res +=
            ", " +
            tcds::utils::escapeAsJSONString("service") +
            ": " +
            tcds::utils::escapeAsJSONString(serviceName);

          // Full application URL.
          std::string const context = prop.getProperty("context");
          std::string const lidStr = prop.getProperty("id");
          std::string const urn = "urn:xdaq-application:lid=" + lidStr;
          std::string const fullUrl = context + "/" + urn;
          res +=
            ", " +
            tcds::utils::escapeAsJSONString("url") +
            ": " +
            tcds::utils::escapeAsJSONString(fullUrl);

          // Application-icon URL.
          std::string const iconUrl = prop.getProperty("icon");
          res +=
            ", " +
            tcds::utils::escapeAsJSONString("iconUrl") +
            ": " +
            tcds::utils::escapeAsJSONString(iconUrl);

          // End of JSON object.
          res += "}";
        }
    }

  res = "[" + res + "]";

  return res;
}

xdata::Table
tcds::cntrl::InfraInfoSpaceUpdater::parseHeartbeatJSON(std::string const& json) const
{
  xdata::Table appTable;

  json_error_t error;
  json_t* root = json_loads(json.c_str(), 0, &error);
  if (!root)
    {
      std::string const msg =
        toolbox::toString("Failed to parse JSON reply from heartbeat server."
                          " Failed at '%s' on line %d.",
                          error.text,
                          error.line);
      XCEPT_RAISE(tcds::exception::RuntimeProblem, msg);
    }

  try
    {
      if (!json_is_object(root))
        {
          std::string const msg = "Failed to parse JSON reply from heartbeat server."
            " Expected to receive an object but found something else.";
          XCEPT_RAISE(tcds::exception::RuntimeProblem, msg);
        }

      json_t* table = json_object_get(root, "table");
      if (!json_is_object(table))
        {
          std::string const msg = "Failed to parse JSON reply from heartbeat server."
            " Expected to receive a top-level 'table' object.";
          XCEPT_RAISE(tcds::exception::RuntimeProblem, msg);
        }

      // Find the table definition.
      json_t* definition = json_object_get(table, "definition");
      if (!json_is_array(definition))
        {
          std::string const msg = "Failed to parse JSON reply from heartbeat server."
            " Could not find the table 'definition' object.";
          XCEPT_RAISE(tcds::exception::RuntimeProblem, msg);
        }

      // Loop over column definitions.
      for (size_t i = 0; i < json_array_size(definition); ++i)
        {
          json_t* colDef = json_array_get(definition, i);
          if (!json_is_object(colDef))
            {
              std::string const msg =
                toolbox::toString("Failed to parse JSON reply from heartbeat server."
                                  " Definition for column %d is not an object.",
                                  i);
              XCEPT_RAISE(tcds::exception::RuntimeProblem, msg);
            }
          json_t* colKey = json_object_get(colDef, "key");
          if (!json_is_string(colKey))
            {
              std::string const msg =
                toolbox::toString("Failed to parse JSON reply from heartbeat server."
                                  " Key for column %d is not a string.",
                                  i);
              XCEPT_RAISE(tcds::exception::RuntimeProblem, msg);
            }
          json_t* colType = json_object_get(colDef, "type");
          if (!json_is_string(colType))
            {
              std::string const msg =
                toolbox::toString("Failed to parse JSON reply from heartbeat server."
                                  " Type for column %d is not a string.",
                                  i);
              XCEPT_RAISE(tcds::exception::RuntimeProblem, msg);
            }
          std::string const key = json_string_value(colKey);
          std::string const type = json_string_value(colType);
          appTable.addColumn(key, type);
        }

      // Now find the table data.
      json_t* rows = json_object_get(table, "rows");
      if (!json_is_array(rows))
        {
          std::string const msg = "Failed to parse JSON reply from heartbeat server."
            " Could not find the table 'rows' object.";
          XCEPT_RAISE(tcds::exception::RuntimeProblem, msg);
        }

      // Loop over the table rows. For each row, use the table we have
      // just defined to find and process all columns.
      std::map<std::string, std::string, xdata::Table::ci_less>
        tableDef = appTable.getTableDefinition();
      for (size_t i = 0; i < json_array_size(rows); ++i)
        {
          json_t* row = json_array_get(rows, i);
          xdata::Table::iterator it = appTable.append();

          for (std::map<std::string, std::string>::const_iterator
                 rowDef = tableDef.begin();
               rowDef != tableDef.end();
               ++rowDef)
            {
              std::string const key = rowDef->first;
              std::string const type = rowDef->second;

              json_t* tmp = json_object_get(row, key.c_str());

              xdata::Serializable* tmpSer = 0;
              if (type == "string")
                {
                  if (json_is_string(tmp))
                    {
                      std::string const tmpVal = json_string_value(tmp);
                      tmpSer = new xdata::String(tmpVal);
                    }
                }
              else if (type == "double")
                {
                  if (json_is_real(tmp))
                    {
                      double const tmpVal = json_real_value(tmp);
                      tmpSer = new xdata::Double(tmpVal);
                    }
                }
              else
                {
                  std::string const msg =
                    toolbox::toString("Failed to parse JSON reply from heartbeat server."
                                      " Type '%s' (for column '%s') is not supported.",
                                      type.c_str(),
                                      key.c_str());
                  XCEPT_RAISE(tcds::exception::RuntimeProblem, msg);
                }
              if (!tmpSer)
                {
                  std::string const msg =
                    toolbox::toString("Failed to parse JSON reply from heartbeat server."
                                      " Value for row %d, column '%s' does not match its type.",
                                      i,
                                      key.c_str());
                  XCEPT_RAISE(tcds::exception::RuntimeProblem, msg);
                }
              else
                {
                  it->setField(key, *tmpSer);
                  delete tmpSer;
                  tmpSer = 0;
                }
            }
        }
    }
  catch (xcept::Exception& err)
    {
      json_decref(root);
      throw;
    }
  json_decref(root);

  return appTable;
}
