#include "tcds/cntrl/AppsInfoSpaceHandler.h"

#include "toolbox/string.h"

#include "tcds/cntrl/WebTableAppsInfo.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/Utils.h"
#include "tcds/utils/WebServer.h"

tcds::cntrl::AppsInfoSpaceHandler::AppsInfoSpaceHandler(xdaq::Application& xdaqApp,
                                                        tcds::utils::InfoSpaceUpdater* updater,
                                                        std::string const& groupName) :
  InfoSpaceHandler(xdaqApp, toolbox::toString("tcds-tcdscentral-apps-%s", groupName.c_str()), updater),
  groupName_(groupName)
{
  // This will hold state information for all TCDS control applications.
  createString("apps_info", "", "json-object");
}

tcds::cntrl::AppsInfoSpaceHandler::~AppsInfoSpaceHandler()
{
}

void
tcds::cntrl::AppsInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  std::string const itemSetName = toolbox::toString("itemset-appsinfo-%s", groupName_.c_str());
  monitor.newItemSet(itemSetName);
  monitor.addItem(itemSetName,
                  "apps_info",
                  "Status of applications",
                  this);
}

void
tcds::cntrl::AppsInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                 tcds::utils::Monitor& monitor,
                                                                 std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? toolbox::toString("TCDS '%s' applications", groupName_.c_str()) : forceTabName;

  webServer.registerTab(tabName,
                        toolbox::toString("Overview of all TCDS '%s' applications", groupName_.c_str()),
                        1);
  webServer.registerWebObject<WebTableAppsInfo>("Application overview",
                                                "",
                                                monitor,
                                                toolbox::toString("itemset-appsinfo-%s", groupName_.c_str()),
                                                tabName);
}

std::string
tcds::cntrl::AppsInfoSpaceHandler::formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const
{
  std::string res = tcds::utils::escapeAsJSONString(tcds::utils::InfoSpaceHandler::kInvalidItemString);
  if (item->isValid())
    {
      std::string name = item->name();
      if (name == "apps_info")
        {
          // Special in the sense that this is something that needs to
          // be interpreted as a proper JavaScript object, so let's
          // not add any more double quotes.
          res = getString(name);
        }
      else
        {
          // For everything else simply call the generic formatter.
          res = InfoSpaceHandler::formatItem(item);
        }
    }
  return res;
}
