#include "tcds/cntrl/WebTableAppsInfo.h"

#include <cassert>
#include <sstream>
#include <string>

#include "tcds/utils/ApplicationStateInfoSpaceHandler.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/WebObject.h"

tcds::cntrl::WebTableAppsInfo::WebTableAppsInfo(std::string const& name,
                                                std::string const& description,
                                                tcds::utils::Monitor const& monitor,
                                                std::string const& itemSetName,
                                                std::string const& tabName,
                                                size_t const colSpan) :
tcds::utils::WebObject(name, description, monitor, itemSetName, tabName, colSpan)
{
}

std::string
tcds::cntrl::WebTableAppsInfo::getHTMLString() const
{
  // NOTE: This has been written to work with the new XDAQ12-style
  // HyperDAQ tabs and doT.js.

  std::stringstream res;

  res << "<div class=\"tcds-item-table-wrapper\">"
      << "\n";

  res << "<p class=\"tcds-item-table-title\">"
      << getName()
      << "</p>";
  res << "\n";

  res << "<p class=\"tcds-item-table-description\">"
      << getDescription()
      << "</p>";
  res << "\n";

  // And now the actual table contents.
  tcds::utils::Monitor::StringPairVector items =
    monitor_.getFormattedItemSet(itemSetName_);

  // ASSERT ASSERT ASSERT
  assert (items.size() == 1);
  // ASSERT ASSERT ASSERT end

  std::string const itemName = items.at(0).first;
  std::string const tmp = "[\"" + itemSetName_ + "\"][\"" + itemName + "\"]";
  std::string const invalidVal = tcds::utils::WebObject::kStringInvalidItem;

  res << "<script type=\"text/x-dot-template\">";
  // Case 0: invalid item ('-').
  res << "{{? it" << tmp << " == '" << invalidVal << "'}}"
      << "<span>" << invalidVal << "</span>"
      << "\n";
  // Case 1: a (hopefully non-empty) array.
  res << "{{??}}"
      << "\n"
      << "<table class=\"xdaq-table tcds-item-table\">"
      << "\n"
      << "<thead>"
      << "<tr>"
      << "<th></th>"
      << "<th>Service</th>"
      << "<th>URL</th>"
      << "<th>Hardware lease owner</th>"
      << "<th>RCMS session ID</th>"
      << "<th>FSM state</th>"
      << "<th>Application state</th>"
      << "<th>Problem description</th>"
      << "<th>Latest update (UTC)</th>"
      << "</tr>"
      << "</thead>"
      << "\n"
      << "<tbody>"
      << "\n"
      << "{{~it" << tmp << " :value:index}}"
      << "\n"
      << "<tr>"
      << "\n"
      << "<td><img class=\"thumb\" src=\"{{=value[\"iconUrl\"]}}\"/></td>"
      << "<td>{{=value[\"service\"]}}</td>"
      << "<td><a href=\"{{=value[\"url\"]}}\" target=\"_blank\"><span>{{=value[\"url\"]}}</span></a></td>"
      << "<td>{{=value[\"hwLeaseOwnerId\"]}}</td>"
      << "<td>{{=value[\"rcmsSessionId\"]}}</td>"
      << "<td class=\"emph-{{=value[\"stateName\"].toLowerCase()}}\">{{=value[\"stateName\"]}}</td>"
      << "<td class=\"emph-{{?value[\"applicationState\"]==='"
      << tcds::utils::ApplicationStateInfoSpaceHandler::kAllOKString
      << "'}}good{{??}}bad{{?}}\">{{=value[\"applicationState\"]}}</td>"
      << "<td class=\"emph-{{?value[\"problemDescription\"]==='"
      << tcds::utils::ApplicationStateInfoSpaceHandler::kNoProblemString
      << "'}}good{{??}}bad{{?}}\">{{=value[\"problemDescription\"]}}</td>"
      << "<td>{{=value[\"timestamp\"]}}</td>"
      << "\n"
      << "</tr>"
      << "\n"
      << "{{~}}"
      << "\n"
      << "</tbody>"
      << "\n"
      << "</table>"
      << "\n";
  res << "{{?}}"
      << "\n";
  res << "</script>"
      << "\n"
      << "<div class=\"target\">" << kDefaultValueString << "</div>"
      << "\n";

  return res.str();
}
