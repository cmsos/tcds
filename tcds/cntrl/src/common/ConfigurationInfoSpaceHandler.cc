#include "tcds/cntrl/ConfigurationInfoSpaceHandler.h"

#include <string>
#include <vector>

#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/WebServer.h"

tcds::cntrl::ConfigurationInfoSpaceHandler::ConfigurationInfoSpaceHandler(xdaq::Application& xdaqApp) :
  tcds::utils::ConfigurationInfoSpaceHandler(xdaqApp)
{
  // This one is for internal use only. It is a vector of strings
  // telling us the service groups of applications to monitor.
  createStringVec("serviceGroups",
                  std::vector<std::string>(),
                  "",
                  tcds::utils::InfoSpaceItem::NOUPDATE,
                  true);

  //----------

  // Where/how to find the flashlists.
  createStringVec("lasURLs",
                  std::vector<std::string>(),
                  "",
                  tcds::utils::InfoSpaceItem::NOUPDATE,
                  true);
  createString("flashlistNameCommon",
               "tcds_common",
               "",
               tcds::utils::InfoSpaceItem::NOUPDATE,
               true);
  createString("flashlistNamePMTTS",
               "tcds_pm_tts_channel",
               "",
               tcds::utils::InfoSpaceItem::NOUPDATE,
               true);

  //----------

  // Where to find the xmas::heartbeat server.
  createString("heartbeatURL",
               "",
               "",
               tcds::utils::InfoSpaceItem::NOUPDATE,
               true);
}

void
tcds::cntrl::ConfigurationInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  std::string itemSetName = "itemset-app-config";
  monitor.newItemSet(itemSetName);
  monitor.addItem(itemSetName,
                  "lasURLs",
                  "LAS URLs to search for the below flashlists",
                  this);
  monitor.addItem(itemSetName,
                  "flashlistNameCommon",
                  "Flashlist name for common information from all TCDS applications",
                  this);
  monitor.addItem(itemSetName,
                  "flashlistNamePMTTS",
                  "Flashlist name for per-channel TTS information from CPM-/LPMController applications",
                  this);

  monitor.addItem(itemSetName,
                  "heartbeatURL",
                  "URL of the xmas::heartbeat server. Used to discover XDAQ infrastructure applications.",
                  this);
}

void
tcds::cntrl::ConfigurationInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                          tcds::utils::Monitor& monitor,
                                                                          std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "Configuration" : forceTabName;

  webServer.registerTab(tabName,
                        "Configuration parameters",
                        2);
  webServer.registerTable("Application configuration",
                          "Application configuration parameters",
                          monitor,
                          "itemset-app-config",
                          tabName);
}
