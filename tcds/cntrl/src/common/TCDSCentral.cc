#include "tcds/cntrl/TCDSCentral.h"

#include <algorithm>
#include <exception>
#include <list>
#include <memory>
#include <sstream>
#include <stdio.h>

#include "toolbox/net/URL.h"
#include "toolbox/string.h"
#include "toolbox/task/Action.h"
#include "toolbox/task/WorkLoop.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "xcept/Exception.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/NamespaceURI.h"
#include "xdaq/exception/ApplicationInstantiationFailed.h"
#include "xoap/Method.h"

#include "tcds/cntrl/AppsInfoSpaceHandler.h"
#include "tcds/cntrl/AppsInfoSpaceUpdater.h"
#include "tcds/cntrl/ConfigurationInfoSpaceHandler.h"
#include "tcds/cntrl/InfraInfoSpaceHandler.h"
#include "tcds/cntrl/InfraInfoSpaceUpdater.h"
#include "tcds/cntrl/TTSInfoSpaceHandler.h"
#include "tcds/cntrl/TTSInfoSpaceUpdater.h"
#include "tcds/exception/Exception.h"
#include "tcds/hwlayer/DeviceBase.h"
#include "tcds/utils/LogMacros.h"
#include "tcds/utils/SOAPUtils.h"

XDAQ_INSTANTIATOR_IMPL(tcds::cntrl::TCDSCentral)

tcds::cntrl::TCDSCentral::TCDSCentral(xdaq::ApplicationStub* const stub)
try
  :
  tcds::utils::XDAQAppBase(stub, std::unique_ptr<tcds::hwlayer::DeviceBase>())
      {
        // Create the InfoSpace holding all configuration information.
        cfgInfoSpaceP_ =
          std::unique_ptr<ConfigurationInfoSpaceHandler>(new ConfigurationInfoSpaceHandler(*this));

        // Bind our special 'DumpSystemState' SOAP command.
        xoap::bind(this, &TCDSCentral::dumpSystemState, "DumpSystemState", XDAQ_NS_URI);
      }
catch (tcds::exception::Exception const& err)
  {
    std::string msgBase = "Something went wrong instantiating the TCDSCentral application";
    std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.what());
    XCEPT_RAISE(xdaq::exception::ApplicationInstantiationFailed, msg.c_str());
  }

tcds::cntrl::TCDSCentral::~TCDSCentral()
{
  for (std::vector<AppsInfoSpaceUpdater*>::iterator i = appsInfoSpaceUpdaterPs_.begin();
       i != appsInfoSpaceUpdaterPs_.end();
       ++i)
    {
      delete *i;
    }
  for (std::vector<AppsInfoSpaceHandler*>::iterator i = appsInfoSpacePs_.begin();
       i != appsInfoSpacePs_.end();
       ++i)
    {
      delete *i;
    }
}

void
tcds::cntrl::TCDSCentral::onException(xcept::Exception& err)
{
  std::string const msg =
    toolbox::toString("Failed to generate a system dump: '%s'.", err.what());
  appStateInfoSpace_.addHistoryItem(msg);
  ERROR(msg);
}

void
tcds::cntrl::TCDSCentral::setupInfoSpaces()
{
  // Make sure the configuration settings are up-to-date.
  cfgInfoSpaceP_->readInfoSpace();

  // Get the vector of service groups from the ConfigurationInfoSpaceHandler.
  std::vector<std::string> const serviceGroups = cfgInfoSpaceP_->getStringVec("serviceGroups");

  // Build all required InfoSpaceHandlers and -Updaters.
  // NOTE: We're doing some simple book keeping along the way.
  std::vector<std::string> alreadyDone;
  for (std::vector<std::string>::const_iterator i = serviceGroups.begin();
       i != serviceGroups.end();
       ++i)
    {
    if (i->empty())
      {
        WARN("Removed empty service group '" << *i << "' from TCDSCentral serviceGroups configuration.");
      }
    else
      {
      if (std::find(alreadyDone.begin(), alreadyDone.end(), *i) != alreadyDone.end())
        {
          WARN("Removed duplicate service group '" << *i << "' from TCDSCentral serviceGroups configuration.");
        }
      else
        {
          appsInfoSpaceUpdaterPs_.push_back(new AppsInfoSpaceUpdater(*this, *i));
          appsInfoSpacePs_.push_back(new AppsInfoSpaceHandler(*this, appsInfoSpaceUpdaterPs_.back(), *i));
          ttsInfoSpaceUpdaterPs_.push_back(new TTSInfoSpaceUpdater(*this, *i));
          ttsInfoSpacePs_.push_back(new TTSInfoSpaceHandler(*this, ttsInfoSpaceUpdaterPs_.back(), *i));

          alreadyDone.push_back(*i);
        }
      }
    }

  infraInfoSpaceUpdaterP_ =
    std::unique_ptr<InfraInfoSpaceUpdater>(new InfraInfoSpaceUpdater(*this));
  infraInfoSpaceP_ =
    std::unique_ptr<InfraInfoSpaceHandler>(new InfraInfoSpaceHandler(*this, infraInfoSpaceUpdaterP_.get()));

  // We don't have an FSM, so no state either.
  appStateInfoSpace_.setString("stateName", "n/a");
  // Similar for the hardware lease.
  appStateInfoSpace_.setString("hwLeaseOwnerId", "n/a");

  // Register all InfoSpaceItems with the Monitor.
  cfgInfoSpaceP_->registerItemSets(monitor_, webServer_);
  appStateInfoSpace_.registerItemSets(monitor_, webServer_);
  infraInfoSpaceP_->registerItemSets(monitor_, webServer_);
  for (std::vector<AppsInfoSpaceHandler*>::iterator i = appsInfoSpacePs_.begin();
       i != appsInfoSpacePs_.end();
       ++i)
    {
      (*i)->registerItemSets(monitor_, webServer_);
    }
  for (std::vector<TTSInfoSpaceHandler*>::iterator i = ttsInfoSpacePs_.begin();
       i != ttsInfoSpacePs_.end();
       ++i)
    {
      (*i)->registerItemSets(monitor_, webServer_);
    }
}

void
tcds::cntrl::TCDSCentral::hwConnectImpl()
{
}

void
tcds::cntrl::TCDSCentral::hwReleaseImpl()
{
}

void
tcds::cntrl::TCDSCentral::hwConfigureImpl()
{
}

xoap::MessageReference
tcds::cntrl::TCDSCentral::dumpSystemState(xoap::MessageReference const msg)
{
  xoap::MessageReference reply;

  bool good = false;
  std::string reason = "";

  try
    {
      // Extract the reason for the dump
      dumpReason_ = tcds::utils::soap::extractSOAPCommandParameterString(msg, "reason");

      toolbox::task::ActionSignature* as;
      as = toolbox::task::bind(this, &tcds::cntrl::TCDSCentral::dumpSystemStateCore, "dumpSystemStateCore");
      toolbox::task::WorkLoop* wl =
        toolbox::task::getWorkLoopFactory()->getWorkLoop("urn:xdaq-workloop:myapplication-test", "waiting");
      wl->addExceptionListener(this);
      if (!wl->isActive())
        {
          wl->activate();
        }
      wl->submit(as);
      good = true;
    }
  catch (xcept::Exception& err)
    {
      reason = err.what();
    }
  catch (std::exception const& err)
    {
      reason = err.what();
    }
  catch (...)
    {
      reason = "An unknown error occurred.";
    }

  if (good)
    {
      // Return success message.
      reply = tcds::utils::soap::makeCommandSOAPReply(msg);
    }
  else
    {
      // Return failure message.
      std::string const errMsg = toolbox::toString("Failed to execute system dump: '%s'.", reason.c_str());
      reply = tcds::utils::soap::makeSOAPFaultReply(this,
                                                    msg,
                                                    tcds::utils::soap::SOAPFaultCodeReceiver,
                                                    errMsg);
    }

  return reply;
}

bool
tcds::cntrl::TCDSCentral::dumpSystemStateCore(toolbox::task::WorkLoop* wl)
{
  // Call our TCDS system-dumper Python script to do the heavy lifting.
  getApplicationContext();
  getApplicationContext()->getContextDescriptor();
  getApplicationContext()->getContextDescriptor()->getURL();
  toolbox::net::URL const url = toolbox::net::URL(getApplicationContext()->getContextDescriptor()->getURL());
  std::string const hostName = url.getHost();
  unsigned int const portNumber = url.getPort();
  std::string const dumpReason = dumpReason_;
  std::string const cmd = toolbox::toString("/opt/xdaq/bin/tcds_system_dump.py --tcdscentral-host=\"%s\" --tcdscentral-port=\"%d\" --dump-reason=\"%s\" 2>&1",
                                            hostName.c_str(),
                                            portNumber,
                                            dumpReason.c_str());

  // Allow the application history to show what is going on.
  std::string const msg =
    toolbox::toString("Executing system dump requested by SOAP. (This takes a while.) Dump reason: '%s'.",
                      dumpReason.c_str());
  appStateInfoSpace_.addHistoryItem(msg);

  bool good = true;
  std::stringstream output;
  std::string err = "";
  FILE* input = 0;
  char buff[512];
  if (!(input = popen(cmd.c_str(), "r")))
    {
      good = false;
      err = toolbox::toString("Failed to spawn a shell to execute command '%s'.", cmd.c_str());
    }

  if (good)
    {
      while (fgets(buff, sizeof(buff), input) != 0)
        {
          output << buff;
        }
      int const status = pclose(input);
      good = (status == 0);
      err = toolbox::toString("Failed to execute command '%s'.", cmd.c_str());
    }

  if (good)
    {
      appStateInfoSpace_.addHistoryItem("Done executing system dump. See log for details.");
      INFO("----- System-dump command output begin -----");
      std::list<std::string> tmp = toolbox::parseTokenList(output.str(), "\n");
      for (std::list<std::string>::const_iterator i = tmp.begin();
           i != tmp.end();
           ++i)
        {
          INFO(*i);
        }
      INFO("----- System-dump command output end -----");
    }
  else
    {
      appStateInfoSpace_.addHistoryItem("Failed to execute system dump requested by SOAP. See log for details.");

      std::string const msg =
        toolbox::toString("Failed to execute system dump. %s",
                          err.c_str());
      ERROR(msg);
      ERROR(toolbox::toString("System-dump command: '%s'.", cmd.c_str()));
      ERROR("----- System-dump command output begin -----");
      std::list<std::string> tmp = toolbox::parseTokenList(output.str(), "\n");
      for (std::list<std::string>::const_iterator i = tmp.begin();
           i != tmp.end();
           ++i)
        {
          ERROR(*i);
        }
      ERROR("----- System-dump command output end -----");
      XCEPT_DECLARE(tcds::exception::RuntimeProblem, err, msg);
      notifyQualified("error", err);
    }
  return false;
}
