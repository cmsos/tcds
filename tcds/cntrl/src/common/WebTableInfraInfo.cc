#include "tcds/cntrl/WebTableInfraInfo.h"

#include <cassert>
#include <sstream>
#include <string>

#include "tcds/utils/ApplicationStateInfoSpaceHandler.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/WebObject.h"

tcds::cntrl::WebTableInfraInfo::WebTableInfraInfo(std::string const& name,
                                                  std::string const& description,
                                                  tcds::utils::Monitor const& monitor,
                                                  std::string const& itemSetName,
                                                  std::string const& tabName,
                                                  size_t const colSpan) :
  tcds::utils::WebObject(name, description, monitor, itemSetName, tabName, colSpan)
{
}

std::string
tcds::cntrl::WebTableInfraInfo::getHTMLString() const
{
  // NOTE: This has been written to work with the new XDAQ12-style
  // HyperDAQ tabs and doT.js.

  std::stringstream res;

  res << "<div class=\"tcds-item-table-wrapper\">"
      << "\n";

  res << "<p class=\"tcds-item-table-title\">"
      << getName()
      << "</p>";
  res << "\n";

  res << "<p class=\"tcds-item-table-description\">"
      << getDescription()
      << "</p>";
  res << "\n";

  // And now the actual table contents.
  tcds::utils::Monitor::StringPairVector items =
    monitor_.getFormattedItemSet(itemSetName_);

  // ASSERT ASSERT ASSERT
  assert (items.size() == 1);
  // ASSERT ASSERT ASSERT end

  std::string const itemName = items.at(0).first;
  std::string const tmp = "[\"" + itemSetName_ + "\"][\"" + itemName + "\"]";
  std::string const invalidVal = tcds::utils::WebObject::kStringInvalidItem;

  res << "<script type=\"text/x-dot-template\">";
  // Case 0: invalid item ('-').
  res << "{{? it" << tmp << " == '" << invalidVal << "'}}"
      << "<span>" << invalidVal << "</span>"
      << "\n";
  // Case 1: a (hopefully non-empty) array.
  res << "{{??}}"
      << "\n"
      << "<table class=\"xdaq-table tcds-item-table\">"
      << "\n"
      << "<thead>"
      << "<tr>"
      << "<th></th>"
      << "<th>Application</th>"
      << "<th>Service</th>"
      << "<th>URL</th>"
      << "</tr>"
      << "</thead>"
      << "\n"
      << "<tbody>"
      << "\n"
      << "{{~it" << tmp << " :value:index}}"
      << "\n"
      << "<tr>"
      << "\n"
      << "<td><img class=\"thumb\" src=\"{{=value[\"iconUrl\"]}}\"/></td>"
      << "<td>{{=value[\"class\"]}}</td>"
      << "<td>{{=value[\"service\"]}}</td>"
      << "<td><a href=\"{{=value[\"url\"]}}\" target=\"_blank\"><span>{{=value[\"url\"]}}</span></a></td>"
      << "\n"
      << "</tr>"
      << "\n"
      << "{{~}}"
      << "\n"
      << "</tbody>"
      << "\n"
      << "</table>"
      << "\n";
  res << "{{?}}"
      << "\n";
  res << "</script>"
      << "\n"
      << "<div class=\"target\">" << kDefaultValueString << "</div>"
      << "\n";

  return res.str();
}
