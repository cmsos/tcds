#include "tcds/cntrl/InfraInfoSpaceHandler.h"

#include "toolbox/string.h"

#include "tcds/cntrl/WebTableInfraInfo.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/Utils.h"
#include "tcds/utils/WebServer.h"

tcds::cntrl::InfraInfoSpaceHandler::InfraInfoSpaceHandler(xdaq::Application& xdaqApp,
                                                          tcds::utils::InfoSpaceUpdater* updater) :
  InfoSpaceHandler(xdaqApp, "tcds-tcdscentral-infra", updater)
{
  // This will hold state information for all non-TCDS XDAQ
  // applications in our zone.
  createString("infra_info", "", "json-object");
}

tcds::cntrl::InfraInfoSpaceHandler::~InfraInfoSpaceHandler()
{
}

void
tcds::cntrl::InfraInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  std::string const itemSetName = "itemset-infrainfo";
  monitor.newItemSet(itemSetName);
  monitor.addItem(itemSetName,
                  "infra_info",
                  "Non-TCDS XDAQ applications",
                  this);
}

void
tcds::cntrl::InfraInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                  tcds::utils::Monitor& monitor,
                                                                  std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "XDAQ infrastructure" : forceTabName;

  webServer.registerTab(tabName,
                        "Overview of all non-TCDS XDAQ applications in the current zone",
                        1);
  webServer.registerWebObject<WebTableInfraInfo>("Application overview",
                                                 "",
                                                 monitor,
                                                 "itemset-infrainfo",
                                                 tabName);
}

std::string
tcds::cntrl::InfraInfoSpaceHandler::formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const
{
  std::string res = tcds::utils::escapeAsJSONString(tcds::utils::InfoSpaceHandler::kInvalidItemString);
  if (item->isValid())
    {
      std::string name = item->name();
      if (name == "infra_info")
        {
          // Special in the sense that this is something that needs to
          // be interpreted as a proper JavaScript object, so let's
          // not add any more double quotes.
          res = getString(name);
        }
      else
        {
          // For everything else simply call the generic formatter.
          res = InfoSpaceHandler::formatItem(item);
        }
    }
  return res;
}
