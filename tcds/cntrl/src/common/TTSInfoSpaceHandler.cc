#include "tcds/cntrl/TTSInfoSpaceHandler.h"

#include <set>

#include "toolbox/string.h"
#include "xdaq/Application.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationDescriptor.h"
#include "xdaq/ApplicationGroup.h"
#include "xdaq/Zone.h"

#include "tcds/cntrl/Utils.h"
#include "tcds/cntrl/WebTableTTSInfo.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/Utils.h"
#include "tcds/utils/WebServer.h"

namespace xcept {
  class Exception;
}

tcds::cntrl::TTSInfoSpaceHandler::TTSInfoSpaceHandler(xdaq::Application& xdaqApp,
                                                      tcds::utils::InfoSpaceUpdater* updater,
                                                      std::string const& groupName) :
  InfoSpaceHandler(xdaqApp, toolbox::toString("tcds-tcdscentral-tts-%s", groupName.c_str()), updater),
  groupName_(groupName)
{
  // Find all desired applications, in our own zone, in the requested
  // group.
  std::string const zoneName = getOwnerApplication().getApplicationContext()->getDefaultZoneName();
  xdaq::ApplicationGroup const* group = 0;
  try
    {
      group = getOwnerApplication().getApplicationContext()->getZone(zoneName)->getApplicationGroup(groupName_);
    }
  catch (xcept::Exception& err)
    {
      // This is probably no big deal. The application group probably
      // does not exist or so.
    }

  std::set<xdaq::ApplicationDescriptor const*, tcds::cntrl::compareApplications> descriptors;
  if (group)
    {
  std::set<xdaq::ApplicationDescriptor const*> const descriptorsCPM =
    group->getApplicationDescriptors("tcds::cpm::CPMController");
  std::set<xdaq::ApplicationDescriptor const*> const descriptorsLPM =
    group->getApplicationDescriptors("tcds::lpm::LPMController");
  descriptors.insert(descriptorsCPM.begin(), descriptorsCPM.end());
  descriptors.insert(descriptorsLPM.begin(), descriptorsLPM.end());
  }

  for (std::set<xdaq::ApplicationDescriptor const*>::const_iterator i = descriptors.begin();
       i != descriptors.end();
       ++i)
    {
      std::string serviceName = "unknown";
      std::string const tmp = (*i)->getAttribute("service");
      if (!tmp.empty())
        {
          serviceName = tmp;
        }

      // Keep track of the service names for later.
      serviceNames_.push_back(serviceName);
    }

  //----------

  // We have found all CPM- and LPMControllers. Now follows the actual
  // item creation.

  for (std::vector<std::string>::const_iterator i = serviceNames_.begin();
       i != serviceNames_.end();
       ++i)
    {
      std::string const serviceName = *i;
      std::string const itemNameBase = toolbox::toString("tts_info_%s_%s",
                                                         groupName_.c_str(),
                                                         serviceName.c_str());
      createString(itemNameBase,
                   "",
                   "",
                   tcds::utils::InfoSpaceItem::PROCESS,
                   false,
                   groupName + ";" + serviceName);

      // // This item holds all contributions with TTS state 'READY'.
      // createString(itemNameBase + "_ready",
      //              "",
      //              "",
      //              tcds::utils::InfoSpaceItem::PROCESS,
      //              false,
      //              groupName + ";" + serviceName + ";" + "ready");
      // // This item holds all contributions with TTS state 'IGNORED'.
      // createString(itemNameBase + "_ignored",
      //              "",
      //              "",
      //              tcds::utils::InfoSpaceItem::PROCESS,
      //              false,
      //              groupName + ";" + serviceName + ";" + "ignored");
      // // This item holds all contributions with TTS state different
      // // from 'READY' (and not 'IGNORED').
      // createString(itemNameBase + "_blocking",
      //              "",
      //              "",
      //              tcds::utils::InfoSpaceItem::PROCESS,
      //              false,
      //              groupName + ";" + serviceName + ";" + "blocking");
    }

  // // This will hold state information for all TCDS control applications.
  // createString("tts_info", "", "json-object");
}

tcds::cntrl::TTSInfoSpaceHandler::~TTSInfoSpaceHandler()
{
}

void
tcds::cntrl::TTSInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  // NOTE: The aim is to build an itemset for each
  // CPMController/LPMController, in the specified group, in our XDAQ
  // zone.

  // // Find all desired applications, in our own zone, in the requested
  // // group.
  // std::string const zoneName = getOwnerApplication().getApplicationContext()->getDefaultZoneName();
  // xdaq::ApplicationGroup* const group =
  //   getOwnerApplication().getApplicationContext()->getZone(zoneName)->getApplicationGroup(groupName_);

  // std::set<xdaq::ApplicationDescriptor*> const descriptorsCPM =
  //   group->getApplicationDescriptors("tcds::cpm::CPMController");
  // std::set<xdaq::ApplicationDescriptor*> const descriptorsLPM =
  //   group->getApplicationDescriptors("tcds::lpm::LPMController");

  // std::set<xdaq::ApplicationDescriptor*> descriptors = descriptorsCPM;
  // descriptors.insert(descriptorsLPM.begin(), descriptorsLPM.end());

  // // std::set<xdaq::ApplicationDescriptor*, compareApplications> apps;
  // // apps.insert(descriptors.begin(), descriptors.end());

  // for (std::set<xdaq::ApplicationDescriptor*>::const_iterator i = descriptors.begin();
  //      i != descriptors.end();
  //      ++i)
  //   {
  //     std::string serviceName = "unknown";
  //     std::string const tmp = (*i)->getAttribute("service");
  //     if (!tmp.empty())
  //       {
  //         serviceName = tmp;
  //       }
  //     std::string const itemSetName =
  //       toolbox::toString("itemset-ttsinfo-%s-%s",
  //                         groupName_.c_str(),
  //                         tmp.c_str());
  //     monitor.newItemSet(itemSetName);
  //     monitor.addItem(itemSetName,
  //                     "tts_info",
  //                     toolbox::toString("TTS summary - %s", serviceName.c_str()),
  //                     this);
  //     // Keep track of the itemset names for later.
  //     itemsetNames_.push_back(itemSetName);
  //   }

  for (std::vector<std::string>::const_iterator i = serviceNames_.begin();
       i != serviceNames_.end();
       ++i)
    {
      std::string const serviceName = *i;
      std::string const itemSetName =
        toolbox::toString("itemset-ttsinfo-%s-%s",
                          groupName_.c_str(),
                          serviceName.c_str());
      monitor.newItemSet(itemSetName);

      // monitor.addItem(itemSetName,
      //                 "tts_info",
      //                 toolbox::toString("TTS summary - %s", serviceName.c_str()),
      //                 this);

      std::string const itemNameBase = toolbox::toString("tts_info_%s_%s",
                                                         groupName_.c_str(),
                                                         serviceName.c_str());

      monitor.addItem(itemSetName,
                      itemNameBase,
                      "TTS info",
                      this);

      // // This item holds all contributions with TTS state 'READY'.
      // monitor.addItem(itemSetName,
      //                 itemNameBase + "_ready",
      //                 "READY",
      //                 this);
      // // This item holds all contributions with TTS state 'IGNORED'.
      // monitor.addItem(itemSetName,
      //                 itemNameBase + "_ignored",
      //                 "IGNORED",
      //                 this);
      // // This item holds all contributions with TTS state different
      // // from 'READY' (and not 'IGNORED').
      // monitor.addItem(itemSetName,
      //                 itemNameBase + "_blocking",
      //                 "blocking",
      //                 this);

      // // Keep track of the itemset names for later.
      // itemsetNames_.push_back(itemSetName);
    }
}

void
tcds::cntrl::TTSInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                tcds::utils::Monitor& monitor,
                                                                std::string const& forceTabName)
{
  // NOTE: If we found no CPM-/LPMControllers in this group, there is
  // no need to even register a tab.
  if (serviceNames_.size() > 0)
    {
      std::string const tabName = forceTabName.empty() ? toolbox::toString("TCDS '%s' TTS summary", groupName_.c_str()) : forceTabName;

      webServer.registerTab(tabName,
                            toolbox::toString("Summary of TTS states for CPM-/LPMControllers in group '%s'", groupName_.c_str()),
                            1);

      for (std::vector<std::string>::const_iterator i = serviceNames_.begin();
           i != serviceNames_.end();
           ++i)
        {
          // for (std::vector<std::string>::const_iterator i = itemsetNames_.begin();
          //      i != itemsetNames_.end();
          //      ++i)
          //   {
          std::string const serviceName = *i;
          std::string const itemSetName =
            toolbox::toString("itemset-ttsinfo-%s-%s",
                              groupName_.c_str(),
                              serviceName.c_str());
          // webServer.registerTable("TTS overview for service '" + serviceName + "'",
          //                         "",
          //                         monitor,
          //                         itemSetName,
          //                         tabName);
          webServer.registerWebObject<WebTableTTSInfo>("TTS overview for service '" + serviceName + "'",
                                                       "",
                                                       monitor,
                                                       itemSetName,
                                                       tabName);
        }
    }
}

std::string
tcds::cntrl::TTSInfoSpaceHandler::formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const
{
  // Specialized formatting rules for the TTCSpy logging settings enums.
  std::string res = tcds::utils::escapeAsJSONString(tcds::utils::InfoSpaceHandler::kInvalidItemString);
  if (item->isValid())
    {
      std::string name = item->name();
      if (toolbox::startsWith(name, "tts_info_"))
        {
          // Special in the sense that this is something that needs to
          // be interpreted as a proper JavaScript object, so let's
          // not add any more double quotes.
          res = getString(name);
        }
      else
        {
          // For everything else simply call the generic formatter.
          res = InfoSpaceHandler::formatItem(item);
        }
    }
  return res;
}
