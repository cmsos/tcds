#include "tcds/cntrl/AppsInfoSpaceUpdater.h"

#include <set>
#include <cstddef>
#include <vector>

#include "toolbox/string.h"
#include "toolbox/TimeVal.h"
#include "xcept/Exception.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationDescriptor.h"
#include "xdaq/ApplicationGroup.h"
#include "xdaq/ContextDescriptor.h"
#include "xdaq/Zone.h"
#include "xdata/Serializable.h"
#include "xdata/TimeVal.h"

#include "tcds/utils/XDAQAppBase.h"
#include "tcds/cntrl/TCDSCentral.h"
#include "tcds/cntrl/Utils.h"
#include "tcds/exception/Exception.h"
#include "tcds/utils/ConfigurationInfoSpaceHandler.h"
#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Utils.h"

tcds::cntrl::AppsInfoSpaceUpdater::AppsInfoSpaceUpdater(tcds::cntrl::TCDSCentral& xdaqApp,
                                                        std::string const& groupName) :
  tcds::utils::InfoSpaceUpdater(xdaqApp),
  groupName_(groupName),
  shouldCheckConfig_(true),
  lasURL_(""),
  flashlistName_("")
{
}

tcds::cntrl::AppsInfoSpaceUpdater::~AppsInfoSpaceUpdater()
{
}

tcds::utils::XDAQAppBase&
tcds::cntrl::AppsInfoSpaceUpdater::getOwnerApplication() const
{
  return static_cast<tcds::utils::XDAQAppBase&>(tcds::utils::InfoSpaceUpdater::getOwnerApplication());
}

void
tcds::cntrl::AppsInfoSpaceUpdater::updateInfoSpaceImpl(tcds::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  if (shouldCheckConfig_)
    {
      // Check if we can find the flashlist we need somewhere.
      std::vector<std::string> const lasURLs =
        getOwnerApplication().getConfigurationInfoSpaceHandler().getStringVec("lasURLs");
      flashlistName_ =
        getOwnerApplication().getConfigurationInfoSpaceHandler().getString("flashlistNameCommon");

      std::vector<std::string>::const_iterator const it =
        tcds::utils::findFlashList(flashlistName_, lasURLs);

      bool const flashlistFound = (it != lasURLs.end());

      if (flashlistFound)
        {
          lasURL_ = *it;
        }
      else
        {
          std::string const msg = toolbox::toString("Flashlist '%s' is not hosted "
                                                    "by any of the specified LASes.",
                                                    flashlistName_.c_str());
          flashlistName_ = "";
          lasURL_ = "";
          XCEPT_RAISE(tcds::exception::ConfigurationProblem, msg);
        }

      // If we found our flashlist, we don't have to do anything next
      // round, but otherwise we'll try again.
      shouldCheckConfig_ = !flashlistFound;
    }

  //----------

  // Check if the LAS already has any data collected for this flashlist.
  if (!tcds::utils::isFlashListPresentInLAS(lasURL_, flashlistName_))
    {
      std::string const msg =
        toolbox::toString("No data available yet for flashlist '%s' in LAS '%s'.",
                          flashlistName_.c_str(),
                          lasURL_.c_str());
      XCEPT_RAISE(tcds::exception::RuntimeProblem, msg);
    }

  //----------

  // Now get the flashlist.
  try
    {
      appsInfo_ = getAppsInfo();
    }
  catch (xcept::Exception const& err)
    {
      shouldCheckConfig_ = true;
    }

  // Then do what we normally do.
  tcds::utils::InfoSpaceUpdater::updateInfoSpaceImpl(infoSpaceHandler);
}

bool
tcds::cntrl::AppsInfoSpaceUpdater::updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                                       tcds::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  bool updated = false;
  std::string const name = item.name();
  tcds::utils::InfoSpaceItem::UpdateType const updateType = item.updateType();
  if (updateType == tcds::utils::InfoSpaceItem::PROCESS)
    {
      // The 'PROCESS' update type means that there is something
      // special to the variable. Figure out what to do based on
      // the variable name.
      if (name == "apps_info")
        {
          std::string const newVal = getAppsInfoString();
          infoSpaceHandler->setString(name, newVal);
          updated = true;
        }
    }
  if (!updated)
    {
      updated = tcds::utils::InfoSpaceUpdater::updateInfoSpaceItem(item, infoSpaceHandler);
    }
  if (updated)
    {
      item.setValid();
    }
  return updated;
}

xdata::Table
tcds::cntrl::AppsInfoSpaceUpdater::getAppsInfo() const
{
  // Get the information about all applications from the LAS.
  xdata::Table const flashlist = tcds::utils::getFlashList(lasURL_, flashlistName_);

  return flashlist;
}

std::string
tcds::cntrl::AppsInfoSpaceUpdater::getAppsInfoString() const
{
  // Find all applications, in our own zone, in the requested group.
  std::string const zoneName = getOwnerApplication().getApplicationContext()->getDefaultZoneName();

  xdaq::ApplicationGroup const* group = 0;
  try
    {
      group = getOwnerApplication().getApplicationContext()->getZone(zoneName)->getApplicationGroup(groupName_);
    }
  catch (xcept::Exception& err)
    {
      // This is probably no big deal. The application group probably
      // simply does not exist, so there is nothing to show.
    }

  std::set<xdaq::ApplicationDescriptor const*, tcds::cntrl::compareApplications> apps;
  if (group)
    {
      std::set<xdaq::ApplicationDescriptor const*> const descriptors =
        group->getApplicationDescriptors();
      apps.insert(descriptors.begin(), descriptors.end());
    }

  //----------

  toolbox::TimeVal const timeNow = toolbox::TimeVal::gettimeofday();

  //----------

  // Loop over all found applications and collect some info.
  std::string res;
  for (std::set<xdaq::ApplicationDescriptor const*>::const_iterator i = apps.begin();
       i != apps.end();
       ++i)
    {
      // Of course we're not interested in our own application
      // state...
      if (!(*i)->equals(*getOwnerApplication().getApplicationDescriptor()))
        {
          // If we're not on the first line, close the previous line
          // with a comma.
          if (!res.empty())
            {
              res += ", ";
            }

            // Start of JSON object.
            res += "{";

            // Application service name.
            std::string serviceName = "unknown";
            std::string tmp = (*i)->getAttribute("service");
            if (!tmp.empty())
              {
                serviceName = tmp;
              }
            res +=
              tcds::utils::escapeAsJSONString("service") +
              ": " +
              tcds::utils::escapeAsJSONString(serviceName);

            // Full application URL.
            std::string const url = (*i)->getContextDescriptor()->getURL();
            unsigned int const lid = (*i)->getLocalId();
            std::string const lidStr = toolbox::toString("%d", lid);
            std::string const urn = (*i)->getURN();
            std::string const fullUrl = url + "/" + urn;
            res +=
              ", " +
              tcds::utils::escapeAsJSONString("url") +
              ": " +
              tcds::utils::escapeAsJSONString(fullUrl);

          // Application-icon URL.
          std::string iconUrl = getOwnerApplication().buildIconPathName(*i);
          res +=
            ", " +
            tcds::utils::escapeAsJSONString("iconUrl") +
            ": " +
            tcds::utils::escapeAsJSONString(iconUrl);

            std::string stateName = "unknown";
            std::string hwLeaseOwner = "unknown";
            std::string rcmsSessionId = "unknown";
            std::string applicationState = "unknown";
            std::string problemDescription = "Application not found or info not updating. Application crashed?";
            std::string timestamp = "unknown";

            // Find the correct row in the table.
            for (size_t j = 0; j != appsInfo_.getRowCount(); ++j)
              {
                xdaq::ContextDescriptor contextDesc(appsInfo_.getValueAt(j, "context")->toString());
                // NOTE: Due to the DNS aliases etc. used, a naive
                // string-based URL comparison does not really work. One
                // really needs xdaq::ContextDescriptor::matchURL() but
                // this performs all kind of parsing and DNS resolving
                // in order to do a 'proper' URL comparison. The
                // side-effect is that this comparison takes quite a lot
                // of time and resources.
                bool urlsMatch = false;
                try
                  {
                    // NOTE: The matchURL() method will fail for
                    // non-existent host names.
                    urlsMatch = contextDesc.matchURL(url);
                  }
                catch (xcept::Exception& err)
                  {
                    problemDescription = "Failed to match url. Does this host exist?";
                  }
                if (urlsMatch &&
                    (appsInfo_.getValueAt(j, "lid")->toString() == lidStr))
                  {
                    // First check how old the data is.
                    xdata::Serializable* timestampTmp = appsInfo_.getValueAt(j, "timestamp");
                    xdata::TimeVal* timestampVal = dynamic_cast<xdata::TimeVal*>(timestampTmp);
                    timestamp = timestampVal->value_.toString("%F %T", toolbox::TimeVal::gmt);
                    // NOTE: During 'Configure' applications do not
                    // update their monitoring, so this comparison has
                    // to survive any plausible configuration duration.
                    if ((timeNow - timestampVal->value_) <= toolbox::TimeVal(30))
                      {
                        stateName = appsInfo_.getValueAt(j, "state_name")->toString();
                        hwLeaseOwner = appsInfo_.getValueAt(j, "hw_lease_owner_id")->toString();
                        rcmsSessionId = appsInfo_.getValueAt(j, "rcms_session_id")->toString();
                        applicationState = appsInfo_.getValueAt(j, "application_state")->toString();
                        problemDescription = appsInfo_.getValueAt(j, "problem_description")->toString();
                      }

                    break;
                  }
              }

            res +=
              ", " +
              tcds::utils::escapeAsJSONString("stateName") +
              ": " +
              tcds::utils::escapeAsJSONString(stateName);
            res +=
              ", " +
              tcds::utils::escapeAsJSONString("hwLeaseOwnerId") +
              ": " +
              tcds::utils::escapeAsJSONString(hwLeaseOwner);
            res +=
              ", " +
              tcds::utils::escapeAsJSONString("rcmsSessionId") +
              ": " +
              tcds::utils::escapeAsJSONString(rcmsSessionId);
            res +=
              ", " +
              tcds::utils::escapeAsJSONString("applicationState") +
              ": " +
              tcds::utils::escapeAsJSONString(applicationState);
            res +=
              ", " +
              tcds::utils::escapeAsJSONString("problemDescription") +
              ": " +
              tcds::utils::escapeAsJSONString(problemDescription);
            res +=
              ", " +
              tcds::utils::escapeAsJSONString("timestamp") +
              ": " +
              tcds::utils::escapeAsJSONString(timestamp);

            // End of JSON object.
            res += "}";
          }
    }

  res = "[" + res + "]";

  return res;
}
