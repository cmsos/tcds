#include "tcds/cntrl/WebTableTTSInfo.h"

#include <cassert>
#include <sstream>
#include <string>

#include "tcds/utils/Definitions.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/Utils.h"
#include "tcds/utils/WebObject.h"

tcds::cntrl::WebTableTTSInfo::WebTableTTSInfo(std::string const& name,
                                                std::string const& description,
                                                tcds::utils::Monitor const& monitor,
                                                std::string const& itemSetName,
                                                std::string const& tabName,
                                                size_t const colSpan) :
tcds::utils::WebObject(name, description, monitor, itemSetName, tabName, colSpan)
{
}

std::string
tcds::cntrl::WebTableTTSInfo::getHTMLString() const
{
  // NOTE: This has been written to work with the new XDAQ12-style
  // HyperDAQ tabs and doT.js.

  std::stringstream res;

  res << "<div class=\"tcds-item-table-wrapper\">"
      << "\n";

  res << "<p class=\"tcds-item-table-title\">"
      << getName()
      << "</p>";
  res << "\n";

  res << "<p class=\"tcds-item-table-description\">"
      << getDescription()
      << "</p>";
  res << "\n";

  // And now the actual table contents.
  tcds::utils::Monitor::StringPairVector items =
    monitor_.getFormattedItemSet(itemSetName_);

  // ASSERT ASSERT ASSERT
  assert (items.size() == 1);
  // ASSERT ASSERT ASSERT end

  std::string const itemName = items.at(0).first;
  std::string const tmp = "[\"" + itemSetName_ + "\"][\"" + itemName + "\"]";
  std::string const invalidVal = tcds::utils::WebObject::kStringInvalidItem;

  res << "<script type=\"text/x-dot-template\">";
  // Case 0: invalid item ('-').
  res << "{{? it" << tmp << " == '" << invalidVal << "'}}"
      << "<span>" << invalidVal << "</span>"
      << "\n";
  // Case 1: a (hopefully non-empty) array.
  res << "{{??}}"
      << "\n"
    // This is not going to win any awards, no...
      << "{{? it" << tmp << ".reduce((acc, value) => acc + value[\"rows\"].length, 0) == 0}}"
      << "\n"
      << "<span>No info</span>"
      << "\n"
      << "{{??}}"
      << "<table class=\"xdaq-table tcds-item-table\">"
      << "\n"
      << "<tbody>"
      << "\n"

      << "{{~it" << tmp << " :value:index}}"
      << "<tr>"
      << "\n"
      << "<td class=\"tcds-item-header\">{{=value[\"group\"]}}</td>"

      << "<td class=\"tcds-item-value\">"

    // Again a case 0:
      << "{{? value[\"rows\"].length == 0}}"
      << "None"
    // And again a case 1:
      << "{{??}}"
      << "{{~value[\"rows\"] :value0:index0}}"
      << "<span class=\"tts\" tts_val=\"{{=value0[\"value\"]}}\">"
      << "{{=value0[\"label\"]}}"
    // The READY and MASKED states are easily distinguishable. The
    // others benefit from a bit more info.
    // But no awards here either...
      << "{{? ['" << tcds::utils::TTSStateToString(tcds::definitions::TTS_STATE_READY) << "', '" << tcds::utils::TTSStateToString(tcds::definitions::TTS_STATE_MASKED) << "'].indexOf(value0[\"value\"]) == -1}}"
      << " ({{=value0[\"value\"]}})"
      << "{{?}}"
      << "{{? value[\"rows\"].length-1 != index0}}"
      << "</span>"
      << ", "
      << "{{?}}"
      << "{{~}}"
      << "{{?}}"


      << "</td>"

      << "\n"
      << "</tr>"
      << "\n"
      << "{{~}}"

      << "\n"
      << "</tbody>"
      << "\n"
      << "</table>"
      << "\n";
  res << "{{?}}"
      << "\n";

  res << "{{?}}"
      << "\n";
  res << "</script>"
      << "\n"
      << "<div class=\"target\">" << kDefaultValueString << "</div>"
      << "\n";

  return res.str();
}
