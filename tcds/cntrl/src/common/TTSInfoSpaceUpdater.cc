#include "tcds/cntrl/TTSInfoSpaceUpdater.h"

#include <algorithm>
#include <cstddef>
#include <stdint.h>
#include <vector>

#include "toolbox/string.h"
#include "xcept/Exception.h"
#include "xdata/String.h"
#include "xdata/Table.h"
#include "xdata/UnsignedInteger32.h"

#include "tcds/cntrl/TCDSCentral.h"
#include "tcds/exception/Exception.h"
#include "tcds/utils/ConfigurationInfoSpaceHandler.h"
#include "tcds/utils/Definitions.h"
#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Utils.h"
#include "tcds/utils/XDAQAppBase.h"

tcds::cntrl::TTSInfoSpaceUpdater::TTSInfoSpaceUpdater(tcds::cntrl::TCDSCentral& xdaqApp,
                                                      std::string const& groupName) :
  tcds::utils::InfoSpaceUpdater(xdaqApp),
  groupName_(groupName),
  shouldCheckConfig_(true),
  lasURL_(""),
  flashlistName_("")
{
}

tcds::cntrl::TTSInfoSpaceUpdater::~TTSInfoSpaceUpdater()
{
}

tcds::utils::XDAQAppBase&
tcds::cntrl::TTSInfoSpaceUpdater::getOwnerApplication() const
{
  return static_cast<tcds::utils::XDAQAppBase&>(tcds::utils::InfoSpaceUpdater::getOwnerApplication());
}

void
tcds::cntrl::TTSInfoSpaceUpdater::updateInfoSpaceImpl(tcds::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  if (shouldCheckConfig_)
    {
      // Check if we can find the flashlist we need somewhere.
      std::vector<std::string> const lasURLs =
        getOwnerApplication().getConfigurationInfoSpaceHandler().getStringVec("lasURLs");
      flashlistName_ =
        getOwnerApplication().getConfigurationInfoSpaceHandler().getString("flashlistNamePMTTS");

      std::vector<std::string>::const_iterator const it =
        tcds::utils::findFlashList(flashlistName_, lasURLs);

      bool const flashlistFound = (it != lasURLs.end());

      if (flashlistFound)
        {
          lasURL_ = *it;
        }
      else
        {
          std::string const msg = toolbox::toString("Could not find flashlist '%s' in any of the LASes.",
                                                    flashlistName_.c_str());
          flashlistName_ = "";
          lasURL_ = "";
          XCEPT_RAISE(tcds::exception::ConfigurationProblem, msg);
        }

      // If we found our flashlist, we don't have to do anything next
      // round, but otherwise we'll try again.
      shouldCheckConfig_ = !flashlistFound;
    }

  //----------

  // Start by getting the flashlist containing all
  // CPMController/LPMController TTS info.
  try
    {
      ttsInfo_ = getTTSInfo();
    }
  catch (xcept::Exception const& err)
    {
      // This is probably no big deal. The above procedure ensures
      // that we're querying the correct LAS, so no response probably
      // just indicates that the LAS has been restarted and has not
      // yet received any flashlist info.
    }

  // Then do what we normally do.
  tcds::utils::InfoSpaceUpdater::updateInfoSpaceImpl(infoSpaceHandler);
}

bool
tcds::cntrl::TTSInfoSpaceUpdater::updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                                      tcds::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  bool updated = false;
  std::string const name = item.name();
  tcds::utils::InfoSpaceItem::UpdateType const updateType = item.updateType();
  if (updateType == tcds::utils::InfoSpaceItem::PROCESS)
    {
      // The 'PROCESS' update type means that there is something
      // special to the variable. Figure out what to do based on
      // the variable name.

      if (toolbox::startsWith(name, "tts_info_"))
        {

          // NOTE: This may not be very pretty, but I don't have much
          // better ideas either.
          std::string const hwName = item.hwName();

          // The last part of the 'hardware name', after the last ';',
          // is the service name.
          size_t const pos = hwName.rfind(';');
          std::string const serviceName = hwName.substr(pos + 1);

          // Determine and store the new value.
          std::string const newVal = getTTSString(serviceName);
          infoSpaceHandler->setString(name, newVal);
          updated = true;
        }

    }
  if (!updated)
    {
      updated = tcds::utils::InfoSpaceUpdater::updateInfoSpaceItem(item, infoSpaceHandler);
    }
  if (updated)
    {
      item.setValid();
    }
  return updated;
}

xdata::Table
tcds::cntrl::TTSInfoSpaceUpdater::getTTSInfo() const
{
  // Get the information about all TTS channels from the LAS.
  xdata::Table const flashlist = tcds::utils::getFlashList(lasURL_, flashlistName_);

  return flashlist;
}

std::string
tcds::cntrl::TTSInfoSpaceUpdater::getTTSString(std::string const& serviceName)
{
  //----------

  std::string res;

  //----------

  // Loop over the flashlist and build JSON object of the information
  // related to the requested service.

  std::map<std::string, std::vector<xdata::Table::iterator> > infoMap;
  for (xdata::Table::iterator it = ttsInfo_.begin();
       it != ttsInfo_.end();
       ++it)
    {
      // Filter by (publishing) service name. I.e., the
      // CPM-/LPMController in charge.
      xdata::String* tmp = dynamic_cast<xdata::String*>(it->getField("service"));
      std::string const serviceField = *tmp;
      if (serviceField == serviceName)
        {
          xdata::UnsignedInteger32* tmp0 =
            dynamic_cast<xdata::UnsignedInteger32*>(it->getField("value"));
          uint32_t const ttsValue = *tmp0;
          if (ttsValue == tcds::definitions::TTS_STATE_READY)
            {
              infoMap["Ready"].push_back(it);
            }
          // MASKED.
          else if (ttsValue == tcds::definitions::TTS_STATE_MASKED)
            {
              infoMap["Masked"].push_back(it);
            }
          // Anything else.
          else
            {
              infoMap["Blocking"].push_back(it);
            }
        }
    }

  for (std::map<std::string, std::vector<xdata::Table::iterator> >::iterator i = infoMap.begin();
       i != infoMap.end();
       ++i)
    {
      for (std::vector<xdata::Table::iterator>::iterator row = i->second.begin();
           row != i->second.end();
           ++row)
        {
          xdata::String* const tmp = dynamic_cast<xdata::String*>(((*row)->getField("label")));
          std::string const label = *tmp;
        }
    }

  //----------

  // For each of the publishing services, sort each vector according
  // to our wishes.
  for (std::map<std::string, std::vector<xdata::Table::iterator> >::iterator i = infoMap.begin();
       i != infoMap.end();
       ++i)
    {
      std::sort(i->second.begin(), i->second.end(), compareHelper());
    }

  //----------

  std::vector<std::string> groupNames;
  groupNames.push_back("Ready");
  groupNames.push_back("Masked");
  groupNames.push_back("Blocking");
  for (std::vector<std::string>::const_iterator i = groupNames.begin();
       i != groupNames.end();
       ++i)
    {
      // If we're not on the first line, close the previous line with
      // a comma.
      if (!res.empty())
        {
          res += ", ";
        }

      // Start of JSON object.
      res += "{";

      // The TTS-state group name.
      res +=
        tcds::utils::escapeAsJSONString("group") +
        ": " +
        tcds::utils::escapeAsJSONString(*i);

      // The rows with the actual data.
      res +=
        ", " +
        tcds::utils::escapeAsJSONString("rows") +
        ": [";

      for (std::vector<xdata::Table::iterator>::iterator row = infoMap[*i].begin();
           row != infoMap[*i].end();
           ++row)
        {

          // If we're not on the first line, close the previous line
          // with a comma.
          if (row != infoMap[*i].begin())
            {
              res += ", ";
            }

          // Start of JSON object.
          res += "{";

          // The label of the TTS source.
          xdata::String* const tmp = dynamic_cast<xdata::String*>((*row)->getField("label"));
          std::string const label = *tmp;
          res +=
            tcds::utils::escapeAsJSONString("label") +
            ": " +
            tcds::utils::escapeAsJSONString(label);

          // The TTS state of the TTS source.
          xdata::UnsignedInteger32* const tmp0 =
            dynamic_cast<xdata::UnsignedInteger32*>((*row)->getField("value"));
          uint32_t const value = *tmp0;
          std::string const valueStr = tcds::utils::TTSStateToString(value);
          res +=
            ", " +
            tcds::utils::escapeAsJSONString("value") +
            ": " +
            tcds::utils::escapeAsJSONString(valueStr);

          // End of JSON object.
          res += "}";
        }

      // End of data rows
      res += "]";

      // End of JSON object.
      res += "}";
    }

  //----------

  res = "[" + res + "]";

  return res;
}
