#ifndef _tcds_cpm_LumiNibble_h_
#define _tcds_cpm_LumiNibble_h_

#include <stdint.h>

#include "tcds/cpm/BRILDAQPacket.h"

namespace tcds {
  namespace cpm {

    class LumiNibble
    {

    public:
      LumiNibble(tcds::cpm::BRILDAQPacket const& brildaqPacket);
      ~LumiNibble();

      bool isCorrupted() const;
      bool isAlmostCorrupted() const;
      bool isInvalid() const;
      bool isGood() const;

      // void markAsGood();
      // void markAsCorrupted();
      // void markAsInvalid();

      uint32_t bstSignalStatus() const;
      // OBSOLETE OBSOLETE OBSOLETE
      uint32_t timestamp() const;
      // OBSOLETE OBSOLETE OBSOLETE end
      // The timestamp of the last orbit of the previous nibble (!).
      double timestampBegin() const;
      // The timestamp of the last orbit of the current nibble.
      double timestampEnd() const;

      uint32_t fillNumber() const;
      uint32_t runNumber() const;
      uint32_t lumiSectionNumber() const;
      uint32_t lumiNibbleNumber() const;
      uint32_t orbitNumber() const;
      uint32_t numOrbits() const;
      uint32_t numNibblesPerSection() const;
      bool isCMSRunActive() const;

      uint32_t numBX(bool const gated=false) const;

      uint32_t triggerCount(unsigned int const trigType,
                            bool const gated=false) const;
      uint32_t triggerCountTotal(bool const gated=false) const;
      uint32_t suppressedTriggerCount(unsigned int const trigType,
                                      bool const gated=false) const;
      uint32_t suppressedTriggerCountTotal(bool const gated=false) const;

      uint32_t suppressedTriggerCountFromTTS(uint32_t const trigType,
                                             bool const gated=false) const;
      uint32_t suppressedTriggerCountFromTriggerRules(uint32_t const trigType,
                                                      bool const gated=false) const;
      uint32_t suppressedTriggerCountFromBunchMaskVeto(uint32_t const trigType,
                                                       bool const gated=false) const;
      uint32_t suppressedTriggerCountFromRetri(uint32_t const trigType,
                                               bool const gated=false) const;
      uint32_t suppressedTriggerCountFromAPVE(uint32_t const trigType,
                                              bool const gated=false) const;
      uint32_t suppressedTriggerCountFromDAQBackpressure(uint32_t const trigType,
                                                         bool const gated=false) const;
      uint32_t suppressedTriggerCountFromCalibration(uint32_t const trigType,
                                                     bool const gated=false) const;
      uint32_t suppressedTriggerCountFromSWPause(uint32_t const trigType,
                                                 bool const gated=false) const;
      uint32_t suppressedTriggerCountFromFWPause(uint32_t const trigType,
                                                 bool const gated=false) const;

      uint32_t deadtimeBXCountTotal(bool const gated=false) const;
      uint32_t deadtimeBXCountFromTTSAllPartitions(bool const gated=false) const;
      uint32_t deadtimeBXCountFromTTSSingleICI(unsigned int const lpmNum,
                                               unsigned int const iciNum,
                                               bool const gated=false) const;
      uint32_t deadtimeBXCountFromTTSSingleAPVE(unsigned int const lpmNum,
                                                unsigned int const apveNum,
                                                bool const gated=false) const;
      uint32_t deadtimeBXCountFromTriggerRules(bool const gated=false) const;
      uint32_t deadtimeBXCountFromBunchMaskVeto(bool const gated=false) const;
      uint32_t deadtimeBXCountFromRetri(bool const gated=false) const;
      uint32_t deadtimeBXCountFromAPVE(bool const gated=false) const;
      uint32_t deadtimeBXCountFromDAQBackpressure(bool const gated=false) const;
      uint32_t deadtimeBXCountFromCalibration(bool const gated=false) const;
      uint32_t deadtimeBXCountFromSWPause(bool const gated=false) const;
      uint32_t deadtimeBXCountFromFWPause(bool const gated=false) const;
      uint32_t deadtimeBXCountFromTriggerRule(unsigned int const ruleNum,
                                              bool const gated=false) const;

      double lumiNibbleDuration() const;
      double lumiSectionDuration() const;

      double triggerRate(unsigned int const trigType,
                         bool const gated=false) const;
      double triggerRateTotal(bool const gated=false) const;
      double suppressedTriggerRate(unsigned int const trigType,
                                   bool const gated=false) const;
      double suppressedTriggerRateTotal(bool const gated=false) const;

      double deadtimeFractionTotal(bool const gated=false) const;
      double deadtimeFractionFromTTSAllPartitions(bool const gated=false) const;
      double deadtimeFractionFromTTSSingleICI(unsigned int const lpmNum,
                                              unsigned int const iciNum,
                                              bool const gated=false) const;
      double deadtimeFractionFromTTSSingleAPVE(unsigned int const lpmNum,
                                               unsigned int const apveNum,
                                               bool const gated=false) const;
      double deadtimeFractionFromTriggerRules(bool const gated=false) const;
      double deadtimeFractionFromBunchMaskVeto(bool const gated=false) const;
      double deadtimeFractionFromRetri(bool const gated=false) const;
      double deadtimeFractionFromAPVE(bool const gated=false) const;
      double deadtimeFractionFromDAQBackpressure(bool const gated=false) const;
      double deadtimeFractionFromCalibration(bool const gated=false) const;
      double deadtimeFractionFromSWPause(bool const gated=false) const;
      double deadtimeFractionFromFWPause(bool const gated=false) const;
      double deadtimeFractionFromTriggerRule(unsigned int const ruleNum,
                                             bool const gated=false) const;

    private:

      tcds::cpm::BRILDAQPacket rawData_;
      // tcds::definitions::LUMI_NIBBLE_QUALITY quality_;

      double toRate(uint32_t const count) const;
      double toTimeFraction(uint32_t const count,
                            bool const gated) const;

      // void setQuality(tcds::definitions::LUMI_NIBBLE_QUALITY const& quality);

    };

  } // namespace cpm
} // namespace tcds

#endif // _tcds_cpm_LumiNibble_h_
