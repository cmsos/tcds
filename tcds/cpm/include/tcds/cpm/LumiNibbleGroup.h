#ifndef _tcds_cpm_LumiNibbleGroup_h_
#define _tcds_cpm_LumiNibbleGroup_h_

#include <vector>

#include "tcds/cpm/LumiNibble.h"
#include "tcds/cpm/LumiNibbleGroupBase.h"

namespace tcds {
  namespace cpm {

    class LumiNibbleGroup : public tcds::cpm::LumiNibbleGroupBase
    {

    public:
      LumiNibbleGroup(tcds::cpm::LumiNibble const& nibble);
      LumiNibbleGroup(std::vector<tcds::cpm::LumiNibble> const& nibbles);
      virtual ~LumiNibbleGroup();

    };

  } // namespace cpm
} // namespace tcds

#endif // _tcds_cpm_LumiNibbleGroup_h_
