#ifndef _tcds_cpm_HwStatusInfoSpaceUpdater_h_
#define _tcds_cpm_HwStatusInfoSpaceUpdater_h_

#include "tcds/utils/HwInfoSpaceUpdaterBase.h"

namespace tcds {
  namespace utils {
    class InfoSpaceHandler;
    class InfoSpaceItem;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace cpm {

    class TCADeviceCPMT1;
    class TCADeviceCPMT2;

    class HwStatusInfoSpaceUpdater : public tcds::utils::HwInfoSpaceUpdaterBase
    {

    public:
      HwStatusInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                               tcds::cpm::TCADeviceCPMT1 const& hwT1,
                               tcds::cpm::TCADeviceCPMT2 const& hwT2);

      virtual bool updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                       tcds::utils::InfoSpaceHandler* const infoSpaceHandler);

    private:
      tcds::cpm::TCADeviceCPMT1 const& getHwT1() const;
      tcds::cpm::TCADeviceCPMT2 const& getHwT2() const;

      tcds::cpm::TCADeviceCPMT1 const& hwT1_;
      tcds::cpm::TCADeviceCPMT2 const& hwT2_;

    };

  } // namespace cpm
} // namespace tcds

#endif // _tcds_cpm_HwStatusInfoSpaceUpdater_h_
