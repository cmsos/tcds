#ifndef _tcds_cpm_BRILDAQNibbleStorageInfoSpaceHandler_h_
#define _tcds_cpm_BRILDAQNibbleStorageInfoSpaceHandler_h_

#include <string>

#include "tcds/utils/InfoSpaceHandler.h"

namespace tcds {
  namespace utils {
    class Monitor;
    class WebServer;
  }
}

namespace xdaq {
  class Application;
}

namespace tcds {
  namespace cpm {

    class LumiNibble;

    /*! This InfoSpaceHandler is intended especially for the
      per-lumi-nibble publication and storage of all BRILDAQ data. It
      is supposed to be filled and published once per lumi nibble
      directly by the BRILDAQLoop. */
    class BRILDAQNibbleStorageInfoSpaceHandler : public tcds::utils::InfoSpaceHandler
    {

    public:
      BRILDAQNibbleStorageInfoSpaceHandler(xdaq::Application& xdaqApp);
      virtual ~BRILDAQNibbleStorageInfoSpaceHandler();

      void update(LumiNibble const& nibble);

    protected:
      virtual void registerItemSetsWithMonitor(tcds::utils::Monitor& monitor);
      virtual void registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                 tcds::utils::Monitor& monitor,
                                                 std::string const& forceTabName="");

    };

  } // namespace cpm
} // namespace tcds

#endif // _tcds_cpm_BRILDAQNibbleStorageInfoSpaceHandler_h_
