#ifndef _tcds_cpm_CPMTTSLinksInfoSpaceUpdater_h_
#define _tcds_cpm_CPMTTSLinksInfoSpaceUpdater_h_

#include <string>

#include "tcds/utils/HwInfoSpaceUpdaterBase.h"

namespace tcds {
  namespace utils {
    class InfoSpaceHandler;
    class InfoSpaceItem;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace cpm {

    class TCADeviceCPMT1;

    class CPMTTSLinksInfoSpaceUpdater : public tcds::utils::HwInfoSpaceUpdaterBase
    {

    public:
      CPMTTSLinksInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                  tcds::cpm::TCADeviceCPMT1 const& hwT1);
      virtual ~CPMTTSLinksInfoSpaceUpdater();

      virtual bool updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                       tcds::utils::InfoSpaceHandler* const infoSpaceHandler);

    private:
      tcds::cpm::TCADeviceCPMT1 const& getHwT1() const;

      unsigned int extractLPMNumber(std::string const& regName) const;

      tcds::cpm::TCADeviceCPMT1 const& hwT1_;

    };

  } // namespace cpm
} // namespace tcds

#endif // _tcds_cpm_CPMTTSLinksInfoSpaceUpdater_h_
