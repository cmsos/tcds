#ifndef _tcds_cpm_BRILDAQLoop_h_
#define _tcds_cpm_BRILDAQLoop_h_

#include <deque>
#include <memory>
#include <stdint.h>
#include <string>
#include <sys/types.h>

#include "toolbox/exception/Listener.h"
#include "toolbox/lang/Class.h"

#include "tcds/cpm/BRILDAQNibbleStorageInfoSpaceHandler.h"
#include "tcds/cpm/BRILDAQSectionStorageInfoSpaceHandler.h"
#include "tcds/cpm/BRILDAQSnapshot.h"
#include "tcds/cpm/GrandMasterStorageInfoSpaceHandler.h"
#include "tcds/cpm/LumiNibble.h"
#include "tcds/cpm/LumiSection.h"
#include "tcds/utils/Lock.h"

namespace toolbox {
  namespace task {
    class ActionSignature;
    class WorkLoop;
  }
}

namespace xcept {
  class Exception;
}

namespace tcds {
  namespace cpm {
    class BRILDAQPublisher;
    class CPMController;
  }
}

namespace tcds {
  namespace cpm {

    class BRILDAQLoop :
      public toolbox::exception::Listener,
      public toolbox::lang::Class
    {

    public:
      BRILDAQLoop(tcds::cpm::CPMController& cpmController);
      virtual ~BRILDAQLoop();

      // The toolbox::exception::Listener callback.
      virtual void onException(xcept::Exception& err);

      // Acquisition loop (start/stop) control methods.
      void start();
      void stop();

      // Reset the book keeping counters (number of nibbles missed,
      // etc.).
      void resetCounters();

      // Clear the 'recent nibbles/sections' history.
      void clearHistory();

      bool isHwConnected() const;

      bool update(toolbox::task::WorkLoop* workLoop);

      BRILDAQSnapshot snapshot() const;

    private:
      static const useconds_t kLoopRelaxTime = 100000;

      std::string const nibbleDAQAlarmName_;
      std::string const brilDAQAlarmName_;
      tcds::cpm::CPMController& cpmController_;
      std::unique_ptr<toolbox::task::ActionSignature> daqWorkLoopActionP_;
      std::unique_ptr<toolbox::task::WorkLoop> daqWorkLoopP_;
      std::string workLoopName_;

      uint32_t numNibblesEmpty_;
      uint32_t numNibblesReadMulti_;
      uint32_t numNibblesInvalid_;
      uint32_t numNibblesCorrupted_;
      uint32_t numNibblesAlmostCorrupted_;
      uint32_t numNibblesMissed_;
      std::deque<tcds::cpm::LumiNibble> nibbles_;
      std::deque<tcds::cpm::LumiSection> sections_;

      mutable tcds::utils::Lock lock_;

      // The thingy that takes care of publishing our data to the
      // BRILDAQ eventing system.
      std::unique_ptr<tcds::cpm::BRILDAQPublisher> brildaqPublisherP_;

      uint32_t swVersion_;
      uint32_t fwVersion_;
      uint16_t fedId_;

      // The InfoSpaceHandlers with the rates, deadtimes, etc. that
      // should go straight into the database.
      BRILDAQNibbleStorageInfoSpaceHandler brildaqNibbleStorage_;
      BRILDAQSectionStorageInfoSpaceHandler brildaqSectionStorage_;

      // A dedicated 'grand master' InfoSpaceHandler containing the
      // master book keeping information from the BRILDAQ data (i.e.,
      // fill and run numbers, lumi section and -nibble numbers, etc.)
      // together with (BST-based) timestamps.
      // NOTE: This is the CMS-wide master!
      GrandMasterStorageInfoSpaceHandler grandmasterStorage_;

    };

  } // namespace cpm
} // namespace tcds

#endif // _tcds_cpm_BRILDAQLoop_h_
