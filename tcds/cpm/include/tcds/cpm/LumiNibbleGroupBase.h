#ifndef _tcds_cpm_LumiNibbleGroupBase_h_
#define _tcds_cpm_LumiNibbleGroupBase_h_

#include <stddef.h>
#include <stdint.h>
#include <vector>

#include "tcds/cpm/LumiNibble.h"

namespace tcds {
  namespace cpm {

    class LumiNibbleGroupBase
    {

    public:
      LumiNibbleGroupBase(tcds::cpm::LumiNibble const& nibble);
      LumiNibbleGroupBase(std::vector<tcds::cpm::LumiNibble> const& nibbles);
      virtual ~LumiNibbleGroupBase();

      virtual void addNibble(tcds::cpm::LumiNibble const& nibble);

      size_t numNibbles() const;
      uint32_t numOrbits() const;
      uint32_t numBX(bool const gated=false) const;

      uint32_t fillNumber() const;
      uint32_t runNumber() const;
      uint32_t lumiSectionNumber() const;

      // The timestamp of the beginning of the first (available)
      // nibble in this section.
      double timestampBegin() const;
      // The timestamp of the end of the last (available) nibble in
      // this section.
      double timestampEnd() const;

      uint32_t triggerCount(uint32_t const trigType,
                            bool const gated=false) const;
      uint32_t triggerCountTotal(bool const gated=false) const;
      uint32_t suppressedTriggerCount(uint32_t const trigType,
                                      bool const gated=false) const;
      uint32_t suppressedTriggerCountTotal(bool const gated=false) const;

      uint32_t suppressedTriggerCountFromTTS(uint32_t const trigType,
                                             bool const gated=false) const;
      uint32_t suppressedTriggerCountFromTriggerRules(uint32_t const trigType,
                                                      bool const gated=false) const;
      uint32_t suppressedTriggerCountFromBunchMaskVeto(uint32_t const trigType,
                                                       bool const gated=false) const;
      uint32_t suppressedTriggerCountFromRetri(uint32_t const trigType,
                                               bool const gated=false) const;
      uint32_t suppressedTriggerCountFromAPVE(uint32_t const trigType,
                                              bool const gated=false) const;
      uint32_t suppressedTriggerCountFromDAQBackpressure(uint32_t const trigType,
                                                         bool const gated=false) const;
      uint32_t suppressedTriggerCountFromCalibration(uint32_t const trigType,
                                                     bool const gated=false) const;
      uint32_t suppressedTriggerCountFromSWPause(uint32_t const trigType,
                                                 bool const gated=false) const;
      uint32_t suppressedTriggerCountFromFWPause(uint32_t const trigType,
                                                 bool const gated=false) const;

      double triggerRate(unsigned int const trigType,
                         bool const gated=false) const;
      double triggerRateTotal(bool const gated=false) const;
      double suppressedTriggerRate(unsigned int const trigType,
                                   bool const gated=false) const;
      double suppressedTriggerRateTotal(bool const gated=false) const;

      uint32_t deadtimeBXCountTotal(bool const gated=false) const;
      uint32_t deadtimeBXCountFromTTSAllPartitions(bool const gated=false) const;
      uint32_t deadtimeBXCountFromTTSSingleICI(unsigned int const lpmNum,
                                               unsigned int const iciNum,
                                               bool const gated=false) const;
      uint32_t deadtimeBXCountFromTTSSingleAPVE(unsigned int const lpmNum,
                                                unsigned int const apveNum,
                                                bool const gated=false) const;
      uint32_t deadtimeBXCountFromTriggerRules(bool const gated=false) const;
      uint32_t deadtimeBXCountFromBunchMaskVeto(bool const gated=false) const;
      uint32_t deadtimeBXCountFromRetri(bool const gated=false) const;
      uint32_t deadtimeBXCountFromAPVE(bool const gated=false) const;
      uint32_t deadtimeBXCountFromDAQBackpressure(bool const gated=false) const;
      uint32_t deadtimeBXCountFromCalibration(bool const gated=false) const;
      uint32_t deadtimeBXCountFromSWPause(bool const gated=false) const;
      uint32_t deadtimeBXCountFromFWPause(bool const gated=false) const;
      uint32_t deadtimeBXCountFromTriggerRule(unsigned int const ruleNum,
                                              bool const gated=false) const;

      double deadtimeFractionTotal(bool const gated=false) const;
      double deadtimeFractionFromTTSAllPartitions(bool const gated=false) const;
      double deadtimeFractionFromTTSSingleICI(unsigned int const lpmNum,
                                              unsigned int const iciNum,
                                              bool const gated=false) const;
      double deadtimeFractionFromTTSSingleAPVE(unsigned int const lpmNum,
                                               unsigned int const apveNum,
                                               bool const gated=false) const;
      double deadtimeFractionFromTriggerRules(bool const gated=false) const;
      double deadtimeFractionFromBunchMaskVeto(bool const gated=false) const;
      double deadtimeFractionFromRetri(bool const gated=false) const;
      double deadtimeFractionFromAPVE(bool const gated=false) const;
      double deadtimeFractionFromDAQBackpressure(bool const gated=false) const;
      double deadtimeFractionFromCalibration(bool const gated=false) const;
      double deadtimeFractionFromSWPause(bool const gated=false) const;
      double deadtimeFractionFromFWPause(bool const gated=false) const;
      double deadtimeFractionFromTriggerRule(unsigned int const ruleNum,
                                             bool const gated=false) const;

      LumiNibble nibble(size_t const i) const;

    protected:
      std::vector<tcds::cpm::LumiNibble> nibbles_;

      double toRate(uint32_t const count) const;
      double toTimeFraction(uint32_t const count,
                            bool const gated) const;

    };

  } // namespace cpm
} // namespace tcds

#endif // _tcds_cpm_LumiNibbleGroupBase_h_
