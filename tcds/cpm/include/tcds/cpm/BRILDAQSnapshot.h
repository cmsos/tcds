#ifndef _tcds_cpm_BRILDAQSnapshot_h_
#define _tcds_cpm_BRILDAQSnapshot_h_

#include <deque>
#include <stdint.h>
#include <vector>

#include "tcds/cpm/LumiNibble.h"
#include "tcds/cpm/LumiSection.h"

namespace tcds {
  namespace cpm {

    class BRILDAQSnapshot
    {

    public:
      BRILDAQSnapshot(uint32_t const numNibblesMissed,
                      uint32_t const numNibblesReadMulti,
                      uint32_t const numNibblesInvalid,
                      uint32_t const numNibblesCorrupted,
                      uint32_t const numNibblesAlmostCorrupted,
                      std::deque<tcds::cpm::LumiNibble> const& nibbles,
                      std::deque<tcds::cpm::LumiSection> const& sections);
      ~BRILDAQSnapshot();

      uint32_t numNibblesMissed() const;
      uint32_t numNibblesReadMulti() const;
      uint32_t numNibblesInvalid() const;
      uint32_t numNibblesCorrupted() const;
      uint32_t numNibblesAlmostCorrupted() const;

      std::vector<tcds::cpm::LumiNibble> recentNibbles() const;
      std::vector<tcds::cpm::LumiSection> recentSections() const;

    private:
      uint32_t numNibblesMissed_;
      uint32_t numNibblesReadMulti_;
      uint32_t numNibblesInvalid_;
      uint32_t numNibblesCorrupted_;
      uint32_t numNibblesAlmostCorrupted_;
      std::deque<tcds::cpm::LumiNibble> nibbles_;
      std::deque<tcds::cpm::LumiSection> sections_;

    };

  } // namespace cpm
} // namespace tcds

#endif // _tcds_cpm_BRILDAQSnapshot_h_
