#ifndef _tcds_cpm_BRILDAQTable_h_
#define _tcds_cpm_BRILDAQTable_h_

#include <stdint.h>

#include "xdata/Table.h"

namespace tcds {
  namespace cpm {

    class LumiNibble;

    class BRILDAQTable : public xdata::Table
    {

    public:
      BRILDAQTable(uint32_t const swVersionIn,
                   uint32_t const fwVersionIn,
                   uint32_t const dataSourceIdIn,
                   tcds::cpm::LumiNibble const& nibble);
      virtual ~BRILDAQTable();

    private:
      static unsigned int const kFormatVersion = 2;

    };

  } // namespace cpm
} // namespace tcds

#endif // _tcds_cpm_BRILDAQTable_h_
