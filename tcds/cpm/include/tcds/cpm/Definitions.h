#ifndef _tcds_cpm_Definitions_h_
#define _tcds_cpm_Definitions_h_

namespace tcds {
  namespace definitions {

    enum CPM_T2_SELECTED_GT {
      CPM_T2_SELECTED_GT_L1A1 = 0,
      CPM_T2_SELECTED_GT_L1A2 = 1,
      CPM_T2_SELECTED_GT_UNKNOWN
    };

    enum CPM_EXTERNAL_TRIGGER0_SOURCE_MODE {
      CPM_EXTERNAL_TRIGGER_SOURCE_DISABLED = 0,
      CPM_EXTERNAL_TRIGGER_SOURCE_L1A1,
      CPM_EXTERNAL_TRIGGER_SOURCE_L1A2,
      CPM_EXTERNAL_TRIGGER_SOURCE_UNKNOWN
    };

  } // namespace definitions
} // namespace tcds

#endif // _tcds_cpm_Definitions_h_
