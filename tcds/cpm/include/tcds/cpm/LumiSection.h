#ifndef _tcds_cpm_LumiSection_h_
#define _tcds_cpm_LumiSection_h_

#include <vector>

#include "tcds/cpm/LumiNibble.h"
#include "tcds/cpm/LumiNibbleGroupBase.h"

namespace tcds {
  namespace cpm {

    class LumiSection : public tcds::cpm::LumiNibbleGroupBase
    {

    public:
      LumiSection(tcds::cpm::LumiNibble const& nibble);
      LumiSection(std::vector<tcds::cpm::LumiNibble> const& nibbles);
      virtual ~LumiSection();

      void addNibble(tcds::cpm::LumiNibble const& nibble);

      // A measure of the level of completeness (in terms of nibbles)
      // of this lumi section. Possible reasons for incomplete
      // sections include:
      // - Nibbles missed by the CPMController software read-out.
      // - Nibbles corruped in the software read-out (and dropped).
      // - The section at the end of a run is cut short to enable the
      //   start of the new run.
      // 0. -> incomplete, 1. -> complete.
      double completeness() const;
      // A measure of the BST signal (reception and decoding) quality.
      // 0. -> bad, 1. -> good.
      double bstSignalQuality() const;
      // A measure of how much of the time CMS was in an active
      // data-taking run (i.e., between a B-go Start and a B-go Stop).
      double fracCMSRunActive() const;

    };

  } // namespace cpm
} // namespace tcds

#endif // _tcds_cpm_LumiSection_h_
