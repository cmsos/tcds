#ifndef _tcds_cpm_TCADeviceCPMT1_h_
#define _tcds_cpm_TCADeviceCPMT1_h_

#include <stdint.h>
#include <string>
#include <vector>

#include "tcds/pm/TCADevicePMCommonBase.h"

namespace tcds {
  namespace cpm {

    /**
     * Implementation of Central Partition Manager (CPM) T1
     * functionality. The CPM is based on the AMC13 board.
     */
    class TCADeviceCPMT1 : public tcds::pm::TCADevicePMCommonBase
    {

    public:
      TCADeviceCPMT1();
      virtual ~TCADeviceCPMT1();

      // This is not particularly pretty, to have special methods to
      // access the raw version numbers (i.e., as numbers instead of
      // as strings). May need to work on this a bit.
      uint32_t readSwVersionRaw() const;
      uint32_t readFwVersionRaw() const;
      uint16_t readFEDId() const;

      void enableLPMBRILDAQTransmission() const;
      void configureTriggerAlignment() const;

      bool isExternalTrigger0Enabled() const;
      void enableExternalTrigger0() const;
      void disableExternalTrigger0() const;

      void resetTTCClockMon() const;
      void resetTTCClockPLL() const;
      void resetTTSLinks() const;
      void resetTTSLinkErrorCounters() const;

      bool isBSTReady() const;

      bool isTTSLinkUp(unsigned int const lpmNum) const;
      bool isTTSLinkAligned(unsigned int const lpmNum) const;
      uint32_t readTTSLinkCRCErrorCount(unsigned int const lpmNum) const;
      uint32_t readTTSLinkSequenceErrorCount(unsigned int const lpmNum) const;

      std::vector<uint32_t> readBRILDAQPacket() const;

    protected:
      virtual std::string readSystemIdImpl() const;
      virtual bool isTTCClockUpImpl() const;
      virtual bool isTTCClockStableImpl() const;
      virtual uint32_t readTTCClockUnlockCounterImpl() const;
      virtual std::string regNamePrefixImpl() const;

    };

  } // namespace cpm
} // namespace tcds

#endif // _tcds_cpm_TCADeviceCPMT1_h_
