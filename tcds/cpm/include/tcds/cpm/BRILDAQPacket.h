#ifndef _tcds_cpm_BRILDAQPacket_h_
#define _tcds_cpm_BRILDAQPacket_h_

#include <cstddef>
#include <stdint.h>
#include <vector>

namespace tcds {
  namespace cpm {

    // NOTE: In the firmware two nibbleDAQ packets with the exact same
    // layout are produced. The difference is in the data
    // contents. One packet contains counters ungated with any beam
    // presence mask. The other packet contains versions of these
    // counters gated with a bunch-crossing mask. (Bit 2 in the
    // bunch-mask, to be precise.)

    // The software reads both blocks at the same time, for IPbus
    // efficiency and book-keeping reasons, and then selectively
    // decodes the whole thing.

    class BRILDAQPacket
    {

    public:
      BRILDAQPacket(std::vector<uint32_t> const& rawData,
                    double const acquisitionTimestamp);
      ~BRILDAQPacket();

      bool isCorrupted() const;
      bool isAlmostCorrupted() const;
      bool isValid() const;
      bool isInvalid() const;
      bool isEmpty() const;
      uint32_t tcdsId() const;

      uint32_t bstSignalStatus() const;
      // OBSOLETE OBSOLETE OBSOLETE
      // This one is kept only for temporary backward compatibility.
      uint32_t timestamp() const;
      // OBSOLETE OBSOLETE OBSOLETE end
      // The timestamp of the last orbit of the previous nibble (!).
      double timestampBegin() const;
      // The timestamp of the last orbit of the current nibble.
      double timestampEnd() const;
      double duration() const;

      uint32_t fillNumber() const;
      uint32_t runNumber() const;
      uint32_t lumiSectionNumber(bool const fromHeader=true) const;
      uint32_t lumiNibbleNumber(bool const fromHeader=true) const;
      uint32_t orbitNumber(bool const fromHeader=true) const;
      uint32_t numOrbits() const;
      uint32_t numNibblesPerSection() const;
      bool isCMSRunActive() const;

      uint32_t numBX(bool const gated) const;

      uint32_t triggerCount(unsigned int const trigType,
                            bool const gated) const;
      uint32_t suppressedTriggerCount(unsigned int const trigType,
                                      bool const gated) const;

      uint32_t suppressedTriggerCountFromTTS(uint32_t const trigType,
                                             bool const gated=false) const;
      uint32_t suppressedTriggerCountFromTriggerRules(uint32_t const trigType,
                                                      bool const gated=false) const;
      uint32_t suppressedTriggerCountFromBunchMaskVeto(uint32_t const trigType,
                                                       bool const gated=false) const;
      uint32_t suppressedTriggerCountFromRetri(uint32_t const trigType,
                                               bool const gated=false) const;
      uint32_t suppressedTriggerCountFromAPVE(uint32_t const trigType,
                                              bool const gated=false) const;
      uint32_t suppressedTriggerCountFromDAQBackpressure(uint32_t const trigType,
                                                         bool const gated=false) const;
      uint32_t suppressedTriggerCountFromCalibration(uint32_t const trigType,
                                                     bool const gated=false) const;
      uint32_t suppressedTriggerCountFromSWPause(uint32_t const trigType,
                                                 bool const gated=false) const;
      uint32_t suppressedTriggerCountFromFWPause(uint32_t const trigType,
                                                 bool const gated=false) const;

      uint32_t deadtimeBXCountTotal(bool const gated=false) const;
      uint32_t deadtimeBXCountFromTTSAllPartitions(bool const gated=false) const;
      uint32_t deadtimeBXCountFromTTSSingleICI(unsigned int const lpmNum,
                                               unsigned int const iciNum,
                                               bool const gated=false) const;
      uint32_t deadtimeBXCountFromTTSSingleAPVE(unsigned int const lpmNum,
                                                unsigned int const apveNum,
                                                bool const gated=false) const;
      uint32_t deadtimeBXCountFromTriggerRules(bool const gated=false) const;
      uint32_t deadtimeBXCountFromBunchMaskVeto(bool const gated=false) const;
      uint32_t deadtimeBXCountFromRetri(bool const gated=false) const;
      uint32_t deadtimeBXCountFromAPVE(bool const gated=false) const;
      uint32_t deadtimeBXCountFromDAQBackpressure(bool const gated=false) const;
      uint32_t deadtimeBXCountFromCalibration(bool const gated=false) const;
      uint32_t deadtimeBXCountFromSWPause(bool const gated=false) const;
      uint32_t deadtimeBXCountFromFWPause(bool const gated=false) const;
      uint32_t deadtimeBXCountFromTriggerRule(unsigned int const ruleNum,
                                              bool const gated=false) const;

    private:
      static size_t const kGatedVersionOffset = 448;

      // NOTE: The 'header' refers to the header of the first
      // nibbleDAQ packet (i.e., the 'plain' one).
      static size_t const kIndexTCDSIdHeader = 0;
      static size_t const kIndexLumiSectionNumberHeader = 1;
      static size_t const kIndexLumiNibbleNumberHeader = 2;
      static size_t const kIndexNumberOfOrbitsHeader = 2;
      static size_t const kIndexNibblesPerSectionHeader = 2;
      static size_t const kIndexOrbitNumberHeader = 3;

      static size_t const kIndexRunNumberLo = 4;
      static size_t const kIndexRunNumberHi = 5;
      static size_t const kIndexDeadtimeBXCountTotal = 6;
      static size_t const kIndexDeadtimeBXCountTTSTotal = 7;
      static size_t const kIndexEventCountTrigType0 = 8;
      static size_t const kIndexSuppressedEventCountTrigType0 = 24;

      // There are 64 bytes of BST info, grouped into 4-byte IPbus
      // words.
      static size_t const kIndexBSTRecord = 40;

      static size_t const kIndexDeadtimeBXCountLPM1ICI1 = 57;
      static size_t const kInterCountOffset = 4;

      static size_t const kIndexDeadtimeBXCountFromTriggerRules = 257;
      static size_t const kIndexDeadtimeBXCountFromBunchMaskVeto = 250;
      static size_t const kIndexDeadtimeBXCountFromRetri = 251;
      static size_t const kIndexDeadtimeBXCountFromAPVE = 252;
      static size_t const kIndexDeadtimeBXCountFromDAQBackpressure = 253;
      static size_t const kIndexDeadtimeBXCountFromCalibration = 254;
      static size_t const kIndexDeadtimeBXCountFromSWPause = 255;
      static size_t const kIndexDeadtimeBXCountFromFWPause = 256;
      static size_t const kIndexDeadtimeBXCountFromTriggerRule1 = 258;

      static size_t const kIndexBXCount = 276;

      // We also have the suppressed trigger counts split by deadtime
      // source and by trigger type.
      static size_t const kIndexSuppressedEventCountFromTTSTrigType0 = 277;
      static size_t const kIndexSuppressedEventCountFromTriggerRulesTrigType0 = 293;
      static size_t const kIndexSuppressedEventCountFromBunchMaskVetoTrigType0 = 309;
      static size_t const kIndexSuppressedEventCountFromRetriTrigType0 = 325;
      static size_t const kIndexSuppressedEventCountFromAPVETrigType0 = 341;
      static size_t const kIndexSuppressedEventCountFromDAQBackpressureTrigType0 = 357;
      static size_t const kIndexSuppressedEventCountFromCalibrationTrigType0 = 373;
      static size_t const kIndexSuppressedEventCountFromSWPauseTrigType0 = 389;
      static size_t const kIndexSuppressedEventCountFromFWPauseTrigType0 = 405;

      // NOTE: The 'footer' refers to the footer of the second
      // nibbleDAQ packet (i.e., the 'gated' one).
      static size_t const kIndexLumiSectionNumberFooter = 445 + kGatedVersionOffset;
      static size_t const kIndexLumiNibbleNumberFooter = 446 + kGatedVersionOffset;
      static size_t const kIndexOrbitNumberFooter = 447 + kGatedVersionOffset;

      // A vector containing the results of the IPbus register block
      // containing the full BRILDAQ data block from the CPM.
      std::vector<uint32_t> rawData_;
      // A fallback timestamp in case there are problems with the BST
      // reception.
      double acquisitionTimestamp_;

    };

  } // namespace cpm
} // namespace tcds

#endif // _tcds_cpm_BRILDAQPacket_h_
