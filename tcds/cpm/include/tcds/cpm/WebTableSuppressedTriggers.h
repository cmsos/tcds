#ifndef _tcds_cpm_WebTableSuppressedTriggers_h
#define _tcds_cpm_WebTableSuppressedTriggers_h

#include <cstddef>
#include <string>

#include "tcds/utils/WebObject.h"

namespace tcds {
  namespace utils {
    class Monitor;
  }
}

namespace tcds {
  namespace cpm {

    class WebTableSuppressedTriggers : public tcds::utils::WebObject
    {

      // This is a '2D' table that lists the suppressed trigger counts
      // by deadtime/suppression source.

    public:
      WebTableSuppressedTriggers(std::string const& name,
                                 std::string const& description,
                                 tcds::utils::Monitor const& monitor,
                                 std::string const& itemSetName,
                                 std::string const& tabName,
                                 size_t const colSpan);

      std::string getHTMLString() const;

    };

  } // namespace cpm
} // namespace tcds

#endif // _tcds_cpm_WebTableSuppressedTriggers_h
