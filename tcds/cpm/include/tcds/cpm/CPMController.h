#ifndef _tcds_cpm_CPMController_h_
#define _tcds_cpm_CPMController_h_

#include <memory>
#include <stdint.h>
#include <string>
#include <vector>

#include "toolbox/Event.h"
#include "xoap/MessageReference.h"
#include "xdaq/Application.h"

#include "tcds/cpm/BRILDAQLoop.h"
#include "tcds/cpm/TCADeviceCPMT1.h"
#include "tcds/pm/PMControllerHelper.h"
#include "tcds/utils/FSMSOAPParHelper.h"
#include "tcds/utils/RegCheckResult.h"
#include "tcds/utils/SOAPCmdBase.h"
#include "tcds/utils/SOAPCmdDisableCyclicGenerator.h"
#include "tcds/utils/SOAPCmdDisableRandomTriggers.h"
#include "tcds/utils/SOAPCmdDumpHardwareState.h"
#include "tcds/utils/SOAPCmdEnableCyclicGenerator.h"
#include "tcds/utils/SOAPCmdEnableRandomTriggers.h"
#include "tcds/utils/SOAPCmdInitCyclicGenerator.h"
#include "tcds/utils/SOAPCmdInitCyclicGenerators.h"
#include "tcds/utils/SOAPCmdReadHardwareConfiguration.h"
#include "tcds/utils/SOAPCmdSendBgo.h"
#include "tcds/utils/SOAPCmdSendBgoTrain.h"
#include "tcds/utils/SOAPCmdSendL1A.h"
#include "tcds/utils/SOAPCmdSendL1APattern.h"
#include "tcds/utils/SOAPCmdStopL1APattern.h"
#include "tcds/utils/XDAQAppWithFSMForPMs.h"

namespace xdaq {
  class ApplicationStub;
}

namespace xgi {
  class Input;
  class Output;
}

namespace tcds {
  namespace hwlayer {
    class DeviceBase;
    class RegisterInfo;
  }
}

namespace tcds {
  namespace hwutilstca {
    class CyclicGensInfoSpaceHandler;
    class CyclicGensInfoSpaceUpdater;
  }
}

namespace tcds {
  namespace pm {
    class ActionsInfoSpaceHandler;
    class ActionsInfoSpaceUpdater;
    class APVEInfoSpaceHandler;
    class APVEInfoSpaceUpdater;
    class CountersInfoSpaceHandler;
    class CountersInfoSpaceUpdater;
    class L1AHistosInfoSpaceHandler;
    class L1AHistosInfoSpaceUpdater;
    class DAQInfoSpaceHandler;
    class DAQInfoSpaceUpdater;
    class ReTriInfoSpaceHandler;
    class ReTriInfoSpaceUpdater;
    class SchedulingInfoSpaceHandler;
    class SchedulingInfoSpaceUpdater;
    class SequencesInfoSpaceHandler;
    class SequencesInfoSpaceUpdater;
    class TTSCountersInfoSpaceHandler;
    class TTSCountersInfoSpaceUpdater;
    class TTSInfoSpaceHandler;
    class TTSInfoSpaceUpdater;
  }
}

namespace tcds {
  namespace utils {
    class FSMSOAPParHelper;
  }
}

namespace tcds {
  namespace cpm {

    class CPMBRILDAQInfoSpaceHandler;
    class CPMBRILDAQInfoSpaceUpdater;
    class CPMInputsInfoSpaceHandler;
    class CPMInputsInfoSpaceUpdater;
    class CPMRatesInfoSpaceHandler;
    class CPMRatesInfoSpaceUpdater;
    class CPMTTSLinksInfoSpaceHandler;
    class CPMTTSLinksInfoSpaceUpdater;
    class HwIDInfoSpaceHandlerTCA;
    class HwIDInfoSpaceUpdaterTCA;
    class HwStatusInfoSpaceHandler;
    class HwStatusInfoSpaceUpdater;
    class TCADeviceCPMT2;

    class CPMController : public tcds::utils::XDAQAppWithFSMForPMs
    {

      friend class BRILDAQLoop;
      friend class tcds::pm::PMControllerHelper;

    public:
      XDAQ_INSTANTIATOR();

      CPMController(xdaq::ApplicationStub* stub);
      virtual ~CPMController();

    protected:
      virtual void setupInfoSpaces();

      /**
       * Access the hardware pointer as TCADeviceCPMT1/2&.
       * @note
       * The main piece of hardware is the T1, but each tongue can be
       * accessed individually.
       */
      virtual tcds::cpm::TCADeviceCPMT1& getHw() const;
      tcds::cpm::TCADeviceCPMT1& getHwT1() const;
      tcds::cpm::TCADeviceCPMT2& getHwT2() const;

      virtual void hwConnectImpl();
      virtual void hwReleaseImpl();

      virtual void hwCfgInitializeImpl();
      virtual void hwCfgFinalizeImpl();

      virtual void coldResetActionImpl(toolbox::Event::Reference event);
      virtual void configureActionImpl(toolbox::Event::Reference event);
      virtual void enableActionImpl(toolbox::Event::Reference event);
      /* virtual void failActionImpl(toolbox::Event::Reference event); */
      virtual void haltActionImpl(toolbox::Event::Reference event);
      virtual void pauseActionImpl(toolbox::Event::Reference event);
      virtual void resumeActionImpl(toolbox::Event::Reference event);
      virtual void stopActionImpl(toolbox::Event::Reference event);
      virtual void ttcHardResetActionImpl(toolbox::Event::Reference event);
      virtual void ttcResyncActionImpl(toolbox::Event::Reference event);
      virtual void zeroActionImpl(toolbox::Event::Reference event);

      virtual tcds::utils::RegCheckResult isRegisterAllowed(tcds::hwlayer::RegisterInfo const& regInfo) const;

      virtual std::vector<tcds::utils::FSMSOAPParHelper> expectedFSMSoapParsImpl(std::string const& commandName) const;
      virtual void loadSOAPCommandParameterImpl(xoap::MessageReference const& msg,
                                                tcds::utils::FSMSOAPParHelper const& param);

      std::string buildLPMLabel(unsigned int const lpmNumber) const;

      /**
       * @note
       * A bit tricky. Keep the 'usual' hwP_ for the main piece of
       * hardware: the T1. Add a hwPT2_ for the T2.
       */
      std::unique_ptr<tcds::hwlayer::DeviceBase> hwT2P_;

    private:
      virtual std::string readHardwareStateImpl();

      uint32_t getSwVersion() const;

      // DIP/PSX access to the CirculatingBunchConfig publications.
      std::vector<unsigned int> getCirculatingBunchConfig(unsigned int const beamNum) const;

      // Various InfoSpaces and their InfoSpaceUpdaters.
      std::unique_ptr<tcds::pm::ActionsInfoSpaceUpdater> actionsInfoSpaceUpdaterP_;
      std::unique_ptr<tcds::pm::ActionsInfoSpaceHandler> actionsInfoSpaceP_;
      std::unique_ptr<tcds::pm::APVEInfoSpaceUpdater> apveInfoSpaceUpdaterP_;
      std::unique_ptr<tcds::pm::APVEInfoSpaceHandler> apveInfoSpaceP_;
      std::unique_ptr<tcds::cpm::CPMBRILDAQInfoSpaceUpdater> brildaqInfoSpaceUpdaterP_;
      std::unique_ptr<tcds::cpm::CPMBRILDAQInfoSpaceHandler> brildaqInfoSpaceP_;
      std::unique_ptr<tcds::pm::CountersInfoSpaceUpdater> countersInfoSpaceUpdaterP_;
      std::unique_ptr<tcds::pm::CountersInfoSpaceHandler> countersInfoSpaceP_;
      std::unique_ptr<tcds::hwutilstca::CyclicGensInfoSpaceUpdater> cyclicGensInfoSpaceUpdaterP_;
      std::unique_ptr<tcds::hwutilstca::CyclicGensInfoSpaceHandler> cyclicGensInfoSpaceP_;
      std::unique_ptr<tcds::pm::DAQInfoSpaceUpdater> daqInfoSpaceUpdaterP_;
      std::unique_ptr<tcds::pm::DAQInfoSpaceHandler> daqInfoSpaceP_;
      std::unique_ptr<tcds::cpm::HwIDInfoSpaceUpdaterTCA> hwIDInfoSpaceUpdaterP_;
      std::unique_ptr<tcds::cpm::HwIDInfoSpaceHandlerTCA> hwIDInfoSpaceP_;
      std::unique_ptr<tcds::cpm::HwStatusInfoSpaceUpdater> hwStatusInfoSpaceUpdaterP_;
      std::unique_ptr<tcds::cpm::HwStatusInfoSpaceHandler> hwStatusInfoSpaceP_;
      std::unique_ptr<tcds::cpm::CPMInputsInfoSpaceUpdater> inputsInfoSpaceUpdaterP_;
      std::unique_ptr<tcds::cpm::CPMInputsInfoSpaceHandler> inputsInfoSpaceP_;
      std::unique_ptr<tcds::pm::L1AHistosInfoSpaceUpdater> l1aHistosInfoSpaceUpdaterP_;
      std::unique_ptr<tcds::pm::L1AHistosInfoSpaceHandler> l1aHistosInfoSpaceP_;
      std::unique_ptr<tcds::cpm::CPMRatesInfoSpaceUpdater> ratesInfoSpaceUpdaterP_;
      std::unique_ptr<tcds::cpm::CPMRatesInfoSpaceHandler> ratesInfoSpaceP_;
      std::unique_ptr<tcds::pm::ReTriInfoSpaceUpdater> retriInfoSpaceUpdaterP_;
      std::unique_ptr<tcds::pm::ReTriInfoSpaceHandler> retriInfoSpaceP_;
      std::unique_ptr<tcds::pm::SchedulingInfoSpaceUpdater> schedulingInfoSpaceUpdaterP_;
      std::unique_ptr<tcds::pm::SchedulingInfoSpaceHandler> schedulingInfoSpaceP_;
      std::unique_ptr<tcds::pm::SequencesInfoSpaceUpdater> sequencesInfoSpaceUpdaterP_;
      std::unique_ptr<tcds::pm::SequencesInfoSpaceHandler> sequencesInfoSpaceP_;
      std::unique_ptr<tcds::pm::TTSCountersInfoSpaceUpdater> ttsCountersInfoSpaceUpdaterP_;
      std::unique_ptr<tcds::pm::TTSCountersInfoSpaceHandler> ttsCountersInfoSpaceP_;
      std::unique_ptr<tcds::pm::TTSInfoSpaceUpdater> ttsInfoSpaceUpdaterP_;
      std::unique_ptr<tcds::pm::TTSInfoSpaceHandler> ttsInfoSpaceP_;
      std::unique_ptr<tcds::cpm::CPMTTSLinksInfoSpaceUpdater> ttsLinksInfoSpaceUpdaterP_;
      std::unique_ptr<tcds::cpm::CPMTTSLinksInfoSpaceHandler> ttsLinksInfoSpaceP_;

      // The SOAP commands.
      template<typename> friend class tcds::utils::SOAPCmdBase;
      template<typename> friend class tcds::utils::SOAPCmdDisableCyclicGenerator;
      template<typename> friend class tcds::utils::SOAPCmdDisableRandomTriggers;
      template<typename> friend class tcds::utils::SOAPCmdDumpHardwareState;
      template<typename> friend class tcds::utils::SOAPCmdEnableCyclicGenerator;
      template<typename> friend class tcds::utils::SOAPCmdEnableRandomTriggers;
      template<typename> friend class tcds::utils::SOAPCmdInitCyclicGenerator;
      template<typename> friend class tcds::utils::SOAPCmdInitCyclicGenerators;
      template<typename> friend class tcds::utils::SOAPCmdReadHardwareConfiguration;
      template<typename> friend class tcds::utils::SOAPCmdSendBgo;
      template<typename> friend class tcds::utils::SOAPCmdSendBgoTrain;
      template<typename> friend class tcds::utils::SOAPCmdSendL1A;
      template<typename> friend class tcds::utils::SOAPCmdSendL1APattern;
      template<typename> friend class tcds::utils::SOAPCmdStopL1APattern;
      tcds::utils::SOAPCmdDisableCyclicGenerator<CPMController> soapCmdDisableCyclicGenerator_;
      tcds::utils::SOAPCmdDisableRandomTriggers<CPMController> soapCmdDisableRandomTriggers_;
      tcds::utils::SOAPCmdDumpHardwareState<CPMController> soapCmdDumpHardwareState_;
      tcds::utils::SOAPCmdEnableCyclicGenerator<CPMController> soapCmdEnableCyclicGenerator_;
      tcds::utils::SOAPCmdEnableRandomTriggers<CPMController> soapCmdEnableRandomTriggers_;
      tcds::utils::SOAPCmdInitCyclicGenerator<CPMController> soapCmdInitCyclicGenerator_;
      tcds::utils::SOAPCmdInitCyclicGenerators<CPMController> soapCmdInitCyclicGenerators_;
      tcds::utils::SOAPCmdReadHardwareConfiguration<CPMController> soapCmdReadHardwareConfiguration_;
      tcds::utils::SOAPCmdSendBgo<CPMController> soapCmdSendBgo_;
      tcds::utils::SOAPCmdSendBgoTrain<CPMController> soapCmdSendBgoTrain_;
      tcds::utils::SOAPCmdSendL1A<CPMController> soapCmdSendL1A_;
      tcds::utils::SOAPCmdSendL1APattern<CPMController> soapCmdSendL1APattern_;
      tcds::utils::SOAPCmdStopL1APattern<CPMController> soapCmdStopL1APattern_;

      // A dedicated acquisition loop for the lumi-DAQ, since that
      // needs to be 'fast and furious.'
      tcds::cpm::BRILDAQLoop brildaqLoop_;

      tcds::pm::PMControllerHelper controllerHelper_;

    };

  } // namespace cpm
} // namespace tcds

#endif // _tcds_cpm_CPMController_h_
