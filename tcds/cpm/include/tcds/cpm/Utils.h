#ifndef _tcds_cpm_Utils_h_
#define _tcds_cpm_Utils_h_

#include <string>

#include "tcds/cpm/Definitions.h"

namespace tcds {
  namespace utils {
    class InfoSpaceHandler;
  }
}

namespace tcds {
  namespace cpm {

    class LumiNibbleGroupBase;

    std::string externalTrigger0ModeToString(tcds::definitions::CPM_EXTERNAL_TRIGGER0_SOURCE_MODE const& mode);

    void CreateBRILDAQStuff(tcds::utils::InfoSpaceHandler& infoSpaceHandler);
    void UpdateBRILDAQStuff(tcds::utils::InfoSpaceHandler& infoSpaceHandler,
                            tcds::cpm::LumiNibbleGroupBase const& input);

  } // namespace cpm
} // namespace tcds

#endif // _tcds_cpm_Utils_h_
