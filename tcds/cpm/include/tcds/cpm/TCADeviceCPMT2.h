#ifndef _tcds_cpm_TCADeviceCPMT2_h_
#define _tcds_cpm_TCADeviceCPMT2_h_

#include <string>

#include "tcds/cpm/Definitions.h"
#include "tcds/hwlayertca/TCADeviceBase.h"

namespace tcds {
  namespace cpm {

    /**
     * Implementation of Central Partition Manager (CPM) T2
     * functionality. The CPM is based on the AMC13 board.
     */
    class TCADeviceCPMT2 : public tcds::hwlayertca::TCADeviceBase
    {

    public:
      TCADeviceCPMT2();
      virtual ~TCADeviceCPMT2();

      void reloadT1() const;
      void reloadT2() const;

      void resetTTCClockDCM() const;

      void setupForTCDS() const;

      // Control over which of the two front-panel GT inputs ('L1A_1'
      // or 'L1A_2') is selected.
      tcds::definitions::CPM_T2_SELECTED_GT readSelectedGTInput() const;
      void selectGTInput(tcds::definitions::CPM_T2_SELECTED_GT const input) const;

    protected:
      virtual std::string readSystemIdImpl() const;
      virtual std::string regNamePrefixImpl() const;

    };

  } // namespace cpm
} // namespace tcds

#endif // _tcds_cpm_TCADeviceCPMT2_h_
