#ifndef _tcds_cpm_ConfigurationInfoSpaceHandler_h_
#define _tcds_cpm_ConfigurationInfoSpaceHandler_h_

#include <string>

#include "tcds/utils/ConfigurationInfoSpaceHandler.h"
#include "tcds/utils/InfoSpaceHandler.h"

namespace xdaq {
  class Application;
}

namespace tcds {
  namespace utils {
    class Monitor;
    class WebServer;
  }
}

namespace tcds {
  namespace cpm {

    class ConfigurationInfoSpaceHandler :
      public tcds::utils::ConfigurationInfoSpaceHandler
    {

    public:
      ConfigurationInfoSpaceHandler(xdaq::Application& xdaqApp);

    protected:
      virtual void registerItemSetsWithMonitor(tcds::utils::Monitor& monitor);
      virtual void registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                 tcds::utils::Monitor& monitor,
                                                 std::string const& forceTabName="");

      virtual std::string formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const;

    };

  } // namespace cpm
} // namespace tcds

#endif // _tcds_cpm_ConfigurationInfoSpaceHandler_h_
