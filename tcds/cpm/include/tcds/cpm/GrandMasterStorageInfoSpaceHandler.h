#ifndef _tcds_cpm_GrandMasterStorageInfoSpaceHandler_h_
#define _tcds_cpm_GrandMasterStorageInfoSpaceHandler_h_

#include <string>

#include "tcds/utils/InfoSpaceHandler.h"

namespace tcds {
  namespace utils {
    class Monitor;
    class WebServer;
  }
}

namespace xdaq {
  class Application;
}

namespace tcds {
  namespace cpm {

    class LumiSection;

    /*! This InfoSpaceHandler is intended especially for the
      per-lumi-section publication and storage of all grandmaster book
      keeping data. It is supposed to be filled and published once per
      lumi section directly by the BRILDAQLoop. */
    class GrandMasterStorageInfoSpaceHandler : public tcds::utils::InfoSpaceHandler
    {

    public:
      GrandMasterStorageInfoSpaceHandler(xdaq::Application& xdaqApp);
      virtual ~GrandMasterStorageInfoSpaceHandler();

      void update(LumiSection const& section);

    protected:
      virtual void registerItemSetsWithMonitor(tcds::utils::Monitor& monitor);
      virtual void registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                 tcds::utils::Monitor& monitor,
                                                 std::string const& forceTabName="");

    };

  } // namespace cpm
} // namespace tcds

#endif // _tcds_cpm_GrandMasterStorageInfoSpaceHandler_h_
