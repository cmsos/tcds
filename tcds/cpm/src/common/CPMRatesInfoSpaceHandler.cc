#include "tcds/cpm/CPMRatesInfoSpaceHandler.h"

#include <cstddef>
#include <string>

#include "toolbox/string.h"

#include "tcds/cpm/WebTableSuppressedTriggers.h"
#include "tcds/pm/Definitions.h"
#include "tcds/pm/Utils.h"
#include "tcds/utils/Definitions.h"
#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/Utils.h"
#include "tcds/utils/WebServer.h"
#include "tcds/utils/XDAQAppBase.h"

namespace tcds {
  namespace utils {
    class ConfigurationInfoSpaceHandler;
  }
}

tcds::cpm::CPMRatesInfoSpaceHandler::CPMRatesInfoSpaceHandler(tcds::utils::XDAQAppBase& xdaqApp,
                                                              tcds::utils::InfoSpaceUpdater* updater) :
  InfoSpaceHandler(xdaqApp, "tcds-cpm-rates-and-deadtimes", updater)
{
  // Start with a little bit of setup (for convenience later).

  // These are the two versions of rate and deadtime
  // accounting. (I.e., normal and beam-active.)
  versions_.push_back("plain");
  versions_.push_back("beamactive");

  // These are the different deadtime sources.
  std::vector<tcds::definitions::DEADTIME_SOURCE> deadtimeSources = tcds::pm::deadtimeSourceList();

  for (std::vector<tcds::definitions::DEADTIME_SOURCE>::const_iterator it = deadtimeSources.begin();
       it != deadtimeSources.end();
       ++it)
    {
      std::string const regName = tcds::pm::deadtimeSourceToRegName(*it);
      types_.push_back(regName);
      typeDescriptions_[regName] = tcds::pm::deadtimeSourceToTitle(*it);
      typeExplanations_[regName] = tcds::pm::deadtimeSourceToDesc(*it);
    }

  //----------

  std::string name;

  //----------

  // The book keeping information.
  // NOTE: These are not shown in the monitoring, but it is published
  // in the flashlist corresponding to this InfoSpace for monitoring
  // purposes.
  createUInt32("fill_number");
  createUInt32("run_number");
  createUInt32("section_number");
  createUInt32("num_nibbles");

  //----------

  // The BeamActive bunch mask.
  createUInt32("bunch_mask_beamactive_size");
  createString("bunch_mask_beamactive");

  //----------

  // Trigger rates, total and per type, plain and beam-active.
  for (std::vector<std::string>::const_iterator version = versions_.begin();
       version != versions_.end();
       ++version)
    {
      // Total rate.
      name = toolbox::toString("brildaq.%s.trigger_rate_total",
                               version->c_str());
      createDouble(name, 0x0, "rate");
      // And per-trigger-type rates.
      for (int trigType = tcds::definitions::kTrigTypeMin;
           trigType <= tcds::definitions::kTrigTypeMax;
           ++trigType)
        {
          name = toolbox::toString("brildaq.%s.trigger_rate_trigtype%d",
                                   version->c_str(),
                                   trigType);
          createDouble(name, 0x0, "rate");
        }
    }

  //----------

  // Suppressed-trigger rates, total and per type, plain and beam-active.
  for (std::vector<std::string>::const_iterator version = versions_.begin();
       version != versions_.end();
       ++version)
    {
      name = toolbox::toString("brildaq.%s.suppressed_trigger_rate_total",
                               version->c_str());
      createDouble(name, 0x0, "rate");
      for (int trigType = tcds::definitions::kTrigTypeMin;
           trigType <= tcds::definitions::kTrigTypeMax;
           ++trigType)
        {
          name = toolbox::toString("brildaq.%s.suppressed_trigger_rate_trigtype%d",
                                   version->c_str(),
                                   trigType);
          createDouble(name, 0x0, "rate");
        }
    }

  // Deadtime, total and by source, plain and beam-active.
  for (std::vector<std::string>::const_iterator version = versions_.begin();
       version != versions_.end();
       ++version)
    {
      createDouble(toolbox::toString("brildaq.%s.deadtime_total",
                                     version->c_str()),
                   0.,
                   "percentage");
      for (std::vector<std::string>::const_iterator type = types_.begin();
           type != types_.end();
           ++type)
        {
          createDouble(toolbox::toString("brildaq.%s.deadtime_%s",
                                         version->c_str(),
                                         type->c_str()),
                       0.,
                       "percentage");
        }
    }

  // Per-trigger-rule deadtime contributions.
  for (std::vector<std::string>::const_iterator version = versions_.begin();
       version != versions_.end();
       ++version)
    {
      for (unsigned int ruleNum = tcds::definitions::kTriggerRuleMin;
           ruleNum <= tcds::definitions::kTriggerRuleMax;
           ++ruleNum)
        {
          createDouble(toolbox::toString("brildaq.%s.deadtime_trigger_rule%d",
                                         version->c_str(),
                                         ruleNum),
                       0.,
                       "percentage");
        }
    }

  //----------

  // Suppressed trigger counts by deadtime/suppression source, plain
  // and beam-active.
  for (std::vector<std::string>::const_iterator version = versions_.begin();
       version != versions_.end();
       ++version)
    {
      for (std::vector<std::string>::const_iterator type = types_.begin();
           type != types_.end();
           ++type)
        {
          for (int trigType = tcds::definitions::kTrigTypeMin;
               trigType <= tcds::definitions::kTrigTypeMax;
               ++trigType)
            {
              name = toolbox::toString("brildaq.%s.suppressed_event_counts_by_deadtime_source.%s.trigtype%d",
                                       version->c_str(),
                                       type->c_str(),
                                       trigType);
              createUInt32(name, 0);
            }
        }
    }

  //----------

  // Deadtime due to TTS, listed per partition, grouped by LPM (plain only).
  for (unsigned int lpmNum = 1; lpmNum <= tcds::definitions::kNumLPMsPerCPM; ++lpmNum)
    {
      for (unsigned int iciNum = 1; iciNum <= tcds::definitions::kNumICIsPerLPM; ++iciNum)
        {
          name = toolbox::toString("brildaq.plain.deadtime_tts_lpm%d_ici%d",
                                   lpmNum,
                                   iciNum);
          createDouble(name, 0., "percentage");
        }
      for (unsigned int apveNum = 1; apveNum <= tcds::definitions::kNumAPVEsPerLPM; ++apveNum)
        {
          name = toolbox::toString("brildaq.plain.deadtime_tts_lpm%d_apve%d",
                                   lpmNum,
                                   apveNum);
          createDouble(name, 0., "percentage");
        }
    }
}

tcds::cpm::CPMRatesInfoSpaceHandler::~CPMRatesInfoSpaceHandler()
{
}

tcds::utils::XDAQAppBase&
tcds::cpm::CPMRatesInfoSpaceHandler::getOwnerApplication() const
{
  return static_cast<tcds::utils::XDAQAppBase&>(tcds::utils::InfoSpaceHandler::getOwnerApplication());
}

void
tcds::cpm::CPMRatesInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  std::string itemSetName;

  // The BeamActive bunch mask.
  itemSetName = "itemset-beamactive-mask";
  monitor.newItemSet(itemSetName);
  monitor.addItem(itemSetName,
                  "bunch_mask_beamactive_size",
                  "Number of BeamActive BXs per orbit",
                  this,
                  "The number of colliding bunch pairs per orbit");
  monitor.addItem(itemSetName,
                  "bunch_mask_beamactive",
                  "BeamActive BXs",
                  this,
                  "List of colliding bunch pairs");

  //----------

  // Trigger rates, total and per type, plain and beam-active.
  for (std::vector<std::string>::const_iterator version = versions_.begin();
       version != versions_.end();
       ++version)
    {
      itemSetName = "itemset-latest-nibbles-rates-" + *version;
      monitor.newItemSet(itemSetName);
      monitor.addItem(itemSetName,
                      toolbox::toString("brildaq.%s.trigger_rate_total",
                                        version->c_str()),
                      "Total trigger rate",
                      this);
      for (int trigType = tcds::definitions::kTrigTypeMin;
           trigType <= tcds::definitions::kTrigTypeMax;
           ++trigType)
        {
          std::string const name =
            toolbox::toString("brildaq.%s.trigger_rate_trigtype%d",
                              version->c_str(),
                              trigType);
          std::string const trigTypeStr =
            tcds::utils::TriggerTypeToString(tcds::definitions::TRIG_TYPE(trigType));
          std::string const desc = trigTypeStr.c_str();
          monitor.addItem(itemSetName, name, desc, this);
        }
    }

  //----------

  // Suppressed-trigger rates, total and per type, plain and beam-active.
  for (std::vector<std::string>::const_iterator version = versions_.begin();
       version != versions_.end();
       ++version)
    {
      itemSetName = "itemset-latest-nibbles-suppressed-rates-" + *version;
      monitor.newItemSet(itemSetName);
      monitor.addItem(itemSetName,
                      toolbox::toString("brildaq.%s.suppressed_trigger_rate_total",
                                        version->c_str()),
                      "Total suppressed-trigger rate",
                      this);
      for (int trigType = tcds::definitions::kTrigTypeMin;
           trigType <= tcds::definitions::kTrigTypeMax;
           ++trigType)
        {
          std::string const name =
            toolbox::toString("brildaq.%s.suppressed_trigger_rate_trigtype%d",
                              version->c_str(),
                              trigType);
          std::string const trigTypeStr =
            tcds::utils::TriggerTypeToString(tcds::definitions::TRIG_TYPE(trigType));
          std::string const desc = trigTypeStr.c_str();
          monitor.addItem(itemSetName, name, desc, this);
        }
    }

  //----------

  // Deadtime, total and per source, plain and beam-active.
  for (std::vector<std::string>::const_iterator version = versions_.begin();
       version != versions_.end();
       ++version)
    {
      itemSetName = "itemset-latest-nibbles-deadtimes-" + *version;
      monitor.newItemSet(itemSetName);
      monitor.addItem(itemSetName,
                      toolbox::toString("brildaq.%s.deadtime_total",
                                        version->c_str()),
                      "Total deadtime",
                      this,
                      "The combination (i.e., OR) of all of the below");
      for (std::vector<std::string>::const_iterator type = types_.begin();
           type != types_.end();
           ++type)
        {
          monitor.addItem(itemSetName,
                          toolbox::toString("brildaq.%s.deadtime_%s",
                                            version->c_str(),
                                            type->c_str()),
                          "Deadtime due to " + typeDescriptions_[*type],
                          this,
                          "Deadtime due to " + typeExplanations_[*type]);
        }
    }

  //----------

  // Deadtime due to trigger rules, split out per trigger rule.
  for (std::vector<std::string>::const_iterator version = versions_.begin();
       version != versions_.end();
       ++version)
    {
      itemSetName = "itemset-latest-nibbles-deadtimes-trigger-rules-" + *version;
      monitor.newItemSet(itemSetName);
      for (unsigned int ruleNum = tcds::definitions::kTriggerRuleMin;
           ruleNum <= tcds::definitions::kTriggerRuleMax;
           ++ruleNum)
        {
          std::string const name =
            toolbox::toString("brildaq.%s.deadtime_trigger_rule%d",
                              version->c_str(),
                              ruleNum);
          std::string const descShort =
            toolbox::toString("Deadtime due to trigger rule %d", ruleNum);
          std::string const descLong =
            toolbox::toString("Deadtime due to trigger rule %d suppressing triggers", ruleNum);
          monitor.addItem(itemSetName, name, descShort, this, descLong);
        }
    }

  //----------

  // Suppressed trigger counts by deadtime/suppression source, plain
  // and beam-active.
  for (std::vector<std::string>::const_iterator version = versions_.begin();
       version != versions_.end();
       ++version)
    {
      itemSetName = "itemset-latest-nibbles-suppressed-triggers-by-deadtime-source-" + *version;
      monitor.newItemSet(itemSetName);
      for (std::vector<std::string>::const_iterator type = types_.begin();
           type != types_.end();
           ++type)
        {
          for (int trigType = tcds::definitions::kTrigTypeMin;
               trigType <= tcds::definitions::kTrigTypeMax;
               ++trigType)
            {
              std::string const name =
                toolbox::toString("brildaq.%s.suppressed_event_counts_by_deadtime_source.%s.trigtype%d",
                                  version->c_str(),
                                  type->c_str(),
                                  trigType);
              std::string const trigTypeStr =
                tcds::utils::TriggerTypeToString(tcds::definitions::TRIG_TYPE(trigType));
              std::string const reasonStr = "Suppressed due to " + typeDescriptions_[*type];
              std::string const descShort = toolbox::toString("%s;%s",
                                                              trigTypeStr.c_str(),
                                                              reasonStr.c_str());
              std::string const descLong = "";
              monitor.addItem(itemSetName, name, descShort, this, descLong);
            }
        }
    }

  //----------

  // Deadtime due to TTS, listed per partition, grouped by LPM (plain only).
  tcds::utils::ConfigurationInfoSpaceHandler const& cfgInfoSpace =
    getOwnerApplication().getConfigurationInfoSpaceHandler();
  for (unsigned int lpmNum = 1; lpmNum <= tcds::definitions::kNumLPMsPerCPM; ++lpmNum)
    {
      std::string const itemSetName = toolbox::toString("itemset-deadtimes-tts-lpm%d-plain", lpmNum);
      monitor.newItemSet(itemSetName);
      for (unsigned int iciNum = 1; iciNum <= tcds::definitions::kNumICIsPerLPM; ++iciNum)
        {
          std::string desc = tcds::utils::formatICILabel(lpmNum, iciNum, cfgInfoSpace, false);
          monitor.addItem(itemSetName,
                          toolbox::toString("brildaq.plain.deadtime_tts_lpm%d_ici%d",
                                            lpmNum, iciNum),
                          desc,
                          this);
        }
      for (unsigned int apveNum = 1; apveNum <= tcds::definitions::kNumAPVEsPerLPM; ++apveNum)
        {
          std::string desc = tcds::utils::formatAPVELabel(lpmNum, apveNum, cfgInfoSpace, false);
          monitor.addItem(itemSetName,
                          toolbox::toString("brildaq.plain.deadtime_tts_lpm%d_apve%d",
                                            lpmNum, apveNum),
                          desc,
                          this);
        }
    }
}

void
tcds::cpm::CPMRatesInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                   tcds::utils::Monitor& monitor,
                                                                   std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "Rates and deadtimes" : forceTabName;

  size_t const tabFullWidth = 4;
  webServer.registerTab(tabName,
                        "Information on rates, deadtimes, etc. Note: Beam-active deadtimes are shown as indication only, and should not be relied upon outside stable beams or collision runs.",
                        tabFullWidth);

  //----------

  webServer.registerTable("BeamActive BX mask",
                          "BeamActive bunch-mask configuration, " \
                          "picked up from the CirculatingBunchConfig DIP publication at Configure time",
                          monitor,
                          "itemset-beamactive-mask",
                          tabName,
                          tabFullWidth);

  //----------

  // Add a little bit of breathing room between the tables.
  webServer.registerSpacer("dummy", "", monitor, "", tabName, tabFullWidth);

  //----------

  // Plain versions.
  webServer.registerTable("Latest trigger rates",
                          "Trigger (i.e., L1A) rates from the latest nibbles read (Hz). Not gated with the active-BX mask.",
                          monitor,
                          "itemset-latest-nibbles-rates-plain",
                          tabName);

  webServer.registerTable("Latest suppressed-trigger rates",
                          "Suppressed-trigger rates from the latest nibbles read (Hz). Not gated with the active-BX mask.",
                          monitor,
                          "itemset-latest-nibbles-suppressed-rates-plain",
                          tabName);

  webServer.registerTable("Latest deadtimes",
                          "Deadtimes from the latest nibbles read (% of time). Not gated with the active-BX mask.",
                          monitor,
                          "itemset-latest-nibbles-deadtimes-plain",
                          tabName);

  webServer.registerTable("Latest trigger-rule deadtimes",
                          "Trigger-rule deadtimes from the latest nibbles read (% of time). Not gated with the active-BX mask.",
                          monitor,
                          "itemset-latest-nibbles-deadtimes-trigger-rules-plain",
                          tabName);

  //----------

  // Suppressed trigger counts by deadtime/suppression source, plain versions.
  webServer.registerWebObject<WebTableSuppressedTriggers>("Suppressed trigger counts",
                                                          "Suppressed trigger counts from the latest nibbles read, by deadtime source. Not gated with the active-BX mask.",
                                                          monitor,
                                                          "itemset-latest-nibbles-suppressed-triggers-by-deadtime-source-plain",
                                                          tabName,
                                                          tabFullWidth);

  //----------

  // Add a little bit of breathing room between the tables.
  webServer.registerSpacer("dummy", "", monitor, "", tabName, tabFullWidth);

  //----------

  // Gated (i.e., BeamActive) versions.
  webServer.registerTable("Latest trigger rates (BeamActive)",
                          "Trigger (i.e., L1A) rates from the latest nibbles read (Hz). Gated with the active-BX mask.",
                          monitor,
                          "itemset-latest-nibbles-rates-beamactive",
                          tabName);

  webServer.registerTable("Latest suppressed-trigger rates (BeamActive)",
                          "Suppressed-trigger rates from the latest nibbles read (Hz). Gated with the active-BX mask.",
                          monitor,
                          "itemset-latest-nibbles-suppressed-rates-beamactive",
                          tabName);

  webServer.registerTable("Latest deadtimes (BeamActive)",
                          "Deadtimes from the latest nibbles read (% of time). Gated with the active-BX mask.",
                          monitor,
                          "itemset-latest-nibbles-deadtimes-beamactive",
                          tabName);

  webServer.registerTable("Latest trigger-rule deadtimes (BeamActive)",
                          "Trigger-rule deadtimes from the latest nibbles read (% of time). Gated with the active-BX mask.",
                          monitor,
                          "itemset-latest-nibbles-deadtimes-trigger-rules-beamactive",
                          tabName);

  // Suppressed trigger counts by deadtime/suppression source, BeamActive versions.
  webServer.registerWebObject<WebTableSuppressedTriggers>("Suppressed trigger counts (BeamActive)",
                                                          "Suppressed trigger counts from the latest nibbles read, by deadtime source. Gated with the active-BX mask.",
                                                          monitor,
                                                          "itemset-latest-nibbles-suppressed-triggers-by-deadtime-source-beamactive",
                                                          tabName,
                                                          tabFullWidth);

  //----------

  // Add a little bit of breathing room between the tables.
  webServer.registerSpacer("dummy", "", monitor, "", tabName, tabFullWidth);

  //----------

  // Deadtime due to TTS, listed per partition, grouped by LPM (plain only).
  tcds::utils::ConfigurationInfoSpaceHandler const& cfgInfoSpace =
    getOwnerApplication().getConfigurationInfoSpaceHandler();
  for (unsigned int lpmNum = 1; lpmNum <= tcds::definitions::kNumLPMsPerCPM; ++lpmNum)
    {
      std::string const tableName =
        toolbox::toString("%s TTS deadtimes",
                          tcds::utils::formatLPMLabel(lpmNum, cfgInfoSpace).c_str());
      std::string const itemSetName = toolbox::toString("itemset-deadtimes-tts-lpm%d-plain", lpmNum);
      webServer.registerTable(tableName,
                              toolbox::toString("TTS deadtime values from LPM %d (%% of time). Not gated with the active-BX mask.", lpmNum),
                              monitor,
                              itemSetName,
                              tabName);
    }
}
