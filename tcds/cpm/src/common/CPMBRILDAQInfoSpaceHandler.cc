#include "tcds/cpm/CPMBRILDAQInfoSpaceHandler.h"

#include <stdint.h>
#include <string>

#include "toolbox/string.h"

#include "tcds/cpm/WebTableNibbleHistory.h"
#include "tcds/cpm/WebTableSectionHistory.h"
#include "tcds/utils/Definitions.h"
#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/Utils.h"
#include "tcds/utils/WebServer.h"
#include "tcds/utils/XDAQAppBase.h"

tcds::cpm::CPMBRILDAQInfoSpaceHandler::CPMBRILDAQInfoSpaceHandler(tcds::utils::XDAQAppBase& xdaqApp,
                                                                  tcds::utils::InfoSpaceUpdater* updater) :
  InfoSpaceHandler(xdaqApp, "tcds-cpm-brildaq", updater)
{
  // Some accounting/monitoring variables for ourselves.
  createUInt32("brildaq.number_nibbles_missed", 0, "", tcds::utils::InfoSpaceItem::PROCESS);
  createUInt32("brildaq.number_nibbles_read_multi", 0, "", tcds::utils::InfoSpaceItem::PROCESS);
  createUInt32("brildaq.number_nibbles_invalid", 0, "", tcds::utils::InfoSpaceItem::PROCESS);
  createUInt32("brildaq.number_nibbles_corrupted", 0, "", tcds::utils::InfoSpaceItem::PROCESS);
  createUInt32("brildaq.number_nibbles_almost_corrupted", 0, "", tcds::utils::InfoSpaceItem::PROCESS);

  //----------

  // Book keeping info from the most-recent nibble packet read.
  createUInt32("brildaq.latest_nibble.bst_signal_status");
  createUInt32("brildaq.latest_nibble.timestamp", 0, "time_val");
  createUInt32("brildaq.latest_nibble.fill_number");
  createUInt64("brildaq.latest_nibble.run_number");
  createUInt32("brildaq.latest_nibble.lumi_section_number");
  createUInt32("brildaq.latest_nibble.lumi_nibble_number");
  createBool("brildaq.latest_nibble.cms_run_active");
  createUInt32("brildaq.latest_nibble.num_orbits");
  createUInt32("brildaq.latest_nibble.num_nibbles_per_section");
  createDouble("brildaq.latest_nibble.nibble_duration", 0., "%.3f");
  createDouble("brildaq.latest_nibble.section_duration", 0., "%.2f");

  //----------

  // Book keeping info from the most-recent section built (from the
  // nibble packets).
  createUInt32("brildaq.latest_section.fill_number");
  createUInt64("brildaq.latest_section.run_number");
  createUInt32("brildaq.latest_section.lumi_section_number");
  createUInt32("brildaq.latest_section.num_nibbles");

  //----------

  // The histories of recent nibbles and sections.
  createString("nibble_history");
  createString("section_history");
}

tcds::cpm::CPMBRILDAQInfoSpaceHandler::~CPMBRILDAQInfoSpaceHandler()
{
}

tcds::utils::XDAQAppBase&
tcds::cpm::CPMBRILDAQInfoSpaceHandler::getOwnerApplication() const
{
  return static_cast<tcds::utils::XDAQAppBase&>(tcds::utils::InfoSpaceHandler::getOwnerApplication());
}

void
tcds::cpm::CPMBRILDAQInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  // Performance monitoring info.
  std::string itemSetName = "itemset-cpm-brildaq-performance";
  monitor.newItemSet(itemSetName);
  monitor.addItem(itemSetName,
                  "brildaq.number_nibbles_missed",
                  "Number of lumi-DAQ nibbles missed by the read-out in this run",
                  this,
                  "The nibble-DAQ flags gaps in the nibble acquisition. A non-zero count here indicates congestion of the IPbus connection to the CPM.");
  monitor.addItem(itemSetName,
                  "brildaq.number_nibbles_read_multi",
                  "Number of lumi-DAQ nibbles read more than once by the read-out in this run",
                  this,
                  "The CPM nibble-DAQ acquisition polls for new data. This counter indicates the number of times a poll request found a nibble packet that had already been read. A high number is a good sign.");
  monitor.addItem(itemSetName,
                  "brildaq.number_nibbles_invalid",
                  "Number of invalid lumi-DAQ nibbles read-out in this run",
                  this,
                  "Count of the number of invalid nibble packets read. A non-zero value here indicates a TCDS software/firmware problem. Invalid packets are thrown away.");
  monitor.addItem(itemSetName,
                  "brildaq.number_nibbles_corrupted",
                  "Number of corrupted lumi-DAQ nibbles read-out in this run",
                  this,
                  "Count of the number of corrupted nibble packets read. Nibble packets are corrupted when the footer does not match a trailer. A non-zero count indicates congestion of the IPbus connection to the CPM. Corrupted packets are thrown away.");
  monitor.addItem(itemSetName,
                  "brildaq.number_nibbles_almost_corrupted",
                  "Number of almost-corrupted lumi-DAQ nibbles read-out in this run",
                  this,
                  "Count of the number of almost-corrupted nibble packets read. Nibble packets are almost-corrupted when the footer does not completely match a trailer, but the mismatch is mino. A non-zero count indicates congestion of the IPbus connection to the CPM. Almost-corrupted packets are processed normally.");

  // Info from the latest nibble read.
  itemSetName = "itemset-cpm-latest-nibble";
  monitor.newItemSet(itemSetName);
  monitor.addItem(itemSetName,
                  "brildaq.latest_nibble.bst_signal_status",
                  "BST signal status",
                  this,
                  "Diagnostic describing the status of the BST signal reception.");
  monitor.addItem(itemSetName,
                  "brildaq.latest_nibble.timestamp",
                  "Timestamp",
                  this,
                  "The approximate timestamp of the start of the nibble, taken from the BST payload. If the BST reception status is not 'good', the timestamp is taken from the OS.");
  monitor.addItem(itemSetName,
                  "brildaq.latest_nibble.fill_number",
                  "LHC fill-number",
                  this);
  monitor.addItem(itemSetName,
                  "brildaq.latest_nibble.run_number",
                  "CMS run-number",
                  this);
  monitor.addItem(itemSetName,
                  "brildaq.latest_nibble.lumi_section_number",
                  "Lumi-section number",
                  this);
  monitor.addItem(itemSetName,
                  "brildaq.latest_nibble.lumi_nibble_number",
                  "Lumi-nibble number",
                  this);
  monitor.addItem(itemSetName,
                  "brildaq.latest_nibble.cms_run_active",
                  "Run active",
                  this);
  monitor.addItem(itemSetName,
                  "brildaq.latest_nibble.num_orbits",
                  "Number of orbits in this nibble",
                  this);
  monitor.addItem(itemSetName,
                  "brildaq.latest_nibble.num_nibbles_per_section",
                  "Number of nibbles per section",
                  this);
  monitor.addItem(itemSetName,
                  "brildaq.latest_nibble.nibble_duration",
                  "Lumi-nibble duration (s)",
                  this);
  monitor.addItem(itemSetName,
                  "brildaq.latest_nibble.section_duration",
                  "Lumi-section duration (s)",
                  this);

  // Info from the latest section built.
  itemSetName = "itemset-cpm-latest-section";
  monitor.newItemSet(itemSetName);
  monitor.addItem(itemSetName,
                  "brildaq.latest_section.fill_number",
                  "LHC fill-number",
                  this);
  monitor.addItem(itemSetName,
                  "brildaq.latest_section.run_number",
                  "CMS run-number",
                  this);
  monitor.addItem(itemSetName,
                  "brildaq.latest_section.lumi_section_number",
                  "Lumi-section number",
                  this);
  monitor.addItem(itemSetName,
                  "brildaq.latest_section.num_nibbles",
                  "Number of nibbles",
                  this);

  // The recent lumi nibble/section history.
  itemSetName = "itemset-cpm-nibble-history";
  monitor.newItemSet(itemSetName);
  monitor.addItem(itemSetName,
                  "nibble_history",
                  "Recent nibbles",
                  this);
  itemSetName = "itemset-cpm-section-history";
  monitor.newItemSet(itemSetName);
  monitor.addItem(itemSetName,
                  "section_history",
                  "Recent sections",
                  this);
}

void
tcds::cpm::CPMBRILDAQInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                     tcds::utils::Monitor& monitor,
                                                                     std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "BRILDAQ" : forceTabName;

  webServer.registerTab(tabName,
                        "Information on the CPM BRILDAQ acquisition status",
                        3);

  //----------

  webServer.registerTable("CPM BRILDAQ performance",
                          "CPM BRILDAQ performance monitoring info",
                          monitor,
                          "itemset-cpm-brildaq-performance",
                          tabName);

  //----------

  webServer.registerTable("Latest nibble",
                          "Identifiers of the latest good lumi nibble read",
                          monitor,
                          "itemset-cpm-latest-nibble",
                          tabName);

  webServer.registerTable("Latest section",
                          "Identifiers of the latest lumi section being assembled",
                          monitor,
                          "itemset-cpm-latest-section",
                          tabName);

  //----------

  webServer.registerWebObject<WebTableNibbleHistory>("Lumi nibble history",
                                                     "Recent lumi nibble history",
                                                     monitor,
                                                     "itemset-cpm-nibble-history",
                                                     tabName,
                                                     3);
  webServer.registerWebObject<WebTableSectionHistory>("Lumi section history",
                                                      "Recent lumi section history",
                                                      monitor,
                                                      "itemset-cpm-section-history",
                                                      tabName,
                                                      3);
}

std::string
tcds::cpm::CPMBRILDAQInfoSpaceHandler::formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const
{
  // Specialized formatting for the recent-nibbles history.
  std::string res = tcds::utils::escapeAsJSONString(tcds::utils::InfoSpaceHandler::kInvalidItemString);
  if (item->isValid())
    {
      std::string name = item->name();
      if (toolbox::endsWith(name, "bst_signal_status"))
        {
          uint32_t const value = getUInt32(name);
          tcds::definitions::BST_SIGNAL_STATUS const valueEnum =
            static_cast<tcds::definitions::BST_SIGNAL_STATUS>(value);
          res = tcds::utils::escapeAsJSONString(tcds::utils::bstStatusToString(valueEnum));
        }
      else if (name == "nibble_history")
        {
          // Special in the sense that this is something that needs to
          // be interpreted as a proper JavaScript object, so let's
          // not add any more double quotes.
          res = getString(name);
        }
      else if (name == "section_history")
        {
          // Special in the sense that this is something that needs to
          // be interpreted as a proper JavaScript object, so let's
          // not add any more double quotes.
          res = getString(name);
        }
      else
        {
          // For everything else simply call the generic formatter.
          res = InfoSpaceHandler::formatItem(item);
        }
    }
  return res;
}
