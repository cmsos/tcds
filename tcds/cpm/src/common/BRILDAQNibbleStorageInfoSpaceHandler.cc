#include "tcds/cpm/BRILDAQNibbleStorageInfoSpaceHandler.h"

#include "tcds/cpm/LumiNibble.h"
#include "tcds/cpm/LumiNibbleGroup.h"
#include "tcds/cpm/Utils.h"

tcds::cpm::BRILDAQNibbleStorageInfoSpaceHandler::BRILDAQNibbleStorageInfoSpaceHandler(xdaq::Application& xdaqApp) :
  InfoSpaceHandler(xdaqApp, "tcds-cpm-brildaqnibblestorage", 0)
{
  // The book keeping information.
  createUInt32("fill_number");
  createUInt32("run_number");
  createUInt32("section_number");
  createUInt32("nibble_number");

  // Quality/misc. information.
  createUInt32("bst_signal_status");
  createBool("run_active");

  //----------

  tcds::cpm::CreateBRILDAQStuff(*this);
}

tcds::cpm::BRILDAQNibbleStorageInfoSpaceHandler::~BRILDAQNibbleStorageInfoSpaceHandler()
{
}

void
tcds::cpm::BRILDAQNibbleStorageInfoSpaceHandler::update(LumiNibble const& nibble)
{
  // Book keeping information.
  setUInt32("fill_number", nibble.fillNumber());
  setUInt32("run_number", nibble.runNumber());
  setUInt32("section_number", nibble.lumiSectionNumber());
  setUInt32("nibble_number", nibble.lumiNibbleNumber());

  // Quality/misc. information.
  setUInt32("bst_signal_status", nibble.bstSignalStatus());
  setBool("run_active", nibble.isCMSRunActive());

  //----------

  tcds::cpm::UpdateBRILDAQStuff(*this, tcds::cpm::LumiNibbleGroup(nibble));
}

void
tcds::cpm::BRILDAQNibbleStorageInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  // Nothing to do in this case.
}

void
tcds::cpm::BRILDAQNibbleStorageInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                                tcds::utils::Monitor& monitor,
                                                                                std::string const& forceTabName)
{
  // Nothing to do in this case.
}
