#include "tcds/cpm/BRILDAQPacket.h"

#include <cassert>
#include <cmath>

#include "tcds/pm/Definitions.h"
#include "tcds/utils/Definitions.h"

tcds::cpm::BRILDAQPacket::BRILDAQPacket(std::vector<uint32_t> const& rawData,
                                        double const acquisitionTimestamp) :
  rawData_(rawData),
  acquisitionTimestamp_(acquisitionTimestamp)
{
}

tcds::cpm::BRILDAQPacket::~BRILDAQPacket()
{
}

bool
tcds::cpm::BRILDAQPacket::isCorrupted() const
{
  // The lumi-section/-nibble numbers are the first repeated data in
  // the header and footer. If these two are different, the data was
  // updated in the buffer while we were reading it.
  uint32_t const lumiSectionNumberHeader = lumiSectionNumber(true);
  uint32_t const lumiSectionNumberFooter = lumiSectionNumber(false);
  uint32_t const lumiNibbleNumberHeader = lumiNibbleNumber(true);
  uint32_t const lumiNibbleNumberFooter = lumiNibbleNumber(false);
  bool const sectionNumbersMatch = (lumiSectionNumberFooter == lumiSectionNumberHeader);
  bool const nibbleNumbersMatch = (lumiNibbleNumberFooter == lumiNibbleNumberHeader);
  return !(sectionNumbersMatch && nibbleNumbersMatch);
}

bool
tcds::cpm::BRILDAQPacket::isAlmostCorrupted() const
{
  // The orbit number is the last repeated data value in the header
  // and footer. If these two values are different, the data was
  // updated in the buffer while we were reading it, but if
  // isCorrupted() returns true, we still got all data correctly (but
  // barely in time).
  uint32_t const orbitNumberHeader = orbitNumber(true);
  uint32_t const orbitNumberFooter = orbitNumber(false);
  return !isCorrupted() && (orbitNumberFooter != orbitNumberHeader);
}

bool
tcds::cpm::BRILDAQPacket::isValid() const
{
  return !isInvalid();
}

bool
tcds::cpm::BRILDAQPacket::isInvalid() const
{
  // Sanity check on the lumi-section and lumi-nibble numbers.
  // NOTE: Before the first lumi nibble marker arrived, everything
  // will be zero. So this leads to a 'technically valid' packet that
  // still has zero lumi section and lumi nibble numbers.
  return (((lumiSectionNumber() == 0) || (lumiNibbleNumber() == 0)) && !isEmpty());
}

bool
tcds::cpm::BRILDAQPacket::isEmpty() const
{
  // NOTE: Before the first lumi nibble marker arrived, everything
  // will be zero. So this leads to a 'technically valid' packet that
  // still has zero lumi section and lumi nibble numbers.
  return (tcdsId() == 0);
}

uint32_t
tcds::cpm::BRILDAQPacket::tcdsId() const
{
  return rawData_.at(kIndexTCDSIdHeader);
}

uint32_t
tcds::cpm::BRILDAQPacket::bstSignalStatus() const
{
  // NOTE: This status is 'added to' the BST record as an extra four
  // bytes, by the CPM firmware.
  uint32_t const tmp = rawData_.at(kIndexBSTRecord + 16);
  uint32_t res = tmp;
  // Add a little protection in case we read complete garbage (or
  // simply something unforeseen).
  if ((res != tcds::definitions::BST_SIGNAL_STATUS_RESET) &&
      (res != tcds::definitions::BST_SIGNAL_STATUS_NO_SIGNAL) &&
      (res != tcds::definitions::BST_SIGNAL_STATUS_NO_DATA) &&
      (res != tcds::definitions::BST_SIGNAL_STATUS_GOOD))
    {
      res = tcds::definitions::BST_SIGNAL_STATUS_UNKNOWN;
    }
  return res;
}

uint32_t
tcds::cpm::BRILDAQPacket::timestamp() const
{
  // NOTE: The timestamp is included in the BST data. Strictly
  // speaking the first four bytes contain the sub-second part of the
  // timestamp (in microseconds) and the second four bytes the number
  // of seconds sinds 01-01-1970. We only care for the second part.

  // ASSERT ASSERT ASSERT
  // Check that the timestamp in the BST record byte-aligns with an
  // IPbus word.
  assert ((kIndexBSTRecord % 4) == 0);
  // ASSERT ASSERT ASSERT end

  uint32_t const timestamp = rawData_.at(kIndexBSTRecord + 1);
  return timestamp;
}

double
tcds::cpm::BRILDAQPacket::timestampBegin() const
{
  // The timestamp is included in the BST data. The first four
  // bytes contain the sub-second part of the timestamp (in
  // microseconds) and the second four bytes the number of seconds
  // sinds 01-01-1970.

  // NOTE: The 'plain' packet contains the BST timestamp of the begin
  // of the nibble (or, technically, the timestamp of the last orbit
  // of the previous nibble), the 'gated' packet contains the BST
  // timestamp of the end of the nibble.

  double timestamp = 0.;
  if (bstSignalStatus() == tcds::definitions::BST_SIGNAL_STATUS_GOOD)
    {
      uint32_t const microSeconds = rawData_.at(kIndexBSTRecord);
      uint32_t const seconds = rawData_.at(kIndexBSTRecord + 1);
      timestamp = seconds + 1.e-6 * microSeconds;
    }
  else
    {
      // Use the fallback timestamp.
      // NOTE: The fallback timestamp is set at acquisition time, so
      // it reflects (an approximation of) the end time of the nibble.
      timestamp = acquisitionTimestamp_ - duration();
    }

  return timestamp;
}

double
tcds::cpm::BRILDAQPacket::timestampEnd() const
{
  // The timestamp is included in the BST data. The first four
  // bytes contain the sub-second part of the timestamp (in
  // microseconds) and the second four bytes the number of seconds
  // sinds 01-01-1970.

  // NOTE: The 'plain' packet contains the BST timestamp of the begin
  // of the nibble (or, technically, the timestamp of the last orbit
  // of the previous nibble), the 'gated' packet contains the BST
  // timestamp of the end of the nibble.

  double timestamp = 0.;
  if (bstSignalStatus() == tcds::definitions::BST_SIGNAL_STATUS_GOOD)
    {
      uint32_t const microSeconds = rawData_.at(kIndexBSTRecord + kGatedVersionOffset);
      uint32_t const seconds = rawData_.at(kIndexBSTRecord + kGatedVersionOffset + 1);
      timestamp = seconds + 1.e-6 * microSeconds;
    }
  else
    {
      // Use the fallback timestamp.
      timestamp = acquisitionTimestamp_;
    }

  return timestamp;
}

double
tcds::cpm::BRILDAQPacket::duration() const
{
  return 1. * numOrbits() / tcds::definitions::kLHCOrbitFreq;
}

uint32_t
tcds::cpm::BRILDAQPacket::fillNumber() const
{
  // NOTE: The LHC fill number is included in the BST data.
  // The LHC fill number lives in bytes 22-25 of the BST record.
  // ASSERT ASSERT ASSERT
  assert (kIndexBSTRecord == 40);
  // ASSERT ASSERT ASSERT end
  uint32_t const tmp0 = rawData_.at(kIndexBSTRecord + (22 / 4));
  uint32_t const tmp1 = rawData_.at(kIndexBSTRecord + (22 / 4) + 1);
  uint32_t const fillNumber = ((tmp0 & 0xffff0000) >> 16) + ((tmp1 & 0x0000ffff) << 16);
  return fillNumber;
}

uint32_t
tcds::cpm::BRILDAQPacket::runNumber() const
{
  return rawData_.at(kIndexRunNumberLo);
}

uint32_t
tcds::cpm::BRILDAQPacket::lumiSectionNumber(bool const fromHeader) const
{
  size_t index = kIndexLumiSectionNumberHeader;
  if (!fromHeader)
    {
      index = kIndexLumiSectionNumberFooter;
    }
  return rawData_.at(index);
}

uint32_t
tcds::cpm::BRILDAQPacket::lumiNibbleNumber(bool const fromHeader) const
{
  size_t index = kIndexLumiNibbleNumberHeader;
  if (!fromHeader)
    {
      index = kIndexLumiNibbleNumberFooter;
    }
  return (rawData_.at(index) & 0x000000ff);
}

uint32_t
tcds::cpm::BRILDAQPacket::orbitNumber(bool const fromHeader) const
{
  size_t index = kIndexOrbitNumberHeader;
  if (!fromHeader)
    {
      index = kIndexOrbitNumberFooter;
    }
  return rawData_.at(index);
}

uint32_t
tcds::cpm::BRILDAQPacket::numOrbits() const
{
  uint32_t numOrbits = ((rawData_.at(kIndexNumberOfOrbitsHeader) & 0xffff0000) >> 16);
  if (numOrbits == 0)
    {
      numOrbits = 1;
    }

  return numOrbits;
}

uint32_t
tcds::cpm::BRILDAQPacket::numNibblesPerSection() const
{
  uint32_t const powerOfTwo = ((rawData_.at(kIndexNibblesPerSectionHeader) & 0x0000f000) >> 12);
  return pow(2, powerOfTwo);
}

bool
tcds::cpm::BRILDAQPacket::isCMSRunActive() const
{
  uint32_t const tmp = (rawData_.at(kIndexNibblesPerSectionHeader) & 0x00000100);
  return (tmp != 0x0);
}

uint32_t
tcds::cpm::BRILDAQPacket::numBX(bool const gated) const
{
  size_t index = kIndexBXCount;
  if (gated)
    {
      index += kGatedVersionOffset;
    }
  return rawData_.at(index);
}

uint32_t
tcds::cpm::BRILDAQPacket::triggerCount(unsigned int const trigType,
                                       bool const gated) const
{
  size_t index = kIndexEventCountTrigType0 + trigType;
  if (gated)
    {
      index += kGatedVersionOffset;
    }
  return rawData_.at(index);
}

uint32_t
tcds::cpm::BRILDAQPacket::suppressedTriggerCount(unsigned int const trigType,
                                                 bool const gated) const
{
  size_t index = kIndexSuppressedEventCountTrigType0 + trigType;
  if (gated)
    {
      index += kGatedVersionOffset;
    }
  return rawData_.at(index);
}

uint32_t
tcds::cpm::BRILDAQPacket::suppressedTriggerCountFromTTS(uint32_t const trigType,
                                                        bool const gated) const
{
  size_t index = kIndexSuppressedEventCountFromTTSTrigType0 + trigType;
  if (gated)
    {
      index += kGatedVersionOffset;
    }
  return rawData_.at(index);
}

uint32_t
tcds::cpm::BRILDAQPacket::suppressedTriggerCountFromTriggerRules(uint32_t const trigType,
                                                                 bool const gated) const
{
  size_t index = kIndexSuppressedEventCountFromTriggerRulesTrigType0 + trigType;
  if (gated)
    {
      index += kGatedVersionOffset;
    }
  return rawData_.at(index);
}

uint32_t
tcds::cpm::BRILDAQPacket::suppressedTriggerCountFromBunchMaskVeto(uint32_t const trigType,
                                                                  bool const gated) const
{
  size_t index = kIndexSuppressedEventCountFromBunchMaskVetoTrigType0 + trigType;
  if (gated)
    {
      index += kGatedVersionOffset;
    }
  return rawData_.at(index);
}

uint32_t
tcds::cpm::BRILDAQPacket::suppressedTriggerCountFromRetri(uint32_t const trigType,
                                                          bool const gated) const
{
  size_t index = kIndexSuppressedEventCountFromRetriTrigType0 + trigType;
  if (gated)
    {
      index += kGatedVersionOffset;
    }
  return rawData_.at(index);
}

uint32_t
tcds::cpm::BRILDAQPacket::suppressedTriggerCountFromAPVE(uint32_t const trigType,
                                                         bool const gated) const
{
  size_t index = kIndexSuppressedEventCountFromAPVETrigType0 + trigType;
  if (gated)
    {
      index += kGatedVersionOffset;
    }
  return rawData_.at(index);
}

uint32_t
tcds::cpm::BRILDAQPacket::suppressedTriggerCountFromDAQBackpressure(uint32_t const trigType,
                                                                    bool const gated) const
{
  size_t index = kIndexSuppressedEventCountFromDAQBackpressureTrigType0 + trigType;
  if (gated)
    {
      index += kGatedVersionOffset;
    }
  return rawData_.at(index);
}

uint32_t
tcds::cpm::BRILDAQPacket::suppressedTriggerCountFromCalibration(uint32_t const trigType,
                                                                bool const gated) const
{
  size_t index = kIndexSuppressedEventCountFromCalibrationTrigType0 + trigType;
  if (gated)
    {
      index += kGatedVersionOffset;
    }
  return rawData_.at(index);
}

uint32_t
tcds::cpm::BRILDAQPacket::suppressedTriggerCountFromSWPause(uint32_t const trigType,
                                                            bool const gated) const
{
  size_t index = kIndexSuppressedEventCountFromSWPauseTrigType0 + trigType;
  if (gated)
    {
      index += kGatedVersionOffset;
    }
  return rawData_.at(index);
}

uint32_t
tcds::cpm::BRILDAQPacket::suppressedTriggerCountFromFWPause(uint32_t const trigType,
                                                            bool const gated) const
{
  size_t index = kIndexSuppressedEventCountFromFWPauseTrigType0 + trigType;
  if (gated)
    {
      index += kGatedVersionOffset;
    }
  return rawData_.at(index);
}

uint32_t
tcds::cpm::BRILDAQPacket::deadtimeBXCountTotal(bool const gated) const
{
  size_t index = kIndexDeadtimeBXCountTotal;
  if (gated)
    {
      index += kGatedVersionOffset;
    }
  return rawData_.at(index);
}

uint32_t
tcds::cpm::BRILDAQPacket::deadtimeBXCountFromTTSAllPartitions(bool const gated) const
{
  size_t index = kIndexDeadtimeBXCountTTSTotal;
  if (gated)
    {
      index += kGatedVersionOffset;
    }
  return rawData_.at(index);
}

uint32_t
tcds::cpm::BRILDAQPacket::deadtimeBXCountFromTTSSingleICI(unsigned int const lpmNum,
                                                          unsigned int const iciNum,
                                                          bool const gated) const
{
  // NOTE: LPMs and ICIs start counting at 1, not 0.
  size_t const ttsChannelNum =
    ((lpmNum - 1) * (tcds::definitions::kNumICIsPerLPM + tcds::definitions::kNumAPVEsPerLPM + kInterCountOffset)) + (iciNum - 1);
  size_t index = kIndexDeadtimeBXCountLPM1ICI1 + ttsChannelNum;
  if (gated)
    {
      index += kGatedVersionOffset;
    }
  return rawData_.at(index);
}

uint32_t
tcds::cpm::BRILDAQPacket::deadtimeBXCountFromTTSSingleAPVE(unsigned int const lpmNum,
                                                           unsigned int const apveNum,
                                                           bool const gated) const
{
  // NOTE: LPMs and APVEs start counting at 1, not 0.
  size_t const ttsChannelNum =
    ((lpmNum - 1) * (tcds::definitions::kNumICIsPerLPM + tcds::definitions::kNumAPVEsPerLPM + kInterCountOffset)) + tcds::definitions::kNumICIsPerLPM + (apveNum - 1);
  size_t index = kIndexDeadtimeBXCountLPM1ICI1 + ttsChannelNum;
  if (gated)
    {
      index += kGatedVersionOffset;
    }
  return rawData_.at(index);
}

uint32_t
tcds::cpm::BRILDAQPacket::deadtimeBXCountFromTriggerRules(bool const gated) const
{
  size_t index = kIndexDeadtimeBXCountFromTriggerRules;
  if (gated)
    {
      index += kGatedVersionOffset;
    }
  return rawData_.at(index);
}

uint32_t
tcds::cpm::BRILDAQPacket::deadtimeBXCountFromBunchMaskVeto(bool const gated) const
{
  size_t index = kIndexDeadtimeBXCountFromBunchMaskVeto;
  if (gated)
    {
      index += kGatedVersionOffset;
    }
  return rawData_.at(index);
}

uint32_t
tcds::cpm::BRILDAQPacket::deadtimeBXCountFromRetri(bool const gated) const
{
  size_t index = kIndexDeadtimeBXCountFromRetri;
  if (gated)
    {
      index += kGatedVersionOffset;
    }
  return rawData_.at(index);
}

uint32_t
tcds::cpm::BRILDAQPacket::deadtimeBXCountFromAPVE(bool const gated) const
{
  size_t index = kIndexDeadtimeBXCountFromAPVE;
  if (gated)
    {
      index += kGatedVersionOffset;
    }
  return rawData_.at(index);
}

uint32_t
tcds::cpm::BRILDAQPacket::deadtimeBXCountFromDAQBackpressure(bool const gated) const
{
  size_t index = kIndexDeadtimeBXCountFromDAQBackpressure;
  if (gated)
    {
      index += kGatedVersionOffset;
    }
  return rawData_.at(index);
}

uint32_t
tcds::cpm::BRILDAQPacket::deadtimeBXCountFromCalibration(bool const gated) const
{
  size_t index = kIndexDeadtimeBXCountFromCalibration;
  if (gated)
    {
      index += kGatedVersionOffset;
    }
  return rawData_.at(index);
}

uint32_t
tcds::cpm::BRILDAQPacket::deadtimeBXCountFromSWPause(bool const gated) const
{
  size_t index = kIndexDeadtimeBXCountFromSWPause;
  if (gated)
    {
      index += kGatedVersionOffset;
    }
  return rawData_.at(index);
}

uint32_t
tcds::cpm::BRILDAQPacket::deadtimeBXCountFromFWPause(bool const gated) const
{
  size_t index = kIndexDeadtimeBXCountFromFWPause;
  if (gated)
    {
      index += kGatedVersionOffset;
    }
  return rawData_.at(index);
}

uint32_t
tcds::cpm::BRILDAQPacket::deadtimeBXCountFromTriggerRule(unsigned int const ruleNum,
                                                         bool const gated) const
{
  size_t index = kIndexDeadtimeBXCountFromTriggerRule1 + (ruleNum - 1);
  if (gated)
    {
      index += kGatedVersionOffset;
    }
  return rawData_.at(index);
}
