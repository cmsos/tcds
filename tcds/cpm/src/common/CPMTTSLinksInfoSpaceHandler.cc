#include "tcds/cpm/CPMTTSLinksInfoSpaceHandler.h"

#include "toolbox/string.h"

#include "tcds/pm/Definitions.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/Utils.h"
#include "tcds/utils/WebServer.h"
#include "tcds/utils/XDAQAppBase.h"

namespace tcds {
  namespace utils {
    class ConfigurationInfoSpaceHandler;
  }
}

tcds::cpm::CPMTTSLinksInfoSpaceHandler::CPMTTSLinksInfoSpaceHandler(tcds::utils::XDAQAppBase& xdaqApp,
                                                                    tcds::utils::InfoSpaceUpdater* updater) :
  InfoSpaceHandler(xdaqApp, "tcds-cpm-tts-link-status", updater)
{
  std::string name;

  // All individual LPM TTS link status flags and error counters.
  for (unsigned int lpmNum = 1; lpmNum <= tcds::definitions::kNumLPMsPerCPM; ++lpmNum)
    {
      // Link-up flag.
      createBool(toolbox::toString("backplane_tts_links_status.lpm%d.link_up",
                                   lpmNum),
                 false,
                 "true/false",
                 tcds::utils::InfoSpaceItem::PROCESS);
      // Link-aligned flag.
      createBool(toolbox::toString("backplane_tts_links_status.lpm%d.link_aligned",
                                   lpmNum),
                 false,
                 "true/false",
                 tcds::utils::InfoSpaceItem::PROCESS);
      // CRC-error counter.
      createUInt32(toolbox::toString("backplane_tts_links_status.lpm%d.crc_error_count",
                                     lpmNum),
                   0,
                   "",
                   tcds::utils::InfoSpaceItem::PROCESS);
      // Sequence-error counter.
      createUInt32(toolbox::toString("backplane_tts_links_status.lpm%d.sequence_error_count",
                                     lpmNum),
                   0,
                   "",
                   tcds::utils::InfoSpaceItem::PROCESS);
    }
}

tcds::cpm::CPMTTSLinksInfoSpaceHandler::~CPMTTSLinksInfoSpaceHandler()
{
}

tcds::utils::XDAQAppBase&
tcds::cpm::CPMTTSLinksInfoSpaceHandler::getOwnerApplication() const
{
  return static_cast<tcds::utils::XDAQAppBase&>(tcds::utils::InfoSpaceHandler::getOwnerApplication());
}

void
tcds::cpm::CPMTTSLinksInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  // All individual LPM TTS link status flags and error counters.
  for (unsigned int lpmNum = 1; lpmNum <= tcds::definitions::kNumLPMsPerCPM; ++lpmNum)
    {
      std::string const itemSetName = toolbox::toString("itemset-tts-link-states-lpm%d", lpmNum);
      monitor.newItemSet(itemSetName);

      // Link-up flag.
      monitor.addItem(itemSetName,
                      toolbox::toString("backplane_tts_links_status.lpm%d.link_up",
                                        lpmNum),
                      "Link up",
                      this);
      // Link-aligned flag.
      monitor.addItem(itemSetName,
                      toolbox::toString("backplane_tts_links_status.lpm%d.link_aligned",
                                        lpmNum),
                      "Link aligned",
                      this);
      // CRC-error counter.
      monitor.addItem(itemSetName,
                      toolbox::toString("backplane_tts_links_status.lpm%d.crc_error_count",
                                        lpmNum),
                      "CRC error count",
                      this,
                      toolbox::toString("The number of CRC errors detected on the backplane link to slot %d. Should be zero for a perfect link.",
                                        lpmNum));
      // Sequence-error counter.
      monitor.addItem(itemSetName,
                      toolbox::toString("backplane_tts_links_status.lpm%d.sequence_error_count",
                                        lpmNum),
                      "Sequence error count",
                      this,
                      toolbox::toString("The number of sequence errors detected on the backplane link to slot %d. Should be zero for a perfect link.",
                                        lpmNum));
    }
}

void
tcds::cpm::CPMTTSLinksInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                      tcds::utils::Monitor& monitor,
                                                                      std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "TTS link status" : forceTabName;

  webServer.registerTab(tabName,
                        "Information on the backplane TTS links from the LPMs to the CPM",
                        4);

  //----------

  // All individual LPM TTS link status flags and error counters.
  tcds::utils::ConfigurationInfoSpaceHandler const& cfgInfoSpace =
    getOwnerApplication().getConfigurationInfoSpaceHandler();
  for (unsigned int lpmNum = 1; lpmNum <= tcds::definitions::kNumLPMsPerCPM; ++lpmNum)
    {
      std::string const tableName =
        toolbox::toString("%s TTS link status",
                          tcds::utils::formatLPMLabel(lpmNum, cfgInfoSpace).c_str());
      std::string const itemSetName = toolbox::toString("itemset-tts-link-states-lpm%d", lpmNum);
      webServer.registerTable(tableName,
                              "",
                              monitor,
                              itemSetName,
                              tabName);
    }
}
