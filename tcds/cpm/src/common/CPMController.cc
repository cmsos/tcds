#include "tcds/cpm/CPMController.h"

#include <algorithm>
#include <iterator>
#include <list>
#include <cstddef>
#include <string>
#include <unistd.h>

#include "toolbox/string.h"
#include "xcept/Exception.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/Zone.h"
#include "xdaq/exception/ApplicationInstantiationFailed.h"
#include "xgi/Method.h"
#include "xoap/MessageFactory.h"

#include "tcds/cpm/CPMBRILDAQInfoSpaceHandler.h"
#include "tcds/cpm/CPMBRILDAQInfoSpaceUpdater.h"
#include "tcds/cpm/CPMInputsInfoSpaceHandler.h"
#include "tcds/cpm/CPMInputsInfoSpaceUpdater.h"
#include "tcds/cpm/CPMRatesInfoSpaceHandler.h"
#include "tcds/cpm/CPMRatesInfoSpaceUpdater.h"
#include "tcds/cpm/CPMTTSLinksInfoSpaceHandler.h"
#include "tcds/cpm/CPMTTSLinksInfoSpaceUpdater.h"
#include "tcds/cpm/ConfigurationInfoSpaceHandler.h"
#include "tcds/cpm/Definitions.h"
#include "tcds/cpm/HwIDInfoSpaceHandlerTCA.h"
#include "tcds/cpm/HwIDInfoSpaceUpdaterTCA.h"
#include "tcds/cpm/HwStatusInfoSpaceHandler.h"
#include "tcds/cpm/HwStatusInfoSpaceUpdater.h"
#include "tcds/cpm/TCADeviceCPMT1.h"
#include "tcds/cpm/TCADeviceCPMT2.h"
#include "tcds/cpm/version.h"
#include "tcds/exception/Exception.h"
#include "tcds/hwlayer/ConfigurationProcessor.h"
#include "tcds/hwlayer/DeviceBase.h"
#include "tcds/hwlayer/RegDumpConfigurationProcessor.h"
#include "tcds/hwlayer/RegisterInfo.h"
#include "tcds/hwutilstca/CyclicGensInfoSpaceHandler.h"
#include "tcds/hwutilstca/CyclicGensInfoSpaceUpdater.h"
#include "tcds/pm/APVEInfoSpaceHandler.h"
#include "tcds/pm/APVEInfoSpaceUpdater.h"
#include "tcds/pm/ActionsInfoSpaceHandler.h"
#include "tcds/pm/ActionsInfoSpaceUpdater.h"
#include "tcds/pm/CountersInfoSpaceHandler.h"
#include "tcds/pm/CountersInfoSpaceUpdater.h"
#include "tcds/pm/DAQInfoSpaceHandler.h"
#include "tcds/pm/DAQInfoSpaceUpdater.h"
#include "tcds/pm/Definitions.h"
#include "tcds/pm/L1AHistosInfoSpaceHandler.h"
#include "tcds/pm/L1AHistosInfoSpaceUpdater.h"
#include "tcds/pm/ReTriInfoSpaceHandler.h"
#include "tcds/pm/ReTriInfoSpaceUpdater.h"
#include "tcds/pm/SchedulingInfoSpaceHandler.h"
#include "tcds/pm/SchedulingInfoSpaceUpdater.h"
#include "tcds/pm/SequencesInfoSpaceHandler.h"
#include "tcds/pm/SequencesInfoSpaceUpdater.h"
#include "tcds/pm/TTSCountersInfoSpaceHandler.h"
#include "tcds/pm/TTSCountersInfoSpaceUpdater.h"
#include "tcds/pm/TTSInfoSpaceHandler.h"
#include "tcds/pm/TTSInfoSpaceUpdater.h"
#include "tcds/utils/Definitions.h"
#include "tcds/utils/DIPUtils.h"
#include "tcds/utils/FSMSOAPParHelper.h"
#include "tcds/utils/Lock.h"
#include "tcds/utils/LockGuard.h"
#include "tcds/utils/LogMacros.h"
#include "tcds/utils/PSXReply.h"
#include "tcds/utils/SOAPUtils.h"
#include "tcds/utils/Utils.h"

namespace xdaq {
  class ApplicationDescriptor;
}

XDAQ_INSTANTIATOR_IMPL(tcds::cpm::CPMController)

tcds::cpm::CPMController::CPMController(xdaq::ApplicationStub* stub)
try
  :
  tcds::utils::XDAQAppWithFSMForPMs(stub, std::unique_ptr<tcds::hwlayer::DeviceBase>(new tcds::cpm::TCADeviceCPMT1())),
    hwT2P_(std::unique_ptr<tcds::hwlayer::DeviceBase>(new tcds::cpm::TCADeviceCPMT2())),
    soapCmdDisableCyclicGenerator_(*this),
    soapCmdDisableRandomTriggers_(*this),
    soapCmdDumpHardwareState_(*this),
    soapCmdEnableCyclicGenerator_(*this),
    soapCmdEnableRandomTriggers_(*this),
    soapCmdInitCyclicGenerator_(*this),
    soapCmdInitCyclicGenerators_(*this),
    soapCmdReadHardwareConfiguration_(*this),
    soapCmdSendBgo_(*this),
    soapCmdSendBgoTrain_(*this),
    soapCmdSendL1A_(*this),
    soapCmdSendL1APattern_(*this),
    soapCmdStopL1APattern_(*this),
    brildaqLoop_(*this),
    controllerHelper_(*this, getHwT1())
    {
      // Create the InfoSpace holding all configuration information.
      cfgInfoSpaceP_ =
        std::unique_ptr<tcds::cpm::ConfigurationInfoSpaceHandler>(new tcds::cpm::ConfigurationInfoSpaceHandler(*this));

      // Make sure the correct default hardware configuration file is found.
      cfgInfoSpaceP_->setString("defaultHwConfigurationFilePath",
                                "${XDAQ_ROOT}/etc/tcds/cpm/hw_cfg_default_cpm.txt");
    }
catch (tcds::exception::Exception const& err)
  {
    std::string msgBase = "Something went wrong instantiating the CPMController application";
    std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.what());
    XCEPT_RAISE(xdaq::exception::ApplicationInstantiationFailed, msg.c_str());
  }

tcds::cpm::CPMController::~CPMController()
{
  hwRelease();
}

void
tcds::cpm::CPMController::setupInfoSpaces()
{
  // Make sure the configuration settings are up-to-date.
  cfgInfoSpaceP_->readInfoSpace();

  // Instantiate all hardware-dependent InfoSpaceHandlers and InfoSpaceUpdaters.
  actionsInfoSpaceUpdaterP_ = std::unique_ptr<tcds::pm::ActionsInfoSpaceUpdater>(new tcds::pm::ActionsInfoSpaceUpdater(*this, getHwT1()));
  actionsInfoSpaceP_ =
    std::unique_ptr<tcds::pm::ActionsInfoSpaceHandler>(new tcds::pm::ActionsInfoSpaceHandler(*this, actionsInfoSpaceUpdaterP_.get(), true));
  apveInfoSpaceUpdaterP_ = std::unique_ptr<tcds::pm::APVEInfoSpaceUpdater>(new tcds::pm::APVEInfoSpaceUpdater(*this, getHw()));
  apveInfoSpaceP_ =
    std::unique_ptr<tcds::pm::APVEInfoSpaceHandler>(new tcds::pm::APVEInfoSpaceHandler(*this, apveInfoSpaceUpdaterP_.get()));
  brildaqInfoSpaceUpdaterP_ = std::unique_ptr<tcds::cpm::CPMBRILDAQInfoSpaceUpdater>(new tcds::cpm::CPMBRILDAQInfoSpaceUpdater(*this, brildaqLoop_));
  brildaqInfoSpaceP_ =
    std::unique_ptr<tcds::cpm::CPMBRILDAQInfoSpaceHandler>(new tcds::cpm::CPMBRILDAQInfoSpaceHandler(*this, brildaqInfoSpaceUpdaterP_.get()));
  inputsInfoSpaceUpdaterP_ = std::unique_ptr<tcds::cpm::CPMInputsInfoSpaceUpdater>(new tcds::cpm::CPMInputsInfoSpaceUpdater(*this, getHwT1(), getHwT2()));
  inputsInfoSpaceP_ =
    std::unique_ptr<tcds::cpm::CPMInputsInfoSpaceHandler>(new tcds::cpm::CPMInputsInfoSpaceHandler(*this, inputsInfoSpaceUpdaterP_.get()));
  hwIDInfoSpaceUpdaterP_ = std::unique_ptr<tcds::cpm::HwIDInfoSpaceUpdaterTCA>(new tcds::cpm::HwIDInfoSpaceUpdaterTCA(*this, getHwT1(), getHwT2()));
  hwIDInfoSpaceP_ =
    std::unique_ptr<tcds::cpm::HwIDInfoSpaceHandlerTCA>(new tcds::cpm::HwIDInfoSpaceHandlerTCA(*this, hwIDInfoSpaceUpdaterP_.get()));
  hwStatusInfoSpaceUpdaterP_ = std::unique_ptr<tcds::cpm::HwStatusInfoSpaceUpdater>(new tcds::cpm::HwStatusInfoSpaceUpdater(*this, getHwT1(), getHwT2()));
  hwStatusInfoSpaceP_ =
    std::unique_ptr<tcds::cpm::HwStatusInfoSpaceHandler>(new tcds::cpm::HwStatusInfoSpaceHandler(*this, hwStatusInfoSpaceUpdaterP_.get()));
  countersInfoSpaceUpdaterP_ = std::unique_ptr<tcds::pm::CountersInfoSpaceUpdater>(new tcds::pm::CountersInfoSpaceUpdater(*this, getHw()));
  countersInfoSpaceP_ =
    std::unique_ptr<tcds::pm::CountersInfoSpaceHandler>(new tcds::pm::CountersInfoSpaceHandler(*this, countersInfoSpaceUpdaterP_.get()));
  cyclicGensInfoSpaceUpdaterP_ = std::unique_ptr<tcds::hwutilstca::CyclicGensInfoSpaceUpdater>(new tcds::hwutilstca::CyclicGensInfoSpaceUpdater(*this, getHw()));
  cyclicGensInfoSpaceP_ =
    std::unique_ptr<tcds::hwutilstca::CyclicGensInfoSpaceHandler>(new tcds::hwutilstca::CyclicGensInfoSpaceHandler(*this, cyclicGensInfoSpaceUpdaterP_.get(), tcds::definitions::kNumCyclicGensPerPM));
  daqInfoSpaceUpdaterP_ = std::unique_ptr<tcds::pm::DAQInfoSpaceUpdater>(new tcds::pm::DAQInfoSpaceUpdater(*this, getHw()));
  daqInfoSpaceP_ =
    std::unique_ptr<tcds::pm::DAQInfoSpaceHandler>(new tcds::pm::DAQInfoSpaceHandler(*this, daqInfoSpaceUpdaterP_.get()));
  l1aHistosInfoSpaceUpdaterP_ = std::unique_ptr<tcds::pm::L1AHistosInfoSpaceUpdater>(new tcds::pm::L1AHistosInfoSpaceUpdater(*this, getHw()));
  l1aHistosInfoSpaceP_ =
    std::unique_ptr<tcds::pm::L1AHistosInfoSpaceHandler>(new tcds::pm::L1AHistosInfoSpaceHandler(*this, l1aHistosInfoSpaceUpdaterP_.get()));
  ratesInfoSpaceUpdaterP_ = std::unique_ptr<tcds::cpm::CPMRatesInfoSpaceUpdater>(new tcds::cpm::CPMRatesInfoSpaceUpdater(*this, getHwT1(), brildaqLoop_));
  ratesInfoSpaceP_ =
    std::unique_ptr<tcds::cpm::CPMRatesInfoSpaceHandler>(new tcds::cpm::CPMRatesInfoSpaceHandler(*this, ratesInfoSpaceUpdaterP_.get()));
  retriInfoSpaceUpdaterP_ = std::unique_ptr<tcds::pm::ReTriInfoSpaceUpdater>(new tcds::pm::ReTriInfoSpaceUpdater(*this, getHw()));
  retriInfoSpaceP_ =
    std::unique_ptr<tcds::pm::ReTriInfoSpaceHandler>(new tcds::pm::ReTriInfoSpaceHandler(*this, retriInfoSpaceUpdaterP_.get()));
  schedulingInfoSpaceUpdaterP_ = std::unique_ptr<tcds::pm::SchedulingInfoSpaceUpdater>(new tcds::pm::SchedulingInfoSpaceUpdater(*this, getHwT1()));
  schedulingInfoSpaceP_ =
    std::unique_ptr<tcds::pm::SchedulingInfoSpaceHandler>(new tcds::pm::SchedulingInfoSpaceHandler(*this, schedulingInfoSpaceUpdaterP_.get()));
  sequencesInfoSpaceUpdaterP_ = std::unique_ptr<tcds::pm::SequencesInfoSpaceUpdater>(new tcds::pm::SequencesInfoSpaceUpdater(*this, getHwT1()));
  sequencesInfoSpaceP_ =
    std::unique_ptr<tcds::pm::SequencesInfoSpaceHandler>(new tcds::pm::SequencesInfoSpaceHandler(*this, sequencesInfoSpaceUpdaterP_.get()));

  ttsCountersInfoSpaceUpdaterP_ = std::unique_ptr<tcds::pm::TTSCountersInfoSpaceUpdater>(new tcds::pm::TTSCountersInfoSpaceUpdater(*this, getHwT1()));
  ttsCountersInfoSpaceP_ =
    std::unique_ptr<tcds::pm::TTSCountersInfoSpaceHandler>(new tcds::pm::TTSCountersInfoSpaceHandler(*this, ttsCountersInfoSpaceUpdaterP_.get(), true));

  ttsInfoSpaceUpdaterP_ = std::unique_ptr<tcds::pm::TTSInfoSpaceUpdater>(new tcds::pm::TTSInfoSpaceUpdater(*this, getHwT1()));
  ttsInfoSpaceP_ =
    std::unique_ptr<tcds::pm::TTSInfoSpaceHandler>(new tcds::pm::TTSInfoSpaceHandler(*this, ttsInfoSpaceUpdaterP_.get(), true));
  ttsInfoSpaceUpdaterP_ = std::unique_ptr<tcds::pm::TTSInfoSpaceUpdater>(new tcds::pm::TTSInfoSpaceUpdater(*this, getHwT1()));
  ttsInfoSpaceP_ =
    std::unique_ptr<tcds::pm::TTSInfoSpaceHandler>(new tcds::pm::TTSInfoSpaceHandler(*this, ttsInfoSpaceUpdaterP_.get(), true));

  ttsLinksInfoSpaceUpdaterP_ = std::unique_ptr<tcds::cpm::CPMTTSLinksInfoSpaceUpdater>(new tcds::cpm::CPMTTSLinksInfoSpaceUpdater(*this, getHwT1()));
  ttsLinksInfoSpaceP_ =
    std::unique_ptr<tcds::cpm::CPMTTSLinksInfoSpaceHandler>(new tcds::cpm::CPMTTSLinksInfoSpaceHandler(*this, ttsLinksInfoSpaceUpdaterP_.get()));

  // Register all InfoSpaceItems with the Monitor.
  cfgInfoSpaceP_->registerItemSets(monitor_, webServer_);
  appStateInfoSpace_.registerItemSets(monitor_, webServer_);
  hwIDInfoSpaceP_->registerItemSets(monitor_, webServer_);
  hwStatusInfoSpaceP_->registerItemSets(monitor_, webServer_);
  inputsInfoSpaceP_->registerItemSets(monitor_, webServer_);
  schedulingInfoSpaceP_->registerItemSets(monitor_, webServer_);
  sequencesInfoSpaceP_->registerItemSets(monitor_, webServer_);
  actionsInfoSpaceP_->registerItemSets(monitor_, webServer_);
  cyclicGensInfoSpaceP_->registerItemSets(monitor_, webServer_);
  retriInfoSpaceP_->registerItemSets(monitor_, webServer_);
  apveInfoSpaceP_->registerItemSets(monitor_, webServer_);
  ttsInfoSpaceP_->registerItemSets(monitor_, webServer_);
  ttsLinksInfoSpaceP_->registerItemSets(monitor_, webServer_);
  ttsCountersInfoSpaceP_->registerItemSets(monitor_, webServer_);
  countersInfoSpaceP_->registerItemSets(monitor_, webServer_);
  l1aHistosInfoSpaceP_->registerItemSets(monitor_, webServer_);
  ratesInfoSpaceP_->registerItemSets(monitor_, webServer_);
  brildaqInfoSpaceP_->registerItemSets(monitor_, webServer_);
  daqInfoSpaceP_->registerItemSets(monitor_, webServer_);

  // Force a write of all TTS InfoSpaces. This triggers an XMAS update
  // in order to populate the flashlists such that other applications
  // can pick them up.
  ttsInfoSpaceP_->writeInfoSpace(true);
}

tcds::cpm::TCADeviceCPMT1&
tcds::cpm::CPMController::getHw() const
{
  return static_cast<tcds::cpm::TCADeviceCPMT1&>(*hwP_.get());
}

tcds::cpm::TCADeviceCPMT1&
tcds::cpm::CPMController::getHwT1() const
{
  return getHw();
}

tcds::cpm::TCADeviceCPMT2&
tcds::cpm::CPMController::getHwT2() const
{
  return static_cast<tcds::cpm::TCADeviceCPMT2&>(*hwT2P_.get());
}

void
tcds::cpm::CPMController::hwConnectImpl()
{
  std::string const connectionsFileName =
    cfgInfoSpaceP_->getString("ipbusConnectionsFile");
  std::string const connectionNameT1 =
    cfgInfoSpaceP_->getString("ipbusConnectionT1");
  std::string const connectionNameT2 =
    cfgInfoSpaceP_->getString("ipbusConnectionT2");

  getHwT1().hwConnect(connectionsFileName, connectionNameT1);
  getHwT2().hwConnect(connectionsFileName, connectionNameT2);
}

void
tcds::cpm::CPMController::hwReleaseImpl()
{
  getHwT1().hwRelease();
  getHwT2().hwRelease();
}

void
tcds::cpm::CPMController::hwCfgInitializeImpl()
{
  tcds::cpm::TCADeviceCPMT1& hwT1 = getHwT1();
  tcds::cpm::TCADeviceCPMT2& hwT2 = getHwT2();

  // Check for the presence of the TTC clock. Without TTC clock it is
  // no use continuing (and apart from that, some registers will not
  // be accessible).

  // NOTE: The CPM has a slightly special clock check procedure. It
  // can be, e.g., if the clock has disappeared roughly from the T2
  // and then came back, that the clock gets lost between the T2 and
  // the T1. This situation can be recovered by following the correct
  // series of resets.
  if (!(hwT1.isTTCClockUp() && hwT1.isTTCClockStable()))
    {
      // Let's see if we can recover the clock.
      // Step 1: reset the TTC clock DCM on the T2.
      hwT2.resetTTCClockDCM();
      ::sleep(1);
      // Step 2: reset the TTC clock PLL on the T1.
      hwT1.resetTTCClockPLL();
      ::sleep(1);
    }

  // Now, if there still is no (stable) clock, we're out of luck.
  if (!(hwT1.isTTCClockUp() && hwT1.isTTCClockStable()))
    {
      std::string const msg = "Could not configure the hardware: no TTC clock present, or clock not stable.";
      ERROR(msg);
      XCEPT_RAISE(tcds::exception::TTCClockProblem, msg.c_str());
    }

  //----------

  // First setup the necessary things on the T2.
  hwT2.setupForTCDS();

  //----------

  // Now setup the usual iPM things.
  controllerHelper_.hwCfgInitialize();
}

void
tcds::cpm::CPMController::hwCfgFinalizeImpl()
{
  // Check for a valid orbit signal. Without that, it's no use
  // continuing.
  if (!getHwT1().isOrbitSignalOk())
    {
      std::string const msg = "Could not configure the hardware: no orbit signal present, or orbit signal unstable/incorrect length.";
      ERROR(msg);
      XCEPT_RAISE(tcds::exception::TTCOrbitProblem, msg.c_str());
    }

  //----------

  controllerHelper_.hwCfgFinalize();

  //----------

  // These are required APVE settings. Other values make no sense for
  // the APVE in the TCDS.
  getHwT1().writeRegister("apve.config.apv_sim_select", 0x1);
  getHwT1().writeRegister("apve.config.fmm_enable", 0x0);

  // This is also really needed.
  getHwT1().writeRegister("apve.trigger_veto_shift_register", 0x4);

  //----------

  // Configure the trigger BX-alignment registers with the right
  // values.
  getHwT1().configureTriggerAlignment();

  //----------

  // Configure the software version, in case anyone is interested.
  getHwT1().setSoftwareVersion(getSwVersion());

  //----------

  // Enable the transmission of some BRILDAQ-related information on
  // the backplane to the LPMs.
  getHwT1().enableLPMBRILDAQTransmission();

  //----------

  // Enable/disable and select the front-panel GT input depending on
  // whether or not the corresponding FEDs are in the run (which is
  // determined from the FED enable mask).

  // NOTE: The choice here is between the 'L1A_1' and 'L1A_2'
  // front-panel inputs. Only one can be enabled (as physics trigger)
  // at a time. The approach here is:
  // - If the FED connected to 'L1A_1' is in the run -> Select 'L1A_1'
  //   and enable external_trigger0.
  // - Else, if the FED connected to 'L1A_2' is in the run -> Select
  //   'L1A_2' and enable external_trigger0.
  // - Else -> Disable external_trigger0.
  std::string const fedEnableMask = cfgInfoSpaceP_->getString("fedEnableMask");
  uint32_t const fedIdGT1 = cfgInfoSpaceP_->getUInt32("fedIdGT1");
  uint32_t const fedIdGT2 = cfgInfoSpaceP_->getUInt32("fedIdGT2");
  bool const enableGT1 = tcds::utils::isFedInRun(fedIdGT1, fedEnableMask);
  bool const enableGT2 = tcds::utils::isFedInRun(fedIdGT2, fedEnableMask);
  if (enableGT1)
    {
      getHwT2().selectGTInput(tcds::definitions::CPM_T2_SELECTED_GT_L1A1);
      getHwT1().enableExternalTrigger0();
    }
  else if (enableGT2)
    {
      getHwT2().selectGTInput(tcds::definitions::CPM_T2_SELECTED_GT_L1A2);
      getHwT1().enableExternalTrigger0();
    }
  else
    {
      getHwT1().disableExternalTrigger0();
    }

  //----------

  // Enable/disable DAQ backpressure based on whether we're going to
  // be read out or not.
  uint32_t const fedId = cfgInfoSpaceP_->getUInt32("fedId");
  bool const useDAQLink = tcds::utils::doesFedDoDAQ(fedId, fedEnableMask);
  if (useDAQLink)
    {
      getHwT1().enableDAQBackpressure();
    }
  else
    {
      getHwT1().disableDAQBackpressure();
    }

  //----------

  // Enable/disable TTS inputs based on whether or not each of the
  // partitions is included in the run.
  std::string const ttcPartitionMap = cfgInfoSpaceP_->getString("ttcPartitionMap");

  // For all LPMs.
  for (unsigned int lpmNum = 1; lpmNum <= tcds::definitions::kNumLPMsPerCPM; ++lpmNum)
    {
      // For all iCIs.
      for (unsigned int iciNum = 1; iciNum <= tcds::definitions::kNumICIsPerLPM; ++iciNum)
        {
          std::string const tmp = toolbox::toString("partitionLabelLPM%dICI%d", lpmNum, iciNum);
          std::string const partitionName = cfgInfoSpaceP_->getString(tmp);
          bool const partitionDoesTTS = tcds::utils::doesPartitionDoTTS(partitionName, ttcPartitionMap);
          if (partitionDoesTTS)
            {
              getHwT1().enableTTSFromICI(lpmNum, iciNum);
            }
          else
            {
              getHwT1().disableTTSFromICI(lpmNum, iciNum);
            }
        }

      // For all APVEs.
      for (unsigned int apveNum = 1; apveNum <= tcds::definitions::kNumAPVEsPerLPM; ++apveNum)
        {
          std::string const tmp = toolbox::toString("partitionLabelLPM%dAPVE%d", lpmNum, apveNum);
          std::string const partitionName = cfgInfoSpaceP_->getString(tmp);
          bool const partitionDoesTTS = tcds::utils::doesPartitionDoTTS(partitionName, ttcPartitionMap);
          if (partitionDoesTTS)
            {
              getHwT1().enableTTSFromAPVE(lpmNum, apveNum);
            }
          else
            {
              getHwT1().disableTTSFromAPVE(lpmNum, apveNum);
            }
        }
    }

  //----------

  // If asked to do so, pull the circulating bunch configuration from
  // DIP/PSX and apply that as mask for the beam-active deadtime
  // calculations.
  bool const useBeamActiveMask = !(cfgInfoSpaceP_->getBool("noBeamActive"));
  if (useBeamActiveMask)
    {
      // Get circulating bunch patterns from DIP/PSX.
      // NOTE: These are RF bucket numbers.
      //   https://wikis.cern.ch/display/expcomm/CirculatingBunchConfig
      std::vector<unsigned int> bunchPatternBeam1;
      std::vector<unsigned int> bunchPatternBeam2;
      try
        {
          bunchPatternBeam1 = getCirculatingBunchConfig(1);
          bunchPatternBeam2 = getCirculatingBunchConfig(2);
        }
      catch (xcept::Exception const& err)
        {
          std::string msgBase = "Problem obtaining CirculatingBunchConfig information from DIP/PSX)";
          std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.what());
          XCEPT_RAISE(tcds::exception::RuntimeProblem, msg.c_str());
        }

      // Remove zeros.
      bunchPatternBeam1.erase(std::remove(bunchPatternBeam1.begin(),
                                          bunchPatternBeam1.end(),
                                          0),
                              bunchPatternBeam1.end());
      bunchPatternBeam2.erase(std::remove(bunchPatternBeam2.begin(),
                                          bunchPatternBeam2.end(),
                                          0),
                              bunchPatternBeam2.end());

      // Convert from bucket to BX numbers.
      std::vector<unsigned int> bxPatternBeam1(bunchPatternBeam1.size());
      std::vector<unsigned int> bxPatternBeam2(bunchPatternBeam2.size());
      std::transform(bunchPatternBeam1.begin(),
                     bunchPatternBeam1.end(),
                     bxPatternBeam1.begin(),
                     tcds::utils::bucketToBX);
      std::transform(bunchPatternBeam2.begin(),
                     bunchPatternBeam2.end(),
                     bxPatternBeam2.begin(),
                     tcds::utils::bucketToBX);

      // Find the colliding bunch pairs at CMS.
      std::vector<unsigned int> collidingPairs;
      // Step 1: sort.
      std::sort(bxPatternBeam1.begin(), bxPatternBeam1.end());
      std::sort(bxPatternBeam2.begin(), bxPatternBeam2.end());
      // Step 2: find intersection.
      std::set_intersection(bxPatternBeam1.begin(),
                            bxPatternBeam1.end(),
                            bxPatternBeam2.begin(),
                            bxPatternBeam2.end(),
                            std::back_inserter(collidingPairs));

      // Turn the colliding-pair info into a mask.
      std::vector<bool> mask(tcds::definitions::kNumBXPerOrbit, false);
      for (std::vector<unsigned int>::const_iterator i = collidingPairs.begin();
           i != collidingPairs.end();
           ++i)
        {
          // NOTE: Subtract one, since BX numbers start at 1 and
          // vector indices start at 0.
          mask.at(*i - 1) = true;
        }

      // Apply the mask in the hardware.
      getHwT1().writeBunchMaskBeamActiveBXs(mask);
    }
}

void
tcds::cpm::CPMController::configureActionImpl(toolbox::Event::Reference event)
{
  // During configuration, of course we cannot guarantee what comes
  // out of the BRILDAQ. So let's disable that for the moment.
  INFO("Stopping BRILDAQ loop at start of Configure.");
  brildaqLoop_.stop();

  //----------

  tcds::utils::XDAQAppWithFSMForPMs::configureActionImpl(event);

  //----------

  // Configuration done. Switch BRILDAQ acquisition back on.
  INFO("Starting BRILDAQ loop at end of Configure.");
  brildaqLoop_.start();
}

void
tcds::cpm::CPMController::coldResetActionImpl(toolbox::Event::Reference event)
{
  // Connect to the hardware.
  hwConnect();

  // Reload the firmware on the T1.
  getHwT2().reloadT1();

  // Release the hardware.
  hwRelease();

  // Reconnect to the hardware. This method will poll while trying to
  // reconnect, so this is a nice way to wait till the board is back
  // online.
  hwConnect();

  // Reset the GTXs of the backplane TTS links from the LPMs. Just to
  // make sure these don't get stuck.
  getHwT1().resetTTSLinks();

  // Finally release to clean up.
  hwRelease();
}

void
tcds::cpm::CPMController::enableActionImpl(toolbox::Event::Reference event)
{
  controllerHelper_.enableAction(event);
}

// void
// tcds::cpm::CPMController::failActionImpl(toolbox::Event::Reference event)
// {
// }

void
tcds::cpm::CPMController::haltActionImpl(toolbox::Event::Reference event)
{
  // Stop the BRILDAQ loop so we can disconnect from the hardware.
  INFO("Stopping BRILDAQ loop in Halt before disconnecting from hardware.");
  brildaqLoop_.stop();

  // Then do the usual thing.
  // controllerHelper_.haltAction(event);
  tcds::utils::XDAQAppWithFSMForPMs::haltActionImpl(event);
}

void
tcds::cpm::CPMController::pauseActionImpl(toolbox::Event::Reference event)
{
  controllerHelper_.pauseAction(event);
}

void
tcds::cpm::CPMController::resumeActionImpl(toolbox::Event::Reference event)
{
  controllerHelper_.resumeAction(event);
}

void
tcds::cpm::CPMController::stopActionImpl(toolbox::Event::Reference event)
{
  controllerHelper_.stopAction(event);
}

void
tcds::cpm::CPMController::ttcHardResetActionImpl(toolbox::Event::Reference event)
{
  controllerHelper_.ttcHardResetAction(event);
}

void
tcds::cpm::CPMController::ttcResyncActionImpl(toolbox::Event::Reference event)
{
  controllerHelper_.ttcResyncAction(event);
}

void
tcds::cpm::CPMController::zeroActionImpl(toolbox::Event::Reference event)
{
  controllerHelper_.zeroAction(event);

  // Reset the TTC clock monitoring counters.
  getHwT1().resetTTCClockMon();

  // Reset the TTS link error counters.
  getHwT1().resetTTSLinkErrorCounters();

  // Reset the book-keeping counters in the lumi-DAQ loop.
  brildaqLoop_.resetCounters();

  // Reset the APVE counters.
  getHwT1().writeRegister("apve.ttc_stats.reset", 0x1);

  // BUG BUG BUG
  // Force a write of all TTS InfoSpaces. This triggers an XMAS update
  // in order to populate the flashlists such that other applications
  // can pick them up.
  ttsInfoSpaceP_->writeInfoSpace(true);
  // BUG BUG BUG end
}

tcds::utils::RegCheckResult
tcds::cpm::CPMController::isRegisterAllowed(tcds::hwlayer::RegisterInfo const& regInfo) const
{
  // Start by applying the default selection.
  tcds::utils::RegCheckResult res = tcds::utils::XDAQAppWithFSMForPMs::isRegisterAllowed(regInfo);

  // Then the XPM-common selection.
  if (res)
    {
      res = controllerHelper_.isRegisterAllowed(regInfo);
    }

  //----------

  // Special for the CPM: the 'semaphore' registers used for internal
  // LPM software business are not supposed to be accessed in the CPM.
  res = (res && (regInfo.name().find("lpm_external_trigger") == std::string::npos))
    ? res : tcds::utils::kRegCheckResultDisallowed;

  //----------

  // The first external trigger input (presumably originating from the
  // GT) is now handled automatically based on the FED enable
  // mask. (The second external trigger input is the 'auxiliary'
  // trigger and that is still controlled manually from the
  // configuration.
  res = (res && (regInfo.name().find("external_trigger0_enable") == std::string::npos))
    ? res : tcds::utils::kRegCheckResultDisallowed;

  //----------

  // Special for the CPM: the following register of course does not
  // make sense in the CPM.
  res = (res && (regInfo.name().find("external_trigger_from_cpm") == std::string::npos))
    ? res : tcds::utils::kRegCheckResultDisallowed;

  // And the same holds for this one.
  res = (res && (regInfo.name().find("cpm_orbit_select") == std::string::npos))
    ? res : tcds::utils::kRegCheckResultDisallowed;

  //----------

  // APVE-related.

  // There is no FMM, so this register should not be messed with.
  res = (res && !toolbox::endsWith(regInfo.name(), "config.fmm_enable"))
    ? res : tcds::utils::kRegCheckResultDisallowed;

  // There is no real APV, so this register should not be messed with.
  res = (res && !toolbox::endsWith(regInfo.name(), "config.apv_sim_select"))
    ? res : tcds::utils::kRegCheckResultDisallowed;

  //----------

  return res;
}

std::vector<tcds::utils::FSMSOAPParHelper>
tcds::cpm::CPMController::expectedFSMSoapParsImpl(std::string const& commandName) const
{
  // Define what we expect in terms of parameters for each SOAP FSM
  // command.
  std::vector<tcds::utils::FSMSOAPParHelper> params =
    tcds::utils::XDAQAppWithFSMForPMs::expectedFSMSoapParsImpl(commandName);

  // Configure for the CPMController takes the usual parameters as
  // well as 'xdaq:fedEnableMask', 'xdaq:ttcPartitionMap', and
  // 'xdaq::noBeamActive'.
  if ((commandName == "Configure") ||
      (commandName == "Reconfigure"))
    {
      params.push_back(tcds::utils::FSMSOAPParHelper(commandName, "fedEnableMask", true));
      params.push_back(tcds::utils::FSMSOAPParHelper(commandName, "ttcPartitionMap", true));
      params.push_back(tcds::utils::FSMSOAPParHelper(commandName, "noBeamActive", true));
    }
  return params;
}

void
tcds::cpm::CPMController::loadSOAPCommandParameterImpl(xoap::MessageReference const& msg,
                                                       tcds::utils::FSMSOAPParHelper const& param)
{
  // NOTE: The assumption is that when entering this method, the
  // command-to-parameter mapping, required parameter presence,
  // etc. have all been checked already. This method only concerns the
  // actual loading of the parameters.

  std::string const parName = param.parameterName();

  if (parName == "fedEnableMask")
    {
      // Import 'xdaq:fedEnableMask'.
      std::string const fedEnableMask =
        tcds::utils::soap::extractSOAPCommandParameterString(msg, parName);
      cfgInfoSpaceP_->setString(parName, fedEnableMask);
    }
  else if (parName == "ttcPartitionMap")
    {
      // Import 'xdaq:ttcPartitionMap'.
      std::string const ttcPartitionMap =
        tcds::utils::soap::extractSOAPCommandParameterString(msg, parName);
      cfgInfoSpaceP_->setString(parName, ttcPartitionMap);
    }
  else if (parName == "noBeamActive")
    {
      // Import 'xdaq:noBeamActive'.
      bool const noBeamActive =
        tcds::utils::soap::extractSOAPCommandParameterBool(msg, parName);
      cfgInfoSpaceP_->setBool(parName, noBeamActive);
    }
  else
    {
      // For other commands: use the usual approach.
      tcds::utils::XDAQAppWithFSMForPMs::loadSOAPCommandParameterImpl(msg, param);
    }
}

std::string
tcds::cpm::CPMController::buildLPMLabel(unsigned int const lpmNumber) const
{
  return controllerHelper_.buildLPMLabel(lpmNumber);
}

std::string
tcds::cpm::CPMController::readHardwareStateImpl()
{
  // NOTE: Things like hwConnect(), hwRelease(), etc., are protected
  // by a lock in the FSM state transitions. In this case we have to
  // take care of locking ourselves. This ensures that nothing fiddles
  // with the hardware while we're dumping the state.
  tcds::utils::LockGuard<tcds::utils::Lock> guardedLock(monitoringLock_);

  // NOTE: The assumption here is that both Ts are connected and
  // disconnected at the same time. (This was also the way the rest of
  // the code was intended.)
  bool const hwWasConnectedT1 = getHwT1().isReadyForUse();
  bool const hwWasConnectedT2 = getHwT2().isReadyForUse();
  bool const hwWasConnected = hwWasConnectedT1 && hwWasConnectedT2;

  if (!hwWasConnected)
    {
      hwConnect();
    }

  tcds::hwlayer::ConfigurationProcessor::RegValVec hwContentsVecT1 = getHwT1().dumpRegisterContents();
  tcds::hwlayer::ConfigurationProcessor::RegValVec hwContentsVecT2 = getHwT2().dumpRegisterContents();

  if (!hwWasConnected)
    {
      hwRelease();
    }

  // Now turn the register contents we found into something that we
  // can stuff into a SOAP message.
  tcds::hwlayer::RegDumpConfigurationProcessor cfgProcessor;
  std::string const resT1 = cfgProcessor.compose(hwContentsVecT1);
  std::string const resT2 = cfgProcessor.compose(hwContentsVecT2);
  std::string const res = resT1 + resT2;
  return res;
}

uint32_t
tcds::cpm::CPMController::getSwVersion() const
{
  // NOTE: No checks on ranges are made before masking. Gonna break at
  // some point...
  uint32_t const swVersion =
    ((TCDS_CPM_VERSION_MAJOR & 0xff) << 24) +
    ((TCDS_CPM_VERSION_MINOR & 0xfff) << 12) +
    (TCDS_CPM_VERSION_PATCH & 0xfff);
  return swVersion;
}

std::vector<unsigned int>
tcds::cpm::CPMController::getCirculatingBunchConfig(unsigned int const beamNum) const
{
  // Figure out the location of the PSX server.
  xdaq::ApplicationDescriptor const* dest = 0;
  try
    {
      dest = getApplicationContext()->getDefaultZone()->getApplicationDescriptor("psx::Application", 0);
    }
  catch (xcept::Exception const& err)
    {
      std::string const msgBase = "Could not locate the PSX application"
        " (in order to obtain the CirculatingBunchConfig information)";
      std::string const msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.what());
      XCEPT_RAISE(tcds::exception::RuntimeProblem, msg.c_str());
    }

  // NOTE: The fact that these DIP paths are hard-coded here is not a
  // big deal. Different publications have different formats and/or
  // types, so this is not truely configurable anyway. The PVSS host
  // is configurable though.
  std::string const pvssHost = cfgInfoSpaceP_->getString("pvssHost");
  std::string const dpName =
    toolbox::toString("%s:ExternalApps/Accelerator/LHC.RunControl.CirculatingBunchConfig.Beam%d",
                      pvssHost.c_str(),
                      beamNum);
  xoap::MessageReference dpGetCmd = tcds::utils::makePSXDPGetSOAPCmd(dpName);

  // Get the DIP data point contents from the PSX server.
  xoap::MessageReference reply = xoap::createMessage();
  try
    {
      reply = executeSOAPCommand(dpGetCmd, *dest);
    }
  catch (xcept::Exception const& err)
    {
      std::string const msgBase = "Could not contact the PSX application"
        " (in order to obtain the CirculatingBunchConfig information)";
      std::string const msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.what());
      XCEPT_RAISE(tcds::exception::RuntimeProblem, msg.c_str());
    }

  // // Check for server-side(-reported) issues.
  // if (tcds::utils::soap::hasFault(reply))
  //   {
  //     std::string const msgBase = "Received fault from PSX server "
  //       " (when trying to obtain the CirculatingBunchConfig information)";
  //     std::string const msgFault = tcds::utils::soap::extractFaultString(reply);
  //     std::string msg = toolbox::toString("%s: '%s'", msgBase.c_str(), msgFault.c_str());
  //     if (tcds::utils::soap::hasFaultDetail(reply))
  //       {
  //         msg += toolbox::toString("(%s).", tcds::utils::soap::extractFaultDetail(reply));
  //       }
  //     else
  //       {
  //         msg += ".";
  //       }
  //     XCEPT_RAISE(tcds::exception::RuntimeProblem, msg.c_str());
  //   }

  tcds::utils::PSXReply psxReply(reply);

  // Check if we can trust the DIP publication.
  // - The data point has to be valid.
  //   NOTE: The valid bit is set by the DIP client inserting the data
  //   point into WinCCOA.
  if (!psxReply.isValid(dpName))
    {
      std::string const msg =
        toolbox::toString("WinCCOA data point for DIP publication '%s' is invalid."
                          " NOTE: The invalid bit is set by the DIP client"
                          " and typically indicates a problem with the publication timestamp.",
                          dpName.c_str());
      XCEPT_RAISE(tcds::exception::RuntimeProblem, msg.c_str());
    }
  // - The data point has to have data quality 'GOOD'.
  //   NOTE: The data quality is set by the DIP publisher.
  std::string const dpDIPQual = psxReply.extractDIPQuality(dpName);
  std::string const dipQualRequired = tcds::utils::dipQualToString(tcds::utils::DIP_QUAL_GOOD);
  if (dpDIPQual != dipQualRequired)
    {
      std::string const msg =
        toolbox::toString("DIP publication quality for publication '%s'"
                          " is '%s' (instead of the required '%s').",
                          dpName.c_str(),
                          dpDIPQual.c_str(),
                          dipQualRequired.c_str());
      XCEPT_RAISE(tcds::exception::RuntimeProblem, msg.c_str());
    }

  // Last step: parse whatever we received into a list of RF bucket
  // numbers.
  std::string const rawRes = psxReply.extractRawResult(dpName);

  std::list<std::string> const pieces = toolbox::parseTokenList(rawRes, "|");

  // The DIP publication should contain exactly 2808 bunch slots. If
  // not, something is wrong.
  size_t const expectedDIPSize = 2808;
  if (pieces.size() != expectedDIPSize)
    {
      std::string msg =
        toolbox::toString("Expected %d bunch slots in '%s' DIP publication, but found %d.",
                          expectedDIPSize,
                          dpName.c_str(),
                          pieces.size());
      XCEPT_RAISE(tcds::exception::RuntimeProblem, msg.c_str());
    }

  std::vector<unsigned int> res(pieces.size());
  size_t index = 0;
  for (std::list<std::string>::const_iterator i = pieces.begin();
       i != pieces.end();
       ++i)
    {
      res.at(index) = toolbox::toUnsignedLong(toolbox::trim(*i));
      ++index;
    }

  return res;
}
