#include "tcds/cpm/CPMBRILDAQInfoSpaceUpdater.h"

#include <string>
#include <vector>

#include "tcds/cpm/BRILDAQLoop.h"
#include "tcds/cpm/BRILDAQSnapshot.h"
#include "tcds/cpm/LumiNibble.h"
#include "tcds/cpm/LumiSection.h"
#include "tcds/cpm/NibbleHistory.h"
#include "tcds/cpm/SectionHistory.h"
#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/XDAQAppBase.h"

tcds::cpm::CPMBRILDAQInfoSpaceUpdater::CPMBRILDAQInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                                                  tcds::cpm::BRILDAQLoop const& brildaqLoop) :
  tcds::utils::InfoSpaceUpdater(xdaqApp),
  brildaqLoop_(brildaqLoop)
{
}

tcds::cpm::CPMBRILDAQInfoSpaceUpdater::~CPMBRILDAQInfoSpaceUpdater()
{
}

void
tcds::cpm::CPMBRILDAQInfoSpaceUpdater::updateInfoSpaceImpl(tcds::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  if (!brildaqLoop_.isHwConnected())
    {
      infoSpaceHandler->setInvalid();
    }
  else
    {
      infoSpaceHandler->setValid();

      // Store latest-nibble data.
      BRILDAQSnapshot const snapshot = brildaqLoop_.snapshot();
      std::vector<LumiNibble> const recentNibbles = snapshot.recentNibbles();
      if (recentNibbles.size() > 0)
        {
          LumiNibble const& latestNibble = recentNibbles.back();

          // Information from the most recent nible.
          infoSpaceHandler->setUInt32("brildaq.latest_nibble.bst_signal_status",
                                      latestNibble.bstSignalStatus());
          infoSpaceHandler->setUInt32("brildaq.latest_nibble.timestamp",
                                      latestNibble.timestampBegin());
          infoSpaceHandler->setUInt32("brildaq.latest_nibble.fill_number",
                                      latestNibble.fillNumber());
          infoSpaceHandler->setUInt64("brildaq.latest_nibble.run_number",
                                      latestNibble.runNumber());
          infoSpaceHandler->setUInt32("brildaq.latest_nibble.lumi_section_number",
                                      latestNibble.lumiSectionNumber());
          infoSpaceHandler->setUInt32("brildaq.latest_nibble.lumi_nibble_number",
                                      latestNibble.lumiNibbleNumber());
          infoSpaceHandler->setBool("brildaq.latest_nibble.cms_run_active",
                                    latestNibble.isCMSRunActive());
          infoSpaceHandler->setUInt32("brildaq.latest_nibble.num_orbits",
                                      latestNibble.numOrbits());
          infoSpaceHandler->setUInt32("brildaq.latest_nibble.num_nibbles_per_section",
                                      latestNibble.numNibblesPerSection());
          infoSpaceHandler->setDouble("brildaq.latest_nibble.nibble_duration",
                                      latestNibble.lumiNibbleDuration());
          infoSpaceHandler->setDouble("brildaq.latest_nibble.section_duration",
                                      latestNibble.lumiSectionDuration());
        }

      //----------

      std::vector<LumiSection> const recentSections = snapshot.recentSections();
      if (recentSections.size() > 0)
        {
          LumiSection const& latestSection = recentSections.back();

          // Information from the most recent nible.
          infoSpaceHandler->setUInt32("brildaq.latest_section.fill_number",
                                      latestSection.fillNumber());
          infoSpaceHandler->setUInt64("brildaq.latest_section.run_number",
                                      latestSection.runNumber());
          infoSpaceHandler->setUInt32("brildaq.latest_section.lumi_section_number",
                                      latestSection.lumiSectionNumber());
          infoSpaceHandler->setUInt32("brildaq.latest_section.num_nibbles",
                                      latestSection.numNibbles());
        }

      //----------

      // Performance/corruption monitoring.
      infoSpaceHandler->setUInt32("brildaq.number_nibbles_missed",
                                  snapshot.numNibblesMissed());
      infoSpaceHandler->setUInt32("brildaq.number_nibbles_read_multi",
                                  snapshot.numNibblesReadMulti());
      infoSpaceHandler->setUInt32("brildaq.number_nibbles_invalid",
                                  snapshot.numNibblesInvalid());
      infoSpaceHandler->setUInt32("brildaq.number_nibbles_corrupted",
                                  snapshot.numNibblesCorrupted());
      infoSpaceHandler->setUInt32("brildaq.number_nibbles_almost_corrupted",
                                  snapshot.numNibblesAlmostCorrupted());

      //----------

      // Prepare a table-able version of the recent-nibble and
      // recent-section histories.
      NibbleHistory nibbleHistory(snapshot.recentNibbles());
      std::string newVal = nibbleHistory.getJSONString();
      infoSpaceHandler->setString("nibble_history", newVal);

      SectionHistory sectionHistory(snapshot.recentSections());
      newVal = sectionHistory.getJSONString();
      infoSpaceHandler->setString("section_history", newVal);

      // Now sync everything from the cache to the InfoSpace itself.
      infoSpaceHandler->writeInfoSpace();
    }
}
