#include "tcds/cpm/NibbleHistory.h"

#include <iomanip>
#include <sstream>
#include <cstddef>

tcds::cpm::NibbleHistory::NibbleHistory(std::vector<tcds::cpm::LumiNibble> const& dataIn) :
  nibbles_(dataIn)
{
}

tcds::cpm::NibbleHistory::~NibbleHistory()
{
}

std::string
tcds::cpm::NibbleHistory::getJSONString() const
{
  std::stringstream res;

  res << "[";
  size_t index = 0;
  for (std::vector<tcds::cpm::LumiNibble>::const_iterator it = nibbles_.begin();
       it != nibbles_.end();
       ++it)
    {
      res << "{";

      // The CMS run number.
      res << "\"RunNumber\": \"" << it->runNumber() << "\"";
      res << ", ";

      // The lumi section number.
      res << "\"SectionNumber\": \"" << it->lumiSectionNumber() << "\"";
      res << ", ";

      // The lumi nibble number.
      res << "\"NibbleNumber\": \"" << it->lumiNibbleNumber() << "\"";
      res << ", ";

      // The CMS 'run active' flag.
      res << "\"RunActive\": \"";
      if (it->isCMSRunActive())
        {
          res << "true";
        }
      else
        {
          res << "false";
        }
      res << "\"";
      res << ", ";

      // The number of orbits in this nibble.
      res << "\"NumOrbits\": \"" << it->numOrbits() << "\"";
      res << ", ";

      // The total trigger rate.
      res << "\"TriggerRate\": \""
          << std::fixed << std::setprecision(2)
          << it->triggerRateTotal()
          << "\"";
      res << ", ";

      // The total suppressed-trigger rate.
      res << "\"SuppressedTriggerRate\": \""
          << std::fixed << std::setprecision(2)
          << it->suppressedTriggerRateTotal()
          << "\"";
      res << ", ";

      // The total deadtime.
      res << "\"Deadtime\": \""
          << std::fixed << std::setprecision(2)
          << it->deadtimeFractionTotal(false)
          << "\"";
      res << ", ";

      // The total BeamActive deadtime.
      res << "\"DeadtimeBeamActive\": \""
          << std::fixed << std::setprecision(2)
          << it->deadtimeFractionTotal(true)
          << "\"";

      res << "}";
      if (index != (nibbles_.size() - 1))
        {
          res << ", ";
        }
      ++index;
    }
  res << "]";

  return res.str();
}
