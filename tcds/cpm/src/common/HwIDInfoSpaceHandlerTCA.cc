#include "tcds/cpm/HwIDInfoSpaceHandlerTCA.h"

#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/WebServer.h"

tcds::cpm::HwIDInfoSpaceHandlerTCA::HwIDInfoSpaceHandlerTCA(xdaq::Application& xdaqApp,
                                                            tcds::utils::InfoSpaceUpdater* updater) :
  InfoSpaceHandler(xdaqApp, "tcds-hw-id", updater)
{
  // General board and firmware information.
  createString("board_id_t1",
               tcds::utils::InfoSpaceHandler::kUninitializedString,
               "",
               tcds::utils::InfoSpaceItem::PROCESS);
  createString("board_id_t2",
               tcds::utils::InfoSpaceHandler::kUninitializedString,
               "",
               tcds::utils::InfoSpaceItem::PROCESS);
  createString("system_id_t1",
               tcds::utils::InfoSpaceHandler::kUninitializedString,
               "",
               tcds::utils::InfoSpaceItem::PROCESS);
  createString("system_id_t2",
               tcds::utils::InfoSpaceHandler::kUninitializedString,
               "",
               tcds::utils::InfoSpaceItem::PROCESS);
  createString("firmware_version_t1",
               tcds::utils::InfoSpaceHandler::kUninitializedString,
               "",
               tcds::utils::InfoSpaceItem::PROCESS);
  createString("firmware_version_t2",
               tcds::utils::InfoSpaceHandler::kUninitializedString,
               "",
               tcds::utils::InfoSpaceItem::PROCESS);
  createString("firmware_date_t1",
               tcds::utils::InfoSpaceHandler::kUninitializedString,
               "",
               tcds::utils::InfoSpaceItem::PROCESS);
  createString("firmware_date_t2",
               tcds::utils::InfoSpaceHandler::kUninitializedString,
               "",
               tcds::utils::InfoSpaceItem::PROCESS);
}

tcds::cpm::HwIDInfoSpaceHandlerTCA::~HwIDInfoSpaceHandlerTCA()
{
}

void
tcds::cpm::HwIDInfoSpaceHandlerTCA::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  monitor.newItemSet("Hardware identifiers T1");
  monitor.newItemSet("Hardware identifiers T2");
  monitor.addItem("Hardware identifiers T1",
                  "board_id_t1",
                  "Board id",
                  this);
  monitor.addItem("Hardware identifiers T2",
                  "board_id_t1",
                  "Board id",
                  this);
  monitor.addItem("Hardware identifiers T1",
                  "system_id_t1",
                  "System ID",
                  this);
  monitor.addItem("Hardware identifiers T2",
                  "system_id_t2",
                  "System ID",
                  this);
  monitor.addItem("Hardware identifiers T1",
                  "firmware_version_t1",
                  "Firmware version",
                  this);
  monitor.addItem("Hardware identifiers T2",
                  "firmware_version_t2",
                  "Firmware version",
                  this);
  monitor.addItem("Hardware identifiers T1",
                  "firmware_date_t1",
                  "Firmware date",
                  this);
  monitor.addItem("Hardware identifiers T2",
                  "firmware_date_t2",
                  "Firmware date",
                  this);
}

void
tcds::cpm::HwIDInfoSpaceHandlerTCA::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                  tcds::utils::Monitor& monitor,
                                                                  std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "Hardware ID" : forceTabName;

  webServer.registerTab(tabName,
                        "Hardware identifiers",
                        3);
  webServer.registerTable("Hardware identifiers - T1",
                          "T1 hardware identifiers",
                          monitor,
                          "Hardware identifiers T1",
                          tabName);
  webServer.registerTable("Hardware identifiers - T2",
                          "T2 hardware identifiers",
                          monitor,
                          "Hardware identifiers T2",
                          tabName);
}
