#include "tcds/cpm/ConfigurationInfoSpaceHandler.h"

#include "toolbox/string.h"

#include "tcds/pm/Definitions.h"
#include "tcds/utils/Definitions.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/Utils.h"
#include "tcds/utils/WebServer.h"
#include "tcds/utils/WebTableNoLabels.h"

tcds::cpm::ConfigurationInfoSpaceHandler::ConfigurationInfoSpaceHandler(xdaq::Application& xdaqApp) :
  tcds::utils::ConfigurationInfoSpaceHandler(xdaqApp)
{
  // IPbus connection parameters.

  // The name of the IPbus connections file.
  createString("ipbusConnectionsFile",
               "${XDAQ_SETUP_ROOT}/${XDAQ_ZONE}/etc/tcds_connections.xml",
               "",
               tcds::utils::InfoSpaceItem::NOUPDATE,
               true);

  // The name of the IPbus connections for the CPM T1/T2 in the above
  // file.
  createString("ipbusConnectionT1",
               "dummy",
               "",
               tcds::utils::InfoSpaceItem::NOUPDATE,
               true);
  createString("ipbusConnectionT2",
               "dummy",
               "",
               tcds::utils::InfoSpaceItem::NOUPDATE,
               true);

  //----------

  // The FED id number (twelve bits, strictly speaking) to be used in
  // the DAQ link.
  createUInt32("fedId",
               tcds::definitions::kImpossibleFEDId,
               "",
               tcds::utils::InfoSpaceItem::NOUPDATE,
               true);

  // The FED id numbers of the GT FEDs connected to the front-panel
  // trigger inputs. These are used to detect whether or not the GT is
  // in the run, in which case the corresponding input needs to be
  // enabled.
  // NOTE: This refers to the 'L1A_1' and 'L1A_2' inputs on the
  // front-panel, and _not_ to the 'NIM' input.
  createUInt32("fedIdGT1",
               tcds::definitions::kImpossibleFEDId,
               "",
               tcds::utils::InfoSpaceItem::NOUPDATE,
               true);
  createUInt32("fedIdGT2",
               tcds::definitions::kImpossibleFEDId,
               "",
               tcds::utils::InfoSpaceItem::NOUPDATE,
               true);

  // The FED vector.
  createString("fedEnableMask",
               "",
               "",
               tcds::utils::InfoSpaceItem::NOUPDATE,
               true);

  // The TTC partition map.
  createString("ttcPartitionMap",
               "",
               "",
               tcds::utils::InfoSpaceItem::NOUPDATE,
               true);

  //----------

  // The PVSS host name for the CirculatingBunchConfig DIP publications.
  createString("pvssHost",
               "dummy",
               "",
               tcds::utils::InfoSpaceItem::NOUPDATE,
               true);

  // The 'ignore-dip-for-beamactive-deadtime-calculation' flag.
  createBool("noBeamActive",
             true,
             "",
             tcds::utils::InfoSpaceItem::NOUPDATE,
             true);

  //----------

  // Nice labels for all the LPMs.
  for (unsigned int lpmNum = 1; lpmNum <= tcds::definitions::kNumLPMsPerCPM; ++lpmNum)
    {
      createString(toolbox::toString("partitionLabelLPM%d", lpmNum),
                   "",
                   "",
                   tcds::utils::InfoSpaceItem::NOUPDATE,
                   true);
    }

  //----------

  // Nice labels for all individual partitions (iCIs and APVEs).
  for (unsigned int lpmNum = 1; lpmNum <= tcds::definitions::kNumLPMsPerCPM; ++lpmNum)
    {
      // For the iCIs.
      for (unsigned int iciNum = 1; iciNum <= tcds::definitions::kNumICIsPerLPM; ++iciNum)
        {
          createString(toolbox::toString("partitionLabelLPM%dICI%d", lpmNum, iciNum),
                       "",
                       "",
                       tcds::utils::InfoSpaceItem::NOUPDATE,
                       true);
        }
      // For the APVEs.
      for (unsigned int apveNum = 1; apveNum <= tcds::definitions::kNumAPVEsPerLPM; ++apveNum)
        {
          createString(toolbox::toString("partitionLabelLPM%dAPVE%d", lpmNum, apveNum),
                       "",
                       "",
                       tcds::utils::InfoSpaceItem::NOUPDATE,
                       true);
        }
    }

  //----------

  // The name of the BRILDAQ eventing bus.
  createString("busName",
               "brildata",
               "",
               tcds::utils::InfoSpaceItem::NOUPDATE,
               true);

  // The name of the TCDS BRILDAQ topic.
  createString("topicName",
               "tcds_brildaq_data",
               "",
               tcds::utils::InfoSpaceItem::NOUPDATE,
               true);
}

void
tcds::cpm::ConfigurationInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  tcds::utils::ConfigurationInfoSpaceHandler::registerItemSetsWithMonitor(monitor);

  // Application configuration parameters.
  std::string itemSetName = "Application configuration";

  // The IPbus connection parameters.
  monitor.addItem(itemSetName,
                  "ipbusConnectionsFile",
                  "IPbus connections file",
                  this);
  monitor.addItem(itemSetName,
                  "ipbusConnectionT1",
                  "T1 IPbus connection name",
                  this);
  monitor.addItem(itemSetName,
                  "ipbusConnectionT2",
                  "T2 IPbus connection name",
                  this);

  // The FED id.
  monitor.addItem(itemSetName,
                  "fedId",
                  "FED id",
                  this);

  // The connected GT FED ids.
  monitor.addItem(itemSetName,
                  "fedIdGT1",
                  "GT trigger FED id (L1A_1)",
                  this,
                  "The FED id of the primary GT FED connected to the CPM 'L1A_1' front-panel input");
  monitor.addItem(itemSetName,
                  "fedIdGT2",
                  "GT trigger FED id (L1A_2)",
                  this,
                  "The FED id of the spare GT FED connected to the CPM 'L1A_2' front-panel input");

  // The PVSS host name for the CirculatingBunchConfig DIP publications.
  monitor.addItem(itemSetName,
                  "pvssHost",
                  "PVSS host for DIP bunch pattern for beam-active deadtime calculation",
                  this);

  //----------

  // Run configuration parameters (from RCMS).
  itemSetName = "Run configuration";

  // The FED vector.
  monitor.addItem(itemSetName,
                  "fedEnableMask",
                  "FED vector",
                  this);

  // The TTC partition map.
  monitor.addItem(itemSetName,
                  "ttcPartitionMap",
                  "TTC partition map",
                  this);

  // The 'ignore-dip-for-beamactive-deadtime-calculation' flag.
  monitor.addItem(itemSetName,
                  "noBeamActive",
                  "Ignore DIP bunch pattern for beam-active deadtime calculation",
                  this);

  //----------

  // BRILDAQ eventing bus parameters.
  itemSetName = "itemset-brildaq-config";

  monitor.newItemSet(itemSetName);
  monitor.addItem(itemSetName,
                  "busName",
                  "BRILDAQ eventing bus name",
                  this);
  monitor.addItem(itemSetName,
                  "topicName",
                  "BRILDAQ eventing topic name",
                  this);
}

void
tcds::cpm::ConfigurationInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                        tcds::utils::Monitor& monitor,
                                                                        std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "Configuration" : forceTabName;

  webServer.registerTab(tabName,
                        "Configuration parameters",
                        3);

  webServer.registerTable("Application configuration",
                          "Application configuration parameters",
                          monitor,
                          "Application configuration",
                          tabName);

  webServer.registerTable("Run configuration",
                          "Run configuration parameters (from RCMS/SOAP commands)",
                          monitor,
                          "Run configuration",
                          tabName);

  webServer.registerTable("BRILDAQ configuration",
                          "BRILDAQ eventing configuration parameters",
                          monitor,
                          "itemset-brildaq-config",
                          tabName);

  webServer.registerWebObject<tcds::utils::WebTableNoLabels>("Hardware configuration (default)",
                                                             "Default hardware configuration parameters read from file",
                                                             monitor,
                                                             "itemset-hardware-configuration-default",
                                                             tabName);
  webServer.registerWebObject<tcds::utils::WebTableNoLabels>("Hardware configuration (received)",
                                                             "Hardware configuration parameters received by SOAP",
                                                             monitor,
                                                             "itemset-hardware-configuration-received",
                                                             tabName);
  webServer.registerWebObject<tcds::utils::WebTableNoLabels>("Hardware configuration (applied)",
                                                             "Final configuration parameters applied to the hardware",
                                                             monitor,
                                                             "itemset-hardware-configuration-applied",
                                                             tabName);
}

std::string
tcds::cpm::ConfigurationInfoSpaceHandler::formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const
{
  std::string res = tcds::utils::escapeAsJSONString(tcds::utils::InfoSpaceHandler::kInvalidItemString);
  if (item->isValid())
    {
      std::string name = item->name();
      if (name == "fedEnableMask" || name == "ttcPartitionMap")
        {
          // These things can become annoyingly long. The idea here is
          // to insert some zero-width spaces in order to make it
          // break into pieces when formatted in the web interface.
          // NOTE: Not very efficient/pretty, but it works.
          std::string const valRaw = getString(name);
          std::string const valNew = tcds::utils::chunkifyFEDVector(valRaw);
          res = tcds::utils::escapeAsJSONString(valNew);
        }
      else
        {
          // For everything else simply call the generic formatter.
          res = InfoSpaceHandler::formatItem(item);
        }
    }
  return res;
}
