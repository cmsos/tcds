#include "tcds/cpm/LumiNibble.h"

#include "tcds/utils/Definitions.h"

tcds::cpm::LumiNibble::LumiNibble(tcds::cpm::BRILDAQPacket const& brildaqPacket) :
  rawData_(brildaqPacket)// ,
  // quality_(tcds::definitions::LUMI_NIBBLE_QUALITY_UNKNOWN)
{
}

tcds::cpm::LumiNibble::~LumiNibble()
{
}

bool
tcds::cpm::LumiNibble::isCorrupted() const
{
  return rawData_.isCorrupted();
}

bool
tcds::cpm::LumiNibble::isAlmostCorrupted() const
{
  return rawData_.isAlmostCorrupted();
}

bool
tcds::cpm::LumiNibble::isInvalid() const
{
  return rawData_.isInvalid();
}

bool
tcds::cpm::LumiNibble::isGood() const
{
  return !(isCorrupted() || isInvalid());
}

// void
// tcds::cpm::LumiNibble::markAsGood()
// {
//   setQuality(tcds::utils::LUMI_NIBBLE_QUALITY_GOOD);
// }

// void
// tcds::cpm::LumiNibble::markAsCorrupted()
// {
//   setQuality(tcds::utils::LUMI_NIBBLE_QUALITY_CORRUPTED);
// }

// void
// tcds::cpm::LumiNibble::markAsInvalid()
// {
//   setQuality(tcds::utils::LUMI_NIBBLE_QUALITY_INVALID);
// }

uint32_t
tcds::cpm::LumiNibble::bstSignalStatus() const
{
  return rawData_.bstSignalStatus();
}

uint32_t
tcds::cpm::LumiNibble::timestamp() const
{
  return rawData_.timestamp();
}

double
tcds::cpm::LumiNibble::timestampBegin() const
{
  return rawData_.timestampBegin();
}

double
tcds::cpm::LumiNibble::timestampEnd() const
{
  return rawData_.timestampEnd();
}

uint32_t
tcds::cpm::LumiNibble::fillNumber() const
{
  return rawData_.fillNumber();
}

uint32_t
tcds::cpm::LumiNibble::runNumber() const
{
  return rawData_.runNumber();
}

uint32_t
tcds::cpm::LumiNibble::lumiSectionNumber() const
{
  return rawData_.lumiSectionNumber();
}

uint32_t
tcds::cpm::LumiNibble::lumiNibbleNumber() const
{
  return rawData_.lumiNibbleNumber();
}

uint32_t
tcds::cpm::LumiNibble::orbitNumber() const
{
  return rawData_.orbitNumber();
}

uint32_t
tcds::cpm::LumiNibble::numOrbits() const
{
  return rawData_.numOrbits();
}

uint32_t
tcds::cpm::LumiNibble::numNibblesPerSection() const
{
  return rawData_.numNibblesPerSection();
}

bool
tcds::cpm::LumiNibble::isCMSRunActive() const
{
  return rawData_.isCMSRunActive();
}

uint32_t
tcds::cpm::LumiNibble::numBX(bool const gated) const
{
  return rawData_.numBX(gated);
}

uint32_t
tcds::cpm::LumiNibble::triggerCount(unsigned int const trigType,
                                    bool const gated) const
{
  return rawData_.triggerCount(trigType, gated);
}

uint32_t
tcds::cpm::LumiNibble::triggerCountTotal(bool const gated) const
{
  uint32_t numEvents = 0;
  for (int trigType = tcds::definitions::kTrigTypeMin;
       trigType <= tcds::definitions::kTrigTypeMax;
       ++trigType)
    {
      numEvents += triggerCount(trigType, gated);
    }
  return numEvents;
}

uint32_t
tcds::cpm::LumiNibble::suppressedTriggerCount(unsigned int const trigType,
                                              bool const gated) const
{
  return rawData_.suppressedTriggerCount(trigType, gated);
}

uint32_t
tcds::cpm::LumiNibble::suppressedTriggerCountTotal(bool const gated) const
{
  uint32_t numEvents = 0;
  for (int trigType = tcds::definitions::kTrigTypeMin;
       trigType <= tcds::definitions::kTrigTypeMax;
       ++trigType)
    {
      numEvents += suppressedTriggerCount(trigType, gated);
    }
  return numEvents;
}

uint32_t
tcds::cpm::LumiNibble::suppressedTriggerCountFromTTS(uint32_t const trigType,
                                                     bool const gated) const
{
  return rawData_.suppressedTriggerCountFromTTS(trigType, gated);
}

uint32_t
tcds::cpm::LumiNibble::suppressedTriggerCountFromTriggerRules(uint32_t const trigType,
                                                              bool const gated) const
{
  return rawData_.suppressedTriggerCountFromTriggerRules(trigType, gated);
}

uint32_t
tcds::cpm::LumiNibble::suppressedTriggerCountFromBunchMaskVeto(uint32_t const trigType,
                                                               bool const gated) const
{
  return rawData_.suppressedTriggerCountFromBunchMaskVeto(trigType, gated);
}

uint32_t
tcds::cpm::LumiNibble::suppressedTriggerCountFromRetri(uint32_t const trigType,
                                                       bool const gated) const
{
  return rawData_.suppressedTriggerCountFromRetri(trigType, gated);
}

uint32_t
tcds::cpm::LumiNibble::suppressedTriggerCountFromAPVE(uint32_t const trigType,
                                                      bool const gated) const
{
  return rawData_.suppressedTriggerCountFromAPVE(trigType, gated);
}

uint32_t
tcds::cpm::LumiNibble::suppressedTriggerCountFromDAQBackpressure(uint32_t const trigType,
                                                                 bool const gated) const
{
  return rawData_.suppressedTriggerCountFromDAQBackpressure(trigType, gated);
}

uint32_t
tcds::cpm::LumiNibble::suppressedTriggerCountFromCalibration(uint32_t const trigType,
                                                             bool const gated) const
{
  return rawData_.suppressedTriggerCountFromCalibration(trigType, gated);
}

uint32_t
tcds::cpm::LumiNibble::suppressedTriggerCountFromSWPause(uint32_t const trigType,
                                                         bool const gated) const
{
  return rawData_.suppressedTriggerCountFromSWPause(trigType, gated);
}

uint32_t
tcds::cpm::LumiNibble::suppressedTriggerCountFromFWPause(uint32_t const trigType,
                                                         bool const gated) const
{
  return rawData_.suppressedTriggerCountFromFWPause(trigType, gated);
}

uint32_t
tcds::cpm::LumiNibble::deadtimeBXCountTotal(bool const gated) const
{
  return rawData_.deadtimeBXCountTotal(gated);
}

uint32_t
tcds::cpm::LumiNibble::deadtimeBXCountFromTTSAllPartitions(bool const gated) const
{
  return rawData_.deadtimeBXCountFromTTSAllPartitions(gated);
}

uint32_t
tcds::cpm::LumiNibble::deadtimeBXCountFromTTSSingleICI(unsigned int const lpmNum,
                                                       unsigned int const iciNum,
                                                       bool const gated) const
{
  return rawData_.deadtimeBXCountFromTTSSingleICI(lpmNum, iciNum, gated);
}

uint32_t
tcds::cpm::LumiNibble::deadtimeBXCountFromTTSSingleAPVE(unsigned int const lpmNum,
                                                        unsigned int const apveNum,
                                                        bool const gated) const
{
  return rawData_.deadtimeBXCountFromTTSSingleAPVE(lpmNum, apveNum, gated);
}

uint32_t
tcds::cpm::LumiNibble::deadtimeBXCountFromTriggerRules(bool const gated) const
{
  return rawData_.deadtimeBXCountFromTriggerRules(gated);
}

uint32_t
tcds::cpm::LumiNibble::deadtimeBXCountFromBunchMaskVeto(bool const gated) const
{
  return rawData_.deadtimeBXCountFromBunchMaskVeto(gated);
}

uint32_t
tcds::cpm::LumiNibble::deadtimeBXCountFromRetri(bool const gated) const
{
  return rawData_.deadtimeBXCountFromRetri(gated);
}

uint32_t
tcds::cpm::LumiNibble::deadtimeBXCountFromAPVE(bool const gated) const
{
  return rawData_.deadtimeBXCountFromAPVE(gated);
}

uint32_t
tcds::cpm::LumiNibble::deadtimeBXCountFromDAQBackpressure(bool const gated) const
{
  return rawData_.deadtimeBXCountFromDAQBackpressure(gated);
}

uint32_t
tcds::cpm::LumiNibble::deadtimeBXCountFromCalibration(bool const gated) const
{
  return rawData_.deadtimeBXCountFromCalibration(gated);
}

uint32_t
tcds::cpm::LumiNibble::deadtimeBXCountFromSWPause(bool const gated) const
{
  return rawData_.deadtimeBXCountFromSWPause(gated);
}

uint32_t
tcds::cpm::LumiNibble::deadtimeBXCountFromFWPause(bool const gated) const
{
  return rawData_.deadtimeBXCountFromFWPause(gated);
}

uint32_t
tcds::cpm::LumiNibble::deadtimeBXCountFromTriggerRule(unsigned int const ruleNum,
                                                      bool const gated) const
{
  return rawData_.deadtimeBXCountFromTriggerRule(ruleNum, gated);
}

double
tcds::cpm::LumiNibble::lumiNibbleDuration() const
{
  return 1. * numOrbits() / tcds::definitions::kLHCOrbitFreq;
}

double
tcds::cpm::LumiNibble::lumiSectionDuration() const
{
  return 1. * numNibblesPerSection() * lumiNibbleDuration();
}

double
tcds::cpm::LumiNibble::triggerRate(unsigned int const trigType,
                                   bool const gated) const
{
  return toRate(triggerCount(trigType, gated));
}

double
tcds::cpm::LumiNibble::triggerRateTotal(bool const gated) const
{
  return toRate(triggerCountTotal(gated));
}

double
tcds::cpm::LumiNibble::suppressedTriggerRate(unsigned int const trigType,
                                             bool const gated) const
{
  return toRate(suppressedTriggerCount(trigType, gated));
}

double
tcds::cpm::LumiNibble::suppressedTriggerRateTotal(bool const gated) const
{
  return toRate(suppressedTriggerCountTotal(gated));
}

double
tcds::cpm::LumiNibble::deadtimeFractionTotal(bool const gated) const
{
  return toTimeFraction(deadtimeBXCountTotal(gated), gated);
}

double
tcds::cpm::LumiNibble::deadtimeFractionFromTTSAllPartitions(bool const gated) const
{
  return toTimeFraction(deadtimeBXCountFromTTSAllPartitions(gated), gated);
}

double
tcds::cpm::LumiNibble::deadtimeFractionFromTTSSingleICI(unsigned int const lpmNum,
                                                        unsigned int const iciNum,
                                                        bool const gated) const
{
  return toTimeFraction(deadtimeBXCountFromTTSSingleICI(lpmNum, iciNum), gated);
}

double
tcds::cpm::LumiNibble::deadtimeFractionFromTTSSingleAPVE(unsigned int const lpmNum,
                                                         unsigned int const apveNum,
                                                         bool const gated) const
{
  return toTimeFraction(deadtimeBXCountFromTTSSingleAPVE(lpmNum, apveNum), gated);
}

double
tcds::cpm::LumiNibble::deadtimeFractionFromTriggerRules(bool const gated) const
{
  return toTimeFraction(deadtimeBXCountFromTriggerRules(gated), gated);
}

double
tcds::cpm::LumiNibble::deadtimeFractionFromBunchMaskVeto(bool const gated) const
{
  return toTimeFraction(deadtimeBXCountFromBunchMaskVeto(gated), gated);
}

double
tcds::cpm::LumiNibble::deadtimeFractionFromRetri(bool const gated) const
{
  return toTimeFraction(deadtimeBXCountFromRetri(gated), gated);
}

double
tcds::cpm::LumiNibble::deadtimeFractionFromAPVE(bool const gated) const
{
  return toTimeFraction(deadtimeBXCountFromAPVE(gated), gated);
}

double
tcds::cpm::LumiNibble::deadtimeFractionFromDAQBackpressure(bool const gated) const
{
  return toTimeFraction(deadtimeBXCountFromDAQBackpressure(gated), gated);
}

double
tcds::cpm::LumiNibble::deadtimeFractionFromCalibration(bool const gated) const
{
  return toTimeFraction(deadtimeBXCountFromCalibration(gated), gated);
}

double
tcds::cpm::LumiNibble::deadtimeFractionFromSWPause(bool const gated) const
{
  return toTimeFraction(deadtimeBXCountFromSWPause(gated), gated);
}

double
tcds::cpm::LumiNibble::deadtimeFractionFromFWPause(bool const gated) const
{
  return toTimeFraction(deadtimeBXCountFromFWPause(gated), gated);
}

double
tcds::cpm::LumiNibble::deadtimeFractionFromTriggerRule(unsigned int const ruleNum,
                                                       bool const gated) const
{
  return toTimeFraction(deadtimeBXCountFromTriggerRule(ruleNum, gated), gated);
}

double
tcds::cpm::LumiNibble::toRate(uint32_t const count) const
{
  double const rate = 1. * count / numOrbits() * tcds::definitions::kLHCOrbitFreq;
  return rate;
}

double
tcds::cpm::LumiNibble::toTimeFraction(uint32_t const count,
                                      bool const gated) const
{
  uint32_t const num = count;
  uint32_t const den = numBX(gated);
  double fraction = 0.;
  if (den != 0.)
    {
      fraction = 100. * num / den;
    }
  return fraction;
}

// void
// tcds::cpm::LumiNibble::setQuality(tcds::definitions::LUMI_NIBBLE_QUALITY const& quality)
// {
//   quality_ = quality;
// }
