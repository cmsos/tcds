#include "tcds/cpm/BRILDAQSectionStorageInfoSpaceHandler.h"

#include "tcds/cpm/LumiSection.h"
#include "tcds/cpm/Utils.h"

tcds::cpm::BRILDAQSectionStorageInfoSpaceHandler::BRILDAQSectionStorageInfoSpaceHandler(xdaq::Application& xdaqApp) :
  InfoSpaceHandler(xdaqApp, "tcds-cpm-brildaqsectionstorage", 0)
{
  // The book keeping information.
  createUInt32("fill_number");
  createUInt32("run_number");
  createUInt32("section_number");
  createUInt32("num_nibbles");

  // Quality/misc. information.
  createFloat("completeness");
  createFloat("bst_signal_quality");
  createFloat("run_active_fraction");

  //----------

  tcds::cpm::CreateBRILDAQStuff(*this);
}

tcds::cpm::BRILDAQSectionStorageInfoSpaceHandler::~BRILDAQSectionStorageInfoSpaceHandler()
{
}

void
tcds::cpm::BRILDAQSectionStorageInfoSpaceHandler::update(LumiSection const& section)
{
  // Book keeping information.
  setUInt32("fill_number", section.fillNumber());
  setUInt32("run_number", section.runNumber());
  setUInt32("section_number", section.lumiSectionNumber());
  setUInt32("num_nibbles", section.numNibbles());

  // Quality/misc. information.
  setFloat("completeness", section.completeness());
  setFloat("bst_signal_quality", section.bstSignalQuality());
  setFloat("run_active_fraction", section.fracCMSRunActive());

  //----------

  tcds::cpm::UpdateBRILDAQStuff(*this, section);
}

void
tcds::cpm::BRILDAQSectionStorageInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  // Nothing to do in this case.
}

void
tcds::cpm::BRILDAQSectionStorageInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                                tcds::utils::Monitor& monitor,
                                                                                std::string const& forceTabName)
{
  // Nothing to do in this case.
}
