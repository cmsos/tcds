#include "tcds/cpm/TCADeviceCPMT1.h"

#include <memory>
#include <vector>

#include "toolbox/string.h"

#include "tcds/hwlayer/Utils.h"
#include "tcds/hwlayertca/TCACarrierBase.h"
#include "tcds/hwlayertca/TCACarrierAMC13.h"
#include "tcds/pm/Definitions.h"

tcds::cpm::TCADeviceCPMT1::TCADeviceCPMT1() :
  tcds::pm::TCADevicePMCommonBase(std::unique_ptr<tcds::hwlayertca::TCACarrierBase>(new tcds::hwlayertca::TCACarrierAMC13(hwDevice_)))
{
}

tcds::cpm::TCADeviceCPMT1::~TCADeviceCPMT1()
{
}

uint32_t
tcds::cpm::TCADeviceCPMT1::readSwVersionRaw() const
{
  uint32_t const res = readRegister("main.event_record_data.sw_version");
  return res;
}

uint32_t
tcds::cpm::TCADeviceCPMT1::readFwVersionRaw() const
{
  uint32_t const res = hwDevice_.readRegister("system.firmware_id");
  return res;
}

uint16_t
tcds::cpm::TCADeviceCPMT1::readFEDId() const
{
  uint32_t const res = readRegister("main.daq_link_config.fed_id");
  return res;
}

std::string
tcds::cpm::TCADeviceCPMT1::readSystemIdImpl() const
{
  // The system ID consists of four characters encoded as a single
  // 32-bit word.
  uint32_t val = hwDevice_.readRegister("system.system_id");
  std::string res = tcds::hwlayer::uint32ToString(val);
  return res;
}

bool
tcds::cpm::TCADeviceCPMT1::isTTCClockUpImpl() const
{
  uint32_t const tmp = hwDevice_.readRegister("cpmt1.status.ttc_clock_from_ttcmi_locked");
  return (tmp != 0x0);
}

bool
tcds::cpm::TCADeviceCPMT1::isTTCClockStableImpl() const
{
  uint32_t const tmp = hwDevice_.readRegister("cpmt1.status.ttc_clock_from_ttcmi_locked_steady");
  return (tmp != 0x0);
}

uint32_t
tcds::cpm::TCADeviceCPMT1::readTTCClockUnlockCounterImpl() const
{
  return hwDevice_.readRegister("cpmt1.ttc_clock_unlock_count");
}

std::string
tcds::cpm::TCADeviceCPMT1::regNamePrefixImpl() const
{
  // All the CPM T1 registers start with 'cpmt1.ipm.'.
  return "cpmt1.ipm.";
}

void
tcds::cpm::TCADeviceCPMT1::enableLPMBRILDAQTransmission() const
{
  hwDevice_.writeRegister("cpmt1.backplane_data_transfer.enable", 0x1);
  hwDevice_.writeRegister("cpmt1.backplane_data_transfer.transmission_bx", 0x1);
}

void
tcds::cpm::TCADeviceCPMT1::configureTriggerAlignment() const
{
  // This one is not really trigger alignment, but it aligns the BX
  // number reported for each trigger in the DAQ event record.
  std::string regName = "main.orbit_alignment_config.daq_record_adjust";
  writeRegister(regName, 0xd6e);

  //----------

  // NOTE: The following numbers assume that in the LPM, the
  // L1A-from-CPM-fast-track option is enabled.

  // BX-alignment for cyclic triggers.
  regName = "main.int_fw_alignment_settings.cyclic_trigger_adjust";
  writeRegister(regName, 0x7c);

  // BX-alignment for the bunch-mask.
  regName = "main.int_fw_alignment_settings.bunch_mask_adjust";
  writeRegister(regName, 0x7d);

  // BX-alignment for the sequence triggers.
  regName = "bgo_trains.bgo_train_config.calib_trigger_adjust";
  writeRegister(regName, 0x7d);
}

bool
tcds::cpm::TCADeviceCPMT1::isExternalTrigger0Enabled() const
{
  uint32_t const tmp = readRegister("main.inselect.external_trigger0_enable");
  bool const isEnabled = (tmp == 1);
  return isEnabled;
}

void
tcds::cpm::TCADeviceCPMT1::enableExternalTrigger0() const
{
  writeRegister("main.inselect.external_trigger0_enable", 1);
}

void
tcds::cpm::TCADeviceCPMT1::disableExternalTrigger0() const
{
  writeRegister("main.inselect.external_trigger0_enable", 0);
}

void
tcds::cpm::TCADeviceCPMT1::resetTTCClockMon() const
{
  std::string const regName = "cpmt1.resets.reset_ttc_clock_mon";
  hwDevice_.writeRegister(regName, 0x0);
  hwDevice_.writeRegister(regName, 0x1);
  hwDevice_.writeRegister(regName, 0x0);
}

void
tcds::cpm::TCADeviceCPMT1::resetTTCClockPLL() const
{
  std::string const regName = "cpmt1.resets.reset_ttc_pll";
  hwDevice_.writeRegister(regName, 0x0);
  hwDevice_.writeRegister(regName, 0x1);
  hwDevice_.writeRegister(regName, 0x0);
}

void
tcds::cpm::TCADeviceCPMT1::resetTTSLinks() const
{
  // NOTE: Each reset resets a full quad of GTXs, hence three resets
  // for twelve LPMs.
  std::vector<std::string> regNames;
  regNames.push_back("cpmt1.backplane_tts_links_status.backplane_tts_control.gtx_resets.gtx0");
  regNames.push_back("cpmt1.backplane_tts_links_status.backplane_tts_control.gtx_resets.gtx1");
  regNames.push_back("cpmt1.backplane_tts_links_status.backplane_tts_control.gtx_resets.gtx2");
  for (std::vector<std::string>::const_iterator regName = regNames.begin();
       regName != regNames.end();
       ++regName)
    {
      hwDevice_.writeRegister(*regName, 0x0);
      hwDevice_.writeRegister(*regName, 0x1);
      hwDevice_.writeRegister(*regName, 0x0);
    }

  // After resetting the links: make sure to reset all error counters
  // as well.
  resetTTSLinkErrorCounters();
}

void
tcds::cpm::TCADeviceCPMT1::resetTTSLinkErrorCounters() const
{
  // NOTE: These registers are reset by writing (anything) to them.
  // NOTE: There are also direct resets for both these counters. All
  // kinda historical, actually. Ask Magnus or Jeroen over a coffee
  // about details.
  std::string regName = "";
  for (unsigned int lpmNum = 1; lpmNum <= tcds::definitions::kNumLPMsPerCPM; ++lpmNum)
    {
      std::string const regNameBase =
        toolbox::toString("cpmt1.backplane_tts_links_status.lpm%d", lpmNum);
      hwDevice_.writeRegister(toolbox::toString("%s.sequence_error_count", regNameBase.c_str()), 0x0);
      hwDevice_.writeRegister(toolbox::toString("%s.crc_error_count", regNameBase.c_str()), 0x0);
    }
}

bool
tcds::cpm::TCADeviceCPMT1::isBSTReady() const
{
  uint32_t const tmp = hwDevice_.readRegister("cpmt1.status.bst_ready");
  return (tmp == 0x1);
}

bool
tcds::cpm::TCADeviceCPMT1::isTTSLinkUp(unsigned int const lpmNum) const
{
  std::string const regName =
    toolbox::toString("cpmt1.backplane_tts_links_status.lpm%d.link_up", lpmNum);
  uint32_t const tmp = hwDevice_.readRegister(regName);
  return (tmp == 0x1);
}

bool
tcds::cpm::TCADeviceCPMT1::isTTSLinkAligned(unsigned int const lpmNum) const
{
  std::string const regName =
    toolbox::toString("cpmt1.backplane_tts_links_status.lpm%d.link_aligned", lpmNum);
  uint32_t const tmp = hwDevice_.readRegister(regName);
  return (tmp == 0x1);
}

uint32_t
tcds::cpm::TCADeviceCPMT1::readTTSLinkCRCErrorCount(unsigned int const lpmNum) const
{
  std::string const regName =
    toolbox::toString("cpmt1.backplane_tts_links_status.lpm%d.crc_error_count", lpmNum);
  return hwDevice_.readRegister(regName);
}

uint32_t
tcds::cpm::TCADeviceCPMT1::readTTSLinkSequenceErrorCount(unsigned int const lpmNum) const
{
  std::string const regName =
    toolbox::toString("cpmt1.backplane_tts_links_status.lpm%d.sequence_error_count", lpmNum);
  return hwDevice_.readRegister(regName);
}

std::vector<uint32_t>
tcds::cpm::TCADeviceCPMT1::readBRILDAQPacket() const
{
  std::vector<uint32_t> const brildaqData = hwDevice_.readBlock("cpmt1.brildaq");
  return brildaqData;
}
