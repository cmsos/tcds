#include "tcds/cpm/BRILDAQSnapshot.h"

#include <algorithm>
#include <iterator>

tcds::cpm::BRILDAQSnapshot::BRILDAQSnapshot(uint32_t const numNibblesMissed,
                                            uint32_t const numNibblesReadMulti,
                                            uint32_t const numNibblesInvalid,
                                            uint32_t const numNibblesCorrupted,
                                            uint32_t const numNibblesAlmostCorrupted,
                                            std::deque<tcds::cpm::LumiNibble> const& nibbles,
                                            std::deque<tcds::cpm::LumiSection> const& sections) :
  numNibblesMissed_(numNibblesMissed),
  numNibblesReadMulti_(numNibblesReadMulti),
  numNibblesInvalid_(numNibblesInvalid),
  numNibblesCorrupted_(numNibblesCorrupted),
  numNibblesAlmostCorrupted_(numNibblesAlmostCorrupted),
  nibbles_(nibbles),
  sections_(sections)
{
}

tcds::cpm::BRILDAQSnapshot::~BRILDAQSnapshot()
{
}

uint32_t
tcds::cpm::BRILDAQSnapshot::numNibblesMissed() const
{
  return numNibblesMissed_;
}

uint32_t
tcds::cpm::BRILDAQSnapshot::numNibblesReadMulti() const
{
  return numNibblesReadMulti_;
}

uint32_t
tcds::cpm::BRILDAQSnapshot::numNibblesInvalid() const
{
  return numNibblesInvalid_;
}

uint32_t
tcds::cpm::BRILDAQSnapshot::numNibblesCorrupted() const
{
  return numNibblesCorrupted_;
}

uint32_t
tcds::cpm::BRILDAQSnapshot::numNibblesAlmostCorrupted() const
{
  return numNibblesAlmostCorrupted_;
}

std::vector<tcds::cpm::LumiNibble>
tcds::cpm::BRILDAQSnapshot::recentNibbles() const
{
  std::vector<tcds::cpm::LumiNibble> res;
  res.clear();
  std::copy(nibbles_.begin(), nibbles_.end(), std::back_inserter(res));
  return res;
}

std::vector<tcds::cpm::LumiSection>
tcds::cpm::BRILDAQSnapshot::recentSections() const
{
  std::vector<tcds::cpm::LumiSection> res;
  res.clear();
  std::copy(sections_.begin(), sections_.end(), std::back_inserter(res));
  return res;
}
