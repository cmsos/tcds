#include "tcds/cpm/CPMRatesInfoSpaceUpdater.h"

#include <algorithm>
#include <cmath>
#include <cstddef>
#include <stdint.h>
#include <string>
#include <utility>
#include <vector>

#include "toolbox/string.h"

#include "tcds/cpm/BRILDAQLoop.h"
#include "tcds/cpm/BRILDAQSnapshot.h"
#include "tcds/cpm/LumiNibble.h"
#include "tcds/cpm/LumiNibbleGroup.h"
#include "tcds/cpm/TCADeviceCPMT1.h"
#include "tcds/pm/Definitions.h"
#include "tcds/utils/Definitions.h"
#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/Utils.h"

tcds::cpm::CPMRatesInfoSpaceUpdater::CPMRatesInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                                              tcds::cpm::TCADeviceCPMT1 const& hwT1,
                                                              tcds::cpm::BRILDAQLoop const& brildaqLoop) :
  tcds::utils::HwInfoSpaceUpdaterBase(xdaqApp, hwT1),
  hwT1_(hwT1),
  brildaqLoop_(brildaqLoop)
{
}

tcds::cpm::CPMRatesInfoSpaceUpdater::~CPMRatesInfoSpaceUpdater()
{
}

void
tcds::cpm::CPMRatesInfoSpaceUpdater::updateInfoSpaceImpl(tcds::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  TCADeviceCPMT1 const& hwT1 = getHwT1();
  std::vector<LumiNibble> recentNibbles;
  if (hwT1.isHwConnected())
    {
      // Update the BeamActive BX mask size.
      std::string name = "bunch_mask_beamactive_size";
      std::vector<uint16_t> bxList = hwT1.readBunchMaskBeamActiveBXs();
      uint32_t const newValSize = bxList.size();
      infoSpaceHandler->setUInt32(name, newValSize);
      // Update the BeamActive BX mask pattern.
      name = "bunch_mask_beamactive";
      std::vector<std::pair<uint16_t, uint16_t> > bxRanges =
        tcds::utils::groupBXListIntoRanges(bxList);
      std::string const newValPattern =  tcds::utils::formatBXRangeList(bxRanges);
      infoSpaceHandler->setString(name, newValPattern);
      BRILDAQSnapshot const snapshot = brildaqLoop_.snapshot();
      recentNibbles = snapshot.recentNibbles();
      infoSpaceHandler->setValid();
    }
  else
    {
      infoSpaceHandler->setInvalid();
    }

  //----------

  // We need to update two sets of lots of things: the 'plain' and the
  // 'gated' (i.e., BeamActive) version.
  std::vector<bool> options;
  options.push_back(false);
  options.push_back(true);

  // Store recent-nibble data.
  if (recentNibbles.size() > 0)
    {
      // In order to reduce fluctuations a bit (and not confuse the
      // shifter), we gather the N most recent nibbles and average
      // over them.

      // NOTE: N is approximated based on the duration of the most
      // recent nibble, and the wish to update roughly in one-second
      // intervals.
      LumiNibble const latestNibble = recentNibbles.back();
      double const nibbleDuration = latestNibble.lumiNibbleDuration();
      // NOTE: The '+ .5' in the next line is a trick to achieve the
      // effect of round() (in C++11) with floor() (without C++11).
      size_t const lastN = std::floor((1. / nibbleDuration) + .5);
      std::vector<LumiNibble> const tmp(recentNibbles.end() - std::min(recentNibbles.size(), lastN),
                                        recentNibbles.end());
      LumiNibbleGroup const& latestNNibbles(tmp);

      //----------

      // Update the book keeping information.
      // NOTE: This is based on the latest nibble. Not 100% pure, but
      // good enough for the monitoring.
      infoSpaceHandler->setUInt32("fill_number", latestNibble.fillNumber());
      infoSpaceHandler->setUInt32("run_number", latestNibble.runNumber());
      infoSpaceHandler->setUInt32("section_number", latestNibble.lumiSectionNumber());
      // And this one is based on the number of recent nibbles we
      // decided to use. Just as an indication of the 'averaging
      // window' size.
      infoSpaceHandler->setUInt32("num_nibbles", lastN);

      //----------

      for (std::vector<bool>::const_iterator option = options.begin();
           option != options.end();
           ++option)
        {
          bool const isGated = *option;
          std::string version = "plain";
          if (isGated)
            {
              version = "beamactive";
            }

          // Trigger rates.
          infoSpaceHandler->setDouble(toolbox::toString("brildaq.%s.trigger_rate_total",
                                                        version.c_str()),
                                      latestNNibbles.triggerRateTotal(isGated));
          for (int trigType = tcds::definitions::kTrigTypeMin;
               trigType <= tcds::definitions::kTrigTypeMax;
               ++trigType)
            {
              std::string const varName =
                toolbox::toString("brildaq.%s.trigger_rate_trigtype%d",
                                  version.c_str(),
                                  trigType);
              double const rate = latestNNibbles.triggerRate(trigType, isGated);
              infoSpaceHandler->setDouble(varName, rate);
            }

          // Suppressed-trigger rates.
          infoSpaceHandler->setDouble(toolbox::toString("brildaq.%s.suppressed_trigger_rate_total",
                                                        version.c_str()),
                                      latestNNibbles.suppressedTriggerRateTotal(isGated));
          for (int trigType = tcds::definitions::kTrigTypeMin;
               trigType <= tcds::definitions::kTrigTypeMax;
               ++trigType)
            {
              std::string const varName =
                toolbox::toString("brildaq.%s.suppressed_trigger_rate_trigtype%d",
                                  version.c_str(),
                                  trigType);
              double const rate = latestNNibbles.suppressedTriggerRate(trigType, isGated);
              infoSpaceHandler->setDouble(varName, rate);
            }

          //----------

          // Deadtime fractions.
          infoSpaceHandler->setDouble(toolbox::toString("brildaq.%s.deadtime_total",
                                                        version.c_str()),
                                      latestNNibbles.deadtimeFractionTotal(isGated));
          infoSpaceHandler->setDouble(toolbox::toString("brildaq.%s.deadtime_tts",
                                                        version.c_str()),
                                      latestNNibbles.deadtimeFractionFromTTSAllPartitions(isGated));
          infoSpaceHandler->setDouble(toolbox::toString("brildaq.%s.deadtime_trigger_rules",
                                                        version.c_str()),
                                      latestNNibbles.deadtimeFractionFromTriggerRules(isGated));
          infoSpaceHandler->setDouble(toolbox::toString("brildaq.%s.deadtime_bunch_mask_veto",
                                                        version.c_str()),
                                      latestNNibbles.deadtimeFractionFromBunchMaskVeto(isGated));
          infoSpaceHandler->setDouble(toolbox::toString("brildaq.%s.deadtime_retri",
                                                        version.c_str()),
                                      latestNNibbles.deadtimeFractionFromRetri(isGated));
          infoSpaceHandler->setDouble(toolbox::toString("brildaq.%s.deadtime_apve",
                                                        version.c_str()),
                                      latestNNibbles.deadtimeFractionFromAPVE(isGated));
          infoSpaceHandler->setDouble(toolbox::toString("brildaq.%s.deadtime_daq_backpressure",
                                                        version.c_str()),
                                      latestNNibbles.deadtimeFractionFromDAQBackpressure(isGated));
          infoSpaceHandler->setDouble(toolbox::toString("brildaq.%s.deadtime_calibration",
                                                        version.c_str()),
                                      latestNNibbles.deadtimeFractionFromCalibration(isGated));
          infoSpaceHandler->setDouble(toolbox::toString("brildaq.%s.deadtime_sw_pause",
                                                        version.c_str()),
                                      latestNNibbles.deadtimeFractionFromSWPause(isGated));
          infoSpaceHandler->setDouble(toolbox::toString("brildaq.%s.deadtime_fw_pause",
                                                        version.c_str()),
                                      latestNNibbles.deadtimeFractionFromFWPause(isGated));

          for (unsigned int ruleNum = tcds::definitions::kTriggerRuleMin;
               ruleNum <= tcds::definitions::kTriggerRuleMax;
               ++ruleNum)
            {
              infoSpaceHandler->setDouble(toolbox::toString("brildaq.%s.deadtime_trigger_rule%d",
                                                            version.c_str(),
                                                            ruleNum),
                                          latestNNibbles.deadtimeFractionFromTriggerRule(ruleNum, isGated));
            }

          //----------

          // Suppressed-trigger counts split per deadtime source (and per trigger type).
          for (int trigType = tcds::definitions::kTrigTypeMin;
               trigType <= tcds::definitions::kTrigTypeMax;
               ++trigType)
            {
              infoSpaceHandler->setUInt32(toolbox::toString("brildaq.%s.suppressed_event_counts_by_deadtime_source.tts.trigtype%d",
                                                            version.c_str(),
                                                            trigType),
                                          latestNNibbles.suppressedTriggerCountFromTTS(trigType, isGated));
              infoSpaceHandler->setUInt32(toolbox::toString("brildaq.%s.suppressed_event_counts_by_deadtime_source.trigger_rules.trigtype%d",
                                                            version.c_str(),
                                                            trigType),
                                          latestNNibbles.suppressedTriggerCountFromTriggerRules(trigType, isGated));
              infoSpaceHandler->setUInt32(toolbox::toString("brildaq.%s.suppressed_event_counts_by_deadtime_source.bunch_mask_veto.trigtype%d",
                                                            version.c_str(),
                                                            trigType),
                                          latestNNibbles.suppressedTriggerCountFromBunchMaskVeto(trigType, isGated));
              infoSpaceHandler->setUInt32(toolbox::toString("brildaq.%s.suppressed_event_counts_by_deadtime_source.retri.trigtype%d",
                                                            version.c_str(),
                                                            trigType),
                                          latestNNibbles.suppressedTriggerCountFromRetri(trigType, isGated));

              infoSpaceHandler->setUInt32(toolbox::toString("brildaq.%s.suppressed_event_counts_by_deadtime_source.apve.trigtype%d",
                                                            version.c_str(),
                                                            trigType),
                                          latestNNibbles.suppressedTriggerCountFromAPVE(trigType, isGated));
              infoSpaceHandler->setUInt32(toolbox::toString("brildaq.%s.suppressed_event_counts_by_deadtime_source.daq_backpressure.trigtype%d",
                                                            version.c_str(),
                                                            trigType),
                                          latestNNibbles.suppressedTriggerCountFromDAQBackpressure(trigType, isGated));
              infoSpaceHandler->setUInt32(toolbox::toString("brildaq.%s.suppressed_event_counts_by_deadtime_source.calibration.trigtype%d",
                                                            version.c_str(),
                                                            trigType),
                                          latestNNibbles.suppressedTriggerCountFromCalibration(trigType, isGated));
              infoSpaceHandler->setUInt32(toolbox::toString("brildaq.%s.suppressed_event_counts_by_deadtime_source.sw_pause.trigtype%d",
                                                            version.c_str(),
                                                            trigType),
                                          latestNNibbles.suppressedTriggerCountFromSWPause(trigType, isGated));
              infoSpaceHandler->setUInt32(toolbox::toString("brildaq.%s.suppressed_event_counts_by_deadtime_source.fw_pause.trigtype%d",
                                                            version.c_str(),
                                                            trigType),
                                          latestNNibbles.suppressedTriggerCountFromFWPause(trigType, isGated));
            }
        }

      //----------

      // Deadtime due to TTS, listed per partition, grouped by LPM (plain only).
      std::string name;
      for (unsigned int lpmNum = 1; lpmNum <= tcds::definitions::kNumLPMsPerCPM; ++lpmNum)
        {
          for (unsigned int iciNum = 1; iciNum <= tcds::definitions::kNumICIsPerLPM; ++iciNum)
            {
              name = toolbox::toString("brildaq.plain.deadtime_tts_lpm%d_ici%d",
                                       lpmNum,
                                       iciNum);
              double const deadtime = latestNNibbles.deadtimeFractionFromTTSSingleICI(lpmNum, iciNum);
              infoSpaceHandler->setDouble(name, deadtime);
            }
          for (unsigned int apveNum = 1; apveNum <= tcds::definitions::kNumAPVEsPerLPM; ++apveNum)
            {
              name = toolbox::toString("brildaq.plain.deadtime_tts_lpm%d_apve%d",
                                       lpmNum,
                                       apveNum);
              double const deadtime = latestNNibbles.deadtimeFractionFromTTSSingleAPVE(lpmNum, apveNum);
              infoSpaceHandler->setDouble(name, deadtime);
            }
        }
    }

  // Now sync everything from the cache to the InfoSpace itself.
  infoSpaceHandler->writeInfoSpace();
}

tcds::cpm::TCADeviceCPMT1 const&
tcds::cpm::CPMRatesInfoSpaceUpdater::getHwT1() const
{
  return hwT1_;
}
