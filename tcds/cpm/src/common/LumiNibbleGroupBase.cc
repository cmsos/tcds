#include "tcds/cpm/LumiNibbleGroupBase.h"

#include "tcds/utils/Definitions.h"

tcds::cpm::LumiNibbleGroupBase::LumiNibbleGroupBase(tcds::cpm::LumiNibble const& nibble)
{
  nibbles_.push_back(nibble);
}

tcds::cpm::LumiNibbleGroupBase::LumiNibbleGroupBase(std::vector<tcds::cpm::LumiNibble> const& nibbles) :
  nibbles_(nibbles)
{
}

tcds::cpm::LumiNibbleGroupBase::~LumiNibbleGroupBase()
{
}

void
tcds::cpm::LumiNibbleGroupBase::addNibble(tcds::cpm::LumiNibble const& nibble)
{
  nibbles_.push_back(nibble);
}

size_t
tcds::cpm::LumiNibbleGroupBase::numNibbles() const
{
  return nibbles_.size();
}

uint32_t
tcds::cpm::LumiNibbleGroupBase::numOrbits() const
{
  uint32_t res = 0;
  for (std::vector<tcds::cpm::LumiNibble>::const_iterator i = nibbles_.begin();
       i != nibbles_.end();
       ++i)
    {
      res += i->numOrbits();
    }
  return res;
}

uint32_t
tcds::cpm::LumiNibbleGroupBase::numBX(bool const gated) const
{
  uint32_t res = 0;
  for (std::vector<tcds::cpm::LumiNibble>::const_iterator i = nibbles_.begin();
       i != nibbles_.end();
       ++i)
    {
      res += i->numBX(gated);
    }
  return res;
}

uint32_t
tcds::cpm::LumiNibbleGroupBase::fillNumber() const
{
  uint32_t res = 0;
  if (nibbles_.size() > 0)
    {
      res = nibbles_.front().fillNumber();
    }
  return res;
}

uint32_t
tcds::cpm::LumiNibbleGroupBase::runNumber() const
{
  uint32_t res = 0;
  if (nibbles_.size() > 0)
    {
      res = nibbles_.front().runNumber();
    }
  return res;
}

uint32_t
tcds::cpm::LumiNibbleGroupBase::lumiSectionNumber() const
{
  uint32_t res = 0;
  if (nibbles_.size() > 0)
    {
      res = nibbles_.front().lumiSectionNumber();
    }
  return res;
}

// The timestamp of the beginning of the first (available) nibble in
// this section.
double
tcds::cpm::LumiNibbleGroupBase::timestampBegin() const
{
  double res = 0;
  if (nibbles_.size() > 0)
    {
      res = nibbles_.front().timestampBegin();
    }
  return res;
}

// The timestamp of the end of the last (available) nibble in this
// section.
double
tcds::cpm::LumiNibbleGroupBase::timestampEnd() const
{
  double res = 0;
  if (nibbles_.size() > 0)
    {
      res = nibbles_.back().timestampEnd();
    }
  return res;
}

uint32_t
tcds::cpm::LumiNibbleGroupBase::triggerCount(unsigned int const trigType,
                                             bool const gated) const
{
  uint32_t res = 0;
  for (std::vector<tcds::cpm::LumiNibble>::const_iterator i = nibbles_.begin();
       i != nibbles_.end();
       ++i)
    {
      res += i->triggerCount(trigType, gated);
    }
  return res;
}

uint32_t
tcds::cpm::LumiNibbleGroupBase::triggerCountTotal(bool const gated) const
{
  uint32_t res = 0;
  for (std::vector<tcds::cpm::LumiNibble>::const_iterator i = nibbles_.begin();
       i != nibbles_.end();
       ++i)
    {
      res += i->triggerCountTotal(gated);
    }
  return res;
}

uint32_t
tcds::cpm::LumiNibbleGroupBase::suppressedTriggerCount(unsigned int const trigType,
                                                       bool const gated) const
{
  uint32_t res = 0;
  for (std::vector<tcds::cpm::LumiNibble>::const_iterator i = nibbles_.begin();
       i != nibbles_.end();
       ++i)
    {
      res += i->suppressedTriggerCount(trigType, gated);
    }
  return res;
}

uint32_t
tcds::cpm::LumiNibbleGroupBase::suppressedTriggerCountTotal(bool const gated) const
{
  uint32_t res = 0;
  for (std::vector<tcds::cpm::LumiNibble>::const_iterator i = nibbles_.begin();
       i != nibbles_.end();
       ++i)
    {
      res += i->suppressedTriggerCountTotal(gated);
    }
  return res;
}

uint32_t
tcds::cpm::LumiNibbleGroupBase::suppressedTriggerCountFromTTS(uint32_t const trigType,
                                                              bool const gated) const
{
  uint32_t res = 0;
  for (std::vector<tcds::cpm::LumiNibble>::const_iterator i = nibbles_.begin();
       i != nibbles_.end();
       ++i)
    {
      res += i->suppressedTriggerCountFromTTS(trigType, gated);
    }
  return res;
}

uint32_t
tcds::cpm::LumiNibbleGroupBase::suppressedTriggerCountFromTriggerRules(uint32_t const trigType,
                                                                       bool const gated) const
{
  uint32_t res = 0;
  for (std::vector<tcds::cpm::LumiNibble>::const_iterator i = nibbles_.begin();
       i != nibbles_.end();
       ++i)
    {
      res += i->suppressedTriggerCountFromTriggerRules(trigType, gated);
    }
  return res;
}

uint32_t
tcds::cpm::LumiNibbleGroupBase::suppressedTriggerCountFromBunchMaskVeto(uint32_t const trigType,
                                                                        bool const gated) const
{
  uint32_t res = 0;
  for (std::vector<tcds::cpm::LumiNibble>::const_iterator i = nibbles_.begin();
       i != nibbles_.end();
       ++i)
    {
      res += i->suppressedTriggerCountFromBunchMaskVeto(trigType, gated);
    }
  return res;
}

uint32_t
tcds::cpm::LumiNibbleGroupBase::suppressedTriggerCountFromRetri(uint32_t const trigType,
                                                                bool const gated) const
{
  uint32_t res = 0;
  for (std::vector<tcds::cpm::LumiNibble>::const_iterator i = nibbles_.begin();
       i != nibbles_.end();
       ++i)
    {
      res += i->suppressedTriggerCountFromRetri(trigType, gated);
    }
  return res;
}

uint32_t
tcds::cpm::LumiNibbleGroupBase::suppressedTriggerCountFromAPVE(uint32_t const trigType,
                                                               bool const gated) const
{
  uint32_t res = 0;
  for (std::vector<tcds::cpm::LumiNibble>::const_iterator i = nibbles_.begin();
       i != nibbles_.end();
       ++i)
    {
      res += i->suppressedTriggerCountFromAPVE(trigType, gated);
    }
  return res;
}

uint32_t
tcds::cpm::LumiNibbleGroupBase::suppressedTriggerCountFromDAQBackpressure(uint32_t const trigType,
                                                                          bool const gated) const
{
  uint32_t res = 0;
  for (std::vector<tcds::cpm::LumiNibble>::const_iterator i = nibbles_.begin();
       i != nibbles_.end();
       ++i)
    {
      res += i->suppressedTriggerCountFromDAQBackpressure(trigType, gated);
    }
  return res;
}

uint32_t
tcds::cpm::LumiNibbleGroupBase::suppressedTriggerCountFromCalibration(uint32_t const trigType,
                                                                      bool const gated) const
{
  uint32_t res = 0;
  for (std::vector<tcds::cpm::LumiNibble>::const_iterator i = nibbles_.begin();
       i != nibbles_.end();
       ++i)
    {
      res += i->suppressedTriggerCountFromCalibration(trigType, gated);
    }
  return res;
}

uint32_t
tcds::cpm::LumiNibbleGroupBase::suppressedTriggerCountFromSWPause(uint32_t const trigType,
                                                                  bool const gated) const
{
  uint32_t res = 0;
  for (std::vector<tcds::cpm::LumiNibble>::const_iterator i = nibbles_.begin();
       i != nibbles_.end();
       ++i)
    {
      res += i->suppressedTriggerCountFromSWPause(trigType, gated);
    }
  return res;
}

uint32_t
tcds::cpm::LumiNibbleGroupBase::suppressedTriggerCountFromFWPause(uint32_t const trigType,
                                                                  bool const gated) const
{
  uint32_t res = 0;
  for (std::vector<tcds::cpm::LumiNibble>::const_iterator i = nibbles_.begin();
       i != nibbles_.end();
       ++i)
    {
      res += i->suppressedTriggerCountFromFWPause(trigType, gated);
    }
  return res;
}

double
tcds::cpm::LumiNibbleGroupBase::triggerRate(unsigned int const trigType,
                                            bool const gated) const
{
  return toRate(triggerCount(trigType, gated));
}

double
tcds::cpm::LumiNibbleGroupBase::triggerRateTotal(bool const gated) const
{
  return toRate(triggerCountTotal(gated));
}

double
tcds::cpm::LumiNibbleGroupBase::suppressedTriggerRate(unsigned int const trigType,
                                                      bool const gated) const
{
  return toRate(suppressedTriggerCount(trigType, gated));
}

double
tcds::cpm::LumiNibbleGroupBase::suppressedTriggerRateTotal(bool const gated) const
{
  return toRate(suppressedTriggerCountTotal(gated));
}

uint32_t
tcds::cpm::LumiNibbleGroupBase::deadtimeBXCountTotal(bool const gated) const
{
  uint32_t res = 0;
  for (std::vector<tcds::cpm::LumiNibble>::const_iterator i = nibbles_.begin();
       i != nibbles_.end();
       ++i)
    {
      res += i->deadtimeBXCountTotal(gated);
    }
  return res;
}

uint32_t
tcds::cpm::LumiNibbleGroupBase::deadtimeBXCountFromTTSAllPartitions(bool const gated) const
{
  uint32_t res = 0;
  for (std::vector<tcds::cpm::LumiNibble>::const_iterator i = nibbles_.begin();
       i != nibbles_.end();
       ++i)
    {
      res += i->deadtimeBXCountFromTTSAllPartitions(gated);
    }
  return res;
}

uint32_t
tcds::cpm::LumiNibbleGroupBase::deadtimeBXCountFromTTSSingleICI(unsigned int const lpmNum,
                                                                unsigned int const iciNum,
                                                                bool const gated) const
{
  uint32_t res = 0;
  for (std::vector<tcds::cpm::LumiNibble>::const_iterator i = nibbles_.begin();
       i != nibbles_.end();
       ++i)
    {
      res += i->deadtimeBXCountFromTTSSingleICI(lpmNum, iciNum, gated);
    }
  return res;
}

uint32_t
tcds::cpm::LumiNibbleGroupBase::deadtimeBXCountFromTTSSingleAPVE(unsigned int const lpmNum,
                                                                 unsigned int const apveNum,
                                                                 bool const gated) const
{
  uint32_t res = 0;
  for (std::vector<tcds::cpm::LumiNibble>::const_iterator i = nibbles_.begin();
       i != nibbles_.end();
       ++i)
    {
      res += i->deadtimeBXCountFromTTSSingleAPVE(lpmNum, apveNum, gated);
    }
  return res;
}

uint32_t
tcds::cpm::LumiNibbleGroupBase::deadtimeBXCountFromTriggerRules(bool const gated) const
{
  uint32_t res = 0;
  for (std::vector<tcds::cpm::LumiNibble>::const_iterator i = nibbles_.begin();
       i != nibbles_.end();
       ++i)
    {
      res += i->deadtimeBXCountFromTriggerRules(gated);
    }
  return res;
}

uint32_t
tcds::cpm::LumiNibbleGroupBase::deadtimeBXCountFromBunchMaskVeto(bool const gated) const
{
  uint32_t res = 0;
  for (std::vector<tcds::cpm::LumiNibble>::const_iterator i = nibbles_.begin();
       i != nibbles_.end();
       ++i)
    {
      res += i->deadtimeBXCountFromBunchMaskVeto(gated);
    }
  return res;
}

uint32_t
tcds::cpm::LumiNibbleGroupBase::deadtimeBXCountFromRetri(bool const gated) const
{
  uint32_t res = 0;
  for (std::vector<tcds::cpm::LumiNibble>::const_iterator i = nibbles_.begin();
       i != nibbles_.end();
       ++i)
    {
      res += i->deadtimeBXCountFromRetri(gated);
    }
  return res;
}

uint32_t
tcds::cpm::LumiNibbleGroupBase::deadtimeBXCountFromAPVE(bool const gated) const
{
  uint32_t res = 0;
  for (std::vector<tcds::cpm::LumiNibble>::const_iterator i = nibbles_.begin();
       i != nibbles_.end();
       ++i)
    {
      res += i->deadtimeBXCountFromAPVE(gated);
    }
  return res;
}

uint32_t
tcds::cpm::LumiNibbleGroupBase::deadtimeBXCountFromDAQBackpressure(bool const gated) const
{
  uint32_t res = 0;
  for (std::vector<tcds::cpm::LumiNibble>::const_iterator i = nibbles_.begin();
       i != nibbles_.end();
       ++i)
    {
      res += i->deadtimeBXCountFromDAQBackpressure(gated);
    }
  return res;
}

uint32_t
tcds::cpm::LumiNibbleGroupBase::deadtimeBXCountFromCalibration(bool const gated) const
{
  uint32_t res = 0;
  for (std::vector<tcds::cpm::LumiNibble>::const_iterator i = nibbles_.begin();
       i != nibbles_.end();
       ++i)
    {
      res += i->deadtimeBXCountFromCalibration(gated);
    }
  return res;
}

uint32_t
tcds::cpm::LumiNibbleGroupBase::deadtimeBXCountFromSWPause(bool const gated) const
{
  uint32_t res = 0;
  for (std::vector<tcds::cpm::LumiNibble>::const_iterator i = nibbles_.begin();
       i != nibbles_.end();
       ++i)
    {
      res += i->deadtimeBXCountFromSWPause(gated);
    }
  return res;
}

uint32_t
tcds::cpm::LumiNibbleGroupBase::deadtimeBXCountFromFWPause(bool const gated) const
{
  uint32_t res = 0;
  for (std::vector<tcds::cpm::LumiNibble>::const_iterator i = nibbles_.begin();
       i != nibbles_.end();
       ++i)
    {
      res += i->deadtimeBXCountFromFWPause(gated);
    }
  return res;
}

uint32_t
tcds::cpm::LumiNibbleGroupBase::deadtimeBXCountFromTriggerRule(unsigned int const ruleNum,
                                                               bool const gated) const
{
  uint32_t res = 0;
  for (std::vector<tcds::cpm::LumiNibble>::const_iterator i = nibbles_.begin();
       i != nibbles_.end();
       ++i)
    {
      res += i->deadtimeBXCountFromTriggerRule(ruleNum, gated);
    }
  return res;
}

double
tcds::cpm::LumiNibbleGroupBase::deadtimeFractionTotal(bool const gated) const
{
  return toTimeFraction(deadtimeBXCountTotal(gated), gated);
}

double
tcds::cpm::LumiNibbleGroupBase::deadtimeFractionFromTTSAllPartitions(bool const gated) const
{
  return toTimeFraction(deadtimeBXCountFromTTSAllPartitions(gated), gated);
}

double
tcds::cpm::LumiNibbleGroupBase::deadtimeFractionFromTTSSingleICI(unsigned int const lpmNum,
                                                                 unsigned int const iciNum,
                                                                 bool const gated) const
{
  return toTimeFraction(deadtimeBXCountFromTTSSingleICI(lpmNum, iciNum, gated), gated);
}

double
tcds::cpm::LumiNibbleGroupBase::deadtimeFractionFromTTSSingleAPVE(unsigned int const lpmNum,
                                                                  unsigned int const apveNum,
                                                                  bool const gated) const
{
  return toTimeFraction(deadtimeBXCountFromTTSSingleAPVE(lpmNum, apveNum, gated), gated);
}

double
tcds::cpm::LumiNibbleGroupBase::deadtimeFractionFromTriggerRules(bool const gated) const
{
  return toTimeFraction(deadtimeBXCountFromTriggerRules(gated), gated);
}

double
tcds::cpm::LumiNibbleGroupBase::deadtimeFractionFromBunchMaskVeto(bool const gated) const
{
  return toTimeFraction(deadtimeBXCountFromBunchMaskVeto(gated), gated);
}

double
tcds::cpm::LumiNibbleGroupBase::deadtimeFractionFromRetri(bool const gated) const
{
  return toTimeFraction(deadtimeBXCountFromRetri(gated), gated);
}

double
tcds::cpm::LumiNibbleGroupBase::deadtimeFractionFromAPVE(bool const gated) const
{
  return toTimeFraction(deadtimeBXCountFromAPVE(gated), gated);
}

double
tcds::cpm::LumiNibbleGroupBase::deadtimeFractionFromDAQBackpressure(bool const gated) const
{
  return toTimeFraction(deadtimeBXCountFromDAQBackpressure(gated), gated);
}

double
tcds::cpm::LumiNibbleGroupBase::deadtimeFractionFromCalibration(bool const gated) const
{
  return toTimeFraction(deadtimeBXCountFromCalibration(gated), gated);
}

double
tcds::cpm::LumiNibbleGroupBase::deadtimeFractionFromSWPause(bool const gated) const
{
  return toTimeFraction(deadtimeBXCountFromSWPause(gated), gated);
}

double
tcds::cpm::LumiNibbleGroupBase::deadtimeFractionFromFWPause(bool const gated) const
{
  return toTimeFraction(deadtimeBXCountFromFWPause(gated), gated);
}

double
tcds::cpm::LumiNibbleGroupBase::deadtimeFractionFromTriggerRule(unsigned int const ruleNum,
                                                                bool const gated) const
{
  return toTimeFraction(deadtimeBXCountFromTriggerRule(ruleNum, gated), gated);
}

tcds::cpm::LumiNibble
tcds::cpm::LumiNibbleGroupBase::nibble(size_t const i) const
{
  return nibbles_.at(i);
}

double
tcds::cpm::LumiNibbleGroupBase::toRate(uint32_t const count) const
{
  double const rate = 1. * count / numOrbits() * tcds::definitions::kLHCOrbitFreq;
  return rate;
}

double
tcds::cpm::LumiNibbleGroupBase::toTimeFraction(uint32_t const count,
                                               bool const gated) const
{
  uint32_t const num = count;
  uint32_t const den = numBX(gated);
  double fraction = 0.;
  if (den != 0.)
    {
      fraction = 100. * num / den;
    }
  return fraction;
}
