#include "tcds/cpm/BRILDAQPublisher.h"

#include <cstddef>

#include "eventing/api/exception/Exception.h"
#include "toolbox/mem/HeapAllocator.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/net/URN.h"
#include "toolbox/string.h"
#include "xdata/exdr/Serializer.h"
#include "xdata/exdr/FixedSizeOutputStreamBuffer.h"
#include "xdata/Properties.h"
#include "xcept/Exception.h"

#include "tcds/cpm/BRILDAQTable.h"
#include "tcds/cpm/LumiNibble.h"
#include "tcds/exception/Exception.h"

tcds::cpm::BRILDAQPublisher::BRILDAQPublisher(xdaq::Application& xdaqApp,
                                              std::string const& busName,
                                              std::string const& topicName) :
  eventing::api::Member(&xdaqApp),
  memPoolFactoryP_(0),
  memPoolP_(0),
  busName_(busName),
  topicName_(topicName)
{
  // Construct our memory pool.
  memPoolFactoryP_ = toolbox::mem::getMemoryPoolFactory();
  memPoolName_ = "mempool_brildaq_publisher";
  toolbox::mem::HeapAllocator* const allocator = new toolbox::mem::HeapAllocator();
  toolbox::net::URN urn("toolbox-mempool", memPoolName_);
  memPoolP_ = memPoolFactoryP_->createPool(urn, allocator);
}

tcds::cpm::BRILDAQPublisher::~BRILDAQPublisher()
{
  toolbox::net::URN urn("toolbox-mempool", memPoolName_);
  memPoolFactoryP_->destroyPool(urn);
}

void
tcds::cpm::BRILDAQPublisher::publishBRILDAQTable(uint32_t const swVersion,
                                                 uint32_t const fwVersion,
                                                 uint32_t const dataSourceId,
                                                 tcds::cpm::LumiNibble const& nibble)
{
  // We need a property list in order to publish. In our case it only
  // contains the data source id, which in practice is the CPM FED id.
  xdata::Properties pList;
  pList.setProperty("DataSourceId", toolbox::toString("%d", dataSourceId));

  size_t const maxSize = 4096;

  toolbox::mem::Reference* const bufRef = memPoolFactoryP_->getFrame(memPoolP_, maxSize);
  xdata::exdr::FixedSizeOutputStreamBuffer outBuffer(static_cast<char*>(bufRef->getDataLocation()), maxSize);
  tcds::cpm::BRILDAQTable table(swVersion, fwVersion, dataSourceId, nibble);

  try
    {
      xdata::exdr::Serializer serializer;
      serializer.exportAll(&table, &outBuffer);
      bufRef->setDataSize(outBuffer.tellp());
    }
  catch (xcept::Exception& err)
    {
      bufRef->release();
      std::string const msg =
        toolbox::toString("Failed to serialize the BRILDAQ data table: '%s'.", err.what());
      XCEPT_RETHROW(tcds::exception::BRILDAQProblem, msg, err);
    }

  try
    {
      getEventingBus(busName_).publish(topicName_, bufRef, pList);
    }
  catch (eventing::api::exception::Exception& err)
    {
      bufRef->release();
      std::string const msg =
        toolbox::toString("Failed to publish to BRILDAQ eventing. Bus name '%s', topic name '%s'. Problem: '%s'.",
                          busName_.c_str(),
                          topicName_.c_str(),
                          err.what());
      XCEPT_RETHROW(tcds::exception::BRILDAQProblem, msg, err);
    }
}
