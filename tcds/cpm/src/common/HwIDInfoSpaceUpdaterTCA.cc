#include "tcds/cpm/HwIDInfoSpaceUpdaterTCA.h"

#include <stdint.h>
#include <string>

#include "tcds/cpm/TCADeviceCPMT1.h"
#include "tcds/cpm/TCADeviceCPMT2.h"
#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/InfoSpaceItem.h"

tcds::cpm::HwIDInfoSpaceUpdaterTCA::HwIDInfoSpaceUpdaterTCA(tcds::utils::XDAQAppBase& xdaqApp,
                                                            tcds::cpm::TCADeviceCPMT1 const& hwT1,
                                                            tcds::cpm::TCADeviceCPMT2 const& hwT2) :
  tcds::utils::HwInfoSpaceUpdaterBase(xdaqApp, hwT1),
  hwT1_(hwT1),
  hwT2_(hwT2)
{
}

bool
tcds::cpm::HwIDInfoSpaceUpdaterTCA::updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                                        tcds::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  bool updated = false;
  tcds::cpm::TCADeviceCPMT1 const& hwT1 = getHwT1();
  tcds::cpm::TCADeviceCPMT2 const& hwT2 = getHwT2();
  if (hwT1.isHwConnected() && hwT2.isHwConnected())
    {
      std::string name = item.name();
      tcds::utils::InfoSpaceItem::UpdateType updateType = item.updateType();
      if (updateType == tcds::utils::InfoSpaceItem::PROCESS)
        {
          // The 'PROCESS' update type means that there is something
          // special to the variable. Figure out what to do based on the
          // variable name.
          if (name == "board_id_t1")
            {
              std::string newVal = hwT1.readBoardId();
              infoSpaceHandler->setString(name, newVal);
              updated = true;
            }
          else if (name == "board_id_t2")
            {
              std::string newVal = hwT2.readBoardId();
              infoSpaceHandler->setString(name, newVal);
              updated = true;
            }
          else if (name == "system_id_t1")
            {
              std::string newVal = hwT1.readSystemId();
              infoSpaceHandler->setString(name, newVal);
              updated = true;
            }
          else if (name == "system_id_t2")
            {
              std::string newVal = hwT2.readSystemId();
              infoSpaceHandler->setString(name, newVal);
              updated = true;
            }
          else if (name == "firmware_version_t1")
            {
              std::string newVal = hwT1.readSystemFirmwareVersion();
              infoSpaceHandler->setString(name, newVal);
              updated = true;
            }
          else if (name == "firmware_version_t2")
            {
              std::string newVal = hwT2.readSystemFirmwareVersion();
              infoSpaceHandler->setString(name, newVal);
              updated = true;
            }
          else if (name == "firmware_date_t1")
            {
              std::string newVal = hwT1.readSystemFirmwareDate();
              infoSpaceHandler->setString(name, newVal);
              updated = true;
            }
          else if (name == "firmware_date_t2")
            {
              std::string newVal = hwT2.readSystemFirmwareDate();
              infoSpaceHandler->setString(name, newVal);
              updated = true;
            }
          else if (name == "pm_state")
            {
              uint32_t newVal = hwT1.getPMState();
              infoSpaceHandler->setUInt32(name, newVal);
              updated = true;
            }
        }
      if (!updated)
        {
          // NOTE: This only works for T1 quantities.
          updated = tcds::utils::HwInfoSpaceUpdaterBase::updateInfoSpaceItem(item, infoSpaceHandler);
        }
    }

  if (updated)
    {
      item.setValid();
    }
  else
    {
      item.setInvalid();
    }

  return updated;
}

tcds::cpm::TCADeviceCPMT1 const&
tcds::cpm::HwIDInfoSpaceUpdaterTCA::getHwT1() const
{
  return hwT1_;
}

tcds::cpm::TCADeviceCPMT2 const&
tcds::cpm::HwIDInfoSpaceUpdaterTCA::getHwT2() const
{
  return hwT2_;
}
