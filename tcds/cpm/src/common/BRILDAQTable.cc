#include "tcds/cpm/BRILDAQTable.h"

#include "xdata/Boolean.h"
#include "xdata/Float.h"
#include "xdata/UnsignedInteger.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/TimeVal.h"
#include "xdata/Vector.h"

#include "tcds/cpm/LumiNibble.h"
#include "tcds/utils/Definitions.h"

tcds::cpm::BRILDAQTable::BRILDAQTable(uint32_t const swVersionIn,
                                      uint32_t const fwVersionIn,
                                      uint32_t const dataSourceIdIn,
                                      tcds::cpm::LumiNibble const& nibble)
{
  // NOTE: Whenever you change the contents, make sure to update the
  // format version as well!

  // NOTE: Because the following code calls virtual methods
  // (addColumn() and setValueAt()) from the constructor, these calls
  // will end up at the BRILDAQTable implementations, and not deeper
  // in the inheritance tree. In this case this is exactly what we
  // want. Therefore these calls are explicitly prefixed with
  // 'BRILDAQTable::'.

  //----------

  // First build the table structure.

  // Our very own data version number.
  BRILDAQTable::addColumn("FormatVersion", "unsigned int");

  // The data-source identifier (i.e., the service name of the parent
  // XDAQ application).
  BRILDAQTable::addColumn("DataSourceId", "unsigned int");

  // Firmware and software identifiers.
  BRILDAQTable::addColumn("SwVersion", "unsigned int 32");
  BRILDAQTable::addColumn("FwVersion", "unsigned int 32");

  // The usuable BRILDAQ book keeping info.
  BRILDAQTable::addColumn("FillNumber", "unsigned int 32");
  BRILDAQTable::addColumn("RunNumber", "unsigned int 32");
  BRILDAQTable::addColumn("SectionNumber", "unsigned int 32");
  BRILDAQTable::addColumn("NibbleNumber", "unsigned int 32");

  // The bulk of the data straight from the CPM then.
  // OBSOLETE OBSOLETE OBSOLETE
  // Just kept for temporary backward compatibility.
  BRILDAQTable::addColumn("BSTSignalState", "unsigned int 32");
  // OBSOLETE OBSOLETE OBSOLETE end
  BRILDAQTable::addColumn("BSTSignalStatus", "unsigned int 32");
  // OBSOLETE OBSOLETE OBSOLETE
  // Just kept for temporary backward compatibility.
  BRILDAQTable::addColumn("BSTTimestamp", "unsigned int 32");
  // OBSOLETE OBSOLETE OBSOLETE end
  BRILDAQTable::addColumn("BSTTimestampBegin", "time");
  BRILDAQTable::addColumn("BSTTimestampEnd", "time");
  BRILDAQTable::addColumn("CMSRunActive", "bool");
  BRILDAQTable::addColumn("NumOrbits", "unsigned int 32");
  BRILDAQTable::addColumn("NumNibblesPerSection", "unsigned int 32");

  // These two do not come from the CPM hardware, but are calculated
  // by the software.
  BRILDAQTable::addColumn("NumBX", "unsigned int 32");
  BRILDAQTable::addColumn("NumBXBeamActive", "unsigned int 32");

  // These two do come from the CPM hardware.
  BRILDAQTable::addColumn("NumBXCMSDead", "unsigned int 32");
  BRILDAQTable::addColumn("NumBXCMSDeadBeamActive", "unsigned int 32");

  // Trigger counts and suppressed-trigger counts for this nibble,
  // split per trigger type.
  BRILDAQTable::addColumn("NumTriggersByType", "vector unsigned int 32");
  BRILDAQTable::addColumn("NumSuppressedTriggersByType", "vector unsigned int 32");
  BRILDAQTable::addColumn("NumTriggersByTypeBeamActive", "vector unsigned int 32");
  BRILDAQTable::addColumn("NumSuppressedTriggersByTypeBeamActive", "vector unsigned int 32");

  // Two versions of the overall deadtime number.
  BRILDAQTable::addColumn("Deadtime", "float");
  BRILDAQTable::addColumn("DeadtimeBeamActive", "float");

  //----------

  // Now fill the table with data. A single row only, of course.
  xdata::UnsignedInteger formatVersion = kFormatVersion;
  xdata::UnsignedInteger dataSourceId = dataSourceIdIn;
  xdata::UnsignedInteger32 swVersion = swVersionIn;
  xdata::UnsignedInteger32 fwVersion = fwVersionIn;
  xdata::UnsignedInteger32 fillNumber = nibble.fillNumber();
  xdata::UnsignedInteger32 runNumber = nibble.runNumber();
  xdata::UnsignedInteger32 sectionNumber = nibble.lumiSectionNumber();
  xdata::UnsignedInteger32 nibbleNumber = nibble.lumiNibbleNumber();
  xdata::UnsignedInteger32 bstSignalStatus = nibble.bstSignalStatus();
  xdata::UnsignedInteger32 bstTimestamp = nibble.timestamp();
  xdata::TimeVal bstTimestampBegin(nibble.timestampBegin());
  xdata::TimeVal bstTimestampEnd(nibble.timestampEnd());
  xdata::Boolean cmsRunActive = nibble.isCMSRunActive();
  xdata::UnsignedInteger32 numOrbits = nibble.numOrbits();
  xdata::UnsignedInteger32 numNibblesPerSection = nibble.numNibblesPerSection();
  xdata::UnsignedInteger32 numBX = nibble.numBX(false);
  xdata::UnsignedInteger32 numBXBeamActive = nibble.numBX(true);
  xdata::UnsignedInteger32 numBXCMSDead = nibble.deadtimeBXCountTotal(false);
  xdata::UnsignedInteger32 numBXCMSDeadBeamActive = nibble.deadtimeBXCountTotal(true);
  xdata::Vector<xdata::UnsignedInteger32> numTriggersByType;
  xdata::Vector<xdata::UnsignedInteger32> numTriggersByTypeBeamActive;
  for (int i = tcds::definitions::kTrigTypeMin;
       i <= tcds::definitions::kTrigTypeMax;
       ++i)
    {
      numTriggersByType.push_back(xdata::UnsignedInteger32(nibble.triggerCount(i, false)));
      numTriggersByTypeBeamActive.push_back(xdata::UnsignedInteger32(nibble.triggerCount(i, true)));
    }
  xdata::Vector<xdata::UnsignedInteger32> numSuppressedTriggersByType;
  xdata::Vector<xdata::UnsignedInteger32> numSuppressedTriggersByTypeBeamActive;
  for (int i = tcds::definitions::kTrigTypeMin;
       i <= tcds::definitions::kTrigTypeMax;
       ++i)
    {
      numSuppressedTriggersByType.push_back(xdata::UnsignedInteger32(nibble.suppressedTriggerCount(i, false)));
      numSuppressedTriggersByTypeBeamActive.push_back(xdata::UnsignedInteger32(nibble.suppressedTriggerCount(i, true)));
    }
  xdata::Float deadtime = nibble.deadtimeFractionTotal(false);
  xdata::Float deadtimeBeamActive = nibble.deadtimeFractionTotal(true);

  BRILDAQTable::setValueAt(0, "FormatVersion", formatVersion);
  BRILDAQTable::setValueAt(0, "DataSourceId", dataSourceId);
  BRILDAQTable::setValueAt(0, "SwVersion", swVersion);
  BRILDAQTable::setValueAt(0, "FwVersion", fwVersion);
  BRILDAQTable::setValueAt(0, "FillNumber", fillNumber);
  BRILDAQTable::setValueAt(0, "RunNumber", runNumber);
  BRILDAQTable::setValueAt(0, "SectionNumber", sectionNumber);
  BRILDAQTable::setValueAt(0, "NibbleNumber", nibbleNumber);
  BRILDAQTable::setValueAt(0, "BSTSignalState", bstSignalStatus);
  BRILDAQTable::setValueAt(0, "BSTSignalStatus", bstSignalStatus);
  BRILDAQTable::setValueAt(0, "BSTTimestamp", bstTimestamp);
  BRILDAQTable::setValueAt(0, "BSTTimestampBegin", bstTimestampBegin);
  BRILDAQTable::setValueAt(0, "BSTTimestampEnd", bstTimestampEnd);
  BRILDAQTable::setValueAt(0, "CMSRunActive", cmsRunActive);
  BRILDAQTable::setValueAt(0, "NumOrbits", numOrbits);
  BRILDAQTable::setValueAt(0, "NumNibblesPerSection", numNibblesPerSection);
  BRILDAQTable::setValueAt(0, "NumBX", numBX);
  BRILDAQTable::setValueAt(0, "NumBXBeamActive", numBXBeamActive);
  BRILDAQTable::setValueAt(0, "NumBXCMSDead", numBXCMSDead);
  BRILDAQTable::setValueAt(0, "NumBXCMSDeadBeamActive", numBXCMSDeadBeamActive);
  BRILDAQTable::setValueAt(0, "NumTriggersByType", numTriggersByType);
  BRILDAQTable::setValueAt(0, "NumSuppressedTriggersByType", numSuppressedTriggersByType);
  BRILDAQTable::setValueAt(0, "NumTriggersByTypeBeamActive", numTriggersByTypeBeamActive);
  BRILDAQTable::setValueAt(0, "NumSuppressedTriggersByTypeBeamActive", numSuppressedTriggersByTypeBeamActive);
  BRILDAQTable::setValueAt(0, "Deadtime", deadtime);
  BRILDAQTable::setValueAt(0, "DeadtimeBeamActive", deadtimeBeamActive);
}

tcds::cpm::BRILDAQTable::~BRILDAQTable()
{
}
