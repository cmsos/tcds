#include "tcds/cpm/CPMInputsInfoSpaceHandler.h"

#include <stdint.h>
#include <vector>

#include "tcds/pm/PatternTriggerRAMContents.h"
#include "tcds/pm/WebTablePatternTriggerRAM.h"
#include "tcds/cpm/Definitions.h"
#include "tcds/cpm/Utils.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/Utils.h"
#include "tcds/utils/WebServer.h"

tcds::cpm::CPMInputsInfoSpaceHandler::CPMInputsInfoSpaceHandler(xdaq::Application& xdaqApp,
                                                                tcds::utils::InfoSpaceUpdater* updater) :
  InfoSpaceHandler(xdaqApp, "tcds-cpm-inputs", updater)
{
  //----------

  // The 'mode' of the GT input. This is a combination of the
  // 'external_trigger0_enable' flag on the T1 and the L1A_1/2
  // selection on the T2.
  createUInt32("external_trigger0_mode",
               tcds::definitions::CPM_EXTERNAL_TRIGGER_SOURCE_DISABLED,
               "",
               tcds::utils::InfoSpaceItem::PROCESS);

  // The enable/disable flag for the front-panel NIM trigger input.
  createBool("main.inselect.external_trigger1_enable", false, "enabled/disabled");

  // Software-triggered triggers and B-gos.
  createBool("main.inselect.combined_software_trigger_and_bgo_enable", false, "enabled/disabled");

  // TTS state-triggered triggers and B-gos.
  createBool("main.inselect.combined_tts_action_trigger_and_bgo_enable", false, "enabled/disabled");

  // Cyclic triggers (and not B-gos!).
  createBool("main.inselect.cyclic_trigger_enable", false, "enabled/disabled");

  // The bunch-mask trigger generator.
  createBool("main.inselect.bunch_mask_trigger_enable", false, "enabled/disabled");

  // The random-trigger generator.
  createBool("main.inselect.random_trigger_enable", false, "enabled/disabled");

  // The pattern trigger generator.
  createBool("main.inselect.pattern_trigger_enable", false, "enabled/disabled");

  //----------

  // The delays (in BX) applied to the external trigger sources.
  createUInt32("main.trigger_delay.external_trigger0_delay");
  createUInt32("main.trigger_delay.external_trigger1_delay");

  //----------

  // Orbit source settings.
  createBool("main.inselect.ttcmi_orbit_select", false, "enabled/disabled");
  createUInt32("main.orbit_alignment_config.orbit_offset");

  //----------

  // The requested random-trigger rate.
  createUInt32("random_trigger_rate", 0, "", tcds::utils::InfoSpaceItem::PROCESS);

  //----------

  // The trigger bunch mask.
  createUInt32("main.bunch_mask_trigger_config.prescale");
  createUInt32("main.bunch_mask_trigger_config.initial_prescale");
  createUInt32("bunch_mask_trigger_pattern_size", 0, "", tcds::utils::InfoSpaceItem::PROCESS);
  createString("bunch_mask_trigger_pattern", "", "", tcds::utils::InfoSpaceItem::PROCESS);

  //----------

  // The RAM-based pattern trigger generator (pattern).
  createBool("main.pattern_trigger_config.loop", false, "true/false");
  createBool("main.pattern_trigger_config.sync_loop", false, "true/false");
  createUInt32Vec("pattern_trigger_ram");
}

tcds::cpm::CPMInputsInfoSpaceHandler::~CPMInputsInfoSpaceHandler()
{
}

void
tcds::cpm::CPMInputsInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  // Trigger and B-go sources.
  monitor.newItemSet("itemset-inputs");
  monitor.addItem("itemset-inputs",
                  "external_trigger0_mode",
                  "Front-panel GT trigger input",
                  this);
  monitor.addItem("itemset-inputs",
                  "main.trigger_delay.external_trigger0_delay",
                  "Front-panel GT trigger input delay (BX)",
                  this);
  monitor.addItem("itemset-inputs",
                  "main.inselect.external_trigger1_enable",
                  "Front-panel NIM trigger input",
                  this);
  monitor.addItem("itemset-inputs",
                  "main.trigger_delay.external_trigger1_delay",
                  "Front-panel NIM trigger input delay (BX)",
                  this);
  monitor.addItem("itemset-inputs",
                  "main.inselect.combined_software_trigger_and_bgo_enable",
                  "IPbus/SOAP-based triggers and B-gos",
                  this);
  monitor.addItem("itemset-inputs",
                  "main.inselect.combined_tts_action_trigger_and_bgo_enable",
                  "TTS state-driven triggers and B-gos",
                  this);
  monitor.addItem("itemset-inputs",
                  "main.inselect.cyclic_trigger_enable",
                  "Cyclic triggers",
                  this);
  monitor.addItem("itemset-inputs",
                  "main.inselect.bunch_mask_trigger_enable",
                  "BX-mask trigger generator",
                  this);
  monitor.addItem("itemset-inputs",
                  "main.inselect.random_trigger_enable",
                  "Random-trigger generator",
                  this);
  monitor.addItem("itemset-inputs",
                  "main.inselect.pattern_trigger_enable",
                  "Pattern trigger generator",
                  this);

  // Orbit sources and settings.
  monitor.newItemSet("itemset-orbitsource");
  monitor.addItem("itemset-orbitsource",
                  "main.inselect.ttcmi_orbit_select",
                  "Orbit source: TTCmi",
                  this,
                  "If set, the front-panel orbit input is used as orbit marker."
                  " Otherwise, an orbit marker is generated internally.");
  monitor.addItem("itemset-orbitsource",
                  "main.orbit_alignment_config.orbit_offset",
                  "Orbit offset (BX)",
                  this);

  // The trigger bunch mask.
  monitor.newItemSet("itemset-trigger-mask");
  monitor.addItem("itemset-trigger-mask",
                  "main.bunch_mask_trigger_config.prescale",
                  "Prescale",
                  this);
  monitor.addItem("itemset-trigger-mask",
                  "main.bunch_mask_trigger_config.initial_prescale",
                  "Initial prescale value",
                  this);
  monitor.addItem("itemset-trigger-mask",
                  "bunch_mask_trigger_pattern_size",
                  "Number of forced-trigger BXs per orbit",
                  this);
  monitor.addItem("itemset-trigger-mask",
                  "bunch_mask_trigger_pattern",
                  "Forced-trigger BXs",
                  this);

  // The random-trigger.
  monitor.newItemSet("itemset-random-trigger");
  monitor.addItem("itemset-random-trigger",
                  "random_trigger_rate",
                  "Requested random-trigger rate (Hz)",
                  this);

  // The RAM-based pattern trigger.
  monitor.newItemSet("itemset-pattern-trigger");
  monitor.addItem("itemset-pattern-trigger",
                  "main.pattern_trigger_config.loop",
                  "Loop mode",
                  this);
  monitor.addItem("itemset-pattern-trigger",
                  "main.pattern_trigger_config.sync_loop",
                  "Synchronize loop to BC0",
                  this);
  monitor.addItem("itemset-pattern-trigger",
                  "pattern_trigger_ram",
                  "Trigger/delay pattern",
                  this);
}

void
tcds::cpm::CPMInputsInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                    tcds::utils::Monitor& monitor,
                                                                    std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "Input sources" : forceTabName;

  webServer.registerTab(tabName,
                        "General CPM information",
                        4);

  webServer.registerTable("Orbit source",
                          "Orbit source selection and configuration",
                          monitor,
                          "itemset-orbitsource",
                          tabName);

  // A spacer to enforce the layout of the HyperDAQ page.
  webServer.registerSpacer("Spacer",
                           "",
                           monitor,
                           "",
                           tabName,
                           3);

  webServer.registerTable("Trigger/B-go inputs",
                          "Trigger and B-go source selection and configuration",
                          monitor,
                          "itemset-inputs",
                          tabName);

  webServer.registerTable("BX-mask trigger",
                          "Bunch-mask trigger configuration",
                          monitor,
                          "itemset-trigger-mask",
                          tabName);

  webServer.registerTable("Random trigger",
                          "Random-trigger configuration",
                          monitor,
                          "itemset-random-trigger",
                          tabName);

  webServer.registerWebObject<tcds::pm::WebTablePatternTriggerRAM>("Pattern trigger",
                                                                   "Pattern trigger configuration",
                                                                   monitor,
                                                                   "itemset-pattern-trigger",
                                                                   tabName);
}

std::string
tcds::cpm::CPMInputsInfoSpaceHandler::formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const
{
  std::string res = tcds::utils::escapeAsJSONString(tcds::utils::InfoSpaceHandler::kInvalidItemString);
  if (item->isValid())
    {
      std::string name = item->name();
      if (name == "pattern_trigger_ram")
        {
          std::vector<uint32_t> const ramContentsRaw = getUInt32Vec(name);
          tcds::pm::PatternTriggerRAMContents const ramContents(ramContentsRaw);
          res = ramContents.getJSONString();
        }
      else if (name == "external_trigger0_mode")
        {
          uint32_t const val = getUInt32(name);
          tcds::definitions::CPM_EXTERNAL_TRIGGER0_SOURCE_MODE const tmp =
            static_cast<tcds::definitions::CPM_EXTERNAL_TRIGGER0_SOURCE_MODE>(val);
          res = tcds::utils::escapeAsJSONString(tcds::cpm::externalTrigger0ModeToString(tmp));
        }
      else
        {
          // For everything else simply call the generic formatter.
          res = InfoSpaceHandler::formatItem(item);
        }
    }
  return res;
}
