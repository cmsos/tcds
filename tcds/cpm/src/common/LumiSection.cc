#include "tcds/cpm/LumiSection.h"

#include <cstddef>
#include <stdint.h>
#include <string>

#include "toolbox/string.h"
#include "xcept/Exception.h"

#include "tcds/exception/Exception.h"
#include "tcds/utils/Definitions.h"

tcds::cpm::LumiSection::LumiSection(tcds::cpm::LumiNibble const& nibble) :
  tcds::cpm::LumiNibbleGroupBase(nibble)
{
}

tcds::cpm::LumiSection::LumiSection(std::vector<tcds::cpm::LumiNibble> const& nibbles) :
  tcds::cpm::LumiNibbleGroupBase(nibbles)
{
}

tcds::cpm::LumiSection::~LumiSection()
{
}

void
tcds::cpm::LumiSection::addNibble(tcds::cpm::LumiNibble const& nibble)
{
  // NOTE: No check on the fill number. The fill number comes from the
  // BST, and can in principle change during a run. One does not want
  // that to crash the run. Similar for the run number (although this
  // comes from software and is not expected to change during runs).

  if (nibble.lumiSectionNumber() != nibbles_.front().lumiSectionNumber())
    {
      std::string msgBase = "Trying to add a lumi nibble from lumi section %d to lumi section %d.";
      std::string const msg = toolbox::toString(msgBase.c_str(),
                                                nibble.lumiSectionNumber(),
                                                nibbles_.front().lumiSectionNumber());
      XCEPT_RAISE(tcds::exception::SoftwareProblem, msg.c_str());
    }

  tcds::cpm::LumiNibbleGroupBase::addNibble(nibble);
}

double
tcds::cpm::LumiSection::completeness() const
{
  uint32_t const numNibbles = nibbles_.size();
  uint32_t const numNibblesExpected = nibbles_.at(0).numNibblesPerSection();
  double const completeness = 1. * numNibbles / numNibblesExpected;
  return completeness;
}

double
tcds::cpm::LumiSection::bstSignalQuality() const
{
  size_t numNibblesGood = 0;
  for (std::vector<tcds::cpm::LumiNibble>::const_iterator i = nibbles_.begin();
       i != nibbles_.end();
       ++i)
    {
      if (i->bstSignalStatus() == tcds::definitions::BST_SIGNAL_STATUS_GOOD)
        {
          ++numNibblesGood;
        }
    }

  size_t const numNibbles = nibbles_.size();
  double const quality = 1. * numNibblesGood / numNibbles;
  return quality;
}

double
tcds::cpm::LumiSection::fracCMSRunActive() const
{
  size_t numNibblesActive = 0;
  for (std::vector<tcds::cpm::LumiNibble>::const_iterator i = nibbles_.begin();
       i != nibbles_.end();
       ++i)
    {
      if (i->isCMSRunActive())
        {
          ++numNibblesActive;
        }
    }

  size_t const numNibbles = nibbles_.size();
  double const activeFraction = 1. * numNibblesActive / numNibbles;
  return activeFraction;
}
