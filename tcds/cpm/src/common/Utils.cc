#include "tcds/cpm/Utils.h"

#include <vector>

#include "toolbox/string.h"
#include "xcept/Exception.h"

#include "tcds/cpm/LumiNibbleGroupBase.h"
#include "tcds/exception/Exception.h"
#include "tcds/utils/Definitions.h"
#include "tcds/utils/InfoSpaceHandler.h"

std::string
tcds::cpm::externalTrigger0ModeToString(tcds::definitions::CPM_EXTERNAL_TRIGGER0_SOURCE_MODE const& mode)
{
  std::string res = "unknown";

  switch (mode)
    {
    case tcds::definitions::CPM_EXTERNAL_TRIGGER_SOURCE_UNKNOWN:
      res = "unknown";
      break;
    case tcds::definitions::CPM_EXTERNAL_TRIGGER_SOURCE_DISABLED:
      res = "disabled";
      break;
    case tcds::definitions::CPM_EXTERNAL_TRIGGER_SOURCE_L1A1:
      res = "L1A_1";
      break;
    case tcds::definitions::CPM_EXTERNAL_TRIGGER_SOURCE_L1A2:
      res = "L1A_2";
      break;
    default:
      XCEPT_RAISE(tcds::exception::ValueError,
                  toolbox::toString("'%d' is not a valid CPM external trigger-0 (i.e., GT) mode.",
                                    mode));
      break;
    }
  return res;
}

void
tcds::cpm::CreateBRILDAQStuff(tcds::utils::InfoSpaceHandler& infoSpaceHandler)
{
  // (Suppressed) trigger counts.
  infoSpaceHandler.createUInt32("trigger_count_total");
  infoSpaceHandler.createUInt32("suppressed_trigger_count_total");
  infoSpaceHandler.createUInt32("trigger_count_beamactive_total");
  infoSpaceHandler.createUInt32("suppressed_trigger_count_beamactive_total");
  for (int trigType = tcds::definitions::kTrigTypeMin;
       trigType <= tcds::definitions::kTrigTypeMax;
       ++trigType)
    {
      // The per-type trigger counts:
      // - plain,
      infoSpaceHandler.createUInt32(toolbox::toString("trigger_count_trigtype%d", trigType));
      // - gated with beam presence.
      infoSpaceHandler.createUInt32(toolbox::toString("trigger_count_beamactive_trigtype%d", trigType));
      // The missed-trigger counts:
      // - plain,
      infoSpaceHandler.createUInt32(toolbox::toString("suppressed_trigger_count_trigtype%d", trigType));
      // - gated with beam presence.
      infoSpaceHandler.createUInt32(toolbox::toString("suppressed_trigger_count_beamactive_trigtype%d", trigType));
    }

  // Rates.
  infoSpaceHandler.createFloat("trigger_rate_total");
  infoSpaceHandler.createFloat("suppressed_trigger_rate_total");
  infoSpaceHandler.createFloat("trigger_rate_beamactive_total");
  infoSpaceHandler.createFloat("suppressed_trigger_rate_beamactive_total");
  for (int trigType = tcds::definitions::kTrigTypeMin;
       trigType <= tcds::definitions::kTrigTypeMax;
       ++trigType)
    {
      // The per-type trigger rates:
      // - plain,
      infoSpaceHandler.createFloat(toolbox::toString("trigger_rate_trigtype%d", trigType));
      // - gated with beam presence.
      infoSpaceHandler.createFloat(toolbox::toString("trigger_rate_beamactive_trigtype%d", trigType));
      // The missed-trigger rates:
      // - plain,
      infoSpaceHandler.createFloat(toolbox::toString("suppressed_trigger_rate_trigtype%d", trigType));
      // - gated with beam presence.
      infoSpaceHandler.createFloat(toolbox::toString("suppressed_trigger_rate_beamactive_trigtype%d", trigType));
    }

  // Deadtimes.
  // - Plain.
  infoSpaceHandler.createFloat("deadtime_total");
  infoSpaceHandler.createFloat("deadtime_tts");
  infoSpaceHandler.createFloat("deadtime_trigger_rules");
  infoSpaceHandler.createFloat("deadtime_bunch_mask_veto");
  infoSpaceHandler.createFloat("deadtime_retri");
  infoSpaceHandler.createFloat("deadtime_apve");
  infoSpaceHandler.createFloat("deadtime_daq_backpressure");
  infoSpaceHandler.createFloat("deadtime_calibration");
  infoSpaceHandler.createFloat("deadtime_sw_pause");
  infoSpaceHandler.createFloat("deadtime_fw_pause");
  // - Gated with beam presence.
  infoSpaceHandler.createFloat("deadtime_beamactive_total");
  infoSpaceHandler.createFloat("deadtime_beamactive_tts");
  infoSpaceHandler.createFloat("deadtime_beamactive_trigger_rules");
  infoSpaceHandler.createFloat("deadtime_beamactive_bunch_mask_veto");
  infoSpaceHandler.createFloat("deadtime_beamactive_retri");
  infoSpaceHandler.createFloat("deadtime_beamactive_apve");
  infoSpaceHandler.createFloat("deadtime_beamactive_daq_backpressure");
  infoSpaceHandler.createFloat("deadtime_beamactive_calibration");
  infoSpaceHandler.createFloat("deadtime_beamactive_sw_pause");
  infoSpaceHandler.createFloat("deadtime_beamactive_fw_pause");
}

void
tcds::cpm::UpdateBRILDAQStuff(tcds::utils::InfoSpaceHandler& infoSpaceHandler,
                              tcds::cpm::LumiNibbleGroupBase const& input)
{
  std::vector<std::string> types;
  types.push_back("");
  types.push_back("_beamactive");

  // The trigger counts.
  for (std::vector<std::string>::const_iterator it = types.begin();
       it != types.end();
       ++it)
    {
      bool const gated = (*it == "_beamactive");
      infoSpaceHandler.setUInt32(toolbox::toString("trigger_count%s_total", it->c_str()),
                                 input.triggerCountTotal(gated));
      for (int trigType = tcds::definitions::kTrigTypeMin;
           trigType <= tcds::definitions::kTrigTypeMax;
           ++trigType)
        {
          infoSpaceHandler.setUInt32(toolbox::toString("trigger_count%s_trigtype%d", it->c_str(), trigType),
                                     input.triggerCount(trigType, gated));
        }
    }

  //----------

  // The missed-trigger counts.
  for (std::vector<std::string>::const_iterator it = types.begin();
       it != types.end();
       ++it)
    {
      bool const gated = (*it == "_beamactive");
      infoSpaceHandler.setUInt32(toolbox::toString("suppressed_trigger_count%s_total", it->c_str()),
                                 input.suppressedTriggerCountTotal(gated));
      for (int trigType = tcds::definitions::kTrigTypeMin;
           trigType <= tcds::definitions::kTrigTypeMax;
           ++trigType)
        {
          infoSpaceHandler.setUInt32(toolbox::toString("suppressed_trigger_count%s_trigtype%d", it->c_str(), trigType),
                                     input.suppressedTriggerCount(trigType, gated));
        }
    }

  //----------

  // The trigger rates.
  for (std::vector<std::string>::const_iterator it = types.begin();
       it != types.end();
       ++it)
    {
      bool const gated = (*it == "_beamactive");
      infoSpaceHandler.setFloat(toolbox::toString("trigger_rate%s_total", it->c_str()),
                                input.triggerRateTotal(gated));
      for (int trigType = tcds::definitions::kTrigTypeMin;
           trigType <= tcds::definitions::kTrigTypeMax;
           ++trigType)
        {
          infoSpaceHandler.setFloat(toolbox::toString("trigger_rate%s_trigtype%d", it->c_str(), trigType),
                                    input.triggerRate(trigType, gated));
        }
    }

  //----------

  // The missed-trigger rates.
  for (std::vector<std::string>::const_iterator it = types.begin();
       it != types.end();
       ++it)
    {
      bool const gated = (*it == "_beamactive");
      infoSpaceHandler.setFloat(toolbox::toString("suppressed_trigger_rate%s_total", it->c_str()),
                                input.suppressedTriggerRateTotal(gated));
      for (int trigType = tcds::definitions::kTrigTypeMin;
           trigType <= tcds::definitions::kTrigTypeMax;
           ++trigType)
        {
          infoSpaceHandler.setFloat(toolbox::toString("suppressed_trigger_rate%s_trigtype%d", it->c_str(), trigType),
                                    input.suppressedTriggerRate(trigType, gated));
        }
    }

  //----------

  // Deadtimes.
  for (std::vector<std::string>::const_iterator it = types.begin();
       it != types.end();
       ++it)
    {
      bool const gated = (*it == "_beamactive");
      infoSpaceHandler.setFloat(toolbox::toString("deadtime%s_total", it->c_str()),
                                input.deadtimeFractionTotal(gated));
      infoSpaceHandler.setFloat(toolbox::toString("deadtime%s_tts", it->c_str()),
                                input.deadtimeFractionFromTTSAllPartitions(gated));
      infoSpaceHandler.setFloat(toolbox::toString("deadtime%s_trigger_rules", it->c_str()),
                                input.deadtimeFractionFromTriggerRules(gated));
      infoSpaceHandler.setFloat(toolbox::toString("deadtime%s_bunch_mask_veto", it->c_str()),
                                input.deadtimeFractionFromBunchMaskVeto(gated));
      infoSpaceHandler.setFloat(toolbox::toString("deadtime%s_retri", it->c_str()),
                                input.deadtimeFractionFromRetri(gated));
      infoSpaceHandler.setFloat(toolbox::toString("deadtime%s_apve", it->c_str()),
                                input.deadtimeFractionFromAPVE(gated));
      infoSpaceHandler.setFloat(toolbox::toString("deadtime%s_daq_backpressure", it->c_str()),
                                input.deadtimeFractionFromDAQBackpressure(gated));
      infoSpaceHandler.setFloat(toolbox::toString("deadtime%s_calibration", it->c_str()),
                                input.deadtimeFractionFromCalibration(gated));
      infoSpaceHandler.setFloat(toolbox::toString("deadtime%s_sw_pause", it->c_str()),
                                input.deadtimeFractionFromSWPause(gated));
      infoSpaceHandler.setFloat(toolbox::toString("deadtime%s_fw_pause", it->c_str()),
                                input.deadtimeFractionFromFWPause(gated));
    }
}
