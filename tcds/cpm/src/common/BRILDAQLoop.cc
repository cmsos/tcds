#include "tcds/cpm/BRILDAQLoop.h"

#include <iostream>
#include <memory>
#include <sstream>
#include <cstddef>
#include <stdint.h>
#include <unistd.h>
#include <utility>
#include <vector>

#include "toolbox/BSem.h"
#include "toolbox/TimeVal.h"
#include "toolbox/string.h"
#include "toolbox/task/Action.h"
#include "toolbox/task/WorkLoop.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/task/exception/Exception.h"
#include "xcept/Exception.h"
#include "xdaq/ApplicationDescriptor.h"

#include "tcds/cpm/BRILDAQPacket.h"
#include "tcds/cpm/BRILDAQPublisher.h"
#include "tcds/cpm/CPMController.h"
#include "tcds/cpm/ConfigurationInfoSpaceHandler.h"
#include "tcds/cpm/TCADeviceCPMT1.h"
#include "tcds/exception/Exception.h"
#include "tcds/utils/LockGuard.h"

tcds::cpm::BRILDAQLoop::BRILDAQLoop(tcds::cpm::CPMController& cpmController) :
  nibbleDAQAlarmName_("CPMNibbleDAQAlarm"),
  brilDAQAlarmName_("CPMBRILDAQAlarm"),
  cpmController_(cpmController),
  workLoopName_(""),
  numNibblesEmpty_(0),
  numNibblesReadMulti_(0),
  numNibblesInvalid_(0),
  numNibblesCorrupted_(0),
  numNibblesAlmostCorrupted_(0),
  numNibblesMissed_(0),
  lock_(toolbox::BSem::FULL),
  swVersion_(0),
  fwVersion_(0),
  fedId_(0),
  brildaqNibbleStorage_(cpmController),
  brildaqSectionStorage_(cpmController),
  grandmasterStorage_(cpmController)
{
  std::stringstream workLoopName;
  uint32_t const localId = cpmController_.getApplicationDescriptor()->getLocalId();
  workLoopName << "BRILDAQWorkLoop_" << localId;
  workLoopName_ = workLoopName.str();
  daqWorkLoopP_ = std::unique_ptr<toolbox::task::WorkLoop>(toolbox::task::getWorkLoopFactory()->getWorkLoop(workLoopName_, "waiting"));
  daqWorkLoopP_->addExceptionListener(this);
}

tcds::cpm::BRILDAQLoop::~BRILDAQLoop()
{
  stop();
  daqWorkLoopP_.reset();
  brildaqPublisherP_.reset();
}

void
tcds::cpm::BRILDAQLoop::onException(xcept::Exception& err)
{
  // Start by raising the alarms.
  std::string const problemDescBase =
    "The CPMController NibbleDAQ/BRILDAQ workloop failed: '%s'.";
  std::string const problemDesc = toolbox::toString(problemDescBase.c_str(), err.what());

  XCEPT_DECLARE(tcds::exception::BRILDAQFailureAlarm, alarmException0, problemDesc);
  cpmController_.raiseAlarm(brilDAQAlarmName_, alarmException0);
  XCEPT_DECLARE(tcds::exception::NibbleDAQFailureAlarm, alarmException1, problemDesc);
  cpmController_.raiseAlarm(nibbleDAQAlarmName_, alarmException1);

  //--------

  // A little grace period before restarting the workloop may help (in
  // case the underlying problem goes away with time). It also makes
  // the problem more noticeable in the monitoring.
  unsigned const kWorkloopRestartGraceTime = 5;

  bool restartSucceeded = false;
  while (! restartSucceeded)
    {
      ::sleep(kWorkloopRestartGraceTime);
      try
        {
          start();
          restartSucceeded = true;
        }
      catch (xcept::Exception const& err)
        {
          // Raise the alarmWs.
          std::string const problemDesc =
            toolbox::toString("Failed to restart the NibbleDAQ/BRILDAQ workloop: '%s'.",
                              err.what());
          XCEPT_DECLARE(tcds::exception::BRILDAQFailureAlarm, alarmException0, problemDesc);
          cpmController_.raiseAlarm(brilDAQAlarmName_, alarmException0);
          XCEPT_DECLARE(tcds::exception::NibbleDAQFailureAlarm, alarmException1, problemDesc);
          cpmController_.raiseAlarm(nibbleDAQAlarmName_, alarmException1);
        }
      catch (...)
        {
          // Raise the alarms.
          std::string const problemDesc =
            "Failed to restart the NibbleDAQ/BRILDAQ workloop.";
          XCEPT_DECLARE(tcds::exception::BRILDAQFailureAlarm, alarmException0, problemDesc);
          cpmController_.raiseAlarm(brilDAQAlarmName_, alarmException0);
          XCEPT_DECLARE(tcds::exception::NibbleDAQFailureAlarm, alarmException1, problemDesc);
          cpmController_.raiseAlarm(nibbleDAQAlarmName_, alarmException1);
        }
    }

  // Revoke the alarms.
  std::string const reason =
    "Successfully restarted the CPMController NibbleDAQ/BRILDAQ workloop.";
  cpmController_.revokeAlarm(brilDAQAlarmName_, reason);
  cpmController_.revokeAlarm(nibbleDAQAlarmName_, reason);
}

void
tcds::cpm::BRILDAQLoop::start()
{
  clearHistory();
  std::string const problemDescBase =
    "Failed to start NibbleDAQ/BRILDAQ workloop: '%s'.";
  std::string problemDesc = "";
  if (daqWorkLoopP_.get())
    {
      try
        {
          if (!daqWorkLoopActionP_.get())
            {
              daqWorkLoopActionP_ =
                std::unique_ptr<toolbox::task::ActionSignature>(toolbox::task::bind(this, &tcds::cpm::BRILDAQLoop::update, "updateAction"));
            }
          // We (try to) remove the old task from the work loop, and
          // then (re)submit the same task again. This covers both the
          // situation in which the run was stopped (and the original
          // task is still present in the work loop queue) and the
          // situation in which an exception was thrown in the task
          // (and the task was removed from the work loop queue).
          try
            {
              daqWorkLoopP_->remove(daqWorkLoopActionP_.get());
            }
          catch (toolbox::task::exception::Exception&)
            {
              // Nothing to do here. We were just making sure that
              // we're not submitting the same task multiple times.
            }
          daqWorkLoopP_->submit(daqWorkLoopActionP_.get());
          if (!daqWorkLoopP_->isActive())
            {
              daqWorkLoopP_->activate();
            }
        }
      catch (toolbox::task::exception::Exception const& err)
        {
          problemDesc = toolbox::toString(problemDescBase.c_str(), err.what());
        }
    }
  else
    {
      problemDesc = toolbox::toString(problemDescBase.c_str(), "Work loop does not exist.");
    }

  // If necessary: raise the alarms.
  if (problemDesc.size())
    {
      XCEPT_DECLARE(tcds::exception::BRILDAQFailureAlarm, alarmException0, problemDesc);
      cpmController_.raiseAlarm(brilDAQAlarmName_, alarmException0);
      XCEPT_DECLARE(tcds::exception::NibbleDAQFailureAlarm, alarmException1, problemDesc);
      cpmController_.raiseAlarm(nibbleDAQAlarmName_, alarmException1);
    }
}

void
tcds::cpm::BRILDAQLoop::stop()
{
  if (daqWorkLoopP_.get())
    {
      if (daqWorkLoopP_->isActive())
        {
          try
            {
              daqWorkLoopP_->cancel();
            }
          catch (...)
            {
              // Nothing to do here. This must mean the workloop
              // already disappeared.
            }
        }
    }
}

void
tcds::cpm::BRILDAQLoop::resetCounters()
{
  tcds::utils::LockGuard<tcds::utils::Lock> guardedLock(lock_);

  numNibblesEmpty_ = 0;
  numNibblesReadMulti_ = 0;
  numNibblesInvalid_ = 0;
  numNibblesCorrupted_ = 0;
  numNibblesAlmostCorrupted_ = 0;
  numNibblesMissed_ = 0;
}

void
tcds::cpm::BRILDAQLoop::clearHistory()
{
  tcds::utils::LockGuard<tcds::utils::Lock> guardedLock(lock_);

  resetCounters();
  nibbles_.clear();
  sections_.clear();
}

bool
tcds::cpm::BRILDAQLoop::isHwConnected() const
{
  tcds::cpm::TCADeviceCPMT1 const& hw = cpmController_.getHwT1();
  return hw.isHwConnected();
}

bool
tcds::cpm::BRILDAQLoop::update(toolbox::task::WorkLoop* workLoop)
{
  static std::unique_ptr<BRILDAQPacket> brildaqPacketPrev;
  std::unique_ptr<BRILDAQPacket> brildaqPacket;
  tcds::cpm::TCADeviceCPMT1 const& hw = cpmController_.getHwT1();

  if (!hw.isHwConnected())
    {
      brildaqPublisherP_.reset();
      swVersion_ = 0;
      fwVersion_ = 0;
      fedId_ = 0;
    }

  if (hw.isHwConnected())
    {

      if (!brildaqPublisherP_.get())
        {
          swVersion_ = cpmController_.getHwT1().readSwVersionRaw();
          fwVersion_ = cpmController_.getHwT1().readFwVersionRaw();
          fedId_ = cpmController_.getHwT1().readFEDId();
          std::string const busName =
            cpmController_.getConfigurationInfoSpaceHandler().getString("busName");
          std::string const topicName =
            cpmController_.getConfigurationInfoSpaceHandler().getString("topicName");
          brildaqPublisherP_ =
            std::unique_ptr<tcds::cpm::BRILDAQPublisher>(new tcds::cpm::BRILDAQPublisher(cpmController_, busName, topicName));
        }

      // Read the (hopefully) new packet.
      try
        {
          std::vector<uint32_t> const rawPacket = hw.readBRILDAQPacket();
          double const timestamp = toolbox::TimeVal::gettimeofday();
          brildaqPacket =
            std::unique_ptr<BRILDAQPacket>(new BRILDAQPacket(rawPacket, timestamp));
          // If we get here, let's revoke any existing alarm...
          cpmController_.revokeAlarm(nibbleDAQAlarmName_);
        }
      catch (tcds::exception::Exception const& err)
        {
          // Raise the alarm.
          std::string const msg =
            toolbox::toString("At least one CPM nibbleDAQ update poll failed: '%s'.", err.what());
          XCEPT_DECLARE(tcds::exception::NibbleDAQFailureAlarm, e, msg);
          cpmController_.raiseAlarm(nibbleDAQAlarmName_, e);
        }

      //----------

      if (brildaqPacket.get())
        {
          std::string errMsg = "UNKNOWN";
          std::string const msgBase = "NibbleDAQ problem encountered";
          bool const isEmpty = brildaqPacket->isEmpty();
          if (isEmpty)
            {
              // Not much we can do in this case.
              std::cout << "DEBUG JGH Found an empty nibble" << std::endl;
              ++numNibblesEmpty_;
              // No error in this case.
              errMsg = "";
            }
          else
            {
              // Check if we already have this packet (and have read
              // it again) or not.
              bool const isDuplicate =
                (brildaqPacketPrev.get()
                 && ((brildaqPacket->runNumber() == brildaqPacketPrev->runNumber())
                     && (brildaqPacket->lumiSectionNumber() == brildaqPacketPrev->lumiSectionNumber())
                     && (brildaqPacket->lumiNibbleNumber() == brildaqPacketPrev->lumiNibbleNumber())));

              // Let's turn the raw BRILDAQ packet into a LumiNibble.
              LumiNibble const currentNibble(*brildaqPacket);
              bool isGood = false;
              if (isDuplicate)
                {
                  ++numNibblesReadMulti_;
                  // No error in this case.
                  errMsg = "";
                }
              else
                {
                  // std::cout << "DEBUG JGH Found a new nibble"
                  //           << " " << currentNibble.runNumber() << ":" << currentNibble.lumiSectionNumber() << ":" << currentNibble.lumiNibbleNumber()
                  //           << " spanning " << currentNibble.numOrbits() << " orbits"
                  //           << " or " << currentNibble.numBX(false) << "/" << currentNibble.numBX(true) << " BX"
                  //           << " of which " << currentNibble.deadtimeBXCountTotal(false) << "/" << currentNibble.deadtimeBXCountTotal(true) << " BX are deadtime"
                  //           << std::endl;

                  // Kinda coarse locking, but okay.
                  // NOTE: Just make sure the BRILDAQ publishing is
                  // not included inside the lock. If that times out,
                  // it will slow down everything else as well.
                  tcds::utils::LockGuard<tcds::utils::Lock> guardedLock(lock_);

                  // Check if our nibble is good or not.
                  if (currentNibble.isCorrupted())
                    {
                      // Handle corrupted nibble.
                      std::cout << "DEBUG JGH Found a corrupted nibble" << std::endl;
                      ++numNibblesCorrupted_;
                      errMsg = toolbox::toString("%s: read a corrupted nibble packet. "
                                                 "Header: run %d, section %d, nibble %d, "
                                                 "footer: run %d, section %d, nibble %d.",
                                                 msgBase.c_str(),
                                                 brildaqPacket->runNumber(),
                                                 brildaqPacket->lumiSectionNumber(true),
                                                 brildaqPacket->lumiNibbleNumber(true),
                                                 brildaqPacket->runNumber(),
                                                 brildaqPacket->lumiSectionNumber(false),
                                                 brildaqPacket->lumiNibbleNumber(false));
                    }
                  else if (currentNibble.isAlmostCorrupted())
                    {
                      // Handle almost-corrupted nibble.
                      std::cout << "DEBUG JGH Found an almost-corrupted nibble" << std::endl;
                      ++numNibblesAlmostCorrupted_;
                      // No error in this case.
                      errMsg = "";
                    }
                  else if (currentNibble.isInvalid())
                    {
                      // Handle invalid nibble.
                      std::cout << "DEBUG JGH Found an invalid nibble" << std::endl;
                      ++numNibblesInvalid_;
                      errMsg = toolbox::toString("%s: read an invalid nibble packet. "
                                                 "Section run %d, %d, nibble %d.",
                                                 msgBase.c_str(),
                                                 brildaqPacket->runNumber(),
                                                 brildaqPacket->lumiSectionNumber(true),
                                                 brildaqPacket->lumiNibbleNumber(true));
                    }
                  else
                    {
                      isGood = true;
                      // No error in this case.
                      errMsg = "";
                    }

                  bool const isFirst = nibbles_.empty();
                  bool isExpected = isFirst;
                  if (isGood && !isFirst)
                    {
                      // Apparently we have a valid, new nibble in
                      // hand. Let's see if it is the one we expect.
                      // There are basically three possibilities for
                      // the 'next expected' nibble after the nibble
                      // with run X, section Y, nibble Z:

                      // - Run X, section Y, nibble Z+1 (if z < 64).
                      // - Run X, section Y+1, nibble 1 (if z = 64).
                      // - Run X', section 1, nibble 1 (in case of a new run).
                      LumiNibble const& latestNibble = nibbles_.back();
                      bool const case1 =
                        ((currentNibble.runNumber() ==
                          latestNibble.runNumber())
                         && (currentNibble.lumiSectionNumber() == latestNibble.lumiSectionNumber())
                         && (currentNibble.lumiNibbleNumber() == (latestNibble.lumiNibbleNumber() + 1)));
                      bool const case2 =
                        ((currentNibble.runNumber() == latestNibble.runNumber())
                         && (currentNibble.lumiSectionNumber() == (latestNibble.lumiSectionNumber() + 1))
                         && (currentNibble.lumiNibbleNumber() == 1));
                      bool const case3 =
                        ((currentNibble.runNumber() > latestNibble.runNumber())
                         && (currentNibble.lumiSectionNumber() == 1)
                         && (currentNibble.lumiNibbleNumber() == 1));

                      isExpected = (case1 || case2 || case3);
                    }

                  int numMissed = 0;
                  if (!isExpected)
                    {
                      // Hmm...... Not the nibble we expected.
                      LumiNibble const& latestNibble = nibbles_.back();
                      uint32_t const numNibblesPerSection = latestNibble.numNibblesPerSection();

                      std::cout << "DEBUG JGH Found an unexpected nibble" << std::endl;
                      std::cout << "DEBUG JGH   latestNibble.runNumber() = "
                                << latestNibble.runNumber() << std::endl;
                      std::cout << "DEBUG JGH   latestNibble.lumiSectionNumber() = "
                                << latestNibble.lumiSectionNumber() << std::endl;
                      std::cout << "DEBUG JGH   latestNibble.lumiNibbleNumber() = "
                                << latestNibble.lumiNibbleNumber() << std::endl;
                      std::cout << "DEBUG JGH   currentNibble.runNumber() = "
                                << currentNibble.runNumber() << std::endl;
                      std::cout << "DEBUG JGH   currentNibble.lumiSectionNumber() = "
                                << currentNibble.lumiSectionNumber() << std::endl;
                      std::cout << "DEBUG JGH   currentNibble.lumiNibbleNumber() = "
                                << currentNibble.lumiNibbleNumber() << std::endl;

                      // Two dinstict cases: change of run, or missing
                      // nibbles within the same run.
                      // TODO TODO TODO
                      // - Still have to finish implementing the
                      //   'gap-filler' nibbles.
                      // TODO TODO TODO end

                      // NOTE: The following is a bit of a hack. In
                      // production conditions it is straightforward
                      // to detect the start of a new run. In test
                      // setups like the B14 TCDS lab, however, test
                      // runs typically all have the same run number,
                      // so we need a little hack to detect thiscase.

                      bool const isNewRun = ((currentNibble.runNumber() != latestNibble.runNumber()) ||
                                             ((currentNibble.runNumber() == latestNibble.runNumber()) &&
                                              (currentNibble.lumiSectionNumber() < latestNibble.lumiSectionNumber())) ||
                                             ((currentNibble.runNumber() == latestNibble.runNumber()) &&
                                              (currentNibble.lumiSectionNumber() == latestNibble.lumiSectionNumber()) &&
                                              (currentNibble.lumiNibbleNumber() < latestNibble.lumiNibbleNumber())));
                      bool const isSameRun = !isNewRun;
                      if (isSameRun)
                        {
                          // Same run.
                          uint32_t secNum = latestNibble.lumiSectionNumber();
                          uint32_t nibNum = latestNibble.lumiNibbleNumber() + 1;
                          if (nibNum > numNibblesPerSection)
                            {
                              ++secNum;
                              nibNum = 1;
                            }
                          while (!(secNum == currentNibble.lumiSectionNumber()
                                   && (nibNum == currentNibble.lumiNibbleNumber())))
                            {
                              std::cout << "DEBUG JGH     0 " << secNum << " " << nibNum << std::endl;
                              ++numMissed;
                              if (nibNum == numNibblesPerSection)
                                {
                                  ++secNum;
                                  nibNum = 1;
                                }
                              else
                                {
                                  ++nibNum;
                                }
                            }

                          // // Add missing nibbles in the last section
                          // // we saw.
                          // for (uint32_t nibNum = latestNibble.lumiNibbleNumber() + 1;
                          //      nibNum <= numNibblesPerSection;
                          //      ++nibNum)
                          //   {
                          //     std::cout << "DEBUG JGH     1 " << nibNum << std::endl;
                          //     ++numMissed;
                          //     // tcds::cpm::LumiNibble(tcds::cpm::BRILDAQPacket(,
                          //     //                                                toolbox::TimeVal::gettimeofday()));
                          //   }
                          // // Add missing nibbles for completely missing sections.
                          // for (uint32_t secNum = latestNibble.lumiSectionNumber() + 1;
                          //      secNum < currentNibble.lumiSectionNumber();
                          //      ++secNum)
                          //   {
                          //     for (uint32_t nibNum = 1;
                          //          nibNum <= numNibblesPerSection;
                          //          ++nibNum)
                          //       {
                          //         std::cout << "DEBUG JGH     2 " << secNum << " " << nibNum << std::endl;
                          //         ++numMissed;
                          //       }
                          //   }
                          // // Add missing nibbles in the current section.
                          // for (uint32_t nibNum = 1;
                          //      nibNum < currentNibble.lumiNibbleNumber();
                          //      ++nibNum)
                          //   {
                          //     std::cout << "DEBUG JGH     3 " << nibNum << std::endl;
                          //     ++numMissed;
                          //   }
                        }
                      else
                        {
                          // Change of run.
                          // NOTE: In this case we only estimate the
                          // missing nibbles in the _current_ run.

                          // Add missing nibbles for completely missing sections.
                          for (uint32_t secNum = 1;
                               secNum < currentNibble.lumiSectionNumber();
                               ++secNum)
                            {
                              for (uint32_t nibNum = 1;
                                   nibNum <= numNibblesPerSection;
                                   ++nibNum)
                                {
                                  std::cout << "DEBUG JGH     4 " << secNum << " " << nibNum << std::endl;
                                  ++numMissed;
                                }
                            }
                          // Add missing nibbles in the current section.
                          for (uint32_t nibNum = 1;
                               nibNum < currentNibble.lumiNibbleNumber();
                               ++nibNum)
                            {
                              std::cout << "DEBUG JGH     5 " << nibNum << std::endl;
                              ++numMissed;
                            }
                        }
                      std::cout << "DEBUG JGH   estimated number of missed nibbles: "
                                << numMissed << std::endl;
                      errMsg = toolbox::toString("%s: missed at least one nibble packet. "
                                                 "Previous nibble received was "
                                                 "run %d, section %d, nibble %d, "
                                                 "expecting %d nibbles per section. "
                                                 "Current nibble is "
                                                 "run %d, section %d, nibble %d.",
                                                 msgBase.c_str(),
                                                 latestNibble.runNumber(),
                                                 latestNibble.lumiSectionNumber(),
                                                 latestNibble.lumiNibbleNumber(),
                                                 numNibblesPerSection,
                                                 currentNibble.runNumber(),
                                                 currentNibble.lumiSectionNumber(),
                                                 currentNibble.lumiNibbleNumber());
                    }
                  numNibblesMissed_ += numMissed;

                  // - Storage of per-nibble BRILDAQ data.
                  brildaqNibbleStorage_.update(currentNibble);
                  brildaqNibbleStorage_.writeInfoSpace();

                  // Keep track of short-term nibble history.
                  nibbles_.push_back(currentNibble);
                  // Limit the nibble history to the number of nibbles
                  // in a section, as determined from the latest
                  // nibble received.
                  size_t const maxNibbleHistoryLength = currentNibble.numNibblesPerSection();
                  while (nibbles_.size() > maxNibbleHistoryLength)
                    {
                      nibbles_.pop_front();
                    }

                  // Add the current nibble to the ongoing (or next)
                  // section.
                  // bool haveNewSection = false;
                  if (sections_.size())
                    {
                      if (currentNibble.lumiSectionNumber() == sections_.back().lumiSectionNumber())
                        {
                          sections_.back().addNibble(currentNibble);
                        }
                      else
                        {
                          // - Storage of per-section BRILDAQ data.
                          tcds::cpm::LumiSection const currentSection = sections_.back();
                          brildaqSectionStorage_.update(currentSection);
                          brildaqSectionStorage_.writeInfoSpace();

                          // Storage of the 'grandmaster' lumi section table.
                          grandmasterStorage_.update(currentSection);
                          grandmasterStorage_.writeInfoSpace();

                          sections_.push_back(LumiSection(currentNibble));
                          // haveNewSection = true;
                        }
                    }
                  else
                    {
                      sections_.push_back(LumiSection(currentNibble));
                      // NOTE: In this case we leave haveNewSection =
                      // false, since there was not even an 'old' lumi
                      // section.
                    }

                  // Limit the section history to some arbitrary (but
                  // well-defined) number. (I.e., the same as the
                  // nibble history length.)
                  size_t const maxSectionHistoryLength = maxNibbleHistoryLength;
                  while (sections_.size() > maxSectionHistoryLength)
                    {
                      sections_.pop_front();
                    }

                  // Publish the current lumi nibble to BRILDAQ, store
                  // it through XMAS, and add it to the running lumi
                  // section.
                  // - Publish to BRILDAQ.
                  try
                    {
                      brildaqPublisherP_->publishBRILDAQTable(swVersion_,
                                                              fwVersion_,
                                                              fedId_,
                                                              currentNibble);
                      // If we get here, let's revoke any existing
                      // alarm...
                      cpmController_.revokeAlarm(brilDAQAlarmName_);
                    }
                  catch (tcds::exception::Exception const& err)
                    {
                      // Raise the alarm.
                      std::string const msg =
                        toolbox::toString("At least one CPM BRILDAQ publication update failed: '%s'.", err.what());
                      XCEPT_DECLARE(tcds::exception::BRILDAQFailureAlarm, e, msg);
                      cpmController_.raiseAlarm(brilDAQAlarmName_, e);
                    }
                }

              // Prepare for the next iteration.
              brildaqPacketPrev = std::move(brildaqPacket);
            }

          if (!errMsg.empty())
            {
              XCEPT_DECLARE(tcds::exception::NibbleDAQProblem, e, errMsg);
              cpmController_.notifyQualified("error", e);
            }
        }

      //----------

      //     bool readSomethingTwice = true;
      //     bool missedSomething = true;
      //     uint32_t numMissed = 0;

      //     uint32_t currSection = 0;
      //     uint32_t currNibble = 0;
      //     uint32_t prevSection = 0;
      //     uint32_t prevNibble = 0;
      //     uint32_t prevNumNibblesPerSection = 0;
      //     uint32_t prevRun = 0;

      //     if (!brildaqPacket->isCorrupted() &&
      //         brildaqPacket->isValid())
      //       {
      //         // See if we have missed any nibbles, or something like
      //         // that.
      //         if (nibbles_.size() < 1)
      //           {
      //             readSomethingTwice = false;
      //             missedSomething = false;
      //           }
      //         else
      //           {
      //             LumiNibble const& latestNibble = nibbles_.back();
      //             currSection = brildaqPacket->lumiSectionNumber();
      //             currNibble = brildaqPacket->lumiNibbleNumber();
      //             prevSection = latestNibble.lumiSectionNumber();
      //             prevNibble = latestNibble.lumiNibbleNumber();
      //             prevNumNibblesPerSection = brildaqPacket->numNibblesPerSection();
      //             prevRun = brildaqPacket->runNumber();

      //             if ((currSection == prevSection) && (currNibble == prevNibble))
      //               {
      //                 readSomethingTwice = true;
      //                 missedSomething = false;
      //               }
      //             else
      //               {
      //                 readSomethingTwice = false;
      //                 // NOTE: Special case for when a new run has
      //                 // started. Recognized from the fact that the
      //                 // current nibble is section 1, nibble 1.
      //                 if ((currSection == 1) && (currNibble == 1))
      //                   {
      //                     numMissed = 0;
      //                   }
      //                 else if (currSection == prevSection)
      //                   {
      //                     numMissed = currNibble - prevNibble - 1;
      //                   }
      //                 else
      //                   {
      //                     uint32_t const numNibblesPerSection = brildaqPacket->numNibblesPerSection();
      //                     numMissed =
      //                       (currSection - prevSection - 1) * numNibblesPerSection +
      //                       numNibblesPerSection - prevNibble +
      //                       currNibble - 1;
      //                   }
      //                 missedSomething = (numMissed != 0);
      //               }
      //           }
      //       }

      //     //----------

      //     bool dropThisPacket =
      //       !brildaqPacket->isValid() ||
      //       brildaqPacket->isCorrupted() ||
      //       readSomethingTwice;
      //     havePacket = !dropThisPacket;

      //     if (!dropThisPacket)
      //       {
      //         LumiNibble const nibbleToKeep(*brildaqPacket);
      //         nibbles_.push_back(nibbleToKeep);
      //         size_t const maxNibbleHistoryLength = brildaqPacket->numNibblesPerSection();
      //         // Limit the nibble history to the number of nibbles in
      //         // a section, as determined from the latest nibble
      //         // received.
      //         while (nibbles_.size() > maxNibbleHistoryLength)
      //           {
      //             nibbles_.pop_front();
      //           }
      //         if (sections_.size() > 0)
      //           {
      //             if (brildaqPacket->lumiSectionNumber() == sections_.back().lumiSectionNumber())
      //               {
      //                 sections_.back().addNibble(*brildaqPacket);
      //               }
      //             else
      //               {
      //                 sections_.push_back(LumiSection(*brildaqPacket));
      //                 haveNewSection = true;
      //               }
      //           }
      //         else
      //           {
      //             sections_.push_back(LumiSection(*brildaqPacket));
      //             // NOTE: In this case we leave haveNewSection =
      //             // false, since there was not even an 'old' lumi
      //             // section.
      //           }

      //         // Limit the section history to some arbitrary (but
      //         // defined) number.
      //         while (sections_.size() > kMaxSectionHistoryLength)
      //           {
      //             sections_.pop_front();
      //           }
      //       }

      //     //----------

      //     // Send an error in case we encountered any kind of problem.
      //     std::string const msgBase = "NibbleDAQ problem encountered";
      //     std::string msg;
      //     if (!brildaqPacket->isValid())
      //       {
      //         numNibblesInvalid_ += 1;
      //         msg = toolbox::toString("%s: read an invalid nibble packet. "
      //                                 "Section run %d, %d, nibble %d.",
      //                                 msgBase.c_str(),
      //                                 brildaqPacket->runNumber(),
      //                                 brildaqPacket->lumiSectionNumber(true),
      //                                 brildaqPacket->lumiNibbleNumber(true));
      //       }
      //     else if (brildaqPacket->isCorrupted())
      //       {
      //         numNibblesCorrupted_ += 1;
      //         msg = toolbox::toString("%s: read a corrupted nibble packet. "
      //                                 "Header: run %d, section %d, nibble %d, "
      //                                 "footer: run %d, section %d, nibble %d.",
      //                                 msgBase.c_str(),
      //                                 brildaqPacket->runNumber(),
      //                                 brildaqPacket->lumiSectionNumber(true),
      //                                 brildaqPacket->lumiNibbleNumber(true),
      //                                 brildaqPacket->runNumber(),
      //                                 brildaqPacket->lumiSectionNumber(false),
      //                                 brildaqPacket->lumiNibbleNumber(false));
      //       }
      //     else if (brildaqPacket->isAlmostCorrupted())
      //       {
      //         numNibblesAlmostCorrupted_ += 1;
      //         // No error in this case.
      //         msg = "";
      //       }
      //     else if (missedSomething)
      //       {
      //         numNibblesMissed_ += numMissed;
      //         msg = toolbox::toString("%s: missed at least one nibble packet. "
      //                                 "Last packet received was "
      //                                 "run %d, section %d, nibble %d, "
      //                                 "expecting %d nibbles per section. "
      //                                 "Current packet is section %d, nibble %d.",
      //                                 msgBase.c_str(),
      //                                 prevRun,
      //                                 prevSection,
      //                                 prevNibble,
      //                                 prevNumNibblesPerSection,
      //                                 currSection,
      //                                 currNibble);
      //       }
      //     else if (readSomethingTwice)
      //       {
      //         numNibblesReadMulti_ += 1;
      //         // No error in this case.
      //         msg = "";
      //       }
      //     if (!msg.empty())
      //       {
      //         XCEPT_DECLARE(tcds::exception::NibbleDAQProblem, e, msg);
      //         cpmController_.notifyQualified("fatal", e);
      //       }
      //   }

      // //----------

      // // If the packet is worth keeping, it's worth publishing to the
      // // BRILDAQ as well.
      // if (havePacket)
      //   {
      //     LumiNibble const nibbleToStore(*brildaqPacket);
      //     try
      //       {
      //         brildaqPublisherP_->publishBRILDAQTable(swVersion_,
      //                                                 fwVersion_,
      //                                                 fedId_,
      //                                                 nibbleToStore);
      //         // If we get here, let's revoke any existing
      //         // alarm...
      //         cpmController_.revokeAlarm(brilDAQAlarmName_);
      //       }
      //     catch (tcds::exception::Exception const& err)
      //       {
      //         // Raise the alarm.
      //         std::string const msg =
      //           toolbox::toString("At least one CPM BRILDAQ publication update failed: '%s'.", err.what());
      //         XCEPT_DECLARE(tcds::exception::BRILDAQFailureAlarm, e, msg);
      //         cpmController_.raiseAlarm(brilDAQAlarmName_, "fatal", e);
      //       }

      //     //----------

      //     // Storage of per-nibble BRILDAQ data.
      //     brildaqNibbleStorage_.update(nibbleToStore);
      //     brildaqNibbleStorage_.writeInfoSpace();
      //   }

      // //----------

      // // If we have a new section, let's push the old one into the
      // // 'storage' InfoSpace on its way to the database.
      // if (haveNewSection)
      //   {
      //     // ASSERT ASSERT ASSERT
      //     assert (sections_.size() > 1);
      //     // ASSERT ASSERT ASSERT end

      //     LumiSection const& sectionToStore = sections_.at(sections_.size() - 2);

      //     //----------

      //     // The book keeping information, for BRILDAQ.
      //     brildaqStorage_.setUInt32("fill_number", sectionToStore.fillNumber());
      //     brildaqStorage_.setUInt32("run_number", sectionToStore.runNumber());
      //     brildaqStorage_.setUInt32("section_number", sectionToStore.lumiSectionNumber());

      //     brildaqStorage_.setUInt32("num_nibbles", sectionToStore.numNibbles());

      //     //----------

      //     std::vector<std::string> types;
      //     types.push_back("");
      //     types.push_back("_beamactive");

      //     //----------

      //     // The trigger counts.
      //     for (std::vector<std::string>::const_iterator it = types.begin();
      //          it != types.end();
      //          ++it)
      //       {
      //         bool const gated = (*it == "_beamactive");
      //         brildaqStorage_.setUInt32(toolbox::toString("trigger_count%s_total", it->c_str()),
      //                                   sectionToStore.triggerCountTotal(gated));
      //         for (int trigType = tcds::definitions::kTrigTypeMin;
      //              trigType <= tcds::definitions::kTrigTypeMax;
      //              ++trigType)
      //           {
      //             brildaqStorage_.setUInt32(toolbox::toString("trigger_count%s_trigtype%d", it->c_str(), trigType),
      //                                       sectionToStore.triggerCount(trigType, gated));
      //           }
      //       }

      //     //----------

      //     // The missed-trigger counts.
      //     for (std::vector<std::string>::const_iterator it = types.begin();
      //          it != types.end();
      //          ++it)
      //       {
      //         bool const gated = (*it == "_beamactive");
      //         brildaqStorage_.setUInt32(toolbox::toString("suppressed_trigger_count%s_total", it->c_str()),
      //                                   sectionToStore.suppressedTriggerCountTotal(gated));
      //         for (int trigType = tcds::definitions::kTrigTypeMin;
      //              trigType <= tcds::definitions::kTrigTypeMax;
      //              ++trigType)
      //           {
      //             brildaqStorage_.setUInt32(toolbox::toString("suppressed_trigger_count%s_trigtype%d", it->c_str(), trigType),
      //                                       sectionToStore.suppressedTriggerCount(trigType, gated));
      //           }
      //       }

      //     //----------

      //     // The trigger rates.
      //     for (std::vector<std::string>::const_iterator it = types.begin();
      //          it != types.end();
      //          ++it)
      //       {
      //         bool const gated = (*it == "_beamactive");
      //         brildaqStorage_.setFloat(toolbox::toString("trigger_rate%s_total", it->c_str()),
      //                                  sectionToStore.triggerRateTotal(gated));
      //         for (int trigType = tcds::definitions::kTrigTypeMin;
      //              trigType <= tcds::definitions::kTrigTypeMax;
      //              ++trigType)
      //           {
      //             brildaqStorage_.setFloat(toolbox::toString("trigger_rate%s_trigtype%d", it->c_str(), trigType),
      //                                      sectionToStore.triggerRate(trigType, gated));
      //           }
      //       }

      //     //----------

      //     // The missed-trigger rates.
      //     for (std::vector<std::string>::const_iterator it = types.begin();
      //          it != types.end();
      //          ++it)
      //       {
      //         bool const gated = (*it == "_beamactive");
      //         brildaqStorage_.setFloat(toolbox::toString("suppressed_trigger_rate%s_total", it->c_str()),
      //                                  sectionToStore.suppressedTriggerRateTotal(gated));
      //         for (int trigType = tcds::definitions::kTrigTypeMin;
      //              trigType <= tcds::definitions::kTrigTypeMax;
      //              ++trigType)
      //           {
      //             brildaqStorage_.setFloat(toolbox::toString("suppressed_trigger_rate%s_trigtype%d", it->c_str(), trigType),
      //                                      sectionToStore.suppressedTriggerRate(trigType, gated));
      //           }
      //       }

      //     //----------

      //     // Deadtimes.
      //     for (std::vector<std::string>::const_iterator it = types.begin();
      //          it != types.end();
      //          ++it)
      //       {
      //         bool const gated = (*it == "_beamactive");
      //         brildaqStorage_.setFloat(toolbox::toString("deadtime%s_total", it->c_str()),
      //                                  sectionToStore.deadtimeFractionTotal(gated));
      //         brildaqStorage_.setFloat(toolbox::toString("deadtime%s_tts", it->c_str()),
      //                                  sectionToStore.deadtimeFractionFromTTSAllPartitions(gated));
      //         brildaqStorage_.setFloat(toolbox::toString("deadtime%s_trigger_rules", it->c_str()),
      //                                  sectionToStore.deadtimeFractionFromTriggerRules(gated));
      //         brildaqStorage_.setFloat(toolbox::toString("deadtime%s_bunch_mask_veto", it->c_str()),
      //                                  sectionToStore.deadtimeFractionFromBunchMaskVeto(gated));
      //         brildaqStorage_.setFloat(toolbox::toString("deadtime%s_retri", it->c_str()),
      //                                  sectionToStore.deadtimeFractionFromRetri(gated));
      //         brildaqStorage_.setFloat(toolbox::toString("deadtime%s_apve", it->c_str()),
      //                                  sectionToStore.deadtimeFractionFromAPVE(gated));
      //         brildaqStorage_.setFloat(toolbox::toString("deadtime%s_daq_backpressure", it->c_str()),
      //                                  sectionToStore.deadtimeFractionFromDAQBackpressure(gated));
      //         brildaqStorage_.setFloat(toolbox::toString("deadtime%s_calibration", it->c_str()),
      //                                  sectionToStore.deadtimeFractionFromCalibration(gated));
      //         brildaqStorage_.setFloat(toolbox::toString("deadtime%s_sw_pause", it->c_str()),
      //                                  sectionToStore.deadtimeFractionFromSWPause(gated));
      //         brildaqStorage_.setFloat(toolbox::toString("deadtime%s_fw_pause", it->c_str()),
      //                                  sectionToStore.deadtimeFractionFromFWPause(gated));
      //       }

      //     //----------

      //     // BUG BUG BUG
      //     // Legacy.
      //     // brildaqStorage_.update(sectionToStore);
      //     brildaqStorage_.writeInfoSpace();
      //     // BUG BUG BUG end

      //     //----------

      //     // Storage of the 'grandmaster' lumi section table.
      //     grandmasterStorage_.update(sectionToStore);
      //     grandmasterStorage_.writeInfoSpace();

      //     //----------

      //     // Storage of per-section BRILDAQ data.
      //     brildaqSectionStorage_.update(sectionToStore);
      //     brildaqSectionStorage_.writeInfoSpace();
      //   }

    }

  //----------

  // Wait a little in order not to completely hog the CPU.
  ::usleep(kLoopRelaxTime);

  // Return true in order to schedule the next iteration.
  return true;
}

tcds::cpm::BRILDAQSnapshot
tcds::cpm::BRILDAQLoop::snapshot() const
{
  // Lock to prevent being interrupted by updates.
  tcds::utils::LockGuard<tcds::utils::Lock> guardedLock(lock_);

  BRILDAQSnapshot snapshot(numNibblesMissed_,
                           numNibblesReadMulti_,
                           numNibblesInvalid_,
                           numNibblesCorrupted_,
                           numNibblesAlmostCorrupted_,
                           nibbles_,
                           sections_);

  return snapshot;
}
