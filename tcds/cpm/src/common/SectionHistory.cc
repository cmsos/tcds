#include "tcds/cpm/SectionHistory.h"

#include <iomanip>
#include <sstream>
#include <cstddef>

tcds::cpm::SectionHistory::SectionHistory(std::vector<tcds::cpm::LumiSection> const& dataIn) :
  sections_(dataIn)
{
}

tcds::cpm::SectionHistory::~SectionHistory()
{
}

std::string
tcds::cpm::SectionHistory::getJSONString() const
{
  std::stringstream res;

  res << "[";
  size_t index = 0;
  for (std::vector<tcds::cpm::LumiSection>::const_iterator it = sections_.begin();
       it != sections_.end();
       ++it)
    {
      res << "{";

      // The CMS run number.
      res << "\"RunNumber\": \"" << it->runNumber() << "\"";
      res << ", ";

      // The lumi section number.
      res << "\"SectionNumber\": \"" << it->lumiSectionNumber() << "\"";
      res << ", ";

      // The number of nibbles in this section (so far).
      res << "\"NumNibbles\": \"" << it->numNibbles() << "\"";
      res << ", ";

      // The total trigger rate.
      res << "\"TriggerRate\": \""
          << std::fixed << std::setprecision(2)
          << it->triggerRateTotal()
          << "\"";
      res << ", ";

      // The total suppressed-trigger rate.
      res << "\"SuppressedTriggerRate\": \""
          << std::fixed << std::setprecision(2)
          << it->suppressedTriggerRateTotal()
          << "\"";
      res << ", ";

      // The total deadtime.
      res << "\"Deadtime\": \""
          << std::fixed << std::setprecision(2)
          << it->deadtimeFractionTotal(false)
          << "\"";
      res << ", ";

      // The total BeamActive deadtime.
      res << "\"DeadtimeBeamActive\": \""
          << std::fixed << std::setprecision(2)
          << it->deadtimeFractionTotal(true)
          << "\"";

      res << "}";
      if (index != (sections_.size() - 1))
        {
          res << ", ";
        }
      ++index;
    }
  res << "]";

  return res.str();
}
