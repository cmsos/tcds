#include "tcds/cpm/HwStatusInfoSpaceUpdater.h"

#include <stdint.h>
#include <string>

#include "tcds/cpm/TCADeviceCPMT1.h"
#include "tcds/cpm/TCADeviceCPMT2.h"
#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/InfoSpaceItem.h"

tcds::cpm::HwStatusInfoSpaceUpdater::HwStatusInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                                              tcds::cpm::TCADeviceCPMT1 const& hwT1,
                                                              tcds::cpm::TCADeviceCPMT2 const& hwT2) :
  tcds::utils::HwInfoSpaceUpdaterBase(xdaqApp, hwT1),
  hwT1_(hwT1),
  hwT2_(hwT2)
{
}

bool
tcds::cpm::HwStatusInfoSpaceUpdater::updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                                         tcds::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  bool updated = false;
  tcds::cpm::TCADeviceCPMT1 const& hwT1 = getHwT1();
  tcds::cpm::TCADeviceCPMT2 const& hwT2 = getHwT2();
  if (hwT1.isHwConnected() && hwT2.isHwConnected())
    {
      std::string name = item.name();
      tcds::utils::InfoSpaceItem::UpdateType updateType = item.updateType();
      if (updateType == tcds::utils::InfoSpaceItem::PROCESS)
        {
          // The 'PROCESS' update type means that there is something
          // special to the variable. Figure out what to do based on the
          // variable name.
          if (name == "ttc_clock_up")
            {
              bool const newVal = hwT1.isTTCClockUp();
              infoSpaceHandler->setBool(name, newVal);
              updated = true;
            }
          else if (name == "ttc_clock_stable")
            {
              bool const newVal = hwT1.isTTCClockStable();
              infoSpaceHandler->setBool(name, newVal);
              updated = true;
            }
          else if (name == "ttc_clock_unlock_count")
            {
              uint32_t const newVal = hwT1.readTTCClockUnlockCounter();
              infoSpaceHandler->setUInt32(name, newVal);
              updated = true;
            }
          else if (name == "bst_ready")
            {
              bool const newVal = hwT1.isBSTReady();
              infoSpaceHandler->setBool(name, newVal);
              updated = true;
            }
          else if (name == "pm_state")
            {
              uint32_t newVal = hwT1.getPMState();
              infoSpaceHandler->setUInt32(name, newVal);
              updated = true;
            }
        }
      if (!updated)
        {
          // NOTE: This only works for T1 quantities.
          updated = tcds::utils::HwInfoSpaceUpdaterBase::updateInfoSpaceItem(item, infoSpaceHandler);
        }
    }

  if (updated)
    {
      item.setValid();
    }
  else
    {
      item.setInvalid();
    }

  return updated;
}

tcds::cpm::TCADeviceCPMT1 const&
tcds::cpm::HwStatusInfoSpaceUpdater::getHwT1() const
{
  return hwT1_;
}

tcds::cpm::TCADeviceCPMT2 const&
tcds::cpm::HwStatusInfoSpaceUpdater::getHwT2() const
{
  return hwT2_;
}
