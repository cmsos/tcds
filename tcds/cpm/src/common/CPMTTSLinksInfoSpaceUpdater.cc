#include "tcds/cpm/CPMTTSLinksInfoSpaceUpdater.h"

#include <sstream>
#include <cstddef>
#include <stdint.h>

#include "toolbox/string.h"

#include "tcds/cpm/TCADeviceCPMT1.h"
#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/InfoSpaceItem.h"

tcds::cpm::CPMTTSLinksInfoSpaceUpdater::CPMTTSLinksInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                                                    tcds::cpm::TCADeviceCPMT1 const& hwT1) :
  tcds::utils::HwInfoSpaceUpdaterBase(xdaqApp, hwT1),
  hwT1_(hwT1)
{
}

tcds::cpm::CPMTTSLinksInfoSpaceUpdater::~CPMTTSLinksInfoSpaceUpdater()
{
}

bool
tcds::cpm::CPMTTSLinksInfoSpaceUpdater::updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                                            tcds::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  tcds::cpm::TCADeviceCPMT1 const& hwT1 = getHwT1();
  bool updated = false;
  if (hwT1.isHwConnected())
    {
      std::string name = item.name();
      tcds::utils::InfoSpaceItem::UpdateType updateType = item.updateType();
      if (updateType == tcds::utils::InfoSpaceItem::PROCESS)
        {
          // The 'PROCESS' update type means that there is something
          // special to the variable. Figure out what to do based on the
          // variable name.
          if (toolbox::endsWith(name, ".link_up"))
            {
              unsigned int lpmNum = extractLPMNumber(name);
              bool const newVal = hwT1.isTTSLinkUp(lpmNum);
              infoSpaceHandler->setBool(name, newVal);
              updated = true;
            }
          else if (toolbox::endsWith(name, ".link_aligned"))
            {
              unsigned int lpmNum = extractLPMNumber(name);
              bool const newVal = hwT1.isTTSLinkAligned(lpmNum);
              infoSpaceHandler->setBool(name, newVal);
              updated = true;
            }
          else if (toolbox::endsWith(name, ".crc_error_count"))
            {
              unsigned int lpmNum = extractLPMNumber(name);
              uint32_t const newVal = hwT1.readTTSLinkCRCErrorCount(lpmNum);
              infoSpaceHandler->setUInt32(name, newVal);
              updated = true;
            }
          else if (toolbox::endsWith(name, ".sequence_error_count"))
            {
              unsigned int lpmNum = extractLPMNumber(name);
              uint32_t const newVal = hwT1.readTTSLinkSequenceErrorCount(lpmNum);
              infoSpaceHandler->setUInt32(name, newVal);
              updated = true;
            }
        }
      if (!updated)
        {
          // NOTE: This only works for T1 quantities.
          updated = tcds::utils::HwInfoSpaceUpdaterBase::updateInfoSpaceItem(item, infoSpaceHandler);
        }
    }

  if (updated)
    {
      item.setValid();
    }
  else
    {
      item.setInvalid();
    }

  return updated;
}

tcds::cpm::TCADeviceCPMT1 const&
tcds::cpm::CPMTTSLinksInfoSpaceUpdater::getHwT1() const
{
  return hwT1_;
}

unsigned int
tcds::cpm::CPMTTSLinksInfoSpaceUpdater::extractLPMNumber(std::string const& regName) const
{
  std::string const patLo = ".lpm";
  size_t const posLo = regName.find(patLo);
  std::string const patHi = ".";
  size_t const posHi = regName.find(patHi, posLo + patLo.size());
  std::stringstream tmp;
  tmp << regName.substr(posLo + patLo.size(), posHi);
  unsigned int lpmNum;
  tmp >> lpmNum;
  return lpmNum;
}
