#include "tcds/pm/TTSInfoSpaceUpdater.h"

#include <stdint.h>
#include <string>
#include <vector>

#include "toolbox/string.h"

#include "tcds/pm/TCADevicePMCommonBase.h"
#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Utils.h"

tcds::pm::TTSInfoSpaceUpdater::TTSInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                                   tcds::pm::TCADevicePMCommonBase const& hw) :
  tcds::utils::HwInfoSpaceUpdaterBase(xdaqApp, hw)
{
}

tcds::pm::TTSInfoSpaceUpdater::~TTSInfoSpaceUpdater()
{
}

bool
tcds::pm::TTSInfoSpaceUpdater::updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                                   tcds::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  tcds::pm::TCADevicePMCommonBase const& hw = getHw();
  bool updated = false;
  if (hw.isHwConnected())
    {
      std::string name = item.name();
      tcds::utils::InfoSpaceItem::UpdateType updateType = item.updateType();
      if (updateType == tcds::utils::InfoSpaceItem::PROCESS)
        {
          // The 'PROCESS' update type means that there is something
          // special to the variable. Figure out what to do based on the
          // variable name.
          if (name == "pm_state")
            {
              uint32_t newVal = hw.getPMState();
              infoSpaceHandler->setUInt32(name, newVal);
              updated = true;
            }
          else if (name == "bunch_mask_veto_pattern_size")
            {
              std::vector<uint16_t> tmp = hw.readBunchMaskVetoBXs();
              uint32_t const newVal = tmp.size();
              infoSpaceHandler->setUInt32(name, newVal);
              updated = true;
            }
          else if (name == "bunch_mask_veto_pattern")
            {
              std::vector<uint16_t> bxList = hw.readBunchMaskVetoBXs();
              std::vector<std::pair<uint16_t, uint16_t> > bxRanges =
                tcds::utils::groupBXListIntoRanges(bxList);
              std::string const newVal =  tcds::utils::formatBXRangeList(bxRanges);
              infoSpaceHandler->setString(name, newVal);
              updated = true;
            }
          else if (toolbox::endsWith(name, "_trigger_deadtime"))
            {
              // The way these are implemented in the firmware, the
              // intuitive value to show on screen is one BX higher
              // than the value stored in the firmware.
              // NOTE: Be careful: trigger rules set to 0 are switched
              // off!
              uint32_t const tmp = hw.readRegister(name);
              uint32_t newVal = tmp;
              if (newVal != 0)
                {
                  newVal += 1;
                }
              infoSpaceHandler->setUInt32(name, newVal);
              updated = true;
            }
        }
      if (!updated)
        {
          updated = tcds::utils::HwInfoSpaceUpdaterBase::updateInfoSpaceItem(item, infoSpaceHandler);
        }
    }

  if (updated)
    {
      item.setValid();
    }
  else
    {
      item.setInvalid();
    }

  return updated;
}

tcds::pm::TCADevicePMCommonBase const&
tcds::pm::TTSInfoSpaceUpdater::getHw() const
{
  return static_cast<tcds::pm::TCADevicePMCommonBase const&>(tcds::utils::HwInfoSpaceUpdaterBase::getHw());
}
