#include "tcds/pm/CountersInfoSpaceHandler.h"

#include <string>

#include "toolbox/string.h"

#include "tcds/pm/Definitions.h"
#include "tcds/utils/Definitions.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/Utils.h"
#include "tcds/utils/WebServer.h"

tcds::pm::CountersInfoSpaceHandler::CountersInfoSpaceHandler(xdaq::Application& xdaqApp,
                                                             tcds::utils::InfoSpaceUpdater* updater) :
  InfoSpaceHandler(xdaqApp, "tcds-pm-counters", updater)
{
  // Trigger counter.
  // NOTE: This is the counter that does _not_ reset upon EC0.
  createUInt32("main.total_event_counter");
  // NOTE: This is the counter that _does_ reset upon EC0.
  createUInt32("main.event_counter");

  // Orbit counter.
  createUInt32("main.orbit_counter");

  // B-go request counters.
  for (int bgoNum = 0; bgoNum <= tcds::definitions::kBgoNumMax; ++bgoNum)
    {
      createUInt32(toolbox::toString("bgo_channels.bgo_channel%d.request_counter", bgoNum));
    }

  // B-go cancellation counters (collisions).
  for (int bgoNum = 0; bgoNum <= tcds::definitions::kBgoNumMax; ++bgoNum)
    {
      createUInt32(toolbox::toString("bgo_channels.bgo_channel%d.cancel_counter_collision", bgoNum));
    }

  // B-go cancellation counters (busy).
  for (int bgoNum = 0; bgoNum <= tcds::definitions::kBgoNumMax; ++bgoNum)
    {
      createUInt32(toolbox::toString("bgo_channels.bgo_channel%d.cancel_counter_busy", bgoNum));
    }

  // Sequence request counters.
  for (int sequenceNum = tcds::definitions::kSequenceNumMin;
       sequenceNum <= tcds::definitions::kSequenceNumMax;
       ++sequenceNum)
    {
      createUInt32(toolbox::toString("bgo_trains.bgo_train%d.request_counter", sequenceNum));
    }

  // Sequence cancellation counters.
  for (int sequenceNum = tcds::definitions::kSequenceNumMin;
       sequenceNum <= tcds::definitions::kSequenceNumMax;
       ++sequenceNum)
    {
      createUInt32(toolbox::toString("bgo_trains.bgo_train%d.cancel_counter", sequenceNum));
    }

  // Action counters.
  for (unsigned int genNum = 0;
       genNum < tcds::definitions::kNumActionGensPerPM;
       ++genNum)
    {
      createUInt32(toolbox::toString("tts_actions.generator%d.status.request_count", genNum));
    }
}

tcds::pm::CountersInfoSpaceHandler::~CountersInfoSpaceHandler()
{
}

void
tcds::pm::CountersInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  // Trigger counter.
  monitor.newItemSet("itemset-trigger-counter");
  monitor.addItem("itemset-trigger-counter",
                  "main.total_event_counter",
                  "Total # L1As in the current run",
                  this,
                  "This counter resets upon OC0 and on explicit software request");
  monitor.addItem("itemset-trigger-counter",
                  "main.event_counter",
                  "# L1As since the last EC0",
                  this,
                  "This counter resets upon EC0 and on explicit software request");

  // Orbit counter.
  monitor.newItemSet("itemset-orbit-counter");
  monitor.addItem("itemset-orbit-counter",
                  "main.orbit_counter",
                  "Orbit counter",
                  this);

  // B-go request counters.
  monitor.newItemSet("itemset-bgo-counters");
  for (int bgoNum = 0; bgoNum <= tcds::definitions::kBgoNumMax; ++bgoNum)
    {
      monitor.addItem("itemset-bgo-counters",
                      toolbox::toString("bgo_channels.bgo_channel%d.request_counter", bgoNum),
                      tcds::utils::formatBgoNameString(static_cast<tcds::definitions::BGO_NUM>(bgoNum)),
                      this);
    }

  // B-go cancellation counters (collisions).
  monitor.newItemSet("itemset-bgo-cancel-counters-collision");
  for (int bgoNum = 0; bgoNum <= tcds::definitions::kBgoNumMax; ++bgoNum)
    {
      monitor.addItem("itemset-bgo-cancel-counters-collision",
                      toolbox::toString("bgo_channels.bgo_channel%d.cancel_counter_collision", bgoNum),
                      tcds::utils::formatBgoNameString(static_cast<tcds::definitions::BGO_NUM>(bgoNum)),
                      this);
    }

  // B-go cancellation counters (busy).
  monitor.newItemSet("itemset-bgo-cancel-counters-busy");
  for (int bgoNum = 0; bgoNum <= tcds::definitions::kBgoNumMax; ++bgoNum)
    {
      monitor.addItem("itemset-bgo-cancel-counters-busy",
                      toolbox::toString("bgo_channels.bgo_channel%d.cancel_counter_busy", bgoNum),
                      tcds::utils::formatBgoNameString(static_cast<tcds::definitions::BGO_NUM>(bgoNum)),
                      this);
    }

  // Sequence request counters.
  monitor.newItemSet("itemset-sequence-counters");
  for (int sequenceNum = tcds::definitions::kSequenceNumMin;
       sequenceNum <= tcds::definitions::kSequenceNumMax;
       ++sequenceNum)
    {
      monitor.addItem("itemset-sequence-counters",
                      toolbox::toString("bgo_trains.bgo_train%d.request_counter", sequenceNum),
                      tcds::utils::formatSequenceNameString(static_cast<tcds::definitions::SEQUENCE_NUM>(sequenceNum)),
                      this);
    }

  // Sequence cancellation counters.
  monitor.newItemSet("itemset-sequence-cancel-counters");
  for (int sequenceNum = tcds::definitions::kSequenceNumMin;
       sequenceNum <= tcds::definitions::kSequenceNumMax;
       ++sequenceNum)
    {
      monitor.addItem("itemset-sequence-cancel-counters",
                      toolbox::toString("bgo_trains.bgo_train%d.cancel_counter", sequenceNum),
                      tcds::utils::formatSequenceNameString(static_cast<tcds::definitions::SEQUENCE_NUM>(sequenceNum)),
                      this);
    }

  // Action counters.
  monitor.newItemSet("itemset-action-counters");
  for (unsigned int genNum = 0;
       genNum < tcds::definitions::kNumActionGensPerPM;
       ++genNum)
    {
      monitor.addItem("itemset-action-counters",
                      toolbox::toString("tts_actions.generator%d.status.request_count", genNum),
                      toolbox::toString("TTS-driven action %d", genNum),
                      this);
    }
}

void
tcds::pm::CountersInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                  tcds::utils::Monitor& monitor,
                                                                  std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "Counters" : forceTabName;

  webServer.registerTab(tabName,
                        "All kinds of counters",
                        6);

  webServer.registerTable("Trigger counters",
                          "",
                          monitor,
                          "itemset-trigger-counter",
                          tabName,
                          6);

  webServer.registerTable("Orbit counter",
                          "",
                          monitor,
                          "itemset-orbit-counter",
                          tabName,
                          6);

  webServer.registerTable("B-go request counters",
                          "The total number of requests, per B-go channel",
                          monitor,
                          "itemset-bgo-counters",
                          tabName);

  webServer.registerTable("B-go cancellation counters (collisions)",
                          "The number of requests cancelled due to BX collisions",
                          monitor,
                          "itemset-bgo-cancel-counters-collision",
                          tabName);

  webServer.registerTable("B-go cancellation counters (busy)",
                          "The number of requests cancelled due to scheduling conflicts",
                          monitor,
                          "itemset-bgo-cancel-counters-busy",
                          tabName);

  webServer.registerTable("Sequence request counters",
                          "The total number of requests, per sequence",
                          monitor,
                          "itemset-sequence-counters",
                          tabName);

  webServer.registerTable("Sequence cancellation counters",
                          "The number of cancelled requests",
                          monitor,
                          "itemset-sequence-cancel-counters",
                          tabName);

  webServer.registerTable("Action counters",
                          "The number of times a TTS-driven action fired, per action",
                          monitor,
                          "itemset-action-counters",
                          tabName);
}
