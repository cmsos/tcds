#include "tcds/pm/CountersInfoSpaceUpdater.h"

#include "tcds/hwlayertca/TCADeviceBase.h"
#include "tcds/utils/InfoSpaceItem.h"

tcds::pm::CountersInfoSpaceUpdater::CountersInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                                             tcds::hwlayertca::TCADeviceBase const& hw) :
  tcds::utils::HwInfoSpaceUpdaterBase(xdaqApp, hw),
  hw_(hw)
{
}

tcds::pm::CountersInfoSpaceUpdater::~CountersInfoSpaceUpdater()
{
}

bool
tcds::pm::CountersInfoSpaceUpdater::updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                                        tcds::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  tcds::hwlayertca::TCADeviceBase const& hw = getHw();
  bool updated = false;
  if (hw.isHwConnected())
    {
      if (!updated)
        {
          updated = tcds::utils::HwInfoSpaceUpdaterBase::updateInfoSpaceItem(item, infoSpaceHandler);
        }
    }

  if (updated)
    {
      item.setValid();
    }
  else
    {
      item.setInvalid();
    }

  return updated;
}

tcds::hwlayertca::TCADeviceBase const&
tcds::pm::CountersInfoSpaceUpdater::getHw() const
{
  return hw_;
}
