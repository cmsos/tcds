#include "tcds/pm/TTSCountersInfoSpaceHandler.h"

#include "toolbox/string.h"

#include "tcds/pm/Utils.h"
#include "tcds/pm/WebTableTTSCounters.h"
#include "tcds/utils/Definitions.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/Utils.h"
#include "tcds/utils/WebServer.h"
#include "tcds/utils/XDAQAppBase.h"

namespace tcds {
  namespace utils {
    class ConfigurationInfoSpaceHandler;
  }
}

tcds::pm::TTSCountersInfoSpaceHandler::TTSCountersInfoSpaceHandler(tcds::utils::XDAQAppBase& xdaqApp,
                                                                   tcds::utils::InfoSpaceUpdater* updater,
                                                                   bool const cpmMode) :
  InfoSpaceHandler(xdaqApp, "tcds-pm-tts-counters", updater),
  isCPMMode_(cpmMode)
{
  // The number of LPMs to include in the monitoring depends on
  // whether we're in LPM- or in CPM mode.
  numLPMs_ = 1;
  if (isCPMMode_)
    {
      numLPMs_ = tcds::definitions::kNumLPMsPerCPM;
    }

  //----------

  // There are two types of 'TTS counters':
  // - One for the number of transitions into each state.
  // - One for the time spent in each state.
  // NOTE: These 'time spent in TTS state' counters are a bit tricky.
  // For details, please see
  // tcds::pm::TCADevicePMCommonBase::readTTSTimeCounter().
  counterTypes_.push_back("transition");
  counterTypes_.push_back("time");

  //----------

  // This is the order of the columns in the counters tables, defined
  // in some reasonable order.
  counterColumns_.push_back("ready");
  counterColumns_.push_back("warning");
  counterColumns_.push_back("warning_due_to_daq_backpressure");
  counterColumns_.push_back("busy");
  counterColumns_.push_back("out_of_sync");
  counterColumns_.push_back("private_request1");
  counterColumns_.push_back("private_request2");
  counterColumns_.push_back("private_request3");
  counterColumns_.push_back("error");
  counterColumns_.push_back("disconnected");
  counterColumns_.push_back("invalid_tts");
  counterColumns_.push_back("invalid_tcds");
  counterColumns_.push_back("link_lost_at_pi");
  counterColumns_.push_back("link_lost_at_lpm");
  counterColumns_.push_back("link_lost_at_cpm");

  // These define the column labels.
  columnLabels_["ready"] =
    tcds::utils::TTSStateToString(tcds::definitions::TTS_STATE_READY);
  columnLabels_["warning"] =
    tcds::utils::TTSStateToString(tcds::definitions::TTS_STATE_WARNING);
  columnLabels_["warning_due_to_daq_backpressure"] =
    tcds::utils::TTSStateToString(tcds::definitions::TTS_STATE_WARNING_DUE_TO_DAQ);
  columnLabels_["busy"] =
    tcds::utils::TTSStateToString(tcds::definitions::TTS_STATE_BUSY);
  columnLabels_["out_of_sync"] =
    tcds::utils::TTSStateToString(tcds::definitions::TTS_STATE_OOS);
  columnLabels_["private_request1"] =
    tcds::utils::TTSStateToString(tcds::definitions::TTS_STATE_PRIV_REQ_1);
  columnLabels_["private_request2"] =
    tcds::utils::TTSStateToString(tcds::definitions::TTS_STATE_PRIV_REQ_2);
  columnLabels_["private_request3"] =
    tcds::utils::TTSStateToString(tcds::definitions::TTS_STATE_PRIV_REQ_3);
  columnLabels_["error"] =
    tcds::utils::TTSStateToString(tcds::definitions::TTS_STATE_ERROR);
  columnLabels_["disconnected"] =
    tcds::utils::TTSStateToString(tcds::definitions::TTS_STATE_DISCONNECTED_0);
  columnLabels_["invalid_tts"] =
    tcds::utils::TTSStateToString(tcds::definitions::TTS_STATE_INVALID_3);
  columnLabels_["invalid_tcds"] =
    tcds::utils::TTSStateToString(tcds::definitions::TTS_STATE_INVALID_TCDS);
  columnLabels_["link_lost_at_pi"] =
    tcds::utils::TTSStateToString(tcds::definitions::TTS_STATE_LOST_INTO_PI);
  columnLabels_["link_lost_at_lpm"] =
    tcds::utils::TTSStateToString(tcds::definitions::TTS_STATE_LOST_INTO_LPM);
  columnLabels_["link_lost_at_cpm"] =
    tcds::utils::TTSStateToString(tcds::definitions::TTS_STATE_LOST_INTO_CPM);

  // These are all the deadtime sources known to the PM.
  deadtimeSources_ = tcds::pm::deadtimeSourceList();

  //----------

  std::string name;

  // All TTS sources in the PM.
  for (std::vector<std::string>::const_iterator counterType = counterTypes_.begin();
       counterType != counterTypes_.end();
       ++counterType)
    {
      for (std::vector<tcds::definitions::DEADTIME_SOURCE>::const_iterator src = deadtimeSources_.begin();
           src != deadtimeSources_.end();
           ++src)
        {
          for (std::vector<std::string>::const_iterator iter = counterColumns_.begin();
               iter != counterColumns_.end();
               ++iter)
            {
              std::string const srcStr = tcds::pm::deadtimeSourceToRegName(*src);
              std::string const suffix = *iter;
              name = toolbox::toString("tts_%s_counters.%s.%s",
                                       counterType->c_str(),
                                       srcStr.c_str(),
                                       suffix.c_str());
              if (*counterType == "time")
                {
                  createUInt64(name,
                               0,
                               "",
                               tcds::utils::InfoSpaceItem::PROCESS);
                }
              else
                {
                  createUInt32(name,
                               0,
                               "",
                               tcds::utils::InfoSpaceItem::HW32);
                }
            }
        }
    }

  // All individual iCI/iAPVE TTS state counters, listed per LPM.
  for (std::vector<std::string>::const_iterator counterType = counterTypes_.begin();
       counterType != counterTypes_.end();
       ++counterType)
    {
      for (unsigned int lpmNum = 1; lpmNum <= numLPMs_; ++lpmNum)
        {
          for (unsigned int iciNum = 1; iciNum <= tcds::definitions::kNumICIsPerLPM; ++iciNum)
            {
              for (std::vector<std::string>::const_iterator iter = counterColumns_.begin();
                   iter != counterColumns_.end();
                   ++iter)
                {
                  std::string const suffix = *iter;
                  name = toolbox::toString("tts_%s_counters.lpm%d.ici%d.%s",
                                           counterType->c_str(), lpmNum, iciNum, suffix.c_str());
                  if (*counterType == "time")
                    {
                      createUInt64(name,
                                   0,
                                   "",
                                   tcds::utils::InfoSpaceItem::PROCESS);
                    }
                  else
                    {
                      createUInt32(name,
                                   0,
                                   "",
                                   tcds::utils::InfoSpaceItem::HW32);
                    }
                }
            }
          for (unsigned int apveNum = 1; apveNum <= tcds::definitions::kNumAPVEsPerLPM; ++apveNum)
            {
              for (std::vector<std::string>::const_iterator iter = counterColumns_.begin();
                   iter != counterColumns_.end();
                   ++iter)
                {
                  std::string const suffix = *iter;
                  name = toolbox::toString("tts_%s_counters.lpm%d.apve%d.%s",
                                           counterType->c_str(), lpmNum, apveNum, suffix.c_str());
                  if (*counterType == "time")
                    {
                      createUInt64(name,
                                   0,
                                   "",
                                   tcds::utils::InfoSpaceItem::PROCESS);
                    }
                  else
                    {
                      createUInt32(name,
                                   0,
                                   "",
                                   tcds::utils::InfoSpaceItem::HW32);
                    }
                }
            }
        }
    }
}

tcds::pm::TTSCountersInfoSpaceHandler::~TTSCountersInfoSpaceHandler()
{
}

tcds::utils::XDAQAppBase&
tcds::pm::TTSCountersInfoSpaceHandler::getOwnerApplication() const
{
  return static_cast<tcds::utils::XDAQAppBase&>(tcds::utils::InfoSpaceHandler::getOwnerApplication());
}

void
tcds::pm::TTSCountersInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  // All TTS sources in the PM.
  for (std::vector<std::string>::const_iterator counterType = counterTypes_.begin();
       counterType != counterTypes_.end();
       ++counterType)
    {
      std::string const itemSetName = toolbox::toString("itemset-tts-%s-counters-pm",
                                                        counterType->c_str());
      monitor.newItemSet(itemSetName);
      for (std::vector<tcds::definitions::DEADTIME_SOURCE>::const_iterator src = deadtimeSources_.begin();
           src != deadtimeSources_.end();
           ++src)
        {
          for (std::vector<std::string>::const_iterator iter = counterColumns_.begin();
               iter != counterColumns_.end();
               ++iter)
            {
              std::string const srcStr = tcds::pm::deadtimeSourceToRegName(*src);
              std::string const srcTitle = tcds::pm::deadtimeSourceToTitle(*src, true);
              std::string const suffix = *iter;
              std::string const label = columnLabels_[suffix];
              std::string const itemName =
                toolbox::toString("tts_%s_counters.%s.%s",
                                  counterType->c_str(),
                                  srcStr.c_str(),
                                  suffix.c_str());
              // As do these descriptions.
              std::string itemDesc =
                toolbox::toString("%s;%s",
                                  srcTitle.c_str(),
                                  label.c_str());
              monitor.addItem(itemSetName, itemName, itemDesc, this);
            }
        }
    }

  //----------

  // All individual iCI/iAPVE TTS state counters, listed per LPM.
  tcds::utils::ConfigurationInfoSpaceHandler const& cfgInfoSpace =
    getOwnerApplication().getConfigurationInfoSpaceHandler();
  for (std::vector<std::string>::const_iterator counterType = counterTypes_.begin();
       counterType != counterTypes_.end();
       ++counterType)
    {
      for (unsigned int lpmNum = 1; lpmNum <= numLPMs_; ++lpmNum)
        {
          std::string itemSetName;
          if (isCPMMode_)
            {
              itemSetName = toolbox::toString("itemset-tts-%s-counters-lpm%d",
                                              counterType->c_str(), lpmNum);
            }
          else
            {
              itemSetName = toolbox::toString("itemset-tts-%s-counters",
                                              counterType->c_str());
            }
          monitor.newItemSet(itemSetName);
          for (std::vector<std::string>::const_iterator iter = counterColumns_.begin();
               iter != counterColumns_.end();
               ++iter)
            {
              std::string const suffix = *iter;
              std::string const label = columnLabels_[suffix];
              for (unsigned int iciNum = 1; iciNum <= tcds::definitions::kNumICIsPerLPM; ++iciNum)
                {
                  std::string const itemName =
                    toolbox::toString("tts_%s_counters.lpm%d.ici%d.%s",
                                      counterType->c_str(), lpmNum, iciNum, suffix.c_str());
                  std::string itemDesc =
                    toolbox::toString("%s;%s",
                                      tcds::pm::formatICILabel(isCPMMode_, lpmNum, iciNum, cfgInfoSpace, false).c_str(),
                                      label.c_str());
                  monitor.addItem(itemSetName, itemName, itemDesc, this);
                }
              for (unsigned int apveNum = 1; apveNum <= tcds::definitions::kNumAPVEsPerLPM; ++apveNum)
                {
                  std::string const itemName =
                    toolbox::toString("tts_%s_counters.lpm%d.apve%d.%s",
                                      counterType->c_str(), lpmNum, apveNum, suffix.c_str());
                  std::string itemDesc =
                    toolbox::toString("%s;%s",
                                      tcds::pm::formatAPVELabel(isCPMMode_, lpmNum, apveNum, cfgInfoSpace, false).c_str(),
                                      label.c_str());
                  monitor.addItem(itemSetName, itemName, itemDesc, this);
                }
            }
        }
    }
}

void
tcds::pm::TTSCountersInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                     tcds::utils::Monitor& monitor,
                                                                     std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "TTS counters" : forceTabName;

  webServer.registerTab(tabName,
                        "Counters showing"
                        " 1) how many times each partition entered each TTS state"
                        ", and"
                        " 2) how much time (in BX) each partition spent in each TTS state.",
                        1);

  //----------

  // All TTS sources in the PM.
  std::string const tableName = "PM-level TTS (and TTS-like) sources";
  for (std::vector<std::string>::const_iterator counterType = counterTypes_.begin();
       counterType != counterTypes_.end();
       ++counterType)
    {
      std::string tableDesc = *counterType;
      if (*counterType == "transition")
        {
          tableDesc = "'Transition into' counters";
        }
      else if (*counterType == "time")
        {
          tableDesc = "'Time spent in' counters";
        }
      std::string const itemSetName = toolbox::toString("itemset-tts-%s-counters-pm",
                                                        counterType->c_str());
      webServer.registerWebObject<tcds::pm::WebTableTTSCounters>(tableName,
                                                                 tableDesc,
                                                                 monitor,
                                                                 itemSetName,
                                                                 tabName);
    }

  //----------

  // Add a little bit of breathing room before the tables for the
  // different LPMs.
  webServer.registerSpacer("dummy", "", monitor, "", tabName);

  //----------

  // All individual iCI/iAPVE TTS state counters, listed per LPM.
  tcds::utils::ConfigurationInfoSpaceHandler const& cfgInfoSpace =
    getOwnerApplication().getConfigurationInfoSpaceHandler();
  for (unsigned int lpmNum = 1; lpmNum <= numLPMs_; ++lpmNum)
    {
      for (std::vector<std::string>::const_iterator counterType = counterTypes_.begin();
           counterType != counterTypes_.end();
           ++counterType)
        {
          std::string tableLabel;
          std::string tableName;
          std::string itemSetName;
          if (isCPMMode_)
            {
              tableLabel = tcds::utils::formatLPMLabel(lpmNum, cfgInfoSpace);
              tableName = toolbox::toString("%s TTS sources", tableLabel.c_str());
              itemSetName = toolbox::toString("itemset-tts-%s-counters-lpm%d",
                                              counterType->c_str(), lpmNum);
            }
          else
            {
              tableName = "Partitions connected to this LPM";
              itemSetName = toolbox::toString("itemset-tts-%s-counters",
                                              counterType->c_str());
            }
          std::string tableDesc = *counterType;
          if (*counterType == "transition")
            {
              tableDesc = "'Transition into' counters";
            }
          else if (*counterType == "time")
            {
              tableDesc = "'Time spent in' counters";
            }
          webServer.registerWebObject<tcds::pm::WebTableTTSCounters>(tableName,
                                                                     tableDesc,
                                                                     monitor,
                                                                     itemSetName,
                                                                     tabName);
        }

      // Add a little bit of breathing room between the tables for the
      // different LPMs.
      if (lpmNum != numLPMs_)
        {
          webServer.registerSpacer("dummy", "", monitor, "", tabName);
        }
    }
}
