#include "tcds/pm/DAQInfoSpaceUpdater.h"

#include <stdint.h>
#include <string>

#include "toolbox/string.h"

#include "tcds/pm/TCADevicePMCommonBase.h"
#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/InfoSpaceItem.h"

tcds::pm::DAQInfoSpaceUpdater::DAQInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                                   tcds::pm::TCADevicePMCommonBase const& hw) :
  tcds::utils::HwInfoSpaceUpdaterBase(xdaqApp, hw),
  hw_(hw)
{
}

tcds::pm::DAQInfoSpaceUpdater::~DAQInfoSpaceUpdater()
{
}

bool
tcds::pm::DAQInfoSpaceUpdater::updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                                   tcds::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  tcds::pm::TCADevicePMCommonBase const& hw = getHw();
  bool updated = false;
  if (hw.isReadyForUse())
    {
      std::string name = item.name();
      tcds::utils::InfoSpaceItem::UpdateType updateType = item.updateType();
      if (updateType == tcds::utils::InfoSpaceItem::HW64)
        {
          // NOTE: This is a wee bit tricky, since IPbus does not
          // natively support 64-bit (single-register) transfers. This
          // implementation relies on the 'x_lo' -> 'x_hi' naming
          // convention.
          if (toolbox::endsWith(name, "_lo"))
            {
              std::string const regNameLo = name;
              std::string const regNameHi = regNameLo.substr(0, regNameLo.size() - 3) + "_hi";
              uint32_t const valLo = hw.readRegister(regNameLo);
              uint32_t const valHi = hw.readRegister(regNameHi);
              uint64_t const tmpLo = valLo;
              uint64_t const tmpHi = valHi;
              uint64_t const newVal = (tmpHi << 32) + tmpLo;
              infoSpaceHandler->setUInt64(name, newVal);
              updated = true;
            }
        }
      else if (updateType == tcds::utils::InfoSpaceItem::PROCESS)
        {
          if (toolbox::endsWith(name, ".derandomiser_watermark"))
            {
              double const newVal = hw.readDAQBufferFillLevel();
              infoSpaceHandler->setDouble(name, newVal);
              updated = true;
            }
          else if (toolbox::endsWith(name, ".fifo_watermark"))
            {
              double const newVal = hw.readDAQFifoFillLevel();
              infoSpaceHandler->setDouble(name, newVal);
              updated = true;
            }
        }
      if (!updated)
        {
          updated = tcds::utils::HwInfoSpaceUpdaterBase::updateInfoSpaceItem(item, infoSpaceHandler);
        }
    }

  if (updated)
    {
      item.setValid();
    }
  else
    {
      item.setInvalid();
    }

  return updated;
}

tcds::pm::TCADevicePMCommonBase const&
tcds::pm::DAQInfoSpaceUpdater::getHw() const
{
  return hw_;
}
