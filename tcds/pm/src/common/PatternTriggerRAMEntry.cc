#include "tcds/pm/PatternTriggerRAMEntry.h"

tcds::pm::PatternTriggerRAMEntry::PatternTriggerRAMEntry(uint32_t const rawData) :
  rawData_(rawData)
{
}

uint32_t
tcds::pm::PatternTriggerRAMEntry::rawData() const
{
  return rawData_;
}

// bool
// tcds::pm::PatternTriggerRAMEntry::isEndOfSequenceLoop() const
// {
//   return (entryVal() == endOfSequenceLoop_);
// }

// bool
// tcds::pm::PatternTriggerRAMEntry::isEndOfSequenceStop() const
// {
//   return (entryVal() == endOfSequenceStop_);
// }

bool
tcds::pm::PatternTriggerRAMEntry::isEndOfSequence() const
{
  return (entryVal() == endOfSequence_);
  // return (isEndOfSequenceLoop() || isEndOfSequenceStop());
}

uint32_t
tcds::pm::PatternTriggerRAMEntry::entryVal() const
{
  return rawData_;
}

uint32_t
tcds::pm::PatternTriggerRAMEntry::delay() const
{
  return entryVal();
}
