#include "tcds/pm/SequencesInfoSpaceHandler.h"

#include <string>

#include "toolbox/string.h"

#include "tcds/pm/WebTableSequenceRAM.h"
#include "tcds/utils/Definitions.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/Utils.h"
#include "tcds/utils/WebServer.h"

tcds::pm::SequencesInfoSpaceHandler::SequencesInfoSpaceHandler(xdaq::Application& xdaqApp,
                                                               tcds::utils::InfoSpaceUpdater* updater) :
  InfoSpaceHandler(xdaqApp, "tcds-cpmcontroller-sequences", updater)
{
  for (int sequenceNum = tcds::definitions::kSequenceNumMin;
       sequenceNum <= tcds::definitions::kSequenceNumMax;
       ++sequenceNum)
    {
      // Sequence-respects-TTS flag.
      createBool(toolbox::toString("bgo_trains.bgo_train%d.configuration.bgo_train_respects_tts", sequenceNum), 0, "true/false");

      // RAM contents.
      createString(toolbox::toString("bgo_trains.bgo_train%d.ram", sequenceNum));

      // Request counts.
      createUInt32(toolbox::toString("bgo_trains.bgo_train%d.request_counter", sequenceNum));

      // Cancel counts.
      createUInt32(toolbox::toString("bgo_trains.bgo_train%d.cancel_counter", sequenceNum));
    }
}

tcds::pm::SequencesInfoSpaceHandler::~SequencesInfoSpaceHandler()
{
}

void
tcds::pm::SequencesInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  for (int sequenceNum = tcds::definitions::kSequenceNumMin;
       sequenceNum <= tcds::definitions::kSequenceNumMax;
       ++sequenceNum)
    {
      // Sequence configuration.
      std::string itemSetName = toolbox::toString("itemset-sequence%d-cfg", sequenceNum);
      monitor.newItemSet(itemSetName);
      monitor.addItem(itemSetName,
                      toolbox::toString("bgo_trains.bgo_train%d.configuration.bgo_train_respects_tts", sequenceNum),
                      "Sequence respects TTS",
                      this);

      // Sequence RAM contents.
      itemSetName = toolbox::toString("itemset-sequence%d-ram", sequenceNum);
      monitor.newItemSet(itemSetName);
      monitor.addItem(itemSetName,
                      toolbox::toString("bgo_trains.bgo_train%d.ram", sequenceNum),
                      "RAM contents",
                      this);

      // Request and cancel counts.
      itemSetName = toolbox::toString("itemset-sequence%d-status", sequenceNum);
      monitor.newItemSet(itemSetName);
      monitor.addItem(itemSetName,
                      toolbox::toString("bgo_trains.bgo_train%d.request_counter", sequenceNum),
                      "Request count",
                      this);
      monitor.addItem(itemSetName,
                      toolbox::toString("bgo_trains.bgo_train%d.cancel_counter", sequenceNum),
                      "Cancel count",
                      this);
    }
}

void
tcds::pm::SequencesInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                   tcds::utils::Monitor& monitor,
                                                                   std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "Sequences" : forceTabName;

  webServer.registerTab(tabName,
                        "Information on firmware-based sequences",
                        3);

  for (int sequenceNum = tcds::definitions::kSequenceNumMin;
       sequenceNum <= tcds::definitions::kSequenceNumMax;
       ++sequenceNum)
    {
      std::string const sequenceNameString =
        tcds::utils::formatSequenceNameString(static_cast<tcds::definitions::SEQUENCE_NUM>(sequenceNum));
      std::string name = toolbox::toString("%s configuration", sequenceNameString.c_str());
      webServer.registerTable(name,
                              "",
                              monitor,
                              toolbox::toString("itemset-sequence%d-cfg", sequenceNum),
                              tabName);
      name = toolbox::toString("%s RAM contents", sequenceNameString.c_str());
      webServer.registerWebObject<WebTableSequenceRAM>(name,
                                                       "",
                                                       monitor,
                                                       toolbox::toString("itemset-sequence%d-ram", sequenceNum),
                                                       tabName);
      name = toolbox::toString("%s status", sequenceNameString.c_str());
      webServer.registerTable(name,
                              "",
                              monitor,
                              toolbox::toString("itemset-sequence%d-status", sequenceNum),
                              tabName);
    }
}

std::string
tcds::pm::SequencesInfoSpaceHandler::formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const
{
  std::string res = tcds::utils::escapeAsJSONString(tcds::utils::InfoSpaceHandler::kInvalidItemString);
  if (item->isValid())
    {
      std::string name = item->name();
      if (toolbox::endsWith(name, ".ram"))
        {
          // Special in the sense that this is something that needs to
          // be interpreted as a proper JavaScript object, so let's
          // not add any more double quotes.
          res = getString(name);
        }
      else
        {
          // For everything else simply call the generic formatter.
          res = InfoSpaceHandler::formatItem(item);
        }
    }
  return res;
}
