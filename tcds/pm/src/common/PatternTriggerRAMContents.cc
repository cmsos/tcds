#include "tcds/pm/PatternTriggerRAMContents.h"

#include <sstream>
#include <cstddef>

tcds::pm::PatternTriggerRAMContents::PatternTriggerRAMContents(std::vector<uint32_t> const dataIn) :
  rawData_(dataIn)
{
  size_t numEntries = dataIn.size();
  entries_.reserve(numEntries);
  entries_.clear();
  for (std::vector<uint32_t>::const_iterator it = dataIn.begin();
       it < dataIn.end();
       ++it)
    {
      entries_.push_back(PatternTriggerRAMEntry(*it));
    }
}

std::string
tcds::pm::PatternTriggerRAMContents::getJSONString() const
{
  std::stringstream res;

  res << "[";
  size_t index = 0;
  for (std::vector<PatternTriggerRAMEntry>::const_iterator it = entries_.begin();
       it != entries_.end();
       ++it)
    {
      res << "{";

      // The entry number.
      res << "\"Entry\": \"" << std::dec << index << "\"";
      res << ", ";

      std::string commandStr = "Unknown";
      if (!it->isEndOfSequence())
        {
          // The delay and trigger info for this entry.
          std::stringstream tmp;
          tmp << "Wait " << it->delay() << " BX and fire L1A";
          commandStr = tmp.str();
        }
      else
        {
          // An end-of-sequence note, if applicable.
          // NOTE: Explicitly mark the end-of-sequence if it is the
          // first (and therefore only) entry.
          if (it == entries_.begin())
            {
              commandStr = "Empty pattern";
            }
          else
            {
              // if (it->isEndOfSequenceLoop())
              //   {
              //     commandStr = "Start over from first entry";
              //   }
              // else if (it->isEndOfSequenceStop())
              //   {
                  commandStr = "End of pattern";
              //   }
              // else
              //   {
              //     commandStr = "End-of-sequence";
              //   }
            }
        }
      res << "\"Command\": \"" << commandStr << "\"";

      res << "}";

      if (it->isEndOfSequence())
        {
          // Pattern ends here.
          break;
        }

      if (it != (entries_.end() - 1))
        {
          res << ", ";
        }
      ++index;
    }
  res << "]";

  return res.str();
}
