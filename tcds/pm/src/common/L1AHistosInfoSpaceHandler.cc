#include "tcds/pm/L1AHistosInfoSpaceHandler.h"

#include "toolbox/string.h"

#include "tcds/pm/WebTableL1AHisto.h"
#include "tcds/utils/Definitions.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/Utils.h"
#include "tcds/utils/WebServer.h"
#include "tcds/utils/XDAQAppBase.h"

tcds::pm::L1AHistosInfoSpaceHandler::L1AHistosInfoSpaceHandler(tcds::utils::XDAQAppBase& xdaqApp,
                                                               tcds::utils::InfoSpaceUpdater* updater) :
  InfoSpaceHandler(xdaqApp, "tcds-pm-l1a-histos", updater)
{
  // The histogram(s) data.
  for (int trigType = tcds::definitions::kTrigTypeMin;
       trigType <= tcds::definitions::kTrigTypeMax;
       ++trigType)
    {
      createString(toolbox::toString("l1a_histos.trigger_type%d", trigType),
                   "",
                   "",
                   tcds::utils::InfoSpaceItem::PROCESS);
    }
}

tcds::pm::L1AHistosInfoSpaceHandler::~L1AHistosInfoSpaceHandler()
{
}

void
tcds::pm::L1AHistosInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  // The histogram(s) data.
  std::string const itemSetName = "itemset-l1a-histos";
  monitor.newItemSet(itemSetName);
  for (int trigType = tcds::definitions::kTrigTypeMin;
       trigType <= tcds::definitions::kTrigTypeMax;
       ++trigType)
    {
      monitor.addItem(itemSetName,
                      toolbox::toString("l1a_histos.trigger_type%d", trigType),
                      tcds::utils::TriggerTypeToString(tcds::definitions::TRIG_TYPE(trigType)),
                      this);
    }
}

void
tcds::pm::L1AHistosInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                   tcds::utils::Monitor& monitor,
                                                                   std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "L1A histos" : forceTabName;

  webServer.registerTab(tabName,
                        "Distributions of L1As across bunch crossings."
                        " Histograms are updated once per lumi nibble,"
                        " accumulated over a single lumi section,"
                        " and reset at each lumi section boundary."
                        " Drag to zoom, alt-drag to pan, click to reset zoom.",
                        1);

  std::string const itemSetName = "itemset-l1a-histos";
  std::string const tableName = "L1A histos";
  webServer.registerWebObject<WebTableL1AHisto>(tableName,
                                                "",
                                                monitor,
                                                itemSetName,
                                                tabName);
}

std::string
tcds::pm::L1AHistosInfoSpaceHandler::formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const
{
  std::string res = tcds::utils::escapeAsJSONString(tcds::utils::InfoSpaceHandler::kInvalidItemString);
  if (item->isValid())
    {
      std::string name = item->name();
      if (toolbox::startsWith(name, "l1a_histos"))
        {
          // Special in the sense that this is something that needs to
          // be interpreted as a proper JavaScript object, so let's
          // not add any more double quotes.
          res = getString(name);
        }
      else
        {
          // For everything else simply call the generic formatter.
          res = InfoSpaceHandler::formatItem(item);
        }
    }
  return res;
}
