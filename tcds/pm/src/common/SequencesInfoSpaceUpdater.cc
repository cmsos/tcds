#include "tcds/pm/SequencesInfoSpaceUpdater.h"

#include <stdint.h>
#include <string>

#include "toolbox/string.h"

#include "tcds/hwlayertca/TCADeviceBase.h"
#include "tcds/pm/SequenceRAMContents.h"
#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/InfoSpaceItem.h"

tcds::pm::SequencesInfoSpaceUpdater::SequencesInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                                               tcds::hwlayertca::TCADeviceBase const& hw) :
  tcds::utils::HwInfoSpaceUpdaterBase(xdaqApp, hw),
  hw_(hw)
{
}

tcds::pm::SequencesInfoSpaceUpdater::~SequencesInfoSpaceUpdater()
{
}

bool
tcds::pm::SequencesInfoSpaceUpdater::updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                                         tcds::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  tcds::hwlayertca::TCADeviceBase const& hw = getHw();
  bool updated = false;
  if (hw.isReadyForUse())
    {
      std::string const name = item.name();
      tcds::utils::InfoSpaceItem::UpdateType updateType = item.updateType();
      if (updateType == tcds::utils::InfoSpaceItem::PROCESS)
        {
          // The 'PROCESS' update type means that there is something
          // special to the variable. Figure out what to do based on the
          // variable name.
          if (toolbox::endsWith(name, ".ram"))
            {
              std::vector<uint32_t> const ramContentsRaw = hw.readBlock(name);
              SequenceRAMContents const ramContents(ramContentsRaw);
              std::string const newVal = ramContents.getJSONString();
              infoSpaceHandler->setString(name, newVal);
              updated = true;
            }
        }
      if (!updated)
        {
          updated = tcds::utils::HwInfoSpaceUpdaterBase::updateInfoSpaceItem(item, infoSpaceHandler);
        }
    }

  if (updated)
    {
      item.setValid();
    }
  else
    {
      item.setInvalid();
    }

  return updated;
}

tcds::hwlayertca::TCADeviceBase const&
tcds::pm::SequencesInfoSpaceUpdater::getHw() const
{
  return hw_;
}
