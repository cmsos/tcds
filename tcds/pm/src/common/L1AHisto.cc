#include "tcds/pm/L1AHisto.h"

#include "tcds/utils/Definitions.h"

#include <sstream>

tcds::pm::L1AHisto::L1AHisto(std::vector<uint32_t> const dataIn) :
  rawData_(dataIn)
{
}

uint32_t
tcds::pm::L1AHisto::lumiSectionNumber() const
{
  return rawData_.at(tcds::pm::L1AHisto::kIndexLumiSectionNumber);
}

uint32_t
tcds::pm::L1AHisto::lumiNibbleNumber() const
{
  return rawData_.at(tcds::pm::L1AHisto::kIndexLumiNibbleNumberHeader);
}

bool
tcds::pm::L1AHisto::isCorrupted() const
{
  // The lumi-nibble number is repeated in the header and footer. If
  // these two values are different, the data was updated in the
  // buffer while we were reading it.
  uint32_t const lumiNibbleNumberHeader = rawData_.at(kIndexLumiNibbleNumberHeader);
  uint32_t const lumiNibbleNumberFooter = rawData_.at(kIndexLumiNibbleNumberFooter);
  return (lumiNibbleNumberFooter != lumiNibbleNumberHeader);
}

std::string
tcds::pm::L1AHisto::getJSONString() const
{
  std::stringstream res;

  res << "[";

  size_t const lo = tcds::pm::L1AHisto::kIndexHistoData;
  size_t const hi = lo + definitions::kNumBXPerOrbit - 1;
  for (size_t ind = lo; ind <= hi; ++ind)
    {
      res << rawData_.at(ind);

      if (ind != hi)
        {
          res << ", ";
        }
    }
  res << "]";

  return res.str();
}
