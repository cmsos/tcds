#include "tcds/pm/WebTableTTSActionTriggers.h"

#include <algorithm>
#include <map>
#include <sstream>
#include <string>
#include <vector>

#include "toolbox/string.h"

#include "tcds/utils/Monitor.h"

tcds::pm::WebTableTTSActionTriggers::WebTableTTSActionTriggers(std::string const& name,
                                                               std::string const& description,
                                                               tcds::utils::Monitor const& monitor,
                                                               std::string const& itemSetName,
                                                               std::string const& tabName,
                                                               size_t const colSpan) :
  tcds::utils::WebObject(name, description, monitor, itemSetName, tabName, colSpan)
{
}

std::string
tcds::pm::WebTableTTSActionTriggers::getHTMLString() const
{
  // NOTE: This has been written to work with the new XDAQ12-style
  // HyperDAQ tabs and doT.js.

  std::stringstream res;

  res << "<div class=\"tcds-item-table-wrapper\">"
      << "\n";

  res << "<p class=\"tcds-item-table-title\">"
      << getName()
      << "</p>";
  res << "\n";

  res << "<p class=\"tcds-item-table-description\">"
      << getDescription()
      << "</p>";
  res << "\n";

  // And now produce actual table contents.
  tcds::utils::Monitor::StringPairVector items = monitor_.getFormattedItemSet(itemSetName_);

  // Step 1: determine the row labels (i.e., the LPMs) and group items
  // (i.e., the ICIs and APVEs in a single LPM) by row label.
  std::string const separator = ";";
  std::map<std::string, std::vector<std::string> > itemsByRow;
  std::vector<std::string> rowLabels;

  tcds::utils::Monitor::StringPairVector::const_iterator iter;
  for (iter = items.begin(); iter != items.end(); ++iter)
    {
      std::string const itemTitle = iter->first;
      std::string const rowLabel = toolbox::parseTokenList(itemTitle, separator).front();
      if (std::find(rowLabels.begin(), rowLabels.end(), rowLabel) == rowLabels.end())
        {
          rowLabels.push_back(rowLabel);
        }
      itemsByRow[rowLabel].push_back(iter->first);
    }

  res << "<table class=\"tcds-item-grid xdaq-table-nohover\">"
      << "<tbody>"
      << "\n";

  for (std::vector<std::string>::const_iterator rowLabel = rowLabels.begin();
       rowLabel != rowLabels.end();
       ++rowLabel)
    {
      res << "<tr>"
          << "<th rowspan=\"2\">" << *rowLabel << "</th>";
      for (std::vector<std::string>::const_iterator itemName = itemsByRow[*rowLabel].begin();
           itemName != itemsByRow[*rowLabel].end();
           ++itemName)
        {
          std::string const itemSubLabel = toolbox::parseTokenList(*itemName, separator).back();
          std::string const tmp = "[\"" + toolbox::jsonquote(itemSetName_) + "\"][\"" + toolbox::jsonquote(*itemName) + "\"]";
          res << "<th>"
              << itemSubLabel
              << "</th>";
        }
      res << "</tr>"
          << "<tr>";
      for (std::vector<std::string>::const_iterator itemName = itemsByRow[*rowLabel].begin();
           itemName != itemsByRow[*rowLabel].end();
           ++itemName)
        {
          std::string const itemSubLabel = toolbox::parseTokenList(*itemName, separator).back();
          std::string const tmp = "[\"" + toolbox::jsonquote(itemSetName_) + "\"][\"" + toolbox::jsonquote(*itemName) + "\"]";
          res << "<td class=\"tcds-item-value\">"
              << "<script type=\"text/x-dot-template\">"
              << "{{=it" << tmp << "}}"
              << "</script>"
              << "<div class=\"target\">" << kDefaultValueString << "</div>"
              << "</td>";
        }
      res << "</tr>"
          << "\n";
    }

  res << "</tbody>"
      << "</table>";
  res << "\n";

  res << "</div>"
      << "\n";

  return res.str();
}
