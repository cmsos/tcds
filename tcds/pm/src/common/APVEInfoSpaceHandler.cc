#include "tcds/pm/APVEInfoSpaceHandler.h"

#include <stdint.h>

#include "toolbox/string.h"

#include "tcds/apve/Definitions.h"
#include "tcds/apve/InfoSpaceHandlerUtils.h"
#include "tcds/apve/Utils.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Utils.h"
#include "tcds/utils/WebServer.h"

tcds::pm::APVEInfoSpaceHandler::APVEInfoSpaceHandler(xdaq::Application& xdaqApp,
                                                     tcds::utils::InfoSpaceUpdater* updater) :
  InfoSpaceHandler(xdaqApp, "tcds-pm-apve-info", updater),
  itemPrefix_("apve.")
{
  tcds::apve::createItemsAPVE(this, itemPrefix_);
  tcds::apve::createItemsAPVETriggerMask(this, itemPrefix_);
  tcds::apve::createItemsAPVEStatus(this, itemPrefix_);
  tcds::apve::createItemsAPVECounters(this, itemPrefix_);
  tcds::apve::createItemsAPVESimHist(this, itemPrefix_);
}

tcds::pm::APVEInfoSpaceHandler::~APVEInfoSpaceHandler()
{
}

void
tcds::pm::APVEInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  tcds::apve::registerItemSetsWithMonitorAPVE(monitor, this, itemPrefix_);
  tcds::apve::registerItemSetsWithMonitorAPVETriggerMask(monitor, this, itemPrefix_);
  tcds::apve::registerItemSetsWithMonitorAPVEStatus(monitor, this, itemPrefix_);
  tcds::apve::registerItemSetsWithMonitorAPVECounters(monitor, this, itemPrefix_);
  tcds::apve::registerItemSetsWithMonitorAPVESimHist(monitor, this, itemPrefix_);
}

void
tcds::pm::APVEInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                              tcds::utils::Monitor& monitor,
                                                              std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "APVE" : forceTabName;

  webServer.registerTab(tabName,
                        "APVE configuration and information",
                        4);
  tcds::apve::registerItemSetsWithWebServerAPVE(webServer, monitor, tabName);
  tcds::apve::registerItemSetsWithWebServerAPVETriggerMask(webServer, monitor, tabName);
  tcds::apve::registerItemSetsWithWebServerAPVEStatus(webServer, monitor, tabName);
  tcds::apve::registerItemSetsWithWebServerAPVECounters(webServer, monitor, tabName);
  tcds::apve::registerItemSetsWithWebServerAPVESimHist(webServer, monitor, tabName, 4);
}

std::string
tcds::pm::APVEInfoSpaceHandler::formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const
{
  // Specialized formatting rules for some things like the pipeline
  // history.
  std::string res = tcds::utils::escapeAsJSONString(tcds::utils::InfoSpaceHandler::kInvalidItemString);
  if (item->isValid())
    {
      std::string name = item->name();
      if (toolbox::endsWith(name, "config.apv_read_mode"))
        {
          uint32_t const value = getUInt32(name);
          tcds::definitions::APVE_READOUT_MODE valueEnum =
            static_cast<tcds::definitions::APVE_READOUT_MODE>(value);
          res = tcds::utils::escapeAsJSONString(tcds::apve::APVEReadoutModeToString(valueEnum));
        }
      else if (toolbox::endsWith(name, "config.apv_trigger_mode"))
        {
          uint32_t const value = getUInt32(name);
          tcds::definitions::APVE_TRIGGER_MODE valueEnum =
            static_cast<tcds::definitions::APVE_TRIGGER_MODE>(value);
          res = tcds::utils::escapeAsJSONString(tcds::apve::APVETriggerModeToString(valueEnum));
        }
      else if (name == "apve.apv_simulated_pipeline_history")
        {
          // Special in the sense that this is something that needs to
          // be interpreted as a proper JavaScript object, so let's
          // not add any more double quotes.
          res = getString(name);
        }
      else
        {
          // For everything else simply call the generic formatter.
          res = InfoSpaceHandler::formatItem(item);
        }
    }
  return res;
}
