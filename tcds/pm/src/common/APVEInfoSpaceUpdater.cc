#include "tcds/pm/APVEInfoSpaceUpdater.h"

#include <stdint.h>
#include <string>
#include <vector>

#include "toolbox/string.h"

#include "tcds/apve/APVHistContents.h"
#include "tcds/pm/TCADevicePMCommonBase.h"
#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/InfoSpaceItem.h"

tcds::pm::APVEInfoSpaceUpdater::APVEInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                                     tcds::pm::TCADevicePMCommonBase const& hw) :
  tcds::utils::HwInfoSpaceUpdaterBase(xdaqApp, hw),
  hw_(hw)
{
}

tcds::pm::APVEInfoSpaceUpdater::~APVEInfoSpaceUpdater()
{
}

// BUG BUG BUG
// The following code is a bastardised copy-paste from
// tcds::apve::APVECountersInfoSpaceUpdater. This could be improved
// upon.
void
tcds::pm::APVEInfoSpaceUpdater::updateInfoSpaceImpl(tcds::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  // Latch the APVE statistics counters.
  tcds::pm::TCADevicePMCommonBase const& hw = getHw();
  if (hw.isHwConnected())
    {
      hw.writeRegister("apve.ttc_stats.latch", 0x1);
    }
  tcds::utils::HwInfoSpaceUpdaterBase::updateInfoSpaceImpl(infoSpaceHandler);
}
// BUG BUG BUG end

bool
tcds::pm::APVEInfoSpaceUpdater::updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                                    tcds::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  tcds::pm::TCADevicePMCommonBase const& hw = getHw();
  bool updated = false;
  if (hw.isHwConnected())
    {
      // BUG BUG BUG
      // The following code is an ugly copy-paste from
      // tcds::apve::APVECountersInfoSpaceUpdater. This could be
      // improved.

      // NOTE: The following are all 64-bit counters. They are
      // 'clocked-out' in four successive 16-bit reads.
      // NOTE: The assumption for the below is that nothing read these
      // registers after the latching in updateInfoSpaceImpl().
      std::string const name = item.name();
      if (toolbox::endsWith(name, "_count"))
        {
          uint32_t const tmp0 = hw.readRegister(name);
          uint32_t const tmp1 = hw.readRegister(name);
          uint32_t const tmp2 = hw.readRegister(name);
          uint32_t const tmp3 = hw.readRegister(name);
          uint64_t const newVal =
            uint64_t(tmp0 & 0x0000ffff) +
            (uint64_t(tmp1 & 0x0000ffff) << 16) +
            (uint64_t(tmp2 & 0x0000ffff) << 32) +
            (uint64_t(tmp3 & 0x0000ffff) << 48);
          infoSpaceHandler->setUInt64(name, newVal);
          updated = true;
        }
      // BUG BUG BUG end
      else if (name == "apve.apv_simulated_pipeline_history")
        {
          std::vector<uint32_t> const histContents = hw.readSimPipelineHistory();
          tcds::apve::APVHistContents const tmp(histContents);
          std::string const newVal = tmp.getJSONString();
          infoSpaceHandler->setString(name, newVal);
          updated = true;
        }
      if (!updated)
        {
          updated = tcds::utils::HwInfoSpaceUpdaterBase::updateInfoSpaceItem(item, infoSpaceHandler);
        }
    }

  if (updated)
    {
      item.setValid();
    }
  else
    {
      item.setInvalid();
    }

  return updated;
}

tcds::pm::TCADevicePMCommonBase const&
tcds::pm::APVEInfoSpaceUpdater::getHw() const
{
  return hw_;
}
