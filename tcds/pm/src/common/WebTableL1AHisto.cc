#include "tcds/pm/WebTableL1AHisto.h"

#include <cassert>
#include <sstream>
#include <string>

#include "tcds/utils/Definitions.h"
#include "tcds/utils/Monitor.h"

tcds::pm::WebTableL1AHisto::WebTableL1AHisto(std::string const& name,
                                             std::string const& description,
                                             tcds::utils::Monitor const& monitor,
                                             std::string const& itemSetName,
                                             std::string const& tabName,
                                             size_t const colSpan) :
  tcds::utils::WebObject(name, description, monitor, itemSetName, tabName, colSpan)
{
}

std::string
tcds::pm::WebTableL1AHisto::getHTMLString() const
{
  // NOTE: This has been written to work with the new XDAQ12-style
  // HyperDAQ tabs and doT.js.

  std::stringstream res;

  res << "<div class=\"tcds-item-table-wrapper\">"
      << "\n";

  res << "<p class=\"tcds-item-table-title\">"
      << getName()
      << "</p>";
  res << "\n";

  res << "<p class=\"tcds-item-table-description\">"
      << getDescription()
      << "</p>";
  res << "\n";

  tcds::utils::Monitor::StringPairVector items =
    monitor_.getFormattedItemSet(itemSetName_);
  // ASSERT ASSERT ASSERT
  assert (items.size() == (tcds::definitions::kTrigTypeMax - tcds::definitions::kTrigTypeMin + 1));
  // ASSERT ASSERT ASSERT end

  res << "<div id=\"l1ahistos-placeholder\">loading...</div>"
      << "\n";

  res << "</div>";

  res << "\n";

  return res.str();
}
