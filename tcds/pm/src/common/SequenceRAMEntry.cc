#include "tcds/pm/SequenceRAMEntry.h"

tcds::pm::SequenceRAMEntry::SequenceRAMEntry(uint32_t const rawData) :
  rawData_(rawData)
{
}

uint32_t
tcds::pm::SequenceRAMEntry::rawData() const
{
  return rawData_;
}

bool
tcds::pm::SequenceRAMEntry::isEndOfSequence() const
{
  return (opCode() == opCodeEndOfSequence_);
}

bool
tcds::pm::SequenceRAMEntry::isFireL1A() const
{
  return (opCode() == opCodeFireL1A_);
}

bool
tcds::pm::SequenceRAMEntry::isFireBgo() const
{
  return (opCode() ==  opCodeFireBgo_);
}

bool
tcds::pm::SequenceRAMEntry::isSwitchToSequenceTriggerMode() const
{
  return (opCode() == opCodeSwitchToSequenceTriggerMode_);
}

bool
tcds::pm::SequenceRAMEntry::isSwitchToNormalTriggerMode() const
{
  return (opCode() == opCodeSwitchToNormalTriggerMode_);
}

bool
tcds::pm::SequenceRAMEntry::isPause() const
{
  return (opCode() == opCodePause_);
}

bool
tcds::pm::SequenceRAMEntry::isUnpause() const
{
  return (opCode() == opCodeUnpause_);
}

uint8_t
tcds::pm::SequenceRAMEntry::opCode() const
{
  return uint8_t(rawData_ & 0xff);
}

uint8_t
tcds::pm::SequenceRAMEntry::bgoNumber() const
{
  return uint8_t((rawData_ & 0xff00) >> 8);
}

uint16_t
tcds::pm::SequenceRAMEntry::delay() const
{
  return uint16_t((rawData_ & 0xffff0000) >> 16);
}
