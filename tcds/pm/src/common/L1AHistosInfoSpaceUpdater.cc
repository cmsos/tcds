#include "tcds/pm/L1AHistosInfoSpaceUpdater.h"

#include <cstddef>
#include <stdint.h>
#include <string>
#include <vector>

#include "toolbox/string.h"

#include "tcds/hwlayer/DeviceBase.h"
#include "tcds/pm/L1AHisto.h"
#include "tcds/utils/Definitions.h"
#include "tcds/utils/InfoSpaceHandler.h"

tcds::pm::L1AHistosInfoSpaceUpdater::L1AHistosInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                                               tcds::hwlayer::DeviceBase const& hw) :
  tcds::utils::HwInfoSpaceUpdaterBase(xdaqApp, hw)
{
}

tcds::pm::L1AHistosInfoSpaceUpdater::~L1AHistosInfoSpaceUpdater()
{
}

void
tcds::pm::L1AHistosInfoSpaceUpdater::updateInfoSpaceImpl(tcds::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  tcds::hwlayer::DeviceBase const& hw = getHw();
  if (hw.isReadyForUse())
    {
      std::vector<uint32_t> const histoContentsRaw = hw.readBlock("l1a_histos");
      size_t const histoSize = histoContentsRaw.size() / tcds::definitions::kNumTrigTypes;
      for (int trigType = tcds::definitions::kTrigTypeMin;
           trigType <= tcds::definitions::kTrigTypeMax;
           ++trigType)
        {
          std::vector<uint32_t>::const_iterator lo =
            histoContentsRaw.begin() + (trigType - tcds::definitions::kTrigTypeMin) * histoSize;
          std::vector<uint32_t>::const_iterator hi = lo + histoSize;
          std::vector<uint32_t> const tmp(lo, hi);
          std::string const name = toolbox::toString("l1a_histos.trigger_type%d", trigType);
          L1AHisto const histo(tmp);
          std::string const newVal = histo.getJSONString();
          infoSpaceHandler->setString(name, newVal);
        }
      infoSpaceHandler->setValid();
    }
  else
    {
      infoSpaceHandler->setInvalid();
    }

  // Now sync everything from the cache to the InfoSpace itself.
  infoSpaceHandler->writeInfoSpace();
}
