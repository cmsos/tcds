#include "tcds/pm/WebTableTTSCounters.h"

#include <algorithm>
#include <tr1/unordered_map>
#include <sstream>
#include <string>
#include <vector>

#include "toolbox/string.h"

#include "tcds/utils/Monitor.h"

tcds::pm::WebTableTTSCounters::WebTableTTSCounters(std::string const& name,
                                                   std::string const& description,
                                                   tcds::utils::Monitor const& monitor,
                                                   std::string const& itemSetName,
                                                   std::string const& tabName,
                                                   size_t const colSpan) :
  tcds::utils::WebObject(name, description, monitor, itemSetName, tabName, colSpan)
{
}

std::string
tcds::pm::WebTableTTSCounters::getHTMLString() const
{
  // NOTE: This has been written to work with the new XDAQ12-style
  // HyperDAQ tabs and doT.js.

  std::stringstream res;

  res << "<div class=\"tcds-item-table-wrapper\">"
      << "\n";

  res << "<p class=\"tcds-item-table-title\">"
      << getName()
      << "</p>";
  res << "\n";

  res << "<p class=\"tcds-item-table-description\">"
      << getDescription()
      << "</p>";
  res << "\n";

  // And now produce actual table contents.
  tcds::utils::Monitor::StringPairVector itemsRaw =
    monitor_.getFormattedItemSet(itemSetName_);

  // Step 1: determine the row labels (i.e., the ICIs and APVEs in a
  // single LPM) and column labels (i.e., TTS states) and group items
  // by row label.
  std::string const separator = ";";
  std::tr1::unordered_map<std::string,
                          std::tr1::unordered_map<std::string, std::string> > items;
  std::vector<std::string> rowLabels;
  std::vector<std::string> colLabels;

  tcds::utils::Monitor::StringPairVector::const_iterator iter;
  for (iter = itemsRaw.begin(); iter != itemsRaw.end(); ++iter)
    {
      std::string const itemTitle = iter->first;
      std::string const rowLabel = toolbox::parseTokenList(itemTitle, separator).front();
      std::string const colLabel = toolbox::parseTokenList(itemTitle, separator).back();
      if (std::find(rowLabels.begin(), rowLabels.end(), rowLabel) == rowLabels.end())
        {
          rowLabels.push_back(rowLabel);
        }
      if (std::find(colLabels.begin(), colLabels.end(), colLabel) == colLabels.end())
        {
          colLabels.push_back(colLabel);
        }
      items[rowLabel][colLabel] = itemTitle;
    }

  res << "<table class=\"tcds-item-grid xdaq-table-nohover\">"
      << "<tbody>"
      << "\n";

  // Step 2: The header row.
  res << "<tr>"
      << "<th></th>";
  for (std::vector<std::string>::const_iterator colLabel = colLabels.begin();
       colLabel != colLabels.end();
       ++colLabel)
    {
      res << "<th>" << *colLabel << "</th>";
    }
  res << "</tr>"
      << "\n";

  // Step 3: The data rows.
  for (std::vector<std::string>::const_iterator rowLabel = rowLabels.begin();
       rowLabel != rowLabels.end();
       ++rowLabel)
    {
      res << "<tr>"
          << "<th>" << *rowLabel << "</th>";
      for (std::vector<std::string>::const_iterator colLabel = colLabels.begin();
           colLabel != colLabels.end();
           ++colLabel)
        {
          std::string const itemName = items[*rowLabel][*colLabel];
          std::string const tmp = "[\"" + toolbox::jsonquote(itemSetName_) + "\"][\"" + toolbox::jsonquote(itemName) + "\"]";
          res << "<td class=\"tcds-item-value\">"
              << "<script type=\"text/x-dot-template\">"
              << "{{=it" << tmp << "}}"
              << "</script>"
              << "<div class=\"target\">" << kDefaultValueString << "</div>"
              << "</td>";
        }
      res << "</tr>"
          << "\n";
    }

  res << "</tbody>"
      << "</table>";
  res << "\n";

  res << "</div>"
      << "\n";

  return res.str();
}
