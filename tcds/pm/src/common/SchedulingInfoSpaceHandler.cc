#include "tcds/pm/SchedulingInfoSpaceHandler.h"

#include <stdint.h>
#include <string>

#include "toolbox/string.h"

#include "tcds/utils/Definitions.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/Utils.h"
#include "tcds/utils/WebServer.h"

tcds::pm::SchedulingInfoSpaceHandler::SchedulingInfoSpaceHandler(xdaq::Application& xdaqApp,
                                                                 tcds::utils::InfoSpaceUpdater* updater) :
  InfoSpaceHandler(xdaqApp, "tcds-scheduling", updater)
{
  // B-go firing BXs.
  for (int bgoNum = 0; bgoNum <= tcds::definitions::kBgoNumMax; ++bgoNum)
    {
      createUInt32(toolbox::toString("bgo_channels.bgo_channel%d.firing_bx", bgoNum));
    }

  // Sequence/calibration trigger firing BX.
  createUInt32("bgo_trains.bgo_train_config.firing_bx_calib_trigger");
}

tcds::pm::SchedulingInfoSpaceHandler::~SchedulingInfoSpaceHandler()
{
}

void
tcds::pm::SchedulingInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  // B-go firing BXs.
  monitor.newItemSet("itemset-scheduling-bgos");
  for (int bgoNum = 0; bgoNum <= tcds::definitions::kBgoNumMax; ++bgoNum)
    {
      monitor.addItem("itemset-scheduling-bgos",
                      toolbox::toString("bgo_channels.bgo_channel%d.firing_bx", bgoNum),
                      tcds::utils::formatBgoNameString(static_cast<tcds::definitions::BGO_NUM>(bgoNum)),
                      this);
    }

  // Sequence/calibration trigger firing BX.
  monitor.newItemSet("itemset-scheduling-trigger");
  monitor.addItem("itemset-scheduling-trigger",
                  "bgo_trains.bgo_train_config.firing_bx_calib_trigger",
                  "Sequence-trigger firing BX",
                  this);
}

void
tcds::pm::SchedulingInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                    tcds::utils::Monitor& monitor,
                                                                    std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "Scheduling" : forceTabName;

  webServer.registerTab(tabName,
                        "Info on what fires when",
                        2);

  webServer.registerTable("B-go firing BXs",
                          "",
                          monitor,
                          "itemset-scheduling-bgos",
                          tabName,
                          1);

  webServer.registerTable("Sequence-trigger firing BX",
                          "",
                          monitor,
                          "itemset-scheduling-trigger",
                          tabName,
                          1);
}

std::string
tcds::pm::SchedulingInfoSpaceHandler::formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const
{
  std::string res = tcds::utils::escapeAsJSONString(tcds::utils::InfoSpaceHandler::kInvalidItemString);
  if (item->isValid())
    {
      std::string name = item->name();
      if (toolbox::endsWith(name, ".firing_bx"))
        {
          // BX 0 is special: this BX never occurs so this effectively
          // means the entry is disabled.
          uint32_t const val = getUInt32(name);
          if (val == 0)
            {
              res = tcds::utils::escapeAsJSONString("disabled");
            }
          else
            {
              res = tcds::utils::InfoSpaceHandler::formatItem(item);
            }
        }
      else
        {
          // For everything else simply call the generic formatter.
          res = InfoSpaceHandler::formatItem(item);
        }
    }
  return res;
}
