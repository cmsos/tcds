#include "tcds/pm/TTSCountersInfoSpaceUpdater.h"

#include <stdint.h>
#include <string>

#include "tcds/pm/TCADevicePMCommonBase.h"
#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/InfoSpaceItem.h"

tcds::pm::TTSCountersInfoSpaceUpdater::TTSCountersInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                                                   tcds::pm::TCADevicePMCommonBase const& hw) :
  tcds::utils::HwInfoSpaceUpdaterBase(xdaqApp, hw)
{
}

tcds::pm::TTSCountersInfoSpaceUpdater::~TTSCountersInfoSpaceUpdater()
{
}

void
tcds::pm::TTSCountersInfoSpaceUpdater::updateInfoSpaceImpl(tcds::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  tcds::pm::TCADevicePMCommonBase const& hw = getHw();
  if (hw.isReadyForUse())
    {
      // Latch all counters so we can read a consistent snapshot.
      getHw().latchTTSCounters();
    }
  tcds::utils::HwInfoSpaceUpdaterBase::updateInfoSpaceImpl(infoSpaceHandler);
}

bool
tcds::pm::TTSCountersInfoSpaceUpdater::updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                                           tcds::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  tcds::pm::TCADevicePMCommonBase const& hw = getHw();
  bool updated = false;
  if (hw.isHwConnected())
    {
      std::string name = item.name();
      tcds::utils::InfoSpaceItem::UpdateType updateType = item.updateType();
      if (updateType == tcds::utils::InfoSpaceItem::PROCESS)
        {
          if (name.find("tts_time_counters") != std::string::npos)
            {
              // NOTE: These 'time spent in TTS state' counters are a
              // bit tricky. For details, please see
              // tcds::pm::TCADevicePMCommonBase::readTTSTimeCounter().
              uint64_t const newVal = hw.readTTSTimeCounter(name);
              infoSpaceHandler->setUInt64(name, newVal);
              updated = true;
            }
        }
      if (!updated)
        {
          updated = tcds::utils::HwInfoSpaceUpdaterBase::updateInfoSpaceItem(item, infoSpaceHandler);
        }
    }

  if (updated)
    {
      item.setValid();
    }
  else
    {
      item.setInvalid();
    }

  return updated;
}

tcds::pm::TCADevicePMCommonBase const&
tcds::pm::TTSCountersInfoSpaceUpdater::getHw() const
{
  return static_cast<tcds::pm::TCADevicePMCommonBase const&>(tcds::utils::HwInfoSpaceUpdaterBase::getHw());
}
