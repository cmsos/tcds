#include "tcds/pm/PMInfoSpaceHandler.h"

#include <stdint.h>

#include "tcds/pm/Definitions.h"
#include "tcds/pm/Utils.h"
#include "tcds/utils/Utils.h"
#include "tcds/utils/XDAQAppBase.h"

tcds::pm::PMInfoSpaceHandler::PMInfoSpaceHandler(tcds::utils::XDAQAppBase& xdaqApp,
                                                 std::string const& name,
                                                 tcds::utils::InfoSpaceUpdater* updater) :
  tcds::utils::InfoSpaceHandler(xdaqApp, name, updater)
{
}

tcds::pm::PMInfoSpaceHandler::~PMInfoSpaceHandler()
{
}

void
tcds::pm::PMInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  // NOTE: This is a dummy method here. The pm::PMInfoSpaceHandler is
  // not intended for use outside the pm::TTSInfoSpaceHandler.
}

void
tcds::pm::PMInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                            tcds::utils::Monitor& monitor,
                                                            std::string const& forceTabName)
{
  // NOTE: This is a dummy method here. The pm::PMInfoSpaceHandler is
  // not intended for use outside the pm::TTSInfoSpaceHandler.
}

std::string
tcds::pm::PMInfoSpaceHandler::formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const
{
  std::string res = tcds::utils::escapeAsJSONString(tcds::utils::InfoSpaceHandler::kInvalidItemString);

  if (item->isValid())
    {
      std::string const name = item->name();
      if (name == "pm_state")
        {
          uint32_t const value = getUInt32(name);
          tcds::definitions::PM_STATE const valueEnum =
            static_cast<tcds::definitions::PM_STATE>(value);
          res = tcds::utils::escapeAsJSONString(tcds::pm::pmStateToString(valueEnum));
        }
      else
        {
          // For everything else simply call the generic formatter.
          res = tcds::utils::InfoSpaceHandler::formatItem(item);
        }
    }

  return res;
}
