#include "tcds/pm/TTSInfoSpaceHandler.h"

#include <cstddef>

#include "toolbox/string.h"

#include "tcds/pm/Definitions.h"
#include "tcds/pm/PMInfoSpaceHandler.h"
#include "tcds/pm/Utils.h"
#include "tcds/utils/Definitions.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/Utils.h"
#include "tcds/utils/WebServer.h"
#include "tcds/utils/WebTableTTS.h"
#include "tcds/utils/XDAQAppBase.h"

namespace tcds {
  namespace utils {
    class ConfigurationInfoSpaceHandler;
  }
}

tcds::pm::TTSInfoSpaceHandler::TTSInfoSpaceHandler(tcds::utils::XDAQAppBase& xdaqApp,
                                                   tcds::utils::InfoSpaceUpdater* updater,
                                                   bool const cpmMode) :
  tcds::utils::MultiInfoSpaceHandler(xdaqApp, updater),
  isCPMMode_(cpmMode)
{
  // The number of LPMs to include in the monitoring depends on
  // whether we're in LPM- or in CPM mode.
  numLPMs_ = 1;
  if (isCPMMode_)
    {
      numLPMs_ = tcds::definitions::kNumLPMsPerCPM;
    }

  //----------

  // This InfoSpaceHandler is used for 'all the stuff that does not
  // fit elsewhere.'
  tcds::utils::InfoSpaceHandler* handler =
    new tcds::pm::PMInfoSpaceHandler(xdaqApp, "tcds-pm-l1a-blocker-cfg", updater);
  infoSpaceHandlers_["misc"] = handler;

  //----------

  // The overall partition controller state (in the firmware).
  handler = infoSpaceHandlers_["misc"];
  std::string name = "pm_state";
  handler->createUInt32(name,
                        0,
                        "",
                        tcds::utils::InfoSpaceItem::PROCESS);

  //----------

  // The top-level TTS output of the overall intelligent-OR.
  handler = new tcds::utils::InfoSpaceHandler(xdaqApp, "tcds-pm-tts-info-top", updater);
  name = "tts_spy_array.tts_final_or";
  infoSpaceHandlers_[name] = handler;
  createTTSChannel(handler,
                   "Top-level TTS",
                   tcds::utils::ttsChannelTypeToString(tcds::definitions::TTS_CHANNEL_TOP_LEVEL),
                   0,
                   0,
                   name);

  // The TTS-like representation of the DAQ link status.
  handler = new tcds::utils::InfoSpaceHandler(xdaqApp, "tcds-pm-tts-info-daq-backpressure", updater);
  name = "tts_spy_array.tts_from_daq_backpressure";
  infoSpaceHandlers_[name] = handler;
  createTTSChannel(handler,
                   "DAQ link",
                   tcds::utils::ttsChannelTypeToString(tcds::definitions::TTS_CHANNEL_DAQ_BACKPRESSURE),
                   0,
                   0,
                   name);

  // The TTS-like representation of the ReTri state.
  handler = new tcds::utils::InfoSpaceHandler(xdaqApp, "tcds-pm-tts-info-retri", updater);
  name = "tts_spy_array.tts_from_retri";
  infoSpaceHandlers_[name] = handler;
  createTTSChannel(handler,
                   "ReTri",
                   tcds::utils::ttsChannelTypeToString(tcds::definitions::TTS_CHANNEL_RETRI),
                   0,
                   0,
                   name);

  // The TTS-like representation of the APVE state.
  // NOTE: The top-level APVE only exists in the CPM, not in the LPM.
  if (isCPMMode_)
    {
      handler = new tcds::utils::InfoSpaceHandler(xdaqApp, "tcds-pm-tts-info-apve", updater);
      name = "tts_spy_array.tts_from_ipm_apve";
      infoSpaceHandlers_[name] = handler;
      createTTSChannel(handler,
                       "PM APVE",
                       tcds::utils::ttsChannelTypeToString(tcds::definitions::TTS_CHANNEL_PM_APVE),
                       0,
                       0,
                       name);
    }

  // The TTS-like representation of the bunch-mask trigger veto.
  handler = new tcds::utils::InfoSpaceHandler(xdaqApp, "tcds-pm-tts-info-bunch-mask", updater);
  name = "tts_spy_array.tts_from_bunch_mask";
  infoSpaceHandlers_[name] = handler;
  createTTSChannel(handler,
                   "Bunch mask",
                   tcds::utils::ttsChannelTypeToString(tcds::definitions::TTS_CHANNEL_BUNCH_MASK),
                   0,
                   0,
                   name);

  //----------

  tcds::utils::ConfigurationInfoSpaceHandler const& cfgInfoSpace =
    getOwnerApplication().getConfigurationInfoSpaceHandler();

  // All individual ICI/APVE TTS input values, listed per LPM.
  for (unsigned int lpmNum = 1; lpmNum <= numLPMs_; ++lpmNum)
    {
      // ICIs.
      for (unsigned int iciNum = 1; iciNum <= tcds::definitions::kNumICIsPerLPM; ++iciNum)
        {
          tcds::utils::InfoSpaceHandler* handler =
            new tcds::utils::InfoSpaceHandler(xdaqApp,
                                              toolbox::toString("tcds-pm-tts-info-lpm%d-ici%d",
                                                                lpmNum, iciNum),
                                              updater);
          infoSpaceHandlers_[toolbox::toString("tts_spy_array.lpm%d.ici%d", lpmNum, iciNum)] = handler;
          createTTSChannel(handler,
                           tcds::pm::formatICILabel(isCPMMode_, lpmNum, iciNum, cfgInfoSpace, true),
                           tcds::utils::ttsChannelTypeToString(tcds::definitions::TTS_CHANNEL_ICI),
                           lpmNum,
                           iciNum,
                           toolbox::toString("tts_spy_array.lpm%d.ici%d", lpmNum, iciNum));
        }
      // APVEs.
      for (unsigned int apveNum = 1; apveNum <= tcds::definitions::kNumAPVEsPerLPM; ++apveNum)
        {
          tcds::utils::InfoSpaceHandler* handler =
            new tcds::utils::InfoSpaceHandler(xdaqApp,
                                              toolbox::toString("tcds-pm-tts-info-lpm%d-apve%d",
                                                                lpmNum, apveNum),
                                              updater);
          infoSpaceHandlers_[toolbox::toString("tts_spy_array.lpm%d.apve%d", lpmNum, apveNum)] = handler;
          createTTSChannel(handler,
                           tcds::pm::formatAPVELabel(isCPMMode_, lpmNum, apveNum, cfgInfoSpace, true),
                           tcds::utils::ttsChannelTypeToString(tcds::definitions::TTS_CHANNEL_APVE),
                           lpmNum,
                           apveNum,
                           toolbox::toString("tts_spy_array.lpm%d.apve%d", lpmNum, apveNum));
        }
    }

  //----------

  // Enabled/disabled flags for all known sources of trigger-blocking.
  handler = infoSpaceHandlers_["misc"];
  handler->createBool("main.inselect.tts_enable", false, "enabled/disabled");
  handler->createBool("main.inselect.daq_backpressure_enable", false, "enabled/disabled");
  handler->createBool("tts_spy_array.tts_from_daq_backpressure", false, "tts_state");
  handler->createBool("main.inselect.retri_enable", false, "enabled/disabled");
  handler->createBool("tts_spy_array.tts_from_retri", false, "tts_state");
  handler->createBool("main.inselect.apve_enable", false, "enabled/disabled");
  handler->createBool("tts_spy_array.tts_from_ipm_apve", false, "tts_state");
  handler->createBool("main.inselect.bunch_mask_veto_enable", false, "enabled/disabled");
  handler->createBool("tts_spy_array.tts_from_bunch_mask", false, "tts_state");

  //----------

  // The trigger veto bunch mask.
  handler = infoSpaceHandlers_["misc"];
  handler->createUInt32("bunch_mask_veto_pattern_size", 0, "", tcds::utils::InfoSpaceItem::PROCESS);
  handler->createString("bunch_mask_veto_pattern", "", "", tcds::utils::InfoSpaceItem::PROCESS);

  //----------

  // The trigger rules.
  handler = infoSpaceHandlers_["misc"];
  handler->createUInt32("trigger_rules.one_trigger_deadtime", 0, "", tcds::utils::InfoSpaceItem::PROCESS);
  handler->createUInt32("trigger_rules.two_trigger_deadtime", 0, "", tcds::utils::InfoSpaceItem::PROCESS);
  handler->createUInt32("trigger_rules.three_trigger_deadtime", 0, "", tcds::utils::InfoSpaceItem::PROCESS);
  handler->createUInt32("trigger_rules.four_trigger_deadtime", 0, "", tcds::utils::InfoSpaceItem::PROCESS);
  handler->createUInt32("trigger_rules.five_trigger_deadtime", 0, "", tcds::utils::InfoSpaceItem::PROCESS);
  handler->createUInt32("trigger_rules.six_trigger_deadtime", 0, "", tcds::utils::InfoSpaceItem::PROCESS);
  handler->createUInt32("trigger_rules.seven_trigger_deadtime", 0, "", tcds::utils::InfoSpaceItem::PROCESS);
  handler->createUInt32("trigger_rules.eight_trigger_deadtime", 0, "", tcds::utils::InfoSpaceItem::PROCESS);
  handler->createUInt32("trigger_rules.nine_trigger_deadtime", 0, "", tcds::utils::InfoSpaceItem::PROCESS);
  handler->createUInt32("trigger_rules.ten_trigger_deadtime", 0, "", tcds::utils::InfoSpaceItem::PROCESS);
  handler->createUInt32("trigger_rules.eleven_trigger_deadtime", 0, "", tcds::utils::InfoSpaceItem::PROCESS);
  handler->createUInt32("trigger_rules.twelve_trigger_deadtime", 0, "", tcds::utils::InfoSpaceItem::PROCESS);
  handler->createUInt32("trigger_rules.thirteen_trigger_deadtime", 0, "", tcds::utils::InfoSpaceItem::PROCESS);
  handler->createUInt32("trigger_rules.fourteen_trigger_deadtime", 0, "", tcds::utils::InfoSpaceItem::PROCESS);
  handler->createUInt32("trigger_rules.fifteen_trigger_deadtime", 0, "", tcds::utils::InfoSpaceItem::PROCESS);
}

tcds::pm::TTSInfoSpaceHandler::~TTSInfoSpaceHandler()
{
  // Cleanup of InfoSpaceHandlers happens in the MultiInfoSpaceHandler
  // base class.
}

tcds::utils::XDAQAppBase&
tcds::pm::TTSInfoSpaceHandler::getOwnerApplication() const
{
  return static_cast<tcds::utils::XDAQAppBase&>(tcds::utils::MultiInfoSpaceHandler::getOwnerApplication());
}

void
tcds::pm::TTSInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  monitor.newItemSet("itemset-running-state");

  // The overall partition controller state (in the firmware).
  monitor.addItem("itemset-running-state",
                  "pm_state",
                  "Running state",
                  infoSpaceHandlers_.at("misc"));

  //----------

  // All known trigger-blocking reasons.
  monitor.newItemSet("itemset-trigger-blockers");

  // Overall TTS enable flag.
  monitor.addItem("itemset-trigger-blockers",
                  "main.inselect.tts_enable",
                  "TTS",
                  infoSpaceHandlers_.at("misc"),
                  "The Trigger Throttling System signals FED readiness.");
  // The top-level TTS output of the overall intelligent-OR.
  monitor.addItem("itemset-trigger-blockers",
                  "tts_source_value",
                  "Top-level TTS state",
                  infoSpaceHandlers_.at("tts_spy_array.tts_final_or"));

  // DAQ backpressure enable flag.
  monitor.addItem("itemset-trigger-blockers",
                  "main.inselect.daq_backpressure_enable",
                  "DAQ back-pressure",
                  infoSpaceHandlers_.at("misc"),
                  "DAQ back-pressure allows trigger throttling based on DAQ readout performance.");
  // A TTS-like representation of the DAQ link status.
  monitor.addItem("itemset-trigger-blockers",
                  "tts_source_value",
                  "DAQ link TTS state",
                  infoSpaceHandlers_.at("tts_spy_array.tts_from_daq_backpressure"));

  // ReTri enable flag.
  monitor.addItem("itemset-trigger-blockers",
                  "main.inselect.retri_enable",
                  "ReTri trigger suppression",
                  infoSpaceHandlers_.at("misc"),
                  "The Resonant-Trigger protection suppresses triggers to avoid resonant trigger conditions.");
  // A TTS-like representation of the ReTri status.
  monitor.addItem("itemset-trigger-blockers",
                  "tts_source_value",
                  "ReTri TTS state",
                  infoSpaceHandlers_.at("tts_spy_array.tts_from_retri"));

  // PM APVE enable flag.
  // NOTE: The top-level APVE only exists in the CPM, not in the LPM.
  if (isCPMMode_)
    {
      monitor.addItem("itemset-trigger-blockers",
                      "main.inselect.apve_enable",
                      "PM APVE trigger suppression",
                      infoSpaceHandlers_.at("misc"),
                      "The APV Emulator in the Partition Manager protects against sub-system buffer overflows.");
      // The TTS-like representation of the APVE state.
      monitor.addItem("itemset-trigger-blockers",
                      "tts_source_value",
                      "PM APVE TTS state",
                      infoSpaceHandlers_.at("tts_spy_array.tts_from_ipm_apve"));
    }

  // Bunch mask trigger suppression enable flag.
  monitor.addItem("itemset-trigger-blockers",
                  "main.inselect.bunch_mask_veto_enable",
                  "Bunch-mask trigger veto",
                  infoSpaceHandlers_.at("misc"),
                  "A bunch-mask can be used as trigger veto.");
  // The TTS-like representation of the bunch-mask trigger veto.
  monitor.addItem("itemset-trigger-blockers",
                  "tts_source_value",
                  "Bunch mask TTS state",
                  infoSpaceHandlers_.at("tts_spy_array.tts_from_bunch_mask"));

  //----------

  // The trigger veto bunch mask.
  monitor.newItemSet("itemset-veto-mask");
  monitor.addItem("itemset-veto-mask",
                  "bunch_mask_veto_pattern_size",
                  "Number of vetoed BXs per orbit",
                  infoSpaceHandlers_.at("misc"));
  monitor.addItem("itemset-veto-mask",
                  "bunch_mask_veto_pattern",
                  "Vetoed BXs",
                  infoSpaceHandlers_.at("misc"));

  //----------

  // The trigger rules.
  monitor.newItemSet("itemset-trigger-rules");
  monitor.addItem("itemset-trigger-rules",
                  "trigger_rules.one_trigger_deadtime",
                  "N for trigger rule 'not more than one L1A in N BXs'",
                  infoSpaceHandlers_.at("misc"));
  monitor.addItem("itemset-trigger-rules",
                  "trigger_rules.two_trigger_deadtime",
                  "N for trigger rule 'not more than two L1As in N BXs'",
                  infoSpaceHandlers_.at("misc"));
  monitor.addItem("itemset-trigger-rules",
                  "trigger_rules.three_trigger_deadtime",
                  "N for trigger rule 'not more than three L1As in N BXs'",
                  infoSpaceHandlers_.at("misc"));
  monitor.addItem("itemset-trigger-rules",
                  "trigger_rules.four_trigger_deadtime",
                  "N for trigger rule 'not more than four L1As in N BXs'",
                  infoSpaceHandlers_.at("misc"));
  monitor.addItem("itemset-trigger-rules",
                  "trigger_rules.five_trigger_deadtime",
                  "N for trigger rule 'not more than five L1As in N BXs'",
                  infoSpaceHandlers_.at("misc"));
  monitor.addItem("itemset-trigger-rules",
                  "trigger_rules.six_trigger_deadtime",
                  "N for trigger rule 'not more than six L1As in N BXs'",
                  infoSpaceHandlers_.at("misc"));
  monitor.addItem("itemset-trigger-rules",
                  "trigger_rules.seven_trigger_deadtime",
                  "N for trigger rule 'not more than seven L1As in N BXs'",
                  infoSpaceHandlers_.at("misc"));
  monitor.addItem("itemset-trigger-rules",
                  "trigger_rules.eight_trigger_deadtime",
                  "N for trigger rule 'not more than eight L1As in N BXs'",
                  infoSpaceHandlers_.at("misc"));
  monitor.addItem("itemset-trigger-rules",
                  "trigger_rules.nine_trigger_deadtime",
                  "N for trigger rule 'not more than nine L1As in N BXs'",
                  infoSpaceHandlers_.at("misc"));
  monitor.addItem("itemset-trigger-rules",
                  "trigger_rules.ten_trigger_deadtime",
                  "N for trigger rule 'not more than ten L1As in N BXs'",
                  infoSpaceHandlers_.at("misc"));
  monitor.addItem("itemset-trigger-rules",
                  "trigger_rules.eleven_trigger_deadtime",
                  "N for trigger rule 'not more than eleven L1As in N BXs'",
                  infoSpaceHandlers_.at("misc"));
  monitor.addItem("itemset-trigger-rules",
                  "trigger_rules.twelve_trigger_deadtime",
                  "N for trigger rule 'not more than twelve L1As in N BXs'",
                  infoSpaceHandlers_.at("misc"));
  monitor.addItem("itemset-trigger-rules",
                  "trigger_rules.thirteen_trigger_deadtime",
                  "N for trigger rule 'not more than thirteen L1As in N BXs'",
                  infoSpaceHandlers_.at("misc"));
  monitor.addItem("itemset-trigger-rules",
                  "trigger_rules.fourteen_trigger_deadtime",
                  "N for trigger rule 'not more than fourteen L1As in N BXs'",
                  infoSpaceHandlers_.at("misc"));
  monitor.addItem("itemset-trigger-rules",
                  "trigger_rules.fifteen_trigger_deadtime",
                  "N for trigger rule 'not more than fifteen L1As in N BXs'",
                  infoSpaceHandlers_.at("misc"));

  //----------

  // All individual ICI/APVE TTS input values, listed per LPM.
  tcds::utils::ConfigurationInfoSpaceHandler const& cfgInfoSpace =
    getOwnerApplication().getConfigurationInfoSpaceHandler();
  for (unsigned int lpmNum = 1; lpmNum <= numLPMs_; ++lpmNum)
    {
      std::string itemSetName;
      if (isCPMMode_)
        {
          itemSetName = toolbox::toString("itemset-tts-states-lpm%d", lpmNum);
        }
        else
          {
            itemSetName = "itemset-tts-states";
          }
      monitor.newItemSet(itemSetName);

      // ICIs.
      for (unsigned int iciNum = 1; iciNum <= tcds::definitions::kNumICIsPerLPM; ++iciNum)
        {
          std::string desc = tcds::pm::formatICILabel(isCPMMode_, lpmNum, iciNum, cfgInfoSpace, false);
          monitor.addItem(itemSetName,
                          "tts_source_value",
                          desc,
                          infoSpaceHandlers_.at(toolbox::toString("tts_spy_array.lpm%d.ici%d", lpmNum, iciNum)));
        }

      // APVEs.
      for (unsigned int apveNum = 1; apveNum <= tcds::definitions::kNumAPVEsPerLPM; ++apveNum)
        {
          std::string desc = tcds::pm::formatAPVELabel(isCPMMode_, lpmNum, apveNum, cfgInfoSpace, false);
          monitor.addItem(itemSetName,
                          "tts_source_value",
                          desc,
                          infoSpaceHandlers_.at(toolbox::toString("tts_spy_array.lpm%d.apve%d", lpmNum, apveNum)));
        }
    }
}

void
tcds::pm::TTSInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                             tcds::utils::Monitor& monitor,
                                                             std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "TTS and trigger blockers" : forceTabName;
  size_t const numColumns = 4;

  webServer.registerTab(tabName,
                        "Information on TTS and other trigger-blocking sources affecting the running state",
                        numColumns);

  //----------

  webServer.registerWebObject<tcds::utils::WebTableTTS>("Running state",
                                                        "Firmware PM running state",
                                                        monitor,
                                                        "itemset-running-state",
                                                        tabName);

  webServer.registerWebObject<tcds::utils::WebTableTTS>("Trigger-blockers",
                                                        "Trigger-blocking reasons",
                                                        monitor,
                                                        "itemset-trigger-blockers",
                                                        tabName);

  webServer.registerTable("BX-mask trigger veto",
                          "Bunch-mask trigger-veto configuration",
                          monitor,
                          "itemset-veto-mask",
                          tabName);

  webServer.registerTable("Trigger rules",
                          "Trigger rules configuration. "
                          "If set to zero, the trigger rule is disabled.",
                          monitor,
                          "itemset-trigger-rules",
                          tabName);

  //----------

  tcds::utils::ConfigurationInfoSpaceHandler const& cfgInfoSpace =
    getOwnerApplication().getConfigurationInfoSpaceHandler();

  // All individual ICI/APVE TTS input values, listed per LPM.
  for (unsigned int lpmNum = 1; lpmNum <= numLPMs_; ++lpmNum)
    {
      std::string tableName;
      std::string itemSetName;
      if (isCPMMode_)
        {
          tableName =
            toolbox::toString("%s TTS values",
                              tcds::utils::formatLPMLabel(lpmNum, cfgInfoSpace).c_str());
          itemSetName = toolbox::toString("itemset-tts-states-lpm%d", lpmNum);
        }
      else
        {
          tableName = "TTS values";
          itemSetName = "itemset-tts-states";
        }

      webServer.registerWebObject<tcds::utils::WebTableTTS>(tableName,
                                                            "",
                                                            monitor,
                                                            itemSetName,
                                                            tabName);
    }

  // And pad with spacers if necessary.
  if (numLPMs_ % numColumns)
    {
      size_t const numSpacerColumns = numColumns - (numLPMs_ % numColumns);
      webServer.registerSpacer("dummy", "", monitor, "", tabName, numSpacerColumns);
    }
}

void
tcds::pm::TTSInfoSpaceHandler::createTTSChannel(tcds::utils::InfoSpaceHandler* const handler,
                                                std::string const sourceLabel,
                                                std::string const sourceType,
                                                unsigned int const pmNumber,
                                                unsigned int const idNumber,
                                                std::string const regName)
{
  // A human-readable label for this TTS channel.
  handler->createString("tts_source_label",
                        sourceLabel,
                        "",
                        tcds::utils::InfoSpaceItem::NOUPDATE);

  // An identifier string indicating the source type of this TTS
  // channel.
  handler->createString("tts_source_type",
                        sourceType,
                        "",
                        tcds::utils::InfoSpaceItem::NOUPDATE);

  // The source (L)PM and ICI/APVE number of the source of this TTS
  // channel.
  handler->createUInt32("tts_source_pm_number",
                        pmNumber,
                        "",
                        tcds::utils::InfoSpaceItem::NOUPDATE);

  // The source ICI/APVE number of the source of this TTS channel.
  handler->createUInt32("tts_source_id_number",
                        idNumber,
                        "",
                        tcds::utils::InfoSpaceItem::NOUPDATE);

  // The actual TTS value for this TTS channel.
  handler->createUInt32("tts_source_value",
                        tcds::definitions::TTS_STATE_UNKNOWN_TCDS,
                        "tts_state",
                        tcds::utils::InfoSpaceItem::HW32,
                        false,
                        regName);
}
