#include "tcds/pm/SequenceRAMContents.h"

#include <cstddef>
#include <iomanip>
#include <sstream>

#include "tcds/utils/Definitions.h"
#include "tcds/utils/Utils.h"

tcds::pm::SequenceRAMContents::SequenceRAMContents(std::vector<uint32_t> const dataIn) :
  rawData_(dataIn)
{
  size_t numEntries = dataIn.size();
  entries_.reserve(numEntries);
  entries_.clear();
  for (std::vector<uint32_t>::const_iterator it = dataIn.begin();
       it < dataIn.end();
       ++it)
    {
      entries_.push_back(SequenceRAMEntry(*it));
    }
}

std::string
tcds::pm::SequenceRAMContents::getJSONString() const
{
  std::stringstream res;

  res << "[";
  size_t index = 0;
  for (std::vector<SequenceRAMEntry>::const_iterator it = entries_.begin();
       it != entries_.end();
       ++it)
    {
      res << "{";

      // The entry number.
      res << "\"Entry\": \"" << std::dec << index << "\"";
      res << ", ";

      // The raw data word.
      res << "\"RawData\": \"0x"
          << std::hex << std::setw(8) << std::setfill('0') << it->rawData() << "\"";
      res << ", ";

      // The command.
      std::string commandStr = "Unknown";
      if (it->isEndOfSequence())
        {
          commandStr = "End-of-sequence";
        }
      else
        {
          if (it->isFireL1A())
            {
              commandStr = "Fire L1A";
            }
          else if (it->isFireBgo())
            {
              uint8_t const bgoNumber = it->bgoNumber();
              commandStr = "Fire " +
                tcds::utils::formatBgoNameString(static_cast<tcds::definitions::BGO_NUM>(bgoNumber));
            }
          else if (it->isSwitchToSequenceTriggerMode())
            {
              commandStr = "Arm sequence-trigger mode (and switch at next TestEnable B-go)";
            }
          else if (it->isSwitchToNormalTriggerMode())
            {
              commandStr = "Switch to normal trigger mode";
            }
          else if (it->isPause())
            {
              commandStr = "Pause";
            }
          else if (it->isUnpause())
            {
              commandStr = "Unpause";
            }
        }
      uint16_t const delay = it->delay();
      if (delay > 0)
        {
          std::stringstream tmp;
          tmp << ", wait for " << delay << " BC0";
          if (delay > 1)
            {
              tmp << "s";
            }
          commandStr += tmp.str();
        }

      res << "\"Command\": \"" << commandStr << "\"";

      res << "}";

      if (it->isEndOfSequence())
        {
          // Done with this sequence.
          break;
        }

      if (it != (entries_.end() - 1))
        {
          res << ", ";
        }
      ++index;
    }
  res << "]";

  return res.str();
}
