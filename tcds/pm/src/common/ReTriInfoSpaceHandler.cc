#include "tcds/pm/ReTriInfoSpaceHandler.h"

#include <string>

#include "tcds/utils/Monitor.h"
#include "tcds/utils/WebServer.h"

tcds::pm::ReTriInfoSpaceHandler::ReTriInfoSpaceHandler(xdaq::Application& xdaqApp,
                                                       tcds::utils::InfoSpaceUpdater* updater) :
  InfoSpaceHandler(xdaqApp, "tcds-pm-retri-info", updater)
{
  // The overall ReTri enabled flag.
  createBool("main.inselect.retri_enable", false, "enabled/disabled");

  // The ReTri configuration settings.
  createUInt32("retri.config.veto_delta");
  createUInt32("retri.config.veto_threshold");
  createUInt32("retri.config.veto_duration");
  createBool("retri.config.veto_range_enable", false, "enabled/disabled");
  createUInt32("retri.config.veto_range_min");
  createUInt32("retri.config.veto_range_max");
}

tcds::pm::ReTriInfoSpaceHandler::~ReTriInfoSpaceHandler()
{
}

void
tcds::pm::ReTriInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  monitor.newItemSet("itemset-retri");
  monitor.addItem("itemset-retri",
                  "main.inselect.retri_enable",
                  "ReTri trigger suppression",
                  this);
  monitor.addItem("itemset-retri",
                  "retri.config.veto_delta",
                  "Veto delta (BX)",
                  this);
  monitor.addItem("itemset-retri",
                  "retri.config.veto_threshold",
                  "Veto threshold",
                  this);
  monitor.addItem("itemset-retri",
                  "retri.config.veto_duration",
                  "Veto duration (BX)",
                  this);
  monitor.addItem("itemset-retri",
                  "retri.config.veto_range_enable",
                  "Trigger separation window",
                  this);
  monitor.addItem("itemset-retri",
                  "retri.config.veto_range_min",
                  "Trigger separation window lower limit (BX)",
                  this);
  monitor.addItem("itemset-retri",
                  "retri.config.veto_range_max",
                  "Trigger separation window upper limit (BX)",
                  this);
}

void
tcds::pm::ReTriInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                               tcds::utils::Monitor& monitor,
                                                               std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "ReTri" : forceTabName;

  webServer.registerTab(tabName,
                        "ReTri information",
                        4);
  webServer.registerTable("ReTri settings",
                          "Overall ReTri information",
                          monitor,
                          "itemset-retri",
                          tabName);
}
