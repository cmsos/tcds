#include "tcds/pm/Utils.h"

#include <cassert>

#include "toolbox/string.h"
#include "xcept/Exception.h"

#include "tcds/exception/Exception.h"
#include "tcds/pm/Definitions.h"
#include "tcds/utils/Utils.h"

std::string
tcds::pm::pmStateToString(tcds::definitions::PM_STATE const pmState)
{
  std::string res;

  switch (pmState)
    {
    case tcds::definitions::PM_STATE_UNKNOWN:
      res = "unknown";
      break;
    case tcds::definitions::PM_STATE_STOPPED:
      res = "stopped";
      break;
    case tcds::definitions::PM_STATE_RUNNING:
      res = "running";
      break;
    case tcds::definitions::PM_STATE_PAUSED_BY_SW:
      res = "paused by software";
      break;
    case tcds::definitions::PM_STATE_PAUSED_BY_FW:
      res = "paused by firmware sequence";
      break;
    case tcds::definitions::PM_STATE_BLOCKED_BY_FW:
      res = "triggers blocked by firmware sequence";
      break;
    case tcds::definitions::PM_STATE_BLOCKED_BY_DAQ_BACKPRESSURE:
      res = "triggers blocked by DAQ backpressure";
      break;
    case tcds::definitions::PM_STATE_BLOCKED_BY_RETRI:
      res = "triggers blocked by ReTri";
      break;
    case tcds::definitions::PM_STATE_BLOCKED_BY_APVE:
      res = "triggers blocked by PM APVE";
      break;
    case tcds::definitions::PM_STATE_BLOCKED_BY_BX_MASK:
      res = "triggers blocked by BX-mask";
      break;
    case tcds::definitions::PM_STATE_BLOCKED_BY_TTS:
      res = "triggers blocked by TTS";
      break;
    default:
      // ASSERT ASSERT ASSERT
      assert (false);
      // ASSERT ASSERT ASSERT end
      break;
    }

  return res;
}

std::string
tcds::pm::daqLinkModeToString(tcds::definitions::DAQ_LINK_MODE const mode)
{
  std::string res;

  switch (mode)
    {
    case tcds::definitions::DAQ_LINK_MODE_TEST:
      res = "test mode";
      break;
    case tcds::definitions::DAQ_LINK_MODE_FED:
      res = "real-FED mode";
      break;
    default:
      // ASSERT ASSERT ASSERT
      assert (false);
      // ASSERT ASSERT ASSERT end
      break;
    }

  return res;
}

std::string
tcds::pm::formatICILabel(bool const isCPMMode,
                         unsigned int const lpmNumber,
                         unsigned int const iciNumber,
                         tcds::utils::ConfigurationInfoSpaceHandler const& cfgInfoSpace,
                         bool const labelForExternal)
{
  std::string res;

  if (isCPMMode)
    {
      res = tcds::utils::formatICILabel(lpmNumber, iciNumber, cfgInfoSpace, labelForExternal);
    }
  else
    {
      res = tcds::utils::formatICILabel(0, iciNumber, cfgInfoSpace, labelForExternal);
    }

  return res;
}

std::string
tcds::pm::formatAPVELabel(bool const isCPMMode,
                          unsigned int const lpmNumber,
                          unsigned int const apveNumber,
                          tcds::utils::ConfigurationInfoSpaceHandler const& cfgInfoSpace,
                          bool const labelForExternal)
{
  std::string res;

  if (isCPMMode)
    {
      res = tcds::utils::formatAPVELabel(lpmNumber, apveNumber, cfgInfoSpace, labelForExternal);
    }
  else
    {
      res = tcds::utils::formatAPVELabel(0, apveNumber, cfgInfoSpace, labelForExternal);
    }

  return res;
}

std::vector<tcds::definitions::DEADTIME_SOURCE>
tcds::pm::deadtimeSourceList()
{
  // This method is only really useful to make sure that the same
  // order of deadtime sources is used everywhere.
  std::vector<tcds::definitions::DEADTIME_SOURCE> res;

  res.push_back(tcds::definitions::DEADTIME_SOURCE_TTS);
  res.push_back(tcds::definitions::DEADTIME_SOURCE_TRIGGER_RULES);
  res.push_back(tcds::definitions::DEADTIME_SOURCE_BUNCH_MASK_VETO);
  res.push_back(tcds::definitions::DEADTIME_SOURCE_RETRI);
  res.push_back(tcds::definitions::DEADTIME_SOURCE_APVE);
  res.push_back(tcds::definitions::DEADTIME_SOURCE_DAQ_BACKPRESSURE);
  res.push_back(tcds::definitions::DEADTIME_SOURCE_CALIBRATION);
  res.push_back(tcds::definitions::DEADTIME_SOURCE_SW_PAUSE);
  res.push_back(tcds::definitions::DEADTIME_SOURCE_FW_PAUSE);

  return res;
}

std::map<tcds::definitions::DEADTIME_SOURCE, std::string>
tcds::pm::deadtimeSourceRegNameMap()
{
  std::map<tcds::definitions::DEADTIME_SOURCE, std::string> res;

  res[tcds::definitions::DEADTIME_SOURCE_TTS] = "tts";
  res[tcds::definitions::DEADTIME_SOURCE_TRIGGER_RULES] = "trigger_rules";
  res[tcds::definitions::DEADTIME_SOURCE_BUNCH_MASK_VETO] = "bunch_mask_veto";
  res[tcds::definitions::DEADTIME_SOURCE_RETRI] = "retri";
  res[tcds::definitions::DEADTIME_SOURCE_APVE] = "apve";
  res[tcds::definitions::DEADTIME_SOURCE_DAQ_BACKPRESSURE] = "daq_backpressure";
  res[tcds::definitions::DEADTIME_SOURCE_CALIBRATION] = "calibration";
  res[tcds::definitions::DEADTIME_SOURCE_SW_PAUSE] = "sw_pause";
  res[tcds::definitions::DEADTIME_SOURCE_FW_PAUSE] = "fw_pause";

  return res;
}

std::map<tcds::definitions::DEADTIME_SOURCE, std::string>
tcds::pm::deadtimeSourceTitleMap(bool const isShort)
{
  std::map<tcds::definitions::DEADTIME_SOURCE, std::string> res;

  if (isShort)
    {
      res[tcds::definitions::DEADTIME_SOURCE_TTS] = "TTS";
      res[tcds::definitions::DEADTIME_SOURCE_TRIGGER_RULES] = "Trigger rules";
      res[tcds::definitions::DEADTIME_SOURCE_BUNCH_MASK_VETO] = "Bunch-mask";
      res[tcds::definitions::DEADTIME_SOURCE_RETRI] = "ReTri";
      res[tcds::definitions::DEADTIME_SOURCE_APVE] = "PM APVE";
      res[tcds::definitions::DEADTIME_SOURCE_DAQ_BACKPRESSURE] = "DAQ backpressure";
      res[tcds::definitions::DEADTIME_SOURCE_CALIBRATION] = "Calibration sequence";
      res[tcds::definitions::DEADTIME_SOURCE_SW_PAUSE] = "Software pauses";
      res[tcds::definitions::DEADTIME_SOURCE_FW_PAUSE] = "Firmware pauses";
    }
  else
    {
      res[tcds::definitions::DEADTIME_SOURCE_TTS] = "TTS";
      res[tcds::definitions::DEADTIME_SOURCE_TRIGGER_RULES] = "trigger rules";
      res[tcds::definitions::DEADTIME_SOURCE_BUNCH_MASK_VETO] = "the bunch-mask trigger veto";
      res[tcds::definitions::DEADTIME_SOURCE_RETRI] = "the ReTri trigger veto";
      res[tcds::definitions::DEADTIME_SOURCE_APVE] = "the PM APVE trigger veto";
      res[tcds::definitions::DEADTIME_SOURCE_DAQ_BACKPRESSURE] = "DAQ backpressure to the PM";
      res[tcds::definitions::DEADTIME_SOURCE_CALIBRATION] = "the calibration sequence";
      res[tcds::definitions::DEADTIME_SOURCE_SW_PAUSE] = "software pauses";
      res[tcds::definitions::DEADTIME_SOURCE_FW_PAUSE] = "firmware pauses";
    }

  return res;
}

std::map<tcds::definitions::DEADTIME_SOURCE, std::string>
tcds::pm::deadtimeSourceDescMap()
{
  std::map<tcds::definitions::DEADTIME_SOURCE, std::string> res;

  res[tcds::definitions::DEADTIME_SOURCE_TTS] = "any partition being not TTS-READY";
  res[tcds::definitions::DEADTIME_SOURCE_TRIGGER_RULES] = "trigger rules suppressing triggers";
  res[tcds::definitions::DEADTIME_SOURCE_BUNCH_MASK_VETO] = "bunch crossings masked to suppress triggers";
  res[tcds::definitions::DEADTIME_SOURCE_RETRI] = "the Resonant-Trigger protection suppressing triggers";
  res[tcds::definitions::DEADTIME_SOURCE_APVE] = "the APVE in the Partition Manager suppressing triggers";
  res[tcds::definitions::DEADTIME_SOURCE_DAQ_BACKPRESSURE] = "DAQ backpressure to the Partition Manager";
  res[tcds::definitions::DEADTIME_SOURCE_CALIBRATION] = "the calibration sequence";
  res[tcds::definitions::DEADTIME_SOURCE_SW_PAUSE] = "software pauses, like TTCResync/TTCHardReset";
  res[tcds::definitions::DEADTIME_SOURCE_FW_PAUSE] = "firmware sequences, like Resync/HardReset";

  return res;
}

std::string
tcds::pm::deadtimeSourceToRegName(tcds::definitions::DEADTIME_SOURCE const src)
{
  std::string const res = deadtimeSourceRegNameMap()[src];
  return res;
}

std::string
tcds::pm::deadtimeSourceToTitle(tcds::definitions::DEADTIME_SOURCE const src,
                                bool const isShort)
{
  std::string const res = deadtimeSourceTitleMap(isShort)[src];
  return res;
}

std::string
tcds::pm::deadtimeSourceToDesc(tcds::definitions::DEADTIME_SOURCE const src)
{
  std::string const res = deadtimeSourceDescMap()[src];
  return res;
}

void
tcds::pm::verifyCyclicGeneratorNumber(unsigned int const genNumber)
{
  if ((genNumber < tcds::definitions::kPMCyclicGenNumMin) ||
      (genNumber > tcds::definitions::kPMCyclicGenNumMax))
    {
      std::string const msg = toolbox::toString("Generator number %d falls outside "
                                                "the allowed range of [%d, %d].",
                                                genNumber,
                                                tcds::definitions::kPMCyclicGenNumMin,
                                                tcds::definitions::kPMCyclicGenNumMax);
      XCEPT_RAISE(tcds::exception::ValueError, msg.c_str());
    }
}
