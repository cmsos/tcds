#include "tcds/pm/WebTableSequenceRAM.h"

#include <cassert>
#include <sstream>
#include <string>

#include "tcds/utils/Monitor.h"

tcds::pm::WebTableSequenceRAM::WebTableSequenceRAM(std::string const& name,
                                                   std::string const& description,
                                                   tcds::utils::Monitor const& monitor,
                                                   std::string const& itemSetName,
                                                   std::string const& tabName,
                                                   size_t const colSpan) :
  tcds::utils::WebObject(name, description, monitor, itemSetName, tabName, colSpan)
{
}

std::string
tcds::pm::WebTableSequenceRAM::getHTMLString() const
{
  // NOTE: This has been written to work with the new XDAQ12-style
  // HyperDAQ tabs and doT.js.

  std::stringstream res;

  res << "<div class=\"tcds-item-table-wrapper\">"
      << "\n";

  res << "<p class=\"tcds-item-table-title\">"
      << getName()
      << "</p>";
  res << "\n";

  res << "<p class=\"tcds-item-table-description\">"
      << getDescription()
      << "</p>";
  res << "\n";

  tcds::utils::Monitor::StringPairVector items =
    monitor_.getFormattedItemSet(itemSetName_);
  // ASSERT ASSERT ASSERT
  assert (items.size() == 1);
  // ASSERT ASSERT ASSERT end

  std::string const itemName = items.at(0).first;
  std::string const tmp = "[\"" + itemSetName_ + "\"][\"" + itemName + "\"]";

  std::string const invalidVal = tcds::utils::WebObject::kStringInvalidItem;
  res << "<script type=\"text/x-dot-template\">";
  // Case 0: invalid item ('-').
  res << "{{? it" << tmp << " == '" << invalidVal << "'}}"
      << "<span>" << invalidVal << "</span>"
      << "\n";
  // Case 1: a (hopefully non-empty) array.
  res << "{{??}}"
      << "\n"
      << "<div class=\"scroll-table-wrapper-outer\">"
      << "\n"
      << "<div class=\"scroll-table-wrapper-inner\">"
      << "\n"
      << "<div class=\"scroll-table-header-background\"></div>"
      << "\n"
      << "<table class=\"xdaq-table tcds-item-table\">"
      << "\n"
      << "<thead>"
      << "<tr>"
      << "<th><div class=\"th-inner\">Entry #</div></th>"
      << "<th><div class=\"th-inner\">Raw data</div></th>"
      << "<th><div class=\"th-inner\">Command</div></th>"
      << "</tr>"
      << "</thead>"
      << "\n"
      << "<tbody>"
      << "\n"
      << "{{~it" << tmp << " :value:index}}"
      << "\n"
      << "<tr>"
      << "\n"
      << "<td class=\"tcds-item-value\">{{=value[\"Entry\"]}}</td>"
      << "<td class=\"tcds-item-value\">{{=value[\"RawData\"]}}</td>"
      << "<td class=\"tcds-item-value\">{{=value[\"Command\"]}}</td>"
      << "\n"
      << "</tr>"
      << "\n"
      << "{{~}}"
      << "\n"
      << "</tbody>"
      << "\n"
      << "</table>"
      << "\n"
      << "</div>"
      << "\n"
      << "</div>"
      << "\n";
  res << "{{?}}"
      << "\n";
  res << "</script>"
      << "\n"
      << "<div class=\"target\">" << kDefaultValueString << "</div>"
      << "\n";

  res << "</div>"
      << "\n";

  return res.str();
}
