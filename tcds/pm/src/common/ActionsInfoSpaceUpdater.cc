#include "tcds/pm/ActionsInfoSpaceUpdater.h"

#include <stdint.h>
#include <string>

#include "toolbox/string.h"

#include "tcds/hwlayer/DeviceBase.h"
#include "tcds/hwutilstca/Definitions.h"
#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/InfoSpaceItem.h"

tcds::pm::ActionsInfoSpaceUpdater::ActionsInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                                           tcds::hwlayer::DeviceBase const& hw) :
  tcds::utils::HwInfoSpaceUpdaterBase(xdaqApp, hw)
{
}

tcds::pm::ActionsInfoSpaceUpdater::~ActionsInfoSpaceUpdater()
{
}

bool
tcds::pm::ActionsInfoSpaceUpdater::updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                                       tcds::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  tcds::hwlayer::DeviceBase const& hw = getHw();
  bool updated = false;
  if (hw.isReadyForUse())
    {
      std::string name = item.name();
      tcds::utils::InfoSpaceItem::UpdateType updateType = item.updateType();
      if (updateType == tcds::utils::InfoSpaceItem::PROCESS)
        {
          // The 'PROCESS' update type means that there is something
          // special to the variable. Figure out what to do based on the
          // variable name.
          if (toolbox::endsWith(name, ".mode"))
            {
              // TODO TODO TODO
              // This is a wee bit hacky. Would be nice to clean this
              // up a little.
              std::string const baseName = name.substr(0, name.size() - std::string(".mode").size());
              bool const isL1A = (hw.readRegister(baseName + ".l1a") == 0x1);
              bool const isBgo = (hw.readRegister(baseName + ".bgo_command") == 0x1);
              bool const isSequence = (hw.readRegister(baseName + ".bgo_sequence_command") == 0x1);
              tcds::definitions::CYCLIC_GEN_MODE cyclicGenMode = tcds::definitions::CYCLIC_GEN_OFF;
              if ((!!isL1A + !!isBgo + !!isSequence) > 1)
                {
                  cyclicGenMode = tcds::definitions::CYCLIC_GEN_MISCONFIGURED;
                }
              else
                {
                  if (isL1A)
                    {
                      cyclicGenMode = tcds::definitions::CYCLIC_GEN_L1A;
                    }
                  else if (isBgo)
                    {
                      cyclicGenMode = tcds::definitions::CYCLIC_GEN_BGO;
                    }
                  else if (isSequence)
                    {
                      cyclicGenMode = tcds::definitions::CYCLIC_GEN_SEQUENCE;
                    }
                }
              uint32_t const newVal = cyclicGenMode;
              // TODO TODO TODO end
              infoSpaceHandler->setUInt32(name, newVal);
              updated = true;
            }
        }
      if (!updated)
        {
          updated = tcds::utils::HwInfoSpaceUpdaterBase::updateInfoSpaceItem(item, infoSpaceHandler);
        }
    }

  if (updated)
    {
      item.setValid();
    }
  else
    {
      item.setInvalid();
    }

  return updated;
}

// tcds::pm::TCADevicePMCommonBase const&
// tcds::pm::ActionsInfoSpaceUpdater::getHw() const
// {
//   return hw_;
// }
