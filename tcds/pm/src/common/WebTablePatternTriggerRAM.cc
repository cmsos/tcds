#include "tcds/pm/WebTablePatternTriggerRAM.h"

#include <sstream>
#include <string>

#include "toolbox/string.h"

#include "tcds/utils/Monitor.h"
#include "tcds/utils/WebObject.h"

tcds::pm::WebTablePatternTriggerRAM::WebTablePatternTriggerRAM(std::string const& name,
                                                               std::string const& description,
                                                               tcds::utils::Monitor const& monitor,
                                                               std::string const& itemSetName,
                                                               std::string const& tabName,
                                                               size_t const colSpan) :
  tcds::utils::WebObject(name, description, monitor, itemSetName, tabName, colSpan)
{
}

std::string
tcds::pm::WebTablePatternTriggerRAM::getHTMLString() const
{
  // NOTE: This has been written to work with the new XDAQ12-style
  // HyperDAQ tabs and doT.js.

  std::stringstream res;

  res << "<div class=\"tcds-item-table-wrapper\">"
      << "\n";

  res << "<p class=\"tcds-item-table-title\">"
      << getName()
      << "</p>";
  res << "\n";

  res << "<p class=\"tcds-item-table-description\">"
      << getDescription()
      << "</p>";
  res << "\n";

  //----------

  res << "<table class=\"tcds-item-table xdaq-table\">"
      << "\n";

  res << "<tbody>";
  res << "\n";

  tcds::utils::Monitor::StringPairVector items =
    monitor_.getItemSetDocStrings(itemSetName_);
  tcds::utils::Monitor::StringPairVector::const_iterator iter;
  for (iter = items.begin(); iter != items.end() - 1; ++iter)
    {
      std::string const itemTitle = iter->first;
      std::string const tmp = "[\"" + toolbox::jsonquote(itemSetName_) + "\"][\"" + toolbox::jsonquote(iter->first) + "\"]";
      std::string const docString = iter->second;

      res << "<tr";
      if (!docString.empty())
        {
          res << " class=\"xdaq-tooltip\"";
        }
      res << ">";
      res << "<td class=\"tcds-item-header\">"
          << "<div>" << itemTitle << "</div>";
      if (!docString.empty())
        {
          res << "<span class=\"xdaq-tooltip-msg\">" << docString << "</span>";
        }
      res << "</td>"
          << "<td class=\"tcds-item-value\">"
          << "<script type=\"text/x-dot-template\">"
          << "{{=it" << tmp << "}}"
          << "</script>"
          << "<div class=\"target\">" << kDefaultValueString << "</div>"
          << "</td>"
          << "</tr>"
          << "\n";
    }

  res << "</tbody>";
  res << "\n";

  res << "</table>";
  res << "\n";

  //----------

  res << "<br/>\n";

  //----------

  items = monitor_.getFormattedItemSet(itemSetName_);

  std::string const itemName = items.at(items.size() - 1).first;
  std::string const tmp = "[\"" + itemSetName_ + "\"][\"" + itemName + "\"]";

  std::string const invalidVal = tcds::utils::WebObject::kStringInvalidItem;
  res << "<script type=\"text/x-dot-template\">";
  // Case 0: invalid item ('-').
  res << "{{? it" << tmp << " == '" << invalidVal << "'}}"
      << "<span>" << invalidVal << "</span>"
      << "\n";
  // Case 1: a (hopefully non-empty) array.
  res << "{{??}}"
      << "\n"
      << "<div class=\"scroll-table-wrapper-outer\">"
      << "\n"
      << "<div class=\"scroll-table-wrapper-inner\">"
      << "\n"
      << "<div class=\"scroll-table-header-background\"></div>"
      << "\n"
      << "<table class=\"xdaq-table tcds-item-table\">"
      << "\n"
      << "<thead>"
      << "<tr>"
      << "\n"
      << "<th><div class=\"th-inner\">Entry #</div></th>"
      << "<th><div class=\"th-inner\">Command</div></th>"
      << "\n"
      << "</tr>"
      << "</thead>"
      << "\n"
      << "<tbody>"
      << "\n"
      << "{{~it" << tmp << " :value:index}}"
      << "\n"
      << "<tr>"
      << "\n"
      << "<td class=\"tcds-item-value\">{{=value[\"Entry\"]}}</td>"
      << "<td class=\"tcds-item-value\">{{=value[\"Command\"]}}</td>"
      << "\n"
      << "</tr>"
      << "\n"
      << "{{~}}"
      << "\n"
      << "</tbody>"
      << "\n"
      << "</table>"
      << "\n"
      << "</div>"
      << "\n"
      << "</div>"
      << "\n";
  res << "{{?}}"
      << "\n";
  res << "</script>"
      << "\n"
      << "<div class=\"target\">" << kDefaultValueString << "</div>"
      << "\n";

  res << "</div>";
  res << "\n";

  return res.str();
}
