#include "tcds/pm/ActionsInfoSpaceHandler.h"

#include <stdint.h>

#include "toolbox/string.h"

#include "tcds/hwutilstca/Definitions.h"
#include "tcds/hwutilstca/Utils.h"
#include "tcds/pm/Definitions.h"
#include "tcds/pm/WebTableTTSActionTriggers.h"
#include "tcds/utils/Definitions.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/Utils.h"
#include "tcds/utils/WebServer.h"
#include "tcds/utils/XDAQAppBase.h"

namespace tcds {
  namespace utils {
    class ConfigurationInfoSpaceHandler;
  }
}

tcds::pm::ActionsInfoSpaceHandler::ActionsInfoSpaceHandler(tcds::utils::XDAQAppBase& xdaqApp,
                                                           tcds::utils::InfoSpaceUpdater* updater,
                                                           bool const cpmMode) :
  InfoSpaceHandler(xdaqApp, "tcds-pm-actions", updater),
  isCPMMode_(cpmMode)
{
  // The 'general settings'.
  // createUInt32("tts_actions.config.recovery_time");
  // createUInt32("tts_actions.config.max_repeat_count");
  createBool("tts_actions.auto_activate.enabled");

  //----------

  // The number of LPMs to include in the monitoring depends on
  // whether we're in LPM- or in CPM mode.
  numLPMs_ = 1;
  if (isCPMMode_)
    {
      numLPMs_ = tcds::definitions::kNumLPMsPerCPM;
    }

  //----------

  // Create the trigger condition items. For each generator: one TTS
  // value for each of the partitions.
  for (unsigned int genNum = 0; genNum < tcds::definitions::kNumActionGensPerPM; ++genNum)
    {
      for (unsigned int lpmNum = 1; lpmNum <= numLPMs_; ++lpmNum)
        {
          for (unsigned int iciNum = 1; iciNum <= tcds::definitions::kNumICIsPerLPM; ++iciNum)
            {
              std::string const name =
                toolbox::toString("tts_actions.generator%d.trigger_tts_values.lpm%d.ici%d",
                                  genNum, lpmNum, iciNum);
              createUInt32(name, 0, "tts_trigger");
            }
          for (unsigned int apveNum = 1; apveNum <= tcds::definitions::kNumAPVEsPerLPM; ++apveNum)
            {
              std::string const name =
                toolbox::toString("tts_actions.generator%d.trigger_tts_values.lpm%d.apve%d",
                                  genNum, lpmNum, apveNum);
              createUInt32(name, 0, "tts_trigger");
            }
        }
    }

  //----------

  // The action configuration, by generator.
  for (unsigned int genNum = 0; genNum < tcds::definitions::kNumActionGensPerPM; ++genNum)
    {
      createUInt32(toolbox::toString("tts_actions.generator%d.action.mode", genNum),
                   0,
                   "",
                   tcds::utils::InfoSpaceItem::PROCESS);
      createBool(toolbox::toString("tts_actions.auto_activate.generator%d", genNum));
      createUInt32(toolbox::toString("tts_actions.generator%d.config.retrigger_holdoff", genNum));
      createUInt32(toolbox::toString("tts_actions.generator%d.config.max_repeat_count", genNum));
      createBool(toolbox::toString("tts_actions.generator%d.action.l1a", genNum));
      createBool(toolbox::toString("tts_actions.generator%d.action.bgo_command", genNum));
      createBool(toolbox::toString("tts_actions.generator%d.action.bgo_sequence_command", genNum));
      createUInt32(toolbox::toString("tts_actions.generator%d.action.id", genNum));
    }

  //----------

  // Status items, by generator.
  for (unsigned int genNum = 0; genNum < tcds::definitions::kNumActionGensPerPM; ++genNum)
    {
      createBool(toolbox::toString("tts_actions.generator%d.status.is_active", genNum));
      createBool(toolbox::toString("tts_actions.generator%d.status.is_in_timeout", genNum));
      createUInt32(toolbox::toString("tts_actions.generator%d.status.request_count", genNum));
    }
}

tcds::pm::ActionsInfoSpaceHandler::~ActionsInfoSpaceHandler()
{
}

tcds::utils::XDAQAppBase&
tcds::pm::ActionsInfoSpaceHandler::getOwnerApplication() const
{
  return static_cast<tcds::utils::XDAQAppBase&>(tcds::utils::InfoSpaceHandler::getOwnerApplication());
}

void
tcds::pm::ActionsInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  // The 'general settings'.
  monitor.newItemSet("itemset-actions-general");
  // monitor.addItem("itemset-actions-general",
  //                 "tts_actions.config.recovery_time",
  //                 "Recovery time after each action (orbits)",
  //                 this);
  // monitor.addItem("itemset-actions-general",
  //                 "tts_actions.config.max_repeat_count",
  //                 "Maximum number of times to trigger an action before going into time-out",
  //                 this);
  monitor.addItem("itemset-actions-general",
                  "tts_actions.auto_activate.enabled",
                  "Automatically re-arm upon TTS READY",
                  this,
                  "If set to true, all TTS actions (also those that timed out) "
                  "are re-activated upon top-level TTS == READY");

  //----------

  // The trigger conditions, grouped by generator.
  tcds::utils::ConfigurationInfoSpaceHandler const& cfgInfoSpace =
    getOwnerApplication().getConfigurationInfoSpaceHandler();
  for (unsigned int genNum = 0; genNum < tcds::definitions::kNumActionGensPerPM; ++genNum)
    {
      std::string const itemSetName =
        toolbox::toString("itemset-action-generator%d-trigger", genNum);
      monitor.newItemSet(itemSetName);

      for (unsigned int lpmNum = 1; lpmNum <= numLPMs_; ++lpmNum)
        {
          for (unsigned int iciNum = 1; iciNum <= tcds::definitions::kNumICIsPerLPM; ++iciNum)
            {
              std::string const itemName =
                toolbox::toString("tts_actions.generator%d.trigger_tts_values.lpm%d.ici%d",
                                  genNum, lpmNum, iciNum);
              std::string itemDesc;
              if (isCPMMode_)
                {
                  itemDesc = toolbox::toString("%s;%s",
                                               tcds::utils::formatLPMLabel(lpmNum, cfgInfoSpace).c_str(),
                                               tcds::utils::formatICILabel(lpmNum, iciNum, cfgInfoSpace, false).c_str());
                }
              else
                {
                  itemDesc = tcds::utils::formatICILabel(0, iciNum, cfgInfoSpace, false).c_str();
                }
              monitor.addItem(itemSetName, itemName, itemDesc, this);
            }
          for (unsigned int apveNum = 1; apveNum <= tcds::definitions::kNumAPVEsPerLPM; ++apveNum)
            {
              std::string const itemName =
                toolbox::toString("tts_actions.generator%d.trigger_tts_values.lpm%d.apve%d",
                                  genNum, lpmNum, apveNum);
              std::string itemDesc;
              if (isCPMMode_)
                {
                  itemDesc = toolbox::toString("%s;%s",
                                               tcds::utils::formatLPMLabel(lpmNum, cfgInfoSpace).c_str(),
                                               tcds::utils::formatAPVELabel(lpmNum, apveNum, cfgInfoSpace, false).c_str());
                }
              else
                {
                  itemDesc = tcds::utils::formatAPVELabel(0, apveNum, cfgInfoSpace, false).c_str();
                }
              monitor.addItem(itemSetName, itemName, itemDesc, this);
            }
        }
    }

  //----------

  // The action configuration, by generator.
  for (unsigned int genNum = 0; genNum < tcds::definitions::kNumActionGensPerPM; ++genNum)
    {
      std::string const itemSetName =
        toolbox::toString("itemset-action-generator%d-action", genNum);
      monitor.newItemSet(itemSetName);

      std::string itemName = toolbox::toString("tts_actions.generator%d.action.mode", genNum);
      monitor.addItem(itemSetName, itemName, "Mode", this);
      itemName = toolbox::toString("tts_actions.generator%d.config.retrigger_holdoff", genNum);
      monitor.addItem(itemSetName, itemName, "Re-trigger hold-off (orbits)", this);
      itemName = toolbox::toString("tts_actions.generator%d.config.max_repeat_count", genNum);
      monitor.addItem(itemSetName, itemName, "Maximum number of times to trigger an action before going into time-out", this);
      itemName = toolbox::toString("tts_actions.auto_activate.generator%d", genNum);
      monitor.addItem(itemSetName, itemName, "Auto-activate", this);
    }

  //----------

  // Status items, by generator.
  for (unsigned int genNum = 0; genNum < tcds::definitions::kNumActionGensPerPM; ++genNum)
    {
      std::string const itemSetName =
        toolbox::toString("itemset-action-generator%d-status", genNum);
      monitor.newItemSet(itemSetName);
      monitor.addItem(itemSetName,
                      toolbox::toString("tts_actions.generator%d.status.is_active", genNum),
                      "Active",
                      this);
      monitor.addItem(itemSetName,
                      toolbox::toString("tts_actions.generator%d.status.is_in_timeout", genNum),
                      "Timed out",
                      this);
      monitor.addItem(itemSetName,
                      toolbox::toString("tts_actions.generator%d.status.request_count", genNum),
                      "Request count",
                      this);
    }
}

void
tcds::pm::ActionsInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                 tcds::utils::Monitor& monitor,
                                                                 std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "Actions" : forceTabName;

  webServer.registerTab(tabName,
                        "TTS-triggered actions configuration",
                        3);

  //----------

  // The 'general settings' table.
  webServer.registerTable("General settings",
                          "Settings applying to all TTS-triggered action generators",
                          monitor,
                          "itemset-actions-general",
                          tabName,
                          3);

  //----------

  // Register all tables, for all generators.
  std::string itemSetTitle = "";
  std::string itemSetName = "";
  for (unsigned int genNum = 0; genNum < tcds::definitions::kNumActionGensPerPM; ++genNum)
    {
      // Trigger conditions.
      // NOTE: The table type/layout depends on whether we're in LPM- or CPM mode.
      itemSetTitle = toolbox::toString("Generator %d trigger conditions", genNum);
      itemSetName = toolbox::toString("itemset-action-generator%d-trigger", genNum);
      std::string const desc = "TTS-triggered action generator trigger conditions";
      if (isCPMMode_)
        {
          webServer.registerWebObject<tcds::pm::WebTableTTSActionTriggers>(itemSetTitle,
                                                                           desc,
                                                                           monitor,
                                                                           itemSetName,
                                                                           tabName);
        }
      else
        {
          webServer.registerTable(itemSetTitle,
                                  desc,
                                  monitor,
                                  itemSetName,
                                  tabName);
        }

      // Action configuration.
      itemSetTitle = toolbox::toString("Generator %d action configuration", genNum);
      itemSetName = toolbox::toString("itemset-action-generator%d-action", genNum);
      webServer.registerTable(itemSetTitle,
                              "Action description",
                              monitor,
                              itemSetName,
                              tabName);

      // Status items.
      itemSetTitle = toolbox::toString("Generator %d status", genNum);
      itemSetName = toolbox::toString("itemset-action-generator%d-status", genNum);
      webServer.registerTable(itemSetTitle,
                              "Generator status items",
                              monitor,
                              itemSetName,
                              tabName);
    }
}

std::string
tcds::pm::ActionsInfoSpaceHandler::formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const
{
  std::string res = tcds::utils::escapeAsJSONString(tcds::utils::InfoSpaceHandler::kInvalidItemString);
  if (item->isValid())
    {
      std::string name = item->name();
      if (toolbox::endsWith(name, ".mode"))
        {
          uint32_t const value = getUInt32(name);
          tcds::definitions::CYCLIC_GEN_MODE const valueEnum =
            static_cast<tcds::definitions::CYCLIC_GEN_MODE>(value);
          std::string tmp = "";
          if ((valueEnum == tcds::definitions::CYCLIC_GEN_BGO) ||
              (valueEnum == tcds::definitions::CYCLIC_GEN_SEQUENCE))
            {
              std::string const baseName = name.substr(0, name.size() - std::string(".mode").size());
              uint32_t const id = getUInt32(baseName + ".id");
              if (valueEnum == tcds::definitions::CYCLIC_GEN_BGO)
                {
                  std::string const bgoNameString =
                    tcds::utils::formatBgoNameString(static_cast<tcds::definitions::BGO_NUM>(id));
                  tmp = "fire " + bgoNameString;
                }
              else if (valueEnum == tcds::definitions::CYCLIC_GEN_SEQUENCE)
                {
                  std::string const sequenceNameString =
                    tcds::utils::formatSequenceNameString(static_cast<tcds::definitions::SEQUENCE_NUM>(id));
                  tmp = "fire " + sequenceNameString;
                }
            }
          else
            {
              tmp = tcds::hwutilstca::cyclicGenModeToString(valueEnum);
            }
          res = tcds::utils::escapeAsJSONString(tmp);
        }
      else
        {
          // For everything else simply call the generic formatter.
          res = InfoSpaceHandler::formatItem(item);
        }
    }
  return res;
}
