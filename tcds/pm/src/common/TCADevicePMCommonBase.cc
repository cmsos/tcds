#include "tcds/pm/TCADevicePMCommonBase.h"

#include <algorithm>
#include <cmath>
#include <cstddef>
#include <unistd.h>
#include <utility>

#include "toolbox/string.h"

#include "tcds/hwlayertca/TCACarrierBase.h"
#include "tcds/pm/Utils.h"
#include "tcds/utils/Utils.h"

tcds::pm::TCADevicePMCommonBase::TCADevicePMCommonBase(std::unique_ptr<tcds::hwlayertca::TCACarrierBase> carrier) :
  tcds::hwlayertca::TCADeviceBase(std::move(carrier))
{
}

tcds::pm::TCADevicePMCommonBase::~TCADevicePMCommonBase()
{
}

void
tcds::pm::TCADevicePMCommonBase::setFEDId(uint32_t const fedId) const
{
  writeRegister("main.daq_link_config.fed_id", fedId);
}

void
tcds::pm::TCADevicePMCommonBase::setRunNumber(uint32_t const runNumber) const
{
  // NOTE: The run number is in principle 64-bits in firmware, but
  // only 32-bits in software. (We only receive 32 bits from
  // RunControl anyway.)
  writeRegister("main.event_record_data.run_number_hi", 0);
  writeRegister("main.event_record_data.run_number_lo", runNumber);
}

void
tcds::pm::TCADevicePMCommonBase::setSoftwareVersion(uint32_t const softwareVersion) const
{
  writeRegister("main.event_record_data.sw_version", softwareVersion);
}

bool
tcds::pm::TCADevicePMCommonBase::isExternalOrbitSelected() const
{
  uint32_t const tmp0 = readRegister("main.inselect.cpm_orbit_select");
  bool const isCPMOrbitSelected = (tmp0 == 1);

  uint32_t const tmp1 = readRegister("main.inselect.ttcmi_orbit_select");
  bool const isTTCMIOrbitSelected = (tmp1 == 1);

  return (isCPMOrbitSelected || isTTCMIOrbitSelected);
}

bool
tcds::pm::TCADevicePMCommonBase::isOrbitSignalOk() const
{
  // Verifies that the orbit signal error counters are stable and not
  // increasing. That should imply that a correct orbit signal is
  // present.

  // NOTE: The orbit signal in question depends on the configuration,
  // and can be various things, like a BC0 from the CPM, or an
  // electrical orbit signal from the TTC-mi.

  // NOTE: In case no external orbit source is selected, the firmware
  // will internally produce an orbit signal based on the
  // bunch-clock. That we cannot check here, and we will assume that
  // it actually works.

  bool res = false;

  if (isExternalOrbitSelected())
    {
      uint32_t const tmp0 =
        readRegister("main.bc_alignment_status.orbit_marker_absent_count");
      uint32_t const tmp1 =
        readRegister("main.bc_alignment_status.system_bc_counter_realignment_count");
      uint32_t const tmp2 =
        readRegister("main.bc_alignment_status.bc_counter_realignment_count");

      ::sleep(1);

      uint32_t const tmp3 =
        readRegister("main.bc_alignment_status.orbit_marker_absent_count");
      uint32_t const tmp4 =
        readRegister("main.bc_alignment_status.system_bc_counter_realignment_count");
      uint32_t const tmp5 =
        readRegister("main.bc_alignment_status.bc_counter_realignment_count");

      res = ((tmp3 == tmp0) && (tmp4 == tmp1) && (tmp5 == tmp2));
    }
  else
    {
      // Apparently we are relying on the internal orbit
      // generator. Let's assume this just works.
      res = true;
    }

  return res;
}

void
tcds::pm::TCADevicePMCommonBase::stop() const
{
  writeRegister("main.resets.internal_bgo_stop", 0x1);
}

void
tcds::pm::TCADevicePMCommonBase::systemPause() const
{
  writeRegister("main.inselect.system_pause", 0x1);
}

void
tcds::pm::TCADevicePMCommonBase::systemUnpause() const
{
  writeRegister("main.inselect.system_pause", 0x0);
}

bool
tcds::pm::TCADevicePMCommonBase::isSystemPaused() const
{
  uint32_t const tmp = readRegister("main.inselect.system_pause");
  return (tmp == 0x1);
}

bool
tcds::pm::TCADevicePMCommonBase::isTTSEnabled() const
{
  uint32_t const tmp = readRegister("main.inselect.tts_enable");
  return (tmp == 0x1);
}

void
tcds::pm::TCADevicePMCommonBase::enableTTSFromICI(unsigned int const lpmNum,
                                                  unsigned int const iciNum) const
{
  switchTTSFromPartition(lpmNum, iciNum, true, true);
}

void
tcds::pm::TCADevicePMCommonBase::disableTTSFromICI(unsigned int const lpmNum,
                                                   unsigned int const iciNum) const
{
  switchTTSFromPartition(lpmNum, iciNum, true, false);
}

void
tcds::pm::TCADevicePMCommonBase::enableTTSFromAPVE(unsigned int const lpmNum,
                                                   unsigned int const apveNum) const
{
  switchTTSFromPartition(lpmNum, apveNum, false, true);
}

void
tcds::pm::TCADevicePMCommonBase::disableTTSFromAPVE(unsigned int const lpmNum,
                                                    unsigned int const apveNum) const
{
  switchTTSFromPartition(lpmNum, apveNum, false, false);
}

bool
tcds::pm::TCADevicePMCommonBase::isL1APausedFW() const
{
  uint32_t const tmp = readRegister("main.pm_status.system_paused_fw");
  return (tmp == 0x1);
}

bool
tcds::pm::TCADevicePMCommonBase::isL1ABlockedFW() const
{
  uint32_t const tmp = readRegister("main.pm_status.l1a_blocked_fw");
  return (tmp == 0x1);
}

bool
tcds::pm::TCADevicePMCommonBase::isL1ABlockedDAQBackpressure() const
{
  uint32_t const tmp = readRegister("tts_spy_array.tts_from_daq_backpressure");
  return isBlockingTTSState(static_cast<tcds::definitions::TTS_STATE>(tmp));
}

bool
tcds::pm::TCADevicePMCommonBase::isL1ABlockedRetri() const
{
  uint32_t const tmp = readRegister("tts_spy_array.tts_from_retri");
  return isBlockingTTSState(static_cast<tcds::definitions::TTS_STATE>(tmp));
}

bool
tcds::pm::TCADevicePMCommonBase::isL1ABlockedApve() const
{
  uint32_t const tmp = readRegister("tts_spy_array.tts_from_ipm_apve");
  return isBlockingTTSState(static_cast<tcds::definitions::TTS_STATE>(tmp));
}

bool
tcds::pm::TCADevicePMCommonBase::isL1ABlockedBXMask() const
{
  uint32_t const tmp = readRegister("tts_spy_array.tts_from_bunch_mask");
  return isBlockingTTSState(static_cast<tcds::definitions::TTS_STATE>(tmp));
}

bool
tcds::pm::TCADevicePMCommonBase::isL1ABlockedTTS() const
{
  bool res = true;
  if (!isTTSEnabled())
    {
      res = false;
    }
  else
    {
      uint32_t const ttsState = readRegister("tts_spy_array.tts_final_or");
      res = isBlockingTTSState(static_cast<tcds::definitions::TTS_STATE>(ttsState));
    }
  return res;
}

bool
tcds::pm::TCADevicePMCommonBase::isBlockingTTSState(tcds::definitions::TTS_STATE const ttsState) const
{
  return ((ttsState != tcds::definitions::TTS_STATE_READY) &&
          (ttsState != tcds::definitions::TTS_STATE_MASKED));
}

bool
tcds::pm::TCADevicePMCommonBase::isRunActive() const
{
  uint32_t const tmp = readRegister("main.pm_status.run_active");
  return (tmp == 0x1);
}

tcds::definitions::PM_STATE
tcds::pm::TCADevicePMCommonBase::getPMState() const
{
  tcds::definitions::PM_STATE res = tcds::definitions::PM_STATE_UNKNOWN;

  if (!isRunActive())
    {
      res = tcds::definitions::PM_STATE_STOPPED;
    }
  else
    {
      if (isSystemPaused())
        {
          res = tcds::definitions::PM_STATE_PAUSED_BY_SW;
        }
      else if (isL1APausedFW())
        {
          res = tcds::definitions::PM_STATE_PAUSED_BY_FW;
        }
      else if (isL1ABlockedFW())
        {
          res = tcds::definitions::PM_STATE_BLOCKED_BY_FW;
        }
      else if (isL1ABlockedDAQBackpressure())
        {
          res = tcds::definitions::PM_STATE_BLOCKED_BY_DAQ_BACKPRESSURE;
        }
      else if (isL1ABlockedRetri())
        {
          res = tcds::definitions::PM_STATE_BLOCKED_BY_RETRI;
        }
      else if (isL1ABlockedApve())
        {
          res = tcds::definitions::PM_STATE_BLOCKED_BY_APVE;
        }
      else if (isL1ABlockedBXMask())
        {
          res = tcds::definitions::PM_STATE_BLOCKED_BY_BX_MASK;
        }
      else if (isL1ABlockedTTS())
        {
          res = tcds::definitions::PM_STATE_BLOCKED_BY_TTS;
        }
      else
        {
          // Not paused, not blocked -> must be running.
          res = tcds::definitions::PM_STATE_RUNNING;
        }
    }

  return res;
}

void
tcds::pm::TCADevicePMCommonBase::enableTrigger() const
{
  std::string const regName = "main.inselect.trigger_enable";
  writeRegister(regName, 0x1);
}

void
tcds::pm::TCADevicePMCommonBase::disableTrigger() const
{
  std::string const regName = "main.inselect.trigger_enable";
  writeRegister(regName, 0x0);
}

void
tcds::pm::TCADevicePMCommonBase::enableCyclicGenerator(unsigned int const genNumber) const
{
  // First check if the number matches an existing generator.
  verifyCyclicGeneratorNumber(genNumber);

  // Now do as we were asked.
  std::string const regName = toolbox::toString("cyclic_generator%d.configuration.enabled",
                                                genNumber);
  writeRegister(regName, 1);
}

void
tcds::pm::TCADevicePMCommonBase::disableCyclicGenerator(unsigned int const genNumber) const
{
  // First check if the number matches an existing generator.
  verifyCyclicGeneratorNumber(genNumber);

  // Now do as we were asked.
  std::string const regName = toolbox::toString("cyclic_generator%d.configuration.enabled",
                                                genNumber);
  writeRegister(regName, 0);
}

void
tcds::pm::TCADevicePMCommonBase::enableDAQBackpressure() const
{
  writeRegister("main.inselect.daq_backpressure_enable", 0x1);
}

void
tcds::pm::TCADevicePMCommonBase::disableDAQBackpressure() const
{
  writeRegister("main.inselect.daq_backpressure_enable", 0x0);
}

void
tcds::pm::TCADevicePMCommonBase::sendL1A() const
{
  std::string const regName = "main.ipbus_requests";
  uint32_t const regVal = tcds::definitions::SOFTWARE_REQUEST_TYPE_L1A;
  writeRegister(regName, regVal);
}

void
tcds::pm::TCADevicePMCommonBase::sendL1APattern() const
{
  std::string const regName = "main.resets.pattern_trigger_start";
  // NOTE: This three-value sequence is a bit overkill, but it is
  // guaranteed to be robust against future changes in trigger
  // behaviour in the firmware.
  writeRegister(regName, 0x0);
  writeRegister(regName, 0x1);
  writeRegister(regName, 0x0);
}

void
tcds::pm::TCADevicePMCommonBase::stopL1APattern() const
{
  std::string const regName = "main.resets.pattern_trigger_reset";
  // NOTE: This three-value sequence is a bit overkill, but it is
  // guaranteed to be robust against future changes in trigger
  // behaviour in the firmware.
  writeRegister(regName, 0x0);
  writeRegister(regName, 0x1);
  writeRegister(regName, 0x0);
}

void
tcds::pm::TCADevicePMCommonBase::sendBgo(tcds::definitions::BGO_NUM const bgoNumber,
                                         bool const isInternalBgo) const
{
  // First check if the number matches a real B-go number.
  tcds::utils::verifyBgoNumber(bgoNumber);

  // Now do as we were asked.
  std::string const regName = isInternalBgo ?
    "main.internal_bgo_and_bgo_train_requests" : "main.ipbus_requests";
  uint32_t const regVal = tcds::definitions::SOFTWARE_REQUEST_TYPE_BGO + bgoNumber;
  writeRegister(regName, regVal);
}

void
tcds::pm::TCADevicePMCommonBase::sendBgoTrain(tcds::definitions::SEQUENCE_NUM const sequenceNumber,
                                              bool const isInternalBgoTrain) const
{
  // First check if the number matches a real sequence number.
  tcds::utils::verifySequenceNumber(sequenceNumber);

  // Now do as we were asked.
  std::string const regName = isInternalBgoTrain ?
    "main.internal_bgo_and_bgo_train_requests" : "main.ipbus_requests";
  uint32_t const regVal = tcds::definitions::SOFTWARE_REQUEST_TYPE_BGO_TRAIN + sequenceNumber;
  writeRegister(regName, regVal);
}

void
tcds::pm::TCADevicePMCommonBase::resetCounters() const
{
  // NOTE: This method does _not_ reset the orbit counter. The orbit
  // counter should only be reset by the OC0 at the start of a
  // run. This defines the start of a new CMS data-taking run, and
  // affects the BRILDAQ too.

  // Reset B-go counters.
  writeRegister("main.resets.bgo_counter_reset", 0x1);

  // Reset event counter.
  writeRegister("main.resets.event_counter_reset", 0x1);

  // Reset sequence counters.
  writeRegister("main.resets.sequence_counter_reset", 0x1);

  // Reset triggers-canceled-by-trigger-rules counter.
  writeRegister("main.resets.trigger_rules_cancel_counter_reset", 0x1);

  // Reset the TTS state-change counters.
  resetTTSCounters();
}

void
tcds::pm::TCADevicePMCommonBase::resetOrbitMonitoring() const
{
  // Reset the orbit realignment counters.
  writeRegister("main.resets.orbit_monitoring_reset", 0x1);
}

void
tcds::pm::TCADevicePMCommonBase::resetL1AHistograms() const
{
  writeRegister("main.resets.trigger_histos_reset", 0x1);
}

void
tcds::pm::TCADevicePMCommonBase::resetBgoHistory() const
{
  writeRegister("main.resets.bgo_history_reset", 0x1);
}

void
tcds::pm::TCADevicePMCommonBase::initPatternTriggerGenerator() const
{
  writeRegister("main.resets.pattern_trigger_reset", 0x1);
}

void
tcds::pm::TCADevicePMCommonBase::initCyclicGenerator(unsigned int const genNumber) const
{
  // First check if the number matches an existing generator.
  verifyCyclicGeneratorNumber(genNumber);

  std::string const regName = toolbox::toString("main.resets.cyclic_generators_init.generator%d",
                                                genNumber);
  writeRegister(regName, 0x1);
}

void
tcds::pm::TCADevicePMCommonBase::initCyclicGenerators() const
{
  writeRegister("main.resets.cyclic_generators_init.all", 0x1);
}

void
tcds::pm::TCADevicePMCommonBase::initSequences() const
{
  writeRegister("main.resets.sequences_init", 0x1);
}

void
tcds::pm::TCADevicePMCommonBase::initDAQLink() const
{
  // NOTE: This resets the TCDS firmware DAQ interface, not the DAQ
  // link firmware block itself. The latter we are _not_ supposed to
  // ever reset.
  std::string const regName = "main.resets.reset_daq_interface";
  writeRegister(regName, 0x0);
  writeRegister(regName, 0x1);
  writeRegister(regName, 0x0);
}

void
tcds::pm::TCADevicePMCommonBase::configureTriggerPatternLength() const
{
  std::vector<uint32_t> const pattern = readBlock("pattern_trigger_ram");
  std::vector<uint32_t>::const_iterator it =
    std::find(pattern.begin(), pattern.end(), 0);
  size_t const patternLength = it - pattern.begin();
  writeRegister("main.pattern_trigger_config.pattern_length", patternLength);
}

double
tcds::pm::TCADevicePMCommonBase::readDAQBufferFillLevel() const
{
  // NOTE: The DAQ buffer watermark bus width is (indirectly)
  // hard-coded here.
  uint32_t const divider = 256;
  std::string const regName = "main.daq_interface_status.derandomiser_watermark";
  uint32_t const regVal = readRegister(regName);
  // NOTE: The result is expressed as a percentage.
  double const res = (100. * regVal) / divider;
  return res;
}

double
tcds::pm::TCADevicePMCommonBase::readDAQFifoFillLevel() const
{
  // NOTE: The DAQ FIFO watermark bus width is (indirectly) hard-coded
  // here.
  uint32_t const divider = 256;
  std::string const regName = "main.daq_interface_status.fifo_watermark";
  uint32_t const regVal = readRegister(regName);
  // NOTE: The result is expressed as a percentage.
  double const res = (100. * regVal) / divider;
  return res;
}

void
tcds::pm::TCADevicePMCommonBase::resetTTSCounters() const
{
  // Reset the TTS state-change counters.
  // NOTE: In the CPM/LPM firmware (written by Magnus) this just
  // requires a 1 to be written. The same counters in the PI (written
  // by Paschalis) require a proper 0->1 transition.
  std::string const regName = "main.resets.tts_counters_reset";
  writeRegister(regName, 0x0);
  writeRegister(regName, 0x1);
}

void
tcds::pm::TCADevicePMCommonBase::latchTTSCounters() const
{
  // NOTE: In the CPM/LPM firmware (written by Magnus) this just
  // requires a 1 to be written. The same counters in the PI (written
  // by Paschalis) require a proper 0->1 transition.
  std::string const regName = "main.resets.tts_counters_latch";
  writeRegister(regName, 0x0);
  writeRegister(regName, 0x1);
}

uint64_t
tcds::pm::TCADevicePMCommonBase::readTTSTimeCounter(std::string const& name) const
{
  // NOTE: These 'time spent in TTS state' counters are a bit
  // tricky. For time reasons we needed counters of more than 32 bits
  // wide, but we did not really want to make the actual readout
  // values more than 32 bits wide. Assuming that at large values we
  // are not interested in the last clock ticks of precision, 44-bit
  // wide counters were implemented, that are read back as a
  // combination of a 4-bit 'shift value' and a 28-bit counter
  // value. So of the 32-bit IPbus register value, the lower 28 bits
  // are to be left-shifted by the number of bits represented by the
  // top 32 bits in order to obtain the true value in clock
  // ticks.

  // NOTE: This of course drops a certain number of least significant
  // bits, but not that many. At the maximum shift applied (0xf), this
  // shift only corresponds to approximately 9.2 orbits, or less than
  // a thousandth of a second.
  uint32_t const rawVal = readRegister(name);
  uint32_t const shiftVal = (rawVal & 0xf0000000) >> 28;
  uint32_t const counterVal = (rawVal & 0x0fffffff);
  uint64_t const res = uint64_t(counterVal) << shiftVal;
  return res;
}

bool
tcds::pm::TCADevicePMCommonBase::isReTriEnabled() const
{
  uint32_t const tmp = readRegister("main.inselect.retri_enable");
  return (tmp != 0x0);
}

void
tcds::pm::TCADevicePMCommonBase::enableReTri() const
{
  writeRegister("main.inselect.retri_enable", 0x1);
}

void
tcds::pm::TCADevicePMCommonBase::disableReTri() const
{
  writeRegister("main.inselect.retri_enable", 0x0);
}

void
tcds::pm::TCADevicePMCommonBase::setReTriDefaults() const
{
  // Configure the ReTri in the iPM with 'CMS default' values.
  // NOTE: These values are based on the pre-LS1 values found here:
  // https://svnweb.cern.ch/trac/cactus/browser/trunk/cactusprojects/retri/ReTRICell/config/RetriParameters.xml

  // 50 BX as delta-t between successive triggers for the second to be
  // counted as potentially-resonant trigger.
  writeRegister("retri.config.veto_delta", 0x32);

  // A veto count threshold of 10.
  writeRegister("retri.config.veto_threshold", 0xa);

  // 80000 BX as veto duration.
  writeRegister("retri.config.veto_duration", 0x13880);

  // The window within which triggers are considered as potentially
  // resonant: 2kHz-100kHz.
  writeRegister("retri.config.veto_range_enable", 0x1);
  writeRegister("retri.config.veto_range_min", 0x190);
  writeRegister("retri.config.veto_range_max", 0x4e20);

  // The choice of the veto counter reset/decrement algorithm.
  writeRegister("retri.config.veto_toggle_algo", 0x0);
}

uint32_t
tcds::pm::TCADevicePMCommonBase::enableRandomTriggers(uint32_t const freqDesired) const
{
  uint32_t const maxRndSetting = 0xffffffff;
  double const scaler = (tcds::definitions::kNumBXPerOrbit *
                         tcds::definitions::kLHCOrbitFreq);

  double tmp = (maxRndSetting * 1. * freqDesired) / scaler;
  uint32_t const val = round(tmp);
  writeRegister("main.random_trigger_config", val);
  uint32_t const freqRealised = readRandomTriggerRate();
  return freqRealised;
}

void
tcds::pm::TCADevicePMCommonBase::disableRandomTriggers() const
{
  writeRegister("main.random_trigger_config", 0x0);
}

uint32_t
tcds::pm::TCADevicePMCommonBase::readRandomTriggerRate() const
{
  uint32_t const maxRndSetting = 0xffffffff;
  double const scaler = (tcds::definitions::kNumBXPerOrbit *
                         tcds::definitions::kLHCOrbitFreq);

  uint32_t const regVal = readRegister("main.random_trigger_config");
  uint32_t const res = round(scaler * regVal / maxRndSetting);
  return res;
}

std::vector<uint16_t>
tcds::pm::TCADevicePMCommonBase::readBunchMaskTriggerBXs() const
{
  return getBunchMaskBXListMatchingMask(kANDMaskBunchMaskTrigger);
}

std::vector<uint16_t>
tcds::pm::TCADevicePMCommonBase::readBunchMaskVetoBXs() const
{
  return getBunchMaskBXListMatchingMask(kANDMaskBunchMaskVeto);
}

std::vector<uint16_t>
tcds::pm::TCADevicePMCommonBase::readBunchMaskBeamActiveBXs() const
{
  return getBunchMaskBXListMatchingMask(kANDMaskBunchMaskBeamActive);
}

void
tcds::pm::TCADevicePMCommonBase::writeBunchMaskBeamActiveBXs(std::vector<bool> const& mask) const
{
  // This is a little bit tricky. But! Since there are 3564 BXs and we
  // want to be fast, we first read the block, then make the changes,
  // and write back the modifications.
  // The firmware mask implements four bits for each BX. So we have to
  // split the block into 4-bit pieces.
  // NOTE: Remember: the block in the firmware starts with 'BX 0'
  // which does not exist, really.
  // NOTE: Some care has to be taken here not to overwrite the 'other'
  // bits. We only 'OR in' the beam-active masking bit.

  std::vector<uint32_t> tmpMasks = readBlock("bunch_mask");
  for (unsigned int bx = tcds::definitions::kFirstBX;
       bx <= tcds::definitions::kLastBX;
       ++bx)
    {
      size_t const index = (bx / 8);
      size_t const offset = (bx % 8);
      size_t const shift = 4 * offset;
      uint32_t tmpVal = tmpMasks.at(index);
      if (mask.at(bx - 1))
        {
          tmpVal |= (kANDMaskBunchMaskBeamActive << shift);
        }
      else
        {
          tmpVal &= (~(kANDMaskBunchMaskBeamActive << shift));
        }
      tmpMasks.at(index) = tmpVal;
    }
  writeBlock("bunch_mask", tmpMasks);
}

std::vector<uint32_t>
tcds::pm::TCADevicePMCommonBase::readBunchMaskRaw() const
{
  // This is a little bit tricky. But! Since there are 3564 BXs and we
  // want to be fast, we have to first read the block and then puzzle
  // apart the results. Reading all 3564 single-BX registers takes
  // several seconds...
  // The firmware mask implements four bits for each BX. So we have to
  // split the block into 4-bit pieces.
  // NOTE: Remember: the block in the firmware starts with 'BX 0'
  // which does not exist, really.

  unsigned int numBX = tcds::definitions::kNumBXPerOrbit;
  std::vector<uint32_t> res;
  res.reserve(numBX);
  std::vector<uint32_t> const tmpRaw = readBlock("bunch_mask");
  for (unsigned int bx = tcds::definitions::kFirstBX;
       bx <= tcds::definitions::kLastBX;
       ++bx)
    {
      size_t const index = (bx / 8);
      size_t const offset = (bx % 8);
      size_t const shift = 4 * offset;
      uint32_t const tmpVal = (tmpRaw.at(index) & (0xf << shift)) >> shift;
      res.push_back(tmpVal);
    }
  return res;
}

std::vector<uint16_t>
tcds::pm::TCADevicePMCommonBase::getBunchMaskBXListMatchingMask(uint32_t const mask) const
{
  std::vector<uint32_t> const rawBunchMask = readBunchMaskRaw();
  std::vector<uint16_t> res;
  for (unsigned int bx = tcds::definitions::kFirstBX;
       bx <= tcds::definitions::kLastBX;
       ++bx)
    {
      size_t const index = bx - tcds::definitions::kFirstBX;
      uint32_t const val = rawBunchMask.at(index);
      if ((val & mask) != 0)
        {
          res.push_back(bx);
        }
    }
  return res;
}

std::vector<uint32_t>
tcds::pm::TCADevicePMCommonBase::readSimPipelineHistory() const
{
  // Lock the history.
  writeRegister("apve.apv_sim_history.locked", 0x1);

  // Reset the read pointer to the start of the buffer.
  writeRegister("apve.apv_sim_history.reset_read_pointer", 0x1);

  // Read the history.
  uint32_t const numEntries = readRegister("apve.apv_sim_history.num_entries");
  uint32_t const blockSize = getBlockSize("apve.apv_sim_history.data");
  std::vector<uint32_t> const res = readBlock("apve.apv_sim_history.data",
                                              std::min(numEntries, blockSize));

  // Unlock the history.
  writeRegister("apve.apv_sim_history.locked", 0x0);

  return res;
}

void
tcds::pm::TCADevicePMCommonBase::switchTTSFromPartition(unsigned int const lpmNum,
                                                        unsigned int const partitionNum,
                                                        bool const isICI,
                                                        bool const onOrOff) const
{
  std::string const tmp = isICI ? "ici" : "apve";
  std::string const regName =
    toolbox::toString("main.tts_source_enable.lpm%d.%s%d",
                      lpmNum,
                      tmp.c_str(),
                      partitionNum);
  writeRegister(regName, onOrOff);
}
