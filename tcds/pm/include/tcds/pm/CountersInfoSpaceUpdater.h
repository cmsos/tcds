#ifndef _tcds_pm_CountersInfoSpaceUpdater_h_
#define _tcds_pm_CountersInfoSpaceUpdater_h_

#include "tcds/utils/HwInfoSpaceUpdaterBase.h"

namespace tcds {
  namespace hwlayertca {
    class TCADeviceBase;
  }
}

namespace tcds {
  namespace utils {
    class InfoSpaceHandler;
    class InfoSpaceItem;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace pm {

    class CountersInfoSpaceUpdater : public tcds::utils::HwInfoSpaceUpdaterBase
    {

    public:
      CountersInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                               tcds::hwlayertca::TCADeviceBase const& hw);
      virtual ~CountersInfoSpaceUpdater();

      virtual bool updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                       tcds::utils::InfoSpaceHandler* const infoSpaceHandler);

    private:
      tcds::hwlayertca::TCADeviceBase const& getHw() const;

      tcds::hwlayertca::TCADeviceBase const& hw_;

    };

  } // namespace pm
} // namespace tcds

#endif // _tcds_pm_CountersInfoSpaceUpdater_h_
