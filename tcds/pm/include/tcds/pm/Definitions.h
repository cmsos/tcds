#ifndef _tcds_pm_Definitions_h_
#define _tcds_pm_Definitions_h_

namespace tcds {
  namespace definitions {

    // The number of LPMs connected to the CPM.
    unsigned int const kNumLPMsPerCPM = 12;

    // The number of ICIs in a single LPM.
    unsigned int const kNumICIsPerLPM = 8;

    // The number of APVEs in a single LPM.
    unsigned int const kNumAPVEsPerLPM = 4;

    // The number of cyclic generators in the CPM/LPM.
    unsigned int const kPMCyclicGenNumMin = 0;
    unsigned int const kPMCyclicGenNumMax = 15;
    unsigned int const kNumCyclicGensPerPM = kPMCyclicGenNumMax - kPMCyclicGenNumMin + 1;

    // The number of TTS-driven generators in the CPM/LPM.
    unsigned int const kNumActionGensPerPM = 8;

    // The number of trigger rules.
    unsigned int const kTriggerRuleMin = 1;
    unsigned int const kTriggerRuleMax = 15;
    unsigned int const kNumTriggerRules = kTriggerRuleMax - kTriggerRuleMin + 1;

    // Firmware 'FSM' state definitions.
    enum PM_STATE {
      PM_STATE_UNKNOWN=0,
      PM_STATE_STOPPED,
      PM_STATE_RUNNING,
      PM_STATE_PAUSED_BY_SW,
      PM_STATE_PAUSED_BY_FW,
      PM_STATE_BLOCKED_BY_FW,
      PM_STATE_BLOCKED_BY_DAQ_BACKPRESSURE,
      PM_STATE_BLOCKED_BY_RETRI,
      PM_STATE_BLOCKED_BY_APVE,
      PM_STATE_BLOCKED_BY_BX_MASK,
      PM_STATE_BLOCKED_BY_TTS
    };

    // DAQ link modes: test mode or 'real FED' mode.
    enum DAQ_LINK_MODE {
      DAQ_LINK_MODE_FED = 0,
      DAQ_LINK_MODE_TEST = 1
    };

    // Sources of deadtime (i.e., things that can veto/suppress L1As).
    enum DEADTIME_SOURCE {
      DEADTIME_SOURCE_TTS,
      DEADTIME_SOURCE_TRIGGER_RULES,
      DEADTIME_SOURCE_BUNCH_MASK_VETO,
      DEADTIME_SOURCE_RETRI,
      DEADTIME_SOURCE_APVE,
      DEADTIME_SOURCE_DAQ_BACKPRESSURE,
      DEADTIME_SOURCE_CALIBRATION,
      DEADTIME_SOURCE_SW_PAUSE,
      DEADTIME_SOURCE_FW_PAUSE
    };

  } // namespace definitions
} // namespace tcds

#endif // _tcds_pm_Definitions_h_
