#ifndef _tcds_pm_ActionsInfoSpaceUpdater_h_
#define _tcds_pm_ActionsInfoSpaceUpdater_h_

#include "tcds/utils/HwInfoSpaceUpdaterBase.h"

namespace tcds {
  namespace hwlayer {
    class DeviceBase;
  }
}

namespace tcds {
  namespace utils {
    class InfoSpaceHandler;
    class InfoSpaceItem;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace pm {

    /* class TCADevicePMCommonBase; */

    class ActionsInfoSpaceUpdater : public tcds::utils::HwInfoSpaceUpdaterBase
    {

    public:
      ActionsInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                              tcds::hwlayer::DeviceBase const& hw);
      virtual ~ActionsInfoSpaceUpdater();

      virtual bool updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                       tcds::utils::InfoSpaceHandler* const infoSpaceHandler);

    /* private: */
    /*   tcds::pm::TCADevicePMCommonBase const& getHw() const; */

    /*   tcds::pm::TCADevicePMCommonBase const& hw_; */

    };

  } // namespace pm
} // namespace tcds

#endif // _tcds_pm_ActionsInfoSpaceUpdater_h_
