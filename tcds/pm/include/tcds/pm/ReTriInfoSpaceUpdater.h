#ifndef _tcds_pm_ReTriInfoSpaceUpdater_h_
#define _tcds_pm_ReTriInfoSpaceUpdater_h_

#include "tcds/utils/HwInfoSpaceUpdaterBase.h"

namespace tcds {
  namespace hwlayertca {
    class TCADeviceBase;
  }
}

namespace tcds {
  namespace utils {
    class InfoSpaceHandler;
    class InfoSpaceItem;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace pm {

    class ReTriInfoSpaceUpdater : public tcds::utils::HwInfoSpaceUpdaterBase
    {

    public:
      ReTriInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                            tcds::hwlayertca::TCADeviceBase const& hw);
      virtual ~ReTriInfoSpaceUpdater();

      virtual bool updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                       tcds::utils::InfoSpaceHandler* const infoSpaceHandler);

    private:
      tcds::hwlayertca::TCADeviceBase const& getHw() const;

      tcds::hwlayertca::TCADeviceBase const& hw_;

    };

  } // namespace pm
} // namespace tcds

#endif // _tcds_pm_ReTriInfoSpaceUpdater_h_
