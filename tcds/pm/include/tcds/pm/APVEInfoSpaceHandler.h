#ifndef _tcds_pm_APVEInfoSpaceHandler_h_
#define _tcds_pm_APVEInfoSpaceHandler_h_

#include <string>

#include "tcds/utils/InfoSpaceHandler.h"

namespace xdaq {
  class Application;
}

namespace tcds {
  namespace utils {
    class InfoSpaceUpdater;
    class Monitor;
    class WebServer;
  }
}

namespace tcds {
  namespace pm {

    class APVEInfoSpaceHandler : public tcds::utils::InfoSpaceHandler
    {

    public:
      APVEInfoSpaceHandler(xdaq::Application& xdaqApp,
                           tcds::utils::InfoSpaceUpdater* updater);
      virtual ~APVEInfoSpaceHandler();

    protected:
      virtual void registerItemSetsWithMonitor(tcds::utils::Monitor& monitor);
      virtual void registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                 tcds::utils::Monitor& monitor,
                                                 std::string const& forceTabName="");

      virtual std::string formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const;

    private:
      std::string const itemPrefix_;

    };

  } // namespace pm
} // namespace tcds

#endif // _tcds_pm_APVEInfoSpaceHandler_h_
