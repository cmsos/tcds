#ifndef _tcds_pm_CountersInfoSpaceHandler_h_
#define _tcds_pm_CountersInfoSpaceHandler_h_

#include <string>

#include "tcds/utils/InfoSpaceHandler.h"

namespace xdaq {
  class Application;
}

namespace tcds {
  namespace utils {
    class InfoSpaceUpdater;
    class Monitor;
    class WebServer;
  }
}

namespace tcds {
  namespace pm {

    class CountersInfoSpaceHandler : public tcds::utils::InfoSpaceHandler
    {

    public:
      CountersInfoSpaceHandler(xdaq::Application& xdaqApp,
                               tcds::utils::InfoSpaceUpdater* updater);
      virtual ~CountersInfoSpaceHandler();

    protected:
      virtual void registerItemSetsWithMonitor(tcds::utils::Monitor& monitor);
      virtual void registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                 tcds::utils::Monitor& monitor,
                                                 std::string const& forceTabName="");

    };

  } // namespace pm
} // namespace tcds

#endif // _tcds_pm_CountersInfoSpaceHandler_h_
