#ifndef _tcds_pm_TTSInfoSpaceHandler_h_
#define _tcds_pm_TTSInfoSpaceHandler_h_

#include <string>

#include "tcds/utils/MultiInfoSpaceHandler.h"

namespace tcds {
  namespace utils {
    class InfoSpaceHandler;
    class InfoSpaceUpdater;
    class Monitor;
    class WebServer;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace pm {

    class TTSInfoSpaceHandler : public tcds::utils::MultiInfoSpaceHandler
    {

      /* NOTE:

        The implementation of this class is far from pretty. For
        example: the TTSInfoSpaceHandler itself creates InfoSpace
        items in the InfoSpaceHandlers it spawns. But then these
        InfoSpaceHandlers are in charge of formatting these items for
        monitoring. This is a messy mix.

        The reason for the above is bad design, really. The current
        implementation of the InfoSpaceHandlers, their items, and the
        separate webserver, mixes the information related to the item
        types and representations with the actual presentation in the
        web interface.

        The concrete problem with the TTSInfoSpaceHandler is that we
        want to group all TTS channels (i.e., ICIs and APVEs) in a
        single LPM together in the monitoring in the web interface,
        while in the InfoSpaceHandlers (i.e., in terms of flash-lists)
        we want to keep them strictly separate.

      */

    public:
      TTSInfoSpaceHandler(tcds::utils::XDAQAppBase& xdaqApp,
                          tcds::utils::InfoSpaceUpdater* updater,
                          bool const cpmMode);
      virtual ~TTSInfoSpaceHandler();

      tcds::utils::XDAQAppBase& getOwnerApplication() const;

    protected:
      virtual void registerItemSetsWithMonitor(tcds::utils::Monitor& monitor);
      virtual void registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                 tcds::utils::Monitor& monitor,
                                                 std::string const& forceTabName="");

    private:
      void createTTSChannel(tcds::utils::InfoSpaceHandler* const handler,
                            std::string const sourceLabel,
                            std::string const sourceType,
                            unsigned int const pmNumber,
                            unsigned int const idNumber,
                            std::string const regName);

      // A flag distinguishing between CPM mode (spanning 12 LPMs) and
      // LPM mode (spanning a single LPM, obviously).
      bool isCPMMode_;
      unsigned int numLPMs_;

    };

  } // namespace pm
} // namespace tcds

#endif // _tcds_pm_TTSInfoSpaceHandler_h_
