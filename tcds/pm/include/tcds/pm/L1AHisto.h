#ifndef _tcds_pm_L1AHisto_h_
#define _tcds_pm_L1AHisto_h_

#include <stddef.h>
#include <stdint.h>
#include <string>
#include <vector>

namespace tcds {
  namespace pm {

    class L1AHisto
    {

    public:
      L1AHisto(std::vector<uint32_t> const dataIn);

      uint32_t lumiSectionNumber() const;
      uint32_t lumiNibbleNumber() const;
      bool isCorrupted() const;

      std::string getJSONString() const;

    private:
      static size_t const kIndexLumiNibbleNumberHeader = 0;
      static size_t const kIndexHistoData = 1;
      static size_t const kIndexLumiSectionNumber = 3565;
      static size_t const kIndexLumiNibbleNumberFooter = 3566;

      // This constructor is not supposed to be used.
      L1AHisto();

      std::vector<uint32_t> const rawData_;

    };

  } // namespace pm
} // namespace tcds

#endif // _tcds_pm_L1AHisto_h_
