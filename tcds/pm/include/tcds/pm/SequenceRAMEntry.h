#ifndef _tcds_pm_SequenceRAMEntry_h_
#define _tcds_pm_SequenceRAMEntry_h_

#include <stdint.h>

namespace tcds {
  namespace pm {

    class SequenceRAMEntry
    {

    public:

      SequenceRAMEntry(uint32_t const rawData);

      uint32_t rawData() const;

      bool isEndOfSequence() const;
      bool isFireL1A() const;
      bool isFireBgo() const;
      bool isSwitchToSequenceTriggerMode() const;
      bool isSwitchToNormalTriggerMode() const;
      bool isPause() const;
      bool isUnpause() const;
      uint8_t opCode() const;
      uint8_t bgoNumber() const;
      uint16_t delay() const;

    private:

      static uint8_t const opCodeEndOfSequence_ = 0x05;
      static uint8_t const opCodeFireL1A_ = 0x07;
      static uint8_t const opCodeFireBgo_ = 0x04;
      /* static uint8_t const opCodeFireSequence_ = 0x04; */
      static uint8_t const opCodeSwitchToSequenceTriggerMode_ = 0x09;
      static uint8_t const opCodeSwitchToNormalTriggerMode_ = 0x0a;
      static uint8_t const opCodePause_ = 0x0b;
      static uint8_t const opCodeUnpause_ = 0x0c;

      // This constructor is not supposed to be used.
      SequenceRAMEntry();

      /* uint32_t ctrlWord_; */
      uint32_t rawData_;

    };

  } // namespace pm
} // namespace tcds

#endif // _tcds_pm_SequenceRAMEntry_h_
