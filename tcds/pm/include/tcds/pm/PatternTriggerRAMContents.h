#ifndef _tcds_pm_PatternTriggerRAMContents_h_
#define _tcds_pm_PatternTriggerRAMContents_h_

#include <stdint.h>
#include <string>
#include <vector>

#include "tcds/pm/PatternTriggerRAMEntry.h"

namespace tcds {
  namespace pm {

    class PatternTriggerRAMContents
    {

    public:
      PatternTriggerRAMContents(std::vector<uint32_t> const dataIn);

      std::string getJSONString() const;

    private:
      // This constructor is not supposed to be used.
      PatternTriggerRAMContents();

      std::vector<uint32_t> const rawData_;
      std::vector<PatternTriggerRAMEntry> entries_;

    };

  } // namespace pm
} // namespace tcds

#endif // _tcds_pm_PatternTriggerRAMContents_h_
