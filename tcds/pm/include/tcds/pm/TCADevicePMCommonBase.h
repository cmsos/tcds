#ifndef _tcds_pm_TCADevicePMCommonBase_h_
#define _tcds_pm_TCADevicePMCommonBase_h_

#include <memory>
#include <stdint.h>
#include <string>
#include <vector>

#include "tcds/hwlayertca/TCADeviceBase.h"
#include "tcds/pm/Definitions.h"
#include "tcds/utils/Definitions.h"

namespace tcds {
  namespace hwlayertca {
    class TCACarrierBase;
  }
}

namespace tcds {
  namespace pm {

    /**
     * Implementation of the common bits between the CPM and the LPM.
     */
    class TCADevicePMCommonBase : public tcds::hwlayertca::TCADeviceBase
    {

    public:
      virtual ~TCADevicePMCommonBase();

      // The following methods set some values that originate in
      // software, but need to go into the hardware in order to be
      // included in the event record sent to the DAQ.
      void setFEDId(uint32_t const fedId) const;
      void setRunNumber(uint32_t const runNumber) const;
      void setSoftwareVersion(uint32_t const softwareVersion) const;

      bool isExternalOrbitSelected() const;
      bool isOrbitSignalOk() const;

      /**
       * This mimics the effects of a B-go Stop for the PM, without
       * sending it to everybody else as well.
       */
      void stop() const;

      // These are pure software.
      void systemPause() const;
      void systemUnpause() const;
      bool isSystemPaused() const;

      bool isTTSEnabled() const;

      void enableTTSFromICI(unsigned int const lpmNum,
                            unsigned int const iciNum) const;
      void disableTTSFromICI(unsigned int const lpmNum,
                             unsigned int const iciNum) const;
      void enableTTSFromAPVE(unsigned int const lpmNum,
                             unsigned int const apveNum) const;
      void disableTTSFromAPVE(unsigned int const lpmNum,
                              unsigned int const apveNum) const;

      // This is pure firmware (modified by firmware sequence
      // actions).
      bool isL1APausedFW() const;
      bool isL1ABlockedFW() const;

      bool isL1ABlockedDAQBackpressure() const;
      bool isL1ABlockedRetri() const;
      bool isL1ABlockedApve() const;
      bool isL1ABlockedBXMask() const;
      bool isL1ABlockedTTS() const;

      // This checks if the internal running state is 'active'. That
      // is: after a B-go Start but before a B-go Stop.
      bool isRunActive() const;

      tcds::definitions::PM_STATE getPMState() const;

      void enableTrigger() const;
      void disableTrigger() const;

      void enableCyclicGenerator(unsigned int const genNumber) const;
      void disableCyclicGenerator(unsigned int const genNumber) const;

      void enableDAQBackpressure() const;
      void disableDAQBackpressure() const;

      void sendL1A() const;
      void sendL1APattern() const;
      void stopL1APattern() const;
      void sendBgo(tcds::definitions::BGO_NUM const bgoNumber,
                   bool const isInternalBgo=false) const;
      void sendBgoTrain(tcds::definitions::SEQUENCE_NUM const sequenceNumber,
                        bool const isInternalBgoTrain=false) const;

      // Resets etc.
      void resetCounters() const;
      void resetOrbitMonitoring() const;
      void resetL1AHistograms() const;
      void resetBgoHistory() const;
      void initPatternTriggerGenerator() const;
      void initCyclicGenerator(unsigned int const genNumber) const;
      void initCyclicGenerators() const;
      void initSequences() const;
      void initDAQLink() const;

      void configureTriggerPatternLength() const;

      // This methods reads the relative fill level (in %) of the
      // input buffer of the DAQ interface in front of the S-link
      // sender core.
      double readDAQBufferFillLevel() const;

      // This method reads the relative fill level (in %) of the FIFO
      // connecting the DAQ interface to the S-link sender core.
      double readDAQFifoFillLevel() const;

      // TTS time and transition counter handling.
      void resetTTSCounters() const;
      void latchTTSCounters() const;
      uint64_t readTTSTimeCounter(std::string const& name) const;

      // ReTri-related.
      bool isReTriEnabled() const;
      void enableReTri() const;
      void disableReTri() const;
      void setReTriDefaults() const;

      // Random-trigger stuff.
      uint32_t enableRandomTriggers(uint32_t const freqDesired) const;
      void disableRandomTriggers() const;
      uint32_t readRandomTriggerRate() const;

      // Some methods to help with the BX masks.
      std::vector<uint16_t> readBunchMaskTriggerBXs() const;
      std::vector<uint16_t> readBunchMaskVetoBXs() const;
      std::vector<uint16_t> readBunchMaskBeamActiveBXs() const;
      void writeBunchMaskBeamActiveBXs(std::vector<bool> const& mask) const;

      // BUG BUG BUG
      // This should be refactored together with the corresponding
      // methods in the APVE.
      std::vector<uint32_t> readSimPipelineHistory() const;
      // BUG BUG BUG end

    protected:
      /**
       * @note
       * Protected constructor since this is an abstract base class.
       * @note
       * The TCADeviceBase class takes ownership of the TCACarrierBase
       * pointer.
       */
      TCADevicePMCommonBase(std::unique_ptr<tcds::hwlayertca::TCACarrierBase> carrier);

      std::vector<uint32_t> readBunchMaskRaw() const;
      std::vector<uint16_t> getBunchMaskBXListMatchingMask(uint32_t const mask) const;

    private:
      static uint32_t const kANDMaskBunchMaskTrigger = 0x1;
      static uint32_t const kANDMaskBunchMaskVeto = 0x2;
      static uint32_t const kANDMaskBunchMaskBeamActive = 0x4;

      bool isBlockingTTSState(tcds::definitions::TTS_STATE const ttsState) const;

      void switchTTSFromPartition(unsigned int const lpmNum,
                                  unsigned int const partitionNum,
                                  bool const isICI,
                                  bool const onOrOff) const;
    };

  } // namespace pm
} // namespace tcds

#endif // _tcds_pm_TCADevicePMCommonBase_h_
