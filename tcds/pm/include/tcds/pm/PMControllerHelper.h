#ifndef _tcds_pm_PMControllerHelper_h_
#define _tcds_pm_PMControllerHelper_h_

#include <string>

#include "toolbox/Event.h"

#include "tcds/utils/RegCheckResult.h"

namespace tcds {
  namespace hwlayer {
    class RegisterInfo;
  }
}

namespace tcds {
  namespace utils {
    class XDAQAppWithFSMForPMs;
  }
}

namespace tcds {
  namespace pm {

    class TCADevicePMCommonBase;

    /**
     * Helper class to coordinate things that the CPMController and
     * LPMController have in common (and that should be identical for
     * CPM and LPM functionality).
     */
    class PMControllerHelper
    {

    public:
      PMControllerHelper(tcds::utils::XDAQAppWithFSMForPMs const& xdaqApp,
                         tcds::pm::TCADevicePMCommonBase const& device);
      ~PMControllerHelper();

      void hwCfgInitialize() const;
      void hwCfgFinalize() const;

      /* void coldResetAction(toolbox::Event::Reference event); */
      /* void configureAction(toolbox::Event::Reference event); */
      void enableAction(toolbox::Event::Reference event);
      /* void failAction(toolbox::Event::Reference event); */
      /* void haltAction(toolbox::Event::Reference event); */
      void pauseAction(toolbox::Event::Reference event);
      void resumeAction(toolbox::Event::Reference event);
      void stopAction(toolbox::Event::Reference event);
      void ttcHardResetAction(toolbox::Event::Reference event);
      void ttcResyncAction(toolbox::Event::Reference event);
      void zeroAction(toolbox::Event::Reference event);

      tcds::utils::RegCheckResult isRegisterAllowed(tcds::hwlayer::RegisterInfo const& regInfo) const;

      std::string buildLPMLabel(unsigned int const lpmNumber) const;

    private:
      tcds::utils::XDAQAppWithFSMForPMs const& xdaqApp_;
      tcds::pm::TCADevicePMCommonBase const& device_;

      void makeSafe() const;

    };

  } // namespace pm
} // namespace tcds

#endif // _tcds_pm_PMControllerHelper_h_
