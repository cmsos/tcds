#ifndef _tcds_pm_PatternTriggerRAMEntry_h_
#define _tcds_pm_PatternTriggerRAMEntry_h_

#include <stdint.h>

namespace tcds {
  namespace pm {

    class PatternTriggerRAMEntry
    {

    public:

      PatternTriggerRAMEntry(uint32_t const rawData);

      uint32_t rawData() const;

      bool isEndOfSequence() const;
      // bool isEndOfSequenceLoop() const;
      // bool isEndOfSequenceStop() const;
      uint32_t entryVal() const;
      uint32_t delay() const;

    private:

      static uint32_t const endOfSequence_ = 0x0;
      // static uint32_t const endOfSequenceLoop_ = 0x0;
      // static uint32_t const endOfSequenceStop_ = 0x1;

      // This constructor is not supposed to be used.
      PatternTriggerRAMEntry();

      uint32_t rawData_;

    };

  } // namespace pm
} // namespace tcds

#endif // _tcds_pm_PatternTriggerRAMEntry_h_
