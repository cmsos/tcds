#ifndef _tcds_pm_TTSCountersInfoSpaceHandler_h_
#define _tcds_pm_TTSCountersInfoSpaceHandler_h_

#include <string>
#include <map>
#include <vector>

#include "tcds/pm/Definitions.h"
#include "tcds/utils/InfoSpaceHandler.h"

namespace tcds {
  namespace utils {
    class InfoSpaceUpdater;
    class Monitor;
    class WebServer;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace pm {

    class TTSCountersInfoSpaceHandler : public tcds::utils::InfoSpaceHandler
    {

    public:
      TTSCountersInfoSpaceHandler(tcds::utils::XDAQAppBase& xdaqApp,
                                  tcds::utils::InfoSpaceUpdater* updater,
                                  bool const cpmMode);
      virtual ~TTSCountersInfoSpaceHandler();

      tcds::utils::XDAQAppBase& getOwnerApplication() const;

    protected:
      virtual void registerItemSetsWithMonitor(tcds::utils::Monitor& monitor);
      virtual void registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                 tcds::utils::Monitor& monitor,
                                                 std::string const& forceTabName="");

    private:
      // A flag distinguishing between CPM mode (spanning 12 LPMs) and
      // LPM mode (spanning a single LPM, obviously).
      bool isCPMMode_;
      unsigned int numLPMs_;

      // A list of register names determining the order of the TTS
      // states in the counters.
      std::vector<std::string> counterColumns_;
      std::map<std::string, std::string> columnLabels_;

      // A little helper to help create the two sets of counters: for
      // transitions into each state, and for time spent in each
      // state.
      std::vector<std::string> counterTypes_;

      std::vector<tcds::definitions::DEADTIME_SOURCE> deadtimeSources_;

    };

  } // namespace pm
} // namespace tcds

#endif // _tcds_pm_TTSCountersInfoSpaceHandler_h_
