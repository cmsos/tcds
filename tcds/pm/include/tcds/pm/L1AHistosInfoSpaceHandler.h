#ifndef _tcds_pm_L1AHistosInfoSpaceHandler_h_
#define _tcds_pm_L1AHistosInfoSpaceHandler_h_

#include <string>

#include "tcds/utils/InfoSpaceHandler.h"

namespace tcds {
  namespace utils {
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace utils {
    class InfoSpaceUpdater;
    class Monitor;
    class WebServer;
  }
}

namespace tcds {
  namespace pm {

    class L1AHistosInfoSpaceHandler : public tcds::utils::InfoSpaceHandler
    {

    public:
      L1AHistosInfoSpaceHandler(tcds::utils::XDAQAppBase& xdaqApp,
                                tcds::utils::InfoSpaceUpdater* updater);
      virtual ~L1AHistosInfoSpaceHandler();

    protected:
      virtual void registerItemSetsWithMonitor(tcds::utils::Monitor& monitor);
      virtual void registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                 tcds::utils::Monitor& monitor,
                                                 std::string const& forceTabName="");

      virtual std::string formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const;

    };

  } // namespace pm
} // namespace tcds

#endif // _tcds_pm_L1AHistosInfoSpaceHandler_h_
