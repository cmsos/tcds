#ifndef _tcds_pm_DAQInfoSpaceHandler_h_
#define _tcds_pm_DAQInfoSpaceHandler_h_

#include <string>

#include "tcds/utils/InfoSpaceHandler.h"

namespace tcds {
  namespace utils {
    class InfoSpaceUpdater;
    class Monitor;
    class WebServer;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace pm {

    class DAQInfoSpaceHandler : public tcds::utils::InfoSpaceHandler
    {

    public:
      DAQInfoSpaceHandler(tcds::utils::XDAQAppBase& xdaqApp,
                          tcds::utils::InfoSpaceUpdater* updater);
      virtual ~DAQInfoSpaceHandler();

      tcds::utils::XDAQAppBase& getOwnerApplication() const;

    protected:
      virtual void registerItemSetsWithMonitor(tcds::utils::Monitor& monitor);
      virtual void registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                 tcds::utils::Monitor& monitor,
                                                 std::string const& forceTabName="");

      virtual std::string formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const;

    };

  } // namespace pm
} // namespace tcds

#endif // _tcds_pm_DAQInfoSpaceHandler_h_
