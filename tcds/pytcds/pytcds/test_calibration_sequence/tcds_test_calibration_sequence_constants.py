###############################################################################

# The addresses of the boards/applications to connect to.
CPM = "cpm"
LPM = "lpm-1"
ICIS = ["ici-11"]
APVES = ["apve-11"]
PIS = ["pi-11"]

# The FED id of the CPM/LPM, so we can disable the readout.
FED_ID_CPM = 1024
FED_ID_LPM = 0

###############################################################################
