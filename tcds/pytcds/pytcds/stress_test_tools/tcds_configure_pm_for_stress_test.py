#!/usr/bin/env python

###############################################################################
## Utility to configure a cyclic generator and a B-go sequence in a
## CPM/LPM to perform sub-system stress tests.
###############################################################################

import datetime
import os
import sys

import uhal

from pytcds.stress_test_tools import __file__ as path_helper
from pytcds.cpm.tcds_cpm import get_cpm_hw
from pytcds.lpm.tcds_lpm import get_lpm_hw
from pytcds.utils.tcds_cmd_line_base import CmdLineBase
from pytcds.utils.tcds_constants import BOARD_TYPE_CPMT1
from pytcds.utils.tcds_constants import BOARD_TYPE_LPM
from pytcds.utils.tcds_constants import BOARD_TYPE_UNKNOWN
from pytcds.utils.tcds_constants import board_names
from pytcds.utils.tcds_utils_hw_connect import get_carrier_hw
from pytcds.utils.tcds_utils_hw_id import connect_to_identified_board
from pytcds.utils.tcds_utils_hw_id import identify_board
from pytcds.utils.tcds_utils_misc import chunkify
from pytcds.utils.tcds_utils_misc import name_matches_pattern
from pytcds.utils.tcds_utils_misc import uint32_to_string
from pytcds.utils.tcds_utils_networking import resolve_network_address

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

NUM_CYCLIC_GENERATORS = 16
NUM_BGO_SEQUENCES = 32

MAX_NUM_CYCLIC_GENERATOR = NUM_CYCLIC_GENERATORS - 1
MAX_NUM_BGO_SEQUENCE = NUM_BGO_SEQUENCES - 1

REG_NAME_PREFIXES = {
    'CPM': "cpmt1.ipm",
    'LPM': "ipm"
    }

TEMPLATE_DIR_NAME = os.path.dirname(path_helper)

CONFIG_FILE_NAMES = {
    'resync': TEMPLATE_DIR_NAME + "/stress_test_config_template_resync.txt",
    'hardreset': TEMPLATE_DIR_NAME + "/stress_test_config_template_hardreset.txt",
    'custom': "stress_test_config_custom.txt"
    }

###############################################################################

class Configurator(CmdLineBase):

    def __init__(self, description=None, usage=None, epilog=None):
        super(Configurator, self).__init__(description, usage, epilog)
        # self.mode = None
        # End of __init__().

    def setup_parser_custom(self):
        # Add sub-parsers for the different stress-test modes.
        subparsers = self.parser.add_subparsers(dest="stress_test_mode",
                                                help="Available stress-test modes")
        parser_mode_resync = subparsers.add_parser("resync",
                                                   help="Standard Resync stress-test")
        parser_mode_hardreset = subparsers.add_parser("hardreset",
                                                      help="Standard HardReset stress-test")
        parser_mode_custom = subparsers.add_parser("custom",
                                                   help="Fully custom stress-test configuration")

        #----------

        # Now stick on the 'target' positional argument.
        # NOTE: This cannot be done before the above subparser
        # construction because that in itself adds a positional
        # argument.
        for parser in [parser_mode_resync, parser_mode_hardreset, parser_mode_custom]:
            parser.add_argument("target",
                                help="The hardware target to operate on."
                                " I.e., the DNS name of the CPM-T1 or the LPM to address.")
            parser.add_argument("--ipm-number",
                                type=int,
                                choices=[1, 2],
                                help="For LPM hardware targets: the iPM number to address."
                                " For CPM hardware targets: ignored.")

        #----------

        # Add the option to choose the cyclic generator number.
        help_str = "Number of the cyclic generator to use. " \
                   "Range: 0-{0:d}. " \
                   "Please consult the CPM-/LPMController application " \
                   "for an unused cyclic generator, " \
                   "or use one of the --force-XXX flags below."
        help_str = help_str.format(MAX_NUM_CYCLIC_GENERATOR)
        for parser in [parser_mode_resync, parser_mode_hardreset]:
            parser.add_argument("generator_number",
                                type=int,
                                choices=range(NUM_CYCLIC_GENERATORS),
                                metavar="[0-{0:d}]".format(MAX_NUM_CYCLIC_GENERATOR),
                                help=help_str)

        # Add the option to choose the B-go sequence number.
        help_str = "Number of the B-go sequence to use. " \
                   "Range: 0-{0:d}. " \
                   "Please consult the CPM-/LPMController application " \
                   "for an unused B-go sequence, " \
                   "or use one of the --force-XXX flags below."
        help_str = help_str.format(MAX_NUM_BGO_SEQUENCE)
        for parser in [parser_mode_resync, parser_mode_hardreset]:
            parser.add_argument("sequence_number",
                                type=int,
                                choices=range(NUM_BGO_SEQUENCES),
                                metavar="[0-{0:d}]".format(MAX_NUM_BGO_SEQUENCE),
                                help=help_str)

        # Add the 'force-XXX' flags.
        for parser in [parser_mode_resync, parser_mode_hardreset]:
            help_str = "Use the current configuration," \
                       " even if it differs from the intended configuration."
            parser.add_argument("--force-use",
                                action="store_true",
                                dest="force_use",
                                help=help_str)
            help_str = "Program the specified configuration," \
                       " even if a non-zero configuration already exists."
            parser.add_argument("--force-program",
                                action="store_true",
                                dest="force_program",
                                help=help_str)

        # Add the option to specify a custom configuration file.
        help_str = "Name of custom configuration file."
        parser_mode_custom.add_argument("custom_file",
                                        help=help_str)

        # End of setup_parser().

    def handle_args(self):
        super(Configurator, self).handle_args()
        self.stress_test_mode = self.args.stress_test_mode
        self.ipm_number = self.args.ipm_number

        try:
            self.generator_number = self.args.generator_number
        except AttributeError:
            self.generator_number = None
        try:
            self.sequence_number = self.args.sequence_number
        except AttributeError:
            self.sequence_number = None
        try:
            self.force_use = self.args.force_use
        except AttributeError:
            self.force_use = None
        try:
            self.force_program = self.args.force_program
        except AttributeError:
            self.force_program = None
        try:
            self.custom_config_file_name = self.args.custom_file
        except AttributeError:
            self.custom_config_file_name = None
        # End of handle_args().

    def _load_config(self, file_name):
        try:
            # The configuration file is supposed to be in the same
            # format as all other TCDS hardware configuration files.
            with open(file_name, 'r') as in_file:
                tmp = in_file.read()
                config_raw = tmp.split(os.linesep)
        except Exception, err:
            msg_base = "Failed to read configuration file {0:s}: {1:s}"
            self.error(msg_base.format(file_name, str(err)))

        # NOTE: The following is a bit messy, due to the need to
        # treat block registers as well as single-word registers.
        config = {}
        for line in config_raw:
            line_stripped = line.strip()
            if not len(line) or (line_stripped[0] == "#"):
                continue
            pieces = [i.strip() for i in line.split()]
            reg_name = pieces[0]
            try:
                reg_vals = [int(i, 16) for i in pieces[1:]]
            except ValueError:
                msg_base = "Failed to convert '{0:s}' to a list of integers. " \
                           "(Found in line '{1:s}' read from " \
                           "configuration file.)"
                self.error(msg_base.format(str(pieces[1:]), line))
            try:
                config[reg_name].extend(reg_vals)
            except KeyError:
                config[reg_name] = reg_vals

        # End of _load_config().
        return config

    def _apply_config(self, config):
        original_values = []
        for (reg_name, reg_vals) in config.iteritems():
            if len(reg_vals) == 1:
                ori = [self.dev.read(self.reg_name_prefix + "." + reg_name)]
                self.dev.write(self.reg_name_prefix + "." + reg_name, reg_vals[0])
            else:
                ori = self.dev.read_block(self.reg_name_prefix + "." + reg_name, len(reg_vals))
                self.dev.write_block(self.reg_name_prefix + "." + reg_name, reg_vals)
            original_values.append((reg_name, ori))
        # End of _apply_config().
        return original_values

    def main(self):

        # This is a tool used by non-experts, so let's remove the
        # usual uhal warnings about address overlaps in the address
        # tables.
        uhal.setLogLevelTo(uhal.LogLevel.ERROR)

        # Validate the target and see if it is an LPM or a CPM.
        if self.verbose:
            print "Validating target..."
        self.validate_target(target_type=CmdLineBase.TARGET_TYPE_HW)
        board_info = self.setup.hw_targets[self.target]
        network_address = board_info.dns_alias
        controlhub_address = board_info.controlhub
        (carrier_type, board_type) = identify_board(network_address,
                                                    controlhub_address,
                                                    verbose=False)

        if self.verbose:
            print "Determining target type..."
        if board_type == BOARD_TYPE_UNKNOWN:
            self.error("Could not connect to {0:s}.".format(self.target))

        if not board_type in [BOARD_TYPE_CPMT1, BOARD_TYPE_LPM]:
            msg_base = "Target is not a CPM(T1) or an LPM, but a {0:s}"
            self.error(msg_base.format(board_names[board_type]))

        self.mode = 'CPM'
        self.ipm = None
        if board_type == BOARD_TYPE_LPM:
            self.mode = 'LPM'
            self.ipm = self.ipm_number
            if not self.ipm:
                self.error("For an LPM hardware target,"
                           " please specify the iPM number to use,"
                           " with the --ipm-number option.")

        if self.verbose:
            print "  Target type: " + self.mode

        self.reg_name_prefix = REG_NAME_PREFIXES[self.mode]
        if self.mode == 'LPM':
            self.reg_name_prefix += str(self.ipm_number)

        if self.mode == 'CPM':
            dev = get_cpm_hw(network_address, controlhub_address)
        else:
            dev = get_lpm_hw(network_address, controlhub_address)

        if dev is None:
            self.error("Could not connect to {0:s}.".format(self.target))
        self.dev = dev

        #----------

        if self.verbose:
            print "Stress-test mode: " + self.stress_test_mode

        original_values = []
        if self.stress_test_mode == "custom":
            # The custom mode is the easiest to implement. It does not
            # know what to expect so there are no cross-checks to be
            # done either.
            if self.verbose:
                print "  Loading and applying custom stress-test configuration..."
            custom_config = self._load_config(self.custom_config_file_name)
            original_values = self._apply_config(custom_config)

        else:

            # Stress-test modes 'resync' and 'hardreset'.

            # Check if the requested cyclic generator is in use or not.
            if self.verbose:
                print "  Checking cyclic generator configuration..."
            reg_name_tmp = "{0:s}.cyclic_generator{1:d}.configuration.enabled"
            reg_name = reg_name_tmp.format(self.reg_name_prefix, self.generator_number)
            reg_val = dev.read(reg_name)
            gen_enabled = (reg_val == 0x1)
            if gen_enabled and (not self.force_use and not self.force_program):
                msg_base = "Cyclic generator number {0:d} is already enabled. " \
                           "Use the --force-use flag to use the current configuration " \
                           "or the --force-program flag to reprogram the sequence and then use it."
                self.error(msg_base.format(self.generator_number))

            # Check if the requested B-go sequence is non-empty (i.e.,
            # already configured).
            if self.verbose:
                print "  Checking B-go sequence configuration..."
            # NOTE: We're reading only the first word. If that is
            # zero, the assumption is that the rest is also zero. (If
            # not, the configuration is a mess already anyway...) If
            # it is 0x5, as unused sequences are configured by the
            # default hardware configuration, it is considered unused
            # as well.
            reg_name_tmp = "{0:s}.bgo_trains.bgo_train{1:d}.ram"
            reg_name = reg_name_tmp.format(self.reg_name_prefix, self.generator_number)
            reg_vals = dev.read_block(reg_name, 1)
            seq_configured = not ((list(set(reg_vals)) == [0x0]) \
                                  or (list(set(reg_vals)) == [0x5]))
            if seq_configured and (not self.force_use and not self.force_program):
                msg_base = "B-go sequence number {0:d} is already configured. " \
                           "Use the --force-use flag to use the current configuration " \
                           "or the --force-program flag to reprogram the sequence and then use it."
                self.error(msg_base.format(self.sequence_number))

            do_program = (not gen_enabled and not seq_configured) or self.force_program

            # Configure the B-go sequence and the cyclic generator.
            if do_program:
                if self.verbose:
                    print "  Loading configuration template..."
                # - Load config template from file.
                config_file_name = CONFIG_FILE_NAMES[self.stress_test_mode]
                config_tmp = self._load_config(config_file_name)
                # - Tweak config by filling in the requested generator and
                #   sequence numbers.
                if self.verbose:
                    print "  Preparing configuration..."
                config = {}
                gen_str = "generatorX"
                seq_str = "bgo_trainY"
                gen_str_num = "generator{0:d}".format(self.generator_number)
                seq_str_num = "bgo_train{0:d}".format(self.sequence_number)
                for (reg_name_ori, reg_vals) in config_tmp.iteritems():
                    if reg_name_ori.find(gen_str) > -1:
                        reg_name = reg_name_ori.replace(gen_str, gen_str_num)
                    elif reg_name_ori.find(seq_str) > -1:
                        reg_name = reg_name_ori.replace(seq_str, seq_str_num)
                    else:
                        msg_base = "Expected only template-like lines " \
                                   "('{0:s}' or '{1:s}') " \
                                   "in config file {2:s}, " \
                                   "but found a line '{3:s}'."
                        self.error(msg_base.format(gen_str,
                                                   seq_str,
                                                   config_file_name,
                                                   reg_name_ori))
                    config[reg_name] = reg_vals
                # - Tweak the configuration even more to set the target
                #   sequence for the stress test.
                reg_name_tmp = "cyclic_generator{0:d}.configuration.trigger_word.id"
                reg_name = reg_name_tmp.format(self.generator_number)
                config[reg_name] = [self.sequence_number]
                # - Apply config.
                if self.verbose:
                    print "  Configuring B-go sequence and cyclic generator..."
                tmp = self._apply_config(config)
                original_values.extend(tmp)

            # Disable both external trigger inputs.
            if self.verbose:
                print "  Disabling external trigger inputs..."
            config = {
                "main.inselect.external_trigger0_enable": [0],
                "main.inselect.external_trigger1_enable": [0]
                }
            tmp = self._apply_config(config)
            original_values.extend(tmp)

            # Enable the cyclic generator.
            if self.verbose:
                print "  Enabling cyclic generator..."
            reg_name_tmp = "cyclic_generator{0:d}.configuration.enabled"
            reg_name = reg_name_tmp.format(self.generator_number)
            config = {
                reg_name : [1]
                }
            tmp = self._apply_config(config)
            original_values.extend(tmp)

        #----------

        # Now store the original values somewhere for either reference
        # or roll-back.
        if self.verbose:
            print "  Storing original values..."

        timestamp = datetime.datetime.utcnow()
        timestamp_str = timestamp.isoformat().split(".")[0].replace(":", "").replace("-", "")
        out_file_name = "stress_test_rollback_{0:s}.txt".format(timestamp_str)
        rollback_cmd = "{0:s} custom {1:s} {2:s}".format(sys.argv[0], self.target, out_file_name)
        out_lines= []
        out_lines.append("# Rollback settings stored by {0:s}.{1:s}".format(sys.argv[0], os.linesep))
        out_lines.append("# Original command line was: {0:s}".format(os.linesep))
        out_lines.append("#   {0:s}{1:s}".format(" ".join(sys.argv), os.linesep))
        out_lines.append("# Rollback with: {0:s}".format(os.linesep))
        out_lines.append("#   {0:s}{1:s}".format(rollback_cmd, os.linesep))
        max_len = 0
        if len(original_values):
            max_len = max([len(i[0]) for i in original_values])
        for (reg_name, reg_vals) in original_values:
            for reg_val in reg_vals:
                val_str = "0x{0:08x}".format(reg_val)
                out_lines.append("{1:{0:d}s} {2:s}{3:s}".format(max_len, reg_name, val_str, os.linesep))
        try:
            with open(out_file_name, "w") as out_file:
                out_file.writelines(out_lines)
            print "  Stored original register values in file '{0:s}'.".format(out_file_name)
        except Exception as err:
            msg_base = "  Failed to write original register values to file: '{0:s}'."
            print msg_base.format(str(err))

        # End of main().

    # End of class Configurator.

###############################################################################

if __name__ == "__main__":

    desc_str = "Utility to configure a cyclic generator and a B-go sequence" \
               " in a CPM/LPM to perform sub-system stress tests." \
               " There are three basic modes: resync, hardreset, and custom." \
               " The custom mode allows full control to registers and values," \
               " and is considered for experts." \
               " The resync and hardreset modes are based on templatized" \
               " 'standard stress tests'." \
               " Please refer to the help for the individual modes for details."
    res = Configurator(desc_str).run()

    print "!!! Please make sure to RECONFIGURE THE TCDS " \
        "to return to normal operating conditions !!!"

    print "Done"

    sys.exit(res)

###############################################################################
