#----------------------------------------------------------------------
# APVE configuration file for CPM part of tests.
#----------------------------------------------------------------------

# Enable the APV.
config.apv_enable                       0x00000001

# Select the APV simulation (since the TCDS does not contain any real
# APVs).
config.apv_sim_select                   0x00000001

# APV latency.
config.apv_latency                      0x00000092

# APV readout mode.
#   0 -> deconvolution/peak-multi mode.
#   1 -> peak-single mode.
config.apv_read_mode                    0x00000000

# APV trigger mode.
#   0 -> deconvolution mode.
#   1 -> peak mode.
config.apv_trigger_mode                 0x00000000

# Number of free buffers at which time to raise 'BUSY'.
config.apv_threshold_busy               0x00000002
# Number of free buffers at which time to raise 'WARNING'.
config.apv_threshold_warning            0x00000004

# APSP and L1A offsets.
config.apsp_offset                      0x0000001b
config.l1a_offset                       0x00000000

# Switch off the FMM input (since it does not exist in the TCDS).
config.fmm_enable                       0x00000000

# Switch off the simulated TCS states.
debug_tts.force_tts_busy                0x00000000
debug_tts.force_tts_error               0x00000000
debug_tts.force_tts_oos                 0x00000000
debug_tts.force_tts_warning             0x00000000

# Switch off the high-rate noise trigger-veto buffer.
trigger_veto_mask.word0                 0x00000000
trigger_veto_mask.word1                 0x00000000
trigger_veto_mask.word2                 0x00000000
trigger_veto_mask.word3                 0x00000000
trigger_veto_mask.word4                 0x00000000
trigger_veto_mask.word5                 0x00000000
trigger_veto_mask.word6                 0x00000000
trigger_veto_mask.word7                 0x00000000
trigger_veto_mask.word8                 0x00000000
trigger_veto_mask.word9                 0x00000000
trigger_veto_mask.word10                0x00000000
trigger_veto_mask.word11                0x00000000
trigger_veto_mask.word12                0x00000000
trigger_veto_mask.word13                0x00000000
trigger_veto_mask.word14                0x00000000
trigger_veto_mask.word15                0x00000000
trigger_veto_mask.word16                0x00000000
trigger_veto_mask.word17                0x00000000

#----------------------------------------------------------------------
