###############################################################################

# The addresses of the boards/applications to connect to.
CPM = "cpm"
ICIS = ["ici-1", "ici-2"]
APVES = []
PIS = ["pi-1", "pi-2"]

# The FED id of the CPM.
FED_ID_CPM = 2048

# The FED id of the AMC13 in the FEROL40 crate.
FED_ID_FEROL40 = 2049
# The FED id of the TCDS-adapter crate.
FED_ID_TCDSADAPTER = 2050

###############################################################################
