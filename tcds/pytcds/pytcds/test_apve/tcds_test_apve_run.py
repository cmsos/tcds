#!/usr/bin/env python

###############################################################################
## Master script to configure several APVEs, iCIs and PIs to test the
## APVE.  NOTE: The 'soap' mode only serves to compare the TTCSpy to
## the APVE. The 'generator' and 'pattern' modes are useful to compare
## the APVE to a real APV. Only the 'generator' pattern is fully
## deterministic and can be used to compare between setups or
## firmware/software versions.
###############################################################################

import os
import sys

from pytcds.test_apve.tcds_test_apve_constants import __file__ as constants_file_name
from pytcds.test_apve.tcds_test_apve_constants import APVES
from pytcds.test_apve.tcds_test_apve_constants import CPM
from pytcds.test_apve.tcds_test_apve_constants import FED_ID_CPM
from pytcds.test_apve.tcds_test_apve_constants import FED_ID_LPM
from pytcds.test_apve.tcds_test_apve_constants import FED_ID_GT1
from pytcds.test_apve.tcds_test_apve_constants import FED_ID_GT2
from pytcds.test_apve.tcds_test_apve_constants import ICIS
from pytcds.test_apve.tcds_test_apve_constants import LPM
from pytcds.test_apve.tcds_test_apve_constants import PIS
from pytcds.utils.tcds_cmd_line_base import CmdLineBase
from pytcds.utils.tcds_command_runner import add_cwd_to_cmds
from pytcds.utils.tcds_command_runner import CommandRunner

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

def append_piece_to_cmds(cmds, piece):
    res = []
    for (cmd, doc) in cmds:
        full_cmd = cmd
        if cmd.endswith(".py"):
            full_cmd = "%s %s" % (cmd, piece)
        res.append((full_cmd, doc))
    # End of append_piece_to_cmds().
    return res

###############################################################################

class TCDSConfigForTestAPVE(CmdLineBase):

    MODE_CHOICES = ["cpm", "lpm", "ici"]
    DRIVER_CHOICES = ["generator", "pattern", "soap"]

    def __init__(self, description=None, usage=None, epilog=None):
        super(TCDSConfigForTestAPVE, self).__init__(description, usage, epilog)
        self.mode = None
        self.driver = None
        # End of __init__().

    def setup_parser_custom(self):
        parser = self.parser
        # Add the choice of running mode.
        help_str = "Running mode"
        parser.add_argument("mode",
                            type=str,
                            action="store",
                            choices=TCDSConfigForTestAPVE.MODE_CHOICES,
                            help=help_str)
        # Add the choice of driving mode.
        help_str = "Driving mode"
        parser.add_argument("driver",
                            type=str,
                            action="store",
                            choices=TCDSConfigForTestAPVE.DRIVER_CHOICES,
                            help=help_str)
        # End of setup_parser_custom().

    def handle_args(self):
        super(TCDSConfigForTestAPVE, self).handle_args()
        self.mode = self.args.mode
        self.driver = self.args.driver
        # End of handle_args().

    def main(self):
        sep_line = "-" * 70

        test_dir_name = os.path.dirname(constants_file_name)

        #----------

        pm = None
        if self.mode == "lpm":
            pm = LPM
        elif self.mode == "cpm":
            pm = CPM

        #----------

        pm_daq_on_off = '0'
        gt1_enabled_disabled = '0'
        gt2_enabled_disabled = '0'

        fed_enable_mask = "{0:d}&{1:s}%{2:d}&{3:s}%{4:d}&{5:s}%".format(FED_ID_CPM,
                                                                        pm_daq_on_off,
                                                                        FED_ID_GT1,
                                                                        gt1_enabled_disabled,
                                                                        FED_ID_GT2,
                                                                        gt2_enabled_disabled)
        ttc_partition_map = "ICI11&1%"

        #----------

        fsm_ctrl_cmd = "../utils/tcds_fsm_control.py --setup-file {0:s} --location {1:s}".format(self.setup_file_name, self.location_name)
        fsm_poll_cmd = "../utils/tcds_fsm_poller.py --setup-file {0:s} --location {1:s}".format(self.setup_file_name, self.location_name)
        pi_force_tts_cmd = "../pi/tcds_pi_force_tts.py --setup-file {0:s} --location {1:s}".format(self.setup_file_name, self.location_name)
        send_soap_cmd = "../utils/tcds_send_soap_command.py --setup-file {0:s} --location {1:s}".format(self.setup_file_name, self.location_name)
        # retri_disable_cmd = "../cpm/tcds_cpm_disable_retri.py"
        # backpressure_disable_cmd = "../cpm/tcds_cpm_disable_backpressure.py"
        read_spy_log_cmd = "../pi/tcds_pi_read_ttcspy_log.py --setup-file {0:s} --location {1:s}".format(self.setup_file_name, self.location_name)
        read_apve_history_cmd = "../apve/tcds_apve_read_pipeline_history.py --setup-file {0:s} --location {1:s}".format(self.setup_file_name, self.location_name)
        analyse_cmd = "./tcds_analyse_apve_vs_ttcspy.py"

        cmds = []

        #----------

        # Halt everything.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Halting all controllers'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = []
        if pm:
            targets = [pm]
        targets.extend(ICIS)
        targets.extend(APVES)
        targets.extend(PIS)
        for target in targets:
            cmds.append(("{0:s} {1:s} Halt".format(fsm_ctrl_cmd, target), ""))

        # Wait till everything is in the Halted state.
        targets = []
        if pm:
            targets = [pm]
        targets.extend(ICIS)
        targets.extend(APVES)
        targets.extend(PIS)
        for target in targets:
            cmds.append(("{0:s} {1:s} --wait-until Halted".format(fsm_poll_cmd, target), ""))

        #----------

        # Configure PM.
        if pm:
            no_beam_active_bit = ""
            if self.mode == 'cpm':
                no_beam_active_bit = " --no-beam-active"
            cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                         ("python -c \"print 'Configuring {0:s}'\"".format(self.mode.upper()), ""),
                         ("python -c \"print '{0:s}'\"".format(sep_line), "")])
            cmds.append(("{0:s} --fed-enable-mask='{1:s}' --ttc-partition-map='{2:s}' {3:s} {4:s} Configure {5:s}/hw_cfg_{6:s}.txt".format(fsm_ctrl_cmd, fed_enable_mask, ttc_partition_map, no_beam_active_bit, pm, test_dir_name, self.mode), ""))
            # Wait till the PM is in the Configured state.
            cmds.append(("{0:s} {1:s} --wait-until Configured".format(fsm_poll_cmd, pm), ""))

        # Configure iCIs.
        targets = list(ICIS)
        if len(targets):
            cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                         ("python -c \"print 'Configuring iCIs'\"", ""),
                         ("python -c \"print '{0:s}'\"".format(sep_line), "")])
            for target in targets:
                tmp = ""
                if pm:
                    tmp = "_under_{0:s}".format(self.mode)
                cmds.append(("{0:s} {1:s} Configure {2:s}/hw_cfg_ici{3:s}.txt".format(fsm_ctrl_cmd, target, test_dir_name, tmp), ""))

            # Wait till everything is in the Configured state.
            for target in targets:
                cmds.append(("{0:s} {1:s} --wait-until Configured".format(fsm_poll_cmd, target), ""))

        # Configure APVEs.
        targets = list(APVES)
        if len(targets):
            cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                         ("python -c \"print 'Configuring APVEs'\"", ""),
                         ("python -c \"print '{0:s}'\"".format(sep_line), "")])
            for target in targets:
                cmds.append(("{0:s} {1:s} Configure {2:s}/hw_cfg_apve.txt".format(fsm_ctrl_cmd, target, test_dir_name), ""))

            # Wait till everything is in the Configured state.
            for target in targets:
                cmds.append(("{0:s} {1:s} --wait-until Configured".format(fsm_poll_cmd, target), ""))

        # Configure PIs.
        targets = list(PIS)
        if len(targets):
            cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                         ("python -c \"print 'Configuring PIs'\"", ""),
                         ("python -c \"print '{0:s}'\"".format(sep_line), "")])
            for target in targets:
                cmds.append(("{0:s} --fed-enable-mask='{1:s}' {2:s} Configure {3:s}/hw_cfg_pi.txt".format(fsm_ctrl_cmd, fed_enable_mask, target, test_dir_name), ""))

            # Wait till everything is in the Configured state.
            for target in targets:
                cmds.append(("{0:s} {1:s} --wait-until Configured".format(fsm_poll_cmd, target), ""))

        #----------

        # Post-configuration hacking....
        if self.driver == "pattern":
            targets = [pm]
            if self.mode == "ici":
                targets = list(ICIS)
            for target in targets:
                cmds.append(("{0:s} {1:s} DisableCyclicGenerator genNumber:unsignedInt:9".format(send_soap_cmd, target), ""))

        #----------

        # Force PIs to be TTS READY.
        targets = list(PIS)
        if len(targets):
            cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                         ("python -c \"print 'Forcing PI TTS states to READY'\"", ""),
                         ("python -c \"print '{0:s}'\"".format(sep_line), "")])
            for target in targets:
                cmds.append(("{0:s} {1:s} READY".format(pi_force_tts_cmd, target), ""))

        #----------

        # Enable PIs.
        targets = list(PIS)
        if len(targets):
            cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                         ("python -c \"print 'Enabling PIs'\"", ""),
                         ("python -c \"print '{0:s}'\"".format(sep_line), "")])
            for target in targets:
                cmds.append(("{0:s} {1:s} Enable".format(fsm_ctrl_cmd, target), ""))

            # Wait till all PIs are in the Enabled state.
            for target in targets:
                cmds.append(("{0:s} {1:s} --wait-until Enabled".format(fsm_poll_cmd, target), ""))

        # Enable APVEs.
        targets = list(APVES)
        if len(targets):
            cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                         ("python -c \"print 'Enabling APVEs'\"", ""),
                         ("python -c \"print '{0:s}'\"".format(sep_line), "")])
            for target in targets:
                cmds.append(("{0:s} {1:s} Enable".format(fsm_ctrl_cmd, target), ""))

            # Wait till all APVEs are in the Enabled state.
            for target in targets:
                cmds.append(("{0:s} {1:s} --wait-until Enabled".format(fsm_poll_cmd, target), ""))

        # Enable iCIs.
        targets = list(ICIS)
        if len(targets):
            cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                         ("python -c \"print 'Enabling iCIs'\"", ""),
                         ("python -c \"print '{0:s}'\"".format(sep_line), "")])
            for target in targets:
                cmds.append(("{0:s} {1:s} Enable".format(fsm_ctrl_cmd, target), ""))

            # Wait till all ICIs are in the Enabled state.
            for target in targets:
                cmds.append(("{0:s} {1:s} --wait-until Enabled".format(fsm_poll_cmd, target), ""))

        # Enable PM (as last!).
        if pm:
            cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                         ("python -c \"print 'Enabling {0:s}'\"".format(self.mode.upper()), ""),
                         ("python -c \"print '{0:s}'\"".format(sep_line), "")])
            cmds.append(("{0:s} {1:s} Enable".format(fsm_ctrl_cmd, pm), ""))

        # Wait till also the PM is in the Enabled state.
        if pm:
            cmds.append(("{0:s} {1:s} --wait-until Enabled".format(fsm_poll_cmd, pm), ""))

        #----------

        # Cyclic-generator mode.
        if self.driver == "generator":
            # Fire the cyclics.
            cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                         ("python -c \"print 'Firing cyclic generator'\"", ""),
                         ("python -c \"print '{0:s}'\"".format(sep_line), "")])
            # Actually, the cyclics fire themselves. They are enabled
            # by the B-go Start in the Start sequence.
            # We do need some time for them to do their job though.
            cmds.append(("python -c \"import time; time.sleep(20)\"", ""))

            # targets = [pm]
            # if self.mode == "ici":
            #     targets = list(ICIS)
            # for target in targets:
            #     cmds.append(("{0:s} {1:s} SendBgo bgoName:string:Stop".format(send_soap_cmd, target), ""))
            #     cmds.append(("{0:s} {1:s} InitCyclicGenerators".format(send_soap_cmd, target), ""))
            #     # cmds.append(("{0:s} {1:s} SendBgo bgoName:string:EC0".format(send_soap_cmd, target), ""))
            #     cmds.append(("{0:s} {1:s} SendBgo bgoName:string:Start".format(send_soap_cmd, target), ""))
            #     cmds.append(("python -c \"import time; time.sleep(20)\"", ""))

        #----------

        # Pattern-generator mode.
        if self.driver == "pattern":
            # Fire the L1A pattern generator.
            cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                         ("python -c \"print 'Firing L1A pattern generator'\"", ""),
                         ("python -c \"print '{0:s}'\"".format(sep_line), "")])
            targets = [pm]
            if self.mode == "ici":
                targets = list(ICIS)
            for target in targets:
                cmds.append(("{0:s} {1:s} SendL1APattern".format(send_soap_cmd, target), ""))
                cmds.append(("python -c \"import time; time.sleep(2)\"", ""))
                cmds.append(("{0:s} {1:s} StopL1APattern".format(send_soap_cmd, target), ""))

        #----------

        # NOTE: The generator-driven mode runs all by itself the
        # moment the Start sequence runs.

        # SOAP-driven mode.
        if self.driver == "soap":
            # Run things by SOAP commands.
            cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                         ("python -c \"print 'Running things by SOAP commands'\"", ""),
                         ("python -c \"print '{0:s}'\"".format(sep_line), "")])
            targets = [pm]
            if self.mode == "ici":
                # NOTE: This is driven by software, and therefore the
                # different ICIs/APVEs cannot be truly synchronised.
                targets = list(ICIS)
                for target in targets:
                    cmds.append(("{0:s} {1:s} SendBgo bgoName:string:Stop".format(send_soap_cmd, target), ""))
                    cmds.append(("{0:s} {1:s} SendBgo bgoName:string:Resync".format(send_soap_cmd, target), ""))
                    cmds.append(("{0:s} {1:s} SendBgo bgoName:string:EC0".format(send_soap_cmd, target), ""))

            for i in xrange(1024):
                for target in targets:
                    cmds.append(("{0:s} {1:s} SendL1A".format(send_soap_cmd, target), ""))

        #----------

        # Wait a little.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Practicing patience...'\"".format(self.mode.upper()), ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        cmds.append(("python -c \"import time; time.sleep(5)\"", ""))

        #----------

        # Check the TTCSpy log in all PIs.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Checking the TTCSpy log in all PIs'\"".format(self.mode.upper()), ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list(PIS)
        for target in targets:
            file_name = "{0:s}_spy_log.txt".format(target)
            cmds.append(("{0:s} {1:s} &> {2:s}".format(read_spy_log_cmd, target, file_name), ""))

        #----------

        # Check the pipeline history in all APVEs.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Checking the pipeline history in all APVEs'\"".format(self.mode.upper()), ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list(APVES)
        for target in targets:
            file_name = "{0:s}_pipeline_history.txt".format(target)
            cmds.append(("{0:s} {1:s} &> {2:s}".format(read_apve_history_cmd, target, file_name), ""))

        #----------

        # # Check the pipeline history in the PM.
        # if pm:
        #     cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
        #                  ("python -c \"print 'Checking the pipeline history in the PM'\"".format(self.mode.upper()), ""),
        #                  ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        #     file_name = "{0:s}_pipeline_history.txt".format(pm)
        #     cmds.append(("{0:s} {1:s} &> {2:s}".format(read_apve_history_cmd, pm, file_name), ""))

        #----------

        # Analyse the results.
        # NOTE: The APVE in the CPM may very well produce different
        # addresses than the 'real' tracker APVEs. The former only
        # serves to protect the preshower front-end buffers, and is
        # (often) configured differently from the tracker APVEs.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Analysing results'\"".format(self.mode.upper()), ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = zip(APVES, PIS)
        for (target_apve, target_pi) in targets:
            cmds.extend([("python -c \"print '  - {0:s} vs {1:s}'\"".format(target_apve, target_pi), "")])
            file_name_apve = "{0:s}_pipeline_history.txt".format(target_apve)
            file_name_pi = "{0:s}_spy_log.txt".format(target_pi)
            cmds.append(("{0:s} {1:s} {2:s}".format(analyse_cmd, file_name_apve, file_name_pi), ""))
        # targets = list(PIS)
        # for target_pi in targets:
        #     cmds.extend([("python -c \"print '  - {0:s} vs {1:s}'\"".format(pm, target_pi), "")])
        #     file_name_pm = "{0:s}_pipeline_history.txt".format(pm)
        #     file_name_pi = "{0:s}_spy_log.txt".format(target_pi)
        #     cmds.append(("{0:s} {1:s} {2:s}".format(analyse_cmd, file_name_pm, file_name_pi), ""))

        #----------

        cmds = add_cwd_to_cmds(cmds)
        if self.verbose:
            cmds = append_piece_to_cmds(cmds, "--verbose")

        runner = CommandRunner(cmds)
        runner.run()

        # End of main().

    # End of class TCDSConfigForTestAPVE.

###############################################################################

if __name__ == "__main__":

    description = "Master script to configure several APVEs, iCIs and PIs " \
                  "in order to (stress-)test the APVE."
    res = TCDSConfigForTestAPVE(description).run()
    print "Done"
    sys.exit(res)

###############################################################################
