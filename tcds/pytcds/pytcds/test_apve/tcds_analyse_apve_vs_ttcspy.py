#!/usr/bin/env python

###############################################################################
## A little script to compare the TTCSpy log to the APVE pipeline history.
###############################################################################

import os
import re
import sys

from pytcds.utils.tcds_cmd_line_base import CmdLineBase

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

# Source:
#   https://github.com/cms-sw/cmssw/blob/0397259dd747cee94b68928f17976224c037057a/EventFilter/SiStripRawToDigi/interface/PipeAddrToTimeLookupTable.h

binary_to_gray_array = [
    48,49,51,50,54,55,53,52,
    60,61,63,62,58,59,57,56,
    40,41,43,42,46,47,45,44,
    36,37,39,38,34,35,33,32,
    96,97,99,98,102,103,101,100,
    108,109,111,110,106,107,105,104,
    120,121,123,122,126,127,125,124,
    116,117,119,118,114,115,113,112,
    80,81,83,82,86,87,85,84,
    92,93,95,94,90,91,89,88,
    72,73,75,74,78,79,77,76,
    68,69,71,70,66,67,65,64,
    192,193,195,194,198,199,197,196,
    204,205,207,206,202,203,201,200,
    216,217,219,218,222,223,221,220,
    212,213,215,214,210,211,209,208,
    240,241,243,242,246,247,245,244,
    252,253,255,254,250,251,249,248,
    232,233,235,234,238,239,237,236,
    228,229,231,230,226,227,225,224,
    160,161,163,162,166,167,165,164,
    172,173,175,174,170,171,169,168,
    184,185,187,186,190,191,189,188,
    180,181,183,182,178,179,177,176
    ]

gray_to_binary_array = [
    200,200,200,200,200,200,200,200,
    200,200,200,200,200,200,200,200,
    200,200,200,200,200,200,200,200,
    200,200,200,200,200,200,200,200,
    31,30,28,29,24,25,27,26,
    16,17,19,18,23,22,20,21,
    0,1,3,2,7,6,4,5,
    15,14,12,13,8,9,11,10,
    95,94,92,93,88,89,91,90,
    80,81,83,82,87,86,84,85,
    64,65,67,66,71,70,68,69,
    79,78,76,77,72,73,75,74,
    32,33,35,34,39,38,36,37,
    47,46,44,45,40,41,43,42,
    63,62,60,61,56,57,59,58,
    48,49,51,50,55,54,52,53,
    200,200,200,200,200,200,200,200,
    200,200,200,200,200,200,200,200,
    200,200,200,200,200,200,200,200,
    200,200,200,200,200,200,200,200,
    160,161,163,162,167,166,164,165,
    175,174,172,173,168,169,171,170,
    191,190,188,189,184,185,187,186,
    176,177,179,178,183,182,180,181,
    96,97,99,98,103,102,100,101,
    111,110,108,109,104,105,107,106,
    127,126,124,125,120,121,123,122,
    112,113,115,114,119,118,116,117,
    159,158,156,157,152,153,155,154,
    144,145,147,146,151,150,148,149,
    128,129,131,130,135,134,132,133,
    143,142,140,141,136,137,139,138
]

###############################################################################

def binary_to_gray(num):
    return binary_to_gray_array[num]

def gray_to_binary(code):
    return gray_to_binary_array[code]

###############################################################################

class Analyzer(CmdLineBase):

    def __init__(self, description=None, usage=None, epilog=None):
        super(Analyzer, self).__init__(description, usage, epilog)
        self.apve_file_name = None
        self.ttcspy_file_name = None
        # End of __init__().

    def setup_parser_custom(self):
        parser = self.parser
        # On the command line we require two file names.
        help_str = "APVE history dump file."
        parser.add_argument("apve_dump_file_name",
                            type=str,
                            action="store",
                            help=help_str)
        help_str = "TTCSpy dump file."
        parser.add_argument("ttcspy_dump_file_name",
                            type=str,
                            action="store",
                            help=help_str)
        # End of setup_parser_custom().

    def handle_args(self):
        self.apve_file_name = self.args.apve_dump_file_name
        self.ttcspy_file_name = self.args.ttcspy_dump_file_name
        # End of handle_args().

    def load_setup(self):
        pass

    def main(self):

        apve_data_raw = None
        ttcspy_data_raw = None

        try:
            in_file = open(self.apve_file_name, "r")
            apve_data_raw = in_file.read()
            in_file.close()
        except IOError, err:
            msg = "Could not read file '{0:s}': '{1:s}'."
            self.error(msg.format(self.apve_file_name, err))

        try:
            in_file = open(self.ttcspy_file_name, "r")
            ttcspy_data_raw = in_file.read()
            in_file.close()
        except IOError, err:
            msg = "Could not read file '{0:s}': '{1:s}'."
            self.error(msg.format(self.ttcspy_file_name, err))

        apve_data = []
        regexp = re.compile("Event # ([0-9]+): pipeline address ([0-9]+), gray code (0x[0-9a-f]+)")
        # NOTE: Careful! The history entries are listed in reverse
        # order, and the history is not cleared before the first
        # trigger.
        prev_event_number = None
        for line in apve_data_raw.split(os.linesep):
            match = regexp.search(line)
            if match:
                event_number = int(match.group(1))
                # if ((prev_event_number is None) or \
                #     (event_number < prev_event_number)):
                    # # NOTE: Event number 0 does not exist! (A zero
                    # # here means 'empty entry.')
                    # if event_number != 0:
                apve_data.append((event_number,
                                  int(match.group(2)),
                                  int(match.group(3), 16)))
                prev_event_number = event_number
        apve_data.reverse()
        if not len(apve_data):
            msg = "There seems to be no data in the APVE history dump file"
            self.error(msg)

        ttcspy_data = []
        regexp = re.compile("Entry *([0-9]+): Addressed command.*with data (0x[0-9a-f]+)")
        for line in ttcspy_data_raw.split(os.linesep):
            match = regexp.search(line)
            if match:
                ttcspy_data.append((len(ttcspy_data),
                                    int(match.group(1)),
                                    int(match.group(2), 16)))
        if not len(ttcspy_data):
            msg = "There seems to be no data in the TTCSpy dump file"
            self.error(msg)

        # Check if there are any 'uncovered' pipeline addresses.
        used_addresses = list(set([i[1] for i in apve_data]))
        unused_addresses = []
        apve_address_min = 0
        apve_address_max = 191
        for address in xrange(apve_address_min, apve_address_max + 1):
            if not address in used_addresses:
                unused_addresses.append(address)
        print "Checking coverage of pipeline address range [{0:d}:{1:d}]".format(apve_address_min, apve_address_max)
        if not len(unused_addresses):
            print "There are no unused pipeline addresses"
        else:
            msg = "There are {0:d} unused pipeline addresses: {1:s}"
            print msg.format(len(unused_addresses), [str(i) for i in unused_addresses])

        # Check the Gray-coded pipeline addresses from the TTCSpy
        # against those from the APVE history.
        apve_gray_codes = [i[2] for i in apve_data]
        ttcspy_gray_codes = [i[2] for i in ttcspy_data]
        if (len(apve_gray_codes) != len(ttcspy_gray_codes)):
            msg = "Found {0:d} entries in the APVE history, but {1:d} in the TTCSpy history."
            self.warning(msg.format(len(apve_gray_codes), len(ttcspy_gray_codes)))

        if (apve_gray_codes == ttcspy_gray_codes):
            print "APVE and TTCSpy report the same Gray-code sequence"
        else:
            print "APVE and TTCSpy report different Gray-code sequences"
            gray_codes_together = []
            for (i, j) in zip(apve_gray_codes, ttcspy_gray_codes):
                gray_codes_together.append((len(gray_codes_together), i, j))
            problem_indices = [i[0] for i in gray_codes_together if (i[1] != i[2])]
            # import pdb
            # pdb.set_trace()
            for ind in problem_indices:
                msg = "!!! Difference at event #{0:d}, TTCSpy entry #{1:d}"
                print msg.format(apve_data[ind][0], ttcspy_data[ind][1])

                code_apve = apve_data[ind][2]
                address_apve = apve_data[ind][1]
                code_ttcspy = ttcspy_data[ind][2]
                address_ttcspy = gray_to_binary(code_ttcspy)
                msg = "!!!   APVE reports address {0:d} Gray code 0x{1:x}"
                print msg.format(apve_data[ind][1], apve_data[ind][2])
                msg = "!!!   TTCSpy reports address {0:d}, B-data 0x{0:x}"
                print msg.format(address_ttcspy, code_ttcspy)

                diff = address_ttcspy - address_apve
                print "!!!   --> address difference = {0:d}".format(diff)

        # End of main().

    # End of class Analyzer.

###############################################################################

if __name__ == "__main__":

    desc_str = "A little script to compare the TTCSpy log to the APVE pipeline history."

    res = Analyzer(desc_str).run()

    print "Done"

    sys.exit(res)

###############################################################################
