###############################################################################
## Various constants used in the TCDS monitoring stress-tests in the lab.
###############################################################################

# The addresses of the boards/applications to connect to.
CPM = "cpm"
LPM = "lpm-11"
ICIS = ["ici-11"]
APVES = ["apve-11"]
PIS = ["pi-11"]

# The FED id of the CPM/LPM, so we can disable the readout.
FED_ID_CPM = 1024
FED_ID_LPM = 1

# The FED id(s) of the GT, so we know if it is in the run or not (in
# which case we have to enable the corresponding trigger input).
FED_ID_GT1 = 1404
FED_ID_GT2 = 1405

###############################################################################
