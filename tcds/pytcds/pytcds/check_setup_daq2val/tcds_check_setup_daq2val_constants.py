###############################################################################

# The addresses of the boards/applications to use in this test.
CPM = "cpm-dv"
LPM = "lpm-dv1"
ICIS = ['ici-dv11', 'ici-dv12']
APVES = ['apve-dv11', 'apve-dv12']
PIS = ['pi-dv11', 'pi-dv12']

# The FED id of the CPM/LPM, so we can disable the readout.
CPM_FED_ID = 1024
LPM_FED_ID = 1026

###############################################################################
