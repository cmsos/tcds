#!/usr/bin/env python

###############################################################################
## Master script to configure and enable an LPM driving several iCIs,
## APVEs, and PIs to test the LPM.
##
## NOTE: In case the boards have not yet been 'bootstrapped', this
## also tests the bootstrap procedure.
###############################################################################

import sys

from pytcds.test_lpm.tcds_test_lpm_constants import APVES
from pytcds.test_lpm.tcds_test_lpm_constants import ICIS
from pytcds.test_lpm.tcds_test_lpm_constants import LPM
from pytcds.test_lpm.tcds_test_lpm_constants import PIS
from pytcds.utils.tcds_cmd_line_base import CmdLineBase
from pytcds.utils.tcds_command_runner import add_cwd_to_cmds
from pytcds.utils.tcds_command_runner import CommandRunner

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

def append_piece_to_cmds(cmds, piece):
    res = []
    for (cmd, doc) in cmds:
        full_cmd = cmd
        if cmd.endswith(".py"):
            full_cmd = "%s %s" % (cmd, piece)
        res.append((full_cmd, doc))
    # End of append_piece_to_cmds().
    return res

###############################################################################

class TCDSConfigForTestLPM(CmdLineBase):

    # There are two different 'SOAP command emission modes' to choose from:
    # - parallel: emits all SOAP commands (for one step) in parallel.
    # - sequential: emits all SOAP commands (for one step) after each other.
    MODE_CHOICES = ["parallel", "sequential"]

    def pre_hook(self):
        self.mode = None
        # End of pre_hook().

    def handle_args(self):
        # NOTE: This is a slight abuse of the CmdLineBase base class
        # since we don't use an IP address argument. Don't tell
        # anyone.

        # One arguments is expected: the choice of mode (parallel or
        # sequential).
        if len(self.args) != 1:
            msg = "One arguments is required: " \
                  "the choice of SOAP message emission. " \
                  "Options are: {0:s}."
            self.error(msg.format(", ".join(TCDSConfigForTestLPM.MODE_CHOICES)))
        else:
            # Extract the mode.
            mode = self.args[0]
            if not mode in TCDSConfigForTestLPM.MODE_CHOICES:
                msg = "'{0:s}' is not a valid mode. Options are: {1:s}."
                msg = msg.format(setup_type, ", ".join(TCDSConfigForTestLPM.MODE_CHOICES))
                self.parser.error(msg)
            self.mode = mode
        # End of handle_args().

    def main(self):
        sep_line = "-" * 70

        fsm_ctrl_cmd = "../utils/tcds_fsm_control.py --cfg-file ../utils/tcds_setup_tcdslab.cfg"
        fsm_poll_cmd = "../utils/tcds_fsm_poller.py --cfg-file ../utils/tcds_setup_tcdslab.cfg"
        send_soap_cmd = "../utils/tcds_send_soap_command.py --cfg-file ../utils/tcds_setup_tcdslab.cfg"

        cmds = []

        #----------

        # Halt everything.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Halting all controllers'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = [LPM]
        targets.extend(ICIS)
        targets.extend(APVES)
        targets.extend(PIS)
        if self.mode == "sequential":
            for target in targets:
                cmds.append(("{0:s} {1:s} Halt".format(fsm_ctrl_cmd, target), ""))
        elif self.mode == "parallel":
            target_list = " ".join(targets)
            cmds.append(("parallel --jobs {0:d} {1:s} {{}} Halt ::: {2:s}".
                         format(len(target_list), fsm_ctrl_cmd, target_list), ""))

        # Wait till everything is in the Halted state.
        cmds.append(("{0:s} {1:s} --wait-until Halted".format(fsm_poll_cmd, LPM), ""))
        targets = list(ICIS)
        targets.extend(APVES)
        targets.extend(PIS)
        for target in targets:
            cmds.append(("{0:s} {1:s} --wait-until Halted".format(fsm_poll_cmd, target), ""))

        #----------

        # Configure LPM.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Configuring LPM'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        cmds.append(("{0:s} {1:s} Configure hw_cfg_lpm.txt".format(fsm_ctrl_cmd, LPM), ""))

        # Configure iCIs.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Configuring iCIs'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list(ICIS)
        if self.mode == "sequential":
            for target in targets:
                cmds.append(("{0:s} {1:s} Configure hw_cfg_ici.txt".format(fsm_ctrl_cmd, target), ""))
        elif self.mode == "parallel":
            target_list = " ".join(targets)
            cmds.append(("parallel --jobs {0:d} {1:s} {{}} Configure hw_cfg_ici.txt ::: {2:s}".
                         format(len(target_list), fsm_ctrl_cmd, target_list), ""))

        # Configure APVEs.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Configuring APVEs'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list(APVES)
        if self.mode == "sequential":
            for target in targets:
                cmds.append(("{0:s} {1:s} Configure hw_cfg_apve.txt".format(fsm_ctrl_cmd, target), ""))
        elif self.mode == "parallel":
            target_list = " ".join(targets)
            cmds.append(("parallel --jobs {0:d} {1:s} {{}} Configure hw_cfg_apve.txt ::: {2:s}".
                         format(len(target_list), fsm_ctrl_cmd, target_list), ""))

        # Configure PIs.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Configuring PIs'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list(PIS)
        if self.mode == "sequential":
            for target in targets:
                cmds.append(("{0:s} {1:s} Configure hw_cfg_pi.txt".format(fsm_ctrl_cmd, target), ""))
        elif self.mode == "parallel":
            target_list = " ".join(targets)
            cmds.append(("parallel --jobs {0:d} {1:s} {{}} Configure hw_cfg_pi.txt ::: {2:s}".
                         format(len(target_list), fsm_ctrl_cmd, target_list), ""))

        # Wait till everything is in the Configured state.
        cmds.append(("{0:s} {1:s} --wait-until Configured".format(fsm_poll_cmd, LPM), ""))
        targets = list(ICIS)
        targets.extend(APVES)
        targets.extend(PIS)
        for target in targets:
            cmds.append(("{0:s} {1:s} --wait-until Configured".format(fsm_poll_cmd, target), ""))

        #----------

        # Enable iCIs, APVEs, PIs.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Enabling iCIs, APVEs, PIs'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list(ICIS)
        targets.extend(APVES)
        targets.extend(PIS)
        if self.mode == "sequential":
            for target in targets:
                cmds.append(("{0:s} {1:s} Enable".format(fsm_ctrl_cmd, target), ""))
        elif self.mode == "parallel":
            target_list = " ".join(targets)
            cmds.append(("parallel --jobs {0:d} {1:s} {{}} Enable ::: {2:s}".
                         format(len(target_list), fsm_ctrl_cmd, target_list), ""))

        # Enable LPM (as last!).
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Enabling LPM'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        cmds.append(("{0:s} {1:s} Enable".format(fsm_ctrl_cmd, LPM), ""))

        #----------

        cmds = add_cwd_to_cmds(cmds)
        if self.verbose:
            cmds = append_piece_to_cmds(cmds, "--verbose")

        runner = CommandRunner(cmds)
        runner.run()

        # End of main().

    # End of class TCDSConfigForTestLPM.

###############################################################################

if __name__ == "__main__":

    description = "Master script to configure and enable an LPM driving several iCIs, "
    "APVEs, and PIs to test the LPM. "
    "NOTE: In case the boards have not yet been 'bootstrapped', this "
    "also tests the bootstrap procedure."
    usage = "usage: %prog [options]"
    res = TCDSConfigForTestLPM(description, usage).run()
    print "Done"
    sys.exit(res)

###############################################################################
