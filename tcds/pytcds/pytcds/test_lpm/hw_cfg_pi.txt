#----------------------------------------------------------------------
# PI configuration file for APVE tests.
#----------------------------------------------------------------------

# BUG BUG BUG
# For the moment everything is commented out in order to test the
# default configuration done in the PIController.
# BUG BUG BUG end

# Logging mode etc.
# ttcspy.logging_control.logging_buffer_mode 0x00000000
# ttcspy.logging_control.logging_mode        0x00000001
# ttcspy.logging_control.logging_logic       0x00000000

# Trigger mask.
# ttcspy.trigger_mask.l1a                 0x00000000
# ttcspy.trigger_mask.add_all             0x00000000
# ttcspy.trigger_mask.brc_all             0x00000000
# ttcspy.trigger_mask.brc_bc0             0x00000000
# ttcspy.trigger_mask.brc_ec0             0x00000000
# ttcspy.trigger_mask.brc_dddd            0x00000000
# ttcspy.trigger_mask.brc_dddd_all        0x00000000
# ttcspy.trigger_mask.brc_tt              0x00000000
# ttcspy.trigger_mask.brc_tt_all          0x00000000
# ttcspy.trigger_mask.brc_val0            0x00000001
# ttcspy.trigger_mask.brc_val1            0x00000000
# ttcspy.trigger_mask.err_sng             0x00000000
# ttcspy.trigger_mask.err_dbl             0x00000000
# ttcspy.trigger_mask.err_com             0x00000000

#----------------------------------------------------------------------
