# This is the overall directory for all address table files.
from pytcds.utils.tcds_constants import ADDRESS_TABLE_DIR

# The VME address table for the RFRXD.
ADDRESS_TABLE_FILE_NAME_RFRXD = "%s/%s" % \
                                (ADDRESS_TABLE_DIR,
                                 "device_address_table_vme_rfrxd.txt")
