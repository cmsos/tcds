###############################################################################
## Utilities related to the RFRXD VME board.
###############################################################################

from pytcds.rfrxd.tcds_rfrxd_constants import ADDRESS_TABLE_FILE_NAME_RFRXD
from pytcds.utils.tcds_constants import board_names
from pytcds.utils.tcds_constants import BOARD_TYPE_RFRXD
from pytcds.utils.tcds_vmeboard import TCDSVME
from pytcds.utils.tcds_utils_vme import get_hw_vme

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class RFRXD(TCDSVME):

    @staticmethod
    def get_base_address(slot_number):
        res = (slot_number << 20)
        # End of get_base_address().
        return res

    def __init__(self, board_type, vme_chain, vme_unit, slot_number, nodes, verbose=False):
        base_address = RFRXD.get_base_address(slot_number)
        super(RFRXD, self).__init__(board_type, vme_chain, vme_unit, slot_number, base_address, nodes, verbose)
        self.slot_number = slot_number
        # End of __init__().

    def get_firmware_version(self, which="system"):
        res = "n/a"
        if which == "system":
            # NOTE: The decimal (!) printed format of the firmware
            # version spells the firmware date as yyyymmdd.
            tmp_lo = self.read("firmware_id_lo")
            tmp_hi = self.read("firmware_id_hi")
            tmp = (tmp_hi << 16) + tmp_lo;
            res = "{0:d}".format(tmp)
        # End of get_firmware_version()
        return res

    def get_firmware_date(self, which=None):
        res = "n/a"
        # End of get_firmware_date()
        return res

    # End of class RFRXD.

###############################################################################

def get_rfrxd_hw(vme_chain,
                 vme_unit,
                 vme_slot,
                 verbose=False):

    rfrxd = get_hw_vme(BOARD_TYPE_RFRXD,
                       ADDRESS_TABLE_FILE_NAME_RFRXD,
                       vme_slot,
                       vme_chain,
                       vme_unit,
                       verbose,
                       RFRXD)

    return rfrxd

###############################################################################
