#!/usr/bin/env python

###############################################################################
## Utility to analyze the results from tcds_pi_ttc_idelay_scan.py.
###############################################################################

import glob
import json
import sys

from pytcds.utils.tcds_cmd_line_base import CmdLineBase

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class Analyser(CmdLineBase):

    def setup_parser_custom(self):
        pass

    def main(self):

        # Get a list of all files that sound as if they contain scan
        # results.
        name_base = "ttc_idelay_scan_*.json"
        file_names = glob.glob(name_base)
        if self.verbose:
            msg = "Found {0:d} PI TTC idelay scan results files"
            print msg.format(len(file_names))

        #----------

        # Load all the data.
        data = {}
        for file_name in file_names:
            with open(file_name, 'r') as in_file:
                tmp = json.load(in_file)
                target_id = tmp['target']
            data[target_id] = tmp

        #----------

        tmp = [len(i) for i in data.keys()]
        max_len = max(tmp)
        idelay_min = 0x00
        idelay_max = 0x1f
        numbers = range(idelay_min, idelay_max + 1)
        number_strings = ["0x{0:02x}".format(i) for i in numbers]
        number_keys = [str(i) for i in numbers]
        header = "{0:s} {1:s}".format(max_len * " ", " ".join(number_strings))
        print header
        keys = data.keys()
        for key in sorted(keys, key=self.sorter):
        # for key in sorted(keys):
            results = data[key]
            tmp_bool = [results['results'][i] for i in number_keys]
            tmp_str = ["{0:4d}".format(int(i)) for i in tmp_bool]
            msg = "{1:{0:d}s} {2:s}".format(max_len, key, " ".join(tmp_str))
            print msg

        #----------

        # End of main().

    def sorter(self, pi_name):
        pi_info = self.setup.hw_targets[pi_name]
        rack = self.setup.hw_targets[pi_name].rack
        crate = self.setup.hw_targets[pi_name].crate
        slot = self.setup.hw_targets[pi_name].slot
        return (rack, crate, slot)

    # End of class Analyser.

###############################################################################

if __name__ == "__main__":

    desc_str = "Utility to analyze the results from tcds_pi_ttc_idelay_scan.py."

    res = Analyser(desc_str).run()

    print "Done"
    sys.exit(res)

###############################################################################
