#!/usr/bin/env python

###############################################################################

import random
import sys
import time

from pytcds.cpm.tcds_cpm import get_cpm_hw
from pytcds.utils.tcds_cmd_line_base import CmdLineBase

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

def check_value(val_str):
    value = int(val_str)
    if value < 1:
        msg_base = "Sorry, but {0:d} as duration" \
                   " or number of transitions" \
                   " does not make sense."
        raise argparse.ArgumentTypeError(msg_base.format(value))
    return value

###############################################################################

class PITTC(CmdLineBase):

    def __init__(self, description=None, usage=None, epilog=None):
        super(PITTC, self).__init__(description, usage, epilog)
        self.num_transitions = None
        # End of pre_hook().

    def setup_parser_custom(self):
        super(PITTC, self).setup_parser_custom()
        parser = self.parser

        # Add the number of transitions to generate.
        help_str = "The number of L1As to generate. " \
                   "Default: 1."
        parser.add_argument("--num-transitions",
                            type=check_value,
                            default=1,
                            help=help_str)

        # Add the desired (approximate) time window the whole sequence
        # should take.
        help_str = "Desired (approximate) time window " \
                   "for the full sequence (in seconds). " \
                   "Default: as little as possible."
        parser.add_argument("--duration",
                            type=check_value,
                            default=None,
                            help=help_str)

        # End of setup_parser_custom().

    def handle_args(self):
        super(PITTC, self).handle_args()
        # Extract the number of transitions requested.
        self.num_transitions = int(self.args.num_transitions)
        self.duration = self.args.duration
        # End of handle_args().

    def main(self):
        self.validate_target(target_type=CmdLineBase.TARGET_TYPE_HW)

        #----------

        board_info = self.setup.hw_targets[self.target]
        network_address = board_info.dns_alias
        controlhub_address = board_info.controlhub
        cpm = get_cpm_hw(network_address, controlhub_address, verbose=self.verbose)

        if not cpm:
            self.error("Could not connect to {0:s}.".format(self.ip_address))

        sleep_time = None
        if not self.duration is None:
            sleep_time = self.duration
            if self.num_transitions > 1:
                sleep_time = 1. * self.duration / (self.num_transitions - 1)

        helper = max(self.num_transitions / 10, 1)
        for i in xrange(self.num_transitions):

            reg_name = "cpmt1.ipm.main.ipbus_requests"
            cpm.write(reg_name, 0x100)

            if sleep_time:
                time.sleep(sleep_time)

            if (i + 1) >= helper:
                perc = 100. * (i + 1) / self.num_transitions
                print "{0:5.1f}% done".format(perc)
                helper += max(self.num_transitions / 10, 1)

        # End of main().

    # End of class PITTC.

###############################################################################

if __name__ == "__main__":

    desc_str = "Send L1As " \
               "in order to fill (part of) the TTC history."
    res = PITTC(desc_str).run()
    print "Done"
    sys.exit(res)

###############################################################################
