#!/usr/bin/env python

###############################################################################
## Utility to scan the TTC idelay setting on a (FC7) PI and find the
## 'locking' range.
###############################################################################

import json
import sys
import time

from pytcds.pi.tcds_pi import get_pi_hw
from pytcds.utils.tcds_cmd_line_base import CmdLineBase
from pytcds.utils.tcds_utils_networking import resolve_network_address

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class Scanner(CmdLineBase):

    IDELAY_MIN = 0x00
    IDELAY_MAX = 0x1f
    SLEEP_TIME = 1
    NUM_TRIES = 1000

    def main(self):
        self.validate_target(target_type=CmdLineBase.TARGET_TYPE_HW)
        board_info = self.setup.hw_targets[self.target]
        network_address = board_info.dns_alias
        controlhub_address = board_info.controlhub

        # NOTE: The use of the primary PI input is hard-coded here.
        # which_ttc = "pri"
        which_ttc = "sec"

        pi = get_pi_hw(network_address, controlhub_address, verbose=self.verbose)
        if not pi:
            self.error("Could not connect to target '{0:s}'".format(self.target))

        #----------

        results = {}

        # Select the correct TTC source.
        tmp = 0
        if which_ttc == "sec":
            tmp = 1
        pi.write("pi.lpm_input_select", tmp)

        # Enabling the TTCSpy in the PI.
        pi.write("pi.ttc_spy.control.enabled", 0x1)

        for idelay in xrange(Scanner.IDELAY_MIN, Scanner.IDELAY_MAX + 1):

            pi.write("pi.ttc_idelay_ctrl.ttc_idelay_ld_lpm_{0:s}".format(which_ttc), 0x0)
            pi.write("pi.ttc_idelay_ctrl.ttc_idelay_tap_lpm_{0:s}".format(which_ttc), idelay)
            pi.write("pi.ttc_idelay_ctrl.ttc_idelay_ld_lpm_{0:s}".format(which_ttc), 0x1)
            time.sleep(Scanner.SLEEP_TIME)
            pi.write("pi.ttc_idelay_ctrl.ttc_idelay_ld_lpm_{0:s}".format(which_ttc), 0x0)

            # ASSERT ASSERT ASSERT
            assert pi.read("pi.ttc_idelay_tap_stat.ttc_tap_stat_lpm_{0:s}".format(which_ttc)) == idelay
            # ASSERT ASSERT ASSERT end

            num_aligned = 0
            for i in xrange(Scanner.NUM_TRIES):
                val = pi.read("pi.ttc_decoder.status.ttc_stream_aligned")
                aligned = (val == 0x1)
                if aligned:
                    num_aligned += 1

            results[idelay] = (num_aligned == Scanner.NUM_TRIES)
            print "0x{0:02x}: {1:b}".format(idelay, aligned)

        #----------

        # Store all results together with the target identifier.
        data = {
            'target' : board_info.identifier,
            'results' : results
            }
        name_base = "ttc_idelay_scan_{0:s}.json"
        out_file_name = name_base.format(board_info.identifier)
        with open(out_file_name, 'w') as out_file:
            json.dump(data, out_file)

        #----------

        # End of main().

    # End of class Scanner.

###############################################################################

if __name__ == "__main__":

    desc_str = "Helper script to scan the 'locking' TTC idelay range of an FC7-PI."

    res = Scanner(desc_str).run()

    print "Done"
    sys.exit(res)

###############################################################################
