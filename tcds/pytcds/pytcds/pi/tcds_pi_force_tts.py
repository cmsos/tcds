#!/usr/bin/env python

###############################################################################
## Configure the PI to report READY on its TTS output.
###############################################################################

import random
import sys

from pytcds.pi.tcds_pi import get_pi_hw
from pytcds.utils.tcds_cmd_line_base import CmdLineBase

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class PITTS(CmdLineBase):

    # The list of allowed TTS states and the corresponding state
    # values.
    TTS_STATE_MAP = {
        "WARNING"     : 0x1,
        "OUT-OF-SYNC" : 0x2,
        "OOS"         : 0x2,
        "BUSY"        : 0x4,
        "READY"       : 0x8,
        "ERROR"       : 0xc,
        }
    TTS_VAL_MIN = 0x00
    TTS_VAL_MAX = 0xff
    tts_val_range = range(TTS_VAL_MIN, TTS_VAL_MAX + 1)
    TTS_STATE_MAP.update(dict(zip(["0X{:X}".format(i) for i in tts_val_range],
                                  tts_val_range)))

    TTS_CHOICES = TTS_STATE_MAP.keys()
    TTS_CHOICES.append("RANDOM")
    TTS_CHOICES.sort()

    def __init__(self, description=None, usage=None, epilog=None):
        super(PITTS, self).__init__(description, usage, epilog)
        # The target TTS state comes from the command line.
        self.tts_state = None
        # End of pre_hook().

    def setup_parser_custom(self):
        super(PITTS, self).setup_parser_custom()
        parser = self.parser

        # Add the target TTS state argument.
        help_str = "The target TTS state"
        parser.add_argument("tts_state",
                            type=str.upper,
                            choices=PITTS.TTS_CHOICES,
                            help=help_str)
        # End of setup_parser_custom().

    def handle_args(self):
        super(PITTS, self).handle_args()
        # Extract the target TTS state.
        self.tts_state = self.args.tts_state.upper()
        # End of handle_args().

    def main(self):
        self.validate_target(target_type=CmdLineBase.TARGET_TYPE_HW)

        #----------

        board_info = self.setup.hw_targets[self.target]
        network_address = board_info.dns_alias
        controlhub_address = board_info.controlhub
        pi = get_pi_hw(network_address, controlhub_address, verbose=self.verbose)

        if not pi:
            self.error("Could not connect to {0:s}.".format(self.network_address))

        tts_state = self.tts_state
        if tts_state == "RANDOM":
            tts_state = random.choice(PITTS.TTS_STATE_MAP.keys())

        if self.verbose:
            print "Forcing TTS state to {0:s}.".format(tts_state)

        # Configure the TTS player.
        desired_values = {
            "pi.tts_output_control.tts_force_player.wrap_around": 0,
            "pi.tts_output_control.tts_force_player.last_entry": 0,
            }
        # desired_values = {
        #     "pi.tts_output_control.tts_force_player.wrap_around": 0,
        #     "pi.tts_output_control.tts_force_player.last_entry": 2,
        #     "pi.tts_output_control.tts_force_player.pattern.entry0.value": 8,
        #     "pi.tts_output_control.tts_force_player.pattern.entry0.duration": 1000,
        #     "pi.tts_output_control.tts_force_player.pattern.entry1.value": 4,
        #     "pi.tts_output_control.tts_force_player.pattern.entry1.duration": 1000,
        #     "pi.tts_output_control.tts_force_player.pattern.entry2.value": 8,
        #     "pi.tts_output_control.tts_force_player.pattern.entry2.duration": 1000,
        #     }
        current_values = {}
        for reg_name in desired_values.keys():
            current_values[reg_name] = pi.read(reg_name)

        for (reg_name, val) in desired_values.iteritems():
            pi.write(reg_name, val)
        pi.write("pi.tts_output_control.tts_force_player.pattern.entry0.value",
                 PITTS.TTS_STATE_MAP[tts_state])

        # Now switch from the TTS OR output to the TTS player output.
        pi.write("pi.tts_output_control.tts_out_enable", 0x0)
        # And make sure the TTS player is enabled. (And force
        # disable-enable the player to load the above settings.)
        if current_values != desired_values:
            pi.write("pi.tts_output_control.tts_force_player.enabled", 0)
        pi.write("pi.tts_output_control.tts_force_player.enabled", 1)

        # End of main().

    # End of class PITTS.

###############################################################################

if __name__ == "__main__":

    desc_str = "Force PI TTS output to given state."
    res = PITTS(desc_str).run()
    print "Done"
    sys.exit(res)

###############################################################################
