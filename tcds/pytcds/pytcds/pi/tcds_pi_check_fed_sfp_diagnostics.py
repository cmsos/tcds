#!/usr/bin/env python

###############################################################################
## A helper script to check the SFP diagnostics on the FED-to-PI SFPs
## on a PI.
###############################################################################

import sys
import time

from pytcds.pi.tcds_pi import get_pi_hw
from pytcds.utils.tcds_cmd_line_base import CmdLineBase

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class TCDSChecker(CmdLineBase):

    def main(self):
        self.validate_target(target_type=CmdLineBase.TARGET_TYPE_HW)
        board_info = self.setup.hw_targets[self.target]
        network_address = board_info.dns_alias
        controlhub_address = board_info.controlhub
        pi = get_pi_hw(network_address, controlhub_address, verbose=self.verbose)
        if not pi:
            self.error("Could not connect to {0:s}.".format(self.network_address))

        for fed_num in xrange(1, 11):
            reg_name_base = "pi.tts_link_status.fed{0:d}".format(fed_num)
            aligned = pi.read(reg_name_base + ".aligned")

            reg_name_base = "pi.sfp_monitoring.fed{0:d}".format(fed_num)
            mod_abs = pi.read(reg_name_base + ".mod_abs")
            rx_los = pi.read(reg_name_base + ".rx_los")
            tx_fault = pi.read(reg_name_base + ".tx_fault")

            somethings_dodgy = False
            if aligned:
                if mod_abs:
                    print "FED{0:d }TTS link aligned without SFP present?".format(fed_num)
                    somethings_dodgy = True
                if rx_los:
                    print "FED{0:d }TTS link aligned without input signal?".format(fed_num)
                    somethings_dodgy = True

            if mod_abs:
                if not rx_los:
                    print "FED{0:d }TTS link detected input signal withhout SFP present?".format(fed_num)
                    somethings_dodgy = True

            if not somethings_dodgy:
                print "{0:s}, FED input {1:d} is fine".format(self.target, fed_num)

        # End of main().

    # End of class TCDSChecker.

###############################################################################

if __name__ == "__main__":

    desc_str = "A helper script to check the SFP diagnostics" \
               " on the FED-to-PI SFPs on a PI."
    res = TCDSChecker(desc_str).run()

    print "Done"
    sys.exit(res)

###############################################################################
