#!/usr/bin/env python

###############################################################################
## Utility to quickly check the configuration of the
## PLL-reset-upon-Configure for a given list of PIControllers.
###############################################################################

import ast
import ConfigParser
import httplib
import os
import json
import sys
import urllib2
# from xml.dom import minidom

from pytcds.utils.tcds_cmd_line_base import CmdLineBase
# from pytcds.utils.tcds_utils_soap import build_xdaq_soap_command_message
# from pytcds.utils.tcds_utils_soap import extract_xdaq_soap_fault
# from pytcds.utils.tcds_utils_soap import send_xdaq_soap_message
# from pytcds.utils.tcds_utils_soap import SOAP_PROTOCOL_VERSION_DEFAULT

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class Checker(CmdLineBase):

    def pre_hook(self):
        # The target and command come from the command line.
        self.targets = None
        self.command = None
        # The following are read from the config file.
        self.possible_targets = {}
        # End of pre_hook().

    def setup_parser(self):
        # Start with the basic parser configuration.
        super(Checker, self).setup_parser()

        # Add the option to choose a custom cfg file.
        help_str = "Configuration file to use [default: %default]"
        self.parser.add_option("--cfg-file",
                               action="store",
                               default="tcds_setup.cfg",
                               help=help_str)
        # End of setup_parser().

    def handle_args(self):
        # NOTE: Do not call the parent handle_args() since we are not
        # requiring an IP address (unlike pretty much all other TCDS
        # scripts).

        # On the command line we require one target application.
        if len(self.args) < 1:
            self.error("At least one arguments is required: " \
                       "a target application.")
        else:
            self.targets = [i.strip() for i in self.args]

        self.cfg_file_name = self.opts.cfg_file
        # End of handle_args().

    def main(self):

        # Host parameters etc. come from the configuration file.
        cfg_parser = ConfigParser.SafeConfigParser()
        config_file_name = self.cfg_file_name
        if self.verbose:
            print "Reading configuration from '{0:s}'".format(config_file_name)
        if not os.path.exists(config_file_name):
            self.error("Config file '{0:s}' does not exist.".format(config_file_name))
        cfg_parser.read(config_file_name)

        # Find all possible target applications.
        targets = cfg_parser.items("targets")
        for (target_name, target_dict) in targets:
            self.possible_targets[target_name] = ast.literal_eval(target_dict)
        if self.verbose:
            if len(self.possible_targets):
                print "Found the following target applications: "
                target_names = self.possible_targets.keys()
                target_names.sort()
                for target_name in target_names:
                    print "  {0:s}".format(target_name)
            else:
                print "No target applications found"

        #----------

        # Check if the target names are valid.
        for target in self.targets:
            if not target in self.possible_targets.keys():
                msg = "'{0:s}' is not a valid target application. " \
                      "Valid targets are read from the config file '{1:s}' " \
                      "and are '{2:s}'."
                self.error(msg.format(target,
                                      self.cfg_file_name,
                                      "', '".join(sorted(self.possible_targets.keys()))))

        #----------

        # Now actually do what we have been asked to do.
        results = {}
        for target in self.targets:
            target_details = self.possible_targets[target]
            host_name = target_details["host"]
            port_number = target_details["port"]
            lid_number = target_details["lid"]

            tmp = "http://{0:s}:{1:d}/urn:xdaq-application:lid={2:d}/update"
            url = tmp.format(host_name, port_number, lid_number)

            req = urllib2.Request(url)
            opener = urllib2.build_opener()
            f = opener.open(req)
            contents = json.load(f)
            reset_pll_on_configure = contents["Application configuration"]["Reset TTC PLL at the start of Configure"]

            results[target] = reset_pll_on_configure

        #----------

        keys = results.keys()
        keys.sort()
        for key in keys:
            print "{0:s}: {1:s}".format(key, results[key])

        # End of main().

    # End of class Checker.

###############################################################################

if __name__ == "__main__":

    desc_str = "Utility to quickly check the configuration of the " \
               "PLL-reset-upon-Configure for a given list of PIControllers."
    usage_str = "usage: %prog device [options] pi0 ... pin"

    Checker(desc_str, usage_str).run()

    print "Done"

###############################################################################
