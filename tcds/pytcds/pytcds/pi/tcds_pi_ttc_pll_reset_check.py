#!/usr/bin/env python

###############################################################################
## Utility to check that the reset of the TTC PLL brings the phase of
## the TTC stream to the same place every time.
###############################################################################

import math
import sys
import time

import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt

from pytcds.pi.tcds_pi import get_pi_hw
from pytcds.utils.tcds_cmd_line_base import CmdLineBase
from pytcds.utils.tcds_utils_networking import resolve_network_address

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class Scanner(CmdLineBase):

    def handle_args(self):
        # One argument is requited. It has to sound like a network
        # address.
        if len(self.args) != 1:
            self.error("One argument is required: " \
                       "the IP address of the device.")
        else:
            # Extract the IP address.
            ip_address_arg = self.args[0]
            ip_address = resolve_network_address(ip_address_arg)
            if not ip_address:
                msg = "'{0:s}' does not sound like an IP/network address.".format(ip_address_arg)
                self.error(msg)
            else:
                self.ip_address = ip_address
        # End of handle_args().

    def main(self):
        pi = get_pi_hw(self.ip_address, verbose=self.verbose)
        if not pi:
            self.error("Could not connect to {0:s}.".format(self.ip_address))

        #----------

        # Reset the 500 MHz sampling clock and check that it is
        # locked.
        pi.write("pi.ttc_phase_mon.clk_500_reset", 0x0)
        pi.write("pi.ttc_phase_mon.clk_500_reset", 0x1)
        pi.write("pi.ttc_phase_mon.clk_500_reset", 0x0)
        locked = pi.read("pi.ttc_phase_mon.clk_500_locked")
        if (locked != 0x1):
            self.error("Scan problem: 500 MHz clock not locked.")

        # Reset the two White-Rabbit MMCMs and check that they lock.
        pi.write("pi.ttc_phase_mon.mmcm1_reset", 0x0)
        pi.write("pi.ttc_phase_mon.mmcm1_reset", 0x1)
        pi.write("pi.ttc_phase_mon.mmcm1_reset", 0x0)
        pi.write("pi.ttc_phase_mon.mmcm2_reset", 0x0)
        pi.write("pi.ttc_phase_mon.mmcm2_reset", 0x1)
        pi.write("pi.ttc_phase_mon.mmcm2_reset", 0x0)
        locked1 = pi.read("pi.ttc_phase_mon.mmcm1_locked")
        locked2 = pi.read("pi.ttc_phase_mon.mmcm2_locked")
        if (locked1 != 0x1):
            self.error("Scan problem: White-Rabbit MMCM1 not locked.")
        if (locked2 != 0x1):
            self.error("Scan problem: White-Rabbit MMCM2 not locked.")

        NUM_MEAS_WR = 0xf
        pi.write("pi.ttc_phase_mon.measurement_duration", NUM_MEAS_WR)
        pi.write("pi.ttc_phase_mon.measurement_enable", 0x1)

        NUM_STEPS = 10
        NUM_SAMPLES = 100
        NUM_SHIFT_STEPS = 10

        for k in xrange(NUM_SHIFT_STEPS):
            # Strobe to make the shift happen.
            pi.write("pi.ttc_phase_mon.shift_strobe", 0x1)
            pi.write("pi.ttc_phase_mon.shift_strobe", 0x0)
            time.sleep(.1)
            done = pi.read("pi.ttc_phase_mon.shift_done")
            if (done != 0x1):
                self.error("Scan problem: phase shift did not succeed.")

        print "Checking..."

        results = {}
        for i in xrange(NUM_STEPS):
            # print "  shifting..."
            # for k in xrange(NUM_SHIFT_STEPS):
            #     # Strobe to make the shift happen.
            #     pi.write("pi.ttc_phase_mon.shift_strobe", 0x1)
            #     pi.write("pi.ttc_phase_mon.shift_strobe", 0x0)
            #     time.sleep(.1)
            #     done = pi.read("pi.ttc_phase_mon.shift_done")
            #     if (done != 0x1):
            #         self.error("Scan problem: phase shift did not succeed.")

            # print "  resetting..."
            # # Reset the main TTC clock PLL so we should end up back at
            # # zero.
            # pi.write("user.ttc_clock_pll_reset", 0x0)
            # pi.write("user.ttc_clock_pll_reset", 0x1)
            # pi.write("user.ttc_clock_pll_reset", 0x0)
            # time.sleep(1.)

            print "  sampling..."
            for j in xrange(NUM_SAMPLES):
                # NOTE: Turn everything into floating point numbers.

                # Take a snapshot of the phase-monitoring output.
                phase_mon_val_hi = 1. * pi.read("pi.ttc_phase_mon.clk_160_mon_res_hi")
                phase_mon_val_lo = 1. * pi.read("pi.ttc_phase_mon.clk_160_mon_res_lo")
                print "DEBUG JGH ", i, "/", j, ": lo = ", phase_mon_val_lo
                print "DEBUG JGH ", i, "/", j, ": hi = ", phase_mon_val_hi

                # White-Rabbit version.
                pi.write("pi.ttc_phase_mon.measurement_enable", 0x0)
                pi.write("pi.ttc_phase_mon.measurement_enable", 0x1)
                time.sleep(.1)
                data_valid = pi.read("pi.ttc_phase_mon.measurement_valid")
                if not data_valid:
                    self.error("Scan problem: White-Rabbit measurement not valid.")
                # phase_mon_val_wr = (1. * pi.read("pi.ttc_phase_mon.measurement_value")) / NUM_MEAS_WR
                phase_mon_val_wr = (1. * pi.read("pi.ttc_phase_mon.measurement_value"))
                # print "DEBUG JGH ", i, "/", j, ": wr = ", phase_mon_val_wr
                print "DEBUG JGH ", i, "/", j, ": wr = ", hex(int(phase_mon_val_wr))

                try:
                    results[i].append([phase_mon_val_hi, phase_mon_val_lo, phase_mon_val_wr])
                except KeyError:
                    results[i] = [[phase_mon_val_hi, phase_mon_val_lo, phase_mon_val_wr]]

        #----------

        # Processing and plotting.

        print "Plotting..."

        fig = plt.figure()
        ax = fig.add_subplot(111)
        x = results.keys()
        y_hi = []
        yerr_hi = []
        y_lo = []
        yerr_lo = []
        y_wr = []
        yerr_wr = []
        for (key, vals) in results.iteritems():
            vals_hi = [i[0] for i in vals]
            vals_lo = [i[1] for i in vals]
            vals_wr = [i[2] for i in vals]
            mean_hi = sum(vals_hi) / len(vals_hi)
            ss_hi = sum((x - mean_hi)**2 for x in vals_hi)
            std_hi = (ss_hi / len(vals_hi))**.5
            y_hi.append(mean_hi)
            yerr_hi.append(std_hi)
            mean_lo = sum(vals_lo) / len(vals_lo)
            ss_lo = sum((x - mean_lo)**2 for x in vals_lo)
            std_lo = (ss_lo / len(vals_lo))**.5
            y_lo.append(mean_lo)
            yerr_lo.append(std_lo)
            mean_wr = sum(vals_wr) / len(vals_wr)
            ss_wr = sum((x - mean_wr)**2 for x in vals_wr)
            std_wr = (ss_wr / len(vals_wr))**.5
            y_wr.append(mean_wr)
            yerr_wr.append(std_wr)
            print "DEBUG JGH ", key, ": y_hi = ", y_hi[-1]
            print "DEBUG JGH ", key, ": y_lo = ", y_lo[-1]
            print "DEBUG JGH ", key, ": y_wr = ", y_wr[-1]
        # ax.errorbar(x, y_hi, yerr=yerr_hi, fmt="o", label="hi")
        # ax.errorbar(x, y_lo, yerr=yerr_lo, fmt="o", label="lo")
        ax.errorbar(x, y_wr, yerr=yerr_wr, fmt="o", label="wr")
        ax.set_xlabel("step number")
        ax.set_ylabel("phase-mon count [a.u.]")
        ax.grid(True)
        ax.legend(numpoints=1)
        fig.savefig("pi_pll_check_trend.png")

        #----------

        # End of main().

    # End of class Scanner.

###############################################################################

if __name__ == "__main__":

    desc_str = "Helper script to test the TTC-stream phase on the PI after PLL resets."
    usage_str = "usage: %prog [OPTIONS] IP"

    Scanner(desc_str, usage_str).run()

    print "Done"

###############################################################################
