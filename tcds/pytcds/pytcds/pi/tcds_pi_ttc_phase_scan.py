#!/usr/bin/env python

###############################################################################
## Helper script to test the TTC-stream phase monitoring on the PI.
###############################################################################

import commands
import math
import pickle
import simplejson
import sys
import time
import urllib2

import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt

from pytcds.pi.tcds_pi import get_pi_hw
from pytcds.utils.tcds_cmd_line_base import CmdLineBase
from pytcds.utils.tcds_utils_networking import resolve_network_address

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class Scanner(CmdLineBase):

    def handle_args(self):
        # One argument is requited. It has to sound like a network
        # address.
        if len(self.args) != 1:
            self.error("One argument is required: " \
                       "the IP address of the device.")
        else:
            # Extract the IP address.
            ip_address_arg = self.args[0]
            ip_address = resolve_network_address(ip_address_arg)
            if not ip_address:
                msg = "'{0:s}' does not sound like an IP/network address.".format(ip_address_arg)
                self.error(msg)
            else:
                self.network_address = ip_address_arg
                self.ip_address = ip_address
        # End of handle_args().

    def main(self):
        pi = get_pi_hw(self.ip_address, verbose=self.verbose)
        if not pi:
            self.error("Could not connect to {0:s}.".format(self.ip_address))

        #----------

        # Reset the main TTC clock PLL so we start from zero.
        pi.write("user.ttc_clock_pll_reset", 0x0)
        pi.write("user.ttc_clock_pll_reset", 0x1)
        pi.write("user.ttc_clock_pll_reset", 0x0)
        time.sleep(1.)

        # Reset the two White-Rabbit MMCMs and check that they lock.
        pi.write("user.ttc_phase_mon.meas_common.mmcm1_reset", 0x0)
        pi.write("user.ttc_phase_mon.meas_common.mmcm1_reset", 0x1)
        pi.write("user.ttc_phase_mon.meas_common.mmcm1_reset", 0x0)
        pi.write("user.ttc_phase_mon.meas_common.mmcm2_reset", 0x0)
        pi.write("user.ttc_phase_mon.meas_common.mmcm2_reset", 0x1)
        pi.write("user.ttc_phase_mon.meas_common.mmcm2_reset", 0x0)
        locked1 = pi.read("user.ttc_phase_mon.meas_common.mmcm1_locked")
        locked2 = pi.read("user.ttc_phase_mon.meas_common.mmcm2_locked")
        if (locked1 != 0x1):
            self.error("Scan problem: White-Rabbit MMCM1 not locked.")
        if (locked2 != 0x1):
            self.error("Scan problem: White-Rabbit MMCM2 not locked.")

        NUM_STEPS = 30
        NUM_SAMPLES = 1

        NUM_MEAS_WR = 0x10
        pi.write("user.ttc_phase_mon.meas_40.measurement_duration", NUM_MEAS_WR)
        pi.write("user.ttc_phase_mon.meas_40.measurement_enable", 0x1)
        pi.write("user.ttc_phase_mon.meas_160.measurement_duration", NUM_MEAS_WR)
        pi.write("user.ttc_phase_mon.meas_160.measurement_enable", 0x1)

        INPUT_CLOCK = 11246. * 3564
        SHIFT_STEP = (1. / (INPUT_CLOCK * 20. * 56.)) * 1.e12
        STEP_SIZE = 56
        DIRECTION = 1

        # NORM_FACTOR = 1. * (NUM_MEAS_WR + 1)
        # NORM_FACTOR = 1. * NUM_MEAS_WR
        # CALIB_FACTOR = 1. / 500. * (15. / 16.)
        NORM_FACTOR = 1.
        CALIB_FACTOR = 1.

        # Choose the scan direction.
        if DIRECTION < 0:
            pi.write("pi.phase_shift_160.direction", 0x0)
        else:
            pi.write("pi.phase_shift_160.direction", 0x1)
        pi.write("pi.phase_shift_160.strobe", 0x0)

        print "Scanning..."

        results_40 = []
        results_160 = []
        results_phasemon = []
        for i in xrange(NUM_STEPS):
            x = DIRECTION * SHIFT_STEP * STEP_SIZE * i
            print "  stepping..."
            for k in xrange(STEP_SIZE):
                # Strobe to make the shift happen.
                pi.write("pi.phase_shift_160.strobe", 0x1)
                pi.write("pi.phase_shift_160.strobe", 0x0)
                time.sleep(.1)
                done = pi.read("pi.phase_shift_160.done")
                if (done != 0x1):
                    self.error("Scan problem: phase shift did not succeed.")

            print "  sampling..."
            data_40 = []
            data_160 = []
            for j in xrange(NUM_SAMPLES):
                # NOTE: Turn everything into floating point numbers.

                # White-Rabbit phase monitoring: 40 MHz.
                pi.write("user.ttc_phase_mon.meas_40.measurement_enable", 0x0)
                pi.write("user.ttc_phase_mon.meas_40.measurement_reset", 0x1)
                pi.write("user.ttc_phase_mon.meas_40.measurement_reset", 0x0)
                pi.write("user.ttc_phase_mon.meas_40.measurement_enable", 0x1)
                time.sleep(1.)
                phase_mon_val_40 = float(pi.read("user.ttc_phase_mon.meas_40.measurement_value"))
                phase_mon_val_40 /= NORM_FACTOR
                phase_mon_val_40 *= CALIB_FACTOR

                print "DEBUG JGH  40: x = {0:8.2f}, value = 0x{1:08x}".format(x, int(phase_mon_val_40))
                try:
                    data_40.append(phase_mon_val_40)
                except KeyError:
                    data_40 = [phase_mon_val_40]

                # White-Rabbit phase monitoring: 160 MHz.
                pi.write("user.ttc_phase_mon.meas_160.measurement_enable", 0x0)
                pi.write("user.ttc_phase_mon.meas_160.measurement_reset", 0x1)
                pi.write("user.ttc_phase_mon.meas_160.measurement_reset", 0x0)
                pi.write("user.ttc_phase_mon.meas_160.measurement_enable", 0x1)
                time.sleep(1.)
                phase_mon_val_160 = float(pi.read("user.ttc_phase_mon.meas_160.measurement_value"))
                phase_mon_val_160 /= NORM_FACTOR
                phase_mon_val_160 *= CALIB_FACTOR

                print "DEBUG JGH 160: x = {0:8.2f}, value = 0x{1:08x}".format(x, int(phase_mon_val_160))
                try:
                    data_160.append(phase_mon_val_160)
                except KeyError:
                    data_160 = [phase_mon_val_160]


            # # Now snapshot the PhaseMon (i.e., external) measurement.
            # print "Snapshotting PhaseMon..."
            # cmd = "../test_phasemon/tcds_do_phasemon_snapshot.py --cfg-file=../utils/tcds_setup_tcdslab.cfg phasemon_dev"
            # (status, output) = commands.getstatusoutput(cmd)
            # if status != 0:
            #     print >> sys.stderr, "ERROR {0:s}".format(output)
            #     sys.exit(1)
            # # Scrape the results off the HyperDAQ page.
            # host_name = "cmstcdslab.cern.ch"
            # port_number = 2101
            # lid_number = 100
            # tmp = "http://{0:s}:{1:d}/urn:xdaq-application:lid={2:d}/update"
            # url = tmp.format(host_name, port_number, lid_number)
            # req = urllib2.Request(url)
            # opener = urllib2.build_opener()
            # f = opener.open(req)
            # contents = simplejson.load(f)
            # phases = {}
            # for (key, meas) in contents.iteritems():
            #     if key.find("channel") > -1:
            #         channel = int(key[key.find("channel") + 7:])
            #         avg = float(meas["Average phase (ps)"])
            #         sd = float(meas["Standard deviation (ps)"])
            #         sweeps = int(meas["Number of sweeps"])
            #         phases[channel] = (avg, sd, sweeps)

            results_40.append((x, data_40))
            results_160.append((x, data_160))
            # results_phasemon.append((x, phases))

        #----------

        print "Storing results..."
        out_file_name = "pi_ttc_phase_scan.pkl"
        data = {
            "40" : results_40,
            "160" : results_160,
            "phasemon" : results_phasemon
            }
        with open(out_file_name, "wb") as out_file:
            pickle.dump(data, out_file)

        #----------

        # # Processing and plotting.

        # print "Plotting..."

        # fig = plt.figure()
        # x_hi = []
        # x_lo = []
        # y_hi = []
        # yerr_hi = []
        # y_lo = []
        # yerr_lo = []
        # try:
        #     x_hi = [(DIRECTION * SHIFT_STEP * STEP_SIZE * i) for i in results_hi.keys()]
        #     for key in results_hi.keys():
        #         vals_hi = results_hi[key]
        #         mean_hi = sum(vals_hi) / len(vals_hi)
        #         ss_hi = sum((x - mean_hi)**2 for x in vals_hi)
        #         std_hi = (ss_hi / len(vals_hi))**.5
        #         y_hi.append(mean_hi)
        #         yerr_hi.append(std_hi)
        #         print "DEBUG JGH ", key, ": y_hi = ", y_hi[-1]
        #     x_lo = [(DIRECTION * SHIFT_STEP * STEP_SIZE * i) for i in results_lo.keys()]
        #     for key in results_lo.keys():
        #         vals_lo = results_lo[key]
        #         mean_lo = sum(vals_lo) / len(vals_lo)
        #         ss_lo = sum((x - mean_lo)**2 for x in vals_lo)
        #         std_lo = (ss_lo / len(vals_lo))**.5
        #         y_lo.append(mean_lo)
        #         yerr_lo.append(std_lo)
        #         print "DEBUG JGH ", key, ": y_lo = ", y_lo[-1]
        # except Exception, err:
        #     pdb.set_trace()
        # try:
        #     ax1 = fig.add_subplot(111)
        #     ax2 = None
        #     if len(x_hi):
        #         ax1.errorbar(x_hi, y_hi, yerr=yerr_hi, fmt="x", color="red", label="hi")
        #     if len(x_lo):
        #         ax2 = ax1
        #         if len(x_hi):
        #             ax2 = ax1.twinx()
        #         ax2.errorbar(x_lo, y_lo, yerr=yerr_lo, fmt="+", color="blue", label="lo")
        #     ax1.set_xlabel("phase-shift (hi) [ps]")
        #     for tl in ax1.get_yticklabels():
        #         tl.set_color("red")
        #     ax1.set_ylabel("phase-mon count [a.u.]", color="red")
        #     ax1.grid(True)
        #     ax1.legend(numpoints=1, loc="upper left")
        #     if ax2:
        #         ax2.set_ylabel("phase-mon count [a.u.]", color="blue")
        #         ax2.legend(numpoints=1, loc="upper right")
        #         for tl in ax2.get_yticklabels():
        #             tl.set_color("blue")
        #     fig.savefig("pi_phase_scan_trend.png")
        # except Exception, err:
        #     pdb.set_trace()

        #----------

        # End of main().

    # End of class Scanner.

###############################################################################

if __name__ == "__main__":

    desc_str = "Helper script to test the TTC-stream phase monitoring on the PI."
    res = Scanner(desc_str).run()
    print "Done"
    sys.exit(res)

###############################################################################
