###############################################################################

# The addresses of the boards/applications to connect to.
RF2TTC = "rf2ttc"
RFRXDS = ["rfrxd1", "rfrxd2"]

# CPM = "cpm"
# LPM = "lpm-11"
# # LPM = "lpm-12"
# ICIS = [
#     "ici-11",
#     # "ici-12",
#     # "ici-13",
#     # "ici-14",
#     # "ici-15",
#     # "ici-16",
#     # "ici-17",
#     # "ici-18"
# ]
# APVES = [
#     "apve-11",
#     # "apve-12",
#     # "apve-13",
#     # "apve-14"
# ]
# PIS = [
#     "pi-11",
#     # "pi-12",
#     # "pi-13",
#     # "pi-14",
#     # "pi-15",
#     # "pi-16",
#     # "pi-17",
#     # "pi-18"
# ]

# # The FED id of the CPM/LPM, so we can disable the readout.
# FED_ID_CPM = 1024
# FED_ID_LPM = 1
# # FED_ID_LPM = 2

# # Some misc. FED ids, so we can enable/disable 'random' inputs on the
# # PI.
# FED_IDS_MISC = [[10, 2]]

###############################################################################
