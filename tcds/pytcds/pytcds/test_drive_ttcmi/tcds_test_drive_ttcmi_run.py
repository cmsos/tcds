#!/usr/bin/env python

###############################################################################
## Master script to configure and enable a/the TTC Machine Interface.
###############################################################################

import os
import sys

from pytcds.test_drive_ttcmi.tcds_test_drive_ttcmi_constants import __file__ as constants_file_name
from pytcds.test_drive_ttcmi.tcds_test_drive_ttcmi_constants import RF2TTC
from pytcds.test_drive_ttcmi.tcds_test_drive_ttcmi_constants import RFRXDS
# from pytcds.test_drive_ttcmi.tcds_test_drive_ttcmi_constants import APVES
# from pytcds.test_drive_ttcmi.tcds_test_drive_ttcmi_constants import CPM
# from pytcds.test_drive_ttcmi.tcds_test_drive_ttcmi_constants import FED_ID_CPM
# from pytcds.test_drive_ttcmi.tcds_test_drive_ttcmi_constants import FED_ID_LPM
# from pytcds.test_drive_ttcmi.tcds_test_drive_ttcmi_constants import FED_IDS_MISC
# from pytcds.test_drive_ttcmi.tcds_test_drive_ttcmi_constants import ICIS
# from pytcds.test_drive_ttcmi.tcds_test_drive_ttcmi_constants import LPM
# from pytcds.test_drive_ttcmi.tcds_test_drive_ttcmi_constants import PIS
from pytcds.utils.tcds_cmd_line_base import CmdLineBase
from pytcds.utils.tcds_command_runner import add_cwd_to_cmds
from pytcds.utils.tcds_command_runner import CommandRunner

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

# TIME_BEFORE_FIRST_L1A = 5
# TIME_BETWEEN_L1AS = 2
# NUM_L1AS = 3

###############################################################################

def append_piece_to_cmds(cmds, piece):
    res = []
    for (cmd, doc) in cmds:
        full_cmd = cmd
        if cmd.endswith(".py"):
            full_cmd = "%s %s" % (cmd, piece)
        res.append((full_cmd, doc))
    # End of append_piece_to_cmds().
    return res

###############################################################################

class TestDriver(CmdLineBase):

    # MODE_CHOICES = ["cpm", "lpm", "ici"]

    def __init__(self, description=None, usage=None, epilog=None):
        super(TestDriver, self).__init__(description, usage, epilog)
        # self.mode = None
        # self.with_daq = False
        # End of __init__().

    def setup_parser_custom(self):
        pass
    #     parser = self.parser
        # # Add the choice of running mode.
        # help_str = "Running mode"
        # parser.add_argument("mode",
        #                     type=str,
        #                     action="store",
        #                     choices=TestDriver.MODE_CHOICES,
        #                     help=help_str)
        # # Add the choice to run with DAQ/FEDkit or not.
        # help_str = "Run with DAQ/FEDkit"
        # parser.add_argument("--with-daq",
        #                     action="store_true",
        #                     help=help_str)
        # End of setup_parser_custom().

    # def handle_args(self):
    #     super(TestDriver, self).handle_args()
        # # Extract the driving mode.
        # self.mode = self.args.mode
        # # Extract the DAQ/FEDkit choice.
        # self.with_daq = self.args.with_daq
        # End of handle_args().

    def main(self):
        sep_line = "-" * 70

        test_dir_name = os.path.dirname(constants_file_name)
        # tmp = "".join(["{0:d}&{1:d}%".format(i[0], i[1]) for i in FED_IDS_MISC])
        # fed_enable_mask = "{0:d}&0%{1:d}&0%{2:s}".format(FED_ID_CPM, FED_ID_LPM, tmp)

        # pm = None
        # if self.mode == "lpm":
        #     pm = LPM
        # elif self.mode == "cpm":
        #     pm = CPM

        #----------

        fsm_ctrl_cmd = "../utils/tcds_fsm_control.py --setup-file {0:s} --location {1:s}".format(self.setup_file_name, self.location_name)
        fsm_poll_cmd = "../utils/tcds_fsm_poller.py --setup-file {0:s} --location {1:s}".format(self.setup_file_name, self.location_name)
        # pi_force_tts_cmd = "../pi/tcds_pi_force_tts.py --setup-file {0:s} --location {1:s}".format(self.setup_file_name, self.location_name)
        # send_soap_cmd = "../utils/tcds_send_soap_command.py --setup-file {0:s} --location {1:s}".format(self.setup_file_name, self.location_name)

        cmds = []

        #----------

        # Halt everything.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Halting all controllers'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = [RF2TTC]
        targets.extend(RFRXDS)
        for target in targets:
            cmds.append(("{0:s} {1:s} Halt".format(fsm_ctrl_cmd, target), ""))

        # Wait till everything is in the Halted state.
        for target in targets:
            cmds.append(("{0:s} {1:s} --wait-until Halted".format(fsm_poll_cmd, target), ""))

        #----------

        # Configure RFRXDs.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Configuring RFRXDs'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list(RFRXDS)
        for target in targets:
            rfrxd_service = target
            cmds.append(("{0:s} {1:s} Configure {2:s}/hw_cfg_{3:s}.txt".format(fsm_ctrl_cmd, target, test_dir_name, rfrxd_service), ""))

        # Wait till all RFRXDs are in the Configured state.
        for target in targets:
            cmds.append(("{0:s} {1:s} --wait-until Configured".format(fsm_poll_cmd, target), ""))

        # Configure the RF2TTC.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Configuring RF2TTC'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        cmds.append(("{0:s} {1:s} Configure {2:s}/hw_cfg_rf2ttc.txt".format(fsm_ctrl_cmd, RF2TTC, test_dir_name), ""))

        # Wait till the RF2TTC is in the Configured state.
        cmds.append(("{0:s} {1:s} --wait-until Configured".format(fsm_poll_cmd, RF2TTC), ""))

        #----------

        # Enable RFRXDs.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Enabling RFRXDs'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list(RFRXDS)
        for target in targets:
            cmds.append(("{0:s} {1:s} Enable".format(fsm_ctrl_cmd, target), ""))

        # Wait till all RFRXDs are in the Enabled state.
        for target in targets:
            cmds.append(("{0:s} {1:s} --wait-until Enabled".format(fsm_poll_cmd, target), ""))

        # Enable APVEs.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Enabling RF2TTC'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        cmds.append(("{0:s} {1:s} Enable".format(fsm_ctrl_cmd, RF2TTC), ""))

        # Wait till the RF2TTC is in the Enabled state.
        cmds.append(("{0:s} {1:s} --wait-until Enabled".format(fsm_poll_cmd, RF2TTC), ""))

        #----------

        cmds = add_cwd_to_cmds(cmds)
        if self.verbose:
            cmds = append_piece_to_cmds(cmds, '--verbose')

        runner = CommandRunner(cmds)
        runner.run()

        # End of main().

    # End of class TestDriver.

###############################################################################

if __name__ == "__main__":

    description = "Master script to configure and enable a/the TTC Machine Interface."
    res = TestDriver(description).run()
    print "Done"
    sys.exit(res)

###############################################################################
