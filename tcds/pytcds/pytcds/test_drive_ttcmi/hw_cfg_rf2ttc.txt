#----------------------------------------------------------------------
# Default RF2TTC configuration.
#----------------------------------------------------------------------

# Bunch clock input selections for the different RF2TTC modes:
# 'manual', 'automatic, beam', and 'automatic, no-beam'.

# For BC1, BC2, BCref:
#   0 -> internal
#   1 -> external

# For BCmain:
#   0 -> internal
#   1 -> BCref
#   2 -> BC2
#   3 -> BC1

# Mode 'manual'
man_select_bc1                    0x00000000
man_select_bc2                    0x00000000
man_select_bcref                  0x00000000
man_select_bcmain                 0x00000000

# Mode 'automatic, beam'.
beam_select_bc1                   0x00000001
beam_select_bc2                   0x00000001
beam_select_bcref                 0x00000001
beam_select_bcmain                0x00000003

# Mode 'automatic, no-beam'.
nobeam_select_bc1                 0x00000000
nobeam_select_bc2                 0x00000000
nobeam_select_bcref               0x00000000
nobeam_select_bcmain              0x00000000

#----------

# Orbit input selections for the different RF2TTC modes: 'manual',
# 'automatic, beam', and 'automatic, no-beam'.

# For orbit1/orbit2:
#   0 -> external
#   1 -> internal

# For orbit-main:
#   0 -> orbit1
#   1 -> orbit2
#   2 -> internal

man_select_orb1                   0x00000001
man_select_orb2                   0x00000001
man_select_orbmain                0x00000002

beam_select_orb1                  0x00000000
beam_select_orb2                  0x00000000
beam_select_orbmain               0x00000000

nobeam_select_orb1                0x00000001
nobeam_select_orb2                0x00000001
nobeam_select_orbmain             0x00000002

#----------

# Working mode registers (manual/automatic) for all outputs.
#   0 -> manual
#   1 -> automatic

working_mode_bc1                  0x00000000
working_mode_bc2                  0x00000000
working_mode_bcmain               0x00000000
working_mode_bcref                0x00000000
working_mode_orb1                 0x00000000
working_mode_orb2                 0x00000000
working_mode_orbmain              0x00000000

#----------

# Beam/no-beam settings for each of the LHC beam modes.
#   0 -> no beam
#   1 -> beam

beam_no_beam_def_mode0            0x00000000
beam_no_beam_def_mode1            0x00000000
beam_no_beam_def_mode2            0x00000000
beam_no_beam_def_mode3            0x00000000
beam_no_beam_def_mode4            0x00000000
beam_no_beam_def_mode5            0x00000000
beam_no_beam_def_mode6            0x00000000
beam_no_beam_def_mode7            0x00000000
beam_no_beam_def_mode8            0x00000001
beam_no_beam_def_mode9            0x00000001
beam_no_beam_def_mode10           0x00000001
beam_no_beam_def_mode11           0x00000001
beam_no_beam_def_mode12           0x00000001
beam_no_beam_def_mode13           0x00000000
beam_no_beam_def_mode14           0x00000000
beam_no_beam_def_mode15           0x00000000
beam_no_beam_def_mode16           0x00000000
beam_no_beam_def_mode17           0x00000000
beam_no_beam_def_mode18           0x00000000
beam_no_beam_def_mode19           0x00000000
beam_no_beam_def_mode20           0x00000000
beam_no_beam_def_mode21           0x00000000

#----------

# Input DAC settings.
bc1_dac                           0x00000080
bc2_dac                           0x00000080
bcref_dac                         0x00000080
orb1_dac                          0x000000aa
orb2_dac                          0x000000aa

#----------

# Relocking mode settings for all bunch-clock inputs.
#   0 -> manual (i.e., upon reset)
#   1 -> automatic (i.e., auto-restart when lock is lost)
qpll_mode_bc1                     0x00000001
qpll_mode_bc2                     0x00000001
qpll_mode_bcmain                  0x00000001
qpll_mode_bcref                   0x00000001

#----------

# BC output phase adjustment settings (i.e., the Delay25 settings).
bc_delay25_bc1_val                0x00000000
bc_delay25_bc2_val                0x00000000
bc_delay25_bcref_val              0x00000000
bc_delay25_bcmain_val             0x00000000

#----------

# Orbit output polarity settings. If set to 1, the corresponding orbit
# output is inverted with respect to its input (i.e., the orbit output
# is negative active).
orb1_polarity                     0x00000000
orb2_polarity                     0x00000000
orbmain_polarity                  0x00000000

#----------

# Orbit signal delay settings.
orbin_delay25_orb1_val            0x00000000
orbin_delay25_orb2_val            0x00000000

orb1_coarse_delay                 0x00000000
orb2_coarse_delay                 0x00000000
orbmain_coarse_delay              0x00000000

orbout_delay25_orb1_val           0x00000000
orbout_delay25_orb2_val           0x00000000
orbout_delay25_orbmain_val        0x00000000

#----------

# Various settings for counters and internals.
orb_counter_enable_orb1           0x00000001
orb_counter_enable_orb2           0x00000001
orb_counter_enable_orbmain        0x00000001

orb1_int_period_set               0x00000dec
orb2_int_period_set               0x00000dec
orbmain_int_period_set            0x00000dec

orb_int_enable_orb1               0x00000001
orb_int_enable_orb2               0x00000001
orb_int_enable_orbmain            0x00000001

orb_period_counter_enable_orb1    0x00000001
orb_period_counter_enable_orb2    0x00000001
orb_period_counter_enable_orbmain 0x00000001

#----------------------------------------------------------------------
