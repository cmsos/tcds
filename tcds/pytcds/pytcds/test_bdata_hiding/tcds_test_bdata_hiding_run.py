#!/usr/bin/env python

###############################################################################
## Master script to configure and enable a CPM driving several iCIs
## and PIs to look for B-data suppression.
###############################################################################

import sys

from pytcds.test_bdata_hiding.tcds_test_bdata_hiding_constants import APVES
from pytcds.test_bdata_hiding.tcds_test_bdata_hiding_constants import CPM
from pytcds.test_bdata_hiding.tcds_test_bdata_hiding_constants import ICIS
from pytcds.test_bdata_hiding.tcds_test_bdata_hiding_constants import PIS
from pytcds.utils.tcds_cmd_line_base import CmdLineBase
from pytcds.utils.tcds_command_runner import add_cwd_to_cmds
from pytcds.utils.tcds_command_runner import CommandRunner

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

def append_piece_to_cmds(cmds, piece):
    res = []
    for (cmd, doc) in cmds:
        full_cmd = cmd
        if cmd.endswith(".py"):
            full_cmd = "%s %s" % (cmd, piece)
        res.append((full_cmd, doc))
    # End of append_piece_to_cmds().
    return res

###############################################################################

class TCDSConfigForTest(CmdLineBase):

    def handle_args(self):
        if len(self.args) != 0:
            self.error("No arguments are allowed.")
        # End of handle_args().

    def main(self):
        sep_line = "-" * 70

        fsm_ctrl_cmd = "../utils/tcds_fsm_control.py --cfg-file ../utils/tcds_setup_tcdslab.cfg"
        fsm_poll_cmd = "../utils/tcds_fsm_poller.py --cfg-file ../utils/tcds_setup_tcdslab.cfg"
        pi_force_ready_cmd = "./tcds_pi_force_ready.py"

        cmds = []

        #----------

        # Halt everything.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Halting all controllers'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = [CPM]
        targets.extend(ICIS)
        targets.extend(APVES)
        targets.extend(PIS)
        for target in targets:
            cmds.append(("{0:s} {1:s} Halt".format(fsm_ctrl_cmd, target), ""))

        # Wait till everything is in the Halted state.
        targets = [CPM]
        targets.extend(ICIS)
        targets.extend(APVES)
        targets.extend(PIS)
        for target in targets:
            cmds.append(("{0:s} {1:s} --wait-until Halted".format(fsm_poll_cmd, target), ""))

        # # Wait a little.
        # cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
        #              ("python -c \"print 'Practicing patience...'\"", ""),
        #              ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        # cmds.append(("python -c \"import time; time.sleep(1)\"", ""))

        #----------

        # Configure CPM.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Configuring CPM'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        cmds.append(("{0:s} {1:s} Configure hw_cfg_cpm.txt".format(fsm_ctrl_cmd, CPM), ""))

        # Configure iCIs.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Configuring iCIs'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list(ICIS)
        for target in targets:
            cmds.append(("{0:s} {1:s} Configure hw_cfg_ici.txt".format(fsm_ctrl_cmd, target), ""))

        # Configure APVEs.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Configuring APVEs'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list(APVES)
        for target in targets:
            cmds.append(("{0:s} {1:s} Configure hw_cfg_apve.txt".format(fsm_ctrl_cmd, target), ""))

        # Configure PIs.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Configuring PIs'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list(PIS)
        for target in targets:
            cmds.append(("{0:s} {1:s} Configure hw_cfg_pi.txt".format(fsm_ctrl_cmd, target), ""))

        # Wait till everything is in the Configured state.
        targets = [CPM]
        targets.extend(ICIS)
        targets.extend(APVES)
        targets.extend(PIS)
        for target in targets:
            cmds.append(("{0:s} {1:s} --wait-until Configured".format(fsm_poll_cmd, target), ""))

        # Force PIs to be TTS READY.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Forcing PI TTS states to READY'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list(PIS)
        for target in targets:
            cmds.append(("{0:s} {1:s}".format(pi_force_ready_cmd, target), ""))

        #----------

        # Enable iCIs, APVEs, PIs.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Enabling iCIs, APVEs, and PIs'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list(ICIS)
        targets.extend(APVES)
        targets.extend(PIS)
        for target in targets:
            cmds.append(("{0:s} {1:s} Enable".format(fsm_ctrl_cmd, target), ""))

        # Enable CPM (as last!).
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Enabling CPM'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        cmds.append(("{0:s} {1:s} Enable".format(fsm_ctrl_cmd, CPM), ""))

        #----------

        cmds = add_cwd_to_cmds(cmds)
        if self.verbose:
            cmds = append_piece_to_cmds(cmds, "--verbose")

        runner = CommandRunner(cmds)
        runner.run()

        # End of main().

    # End of class TCDSConfigForTest.

###############################################################################

if __name__ == "__main__":

    description = "Master script to configure and enable a CPM " \
                  "driving several iCIs and PIs to look for B-data suppression."
    usage = "usage: %prog [options]"
    res = TCDSConfigForTest(description, usage).run()
    print "Done"
    sys.exit(res)

###############################################################################
