#!/usr/bin/env python

###############################################################################
## A little script to read the APVE pipeline history contents.
###############################################################################

import json
import os
import urllib2
import sys

from pytcds.utils.tcds_cmd_line_base import CmdLineBase

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class UpdateGetter(CmdLineBase):

    def main(self):
        # Check if the target is valid.
        self.validate_target(target_type=CmdLineBase.TARGET_TYPE_SW)

        # Now do what we have been asked to do.
        target_info = self.setup.sw_targets[self.target]
        host_name = target_info.host
        port_number = target_info.port
        lid_number = target_info.lid

        tmp = "http://{0:s}:{1:d}/urn:xdaq-application:lid={2:d}/update"
        url = tmp.format(host_name, port_number, lid_number)

        req = urllib2.Request(url)
        opener = urllib2.build_opener()
        f = opener.open(req)
        contents = json.load(f)
        history_contents =  contents["itemset-simhist"]["Simulated APV pipeline history"]
        for entry in history_contents:
            msg = "Event # {0:s}: pipeline address {1:s}, gray code {2:s}"
            print msg.format(entry["EventNumber"],
                             entry["PipelineAddress"],
                             entry["PipelineAddressGrayCode"])

        # End of main().

    # End of class UpdateGetter.

###############################################################################

if __name__ == "__main__":

    desc_str = "A little script to read the APVE pipeline history contents."
    usage_str = "usage: %prog device"
    epilog_str_tmp = ""
    epilog_str = epilog_str_tmp.format(os.path.basename(sys.argv[0]))

    res = UpdateGetter(desc_str, usage_str, epilog_str).run()

    print "Done"

    sys.exit(res)

###############################################################################
