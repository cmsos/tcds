#!/usr/bin/env python

###############################################################################
## Master script to configure and enable an CPM/LPM driving several
## iCIs and PIs.
###############################################################################

import os
import sys

from pytcds.test_alignment_factors.tcds_test_alignment_factors_constants import __file__ as constants_file_name
from pytcds.test_alignment_factors.tcds_test_alignment_factors_constants import APVES
from pytcds.test_alignment_factors.tcds_test_alignment_factors_constants import CPM
from pytcds.test_alignment_factors.tcds_test_alignment_factors_constants import FED_ID_CPM
from pytcds.test_alignment_factors.tcds_test_alignment_factors_constants import FED_ID_GT1
from pytcds.test_alignment_factors.tcds_test_alignment_factors_constants import FED_ID_GT2
from pytcds.test_alignment_factors.tcds_test_alignment_factors_constants import FED_ID_LPM
from pytcds.test_alignment_factors.tcds_test_alignment_factors_constants import FED_IDS_MISC
from pytcds.test_alignment_factors.tcds_test_alignment_factors_constants import ICIS
from pytcds.test_alignment_factors.tcds_test_alignment_factors_constants import LPM
from pytcds.test_alignment_factors.tcds_test_alignment_factors_constants import PIS
from pytcds.utils.tcds_cmd_line_base import CmdLineBase
from pytcds.utils.tcds_command_runner import add_cwd_to_cmds
from pytcds.utils.tcds_command_runner import CommandRunner

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

TIME_BEFORE_FIRST_L1A = 5
TIME_BETWEEN_L1AS = 2
NUM_L1AS = 3

###############################################################################

def append_piece_to_cmds(cmds, piece):
    res = []
    for (cmd, doc) in cmds:
        full_cmd = cmd
        if cmd.endswith(".py"):
            full_cmd = "%s %s" % (cmd, piece)
        res.append((full_cmd, doc))
    # End of append_piece_to_cmds().
    return res

###############################################################################

class TCDSConfigForTestBXAlignment(CmdLineBase):

    MODE_CHOICES = ["cpm", "lpm", "ici"]

    def __init__(self, description=None, usage=None, epilog=None):
        super(TCDSConfigForTestBXAlignment, self).__init__(description, usage, epilog)
        self.mode = None
        self.with_daq = False
        self.with_gt = False
        self.do_stop = True
        self.do_halt = False
        self.do_configure = True
        self.do_enable = True
        # End of __init__().

    def setup_parser_custom(self):
        parser = self.parser

        # Add the choice of running mode.
        help_str = "Running mode"
        parser.add_argument("mode",
                            type=str,
                            action="store",
                            choices=TCDSConfigForTestBXAlignment.MODE_CHOICES,
                            help=help_str)

        # Add the choice to run with DAQ/FEDkit or not.
        help_str = "Run with DAQ/FEDkit"
        parser.add_argument("--with-daq",
                            action="store_true",
                            default=False,
                            help=help_str)

        # Add the choice to run with external (i.e., GT) trigger input
        # or not.
        help_str = "Run with external (GT) trigger input enabled"
        parser.add_argument("--with-gt",
                            action="store_true",
                            default=False,
                            help=help_str)

        # Add the choice to do/skip the 'stop' step.
        help_str = "Stop all applications"
        parser.add_argument("--stop",
                            action="store_true",
                            help=help_str,
                            dest='do_stop')
        help_str = "Don't stop all applications"
        parser.add_argument("--no-stop",
                            action="store_false",
                            help=help_str,
                            dest='do_stop')
        parser.set_defaults(do_stop=False)

        # Add the choice to do/skip the 'halt' step.
        help_str = "Halt all applications"
        parser.add_argument("--halt",
                            action="store_true",
                            help=help_str,
                            dest='do_halt')
        help_str = "Don't halt all applications"
        parser.add_argument("--no-halt",
                            action="store_false",
                            help=help_str,
                            dest='do_halt')
        parser.set_defaults(do_halt=True)

        # Add the choice to do/skip the 'configure' step.
        help_str = "Configure all applications"
        parser.add_argument("--configure",
                            action="store_true",
                            help=help_str,
                            dest='do_configure')
        help_str = "Don't configure all applications"
        parser.add_argument("--no-configure",
                            action="store_false",
                            help=help_str,
                            dest='do_configure')
        parser.set_defaults(do_configure=True)

        # Add the choice to do/skip the 'enable' step.
        help_str = "Enable all applications"
        parser.add_argument("--enable",
                            action="store_true",
                            help=help_str,
                            dest='do_enable')
        help_str = "Don't enable all applications"
        parser.add_argument("--no-enable",
                            action="store_false",
                            help=help_str,
                            dest='do_enable')
        parser.set_defaults(do_enable=True)

        # End of setup_parser_custom().

    def handle_args(self):
        super(TCDSConfigForTestBXAlignment, self).handle_args()
        # Extract the driving mode.
        self.mode = self.args.mode
        # Extract the various choices.
        self.with_daq = self.args.with_daq
        self.with_gt = self.args.with_gt
        self.do_stop = self.args.do_stop
        self.do_halt = self.args.do_halt
        self.do_configure = self.args.do_configure
        self.do_enable = self.args.do_enable
        # End of handle_args().

    def main(self):
        sep_line = "-" * 70

        test_dir_name = os.path.dirname(constants_file_name)

        #----------

        pm = None
        if self.mode == "lpm":
            pm = LPM
        elif self.mode == "cpm":
            pm = CPM

        #----------

        tmp = "".join(["{0:d}&{1:d}%".format(i[0], i[1]) for i in FED_IDS_MISC])
        if self.with_daq:
            pm_daq_on_off = '1'
        else:
            pm_daq_on_off = '0'

        if self.with_gt:
            gt1_enabled_disabled = '1'
            gt2_enabled_disabled = '1'
        else:
            gt1_enabled_disabled = '0'
            gt2_enabled_disabled = '0'

        fed_enable_mask = "{0:d}&{1:s}%{2:d}&{3:s}%{4:d}&{5:s}%{6:d}&{7:s}%{8:s}".format(FED_ID_CPM,
                                                                                         pm_daq_on_off,
                                                                                         FED_ID_LPM,
                                                                                         pm_daq_on_off,
                                                                                         FED_ID_GT1,
                                                                                         gt1_enabled_disabled,
                                                                                         FED_ID_GT2,
                                                                                         gt2_enabled_disabled,
                                                                                         tmp)
        ttc_partition_map = \
            "ICI11&1%" \
            "ICI12&0%" \
            "ICI13&0%" \
            "ICI14&0%" \
            "ICI15&0%" \
            "ICI16&0%" \
            "ICI17&0%" \
            "ICI18&0%" \
            "APVE11&1%" \
            "APVE12&0%" \
            "APVE13&0%" \
            "APVE14&0%" \
            \
            "ICI21&0%" \
            "ICI22&0%" \
            "ICI23&0%" \
            "ICI24&0%" \
            "ICI25&0%" \
            "ICI26&0%" \
            "ICI27&0%" \
            "ICI28&0%" \
            "APVE21&0%" \
            "APVE22&0%" \
            "APVE23&0%" \
            "APVE24&0%" \
            \
            "ICI111&1%" \
            "ICI112&0%" \
            "ICI113&0%" \
            "ICI114&0%" \
            "ICI115&0%" \
            "ICI116&0%" \
            "ICI117&0%" \
            "ICI118&0%" \
            "APVE111&1%" \
            "APVE112&0%" \
            "APVE113&0%" \
            "APVE114&0%" \
            \
            "ICI121&0%" \
            "ICI122&0%" \
            "ICI123&0%" \
            "ICI124&0%" \
            "ICI125&0%" \
            "ICI126&0%" \
            "ICI127&0%" \
            "ICI128&0%" \
            "APVE121&0%" \
            "APVE122&0%" \
            "APVE123&0%" \
            "APVE124&0%"

        #----------

        fsm_ctrl_cmd = "../utils/tcds_fsm_control.py --setup-file {0:s} --location {1:s}".format(self.setup_file_name, self.location_name)
        fsm_poll_cmd = "../utils/tcds_fsm_poller.py --setup-file {0:s} --location {1:s}".format(self.setup_file_name, self.location_name)
        pi_force_tts_cmd = "../pi/tcds_pi_force_tts.py --setup-file {0:s} --location {1:s}".format(self.setup_file_name, self.location_name)
        send_soap_cmd = "../utils/tcds_send_soap_command.py --setup-file {0:s} --location {1:s}".format(self.setup_file_name, self.location_name)

        cmds = []

        #----------

        if self.do_stop:

            # Stop PM.
            if pm:
                no_beam_active_bit = ""
                cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                             ("python -c \"print 'Stopping {0:s}'\"".format(self.mode.upper()), ""),
                             ("python -c \"print '{0:s}'\"".format(sep_line), "")])
                cmds.append(("{0:s} {1:s} Stop".format(fsm_ctrl_cmd, pm), ""))
                # Wait till the PM is in the Configured state.
                cmds.append(("{0:s} {1:s} --wait-until Configured".format(fsm_poll_cmd, pm), ""))

            # Stop iCIs.
            targets = list(ICIS)
            if len(targets):
                cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                             ("python -c \"print 'Stopping iCIs'\"", ""),
                             ("python -c \"print '{0:s}'\"".format(sep_line), "")])
                for target in targets:
                    tmp = ""
                    if pm:
                        tmp = "_under_{0:s}".format(self.mode)
                    cmds.append(("{0:s} {1:s} Stop".format(fsm_ctrl_cmd, target), ""))

                # Wait till everything is in the Configured state.
                for target in targets:
                    cmds.append(("{0:s} {1:s} --wait-until Configured".format(fsm_poll_cmd, target), ""))

            # Stop APVEs.
            targets = list(APVES)
            if len(targets):
                cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                             ("python -c \"print 'Stopping APVEs'\"", ""),
                             ("python -c \"print '{0:s}'\"".format(sep_line), "")])
                for target in targets:
                    cmds.append(("{0:s} {1:s} Stop".format(fsm_ctrl_cmd, target), ""))

                # Wait till everything is in the Configured state.
                for target in targets:
                    cmds.append(("{0:s} {1:s} --wait-until Configured".format(fsm_poll_cmd, target), ""))

            # Stop PIs.
            targets = list(PIS)
            if len(targets):
                cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                             ("python -c \"print 'Stopping PIs'\"", ""),
                             ("python -c \"print '{0:s}'\"".format(sep_line), "")])
                for target in targets:
                    cmds.append(("{0:s} {1:s} Stop".format(fsm_ctrl_cmd, target), ""))

                # Wait till everything is in the Configured state.
                for target in targets:
                    cmds.append(("{0:s} {1:s} --wait-until Configured".format(fsm_poll_cmd, target), ""))

        #----------

        if self.do_halt:

            # Halt everything.
            targets = []
            if pm:
                targets = [pm]
            targets.extend(ICIS)
            targets.extend(APVES)
            targets.extend(PIS)
            if len(targets):
                cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                             ("python -c \"print 'Halting all controllers'\"", ""),
                             ("python -c \"print '{0:s}'\"".format(sep_line), "")])
                for target in targets:
                    cmds.append(("{0:s} {1:s} Halt".format(fsm_ctrl_cmd, target), ""))

                # Wait till everything is in the Halted state.
                targets = []
                if pm:
                    targets = [pm]
                targets.extend(ICIS)
                targets.extend(APVES)
                targets.extend(PIS)
                for target in targets:
                    cmds.append(("{0:s} {1:s} --wait-until Halted".format(fsm_poll_cmd, target), ""))

        #----------

        if self.do_configure:

            # Configure PM.
            if pm:
                no_beam_active_bit = ""
                if self.mode == 'cpm':
                    no_beam_active_bit = " --no-beam-active"
                cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                             ("python -c \"print 'Configuring {0:s}'\"".format(self.mode.upper()), ""),
                             ("python -c \"print '{0:s}'\"".format(sep_line), "")])
                cmds.append(("{0:s} --fed-enable-mask='{1:s}' --ttc-partition-map='{2:s}' {3:s} {4:s} Configure {5:s}/hw_cfg_{6:s}.txt".format(fsm_ctrl_cmd, fed_enable_mask, ttc_partition_map, no_beam_active_bit, pm, test_dir_name, self.mode), ""))
                # Wait till the PM is in the Configured state.
                cmds.append(("{0:s} {1:s} --wait-until Configured".format(fsm_poll_cmd, pm), ""))

            # Configure iCIs.
            targets = list(ICIS)
            if len(targets):
                cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                             ("python -c \"print 'Configuring iCIs'\"", ""),
                             ("python -c \"print '{0:s}'\"".format(sep_line), "")])
                for target in targets:
                    tmp = ""
                    if pm:
                        tmp = "_under_{0:s}".format(self.mode)
                    cmds.append(("{0:s} {1:s} Configure {2:s}/hw_cfg_ici{3:s}.txt".format(fsm_ctrl_cmd, target, test_dir_name, tmp), ""))

                # Wait till everything is in the Configured state.
                for target in targets:
                    cmds.append(("{0:s} {1:s} --wait-until Configured".format(fsm_poll_cmd, target), ""))

            # Configure APVEs.
            targets = list(APVES)
            if len(targets):
                cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                             ("python -c \"print 'Configuring APVEs'\"", ""),
                             ("python -c \"print '{0:s}'\"".format(sep_line), "")])
                for target in targets:
                    cmds.append(("{0:s} {1:s} Configure {2:s}/hw_cfg_apve.txt".format(fsm_ctrl_cmd, target, test_dir_name), ""))

                # Wait till everything is in the Configured state.
                for target in targets:
                    cmds.append(("{0:s} {1:s} --wait-until Configured".format(fsm_poll_cmd, target), ""))

            # Configure PIs.
            targets = list(PIS)
            if len(targets):
                cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                             ("python -c \"print 'Configuring PIs'\"", ""),
                             ("python -c \"print '{0:s}'\"".format(sep_line), "")])
                for target in targets:
                    cmds.append(("{0:s} --fed-enable-mask='{1:s}' {2:s} Configure {3:s}/hw_cfg_pi.txt".format(fsm_ctrl_cmd, fed_enable_mask, target, test_dir_name), ""))

                # Wait till everything is in the Configured state.
                for target in targets:
                    cmds.append(("{0:s} {1:s} --wait-until Configured".format(fsm_poll_cmd, target), ""))

            #----------

            # Force PIs to be TTS READY.
            targets = list(PIS)
            if len(targets):
                cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                             ("python -c \"print 'Forcing PI TTS states to READY'\"", ""),
                             ("python -c \"print '{0:s}'\"".format(sep_line), "")])
                for target in targets:
                    cmds.append(("{0:s} {1:s} READY".format(pi_force_tts_cmd, target), ""))

            #----------

            if self.with_daq:
                # Now wait, and give the user the opportunity to configure
                # the DAQ/FED-kit before continuing.
                cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                             ("python -c \"print 'Please configure the DAQ and then press enter to continue'\"", ""),
                             ("python -c \"raw_input('')\" &> /dev/null", ""),
                             ("python -c \"print '{0:s}'\"".format(sep_line), "")])

            # cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
            #              ("python -c \"print 'Please press enter to continue'\"", ""),
            #              ("python -c \"raw_input('')\" &> /dev/null", ""),
            #              ("python -c \"print '{0:s}'\"".format(sep_line), "")])

            #----------

        if self.do_enable:
            # Enable PIs.
            targets = list(PIS)
            if len(targets):
                cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                             ("python -c \"print 'Enabling PIs'\"", ""),
                             ("python -c \"print '{0:s}'\"".format(sep_line), "")])
                for target in targets:
                    cmds.append(("{0:s} {1:s} Enable".format(fsm_ctrl_cmd, target), ""))

                # Wait till all PIs are in the Enabled state.
                for target in targets:
                    cmds.append(("{0:s} {1:s} --wait-until Enabled".format(fsm_poll_cmd, target), ""))

            # Enable APVEs.
            targets = list(APVES)
            if len(targets):
                cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                             ("python -c \"print 'Enabling APVEs'\"", ""),
                             ("python -c \"print '{0:s}'\"".format(sep_line), "")])
                for target in targets:
                    cmds.append(("{0:s} {1:s} Enable".format(fsm_ctrl_cmd, target), ""))

                # Wait till all APVEs are in the Enabled state.
                for target in targets:
                    cmds.append(("{0:s} {1:s} --wait-until Enabled".format(fsm_poll_cmd, target), ""))

            # Enable iCIs.
            targets = list(ICIS)
            if len(targets):
                cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                             ("python -c \"print 'Enabling iCIs'\"", ""),
                             ("python -c \"print '{0:s}'\"".format(sep_line), "")])
                for target in targets:
                    cmds.append(("{0:s} {1:s} Enable".format(fsm_ctrl_cmd, target), ""))

                # Wait till all ICIs are in the Enabled state.
                for target in targets:
                    cmds.append(("{0:s} {1:s} --wait-until Enabled".format(fsm_poll_cmd, target), ""))

            # Enable PM (as last!).
            if pm:
                cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                             ("python -c \"print 'Enabling {0:s}'\"".format(self.mode.upper()), ""),
                             ("python -c \"print '{0:s}'\"".format(sep_line), "")])
                cmds.append(("{0:s} {1:s} Enable".format(fsm_ctrl_cmd, pm), ""))

            # Wait till also the PM is in the Enabled state.
            if pm:
                cmds.append(("{0:s} {1:s} --wait-until Enabled".format(fsm_poll_cmd, pm), ""))

            #----------

            # Wait a bit, and then send a few L1As.
            cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                         ("python -c \"print 'Waiting a bit, and then sending a few L1As'\"", ""),
                         ("python -c \"print '{0:s}'\"".format(sep_line), "")])
            targets = list(ICIS)
            if pm:
                targets = [pm]
            cmds.extend([("python -c \"print '  Waiting ({0:d} s)...'\"".format(TIME_BEFORE_FIRST_L1A), "")])
            cmds.extend([("python -c \"import time; time.sleep({0:d})\"".format(TIME_BEFORE_FIRST_L1A), "")])
            for i in xrange(NUM_L1AS):
                cmds.extend([("python -c \"print '  Sending L1A ({0:d} of {1:d})'\"".format(i + 1, NUM_L1AS), "")])
                for target in targets:
                    cmds.append(("{0:s} {1:s} SendL1A".format(send_soap_cmd, target), ""))
                if i != (NUM_L1AS - 1):
                    cmds.extend([("python -c \"print '  Waiting ({0:d} s)...'\"".format(TIME_BETWEEN_L1AS), "")])
                    cmds.extend([("python -c \"import time; time.sleep({0:d})\"".format(TIME_BETWEEN_L1AS), "")])

        #----------

        cmds = add_cwd_to_cmds(cmds)
        if self.verbose:
            cmds = append_piece_to_cmds(cmds, '--verbose')

        runner = CommandRunner(cmds)
        runner.run()

        # End of main().

    # End of class TCDSConfigForTestBXAlignment.

###############################################################################

if __name__ == "__main__":

    description = "Master script to look at BX alignment."
    res = TCDSConfigForTestBXAlignment(description).run()
    print "Done"
    sys.exit(res)

###############################################################################
