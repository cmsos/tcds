#!/usr/bin/env python

###############################################################################
## Master script to configure an iCI with a PI to test the TTCSpy
## built into the PI.
###############################################################################

import sys

from pytcds.test_pispy.tcds_test_pispy_constants import LPM
from pytcds.test_pispy.tcds_test_pispy_constants import PI
# from pytcds.test_pispy.tcds_test_pispy_constants import TTCSPY
from pytcds.utils.tcds_cmd_line_base import CmdLineBase
from pytcds.utils.tcds_command_runner import add_cwd_to_cmds
from pytcds.utils.tcds_command_runner import CommandRunner

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

def append_piece_to_cmds(cmds, piece):
    res = []
    for (cmd, doc) in cmds:
        full_cmd = cmd
        if cmd.endswith(".py"):
            full_cmd = "%s %s" % (cmd, piece)
        res.append((full_cmd, doc))
    # End of append_piece_to_cmds().
    return res

###############################################################################

class TCDSConfigForTestPiSpy(CmdLineBase):

    def handle_args(self):
        # NOTE: This is a slight abuse of the CmdLineBase base class
        # since we don't use an IP address argument. Don't tell
        # anyone.
        pass
        # End of handle_args().

    def main(self):
        sep_line = "-" * 70

        fsm_ctrl_cmd = "../utils/tcds_fsm_control.py --cfg-file ../utils/tcds_setup.cfg"

        cmds = [
            ("python -c \"print '{0:s}'\"".format(sep_line), ""),
            ("python -c \"print 'Loading user firmware on all boards'\"", ""),
            ("python -c \"print '{0:s}'\"".format(sep_line), ""),
            ("../utils/tcds_jump_to_firmware_image.py {0:s} user".format(LPM), ""),
            ("../utils/tcds_jump_to_firmware_image.py {0:s} user".format(PI), ""),
            # ("../utils/tcds_jump_to_firmware_image.py {0:s} user".format(TTCSPY), ""),

            ("python -c \"print '{0:s}'\"".format(sep_line), ""),
            ("python -c \"print 'Powering up FMCs on all boards'\"", ""),
            ("python -c \"print '{0:s}'\"".format(sep_line), ""),
            ("../utils/tcds_carrier_powerup.py {0:s}".format(LPM), ""),
            ("../utils/tcds_carrier_powerup.py {0:s}".format(PI), ""),

            ("python -c \"print '{0:s}'\"".format(sep_line), ""),
            ("python -c \"print 'Dumping board information for all boards involved'\"", ""),
            ("python -c \"print '{0:s}'\"".format(sep_line), ""),
            ("../utils/tcds_board_info.py {0:s}".format(LPM), ""),
            ("../utils/tcds_board_info.py {0:s}".format(PI), ""),
            # ("../utils/tcds_board_info.py {0:s}".format(TTCSPY), ""),

            ("python -c \"print '{0:s}'\"".format(sep_line), ""),
            ("python -c \"print 'Switching all boards to backplane (port 3) TTC clock'\"", ""),
            ("python -c \"print '{0:s}'\"".format(sep_line), ""),
            ("../utils/tcds_carrier_select_fclka.py {0:s}".format(LPM), ""),
            ("../utils/tcds_carrier_select_fclka.py {0:s}".format(PI), ""),
            # ("../utils/tcds_carrier_select_fclka.py {0:s}".format(TTCSPY), ""),

            ("python -c \"print '{0:s}'\"".format(sep_line), ""),
            ("python -c \"print 'Checking all boards for presence of TTC clock'\"", ""),
            ("python -c \"print '{0:s}'\"".format(sep_line), ""),
            ("../utils/tcds_carrier_check_ttc_clock.py {0:s}".format(LPM), ""),
            ("../utils/tcds_carrier_check_ttc_clock.py {0:s}".format(PI), ""),
            # ("../utils/tcds_carrier_check_ttc_clock.py {0:s}".format(TTCSPY), ""),

            ("python -c \"print '{0:s}'\"".format(sep_line), ""),
            ("python -c \"print 'Switching on all FMCs on all boards'\"", ""),
            ("python -c \"print '{0:s}'\"".format(sep_line), ""),
            ("../utils/tcds_carrier_switch_sfps.py {0:s} on".format(LPM), ""),
            ("../utils/tcds_carrier_switch_sfps.py {0:s} on".format(PI), ""),

            ("python -c \"print '{0:s}'\"".format(sep_line), ""),
            ("python -c \"print 'Halting iCI and PI'\"", ""),
            ("python -c \"print '{0:s}'\"".format(sep_line), ""),
            ("{0:s} {1:s} Halt".format(fsm_ctrl_cmd, LPM), ""),
            ("{0:s} {1:s} Halt".format(fsm_ctrl_cmd, PI), ""),

            ("python -c \"print '{0:s}'\"".format(sep_line), ""),
            ("python -c \"print 'Configuring iCI'\"", ""),
            ("python -c \"print '{0:s}'\"".format(sep_line), ""),
            ("{0:s} {1:s} Configure hw_cfg_ici.txt".format(fsm_ctrl_cmd, LPM), ""),

            ("python -c \"print '{0:s}'\"".format(sep_line), ""),
            ("python -c \"print 'Configuring PI'\"", ""),
            ("python -c \"print '{0:s}'\"".format(sep_line), ""),
            ("{0:s} {1:s} Configure hw_cfg_pi.txt".format(fsm_ctrl_cmd, PI), "")
        ]
        cmds = add_cwd_to_cmds(cmds)
        if self.verbose:
            cmds = append_piece_to_cmds(cmds, "--verbose")

        runner = CommandRunner(cmds)
        runner.run()

        # End of main().

    # End of class TCDSConfigForTestPiSpy.

###############################################################################

if __name__ == "__main__":

    description = "Master script to configure an iCI with a PI " \
                  "to test the TTCSpy built into the PI."
    usage = "usage: %prog [options]"
    res = TCDSConfigForTestPiSpy(description, usage).run()
    print "Done"
    sys.exit(res)

###############################################################################
