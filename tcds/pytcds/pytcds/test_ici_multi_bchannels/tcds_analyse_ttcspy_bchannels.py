#!/usr/bin/env python

###############################################################################
## A little script to check the TTCSpy log for (multi-)B-channel output.
## NOTE: This of course strongly depends on the CPM and ICI hardware
## configurations.
###############################################################################

import collections
import os
import re
import sys

from pytcds.utils.tcds_cmd_line_base import CmdLineBase

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class Analyzer(CmdLineBase):

    TTCSpyEntry = collections.namedtuple('TTCSpyEntry',
                                         ['number', 'data', 'orbit', 'bx'])

    def setup_parser_custom(self):
        # Add the data file choice.
        help_str = "The name of the captured data file to analyze."
        self.parser.add_argument("data_file",
                                 type=str,
                                 action="store",
                                 help=help_str)

        # End of setup_parser_custom().

    def handle_args(self):
        # NOTE: Do not call the parent handle_args() since we are not
        # requiring an IP address (unlike pretty much all other TCDS
        # scripts).
        self.ttcspy_file_name = self.args.data_file
        # End of handle_args().

    def load_setup(self):
        pass

    def main(self):

        ttcspy_data_raw = None

        try:
            in_file = open(self.ttcspy_file_name, "r")
            ttcspy_data_raw = in_file.read()
            in_file.close()
        except IOError, err:
            msg = "Could not read file '{0:s}': '{1:s}'."
            self.error(msg.format(self.ttcspy_file_name, err))

        ttcspy_data = []
        regexp = re.compile("^Entry +([0-9]+): Broadcast command \(with address n/a\) with data (0x[0-9a-f]{2}) at orbit +([0-9]+) and BX +([0-9]+).*$")
        for line in ttcspy_data_raw.split(os.linesep):
            match = regexp.match(line)
            if match:
                # 0: Entry number.
                # 1: Data payload.
                # 2: Orbit number.
                # 3: BX number.
                ttcspy_data.append(Analyzer.TTCSpyEntry(int(match.group(1)),
                                                        int(match.group(2), 16),
                                                        int(match.group(3)),
                                                        int(match.group(4))))

        if not len(ttcspy_data):
            self.error("No relevant data found in the TTCSpy capture data")
        else:
            print "Found {} lines of relevant data in the TTCSpy capture data".format(len(ttcspy_data))

        #----------

        # Now let's have a look if things are in the right place...

        # NOTE: This of course strongly depends on the CPM and ICI
        # hardware configurations.

        to_verify = [

            # B-go 2, emitted by the CPM/LPM/iCI in BX 0x00000cd3, and
            # delayed in the ICI by 0 BX.
            (2, 0x02, 0x00000cd3 + 0x00000000),

            # B-go 3, emitted by the CPM/LPM/iCI in BX 0x00000cd3, and
            # emitted (fixed) by the ICI in BX 200 = 0x000000c8.
            (3, 0x03, 0x000000c8),

            # B-go 16, emitted by the CPM/LPM/iCI in BX 0x000003e8,
            # and delayed in the ICI by 0 BX.
            (16, 0x10, 0x000003e8 + 0x00000000),

            # B-go 32, emitted by the CPM/LPM/iCI in BX 0x000003d0,
            # and emitted (fixed) by the ICI in BX 1280 = 0x00000500.
            (32, 0x20, 0x00000500)
        ]

        for i in to_verify:
            found = []
            b_go = i[0]
            data = i[1]
            bx = i[2]
            for entry in ttcspy_data:
                if entry.data == data:
                    found.append(entry.bx)
            if not len(found):
                self.error("Failed to find any broadcast command from B-go {0:d}" \
                           " (identified by data 0x{1:02x}, should be in BX {2:d})".format(b_go, data, bx))
            else:
                tmp = list(set(found))
                if len(tmp) > 1:
                    self.error("Found multiple candidates for broadcast command from B-go {0:d}" \
                               " (identified by data 0x{1:02x}, should be in BX {2:d})".format(b_go, data, bx))
                else:
                    if tmp[0] != bx:
                        self.error("Found a candidate for broadcast command from B-go {0:d}" \
                                   " (identified by data 0x{1:02x}, should be in BX {2:d})," \
                                   " but in the wrong BX: {3:d}".format(b_go, data, bx, tmp[0]))
            print "Successfully verified B-go {0:d}" \
                " (identified by data 0x{1:02x} in BX {2:d})".format(b_go, data, bx)

        #----------

        # End of main().

    # End of class Analyzer.

###############################################################################

if __name__ == "__main__":

    desc_str = "A little script to check the TTCSpy log" \
        " for (multi-)B-channel output."
    usage_str = None
    epilog_str_tmp = ""
    epilog_str = epilog_str_tmp.format(os.path.basename(sys.argv[0]))

    Analyzer(desc_str, usage_str, epilog_str).run()

    print "Done"

###############################################################################
