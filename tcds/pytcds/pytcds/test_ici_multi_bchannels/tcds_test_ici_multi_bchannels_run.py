#!/usr/bin/env python

###############################################################################
## Master script to verify the ICI multi-B-channels.
###############################################################################

import os
import sys

from pytcds.test_ici_multi_bchannels.tcds_test_ici_multi_bchannels_constants import __file__ as constants_file_name
from pytcds.test_ici_multi_bchannels.tcds_test_ici_multi_bchannels_constants import APVES
from pytcds.test_ici_multi_bchannels.tcds_test_ici_multi_bchannels_constants import CPM
from pytcds.test_ici_multi_bchannels.tcds_test_ici_multi_bchannels_constants import FED_ID_CPM
from pytcds.test_ici_multi_bchannels.tcds_test_ici_multi_bchannels_constants import FED_ID_GT1
from pytcds.test_ici_multi_bchannels.tcds_test_ici_multi_bchannels_constants import FED_ID_GT2
from pytcds.test_ici_multi_bchannels.tcds_test_ici_multi_bchannels_constants import FED_ID_LPM
from pytcds.test_ici_multi_bchannels.tcds_test_ici_multi_bchannels_constants import FED_IDS_MISC
from pytcds.test_ici_multi_bchannels.tcds_test_ici_multi_bchannels_constants import ICIS
from pytcds.test_ici_multi_bchannels.tcds_test_ici_multi_bchannels_constants import LPM
from pytcds.test_ici_multi_bchannels.tcds_test_ici_multi_bchannels_constants import PIS
from pytcds.utils.tcds_cmd_line_base import CmdLineBase
from pytcds.utils.tcds_command_runner import add_cwd_to_cmds
from pytcds.utils.tcds_command_runner import CommandRunner

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

def append_piece_to_cmds(cmds, piece):
    res = []
    for (cmd, doc) in cmds:
        full_cmd = cmd
        if cmd.endswith(".py"):
            full_cmd = "%s %s" % (cmd, piece)
        res.append((full_cmd, doc))
    # End of append_piece_to_cmds().
    return res

###############################################################################

class TCDSConfigForTestMultiBChannels(CmdLineBase):

    MODE_CHOICES = ["cpm", "lpm", "ici", "ttcmi"]

    def __init__(self, description=None, usage=None, epilog=None):
        super(TCDSConfigForTestMultiBChannels, self).__init__(description, usage, epilog)
        self.mode = None
        self.with_daq = False
        self.with_gt = False
        # End of __init__().

    def setup_parser_custom(self):
        parser = self.parser
        # Add the choice of running mode.
        help_str = "Running mode"
        parser.add_argument("mode",
                            type=str,
                            action="store",
                            choices=TCDSConfigForTestMultiBChannels.MODE_CHOICES,
                            help=help_str)
        # End of setup_parser_custom().

    def handle_args(self):
        super(TCDSConfigForTestMultiBChannels, self).handle_args()
        # Extract the driving mode.
        self.mode = self.args.mode
        # End of handle_args().

    def main(self):
        sep_line = "-" * 70

        test_dir_name = os.path.dirname(constants_file_name)

        #----------

        pm = None
        if self.mode == "lpm":
            pm = LPM
        elif self.mode == "cpm":
            pm = CPM

        #----------

        tmp = "".join(["{0:d}&{1:d}%".format(i[0], i[1]) for i in FED_IDS_MISC])
        if self.with_daq:
            pm_daq_on_off = '1'
        else:
            pm_daq_on_off = '0'

        if self.with_gt:
            gt1_enabled_disabled = '1'
            gt2_enabled_disabled = '1'
        else:
            gt1_enabled_disabled = '0'
            gt2_enabled_disabled = '0'

        fed_enable_mask = "{0:d}&{1:s}%{2:d}&{3:s}%{4:d}&{5:s}%{6:d}&{7:s}%{8:s}".format(FED_ID_CPM,
                                                                                         pm_daq_on_off,
                                                                                         FED_ID_LPM,
                                                                                         pm_daq_on_off,
                                                                                         FED_ID_GT1,
                                                                                         gt1_enabled_disabled,
                                                                                         FED_ID_GT2,
                                                                                         gt2_enabled_disabled,
                                                                                         tmp)
        #----------

        fsm_ctrl_cmd = "../utils/tcds_fsm_control.py --setup-file {0:s} --location {1:s}".format(self.setup_file_name, self.location_name)
        fsm_poll_cmd = "../utils/tcds_fsm_poller.py --setup-file {0:s} --location {1:s}".format(self.setup_file_name, self.location_name)
        pi_force_tts_cmd = "../pi/tcds_pi_force_tts.py --setup-file {0:s} --location {1:s}".format(self.setup_file_name, self.location_name)
        read_ttcspy_log_cmd = "../pi/tcds_pi_read_ttcspy_log.py --setup-file {0:s} --location {1:s}".format(self.setup_file_name, self.location_name)
        ici_send_soap_cmd = "../utils/tcds_send_soap_command.py --setup-file {0:s} --location {1:s}".format(self.setup_file_name, self.location_name)
        analyse_cmd = "./tcds_analyse_ttcspy_bchannels.py"
        cmds = []

        #----------

        # Halt everything.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Halting all controllers'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = []
        if pm:
            targets = [pm]
        targets.extend(ICIS)
        targets.extend(APVES)
        targets.extend(PIS)
        for target in targets:
            cmds.append(("{0:s} {1:s} Halt".format(fsm_ctrl_cmd, target), ""))

        # Wait till everything is in the Halted state.
        targets = []
        if pm:
            targets = [pm]
        targets.extend(ICIS)
        targets.extend(APVES)
        targets.extend(PIS)
        for target in targets:
            cmds.append(("{0:s} {1:s} --wait-until Halted".format(fsm_poll_cmd, target), ""))

        #----------

        # Configure PM.
        if pm:
            no_beam_active_bit = ""
            if self.mode == 'cpm':
                no_beam_active_bit = " --no-beam-active"
            cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                         ("python -c \"print 'Configuring {0:s}'\"".format(self.mode.upper()), ""),
                         ("python -c \"print '{0:s}'\"".format(sep_line), "")])
            cmds.append(("{0:s} --fed-enable-mask='{1:s}' {2:s} {3:s} Configure {4:s}/hw_cfg_{5:s}.txt".format(fsm_ctrl_cmd, fed_enable_mask, no_beam_active_bit, pm, test_dir_name, self.mode), ""))
            # Wait till the PM is in the Configured state.
            cmds.append(("{0:s} {1:s} --wait-until Configured".format(fsm_poll_cmd, pm), ""))

        # Configure iCIs.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Configuring iCIs'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list(ICIS)
        for target in targets:
            tmp = ""
            if pm or self.mode == "ttcmi":
                tmp = "_under_{0:s}".format(self.mode)
            cmds.append(("{0:s} {1:s} Configure {2:s}/hw_cfg_ici{3:s}.txt".format(fsm_ctrl_cmd, target, test_dir_name, tmp), ""))

        # Wait till everything is in the Configured state.
        for target in targets:
            cmds.append(("{0:s} {1:s} --wait-until Configured".format(fsm_poll_cmd, target), ""))

        # Configure APVEs.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Configuring APVEs'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list(APVES)
        for target in targets:
            cmds.append(("{0:s} {1:s} Configure {2:s}/hw_cfg_apve.txt".format(fsm_ctrl_cmd, target, test_dir_name), ""))

        # Wait till everything is in the Configured state.
        for target in targets:
            cmds.append(("{0:s} {1:s} --wait-until Configured".format(fsm_poll_cmd, target), ""))

        # Configure PIs.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Configuring PIs'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list(PIS)
        for target in targets:
            cmds.append(("{0:s} --fed-enable-mask='{1:s}' {2:s} Configure {3:s}/hw_cfg_pi.txt".format(fsm_ctrl_cmd, fed_enable_mask, target, test_dir_name), ""))

        # Wait till everything is in the Configured state.
        for target in targets:
            cmds.append(("{0:s} {1:s} --wait-until Configured".format(fsm_poll_cmd, target), ""))

        #----------

        # Force PIs to be TTS READY.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Forcing PI TTS states to READY'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list(PIS)
        for target in targets:
            cmds.append(("{0:s} {1:s} READY".format(pi_force_tts_cmd, target), ""))

        #----------

        # Enable PIs.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Enabling PIs'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list(PIS)
        for target in targets:
            cmds.append(("{0:s} {1:s} Enable".format(fsm_ctrl_cmd, target), ""))

        # Wait till all PIs are in the Enabled state.
        for target in targets:
            cmds.append(("{0:s} {1:s} --wait-until Enabled".format(fsm_poll_cmd, target), ""))

        # Enable APVEs.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Enabling APVEs'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list(APVES)
        for target in targets:
            cmds.append(("{0:s} {1:s} Enable".format(fsm_ctrl_cmd, target), ""))

        # Wait till all APVEs are in the Enabled state.
        for target in targets:
            cmds.append(("{0:s} {1:s} --wait-until Enabled".format(fsm_poll_cmd, target), ""))

        # Enable iCIs.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Enabling iCIs'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list(ICIS)
        for target in targets:
            cmds.append(("{0:s} {1:s} Enable".format(fsm_ctrl_cmd, target), ""))

        # Wait till all ICIs are in the Enabled state.
        for target in targets:
            cmds.append(("{0:s} {1:s} --wait-until Enabled".format(fsm_poll_cmd, target), ""))

        # Enable PM (as last!).
        # NOTE: If we're running without PM (i.e., in a single iCI run
        # driven by the iCI itself) we need to issue a 'fake' run start sequence.
        if pm:
            cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                         ("python -c \"print 'Enabling {0:s}'\"".format(self.mode.upper()), ""),
                         ("python -c \"print '{0:s}'\"".format(sep_line), "")])
            cmds.append(("{0:s} {1:s} Enable".format(fsm_ctrl_cmd, pm), ""))
        else:
            cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                         ("python -c \"print 'Sending fake run start"
                          " (because we\\'re running without CPM/LPM)'\"", ""),
                         ("python -c \"print '{0:s}'\"".format(sep_line), "")])
            targets = list(ICIS)
            for target in targets:
                for bgo in ["Resync", "OC0", "Start", "EC0"]:
                    cmds.append(("python -c \"print '  B-go {0:s}'\"".format(bgo), ""))
                    cmds.append(("{0:s} {1:s} SendBgo bgoName:string:{2:s}".format(ici_send_soap_cmd, target, bgo), ""))

        # Wait till also the PM is in the Enabled state.
        if pm:
            cmds.append(("{0:s} {1:s} --wait-until Enabled".format(fsm_poll_cmd, pm), ""))

        #----------

        # Wait a little.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Practicing patience...'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        cmds.append(("python -c \"import time; time.sleep(10)\"", ""))

        #----------

        # Check the TTCSpy log in all PIs.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Checking the TTCSpy log in all PIs'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list(PIS)
        for target in targets:
            file_name = "{0:s}_ttcspy_log.txt".format(target)
            cmds.append(("{0:s} {1:s} &> {2:s}".format(read_ttcspy_log_cmd, target, file_name), ""))

        #----------

        # Analyse the results.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Analysing results'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = PIS
        for target in targets:
            cmds.extend([("python -c \"print '  {0:s}'\"".format(target), "")])
            file_name = "{0:s}_ttcspy_log.txt".format(target)
            cmds.append(("{0:s} {1:s}".format(analyse_cmd, file_name), ""))

        #----------

        cmds = add_cwd_to_cmds(cmds)
        if self.verbose:
            cmds = append_piece_to_cmds(cmds, '--verbose')

        runner = CommandRunner(cmds)
        res = runner.run()

        # End of main().
        return res

    # End of class TCDSConfigForTestMultiBChannels.

###############################################################################

if __name__ == "__main__":

    description = "Master script to verify the ICI multi-B-channels."
    res = TCDSConfigForTestMultiBChannels(description).run()
    print "Done"
    sys.exit(res)

###############################################################################
