#----------------------------------------------------------------------
# iCI configuration file for iCI part of tests.
#----------------------------------------------------------------------

# Enabled inputs.
main.inselect.combined_cyclic_bgo_enable                                 0x00000001
main.inselect.combined_cyclic_trigger_enable                             0x00000001
main.inselect.lemo_orbit_select                                          0x00000001

# Disabled inputs.
main.inselect.combined_software_bgo_enable                               0x00000000
main.inselect.combined_software_trigger_enable                           0x00000000
main.inselect.combined_achannel_trigger_enable                           0x00000000
main.inselect.combined_bchannel_trigger_enable                           0x00000000
main.inselect.combined_bunch_trigger_generator_trigger_enable            0x00000000
main.inselect.combined_l1a_in0_trigger_enable                            0x00000000
main.inselect.combined_l1a_in1_trigger_enable                            0x00000000
main.inselect.combined_trigger_generator_trigger_enable                  0x00000000
main.inselect.exclusive_cpm_bgo_enable                                   0x00000000
main.inselect.exclusive_cpm_trigger_enable                               0x00000000
main.inselect.exclusive_lpm_bgo_enable                                   0x00000000
main.inselect.exclusive_lpm2_bgo_enable                                  0x00000000
main.inselect.exclusive_lpm_trigger_enable                               0x00000000
main.inselect.exclusive_lpm2_trigger_enable                              0x00000000
main.inselect.cpm_orbit_select                                           0x00000000
main.inselect.lpm_orbit_select                                           0x00000000
main.inselect.lpm2_orbit_select                                          0x00000000
main.inselect.sequence_trigger_generator_enable                          0x00000000

#----------------------------------------
# Cyclic generator 0: LumiNibble B-go.
#----------------------------------------
cyclic_generator0.configuration.enabled                                 0x00000001
cyclic_generator0.configuration.initial_prescale                        0x00001000
cyclic_generator0.configuration.pause                                   0x00000000
cyclic_generator0.configuration.permanent                               0x00000001
cyclic_generator0.configuration.postscale                               0x00000000
cyclic_generator0.configuration.prescale                                0x00001000
cyclic_generator0.configuration.repeat_cycle                            0x00000001
cyclic_generator0.configuration.start_bx                                0x00000001
cyclic_generator0.configuration.init_on_bgo                             0x00000001
cyclic_generator0.configuration.init_bgo_id                             0x00000008
cyclic_generator0.configuration.trigger_word.bgo_command                0x00000001
cyclic_generator0.configuration.trigger_word.bgo_sequence_command       0x00000000
cyclic_generator0.configuration.trigger_word.id                         0x00000000
cyclic_generator0.configuration.trigger_word.l1a                        0x00000000

#----------------------------------------
# Cyclic generator 1: BC0 B-go.
#----------------------------------------
cyclic_generator1.configuration.enabled                                 0x00000001
cyclic_generator1.configuration.initial_prescale                        0x00000000
cyclic_generator1.configuration.pause                                   0x00000000
cyclic_generator1.configuration.permanent                               0x00000001
cyclic_generator1.configuration.postscale                               0x00000000
cyclic_generator1.configuration.prescale                                0x00000000
cyclic_generator1.configuration.repeat_cycle                            0x00000001
cyclic_generator1.configuration.start_bx                                0x00000dd4
cyclic_generator1.configuration.init_on_bgo                             0x00000000
cyclic_generator1.configuration.init_bgo_id                             0x00000000
cyclic_generator1.configuration.trigger_word.bgo_command                0x00000001
cyclic_generator1.configuration.trigger_word.bgo_sequence_command       0x00000000
cyclic_generator1.configuration.trigger_word.id                         0x00000001
cyclic_generator1.configuration.trigger_word.l1a                        0x00000000

#----------------------------------------
# Cyclic generator 2: PrivateGap B-go.
#----------------------------------------
cyclic_generator2.configuration.enabled                                 0x00000001
cyclic_generator2.configuration.initial_prescale                        0x00000003
cyclic_generator2.configuration.pause                                   0x00000000
cyclic_generator2.configuration.permanent                               0x00000000
cyclic_generator2.configuration.postscale                               0x00000000
cyclic_generator2.configuration.prescale                                0x00000003
cyclic_generator2.configuration.repeat_cycle                            0x00000001
cyclic_generator2.configuration.start_bx                                0x00000cd3
cyclic_generator2.configuration.init_on_bgo                             0x00000001
cyclic_generator2.configuration.init_bgo_id                             0x00000008
cyclic_generator2.configuration.trigger_word.bgo_command                0x00000001
cyclic_generator2.configuration.trigger_word.bgo_sequence_command       0x00000000
cyclic_generator2.configuration.trigger_word.id                         0x00000003
cyclic_generator2.configuration.trigger_word.l1a                        0x00000000

#----------------------------------------
# Cyclic generator 3: B-go 2.
#----------------------------------------
cyclic_generator3.configuration.enabled                                 0x00000001
cyclic_generator3.configuration.initial_prescale                        0x00000070
cyclic_generator3.configuration.pause                                   0x00000000
cyclic_generator3.configuration.permanent                               0x00000000
cyclic_generator3.configuration.postscale                               0x00000000
# Prescale 0x70 = 112: rate becomes 100 Hz.
cyclic_generator3.configuration.prescale                                0x00000070
cyclic_generator3.configuration.repeat_cycle                            0x00000001
cyclic_generator3.configuration.start_bx                                0x00000cd3
cyclic_generator3.configuration.init_on_bgo                             0x00000000
cyclic_generator3.configuration.init_bgo_id                             0x00000000
cyclic_generator3.configuration.trigger_word.bgo_command                0x00000001
cyclic_generator3.configuration.trigger_word.bgo_sequence_command       0x00000000
cyclic_generator3.configuration.trigger_word.id                         0x00000002
cyclic_generator3.configuration.trigger_word.l1a                        0x00000000

#----------------------------------------
# Cyclic generator 4: Periodic Resync for debugging/commissioning.
#----------------------------------------
cyclic_generator4.configuration.enabled                                 0x00000000
cyclic_generator4.configuration.initial_prescale                        0x000057dc
cyclic_generator4.configuration.pause                                   0x00000000
cyclic_generator4.configuration.permanent                               0x00000000
cyclic_generator4.configuration.postscale                               0x00000000
# Prescale to about 0.5 Hz.
cyclic_generator4.configuration.prescale                                0x000057dc
cyclic_generator4.configuration.repeat_cycle                            0x00000001
cyclic_generator4.configuration.start_bx                                0x00000010
cyclic_generator4.configuration.init_on_bgo                             0x00000000
cyclic_generator4.configuration.init_bgo_id                             0x00000000
cyclic_generator4.configuration.trigger_word.bgo_command                0x00000000
cyclic_generator4.configuration.trigger_word.bgo_sequence_command       0x00000001
cyclic_generator4.configuration.trigger_word.id                         0x00000004
cyclic_generator4.configuration.trigger_word.l1a                        0x00000000

#----------------------------------------
# Cyclic generator 5: Periodic HardReset for debugging/commissioning.
#----------------------------------------
cyclic_generator5.configuration.enabled                                 0x00000000
cyclic_generator5.configuration.initial_prescale                        0x000057dc
cyclic_generator5.configuration.pause                                   0x00000000
cyclic_generator5.configuration.permanent                               0x00000000
cyclic_generator5.configuration.postscale                               0x00000000
# Prescale to about 0.5 Hz.
cyclic_generator5.configuration.prescale                                0x000057dc
cyclic_generator5.configuration.repeat_cycle                            0x00000001
cyclic_generator5.configuration.start_bx                                0x00000020
cyclic_generator5.configuration.init_on_bgo                             0x00000000
cyclic_generator5.configuration.init_bgo_id                             0x00000000
cyclic_generator5.configuration.trigger_word.bgo_command                0x00000000
cyclic_generator5.configuration.trigger_word.bgo_sequence_command       0x00000001
cyclic_generator5.configuration.trigger_word.id                         0x00000005
cyclic_generator5.configuration.trigger_word.l1a                        0x00000000

#----------------------------------------
# Cyclic generator 6: Playtime! (B-go 16)
#----------------------------------------
cyclic_generator6.configuration.enabled                                 0x00000001
cyclic_generator6.configuration.initial_prescale                        0x00000000
cyclic_generator6.configuration.pause                                   0x00000000
cyclic_generator6.configuration.permanent                               0x00000000
cyclic_generator6.configuration.postscale                               0x00000000
cyclic_generator6.configuration.prescale                                0x00000000
cyclic_generator6.configuration.repeat_cycle                            0x00000001
cyclic_generator6.configuration.start_bx                                0x000003e8
cyclic_generator6.configuration.init_on_bgo                             0x00000000
cyclic_generator6.configuration.init_bgo_id                             0x00000000
cyclic_generator6.configuration.trigger_word.bgo_command                0x00000001
cyclic_generator6.configuration.trigger_word.bgo_sequence_command       0x00000000
cyclic_generator6.configuration.trigger_word.id                         0x00000010
cyclic_generator6.configuration.trigger_word.l1a                        0x00000000

#----------------------------------------
# Cyclic generator 7: Playtime! (B-go 32)
#----------------------------------------
cyclic_generator7.configuration.enabled                                 0x00000001
cyclic_generator7.configuration.initial_prescale                        0x00000000
cyclic_generator7.configuration.pause                                   0x00000000
cyclic_generator7.configuration.permanent                               0x00000000
cyclic_generator7.configuration.postscale                               0x00000000
cyclic_generator7.configuration.prescale                                0x00000000
cyclic_generator7.configuration.repeat_cycle                            0x00000001
cyclic_generator7.configuration.start_bx                                0x00000018
cyclic_generator7.configuration.init_on_bgo                             0x00000000
cyclic_generator7.configuration.init_bgo_id                             0x00000000
cyclic_generator7.configuration.trigger_word.bgo_command                0x00000001
cyclic_generator7.configuration.trigger_word.bgo_sequence_command       0x00000000
cyclic_generator7.configuration.trigger_word.id                         0x00000020
cyclic_generator7.configuration.trigger_word.l1a                        0x00000000

#------------------------------
# B-channels.
#------------------------------

# B-channel 0: LumiNibble.
bchannels.bchannel0.configuration.single                                 0x00000000
bchannels.bchannel0.configuration.double                                 0x00000000
bchannels.bchannel0.configuration.bx_sync                                0x00000000
bchannels.bchannel0.configuration.bx_or_delay                            0x00000000
bchannels.bchannel0.configuration.initial_prescale                       0x00000000
bchannels.bchannel0.configuration.prescale                               0x00000000
bchannels.bchannel0.configuration.postscale                              0x00000000
bchannels.bchannel0.configuration.repeat_cycle                           0x00000001
bchannels.bchannel0.ram                                                  0x00000000 0x00000000
bchannels.bchannel0.ram                                                  0x00000000 0x00000006

# B-channel 1: BC0.
bchannels.bchannel1.configuration.single                                 0x00000001
bchannels.bchannel1.configuration.double                                 0x00000000
bchannels.bchannel1.configuration.bx_sync                                0x00000000
bchannels.bchannel1.configuration.bx_or_delay                            0x00000000
bchannels.bchannel1.configuration.initial_prescale                       0x00000000
bchannels.bchannel1.configuration.prescale                               0x00000000
bchannels.bchannel1.configuration.postscale                              0x00000000
bchannels.bchannel1.configuration.repeat_cycle                           0x00000001
bchannels.bchannel1.ram                                                  0x00000001 0x00000000
bchannels.bchannel1.ram                                                  0x00000000 0x00000006

# B-channel 2: TestEnable.
# NOTE: classic B-channel in delayed emission mode.
bchannels.bchannel2.configuration.single                                 0x00000001
bchannels.bchannel2.configuration.double                                 0x00000000
bchannels.bchannel2.configuration.bx_sync                                0x00000000
bchannels.bchannel2.configuration.bx_or_delay                            0x00000000
bchannels.bchannel2.configuration.initial_prescale                       0x00000000
bchannels.bchannel2.configuration.prescale                               0x00000000
bchannels.bchannel2.configuration.postscale                              0x00000000
bchannels.bchannel2.configuration.repeat_cycle                           0x00000001
bchannels.bchannel2.ram                                                  0x00000002 0x00000000
bchannels.bchannel2.ram                                                  0x00000000 0x00000006

# B-channel 3: PrivateGap.
# NOTE: classic B-channel in fixed-BX emission mode.
bchannels.bchannel3.configuration.single                                 0x00000001
bchannels.bchannel3.configuration.double                                 0x00000000
bchannels.bchannel3.configuration.bx_sync                                0x00000001
bchannels.bchannel3.configuration.bx_or_delay                            0x000000c8
bchannels.bchannel3.configuration.initial_prescale                       0x00000000
bchannels.bchannel3.configuration.prescale                               0x00000000
bchannels.bchannel3.configuration.postscale                              0x00000000
bchannels.bchannel3.configuration.repeat_cycle                           0x00000001
bchannels.bchannel3.ram                                                  0x00000003 0x00000000
bchannels.bchannel3.ram                                                  0x00000000 0x00000006

# B-channel 4: PrivateOrbit.
bchannels.bchannel4.configuration.single                                 0x00000000
bchannels.bchannel4.configuration.double                                 0x00000000
bchannels.bchannel4.configuration.bx_sync                                0x00000000
bchannels.bchannel4.configuration.bx_or_delay                            0x00000000
bchannels.bchannel4.configuration.initial_prescale                       0x00000000
bchannels.bchannel4.configuration.prescale                               0x00000000
bchannels.bchannel4.configuration.postscale                              0x00000000
bchannels.bchannel4.configuration.repeat_cycle                           0x00000001
bchannels.bchannel4.ram                                                  0x00000004 0x00000000
bchannels.bchannel4.ram                                                  0x00000000 0x00000006

# B-channel 5: Resync.
bchannels.bchannel5.configuration.single                                 0x00000000
bchannels.bchannel5.configuration.double                                 0x00000000
bchannels.bchannel5.configuration.bx_sync                                0x00000000
bchannels.bchannel5.configuration.bx_or_delay                            0x00000000
bchannels.bchannel5.configuration.initial_prescale                       0x00000000
bchannels.bchannel5.configuration.prescale                               0x00000000
bchannels.bchannel5.configuration.postscale                              0x00000000
bchannels.bchannel5.configuration.repeat_cycle                           0x00000001
bchannels.bchannel5.ram                                                  0x00000005 0x00000000
bchannels.bchannel5.ram                                                  0x00000000 0x00000006

# B-channel 6: HardReset.
bchannels.bchannel6.configuration.single                                 0x00000000
bchannels.bchannel6.configuration.double                                 0x00000000
bchannels.bchannel6.configuration.bx_sync                                0x00000000
bchannels.bchannel6.configuration.bx_or_delay                            0x00000000
bchannels.bchannel6.configuration.initial_prescale                       0x00000000
bchannels.bchannel6.configuration.prescale                               0x00000000
bchannels.bchannel6.configuration.postscale                              0x00000000
bchannels.bchannel6.configuration.repeat_cycle                           0x00000001
bchannels.bchannel6.ram                                                  0x00000006 0x00000000
bchannels.bchannel6.ram                                                  0x00000000 0x00000006

# B-channel 7: EC0.
bchannels.bchannel7.configuration.single                                 0x00000000
bchannels.bchannel7.configuration.double                                 0x00000000
bchannels.bchannel7.configuration.bx_sync                                0x00000000
bchannels.bchannel7.configuration.bx_or_delay                            0x00000000
bchannels.bchannel7.configuration.initial_prescale                       0x00000000
bchannels.bchannel7.configuration.prescale                               0x00000000
bchannels.bchannel7.configuration.postscale                              0x00000000
bchannels.bchannel7.configuration.repeat_cycle                           0x00000001
bchannels.bchannel7.ram                                                  0x00000007 0x00000000
bchannels.bchannel7.ram                                                  0x00000000 0x00000006

# B-channel 8: OC0.
bchannels.bchannel8.configuration.single                                 0x00000000
bchannels.bchannel8.configuration.double                                 0x00000000
bchannels.bchannel8.configuration.bx_sync                                0x00000000
bchannels.bchannel8.configuration.bx_or_delay                            0x00000000
bchannels.bchannel8.configuration.initial_prescale                       0x00000000
bchannels.bchannel8.configuration.prescale                               0x00000000
bchannels.bchannel8.configuration.postscale                              0x00000000
bchannels.bchannel8.configuration.repeat_cycle                           0x00000001
bchannels.bchannel8.ram                                                  0x00000008 0x00000000
bchannels.bchannel8.ram                                                  0x00000000 0x00000006

# B-channel 9: Start.
bchannels.bchannel9.configuration.single                                 0x00000000
bchannels.bchannel9.configuration.double                                 0x00000000
bchannels.bchannel9.configuration.bx_sync                                0x00000000
bchannels.bchannel9.configuration.bx_or_delay                            0x00000000
bchannels.bchannel9.configuration.initial_prescale                       0x00000000
bchannels.bchannel9.configuration.prescale                               0x00000000
bchannels.bchannel9.configuration.postscale                              0x00000000
bchannels.bchannel9.configuration.repeat_cycle                           0x00000001
bchannels.bchannel9.ram                                                  0x00000009 0x00000000
bchannels.bchannel9.ram                                                  0x00000000 0x00000006

# B-channel 10: Stop.
bchannels.bchannel10.configuration.single                                0x00000000
bchannels.bchannel10.configuration.double                                0x00000000
bchannels.bchannel10.configuration.bx_sync                               0x00000000
bchannels.bchannel10.configuration.bx_or_delay                           0x00000000
bchannels.bchannel10.configuration.initial_prescale                      0x00000000
bchannels.bchannel10.configuration.prescale                              0x00000000
bchannels.bchannel10.configuration.postscale                             0x00000000
bchannels.bchannel10.configuration.repeat_cycle                          0x00000001
bchannels.bchannel10.ram                                                 0x0000000a 0x00000000
bchannels.bchannel10.ram                                                 0x00000000 0x00000006

# B-channel 11: StartOfGap.
bchannels.bchannel11.configuration.single                                0x00000000
bchannels.bchannel11.configuration.double                                0x00000000
bchannels.bchannel11.configuration.bx_sync                               0x00000000
bchannels.bchannel11.configuration.bx_or_delay                           0x00000000
bchannels.bchannel11.configuration.initial_prescale                      0x00000000
bchannels.bchannel11.configuration.prescale                              0x00000000
bchannels.bchannel11.configuration.postscale                             0x00000000
bchannels.bchannel11.configuration.repeat_cycle                          0x00000001
bchannels.bchannel11.ram                                                 0x0000000b 0x00000000
bchannels.bchannel11.ram                                                 0x00000000 0x00000006

# B-channel 12.
bchannels.bchannel12.configuration.single                                0x00000000
bchannels.bchannel12.configuration.double                                0x00000000
bchannels.bchannel12.configuration.bx_sync                               0x00000000
bchannels.bchannel12.configuration.bx_or_delay                           0x00000000
bchannels.bchannel12.configuration.initial_prescale                      0x00000000
bchannels.bchannel12.configuration.prescale                              0x00000000
bchannels.bchannel12.configuration.postscale                             0x00000000
bchannels.bchannel12.configuration.repeat_cycle                          0x00000001
bchannels.bchannel12.ram                                                 0x0000000c 0x00000000
bchannels.bchannel12.ram                                                 0x00000000 0x00000006

# B-channel 13: WarningTestEnable.
bchannels.bchannel13.configuration.single                                0x00000000
bchannels.bchannel13.configuration.double                                0x00000000
bchannels.bchannel13.configuration.bx_sync                               0x00000000
bchannels.bchannel13.configuration.bx_or_delay                           0x00000000
bchannels.bchannel13.configuration.initial_prescale                      0x00000000
bchannels.bchannel13.configuration.prescale                              0x00000000
bchannels.bchannel13.configuration.postscale                             0x00000000
bchannels.bchannel13.configuration.repeat_cycle                          0x00000001
bchannels.bchannel13.ram                                                 0x0000000d 0x00000000
bchannels.bchannel13.ram                                                 0x00000000 0x00000006

# B-channel 14.
bchannels.bchannel14.configuration.single                                0x00000000
bchannels.bchannel14.configuration.double                                0x00000000
bchannels.bchannel14.configuration.bx_sync                               0x00000000
bchannels.bchannel14.configuration.bx_or_delay                           0x00000000
bchannels.bchannel14.configuration.initial_prescale                      0x00000000
bchannels.bchannel14.configuration.prescale                              0x00000000
bchannels.bchannel14.configuration.postscale                             0x00000000
bchannels.bchannel14.configuration.repeat_cycle                          0x00000001
bchannels.bchannel14.ram                                                 0x0000000e 0x00000000
bchannels.bchannel14.ram                                                 0x00000000 0x00000006

# B-channel 15.
bchannels.bchannel15.configuration.single                                0x00000000
bchannels.bchannel15.configuration.double                                0x00000000
bchannels.bchannel15.configuration.bx_sync                               0x00000000
bchannels.bchannel15.configuration.bx_or_delay                           0x00000000
bchannels.bchannel15.configuration.initial_prescale                      0x00000000
bchannels.bchannel15.configuration.prescale                              0x00000000
bchannels.bchannel15.configuration.postscale                             0x00000000
bchannels.bchannel15.configuration.repeat_cycle                          0x00000001
bchannels.bchannel15.ram                                                 0x0000000f 0x00000000
bchannels.bchannel15.ram                                                 0x00000000 0x00000006

#------------------------------
# Multi-B-channels.
#------------------------------

# Multi-B-channel 0. Spans B-gos 16-31.
# NOTE: multi-B-channel in delayed emission mode.
multibchannels.multibchannel0.configuration.bx_sync                     0x00000000
multibchannels.multibchannel0.configuration.bx_or_delay                 0x00000000
multibchannels.multibchannel0.configuration.bgo_selection.bgo16         0x00000001
multibchannels.multibchannel0.configuration.bgo_selection.bgo17         0x00000000
multibchannels.multibchannel0.configuration.bgo_selection.bgo18         0x00000000
multibchannels.multibchannel0.configuration.bgo_selection.bgo19         0x00000000
multibchannels.multibchannel0.configuration.bgo_selection.bgo20         0x00000000
multibchannels.multibchannel0.configuration.bgo_selection.bgo21         0x00000000
multibchannels.multibchannel0.configuration.bgo_selection.bgo22         0x00000000
multibchannels.multibchannel0.configuration.bgo_selection.bgo23         0x00000000
multibchannels.multibchannel0.configuration.bgo_selection.bgo24         0x00000000
multibchannels.multibchannel0.configuration.bgo_selection.bgo25         0x00000000
multibchannels.multibchannel0.configuration.bgo_selection.bgo26         0x00000000
multibchannels.multibchannel0.configuration.bgo_selection.bgo27         0x00000000
multibchannels.multibchannel0.configuration.bgo_selection.bgo28         0x00000000
multibchannels.multibchannel0.configuration.bgo_selection.bgo29         0x00000000
multibchannels.multibchannel0.configuration.bgo_selection.bgo30         0x00000000
multibchannels.multibchannel0.configuration.bgo_selection.bgo31         0x00000000

multibchannels.multibchannel0.ram.bgo16                                 0x000000ff
multibchannels.multibchannel0.ram.bgo17                                 0x00000001
multibchannels.multibchannel0.ram.bgo18                                 0x00000002
multibchannels.multibchannel0.ram.bgo19                                 0x00000003
multibchannels.multibchannel0.ram.bgo20                                 0x00000004
multibchannels.multibchannel0.ram.bgo21                                 0x00000005
multibchannels.multibchannel0.ram.bgo22                                 0x00000006
multibchannels.multibchannel0.ram.bgo23                                 0x00000007
multibchannels.multibchannel0.ram.bgo24                                 0x00000008
multibchannels.multibchannel0.ram.bgo25                                 0x00000009
multibchannels.multibchannel0.ram.bgo26                                 0x0000000a
multibchannels.multibchannel0.ram.bgo27                                 0x0000000b
multibchannels.multibchannel0.ram.bgo28                                 0x0000000c
multibchannels.multibchannel0.ram.bgo29                                 0x0000000d
multibchannels.multibchannel0.ram.bgo30                                 0x0000000e
multibchannels.multibchannel0.ram.bgo31                                 0x0000000f

# Multi-B-channel 1. Spans B-gos 32-47.
# NOTE: multi-B-channel in fixed-BX emission mode.
multibchannels.multibchannel1.configuration.bx_sync                     0x00000001
multibchannels.multibchannel1.configuration.bx_or_delay                 0x00000500
multibchannels.multibchannel1.configuration.bgo_selection.bgo32         0x00000001
multibchannels.multibchannel1.configuration.bgo_selection.bgo33         0x00000000
multibchannels.multibchannel1.configuration.bgo_selection.bgo34         0x00000000
multibchannels.multibchannel1.configuration.bgo_selection.bgo35         0x00000000
multibchannels.multibchannel1.configuration.bgo_selection.bgo36         0x00000000
multibchannels.multibchannel1.configuration.bgo_selection.bgo37         0x00000000
multibchannels.multibchannel1.configuration.bgo_selection.bgo38         0x00000000
multibchannels.multibchannel1.configuration.bgo_selection.bgo39         0x00000000
multibchannels.multibchannel1.configuration.bgo_selection.bgo40         0x00000000
multibchannels.multibchannel1.configuration.bgo_selection.bgo41         0x00000000
multibchannels.multibchannel1.configuration.bgo_selection.bgo42         0x00000000
multibchannels.multibchannel1.configuration.bgo_selection.bgo43         0x00000000
multibchannels.multibchannel1.configuration.bgo_selection.bgo44         0x00000000
multibchannels.multibchannel1.configuration.bgo_selection.bgo45         0x00000000
multibchannels.multibchannel1.configuration.bgo_selection.bgo46         0x00000000
multibchannels.multibchannel1.configuration.bgo_selection.bgo47         0x00000000

multibchannels.multibchannel1.ram.bgo32                                 0x000000ee
multibchannels.multibchannel1.ram.bgo33                                 0x00000001
multibchannels.multibchannel1.ram.bgo34                                 0x00000002
multibchannels.multibchannel1.ram.bgo35                                 0x00000003
multibchannels.multibchannel1.ram.bgo36                                 0x00000004
multibchannels.multibchannel1.ram.bgo37                                 0x00000005
multibchannels.multibchannel1.ram.bgo38                                 0x00000006
multibchannels.multibchannel1.ram.bgo39                                 0x00000007
multibchannels.multibchannel1.ram.bgo40                                 0x00000008
multibchannels.multibchannel1.ram.bgo41                                 0x00000009
multibchannels.multibchannel1.ram.bgo42                                 0x0000000a
multibchannels.multibchannel1.ram.bgo43                                 0x0000000b
multibchannels.multibchannel1.ram.bgo44                                 0x0000000c
multibchannels.multibchannel1.ram.bgo45                                 0x0000000d
multibchannels.multibchannel1.ram.bgo46                                 0x0000000e
multibchannels.multibchannel1.ram.bgo47                                 0x0000000f

# Multi-B-channel 2. Spans B-gos 48-63.
multibchannels.multibchannel2.configuration.bx_sync                     0x00000000
multibchannels.multibchannel2.configuration.bx_or_delay                 0x00000000
multibchannels.multibchannel2.configuration.bgo_selection.bgo48         0x00000000
multibchannels.multibchannel2.configuration.bgo_selection.bgo49         0x00000000
multibchannels.multibchannel2.configuration.bgo_selection.bgo50         0x00000000
multibchannels.multibchannel2.configuration.bgo_selection.bgo51         0x00000000
multibchannels.multibchannel2.configuration.bgo_selection.bgo52         0x00000000
multibchannels.multibchannel2.configuration.bgo_selection.bgo53         0x00000000
multibchannels.multibchannel2.configuration.bgo_selection.bgo54         0x00000000
multibchannels.multibchannel2.configuration.bgo_selection.bgo55         0x00000000
multibchannels.multibchannel2.configuration.bgo_selection.bgo56         0x00000000
multibchannels.multibchannel2.configuration.bgo_selection.bgo57         0x00000000
multibchannels.multibchannel2.configuration.bgo_selection.bgo58         0x00000000
multibchannels.multibchannel2.configuration.bgo_selection.bgo59         0x00000000
multibchannels.multibchannel2.configuration.bgo_selection.bgo60         0x00000000
multibchannels.multibchannel2.configuration.bgo_selection.bgo61         0x00000000
multibchannels.multibchannel2.configuration.bgo_selection.bgo62         0x00000000
multibchannels.multibchannel2.configuration.bgo_selection.bgo63         0x00000000

multibchannels.multibchannel2.ram.bgo48                                 0x00000000
multibchannels.multibchannel2.ram.bgo49                                 0x00000001
multibchannels.multibchannel2.ram.bgo50                                 0x00000002
multibchannels.multibchannel2.ram.bgo51                                 0x00000003
multibchannels.multibchannel2.ram.bgo52                                 0x00000004
multibchannels.multibchannel2.ram.bgo53                                 0x00000005
multibchannels.multibchannel2.ram.bgo54                                 0x00000006
multibchannels.multibchannel2.ram.bgo55                                 0x00000007
multibchannels.multibchannel2.ram.bgo56                                 0x00000008
multibchannels.multibchannel2.ram.bgo57                                 0x00000009
multibchannels.multibchannel2.ram.bgo58                                 0x0000000a
multibchannels.multibchannel2.ram.bgo59                                 0x0000000b
multibchannels.multibchannel2.ram.bgo60                                 0x0000000c
multibchannels.multibchannel2.ram.bgo61                                 0x0000000d
multibchannels.multibchannel2.ram.bgo62                                 0x0000000e
multibchannels.multibchannel2.ram.bgo63                                 0x0000000f

#------------------------------
# B-data.
#------------------------------

# B-data configuration.
bchannels.main.bdata_config.bdata_enable                                 0x00000000
bchannels.main.bdata_config.bdata_source_select                          0x00000000
bchannels.main.bdata_config.bdata_type                                   0x00000001
bchannels.main.bdata_config.ttcrx_address                                0x00000000
bchannels.main.bdata_config.target_address                               0x00000000
bchannels.main.bdata_config.is_external                                  0x00000001

#----------------------------------------------------------------------
# End of iCI configuration.
#----------------------------------------------------------------------
