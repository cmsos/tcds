#!/bin/bash

# Usage: stdbuf -e 0 -o 0 ./run_all.sh |& tee run_all.out

declare -a cmds=(
    "cpm"
    "lpm"
    "ici"
    "ttcmi"
)

res=0
for i in "${cmds[@]}"
do
    echo "=========="
    echo "Mode: ${i}"
    echo "=========="
    if ./tcds_test_ici_multi_bchannels_run.py ${i}; then
        echo "Test result: succeeded"
    else
        echo "Test result: failed"
        res=1
    fi
done

echo "=========="
if [[ res -eq 0 ]]; then
    echo "Overall result: SUCCESS"
else
    echo "Overall result: FAILURE"
fi
echo "=========="
