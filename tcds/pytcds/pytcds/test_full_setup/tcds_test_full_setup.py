#!/usr/bin/env python

###############################################################################
## Master script to quickly check out a full TCDS setup by configuring
## and starting a CPM-driven run using the full system.
###############################################################################

import os
import sys

from pytcds.utils.tcds_cmd_line_base import CmdLineBase
from pytcds.utils.tcds_command_runner import CommandRunner
from pytcds.utils.tcds_command_runner import add_cwd_to_cmds
from pytcds.utils.tcds_utils_setup_description import sw_sorter_helper

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

FED_IDS = {
    'tcdsd3v' : {
        'CPM' : 1088,
        'GT1' : 4095,
        'GT2' : 4095
    },
    'tcdslight' : {
        'CPM' : 1024,
        'GT1' : 4095,
        'GT2' : 4095
    }
}

PARTITIONS = {
    'tcdsd3v' : ['D3V-1-1'],
    'tcdslight' : ['LIGHT-11', 'LIGHT-12', 'LIGHT-13', 'LIGHT-14', 'LIGHT-15', 'LIGHT-16', 'LIGHT-17']
}

###############################################################################

def append_piece_to_cmds(cmds, piece):
    res = []
    for (cmd, doc) in cmds:
        full_cmd = cmd
        if cmd.endswith(".py"):
            full_cmd = "%s %s" % (cmd, piece)
        res.append((full_cmd, doc))
    # End of append_piece_to_cmds().
    return res

###############################################################################

class TCDSConfigForTest(CmdLineBase):

    def __init__(self, description=None, usage=None, epilog=None):
        super(TCDSConfigForTest, self).__init__(description, usage, epilog)
        self.halt_only = False
        self.halt_and_configure_only = False
        # End of __init__().

    def setup_parser_custom(self):
        parser = self.parser
        # Add the choice to run only the 'halt everything' step.
        help_str = "Only Halt all applications"
        parser.add_argument("--halt-only",
                            action="store_true",
                            help=help_str)
        # Add the choice to run only the 'halt everything' and
        # 'configure' steps.
        help_str = "Only Halt and Configure all applications"
        parser.add_argument("--halt-and-configure-only",
                            action="store_true",
                            help=help_str)
        # End of setup_parser_custom().

    def handle_args(self):
        super(TCDSConfigForTest, self).handle_args()
        self.halt_only = self.args.halt_only
        self.halt_and_configure_only = self.args.halt_and_configure_only
        # End of handle_args().

    def main(self):
        sep_line = "-" * 70

        test_dir_name = os.path.dirname(os.path.abspath(__file__))

        fsm_ctrl_cmd = "../utils/tcds_fsm_control.py --setup-file {0:s} --location {1:s}".format(self.setup_file_name, self.location_name)
        fsm_poll_cmd = "../utils/tcds_fsm_poller.py --setup-file {0:s} --location {1:s}".format(self.setup_file_name, self.location_name)
        pi_force_tts_cmd = "../pi/tcds_pi_force_tts.py --setup-file {0:s} --location {1:s}".format(self.setup_file_name, self.location_name)

        cmds = []

        #----------

        do_halt = True
        do_configure = True
        do_enable = True
        if self.halt_only:
            do_halt = True
            do_configure = False
            do_enable = False
        elif self.halt_and_configure_only:
            do_halt = True
            do_configure = True
            do_enable = False

        #----------

        setup_name = self.setup.identifier

        # Let's figure out what our targets are.
        # NOTE: Yes, ugly...
        cpms = [j for (i, j) in self.setup.sw_targets.iteritems() \
                if i.startswith('cpm')]
        assert len(cpms) == 1
        cpm = cpms[0]
        icis = [j for (i, j) in self.setup.sw_targets.iteritems() \
                if i.startswith("ici")]
        apves = [j for (i, j) in self.setup.sw_targets.iteritems() \
                if i.startswith("apve")]
        pis = [j for (i, j) in self.setup.sw_targets.iteritems() \
               if i.startswith("pi")]
        fed_id_cpm = FED_IDS[setup_name]['CPM']
        fed_id_gt1 = FED_IDS[setup_name]['GT1']
        fed_id_gt2 = FED_IDS[setup_name]['GT2']

        pm_daq_on_off = '1'
        gt1_enabled_disabled = '0'
        gt2_enabled_disabled = '0'
        fed_enable_mask = "{0:d}&{1:s}%{2:d}&{3:s}%{4:d}&{5:s}%".format(fed_id_cpm,
                                                                        pm_daq_on_off,
                                                                        fed_id_gt1,
                                                                        gt1_enabled_disabled,
                                                                        fed_id_gt2,
                                                                        gt2_enabled_disabled)
        ttc_partition_map = "%".join(["{0:s}&1".format(i) for i in PARTITIONS[setup_name]]) + "%"

        # Sort everything once and for all.
        icis.sort(key=sw_sorter_helper)
        apves.sort(key=sw_sorter_helper)
        pis.sort(key=sw_sorter_helper)

        print "Using TCDS setup description '{0:s}'".format(self.setup.identifier)
        print "  CPM: {0:s}".format(cpm.identifier)
        print "  ICIs: {0:s}".format(", ".join([i.identifier for i in icis]))
        print "  APVEs: {0:s}".format(", ".join([i.identifier for i in apves]))
        print "  PIs: {0:s}".format(", ".join([i.identifier for i in pis]))

        #----------

        if do_halt:

            # Halt everything.
            cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                         ("python -c \"print 'Halting all controllers'\"", ""),
                         ("python -c \"print '{0:s}'\"".format(sep_line), "")])
            targets = []
            if not cpm is None:
                targets = [cpm]
            targets.extend(icis)
            targets.extend(apves)
            targets.extend(pis)
            for target in targets:
                cmds.append(("{0:s} {1:s} Halt".format(fsm_ctrl_cmd, target.identifier), ""))

            # Wait till everything is in the Halted state.
            targets = []
            if not cpm is None:
                targets = [cpm]
            targets.extend(icis)
            targets.extend(apves)
            targets.extend(pis)
            for target in targets:
                cmds.append(("{0:s} {1:s} --wait-until Halted".format(fsm_poll_cmd, target.identifier), ""))

        #----------

        if do_configure:

            # Configure CPM.
            if not cpm is None:
                cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                             ("python -c \"print 'Configuring CPM'\"", ""),
                             ("python -c \"print '{0:s}'\"".format(sep_line), "")])
                cmds.append(("{0:s} --fed-enable-mask='{1:s}' --ttc-partition-map='{2:s}' --no-beam-active {3:s} Configure {4:s}/hw_cfg_cpm.txt".format(fsm_ctrl_cmd, fed_enable_mask, ttc_partition_map, cpm.identifier, test_dir_name), ""))
                # Wait till the CPM is in the Configured state.
                cmds.append(("{0:s} {1:s} --wait-until Configured".format(fsm_poll_cmd, cpm.identifier), ""))

            # Configure ICIs.
            targets = list(icis)
            if len(targets):
                cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                             ("python -c \"print 'Configuring ICIs'\"", ""),
                             ("python -c \"print '{0:s}'\"".format(sep_line), "")])
                for target in targets:
                    cmds.append(("{0:s} {1:s} Configure hw_cfg_ici.txt".format(fsm_ctrl_cmd, target.identifier), ""))

                # Wait till everything is in the Configured state.
                for target in targets:
                    cmds.append(("{0:s} {1:s} --wait-until Configured".format(fsm_poll_cmd, target.identifier), ""))

            # Configure APVEs.
            targets = list(apves)
            if len(targets):
                cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                             ("python -c \"print 'Configuring APVEs'\"", ""),
                             ("python -c \"print '{0:s}'\"".format(sep_line), "")])
                for target in targets:
                    cmds.append(("{0:s} {1:s} Configure hw_cfg_apve.txt".format(fsm_ctrl_cmd, target.identifier), ""))

                # Wait till everything is in the Configured state.
                for target in targets:
                    cmds.append(("{0:s} {1:s} --wait-until Configured".format(fsm_poll_cmd, target.identifier), ""))

            # Configure PIs.
            targets = list(pis)
            if len(targets):
                cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                             ("python -c \"print 'Configuring PIs'\"", ""),
                             ("python -c \"print '{0:s}'\"".format(sep_line), "")])
                for target in targets:
                    cmds.append(("{0:s} --fed-enable-mask='{1:s}' {2:s} Configure hw_cfg_pi.txt".format(fsm_ctrl_cmd, fed_enable_mask, target.identifier), ""))

                # Wait till everything is in the Configured state.
                for target in targets:
                    cmds.append(("{0:s} {1:s} --wait-until Configured".format(fsm_poll_cmd, target.identifier), ""))

            #----------

            # # Force PIs to be TTS READY.
            # targets = list(pis)
            # if len(targets):
            #     cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
            #                  ("python -c \"print 'Forcing PI TTS states to READY'\"", ""),
            #                  ("python -c \"print '{0:s}'\"".format(sep_line), "")])
            #     for target in targets:
            #         cmds.append(("{0:s} {1:s} READY".format(pi_force_tts_cmd, target.hardware), ""))

        # #----------

        if do_enable:

            # Enable PIs.
            targets = list(pis)
            if len(targets):
                cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                             ("python -c \"print 'Enabling PIs'\"", ""),
                             ("python -c \"print '{0:s}'\"".format(sep_line), "")])
                for target in targets:
                    cmds.append(("{0:s} {1:s} Enable".format(fsm_ctrl_cmd, target.identifier), ""))

                # Wait till everything is in the Enabled state.
                for target in targets:
                    cmds.append(("{0:s} {1:s} --wait-until Enabled".format(fsm_poll_cmd, target.identifier), ""))

            # Enable APVEs.
            targets = list(apves)
            if len(targets):
                cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                             ("python -c \"print 'Enabling APVEs'\"", ""),
                             ("python -c \"print '{0:s}'\"".format(sep_line), "")])
                for target in targets:
                    cmds.append(("{0:s} {1:s} Enable".format(fsm_ctrl_cmd, target.identifier), ""))

                # Wait till everything is in the Enabled state.
                for target in targets:
                    cmds.append(("{0:s} {1:s} --wait-until Enabled".format(fsm_poll_cmd, target.identifier), ""))

            # Enable ICIs.
            targets = list(icis)
            if len(targets):
                cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                             ("python -c \"print 'Enabling ICIs'\"", ""),
                             ("python -c \"print '{0:s}'\"".format(sep_line), "")])
                for target in targets:
                    cmds.append(("{0:s} {1:s} Enable".format(fsm_ctrl_cmd, target.identifier), ""))

                # Wait till everything is in the Enabled state.
                for target in targets:
                    cmds.append(("{0:s} {1:s} --wait-until Enabled".format(fsm_poll_cmd, target.identifier), ""))

            # Enable CPM (as last!).
            if not cpm is None:
                cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                             ("python -c \"print 'Enabling CPM'\"", ""),
                             ("python -c \"print '{0:s}'\"".format(sep_line), "")])
                cmds.append(("{0:s} {1:s} Enable".format(fsm_ctrl_cmd, cpm.identifier), ""))

        #----------

        cmds = add_cwd_to_cmds(cmds)
        if self.verbose:
            cmds = append_piece_to_cmds(cmds, "--verbose")

        runner = CommandRunner(cmds)
        runner.run()

        # End of main().

    # End of class TCDSConfigForTest.

###############################################################################

if __name__ == "__main__":

    description = "Master script to quickly check out a full TCDS setup."

    res = TCDSConfigForTest(description).run()

    print "Done"
    sys.exit(res)

###############################################################################
