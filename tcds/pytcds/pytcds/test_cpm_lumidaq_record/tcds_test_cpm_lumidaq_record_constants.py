###############################################################################

# The addresses of the boards/applications to connect to.
CPM = "cpm"
ICIS = [
    "ici-11",
    # "ici-12",
    # "ici-13",
    # "ici-14",
    # "ici-15",
    # "ici-16",
    # "ici-17",
    # "ici-18"
]
APVES = [
    "apve-11",
    # "apve-12",
    # "apve-13",
    # "apve-14"
]
PIS = [
    "pi-11"
]

# The FED id of the CPM, so we can disable the readout.
FED_ID_CPM = 1024

# The FED id(s) of the GT, so we know if it is in the run or not (in
# which case we have to enable the corresponding trigger input).
FED_ID_GT1 = 1404
FED_ID_GT2 = 1405

# Some misc. FED ids, so we can enable/disable 'random' inputs on the
# PI.
FED_IDS_MISC = [[10, 2]]

###############################################################################
