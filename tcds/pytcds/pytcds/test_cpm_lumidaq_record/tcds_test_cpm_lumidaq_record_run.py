#!/usr/bin/env python

###############################################################################
## Master script to verify the BRILDAQ packet produced by the CPM.
## NOTE: This checks for a very specific bug where the CPM misses a
## one-BX count in the deadtimes when there is 100% deadtime. (I.e.,
## the BX in which the new lumi nibble starts.)
###############################################################################

import os
import sys

from pytcds.test_cpm_lumidaq_record.tcds_test_cpm_lumidaq_record_constants import __file__ as constants_file_name
from pytcds.test_cpm_lumidaq_record.tcds_test_cpm_lumidaq_record_constants import APVES
from pytcds.test_cpm_lumidaq_record.tcds_test_cpm_lumidaq_record_constants import CPM
from pytcds.test_cpm_lumidaq_record.tcds_test_cpm_lumidaq_record_constants import FED_ID_CPM
from pytcds.test_cpm_lumidaq_record.tcds_test_cpm_lumidaq_record_constants import FED_ID_GT1
from pytcds.test_cpm_lumidaq_record.tcds_test_cpm_lumidaq_record_constants import FED_ID_GT2
from pytcds.test_cpm_lumidaq_record.tcds_test_cpm_lumidaq_record_constants import FED_IDS_MISC
from pytcds.test_cpm_lumidaq_record.tcds_test_cpm_lumidaq_record_constants import ICIS
from pytcds.test_cpm_lumidaq_record.tcds_test_cpm_lumidaq_record_constants import PIS
from pytcds.utils.tcds_cmd_line_base import CmdLineBase
from pytcds.utils.tcds_command_runner import add_cwd_to_cmds
from pytcds.utils.tcds_command_runner import CommandRunner

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

def append_piece_to_cmds(cmds, piece):
    res = []
    for (cmd, doc) in cmds:
        full_cmd = cmd
        if cmd.endswith(".py"):
            full_cmd = "%s %s" % (cmd, piece)
        res.append((full_cmd, doc))
    # End of append_piece_to_cmds().
    return res

###############################################################################

class TCDSConfigForTest(CmdLineBase):

    def setup_parser_custom(self):
        # NOTE: Skip the default, since we don't need an argument.
        pass
        # End of setup_parser_custom().

    def main(self):
        sep_line = "-" * 70

        test_dir_name = os.path.dirname(constants_file_name)

        #----------

        tmp = "".join(["{0:d}&{1:d}%".format(i[0], i[1]) for i in FED_IDS_MISC])

        pm_daq_on_off = '0'
        gt1_enabled_disabled = '0'
        gt2_enabled_disabled = '0'

        fed_enable_mask = "{0:d}&{1:s}%{2:d}&{3:s}%{4:d}&{5:s}%{6:s}".format(FED_ID_CPM,
                                                                             pm_daq_on_off,
                                                                             FED_ID_GT1,
                                                                             gt1_enabled_disabled,
                                                                             FED_ID_GT2,
                                                                             gt2_enabled_disabled,
                                                                             tmp)

        #----------

        fsm_ctrl_cmd = "../utils/tcds_fsm_control.py --setup-file {0:s} --location {1:s}" \
            .format(self.setup_file_name, self.location_name)
        fsm_poll_cmd = "../utils/tcds_fsm_poller.py --setup-file {0:s} --location {1:s}" \
            .format(self.setup_file_name, self.location_name)
        pi_force_tts_cmd = "../pi/tcds_pi_force_tts.py --setup-file {0:s} --location {1:s}" \
            .format(self.setup_file_name, self.location_name)
        check_cmd = "../cpm/tcds_cpm_check_brildaq_record.py --setup-file {0:s} --location {1:s}" \
            .format(self.setup_file_name, self.location_name)

        cmds = []

        #----------

        # Halt everything.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Halting all controllers'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = [CPM]
        targets.extend(ICIS)
        targets.extend(APVES)
        targets.extend(PIS)
        for target in targets:
            cmds.append(("{0:s} {1:s} Halt".format(fsm_ctrl_cmd, target), ""))

        # Wait till everything is in the Halted state.
        targets = [CPM]
        targets.extend(ICIS)
        targets.extend(APVES)
        targets.extend(PIS)
        for target in targets:
            cmds.append(("{0:s} {1:s} --wait-until Halted".format(fsm_poll_cmd, target), ""))

        #----------

        # Configure CPM.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Configuring CPM'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        cmds.append(("{0:s} --fed-enable-mask='{1:s}' --no-beam-active {2:s} Configure {3:s}/hw_cfg_cpm.txt".format(fsm_ctrl_cmd, fed_enable_mask, CPM, test_dir_name), ""))
        # Wait till the CPM is in the Configured state.
        cmds.append(("{0:s} {1:s} --wait-until Configured".format(fsm_poll_cmd, CPM), ""))

        # Configure iCIs.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Configuring iCIs'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list(ICIS)
        for target in targets:
            cmds.append(("{0:s} {1:s} Configure {2:s}/hw_cfg_ici.txt".format(fsm_ctrl_cmd, target, test_dir_name), ""))

        # Wait till everything is in the Configured state.
        for target in targets:
            cmds.append(("{0:s} {1:s} --wait-until Configured".format(fsm_poll_cmd, target), ""))

        # Configure APVEs.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Configuring APVEs'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list(APVES)
        for target in targets:
            cmds.append(("{0:s} {1:s} Configure {2:s}/hw_cfg_apve.txt".format(fsm_ctrl_cmd, target, test_dir_name), ""))

        # Wait till everything is in the Configured state.
        for target in targets:
            cmds.append(("{0:s} {1:s} --wait-until Configured".format(fsm_poll_cmd, target), ""))

        # Configure PIs.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Configuring PIs'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list(PIS)
        for target in targets:
            cmds.append(("{0:s} --fed-enable-mask='{1:s}' {2:s} Configure {3:s}/hw_cfg_pi.txt".format(fsm_ctrl_cmd, fed_enable_mask, target, test_dir_name), ""))

        # Wait till everything is in the Configured state.
        for target in targets:
            cmds.append(("{0:s} {1:s} --wait-until Configured".format(fsm_poll_cmd, target), ""))

        #----------

        # Force PIs TTS states in order to create 100% TTS deadtime.
        pi_tts_state = "BUSY"
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Forcing PI TTS states to {0:s}'\"".format(pi_tts_state), ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list(PIS)
        for target in targets:
            cmds.append(("{0:s} {1:s} {2:s}".format(pi_force_tts_cmd, target, pi_tts_state), ""))

        #----------

        # Enable PIs.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Enabling PIs'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list(PIS)
        for target in targets:
            cmds.append(("{0:s} {1:s} Enable".format(fsm_ctrl_cmd, target), ""))

        # Wait till all PIs are in the Enabled state.
        for target in targets:
            cmds.append(("{0:s} {1:s} --wait-until Enabled".format(fsm_poll_cmd, target), ""))

        # Enable APVEs.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Enabling APVEs'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list(APVES)
        for target in targets:
            cmds.append(("{0:s} {1:s} Enable".format(fsm_ctrl_cmd, target), ""))

        # Wait till all APVEs are in the Enabled state.
        for target in targets:
            cmds.append(("{0:s} {1:s} --wait-until Enabled".format(fsm_poll_cmd, target), ""))

        # Enable iCIs.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Enabling iCIs'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list(ICIS)
        for target in targets:
            cmds.append(("{0:s} {1:s} Enable".format(fsm_ctrl_cmd, target), ""))

        # Wait till all ICIs are in the Enabled state.
        for target in targets:
            cmds.append(("{0:s} {1:s} --wait-until Enabled".format(fsm_poll_cmd, target), ""))

        # Enable CPM (as last!).
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Enabling CPM'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        cmds.append(("{0:s} {1:s} Enable".format(fsm_ctrl_cmd, CPM), ""))

        # Wait till also the CPM is in the Enabled state.
        cmds.append(("{0:s} {1:s} --wait-until Enabled".format(fsm_poll_cmd, CPM), ""))

        #----------

        # Wait a little.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Practicing patience...'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        cmds.append(("python -c \"import time; time.sleep(10)\"", ""))

        #----------

        # Check the BRILDAQ record in the CPM
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Checking the BRILDAQ record in the CPM'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        # BUG BUG BUG
        # Hard-coded location of the hardware...
        cmds.append(("{0:s} {1:s}".format(check_cmd, "cpm-t1"), ""))
        # BUG BUG BUG end

        #----------

        cmds = add_cwd_to_cmds(cmds)
        if self.verbose:
            cmds = append_piece_to_cmds(cmds, '--verbose')

        runner = CommandRunner(cmds)
        runner.run()

        # End of main().

    # End of class TCDSConfigForTest.

###############################################################################

if __name__ == "__main__":

    description = "Master script to verify the BRILDAQ packet" \
        "produced by the CPM."
    res = TCDSConfigForTest(description).run()
    print "Done"
    sys.exit(res)

###############################################################################
