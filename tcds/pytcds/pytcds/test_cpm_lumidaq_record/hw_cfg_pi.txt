#----------------------------------------------------------------------
# PI configuration file for APVE tests.
#----------------------------------------------------------------------

#----------------------------------------
# TTCSpy configuration.
#----------------------------------------
# Logging mode etc.
ttc_spy.control.logging_mode                            0x00000000
ttc_spy.control.trigger_combination_operator            0x00000000
sync.bc0_bcommand                                       0x00000001
sync.bc0_reset_val                                      0x00000dd4

# Trigger mask.
ttc_spy.trigger_mask.l1a                                0x00000000
ttc_spy.trigger_mask.add_all                            0x00000000
ttc_spy.trigger_mask.brc_all                            0x00000000
ttc_spy.trigger_mask.brc_bc0                            0x00000000
ttc_spy.trigger_mask.brc_ec0                            0x00000000
ttc_spy.trigger_mask.brc_dddd_all                       0x00000000
ttc_spy.trigger_mask.brc_tt_all                         0x00000000
ttc_spy.trigger_mask.brc_zero_data                      0x00000001
ttc_spy.trigger_mask.adr_zero_data                      0x00000000
ttc_spy.trigger_mask.err_com                            0x00000000
ttc_spy.trigger_mask.brc_tt                             0x00000000
ttc_spy.trigger_mask.brc_dddd                           0x00000000
ttc_spy.trigger_mask.brc_val0                           0x00000008
ttc_spy.trigger_mask.brc_val1                           0x00000000
ttc_spy.trigger_mask.brc_val2                           0x00000000
ttc_spy.trigger_mask.brc_val3                           0x00000000
ttc_spy.trigger_mask.brc_val4                           0x00000000
ttc_spy.trigger_mask.brc_val5                           0x00000000

#----------------------------------------------------------------------
