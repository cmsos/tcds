#!/usr/bin/env python

###############################################################################
## Utility to scan the phase of the reference clock. Used to find the
## calibration factor for the PhaseMon.
###############################################################################

import commands
import math
import pickle
import simplejson
import sys
import time
import urllib2

import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt

from pytcds.phasemon.tcds_phasemon import get_phasemon_hw
from pytcds.utils.tcds_cmd_line_base import CmdLineBase
from pytcds.utils.tcds_utils_networking import resolve_network_address

try:
    import debug_hook
    import pdb
except ImportError:
    pass


###############################################################################

# The number of steps in the scan.
NUM_STEPS = 25

# The number of measurements to take at each scan point.
NUM_SAMPLES = 5

# The number of samples to integrate into each White Rabbit
# measurement.
NUM_MEAS_WR = 0x10

# The clock frequency.
INPUT_CLOCK = 11246. * 3564

# The size of a single PLL phase-shift step.
SHIFT_STEP = (1. / (INPUT_CLOCK * 20. * 56.)) * 1.e12

# The step size between two adjacent scan points, expressed in
# PLL phase-shift steps.
STEP_SIZE = 56

# The direction of the scan: up or down.
DIRECTION = 1

###############################################################################

class Scanner(CmdLineBase):

    def handle_args(self):
        # One argument is requited. It has to sound like a network
        # address.
        if len(self.args) != 1:
            self.error("One argument is required: " \
                       "the IP address of the device.")
        else:
            # Extract the IP address.
            ip_address_arg = self.args[0]
            ip_address = resolve_network_address(ip_address_arg)
            if not ip_address:
                msg = "'{0:s}' does not sound like an IP/network address.".format(ip_address_arg)
                self.error(msg)
            else:
                self.network_address = ip_address_arg
                self.ip_address = ip_address
        # End of handle_args().

    def main(self):
        phasemon = get_phasemon_hw(self.ip_address, verbose=self.verbose)
        if not phasemon:
            self.error("Could not connect to {0:s}.".format(self.ip_address))

        #----------

        print "Preparing scan..."

        # Select the external reference clock.
        phasemon.write("phasemon.refclk_select", 0x1)

        # Reset the main TTC clock PLL so we start from zero.
        phasemon.write("user.ttc_clock_pll_reset", 0x0)
        phasemon.write("user.ttc_clock_pll_reset", 0x1)
        phasemon.write("user.ttc_clock_pll_reset", 0x0)
        time.sleep(1.)

        # Reset the two White-Rabbit MMCMs and check that they lock.
        phasemon.write("phasemon.meas_common.mmcm1_reset", 0x0)
        phasemon.write("phasemon.meas_common.mmcm1_reset", 0x1)
        phasemon.write("phasemon.meas_common.mmcm1_reset", 0x0)
        phasemon.write("phasemon.meas_common.mmcm2_reset", 0x0)
        phasemon.write("phasemon.meas_common.mmcm2_reset", 0x1)
        phasemon.write("phasemon.meas_common.mmcm2_reset", 0x0)
        locked1 = phasemon.read("phasemon.meas_common.mmcm1_locked")
        locked2 = phasemon.read("phasemon.meas_common.mmcm2_locked")
        if (locked1 != 0x1):
            self.error("Scan problem: White-Rabbit MMCM1 not locked.")
        if (locked2 != 0x1):
            self.error("Scan problem: White-Rabbit MMCM2 not locked.")

        phasemon.write("phasemon.meas_common.measurement_duration", NUM_MEAS_WR)

        inputs = [
            "sfp1",
            "sfp2",
            "sfp3",
            "sfp4",
            "sfp5",
            "sfp6",
            "sfp7",
            "ttc_fmc",
            "fclka"
            ]
        for i in inputs:
            phasemon.write("phasemon.meas_{0:s}.enable".format(i), 0x1)

        # NORM_FACTOR = 1.
        # CALIB_FACTOR = 1.

        # Choose the scan direction.
        if DIRECTION < 0:
            phasemon.write("phasemon.phase_shift.direction", 0x0)
        else:
            phasemon.write("phasemon.phase_shift.direction", 0x1)
        phasemon.write("phasemon.phase_shift.strobe", 0x0)

        print "Scanning..."

        x_vals = []
        results = {}
        for i in xrange(NUM_STEPS):
            x = DIRECTION * SHIFT_STEP * STEP_SIZE * i
            print "  stepping..."
            for k in xrange(STEP_SIZE):
                # Strobe to make the shift happen.
                phasemon.write("phasemon.phase_shift.strobe", 0x1)
                phasemon.write("phasemon.phase_shift.strobe", 0x0)
                time.sleep(.1)
                done = phasemon.read("phasemon.phase_shift.done")
                if (done != 0x1):
                    self.error("Scan problem: phase shift did not succeed.")

            print "  sampling..."
            data = {}
            for j in xrange(NUM_SAMPLES):
                # NOTE: Turn everything into floating point numbers.

                # White Rabbit phase monitoring.
                for i in inputs:
                    phasemon.write("phasemon.meas_{0:s}.enable".format(i), 0x0)
                    phasemon.write("phasemon.meas_{0:s}.reset".format(i), 0x1)
                    phasemon.write("phasemon.meas_{0:s}.reset".format(i), 0x0)
                    phasemon.write("phasemon.meas_{0:s}.enable".format(i), 0x1)
                time.sleep(1.)
                for i in inputs:
                    phase_mon_val = float(phasemon.read("phasemon.meas_{0:s}.result".format(i)))
                    # phase_mon_val /= NORM_FACTOR
                    # phase_mon_val *= CALIB_FACTOR

        #         print "DEBUG JGH  40: x = {0:8.2f}, value = 0x{1:08x}".format(x, int(phase_mon_val_40))
                    try:
                        data[i].append(phase_mon_val)
                    except KeyError:
                        data[i] = [phase_mon_val]

            x_vals.append(x)
            for i in inputs:
                try:
                    results[i].append(data[i])
                except KeyError:
                    results[i] = [data[i]]

        #----------

        print "Storing results..."
        out_file_name = "phasemon_refclk_phase_scan.pkl"
        data = {
            "inputs" : inputs,
            "x_vals" : x_vals,
            "data" : results
            }
        with open(out_file_name, "wb") as out_file:
            pickle.dump(data, out_file)

        #----------

        # End of main().

    # End of class Scanner.

###############################################################################

if __name__ == "__main__":

    desc_str = "Helper script to test the PhaseMon."
    usage_str = "usage: %prog [OPTIONS] IP"

    Scanner(desc_str, usage_str).run()

    print "Done"

###############################################################################
