#!/usr/bin/env python

###############################################################################
## Master script to drive PhaseMon tests of the PI TTC PLLs (step 2).
###############################################################################

import sys

from pytcds.test_phasemon.tcds_test_phasemon_constants import PHASEMON
from pytcds.test_phasemon.tcds_test_phasemon_constants import PIS
from pytcds.test_phasemon.tcds_test_phasemon_constants import RF2TTC
from pytcds.test_phasemon.tcds_test_phasemon_constants import RF2TTC_VME
from pytcds.utils.tcds_cmd_line_base import CmdLineBase
from pytcds.utils.tcds_command_runner import add_cwd_to_cmds
from pytcds.utils.tcds_command_runner import CommandRunner

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

def append_piece_to_cmds(cmds, piece):
    res = []
    for (cmd, doc) in cmds:
        full_cmd = cmd
        if cmd.endswith(".py"):
            full_cmd = "%s %s" % (cmd, piece)
            res.append((full_cmd, doc))
            # End of append_piece_to_cmds().
    return res

###############################################################################

class TCDSConfigForTest(CmdLineBase):

    def handle_args(self):
        # NOTE: This is a slight abuse of the CmdLineBase base class
        # since we don't use an IP address argument. Don't tell
        # anyone.

        # No arguments are expected.
        if len(self.args) != 0:
            msg = "No arguments are expected."
            self.error(msg)
            # End of handle_args().

    def main(self):
        sep_line = "-" * 70

        phasemon_snapshot_cmd = "./tcds_do_phasemon_snapshot.py --cfg-file=../utils/tcds_setup_tcdslab.cfg"
        pi_snapshot_cmd = "./tcds_do_pi_snapshot.py --cfg-file=../utils/tcds_setup_tcdslab.cfg"
        rf2ttc_snapshot_cmd = "./tcds_do_rf2ttc_snapshot.py --cfg-file=../utils/tcds_setup_tcdslab.cfg"
        rf2ttc_toggle_cmd = "../rf2ttc/tcds_rf2ttc_toggle_bcmain.py"

        cmds = []

        #----------

        # Do RF2TTCController check and store result.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Snapshotting RF2TTC'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        cmds.append(("{0:s} {1:s}".format(rf2ttc_snapshot_cmd, RF2TTC), ""))

        #----------

        # Do PhaseMon measurement and store result.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Snapshotting PhaseMon'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        cmds.append(("{0:s} {1:s}".format(phasemon_snapshot_cmd, PHASEMON), ""))

        # Do PIController checks and store result.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Snapshotting PI phase heuristics'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        for pi in PIS:
            cmds.append(("{0:s} {1:s}".format(pi_snapshot_cmd, pi), ""))

        #----------

        # Toggle BC-main.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                    ("python -c \"print 'Toggling BC-main on RF2TTC'\"", ""),
                    ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        cmds.append(("{0:s} {1:s}".format(rf2ttc_toggle_cmd, RF2TTC_VME), ""))

        #----------

        # Do RF2TTCController check and store result.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Snapshotting RF2TTC'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        cmds.append(("{0:s} {1:s}".format(rf2ttc_snapshot_cmd, RF2TTC), ""))

        #----------

        # Do PhaseMon measurement and store result.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Snapshotting PhaseMon'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        cmds.append(("{0:s} {1:s}".format(phasemon_snapshot_cmd, PHASEMON), ""))

        # Do PIController checks and store result.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Snapshotting PI phase heuristics'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        for pi in PIS:
            cmds.append(("{0:s} {1:s}".format(pi_snapshot_cmd, pi), ""))

        #----------

        cmds = add_cwd_to_cmds(cmds)
        if self.verbose:
            cmds = append_piece_to_cmds(cmds, "--verbose")

        runner = CommandRunner(cmds)
        runner.run()

        # End of main().

    # End of class TCDSConfigForTest.

###############################################################################

if __name__ == "__main__":

    description = "Master script to drive PhaseMon tests of the PI TTC PLLs (step 2: measurement)."
    usage = "usage: %prog [options]"
    res = TCDSConfigForTest(description, usage).run()
    print "Done"
    sys.exit(res)

###############################################################################
