#----------------------------------------------------------------------
# PI configuration file for APVE tests.
#----------------------------------------------------------------------

#----------------------------------------
# TTCSpy configuration.
#----------------------------------------
# Logging mode etc.
ttcspy.logging_control.logging_buffer_mode              0x00000000
ttcspy.logging_control.logging_mode                     0x00000001
ttcspy.logging_control.trigger_combination_operator     0x00000000
ttcspy.logging_control.bc0_val                          0x00000001
ttcspy.logging_control.expected_bc0_bx                  0x00000dd4

# Trigger mask.
ttcspy.trigger_mask.l1a                                 0x00000000
ttcspy.trigger_mask.add_all                             0x00000000
ttcspy.trigger_mask.brc_all                             0x00000000
ttcspy.trigger_mask.brc_bc0                             0x00000000
ttcspy.trigger_mask.brc_ec0                             0x00000000
ttcspy.trigger_mask.brc_dddd_all                        0x00000000
ttcspy.trigger_mask.brc_tt_all                          0x00000000
ttcspy.trigger_mask.brc_zero_data                       0x00000001
ttcspy.trigger_mask.adr_zero_data                       0x00000000
ttcspy.trigger_mask.err_com                             0x00000000
ttcspy.trigger_mask.brc_tt                              0x00000000
ttcspy.trigger_mask.brc_dddd                            0x00000000
ttcspy.trigger_mask.brc_val0                            0x00000001
ttcspy.trigger_mask.brc_val1                            0x00000000
ttcspy.trigger_mask.brc_val2                            0x00000000
ttcspy.trigger_mask.brc_val3                            0x00000000
ttcspy.trigger_mask.brc_val4                            0x00000000
ttcspy.trigger_mask.brc_val5                            0x00000000

#----------------------------------------------------------------------
