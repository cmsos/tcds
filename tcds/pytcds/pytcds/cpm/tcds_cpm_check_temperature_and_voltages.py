#!/usr/bin/env python

###############################################################################
## Check CPM T1 FPGA temperature and voltages.
## NOTE: For testing purposes only.
###############################################################################

import sys

from collections import OrderedDict

from pytcds.cpm.tcds_cpm import get_cpm_hw
from pytcds.utils.tcds_cmd_line_base import CmdLineBase
from pytcds.utils.tcds_utils_networking import resolve_network_address

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class Checker(CmdLineBase):

    def main(self):
        self.validate_target(target_type=CmdLineBase.TARGET_TYPE_HW)
        board_info = self.setup.hw_targets[self.target]
        network_address = board_info.dns_alias
        controlhub_address = board_info.controlhub
        cpm = get_cpm_hw(network_address, controlhub_address, verbose=self.verbose)
        if not cpm:
            self.error("Could not connect to {0:s}.".format(self.ip_address))

        #----------

        measurements = OrderedDict([
            ["Temperature", ("temperature", .1, "C")],
            ["12V", ("12v", 1.e-3, "V")],
            ["1V0 (Vcc BRAM)", ("1v0_vccbram", 1.e-3, "V")],
            ["1V0 (Vccint)", ("1v0_vccint", 1.e-3, "V")],
            ["1V0A", ("1v0a", 1.e-3, "V")],
            ["1V2A", ("1v2a", 1.e-3, "V")],
            ["1V5", ("1v5", 1.e-3, "V")],
            ["1V8 (Vcc aux.)", ("1v8_vccaux", 1.e-3, "V")],
            ["1V8A (aux. GTX)", ("1v8a_vccauxgtx", 1.e-3, "V")],
            ["2V0 (aux. I/O)", ("2v0_vccauxio", 1.e-3, "V")],
            ["2V5", ("2v5", 1.e-3, "V")],
            ["3V3", ("3v3", 1.e-3, "V")],
            ["DDR3 Vref", ("ddr3_vref", 1.e-3, "V")],
            ["DDR3 Vtt", ("ddr3_vtt", 1.e-3, "V")]
        ])

        max_len = max([len(i) for i in measurements.keys()])
        for (meas, (reg_name_base, factor, units)) in measurements.iteritems():
            reg_name = "cpmt1.voltage_check." + reg_name_base
            val = factor * cpm.read(reg_name)
            print "{1:{0:d}s}: {2:5.2f} {3:s}".format(max_len, meas, val, units)

        #----------

        # End of main().

    # End of class Checker.

###############################################################################

if __name__ == "__main__":

    description = "Check CPM T1 FPGA temperature and voltages."

    res = Checker(description).run()

    print "Done"
    sys.exit(res)

###############################################################################
