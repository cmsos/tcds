#!/usr/bin/env python

###############################################################################
## Disable the DAQ backpressure on the CPM.
## NOTE: For testing purposes only.
###############################################################################

import sys
import time

from pytcds.cpm.tcds_cpm import get_cpm_hw
from pytcds.utils.tcds_cmd_line_base import CmdLineBase
from pytcds.utils.tcds_utils_networking import resolve_network_address

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class BackpressureDisabler(CmdLineBase):

    def handle_args(self):
        # One and only one argument is expected. It has to sound like
        # an IP address.
        if len(self.args) != 1:
            msg = "One arguments is required: " \
                  "the IP address of the device."
            self.error(msg)
        else:
            # Extract the IP address.
            ip_address_arg = self.args[0]
            ip_address = resolve_network_address(ip_address_arg)
            if not ip_address:
                msg = "'{0:s}' does not sound like a valid network address."
                msg = msg.format(ip_address_arg)
                self.error(msg)
            else:
                self.ip_address = ip_address
        # End of handle_args().

    def main(self):
        cpm = get_cpm_hw(self.ip_address, verbose=self.verbose)
        if not cpm:
            self.error("Could not connect to {0:s}.".format(self.ip_address))

        #----------

        cpm.write("cpmt1.ipm.main.inselect.daq_backpressure_enable", 0)

        #----------

        # End of main().

    # End of class BackpressureDisabler.

###############################################################################

if __name__ == "__main__":

    description = "Disable the DAQ backpressure on the CPM."
    usage = "usage: %prog [options] IP"
    res = BackpressureDisabler(description, usage).run()
    print "Done"
    sys.exit(res)

###############################################################################
