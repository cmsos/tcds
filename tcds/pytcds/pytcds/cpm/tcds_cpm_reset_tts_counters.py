#!/usr/bin/env python

###############################################################################
## Little helper to reset the TTS state-counter/-transition counters.
###############################################################################

import sys

from pytcds.utils.tcds_utils_hw_connect import get_amc13t1_hw
from pytcds.utils.tcds_cmd_line_base import CmdLineBase

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class Resetter(CmdLineBase):

    def main(self):
        self.validate_target(target_type=CmdLineBase.TARGET_TYPE_HW)
        board_info = self.setup.hw_targets[self.target]
        network_address = board_info.dns_alias
        controlhub_address = board_info.controlhub
        cpm = get_amc13t1_hw(network_address, controlhub_address, verbose=self.verbose)
        if not cpm:
            self.error("Could not connect to {0:s}.".format(self.ip_address))

        reg_name = "cpmt1.ipm.main.resets.tts_counters_reset"
        cpm.write(reg_name, 0x0)
        cpm.write(reg_name, 0x1)
        cpm.write(reg_name, 0x0)

        # End of main().

    # End of class Resetter.

###############################################################################

if __name__ == "__main__":

    desc_str = "Little helper to reset the TTS state-counter/-transition counters."

    res = Resetter(desc_str).run()

    sys.exit(res)
    print "Done"

###############################################################################
