#!/usr/bin/env python

###############################################################################
## Little helper to performa a coarse check of the book-keeping
## numbers in the CPM trigger-BX histograms.
###############################################################################

import sys

from pytcds.utils.tcds_utils_hw_connect import get_amc13t1_hw
from pytcds.utils.tcds_cmd_line_base import CmdLineBase

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class HistChecker(CmdLineBase):

    def main(self):
        num_trig_types = 16
        sep_line = 50 * "-"

        #----------

        self.validate_target(target_type=CmdLineBase.TARGET_TYPE_HW)

        #----------

        board_info = self.setup.hw_targets[self.target]
        network_address = board_info.dns_alias
        controlhub_address = board_info.controlhub
        cpm = get_amc13t1_hw(network_address, controlhub_address, verbose=self.verbose)
        if not cpm:
            self.error("Could not connect to {0:s}.".format(network_address))

        #----------

        # Read the zeroth entry (i.e., the lumi nibble number header)
        # for each trigger type.
        offset = 0
        print sep_line
        print "  BX histo lumi-nibble headers by trigger type (offset = 0x{0:x}) -> nibble number".format(offset)
        print sep_line
        for trig_type in xrange(num_trig_types):
            reg_name = "cpmt1.ipm.l1a_histos.trigger_type{0:d}".format(trig_type)
            node = cpm.get_node(reg_name)
            address = node.getAddress()
            print "  {0:d} -> {1:d}".format(trig_type, cpm.read(address + offset))

        # Read the last entry (i.e., the lumi nibble number trailer)
        # for each trigger type.
        offset = 3566
        print sep_line
        print "  BX histo lumi-nibble trailers by trigger type (offset = 0x{0:x}) -> nibble number".format(offset)
        print sep_line
        for trig_type in xrange(num_trig_types):
            reg_name = "cpmt1.ipm.l1a_histos.trigger_type{0:d}".format(trig_type)
            node = cpm.get_node(reg_name)
            address = node.getAddress()
            print "  {0:d} -> {1:d}".format(trig_type, cpm.read(address + offset))

        # Read the one-but-last entry (i.e., the lumi section number
        # trailer) for each trigger type.
        offset = 3565
        print sep_line
        print "  BX histo lumi-section trailers by trigger type (offset = 0x{0:x}) -> section number".format(offset)
        print sep_line
        for trig_type in xrange(num_trig_types):
            reg_name = "cpmt1.ipm.l1a_histos.trigger_type{0:d}".format(trig_type)
            node = cpm.get_node(reg_name)
            address = node.getAddress()
            print "  {0:d} -> {1:d}".format(trig_type, cpm.read(address + offset))

        # Read bin contents for BX 3564.
        offset = 3564
        print sep_line
        print "  BX histo content for BX 3564 (offset = 0x{0:x})".format(offset)
        print sep_line
        for trig_type in xrange(num_trig_types):
            reg_name = "cpmt1.ipm.l1a_histos.trigger_type{0:d}".format(trig_type)
            node = cpm.get_node(reg_name)
            address = node.getAddress()
            print "  {0:d} -> {1:d}".format(trig_type, cpm.read(address + offset))

        # Read bin contents for BX 3563.
        offset = 3563
        print sep_line
        print "  BX histo content for BX 3563 (offset = 0x{0:x})".format(offset)
        print sep_line
        for trig_type in xrange(num_trig_types):
            reg_name = "cpmt1.ipm.l1a_histos.trigger_type{0:d}".format(trig_type)
            node = cpm.get_node(reg_name)
            address = node.getAddress()
            print "  {0:d} -> {1:d}".format(trig_type, cpm.read(address + offset))

        # BUG BUG BUG
        # Just trying something.
        # cpm.write("cpmt1.ipm.main.resets.trigger_histos_reset", 0x0)
        # cpm.write("cpmt1.ipm.main.resets.event_counter_reset", 0x0)
        # BUG BUG BUG end

        # End of main().

    # End of class HistChecker.

###############################################################################

if __name__ == "__main__":

    desc_str = "Rough check of CPM L1A histograms."

    res = HistChecker(desc_str).run()

    print "Done"
    sys.exit(res)

###############################################################################
