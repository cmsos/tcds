#!/usr/bin/env python

###############################################################################
## Check some crucial CPM T1 frequencies.
## NOTE: For testing purposes only.
###############################################################################

import math
import sys
import time

from collections import OrderedDict

from pytcds.cpm.tcds_cpm import get_cpm_hw
from pytcds.utils.tcds_cmd_line_base import CmdLineBase
from pytcds.utils.tcds_utils_networking import resolve_network_address

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class Checker(CmdLineBase):

    def main(self):
        self.validate_target(target_type=CmdLineBase.TARGET_TYPE_HW)
        board_info = self.setup.hw_targets[self.target]
        network_address = board_info.dns_alias
        controlhub_address = board_info.controlhub
        cpm = get_cpm_hw(network_address, controlhub_address, verbose=self.verbose)
        if not cpm:
            self.error("Could not connect to {0:s}.".format(self.ip_address))

        #----------

        measurements = OrderedDict([
            ["Clk40", 0],
            ["Clk80", 1],
            ["Clk160", 2],
            ["Clk125", 3],
            ["Clk250", 4]
        ])

        max_len = max([len(i) for i in measurements.keys()])

        cpm.write("cpmt1.freq_check.ctrl.en_crap_mode", 0x0)
        max_len = max([len(i) for i in measurements.keys()])
        for (meas, channel) in measurements.iteritems():
            cpm.write("cpmt1.freq_check.ctrl.chan_sel", channel)
            is_ready = cpm.read("cpmt1.freq_check.freq.valid")
            while not is_ready:
                is_ready = cpm.read("cpmt1.freq_check.freq.valid")
                time.sleep(.1)
            count = cpm.read("cpmt1.freq_check.freq.count")
            value = count / math.pow(2., 24) * 31.25e6 * 64
            value /= 1.e6
            print "{1:{0:d}s}: {2:6.2f} MHz".format(max_len, meas, value)

        #----------

        # End of main().

    # End of class Checker.

###############################################################################

if __name__ == "__main__":

    description = "Check some crucial CPM T1 frequencies."

    res = Checker(description).run()

    print "Done"
    sys.exit(res)

###############################################################################
