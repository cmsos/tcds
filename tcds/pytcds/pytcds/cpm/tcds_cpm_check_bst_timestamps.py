#!/usr/bin/env python

###############################################################################
## Check BST timestamps from the CPM BRILDAQ packets.
##
## NOTE: Since this script uses sequential read-out of the BRILDAQ
## data, and the BRILDAQ loop is probably running in the background,
## all info shown should be taken with a grain of salt.
###############################################################################

import datetime
import sys
import time

from pytcds.utils.tcds_utils_hw_connect import get_amc13t1_hw
from pytcds.utils.tcds_cmd_line_base import CmdLineBase

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

NUM_ITERATIONS = 100
SLEEP_TIME = .4

###############################################################################

class Checker(CmdLineBase):

    def main(self):
        self.validate_target(target_type=CmdLineBase.TARGET_TYPE_HW)

        #----------

        board_info = self.setup.hw_targets[self.target]
        network_address = board_info.dns_alias
        controlhub_address = board_info.controlhub
        cpm = get_amc13t1_hw(network_address, controlhub_address, verbose=self.verbose)
        if not cpm:
            self.error("Could not connect to {0:s}.".format(network_address))

        #----------

        # Read the timestamps (which, if all is well, originate from
        # the BST data received by the CPM).
        # - The gated record will contain the timestamp corresponding
        #   to the last orbit in the nibble.
        # - The plain record will contain the timestamp corresponding
        #   to the last orbit in the _previous_ nibble.
        try:
            timestamp_prev = None
            for i in xrange(NUM_ITERATIONS):
                section_number = cpm.read("cpmt1.brildaq.gated.lumi_section_number")
                nibble_number = cpm.read("cpmt1.brildaq.gated.lumi_nibble_number")
                bst_reception_status = cpm.read("cpmt1.brildaq.plain.bst_status")

                # Ungated BRILDAQ packet.
                n_microsec = cpm.read("cpmt1.brildaq.plain.bst_timestamp.microseconds")
                n_sec = cpm.read("cpmt1.brildaq.plain.bst_timestamp.seconds")
                timestamp_plain = datetime.datetime.utcfromtimestamp(n_sec) + \
                                  datetime.timedelta(microseconds=n_microsec)

                # Gated BRILDAQ packet.
                n_microsec = cpm.read("cpmt1.brildaq.gated.bst_timestamp.microseconds")
                n_sec = cpm.read("cpmt1.brildaq.gated.bst_timestamp.seconds")
                timestamp_gated = datetime.datetime.utcfromtimestamp(n_sec) + \
                                  datetime.timedelta(microseconds=n_microsec)

                timestamp_lo = timestamp_plain
                timestamp_hi = timestamp_gated

                # Time difference.
                delta = timestamp_hi - timestamp_lo
                bst_status_str = "BAD"
                if bst_reception_status == 0x0000bea0:
                    bst_status_str = "GOOD"
                print "----------"
                print "  section {0:d}, nibble {1:d}".format(section_number, nibble_number)
                print "  BST reception status: 0x{0:08x} -> {1:s}".format(bst_reception_status,
                                                                          bst_status_str)
                print "  plain (=start time): {0:s}".format(timestamp_plain.isoformat())
                print "  gated (=end time):   {0:s}".format(timestamp_gated.isoformat())
                print "    delta = {0:s}".format(delta)

                if not timestamp_prev is None and timestamp_lo != timestamp_prev:
                    msg = "    !!! Timestamp skip, or missed nibble???? (delta = {0:s})"
                    print msg.format(timestamp_lo - timestamp_prev)

                timestamp_prev = timestamp_hi
                time.sleep(SLEEP_TIME)

                print "  press CTRL-C to stop..."

        except KeyboardInterrupt:
            pass

        #----------

        # End of main().

    # End of class Checker.

###############################################################################

if __name__ == "__main__":

    desc_str = "Little helper to check the book-keeping numbers" \
    " in the CPM BRILDAQ record.\n\n" \
    "NOTE: Since this script uses sequential read-out" \
    " of the BRILDAQ data, and the BRILDAQ loop is probably running" \
    " in the background, all info shown should be taken with a grain of salt."

    res = Checker(desc_str).run()

    print "Done"
    sys.exit(res)

###############################################################################
