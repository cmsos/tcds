#!/usr/bin/env python

###############################################################################
## Little helper to check the book-keeping numbers in the CPM BRILDAQ
## record.
##
## NOTE: Since this script uses sequential read-out of the BRILDAQ
## data, and the BRILDAQ loop is probably running in the background,
## all info shown should be taken with a grain of salt.
###############################################################################

import datetime
import sys

from pytcds.utils.tcds_constants import FIRST_BX
from pytcds.utils.tcds_constants import LAST_BX
from pytcds.utils.tcds_constants import NUM_BX_PER_ORBIT
from pytcds.utils.tcds_utils_hw_connect import get_amc13t1_hw
from pytcds.utils.tcds_cmd_line_base import CmdLineBase

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class Checker(CmdLineBase):

    def main(self):
        self.validate_target(target_type=CmdLineBase.TARGET_TYPE_HW)

        #----------

        board_info = self.setup.hw_targets[self.target]
        network_address = board_info.dns_alias
        controlhub_address = board_info.controlhub
        cpm = get_amc13t1_hw(network_address, controlhub_address, verbose=self.verbose)
        if not cpm:
            self.error("Could not connect to {0:s}.".format(network_address))

        #----------

        # Read the active BX mask.
        bx_mask = []
        for bx in xrange(FIRST_BX, LAST_BX + 1):
            reg_name = "cpmt1.ipm.bunch_mask.bx{0:d}".format(bx)
            tmp = cpm.read(reg_name)
            bx_mask.append(tmp)

        active_bxs = [i for (i, j) in enumerate(bx_mask) if (j & 0x4)]

        num_active_bxs = len(active_bxs)
        print "Found {0:d} beam-active BXs in the CPM bunch mask".format(num_active_bxs)

        #----------

        # Read the current lumi section and nibble numbers.
        section_number = cpm.read("cpmt1.brildaq.gated.lumi_section_number")
        nibble_number = cpm.read("cpmt1.brildaq.gated.lumi_nibble_number")

        msg = "Current BRILDAQ record corresponds to lumi section {0:d}, nibble {1:d}"
        print msg.format(section_number, nibble_number)

        # Read the timestamps (which, if all is well, originate from
        # the BST data received by the CPM).
        # - The gated record will contain the timestamp corresponding
        #   to the last orbit in the nibble.
        # - The plain record will contain the timestamp corresponding
        #   to the last orbit in the _previous_ nibble.
        timestamp_lo_sec = cpm.read("cpmt1.brildaq.plain.bst_timestamp.seconds")
        timestamp_lo_usec = cpm.read("cpmt1.brildaq.plain.bst_timestamp.microseconds")
        timestamp_hi_sec = cpm.read("cpmt1.brildaq.gated.bst_timestamp.seconds")
        timestamp_hi_usec = cpm.read("cpmt1.brildaq.gated.bst_timestamp.microseconds")

        tmp = timestamp_lo_sec + 1.e-6 * timestamp_lo_usec
        timestamp_lo = datetime.datetime.utcfromtimestamp(tmp)
        tmp = timestamp_hi_sec + 1.e-6 * timestamp_hi_usec
        timestamp_hi = datetime.datetime.utcfromtimestamp(tmp)
        delta = timestamp_hi - timestamp_lo

        msg = "Nibble runs from {0:s} to {1:s} --> duration = {2:s}"
        print msg.format(timestamp_lo.isoformat(), timestamp_hi.isoformat(), delta)

        #----------

        # Read the number of orbits that should be in each lumi
        # nibble.
        num_orbits_per_nibble = cpm.read("cpmt1.brildaq.gated.num_orbits")
        print "According to the CPM BRILDAQ record," \
            " there are {0:d} orbits in each nibble".format(num_orbits_per_nibble)

        num_bx_total_expected = NUM_BX_PER_ORBIT * num_orbits_per_nibble
        num_bx_beamactive_expected = num_active_bxs * num_orbits_per_nibble

        # Read the total number of BXs in the lumi nibble.
        # - Plain.
        num_bx_total = cpm.read("cpmt1.brildaq.plain.bx_count")
        print "The total BX count in the lumi nibble," \
            " according to the CPM BRILDAQ packet, is {0:d}".format(num_bx_total)
        if (num_bx_total != num_bx_total_expected):
            print "  !!! --> expected {0:d} BXs but found {1:d} !!!".format(num_bx_total_expected, num_bx_total)
        else:
            print "  Looks good"

        # - Beam-active.
        num_bx_beamactive = cpm.read("cpmt1.brildaq.gated.bx_count")
        print "The beam-active BX count in the lumi nibble," \
            " according to the CPM BRILDAQ packet, is {0:d}".format(num_bx_beamactive)
        if (num_bx_beamactive != num_bx_beamactive_expected):
            print "  !!! --> expected {0:d} BXs but found {1:d} !!!".format(num_bx_beamactive_expected, num_bx_beamactive)
        else:
            print "  Looks good"

        # Read the total number of 'dead' BXs in the lumi nibble.
        # - Plain.
        num_bx_dead_plain = cpm.read("cpmt1.brildaq.plain.deadtime_bx_count_total")
        print "The plain deadtime BX count in the lumi nibble," \
            " according to the CPM BRILDAQ packet, is {0:d}".format(num_bx_dead_plain)
        if (num_bx_dead_plain != num_bx_total_expected):
            print "  !!! --> expected {0:d} BXs but found {1:d} !!!".format(num_bx_total_expected, num_bx_dead_plain)
        else:
            print "  Looks good"

        # Read the total number of 'dead' BXs in the lumi nibble.
        # - Beam-active.
        num_bx_dead_beamactive = cpm.read("cpmt1.brildaq.gated.deadtime_bx_count_total")
        print "The beam-active deadtime BX count in the lumi nibble," \
            " according to the CPM BRILDAQ packet, is {0:d}".format(num_bx_dead_beamactive)
        if (num_bx_dead_beamactive != num_bx_beamactive_expected):
            print "  !!! --> expected {0:d} BXs but found {1:d} !!!".format(num_bx_beamactive_expected, num_bx_dead_beamactive)
        else:
            print "  Looks good"

        # End of main().

    # End of class Checker.

###############################################################################

if __name__ == "__main__":

    desc_str = "Little helper to check the book-keeping numbers" \
    " in the CPM BRILDAQ record.\n\n" \
    "NOTE: Since this script uses sequential read-out" \
    " of the BRILDAQ data, and the BRILDAQ loop is probably running" \
    " in the background, all info shown should be taken with a grain of salt."

    res = Checker(desc_str).run()

    print "Done"
    sys.exit(res)

###############################################################################
