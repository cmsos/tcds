#!/usr/bin/env python

###############################################################################
## Send an L1A from an iPM/iCI.
###############################################################################

import sys
import time

from pytcds.lpm.tcds_lpm import get_lpm_hw
from pytcds.utils.tcds_cmd_line_base import CmdLineBase
from pytcds.utils.tcds_constants import ICI_NUMBERS
from pytcds.utils.tcds_utils_networking import resolve_network_address

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class L1ASender(CmdLineBase):

    def pre_hook(self):
        self.ici_number_string = None
        self.ici_number = None
        # End of pre_hook().

    def handle_args(self):
        # Three arguments are expected. The first argument has to
        # sound like an IP address.
        if len(self.args) != 2:
            msg = "Two arguments are required: " \
                  "the IP address of the device, and " \
                  "the iCI number ({0:s})."
            self.error(msg.format(", ".join(sorted(ICI_NUMBERS.keys()))))
        else:
            # Extract the IP address.
            ip_address_arg = self.args[0]
            ip_address = resolve_network_address(ip_address_arg)
            if not ip_address:
                msg = "'{0:s}' does not sound like a valid network address."
                msg = msg.format(ip_address_arg)
                self.error(msg)
            else:
                self.ip_address = ip_address
            # Extract the iCI number.
            self.ici_number_string = self.args[1]
            try:
                self.ici_number = ICI_NUMBERS[self.ici_number_string]
            except KeyError:
                msg = "Failed to interpret '{0:s}' as a valid iCI number. " \
                      "Options are: {1:s}."
                self.error(msg.format(self.ici_number_string,
                                      ", ".join(sorted(ICI_NUMBERS.keys()))))
        # End of handle_args().

    def main(self):
        lpm = get_lpm_hw(self.ip_address, verbose=self.verbose)
        if not lpm:
            self.error("Could not connect to {0:s}.".format(self.ip_address))

        if self.verbose:
            if self.ici_number == 0:
                print "Sending L1A through the iPM."
            else:
                print "Sending L1A through iCI #{1:d}."

        #----------

        lpm.enable_ipbus_requests(self.ici_number)
        lpm.send_ipbus_l1a(self.ici_number)
        lpm.disable_ipbus_requests(self.ici_number)

        #----------

        # End of main().

    # End of class L1ASender.

###############################################################################

if __name__ == "__main__":

    description = "Send an L1A from an iPM/iCI."
    usage = "usage: %prog [options] IP ICI_NUMBER"
    epilog = "ICI_NUMBER: {0:s}.".format(", ".join(sorted(ICI_NUMBERS.keys())))
    res = L1ASender(description, usage, epilog).run()
    print "Done"
    sys.exit(res)

###############################################################################
