#!/usr/bin/env python

###############################################################################
##
###############################################################################

from tcds_lpm_common import get_lpm_hw

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

if __name__ == "__main__":

    lpm = get_lpm_hw()

    # # Switch the clock to the on-board crystal.
    # lpm.getNode("general_purpose.fmc_clock_control").write(0x4)
    # lpm.dispatch()

    for fmc_num in [1, 2]:
        val = lpm.getNode("glib.status.fmc{0:d}_presence".format(fmc_num).read())
        lpm.dispatch()
        print "FMC{0:d} present: {1:s}".format(fmc_num, val)

    tmp = lpm.getNode("ici0.main.firmware_version").read()
    lpm.dispatch()
    print "ici Firmware version: {0:s}".format(hex(tmp))

    lpm.getNode("ici0.main.counter_resets").write(0xffffffff)
    lpm.dispatch()

    ##########

    # Set up the inselect register to accept cyclic BGOs.
    lpm.getNode("ici0.main.inselect").write(0x0)
    lpm.dispatch()
    lpm.getNode("ici0.main.inselect.combined_cyclic_trigger_enable").write(0x1)
    lpm.dispatch()
    lpm.getNode("ici0.main.inselect.combined_l1a_in_0_trigger_enable").write(0x1)
    lpm.dispatch()
    lpm.getNode("ici0.main.inselect.combined_l1a_in_1_trigger_enable").write(0x1)
    lpm.dispatch()
    lpm.getNode("ici0.main.inselect.combined_trigger_generator_trigger_enable").write(0x1)
    lpm.dispatch()
    lpm.getNode("ici0.main.inselect.combined_soft_bgo_enable").write(0x1)
    lpm.dispatch()
    lpm.getNode("ici0.main.inselect.combined_cyclic_bgo_enable").write(0x1)
    lpm.dispatch()

    res = lpm.getNode("ici0.main.inselect").read()
    lpm.dispatch()
    print "INSELECT: {0:s}".format(hex(res.value()))

    ##########

    # Set up cyclic BGO generator 0 to request BGO1 (BC0) permanently
    # and repetitively on bunch crossing 16.
    lpm.getNode("ici0.cyclic_generator0.configuration.permanent").write(0x1)
    lpm.dispatch()
    lpm.getNode("ici0.cyclic_generator0.configuration.loop").write(0x1)
    lpm.dispatch()
    lpm.getNode("ici0.cyclic_generator0.configuration.repeat_cycle").write(0x1)
    lpm.dispatch()
    lpm.getNode("ici0.cyclic_generator0.configuration.start_bc").write(0x10)
    lpm.dispatch()
    # (address => X"0000009D", data =>  X"00000041", ipbus_write => '1'), -- trigger value = 1, strobe (bit 6) = 1

    # Set up bchannel 1 for BC0.
    # (address => X"00020800", data =>  X"00000025", ipbus_write => '1'), -- send 25
    # (address => X"00020801", data =>  X"00000000", ipbus_write => '1'), -- short
    # (address => X"00020802", data =>  X"00000000", ipbus_write => '1'), -- null
    # (address => X"00020803", data =>  X"00000006", ipbus_write => '1'), -- no transmit, end of sequence
    # (address => X"00000104", data =>  X"90000000", ipbus_write => '1'), -- single command, repeat sequence
    # (address => X"00000105", data =>  X"00000000", ipbus_write => '1'), -- no pre-scale
    # (address => X"00000106", data =>  X"00000000", ipbus_write => '1'), -- no post-scale

# By the way, the ports :

#    SelectedSystemOrb :    in std_logic := '0';
#    SelectedSystemBCID :        in std_logic_vector(11 downto 0) := (others => '0');
#    SelectedSystemOrbID :in std_logic_vector(31 downto 0) := (others => '0');

# need to be driven for this to work :-)

# The test bench hoha for this is

#    orbit_stuff: process(clk40)
#      begin
#       if rising_edge(clk40) then
#        if reset= '1' then
#            SelectedSystemBCID <= 0;
#            SelectedSystemOrb  <= '0';
#            SelectedSystemOrbID <= 0;
#        elsif SelectedSystemBCID = to 3564 then
#            SelectedSystemBCID <= 1;
#            SelectedSystemOrb  <= '1';
#            SelectedSystemOrbID  <= SelectedSystemOrbID  + 1;
#        else
#            SelectedSystemBCID <= SelectedSystemBCID  + 1;
#         SelectedSystemOrb  <= '0';
#            SelectedSystemOrbID  <= SelectedSystemOrbID;
#       end if;
#      end process fourty_mhz_clock;

    # BUG BUG BUG
    lpm.getNode("ici0.main.counter_resets").write(0xffffffff)
    lpm.dispatch()
    # BUG BUG BUG end

    for reg_name in ["orbit_counter", "event_counter", "bgo_counter"]:
        tmp = lpm.getNode("ici0.main.{0:s}".format(reg_name).read())
        lpm.dispatch()
        print "{0:s} = {1:d}".format(reg_name, val)

    print "Done"

###############################################################################
