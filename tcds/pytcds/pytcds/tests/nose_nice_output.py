###############################################################################
## A Python nose plugin to produce output in a slightly tuned
## format. Strongly inspired by the NiceDots plugin.
###############################################################################

import nose
import os

###############################################################################

def nice_address(test):
    """Returns a tuned version of the test address."""

    res = None
    if isinstance(test, nose.suite.ContextSuite):
        addr = nose.util.test_address(test.context)
        if hasattr(test, "error_context") and test.error_context:
            addr = list(addr)
            if addr[2]:
                # The test is a class.
                addr[2] = "{0:s}.{1:s}".format(addr[2], test.error_context)
            else:
                # The test is a module.
                addr[2] = test.error_context
    else:
        addr = nose.util.test_address(test)
    if addr is None:
        res = "?"
    (path, module, test_path) = addr
    path = nice_path(path)
    if test_path is None:
        res = path
    else:
        res = "{0:s}:{1:s}".format(path, test_path)
    # End of nice_address().
    return res

###############################################################################

def nice_path(path):
    """Returns a tuned version of the test (context) path."""

    if not path is None:
        path = os.path.abspath(path)
        try:
            cwd = os.getcwd()
        except OSError:
            pass
        else:
            if path.startswith(cwd):
                path = path.replace(cwd, "")[1:]
        if path.endswith(".pyc"):
            path = path[0:-1]
    # End of nice_path().
    return path

###############################################################################

class NiceOutput(nose.plugins.Plugin):

    name = "nice-output"

    def configure(self, options, conf):
        super(NiceOutput, self).configure(options, conf)
        if not self.enabled:
            return
        self.cmd_options = options
        self.config = conf
        # End of configure().

    def prepareTestResult(self, result):
        nice_result = NiceOutputResult(self.runner.stream,
                                       self.runner.descriptions,
                                       self.runner.verbosity)

        # Monkey-patch the unittest result with our own version.
        for fn in nice_result.__class__.__dict__:
            setattr(result, fn, getattr(nice_result, fn))

        # The following are required for the summary to work.
        for a in ["failures", "errors", "skipped", "errorClasses"]:
            setattr(nice_result, a, getattr(result, a))

        # Return the patched result so other plugins keep their
        # fingers off.
        # End of prepareTestResult().
        return nice_result

    def prepareTestRunner(self, runner):
        self.runner = runner
        # End of prepareTestRunner().

    # End of class NiceOutput.

###############################################################################

class NiceOutputResult(nose.result.TextTestResult):

    def __init__(self, stream, descriptions, verbosity, config=None, errorClasses=None):
        super(NiceOutputResult, self).__init__(stream, descriptions, verbosity, config, errorClasses)
        # End of __init__().

    def getDescription(self, test):
        name = nice_address(test)
        args = test.test._descriptors()[1]
        if args:
            # NOTE: For some reason the args are always a tuple, even
            # with a single argument. Fix this up a bit for the
            # output.
            res = "{0:s}({1:s})".format(name, ", ".join([str(i) for i in args]))
        else:
            res = name
        return res

    def addError(self, test, err):
        super(NiceOutputResult, self).addError(test, err)
        (exc, val, tb) = err
        if issubclass(exc, nose.SkipTest):
            self.printDetail("SKIP", err, test)
        else:
            self.printDetail("ERROR", err, test)
        self.stream.flush()
        # End of addError().

    def addFailure(self, test, err):
        super(NiceOutputResult, self).addFailure(test, err)
        self.printDetail("FAIL", err, test)
        self.stream.flush()
        # End of addFailure().

    def printDetail(self, flavour, err, test):
        detail = None
        if flavour.lower() == "error":
            detail = super(NiceOutputResult, self)._exc_info_to_string(err, test)
        else:
            detail = str(err[1])
        detail = detail.strip()
        if self.dots:
            self.stream.writeln()
        self.stream.writeln(self.separator1)
        self.stream.writeln("{0:s}: {1:s}".format(flavour, self.getDescription(test)))
        self.stream.writeln(self.separator2)
        self.stream.writeln(detail)
        self.stream.writeln(self.separator1)
        # End of printDetail().

    def printErrors(self):
        # No need to re-print the errors and failures again at the
        # end.
        pass

    def printSummary(self, start, stop):
        if self.dots:
            self.stream.writeln()
        super(NiceOutputResult, self).printSummary(start, stop)

    # End of class NiceOutputResult.

###############################################################################
