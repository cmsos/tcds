###############################################################################
## Slightly modified version of the patch found here:
##   https://code.google.com/p/python-nose/issues/detail?id=26
###############################################################################

from nose.plugins.manager import BuiltinPluginManager
from nose.plugins.manager import EntryPointPluginManager

###############################################################################

class PythonPathPluginManager(BuiltinPluginManager, EntryPointPluginManager):
    """Nose plugin manager that loads specified plugins from sys.path.

    Nose plugin manager that loads specified plugins from sys.path, as
    well as any from the BuiltinPluginManager and
    EntryPointPluginManager.

    """

    def __init__(self, plugin_modules, plugins=None):
        # NOTE: Here one actually really needs super().
        self._plugin_modules = list(plugin_modules)
        super(PythonPathPluginManager, self).__init__()
        # End of __init__().

    def loadPlugins(self):
        # NOTE: Here one actually really needs super().
        super(PythonPathPluginManager, self).loadPlugins()
        for module_name, plugin_name in self._plugin_modules:
            plug = getattr(__import__(module_name), plugin_name)()
            self.addPlugin(plug)
        # End of loadPlugins().

    # End of class PythonPathPluginManager.

###############################################################################
