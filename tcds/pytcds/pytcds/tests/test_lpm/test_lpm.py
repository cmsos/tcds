###############################################################################

import nose
import random
import time

from nose.tools import assert_equal
from nose.tools import assert_false
from nose.tools import assert_not_equal
from nose.tools import assert_true

from pytcds.lpm.tcds_lpm import get_lpm_hw
from pytcds.tests import attr
from pytcds.tests import settings
from pytcds.tests.test_glib.test_glib import TestGLIB
from pytcds.ttcspy.tcds_ttcspy import get_ttcspy_hw
from pytcds.utils.tcds_constants import BOARD_TYPE_LPM
from pytcds.utils.tcds_constants import NUM_BX_PER_ORBIT
from pytcds.utils.tcds_constants import NUM_DATA_BITS_IN_BCOMMAND

###############################################################################

# TODO TODO TODO
# Should get rid of this at some point.
# The FMC to be used for testing. With the GLIB we can only use FMC 1.
FMC_NUM = 1
# TODO TODO TODO end

# The number of seconds to take data for tests that should take data.
ACQ_TIME = .5

# The number of iterations to run for tests that influence only counters.
NUM_ITERATIONS = 10

###############################################################################

def setup():
    TestLPM().check_setup()
    # End of setup().

###############################################################################

class TestLPM(TestGLIB):

    def __init__(self):
        TestGLIB.__init__(self)
        self.board_type = BOARD_TYPE_LPM

        self.address = settings.lpm.address
        self.port = settings.lpm.port
        self.ttcspy_address = settings.ttcspy.address
        self.ttcspy_port = settings.ttcspy.port

        self.hw = None
        self.ttcspy = None

        self.ici_connected_to_ttcspy = settings.tests.ici_connected_to_ttcspy
        self.icis_to_test = [self.ici_connected_to_ttcspy]
        if not settings.tests.quick_test:
            num_icis = settings.lpm.num_icis
            self.icis_to_test = range(1, num_icis + 1)

        num_cyclic_gens_ici = settings.ici.num_cyclic_generators
        self.cyclic_gens_to_test_ici = None
        if settings.tests.quick_test:
            self.cyclic_gens_to_test_ici = [random.randint(0, num_cyclic_gens_ici - 1)]
        else:
            self.cyclic_gens_to_test_ici = range(num_cyclic_gens_ici)

        num_cyclic_gens_ipm = settings.ipm.num_cyclic_generators
        self.cyclic_gens_to_test_ipm = None
        if settings.tests.quick_test:
            self.cyclic_gens_to_test_ipm = [random.randint(0, num_cyclic_gens_ipm - 1)]
        else:
            self.cyclic_gens_to_test_ipm = range(num_cyclic_gens_ipm)

        self.bxs_to_test = settings.tests.bxs_to_test
        # End of __init__().

    def setup(self):
        TestGLIB.setup(self)

        # Connect to the LPM.
        try:
            self.hw = get_lpm_hw(self.address,
                                 self.controlhub_address)
        except Exception, err:
            controlhub_str = ""
            if self.controlhub_address:
                controlhub_str = " (using controlhub at IP address {0:s})".format(self.controlhub_address)
            msg = "Could not connect to the LPM at IP address {0:s}{1:s}."
            msg = msg.format(self.address, controlhub_str)
            raise nose.SkipTest(msg)

        # Start with a clean slate.
        self.hw.reset()

        # Select the clock from the AMC13/CPM on the backplane as
        # clock source.
        self.hw.configure_xpoint_switch("fclka")

        # Now the clock should be okay.
        if not self.hw.is_ttc_clock_up():
            msg = "No backplane clock detected."
            raise nose.SkipTest(msg)

        # Connect to the TTCSpy.
        try:
            self.ttcspy = get_ttcspy_hw(self.ttcspy_address,
                                        self.controlhub_address)
        except Exception, err:
            controlhub_str = ""
            if self.controlhub_address:
                controlhub_str = " (using controlhub at IP address {0:s})".format(self.controlhub_address)
            msg = "Could not connect to the TTCSpy at IP address {0:s}{1:s}."
            msg = msg.format(self.ttcspy_address, controlhub_str)
            raise nose.SkipTest(msg)
        # End of setup().

    def check_setup(self):
        TestGLIB.check_setup(self)

        # Check if the address points to the right type of board.
        try:
            self.verify_board_identity()
        except RuntimeError, err:
            raise nose.SkipTest(err)

        self.setup()

        # Check the connection to the TTCSpy.
        # Step 1: switch off the SFP and verify that there is no signal.
        self.hw.switch_sfp(self.ici_connected_to_ttcspy, False)
        time.sleep(1)
        if self.ttcspy.is_cdr_ok():
            msg = "SFP corresponding to iCI {0:d} is off " \
                  "but there is still signal on the TTCSpy. " \
                  "Is the 'ici_connected_to_ttcspy' setting correct?"
            msg = msg.format(self.ici_connected_to_ttcspy)
            raise nose.SkipTest(msg)
        # Step 2: switch the SFP on and verify that now there is signal.
        self.hw.switch_sfp(self.ici_connected_to_ttcspy, True)
        time.sleep(1)
        if not self.ttcspy.is_cdr_ok():
            msg = "SFP corresponding to iCI {0:d} is on " \
                  "but there is no signal on the TTCSpy. " \
                  "Is the 'ici_connected_to_ttcspy' setting correct?"
            msg = msg.format(self.ici_connected_to_ttcspy)
            raise nose.SkipTest(msg)

        # End of check_setup().

    #----------
    # Tests of the test settings. Make sure nothing is skipped.
    #----------

    def test_settings_num_icis(self):
        num_icis_in_hw = self.hw.get_num_icis()
        num_icis_in_settings = settings.lpm.num_icis
        msg = "The hardware LPM contains {0:d} iCIs " \
              "but the test settings say {1:d}."
        msg = msg.format(num_icis_in_hw, num_icis_in_settings)
        assert_equal(num_icis_in_hw, num_icis_in_settings, msg)
        # End of test_settings_num_icis().

    def test_settings_num_cyclic_gens_in_ipm(self):
        num_cyclic_gens_in_hw = self.hw.get_num_cyclic_gens_in_ipm()
        num_cyclic_gens_in_settings = settings.ipm.num_cyclic_generators
        msg = "The hardware iPM contains {0:d} cyclis generators " \
              "but the test settings say {1:d}."
        msg = msg.format(num_cyclic_gens_in_hw, num_cyclic_gens_in_settings)
        assert_equal(num_cyclic_gens_in_hw, num_cyclic_gens_in_settings, msg)
        # End of test_settings_num_cyclic_gens_in_ipm().

    def _test_settings_num_cyclic_gens_in_ici(self, ici_num):
        num_cyclic_gens_in_hw = self.hw.get_num_cyclic_gens_in_ici(ici_num)
        num_cyclic_gens_in_settings = settings.ici.num_cyclic_generators
        msg = "The hardware ici contains {0:d} cyclis generators " \
              "but the test settings say {1:d}."
        msg = msg.format(num_cyclic_gens_in_hw, num_cyclic_gens_in_settings)
        assert_equal(num_cyclic_gens_in_hw, num_cyclic_gens_in_settings, msg)
        # End of _test_settings_num_cyclic_gens_in_ici().

    def test_settings_num_cyclic_gens_in_ici(self):
        for ici_num in self.icis_to_test:
            yield (self._test_settings_num_cyclic_gens_in_ici, ici_num)
        # End of test_settings_num_cyclic_gens_in_ici().

    def _test_settings_num_bchannels(self, ici_num):
        num_bchannels_in_hw = self.hw.get_num_bchannels(ici_num)
        num_bchannels_in_settings = settings.ici.num_bchannels
        msg = "The hardware iCI contains {0:d} B-channels " \
              "but the test settings say {1:d}."
        msg = msg.format(num_bchannels_in_hw, num_bchannels_in_settings)
        assert_equal(num_bchannels_in_hw, num_bchannels_in_settings, msg)
        # End of test_settings_num_bchannels().

    def test_settings_num_bchannels(self):
        for ici_num in self.icis_to_test:
            yield (self._test_settings_num_bchannels, ici_num)
        # End of test_settings_num_bchannels().

    #----------
    # Tests of the ipbus interface.
    #----------

    def test_ipbus_read_write_user_ici(self):
        values = [0, 42]
        for ici_num in self.icis_to_test:
            reg_name = "ici{0:d}.main.inselect".format(ici_num)
            for val in values:
                yield (self._test_ipbus_read_write, reg_name, val)
        # End of test_ipbus_read_write_user_ici().

    def test_ipbus_read_write_user_ipm(self):
        values = [0, 42]
        reg_name = "ipm.main.random_trigger_config"
        for val in values:
            yield (self._test_ipbus_read_write, reg_name, val)
        # End of test_ipbus_read_write_user_ipm().

    #----------
    # Tests of some default values coded into the firmware. Since we
    # don't intend to touch these values they'd better be correct.
    #----------

    def test_fw_default_values_trigger_rules(self):
        # TODO TODO TODO
        # This checks the iPM implementation. Should check each of the iCIs as well.
        # TODO TODO TODO end
        # Source: CMS NOTE 2002/033.
        # 1) No more than 1 L1As per 75 ns (minimum 2 BX between L1As,
        #    required by tracker and preshower).
        # 2) No more than 2 L1As per 625 ns (25 BX).
        # 3) No more than 3 L1As per 2.5 us (100 BX).
        # 4) No more than 4 L1As per 6 us (240 BX).
        word_map = {1 : "one",
                    2 : "two",
                    3 : "three",
                    4 : "four",
                    5 : "five",
                    6 : "six",
                    7 : "seven",
                    8 : "eight",
                    9 : "nine"}
        # NOTE: This depends on the firmware implementation. Have to
        # add one back to get the correct value for all non-zero
        # (i.e., switched-on) trigger rules.
        expected_values = {1 : 2,
                           2 : 24,
                           3 : 99,
                           4 : 239,
                           5 : 0,
                           6 : 0,
                           7 : 0,
                           8 : 0,
                           9 : 0}
        for i in xrange(1, 9):
            reg_name = "ipm.trigger_rules.{0:s}_trigger_deadtime".format(word_map[i])
            value_in_fw = self.hw.read(reg_name)
            value_expected = expected_values[i]
            msg = "Checking default setting for trigger rule #{0:d}. " \
                  "Expected {1:d} but found {2:d}."
            msg = msg.format(i, value_expected, value_in_fw)
            assert_equal(value_in_fw, value_expected, msg)
        # End of test_fw_default_value_trigger_rules().

    def test_fw_default_values_bgo_map(self):
        raise nose.SkipTest("Not yet implemented")
        # End of test_fw_default_value_trigger_rules().

    #----------
    # Tests of iPM functionality.
    #----------

    def test_ipm_orbit_generator(self):
        # The internal orbit counter of the iPM should always count
        # when there is an input clock.
        reg_name = "ipm.main.orbit_counter"
        tmp0 = self.hw.read(reg_name)
        time.sleep(ACQ_TIME)
        tmp1 = self.hw.read(reg_name)
        if (tmp1 == tmp0):
            msg = "The iPM internal orbit generator does not seem to work " \
                  "(or there is no input clock)."
            raise nose.SkipTest(msg)
        # End of test_ipm_orbit_counter().

    def _test_ipm_cyclic_generator_l1a(self, cyclic_gen_num, bx):
        ici_num = self.ici_connected_to_ttcspy
        reg_name = "ici{0:d}.main.inselect.exclusive_lpm_ipm1_trigger_enable".format(ici_num)
        self.hw.write(reg_name, 0x1)
        self._test_cyclic_generator("ipm", cyclic_gen_num, bx, True, None)
        # End of _test_ipm_cyclic_generator_l1a().

    def test_ipm_cyclic_generator_l1a(self):
        for cyclic_num in self.cyclic_gens_to_test_ipm:
            for bx in self.bxs_to_test:
                yield (self._test_ipm_cyclic_generator_l1a, cyclic_num, bx)
        # End of test_ipm_cyclic_generator_l1a().

    def _test_ipm_cyclic_generator_bgo(self, cyclic_gen_num, bx):
        ici_num = self.ici_connected_to_ttcspy
        bgo_num = 1
        reg_name = "ipm.bgo_channels.bgo_channel{0:d}.firing_bc".format(bgo_num)
        self.hw.write(reg_name, 0x1)
        reg_name = "ici{0:d}.main.inselect.exclusive_lpm_ipm1_bgo_enable".format(ici_num)
        self.hw.write(reg_name, 0x1)
        reg_name = "ici{0:d}.bchannels.bchannel{1:d}.configuration.repeat_cycle".format(ici_num, bgo_num)
        self.hw.write(reg_name, 0x1)
        reg_name = "ici{0:d}.bchannels.bchannel{1:d}.configuration.single".format(ici_num, bgo_num)
        self.hw.write(reg_name, 0x1)
        reg_name = "ici{0:d}.bchannels.bchannel{1:d}.configuration.bx_sync".format(ici_num, bgo_num)
        self.hw.write(reg_name, 0x1)
        reg_name = "ici{0:d}.bchannels.bchannel{1:d}.configuration.bx".format(ici_num, bgo_num)
        self.hw.write(reg_name, 0x100)
        self._test_cyclic_generator("ipm", cyclic_gen_num, bx, False, bgo_num)
        # End of _test_ipm_cyclic_generator_bgo().

    def test_ipm_cyclic_generator_bgo(self):
        for cyclic_num in self.cyclic_gens_to_test_ipm:
            for bx in self.bxs_to_test:
                yield (self._test_ipm_cyclic_generator_bgo, cyclic_num, bx)
        # End of test_ipm_cyclic_generator_bgo().

    def test_ipm_cyclic_generator_prescaling(self):
        # Setup a first cyclic generator to send unprescaled B-gos.
        # TODO TODO TODO
        # Setup a second cyclic generator to send prescaled B-gos.
        # TODO TODO TODO
        # Check that we receive 'prescale times as many' of the first
        # B-go compared to the second one.
        # TODO TODO TODO
        raise nose.SkipTest("Not yet implemented")
        # End of test_ipm_cyclic_generator_prescaling().

    #----------
    # Tests of iCI functionality.
    #----------

    def _test_ici_internal_orbit_generator(self, ici_num):
        reg_name = "ici{0:d}.main.orbit_counter".format(ici_num)
        tmp0 = self.hw.read(reg_name)
        time.sleep(ACQ_TIME)
        tmp1 = self.hw.read(reg_name)
        msg = "The iCI{0:d} internal orbit generator does not seem to work.".format(ici_num)
        assert_not_equal(tmp0, tmp1, msg)
        # End of _test_ici_internal_orbit_generator().

    def test_ici_internal_orbit_generator(self):
        for ici_num in self.icis_to_test:
            yield (self._test_ici_internal_orbit_generator, ici_num)
        # End of test_ici_internal_orbit_generator().

    def test_ici_ipbus_l1a(self):
        num_l1as_to_send = NUM_ITERATIONS
        ici_num = self.ici_connected_to_ttcspy
        reg_name = "ici{0:d}.main.inselect.combined_software_trigger_enable".format(ici_num)
        self.hw.write(reg_name, 1)
        self.ttcspy.stop_logging()
        self.ttcspy.reset_logging()
        self.ttcspy.clear_filter_mask()
        self.ttcspy.set_trigger_mask(l1accept=True)
        self.ttcspy.select_source("frontpanel")
        self.ttcspy.start_logging()
        event_counter_pre = self.hw.get_event_count(ici_num)
        for i in xrange(num_l1as_to_send):
            self.hw.send_ipbus_l1a(ici_num)
        # Verify that the TTCSpy received the correct number of L1A.
        num_l1as_received = self.ttcspy.get_num_entries()
        msg = "Supposedly sent {0:d} L1As, but received {1:d}."
        msg = msg.format(num_l1as_to_send, num_l1as_received)
        assert_equal(num_l1as_received, num_l1as_to_send, msg)
        # Verify that our counter increased by the correct amount.
        event_counter_post = self.hw.get_event_count(ici_num)
        num_l1as_counted = event_counter_post - event_counter_pre
        msg = "Supposedly sent {0:d} L1As, but the event counter increased by {1:d}."
        msg = msg.format(num_l1as_to_send, num_l1as_counted)
        assert_equal(num_l1as_counted, num_l1as_to_send, msg)
        # End of test_ici_ipbus_l1a().

    # TODO TODO TODO
    # Should really implement a full verification of the individual
    # B-go counters. And for all B-gos.
    # TODO TODO TODO end
    def test_ici_ipbus_bgo(self):
        num_bgos_to_send = NUM_ITERATIONS
        ici_num = self.ici_connected_to_ttcspy
        reg_name = "ici{0:d}.main.inselect.combined_software_bgo_enable".format(ici_num)
        self.hw.write(reg_name, 1)
        self.ttcspy.stop_logging()
        self.ttcspy.reset_logging()
        self.ttcspy.clear_filter_mask()
        self.ttcspy.set_trigger_mask(brc_all=True, adr_all=True)
        self.ttcspy.select_source("frontpanel")
        self.ttcspy.start_logging()
        bgo_counter_pre = self.hw.get_bgo_count(ici_num)
        for i in xrange(num_bgos_to_send):
            self.hw.send_ipbus_bgo(ici_num)
        # Verify that the TTCSpy received the correct number of B-gos.
        num_bgos_received = self.ttcspy.get_num_entries()
        msg = "Supposedly sent {0:d} B-gos, but received {1:d}."
        msg = msg.format(num_bgos_to_send, num_bgos_received)
        assert_equal(num_bgos_received, num_bgos_to_send, msg)
        # Verify that our counter increased by the correct amount.
        bgo_counter_post = self.hw.get_bgo_count(ici_num)
        num_bgos_counted = bgo_counter_post - bgo_counter_pre
        msg = "Supposedly sent {0:d} BGOs, but the B-go counter increased by {1:d}."
        msg = msg.format(num_bgos_to_send, num_bgos_counted)
        assert_equal(num_bgos_counted, num_bgos_to_send, msg)
        # End of test_ici_ipbus_bgo().

    def _test_ici_ipbus_bcommand(self, bcommand_data, is_long):
        num_bcommands_to_send = NUM_ITERATIONS
        ici_num = self.ici_connected_to_ttcspy
        self.ttcspy.stop_logging()
        self.ttcspy.reset_logging()
        self.ttcspy.clear_filter_mask()
        self.ttcspy.set_trigger_mask(brc_all=True, adr_all=True)
        self.ttcspy.select_source("frontpanel")
        self.ttcspy.start_logging()
        tmp = self.ttcspy.get_num_entries()
        for i in xrange(num_bcommands_to_send):
            self.hw.send_ipbus_bcommand(ici_num, bcommand_data, is_long)
        num_bcommands_received = self.ttcspy.get_num_entries()
        msg = "Supposedly sent {0:d} B-commands, but received {1:d}."
        msg = msg.format(num_bcommands_to_send, num_bcommands_received)
        # Verify that the correct number of B-commands was received.
        assert_equal(num_bcommands_received, num_bcommands_to_send, msg)
        # Verify that the correct B-command data was received.
        spy_data = self.ttcspy.get_entries()
        for entry in spy_data:
            if is_long:
                msg = "Supposedly sent a long B-command but found something else."
                assert_true(entry.is_bcommand_long(), msg)
            else:
                msg = "Supposedly sent a short B-command but found something else."
                assert_true(entry.is_bcommand_short(), msg)
            bcommand_data_received = entry.get_bcommand_data()
            msg = "Supposedly sent B-command data 0x{0:x} but received 0x{1:x}."
            msg = msg.format(bcommand_data, bcommand_data_received)
            assert_equal(bcommand_data_received, bcommand_data, msg)
        # End of _test_ici_ipbus_bcommand().

    def _test_ici_ipbus_bcommand_short(self, bcommand_data):
        self._test_ici_ipbus_bcommand(bcommand_data, False)

    def _test_ici_ipbus_bcommand_long(self, bcommand_data):
        self._test_ici_ipbus_bcommand(bcommand_data, True)

    def test_ici_ipbus_bcommand_shorts(self):
        bcommand_values = None
        max_val = 2**NUM_DATA_BITS_IN_BCOMMAND
        if not settings.tests.quick_test:
            bcommand_values = range(max_val)
        else:
            bcommand_values = [random.randint(0, max_val)]
        for bcommand_data in bcommand_values:
            yield (self._test_ici_ipbus_bcommand_short, bcommand_data)
        # End of test_ici_ipbus_bcommand_shorts().

    def test_ici_ipbus_bcommand_longs(self):
        bcommand_values = None
        max_val = 2**NUM_DATA_BITS_IN_BCOMMAND
        if not settings.tests.quick_test:
            bcommand_values = range(max_val)
        else:
            bcommand_values = [random.randint(0, max_val)]
        for bcommand_data in bcommand_values:
            yield (self._test_ici_ipbus_bcommand_long, bcommand_data)
        # End of test_ici_ipbus_bcommand_longs().

    def _test_cyclic_generator(self, module_name, cyclic_gen_num, bx, is_l1a, bgo_or_train_id):
        reg_name_base = "{0:s}.cyclic_generator{1:d}.configuration".format(module_name, cyclic_gen_num)
        if is_l1a:
            reg_name = "{0:s}.trigger_word.l1a_command".format(reg_name_base)
            self.hw.write(reg_name, 0x1)
        else:
            reg_name = "{0:s}.trigger_word.bgo_command".format(reg_name_base)
            self.hw.write(reg_name, 0x1)
            reg_name = "{0:s}.trigger_word.id".format(reg_name_base)
            self.hw.write(reg_name, bgo_or_train_id)
        reg_name = "{0:s}.permanent".format(reg_name_base)
        self.hw.write(reg_name, 0x1)
        reg_name = "{0:s}.enabled".format(reg_name_base)
        self.hw.write(reg_name, 0x1)
        reg_name = "{0:s}.start_bx".format(reg_name_base)
        self.hw.write(reg_name, bx)

        # Acquire the output with the TTCSpy.
        self.ttcspy.clear_filter_mask()
        self.ttcspy.set_trigger_mask(l1accept=True, brc_all=True, adr_all=True)
        self.ttcspy.select_source("frontpanel")
        spy_data = self.ttcspy.log_for_n_seconds(ACQ_TIME)

        # By this time the TTCSpy memory should be full.
        ttcspy_memory_empty = self.ttcspy.is_memory_empty()
        ttcspy_memory_full = self.ttcspy.is_memory_full()
        msg = "The {0:s} cyclic generator did not produce anything in {1:.1f} seconds."
        msg = msg.format(module_name, ACQ_TIME)
        assert_false(ttcspy_memory_empty, msg)
        msg = "The {0:s} cyclic generator only produced {1:d} entries in {2:.1f} seconds."
        msg = msg.format(module_name, self.ttcspy.get_num_entries(), ACQ_TIME)
        assert_true(ttcspy_memory_full, msg)

        # Verify that we received the right kind of 'thing.'
        if is_l1a:
            tmp = list(set([i.is_l1a() for i in spy_data]))
        else:
            tmp = list(set([i.is_bcommand() for i in spy_data]))
        type_str = "L1As"
        if not is_l1a:
            type_str = "B-gos"
        msg = "Supposedly the cyclic generator sent only {0:s} " \
              "but the TTCSpy also received something else."
        msg = msg.format(type_str)
        assert_equal(tmp, [True], msg)

        # Verify that all entries were spaced by a single orbit.
        tmp = list(set([(j.get_timestamp() - i.get_timestamp()) \
                        for (i, j) in zip(spy_data[:-1], spy_data[1:])]))
        tmp.sort()
        msg = "Expected all cyclic generator products to be separated " \
              "by a single orbit (i.e., {0:d} BX), but found separation(s) of {1:s} BX."
        msg = msg.format(NUM_BX_PER_ORBIT, ", ".join([str(i) for i in tmp]))
        assert_equal(tmp, [NUM_BX_PER_ORBIT], msg)
        # End of _test_cyclic_generator().

    def _test_ici_cyclic_generator_l1a(self, cyclic_gen_num, bx):
        ici_num = self.ici_connected_to_ttcspy
        reg_name = "ici{0:d}.main.inselect.combined_cyclic_trigger_enable".format(ici_num)
        self.hw.write(reg_name, 0x1)
        self._test_cyclic_generator("ici{0:d}".format(ici_num), cyclic_gen_num, bx, True, None)
        # End of _test_ici_cyclic_generator_l1a().

    def test_ici_cyclic_generator_l1a(self):
        for cyclic_num in self.cyclic_gens_to_test_ici:
            for bx in self.bxs_to_test:
                yield (self._test_ici_cyclic_generator_l1a, cyclic_num, bx)
        # End of test_ici_cyclic_generator_l1a().

    def _test_ici_cyclic_generator_bgo(self, cyclic_gen_num, bx):
        print "DEBUG JGH 0"
        ici_num = self.ici_connected_to_ttcspy
        bgo_num = 1
        reg_name = "ici{0:d}.main.inselect.combined_cyclic_bgo_enable".format(ici_num)
        self.hw.write(reg_name, 0x1)
        reg_name = "ici{0:d}.bchannels.bchannel{1:d}.configuration.repeat_cycle".format(ici_num, bgo_num)
        self.hw.write(reg_name, 0x1)
        reg_name = "ici{0:d}.bchannels.bchannel{1:d}.configuration.single".format(ici_num, bgo_num)
        self.hw.write(reg_name, 0x1)
        self._test_cyclic_generator("ici{0:d}".format(ici_num), cyclic_gen_num, bx, False, bgo_num)
        print "DEBUG JGH 1"
        # End of _test_ici_cyclic_generator_bgo().

    @attr("dev")
    def test_ici_cyclic_generator_bgo(self):
        for cyclic_num in self.cyclic_gens_to_test_ici:
            for bx in self.bxs_to_test:
                yield (self._test_ici_cyclic_generator_bgo, cyclic_num, bx)
        # End of test_ici_cyclic_generator_bgo().

    # End of class TestLPM.

###############################################################################
