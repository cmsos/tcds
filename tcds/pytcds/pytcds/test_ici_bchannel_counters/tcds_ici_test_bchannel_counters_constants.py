###############################################################################

# The addresses of the boards/applications to connect to.
CPM = "cpm"
LPM = "lpm4"
ICIS = ["ici41", "ici61"]
APVES = ["apve41", "apve61"]
PIS = ["pi41", "pi61"]

###############################################################################
