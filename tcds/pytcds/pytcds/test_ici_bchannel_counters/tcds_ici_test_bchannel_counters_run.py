#!/usr/bin/env python

###############################################################################
## Master script to study B-channel counters.
###############################################################################

import sys

from pytcds.test_ici_bchannel_counters.tcds_test_ici_bchannel_counters_constants import APVES
from pytcds.test_ici_bchannel_counters.tcds_test_ici_bchannel_counters_constants import CPM
from pytcds.test_ici_bchannel_counters.tcds_test_ici_bchannel_counters_constants import ICIS
from pytcds.test_ici_bchannel_counters.tcds_test_ici_bchannel_counters_constants import LPM
from pytcds.test_ici_bchannel_counters.tcds_test_ici_bchannel_counters_constants import PIS
from pytcds.utils.tcds_cmd_line_base import CmdLineBase
from pytcds.utils.tcds_command_runner import add_cwd_to_cmds
from pytcds.utils.tcds_command_runner import CommandRunner

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

def append_piece_to_cmds(cmds, piece):
    res = []
    for (cmd, doc) in cmds:
        full_cmd = cmd
        if cmd.endswith(".py"):
            full_cmd = "%s %s" % (cmd, piece)
        res.append((full_cmd, doc))
    # End of append_piece_to_cmds().
    return res

###############################################################################

class Tester(CmdLineBase):

    MODE_CHOICES = ["cpm", "lpm", "ici"]

    def pre_hook(self):
        self.mode = None
        # End of pre_hook().

    def handle_args(self):
        # NOTE: This is a slight abuse of the CmdLineBase base class
        # since we don't use an IP address argument. Don't tell
        # anyone.

        # One arguments is expected: the choice of mode (cpm, lpm, or ici).
        if len(self.args) != 1:
            msg = "One arguments is required: " \
                  "the choice of Partition Manager mode. " \
                  "Options are: {0:s}."
            self.error(msg.format(", ".join(Tester.MODE_CHOICES)))
        else:
            # Extract the mode.
            mode = self.args[0]
            if not mode in Tester.MODE_CHOICES:
                msg = "'{0:s}' is not a valid mode. Options are: {1:s}."
                msg = msg.format(setup_type, ", ".join(Tester.MODE_CHOICES))
                self.parser.error(msg)
            self.mode = mode

        # End of handle_args().

    def main(self):
        pm = None
        if self.mode == "lpm":
            pm = LPM
        elif self.mode == "cpm":
            pm = CPM

        sep_line = "-" * 70

        fsm_ctrl_cmd = "../utils/tcds_fsm_control.py --cfg-file ../utils/tcds_setup_tcdslab.cfg"
        fsm_poll_cmd = "../utils/tcds_fsm_poller.py --cfg-file ../utils/tcds_setup_tcdslab.cfg"
        pi_force_ready_cmd = "./tcds_pi_force_ready.py"

        cmds = []

        #----------

        # Halt everything.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Halting all controllers'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = []
        if pm:
            targets = [pm]
        targets.extend(ICIS)
        targets.extend(APVES)
        targets.extend(PIS)
        for target in targets:
            cmds.append(("{0:s} {1:s} Halt".format(fsm_ctrl_cmd, target), ""))

        # Wait till everything is in the Halted state.
        targets = []
        if pm:
            targets = [pm]
        targets.extend(ICIS)
        targets.extend(APVES)
        targets.extend(PIS)
        for target in targets:
            cmds.append(("{0:s} {1:s} --wait-until Halted".format(fsm_poll_cmd, target), ""))

        #----------

        # Configure PM.
        if pm:
            cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                         ("python -c \"print 'Configuring {0:s}'\"".format(self.mode.upper()), ""),
                         ("python -c \"print '{0:s}'\"".format(sep_line), "")])
            cmds.append(("{0:s} {1:s} Configure hw_cfg_{2:s}.txt".format(fsm_ctrl_cmd, pm, self.mode), ""))

            # Wait till the PM is in the Configured state.
            cmds.append(("{0:s} {1:s} --wait-until Configured".format(fsm_poll_cmd, pm), ""))

        # Configure iCIs.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Configuring iCIs'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list(ICIS)
        for target in targets:
            tmp = ""
            if pm:
                tmp = "_under_{0:s}".format(self.mode)
            cmds.append(("{0:s} {1:s} Configure hw_cfg_ici{2:s}.txt".format(fsm_ctrl_cmd, target, tmp), ""))

        # Wait till everything is in the Configured state.
        for target in targets:
            cmds.append(("{0:s} {1:s} --wait-until Configured".format(fsm_poll_cmd, target), ""))

        # Configure APVEs.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Configuring APVEs'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list(APVES)
        for target in targets:
            cmds.append(("{0:s} {1:s} Configure hw_cfg_apve.txt".format(fsm_ctrl_cmd, target), ""))

        # Wait till everything is in the Configured state.
        for target in targets:
            cmds.append(("{0:s} {1:s} --wait-until Configured".format(fsm_poll_cmd, target), ""))

        # Configure PIs.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Configuring PIs'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list(PIS)
        for target in targets:
            cmds.append(("{0:s} {1:s} Configure hw_cfg_pi.txt".format(fsm_ctrl_cmd, target), ""))

        # Wait till everything is in the Configured state.
        for target in targets:
            cmds.append(("{0:s} {1:s} --wait-until Configured".format(fsm_poll_cmd, target), ""))

        #----------

        # Force PIs to be TTS READY.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Forcing PI TTS states to READY'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list(PIS)
        for target in targets:
            cmds.append(("{0:s} {1:s}".format(pi_force_ready_cmd, target), ""))

        #----------

        # Enable iCIs, APVEs, PIs.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Enabling iCIs, APVEs, and PIs'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list(ICIS)
        targets.extend(APVES)
        targets.extend(PIS)
        for target in targets:
            cmds.append(("{0:s} {1:s} Enable".format(fsm_ctrl_cmd, target), ""))

        # Enable PM (as last!).
        if pm:
            cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                         ("python -c \"print 'Enabling {0:s}'\"".format(self.mode.upper()), ""),
                         ("python -c \"print '{0:s}'\"".format(sep_line), "")])
            cmds.append(("{0:s} {1:s} Enable".format(fsm_ctrl_cmd, pm), ""))

        #----------

        cmds = add_cwd_to_cmds(cmds)
        if self.verbose:
            cmds = append_piece_to_cmds(cmds, "--verbose")

        runner = CommandRunner(cmds)
        runner.run()

        # End of main().

    # End of class Tester.

###############################################################################

if __name__ == "__main__":

    description = "Master script to study B-channel counters."
    usage = "usage: %prog [options]"
    res = Tester(description, usage).run()
    print "Done"
    sys.exit(res)

###############################################################################
