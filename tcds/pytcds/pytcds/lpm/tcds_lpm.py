###############################################################################
## Utilities related to the LPM board.
###############################################################################

from pytcds.lpm.tcds_lpm_constants import ADDRESS_TABLE_FILE_NAME_LPM
from pytcds.utils.tcds_constants import BOARD_TYPE_FC7
from pytcds.utils.tcds_constants import BOARD_TYPE_LPM
from pytcds.utils.tcds_constants import IPM
from pytcds.utils.tcds_fc7 import TCDSFC7
from pytcds.utils.tcds_utils_uhal import get_hw_tca

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

# The address of the I2C multiplexer switching between the different
# SFPs on the TCDS FMCs.
# NOTE: Adresses specified by FMC number.
I2C_ADDRESS_MUX_SFP_INFO_REGISTERS = {
    1 : 0x70,
    2 : 0x73
}

# The address of the I2C multiplexer switching between the status of
# the different SFPs on the TCDS FMCs.
# NOTE: Adresses specified by FMC number.
I2C_ADDRESS_MUX_SFP_STATUS = {
    1 : 0x74,
    2 : 0x77
}

MUX_CHANNELS_SFP_STATUS = {
    "mod_abs" : 1,
    "tx_dis" : 2,
    "tx_fault" : 3,
    "rx_los" : 4
}

# The address of the SFP status registers on the TCDS FMCs.
I2C_ADDRESS_SFP_STATUS_REGISTERS = 0x38

# # The address of the TCDS FMC module type register.
# I2C_ADDRESS_FMC_TYPE = 0x38

# The address of the SFP vendor-info registers on the TCDS FMCs.
I2C_ADDRESS_SFP_VENDOR_INFO = 0x50

# The mapping between SFPs and FMCs.
SFP_TO_FMC_MAP = {
    1 : 1,
    2 : 1,
    3 : 1,
    4 : 1,
    5 : 1,
    6 : 1,
    7 : 1,
    8 : 1
    }

# Constants defining the various 'soft' trigger and B-go requests.
IPBUS_REQUEST_TYPE_L1A = 0x100
IPBUS_REQUEST_TYPE_BGO = 0x200
IPBUS_REQUEST_TYPE_BGO_TRAIN = 0x400

###############################################################################

class LPM(TCDSFC7):

    def get_sfp_status(self, fmc_num, status_type, verbose=False):
        i2c_address = I2C_ADDRESS_MUX_SFP_STATUS[fmc_num]
        tmp_status_type = status_type.lower()
        channel_num = MUX_CHANNELS_SFP_STATUS[tmp_status_type]
        self.i2c.enable("user", fmc_num)
        msg = "Select mux channel {0:d}: {1:s}".format(channel_num, tmp_status_type)
        self.i2c.write_register("user",
                                i2c_address,
                                0x1 << channel_num,
                                msg,
                                verbose)
        res = self.i2c.read_register("user",
                                     I2C_ADDRESS_SFP_STATUS_REGISTERS,
                                     "Read {0:s}".format(tmp_status_type),
                                     verbose)
        # End of get_sfp_status().
        return res

    def switch_sfp(self, sfp_num, switch_on, verbose=False):
        # ASSERT ASSERT ASSERT
        assert (0 < sfp_num <= get_num_icis())
        # ASSERT ASSERT ASSERT end
        fmc_num = SFP_TO_FMC_MAP[sfp_num]
        i2c_address = I2C_ADDRESS_MUX_SFP_STATUS[fmc_num]
        self.i2c.enable("user", fmc_num)
        channel_num = MUX_CHANNELS_SFP_STATUS["tx_dis"]
        msg = "Select mux channel {0:d}: tx_dis".format(channel_num)
        self.i2c.write_register("user",
                                i2c_address,
                                0x1 << channel_num,
                                msg,
                                verbose)
        val_ori=  self.i2c.read_register("user",
                                         I2C_ADDRESS_SFP_STATUS_REGISTERS,
                                         "Read tx_dis",
                                         verbose)
        # NOTE: Good to remember when masking: the value we're dealing
        # with is the TX_DISABLE register -> negative logic.
        mask = (1 << (sfp_num - 1))
        val_new = None
        if switch_on:
            val_new = val_ori & ~mask
        else:
            val_new = val_ori | mask
        self.i2c.write_register("user",
                                I2C_ADDRESS_SFP_STATUS_REGISTERS,
                                val_new,
                                "Write tx_dis",
                                verbose)
        # End of switch_sfp().

    def switch_all_sfps(self, fmc_num, switch_on, verbose=False):
        i2c_address = I2C_ADDRESS_MUX_SFP_STATUS[fmc_num]
        self.i2c.enable("user", fmc_num)
        channel_num = MUX_CHANNELS_SFP_STATUS["tx_dis"]
        msg = "Select mux channel {0:d}: tx_dis".format(channel_num)
        self.i2c.write_register("user",
                                i2c_address,
                                0x1 << channel_num,
                                msg,
                                verbose)
        value = 0x0
        if not switch_on:
            value = 0xffff
        self.i2c.write_register("user",
                                I2C_ADDRESS_SFP_STATUS_REGISTERS,
                                value,
                                "Write tx_dis",
                                verbose)
        # End of switch_all_sfps().

    def get_sfp_vendor_info(self, fmc_num, sfp_num, verbose=False):
        # This reads straight from the SFP. The memory locations and
        # string lengths come straigh from the specification:
        #   'INF-8074i Specification for SFP (Small Formfactor Pluggable) Transceiver'
        #   ftp://ftp.seagate.com/sff/INF-8074.PDF
        i2c_address = I2C_ADDRESS_MUX_SFP_INFO_REGISTERS[fmc_num]
        self.i2c.write_register("user",
                                i2c_address,
                                0x1 << (sfp_num - 1),
                                "Select SFP-info mux")
        sfp_vendor_name = self.i2c.read_string("user",
                                               I2C_ADDRESS_SFP_VENDOR_INFO,
                                               20,
                                               16,
                                               "Read SFP vendor name",
                                               verbose)
        sfp_vendor_pn = self.i2c.read_string("user",
                                             I2C_ADDRESS_SFP_VENDOR_INFO,
                                             40,
                                             16,
                                             "Read SFP vendor part number",
                                             verbose)
        sfp_vendor_rev = self.i2c.read_string("user",
                                              I2C_ADDRESS_SFP_VENDOR_INFO,
                                              56,
                                              4,
                                              "Read SFP vendor revision",
                                              verbose)
        sfp_vendor_sn = self.i2c.read_string("user",
                                             I2C_ADDRESS_SFP_VENDOR_INFO,
                                             68,
                                             16,
                                             "Read SFP vendor serial number",
                                             verbose)
        res = {"vendor_name" : sfp_vendor_name,
               "vendor_part_number" : sfp_vendor_pn,
               "vendor_revision" : sfp_vendor_rev,
               "vendor_serial_number" : sfp_vendor_sn}
        # End of get_sfp_vendor_info().
        return res

    def get_num_icis(self):
        """Returns the number of iCI instances in this LPM."""
        res = self.read("fmc_main.build_info.n_o_icis")
        # End of get_num_icis().
        return res

    def get_num_cyclic_gens_in_ipm(self):
        """Returns the number of cyclic generators implemented in the iPM."""
        res = self.read("ipm.main.build_info.n_o_cyclic_generators")
        # End of get_num_cyclic_gens_in_ipm().
        return res

    def get_num_cyclic_gens_in_ici(self, ici_num):
        """Returns the number of cyclic generators implemented in an iCI."""
        reg_name = "ici{0:d}.main.build_info.n_o_cyclic_generators".format(ici_num)
        res = self.read(reg_name)
        # End of get_num_cyclic_gens_in_ici().
        return res

    def get_num_bchannels(self, ici_num):
        """Returns the number of B-channels implemented in an iCI."""
        reg_name = "ici{0:d}.main.build_info.n_o_bchannels".format(ici_num)
        res = self.read(reg_name)
        # End of get_num_bchannels().
        return res

    def get_num_bgo_channels(self):
        """Returns the number of B-channels implemented in the iPM."""
        reg_name = "ipm.main.build_info.n_o_bgo_channels"
        res = self.read(reg_name)
        # End of get_num_bgo_channels().
        return res

    def get_event_count(self, ici_num=None):
        if ici_num is None:
            reg_name = "ipm.main.event_counter"
        else:
            reg_name = "ici{0:d}.main.event_counter".format(ici_num)
        res = self.hw.read(reg_name)
        # End of get_event_count().
        return res

    def get_bgo_count(self, ici_num=None):
        if ici_num is None:
            reg_name = "ipm.main.bgo_counter"
        else:
            reg_name = "ici{0:d}.main.bgo_counter".format(ici_num)
        res = self.hw.read(reg_name)
        # End of get_bgo_count().
        return res

    def reset(self):
        """Reset the device to a well-known configuration."""
        # TODO TODO TODO
        # Needs some more work. Like clearing out B-channel RAMs etc.
        self.reset_cyclic_generators(None)
        num_icis = self.get_num_icis()
        for ici_num in xrange(1, num_icis + 1):
            reg_name = "ici{0:d}.main.inselect".format(ici_num)
            self.write(reg_name, 0)
            # reg_name = "ici{0:d}.main.counter_resets".format(ici_num)
            # self.write(reg_name, 0xffffffff)
            self.reset_cyclic_generators(ici_num)
            self.reset_bchannel_rams(ici_num)
        # TODO TODO TODO end
        # End of reset().

    def reset_cyclic_generators(self, ici_num=None):
        if ici_num is None:
            num_cyclic_gens = self.get_num_cyclic_gens_in_ipm()
        else:
            num_cyclic_gens = self.get_num_cyclic_gens_in_ici(ici_num)
        for cyclic_gen_num in xrange(num_cyclic_gens):
            self.reset_cyclic_generator(ici_num, cyclic_gen_num)
        # End of reset_cyclic_generators().

    def reset_cyclic_generator(self, ici_num, cyclic_gen_num):
        reg_names = [
            "enabled",
            "initial_prescale",
            "pause",
            "permanent",
            "postscale",
            "prescale",
            "repeat_cycle",
            "start_bx",
            "trigger_word"
            ]
        if ici_num is None:
            reg_name_base = "ipm.cyclic_generator{0:d}.configuration".format(cyclic_gen_num)
        else:
            reg_name_base = "ici{0:d}.cyclic_generator{1:d}.configuration".format(ici_num, cyclic_gen_num)
        for reg_name in reg_names:
            self.hw.write("{0:s}.{1:s}".format(reg_name_base, reg_name), 0x0)
        # End of reset_cyclic_generator().

    def reset_bchannel_rams(self, ici_num):
        num_bchannels = self.get_num_bchannels(ici_num)
        for bchannel_num in xrange(num_bchannels):
            self.reset_bchannel_ram(ici_num, bchannel_num)
        # End of reset_bchannel_rams().

    def reset_bchannel_ram(self, ici_num, bchannel_num):
        reg_name = "ici{0:d}.bchannels.bchannel{1:d}.ram".format(ici_num, bchannel_num)
        ram_size = self.hw.get_node(reg_name).getSize()
        self.hw.write_block(reg_name, [0x0] * ram_size)
        # End of reset_bchannel_ram().

    def enable_ipbus_requests(self, ici_number):
        if ici_number == 0:
            # Use the iPM -> but nothing is needed in the iPM to
            # enable/disable IPbus requests.
            pass
        else:
            reg_name_base = "ici{0:d}.main.inselect".format(ici_number)
            self.write("{0:s}.combined_software_bgo_enable".format(reg_name_base), 0x1)
            self.write("{0:s}.combined_software_trigger_enable".format(reg_name_base), 0x1)
        # End of enable_ipbus_requests().

    def disable_ipbus_requests(self, ici_number):
        if ici_number == 0:
            # Use the iPM -> but nothing is needed in the iPM to
            # enable/disable IPbus requests.
            pass
        else:
            # Use the requested iCI.
            reg_name_base = "ici{0:d}.main.inselect".format(ici_number)
            self.write("{0:s}.combined_software_bgo_enable".format(reg_name_base), 0x0)
            self.write("{0:s}.combined_software_trigger_enable".format(reg_name_base), 0x0)
        # End of disable_ipbus_requests().

    def send_ipbus_l1a(self, ici_number):
        """Sends an IPbus request for a trigger."""
        if ici_number == IPM:
            # Use the iPM.
            reg_name = "ipm.main.ipbus_requests"
        else:
            # Use the requested iCI.
            reg_name = "ici{0:d}.main.ipbus_requests".format(ici_number)
        tmp = IPBUS_REQUEST_TYPE_L1A
        self.write(reg_name, tmp)
        # End of send_ipbus_l1a().

    def send_ipbus_bgo(self, ici_number, bgo_number):
        """Sends an IPbus request for a B-go."""
        if ici_number == IPM:
            # Use the iPM.
            reg_name = "ipm.main.ipbus_requests"
        else:
            # Use the requested iCI.
            reg_name = "ici{0:d}.main.ipbus_requests".format(ici_number)
        tmp = IPBUS_REQUEST_TYPE_BGO | bgo_number
        self.write(reg_name, tmp)
        # End of send_ipbus_bgo().

    def send_ipbus_bgo_train(self, bgo_train_number):
        """Sends an IPbus request for a B-go train/sequence."""
        reg_name = "ipm.main.ipbus_requests"
        tmp = IPBUS_REQUEST_TYPE_BGO_TRAIN | bgo_train_number
        self.write(reg_name, tmp)
        # End of send_ipbus_bgo_train().

    def send_ipbus_bcommand(self, ici_number, bcommand_data, is_long=False):
        """Sends an IPbus request for a B-command.

        NOTE: These requests use their own B-channels and cannot be
        blocked by anything in the input selection register.

        """

        tmp = "short"
        if is_long:
            tmp = "long"
        reg_name_base = "ici{0:d}.bchannels.main.ipbus_requests.bchannel_{1:s}"
        reg_name = reg_name_base.format(ici_number, tmp)
        self.write(reg_name, bcommand_data)
        # End of send_ipbus_bcommand().

    # End of class LPM.

###############################################################################

def get_lpm_hw(address_or_name,
               controlhub_address=None,
               verbose=False,
               hw_cls=LPM):

    lpm = get_hw_tca(BOARD_TYPE_FC7,
                     BOARD_TYPE_LPM,
                     ADDRESS_TABLE_FILE_NAME_LPM,
                     address_or_name,
                     controlhub_address,
                     verbose,
                     hw_cls)

    return lpm

###############################################################################
