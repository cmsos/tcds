#!/usr/bin/env python

###############################################################################
## Check ETC + BST timestamps from the CPM (along the backplane) to the LPM.
###############################################################################

import datetime
import sys
import time

from pytcds.lpm.tcds_lpm import get_lpm_hw
from pytcds.utils.tcds_cmd_line_base import CmdLineBase

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

NUM_ITERATIONS = 100
SLEEP_TIME = .4

###############################################################################

class Checker(CmdLineBase):

    def main(self):
        self.validate_target(target_type=CmdLineBase.TARGET_TYPE_HW)

        #----------

        board_info = self.setup.hw_targets[self.target]
        network_address = board_info.dns_alias
        controlhub_address = board_info.controlhub
        lpm = get_lpm_hw(network_address, controlhub_address, verbose=self.verbose)
        if not lpm:
            self.error("Could not connect to {0:s}.".format(network_address))

        #----------

        while True:
            print "A"
            lpm.write("ici1.main.resets.cyclic_generators_and_bchannels_init", 0x1)
            # lpm.write("ici5.main.resets.", 0x1)
            time.sleep(0.1)

        #----------

        # End of main().

    # End of class Checker.

###############################################################################

if __name__ == "__main__":

    res = Checker("").run()

    print "Done"
    sys.exit(res)

###############################################################################
