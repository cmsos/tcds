#!/usr/bin/env python

###############################################################################
## Send a B-go from an LPM/iCI.
###############################################################################

import sys
import time

from pytcds.lpm.tcds_lpm import get_lpm_hw
from pytcds.utils.tcds_cmd_line_base import CmdLineBase
from pytcds.utils.tcds_constants import BGO_NUMBERS
from pytcds.utils.tcds_constants import ICI_NUMBERS
from pytcds.utils.tcds_utils_networking import resolve_network_address

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

def parse_as_bgo_number(bgo_number_string):
    bgo_number = None
    try:
        bgo_number = BGO_NUMBERS[bgo_number_string]
    except KeyError:
        try:
            bgo_number = int(bgo_number_string)
        except ValueError:
            msg = "Could not translate '{0:s}' into a B-go number."
            raise ValueError(msg.format(bgo_number_string))

    # ASSERT ASSERT ASSERT
    assert (bgo_number >= 0) and (bgo_number <= 15), \
        "Invalid B-go number: {0:d}.".format(bgo_number)
    # ASSERT ASSERT ASSERT end

    # End of parse_as_bgo_number().
    return bgo_number

###############################################################################

class BgoSender(CmdLineBase):

    def __init__(self, description=None, usage=None, epilog=None):
        super(BgoSender, self).__init__(description, usage, epilog)
        self.ici_number_string = None
        self.ici_number = None
        self.bgo_number_string = None
        self.bgo_number = None
        # End of __init__().

    def setup_parser_custom(self):
        super(BgoSender, self).setup_parser_custom()
        parser = self.parser

        # Add the iCI number argument.
        help_str = "The target iCI (or iPM)"
        parser.add_argument("ici",
                            type=str,
                            choices=sorted(ICI_NUMBERS),
                            help=help_str)

        # Add the B-go number argument.
        help_str = "The target B-go name or number"
        parser.add_argument("bgo",
                            type=str,
                            help=help_str)

        # End of setup_parser_custom().

    def handle_args(self):
        super(BgoSender, self).handle_args()

        # Extract the iCI number.
        self.ici_number_string = self.args.ici
        try:
            self.ici_number = ICI_NUMBERS[self.ici_number_string]
        except KeyError:
            msg = "Failed to interpret '{0:s}' as a valid iCI number. " \
                "Options are: {1:s}."
            self.error(msg.format(self.ici_number_string,
                                  ", ".join(sorted(ICI_NUMBERS.keys()))))

        # Extract the B-go number.
        self.bgo_number_string = self.args.bgo
        try:
            self.bgo_number = parse_as_bgo_number(self.bgo_number_string)
        except Exception:
            msg = "Failed to interpret '{0:s}' as a B-go number."
            self.error(msg.format(self.bgo_number_string))
        # End of handle_args().

    def main(self):
        self.validate_target(target_type=CmdLineBase.TARGET_TYPE_HW)
        board_info = self.setup.hw_targets[self.target]
        network_address = board_info.dns_alias
        controlhub_address = board_info.controlhub
        lpm = get_lpm_hw(network_address, controlhub_address, verbose=self.verbose)
        if not lpm:
            self.error("Could not connect to {0:s}.".format(self.ip_address))

        if self.verbose:
            bgo_number_string = str(self.bgo_number)
            if self.bgo_number_string != bgo_number_string:
                bgo_number_string = "{0:s} (B-go #{1:d})".format(self.bgo_number_string,
                                                                 self.bgo_number)
            if self.ici_number == 0:
                msg = "Sending B-go {0:s} through the LPM."
                print msg.format(bgo_number_string)
            else:
                msg = "Sending B-go {0:s} through iCI #{1:d}."
                print msg.format(bgo_number_string, self.ici_number)

        #----------

        lpm.enable_ipbus_requests(self.ici_number)
        lpm.send_ipbus_bgo(self.ici_number, self.bgo_number)
        lpm.disable_ipbus_requests(self.ici_number)

        #----------

        # End of main().

    # End of class BgoSender.

###############################################################################

if __name__ == "__main__":

    description = "Send a B-go from an LPM/iCI."
    epilog = "ICI_NUMBER: {0:s}.".format(", ".join(sorted(ICI_NUMBERS.keys())))
    res = BgoSender(epilog=epilog).run()
    print "Done"
    sys.exit(res)

###############################################################################
