#!/usr/bin/env python

###############################################################################
## Let's try Paschalis's I2CPlayer.
###############################################################################

import sys
import time

from pytcds.lpm.tcds_lpm import get_lpm_hw
from pytcds.utils.tcds_cmd_line_base import CmdLineBase
from pytcds.utils.tcds_utils_networking import resolve_network_address

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class PlayerTester(CmdLineBase):

    def main(self):
        lpm = get_lpm_hw(self.ip_address, verbose=self.verbose)
        if not lpm:
            self.error("Could not connect to {0:s}.".format(self.ip_address))

        #----------

        # I2C-player settings.
        lpm.write("user.i2c_player_ctrl.mode", 0x0)
        lpm.write("user.i2c_player_ctrl.sleep_time", 0x3e8)

        # Fill the command buffer.
        i2c_player_cmds = []
        # L8 status.
        i2c_player_cmds.append(0x80f70001)
        i2c_player_cmds.append(0x80380000)
        i2c_player_cmds.append(0x80f70002)
        i2c_player_cmds.append(0x80380000)
        i2c_player_cmds.append(0x80f70004)
        i2c_player_cmds.append(0x80380000)
        i2c_player_cmds.append(0x80f70008)
        i2c_player_cmds.append(0x80380000)
        i2c_player_cmds.append(0x80f70010)
        i2c_player_cmds.append(0x80380000)
        # L8 SFP A real-time diagnostics.
        i2c_player_cmds.append(0x80f30001)
        i2c_player_cmds.append(0x82516000)
        i2c_player_cmds.append(0x82516200)
        i2c_player_cmds.append(0x82516400)
        i2c_player_cmds.append(0x82516600)
        i2c_player_cmds.append(0x82516800)
        # L8 SFP B real-time diagnostics.
        i2c_player_cmds.append(0x80f30002)
        i2c_player_cmds.append(0x82516000)
        i2c_player_cmds.append(0x82516200)
        i2c_player_cmds.append(0x82516400)
        i2c_player_cmds.append(0x82516600)
        i2c_player_cmds.append(0x82516800)
        i2c_player_cmds.append(0x0)
        lpm.write_block("user.i2c_player_tx", i2c_player_cmds)

        # Enable the I2C-player.
        lpm.write("user.i2c_player_ctrl.enable", 0x1)

        # Wait for a little while.
        time.sleep(1)

        num_cmds = len(i2c_player_cmds)
        results = lpm.read_block("user.i2c_player_rx", num_cmds)
        for i in results:
            print "0x{0:08x}".format(i)

        # # Wait for a little while.
        # time.sleep(10)

        # Disable the I2C-player.
        lpm.write("user.i2c_player_ctrl.enable", 0x0)

        #----------

        # End of main().

    # End of class PlayerTester.

###############################################################################

if __name__ == "__main__":

    description = "Try the I2C-player of an LPM."
    usage = "usage: %prog [options]"
    res = PlayerTester(description, usage).run()
    print "Done"
    sys.exit(res)

###############################################################################
