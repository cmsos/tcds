#!/usr/bin/env python

###############################################################################
## Check ETC + BST timestamps from the CPM (along the backplane) to the LPM.
###############################################################################

import datetime
import sys
import time

from pytcds.lpm.tcds_lpm import get_lpm_hw
from pytcds.utils.tcds_cmd_line_base import CmdLineBase

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

NUM_ITERATIONS = 100
SLEEP_TIME = .4

###############################################################################

class Checker(CmdLineBase):

    def main(self):
        self.validate_target(target_type=CmdLineBase.TARGET_TYPE_HW)

        #----------

        board_info = self.setup.hw_targets[self.target]
        network_address = board_info.dns_alias
        controlhub_address = board_info.controlhub
        lpm = get_lpm_hw(network_address, controlhub_address, verbose=self.verbose)
        if not lpm:
            self.error("Could not connect to {0:s}.".format(network_address))

        #----------

        # Read the timestamps (which, if all is well, originate from
        # the BST data received by the CPM) and some other stuff.
        try:
            for i in xrange(NUM_ITERATIONS):
                run_number_lo = lpm.read("lpm_main.bst_etc_spy.etc_unpacked.etc_run_number_lo")
                run_number_hi = lpm.read("lpm_main.bst_etc_spy.etc_unpacked.etc_run_number_hi")
                run_number = (run_number_hi << 32) + run_number_lo
                section_number = lpm.read("lpm_main.bst_etc_spy.etc_unpacked.etc_lumi_section_number")
                nibble_number = lpm.read("lpm_main.bst_etc_spy.etc_unpacked.etc_lumi_nibble_number")
                cms_run_active = lpm.read("lpm_main.bst_etc_spy.etc_unpacked.etc_cms_run_active")

                # bst_reception_status = cpm.read("lpm_main.bst_etc_spy.bst_status")
                # n_microsec = cpm.read("cpmt1.brildaq.plain.bst_timestamp.microseconds")
                # n_sec = cpm.read("cpmt1.brildaq.plain.bst_timestamp.seconds")
                # timestamp = datetime.datetime.utcfromtimestamp(n_sec) + \
                #             datetime.timedelta(microseconds=n_microsec)

                print "----------"
                print "  run {0:d}, section {1:d}, nibble {2:d}".format(run_number,
                                                                        section_number,
                                                                        nibble_number)
                print "  CMS run active: {0:b}".format(cms_run_active)
                # print "  BST timestamp: {0:s}".format(timestamp.isoformat())

                time.sleep(SLEEP_TIME)

                print "  press CTRL-C to stop..."

        except KeyboardInterrupt:
            pass

        #----------

        # End of main().

    # End of class Checker.

###############################################################################

if __name__ == "__main__":

    desc_str = "Check ETC + BST timestamps from the CPM" \
        " (along the backplane)" \
        " to the LPM."

    res = Checker(desc_str).run()

    print "Done"
    sys.exit(res)

###############################################################################
