#!/usr/bin/env python

###############################################################################
## Master script to configure and enable an CPM/LPM driving several
## iCIs and PIs for multiple runs, semi-randomly.
###############################################################################

import os
import random
import sys
import time

from pytcds.test_multi_runs.tcds_test_multi_runs_constants import __file__ as constants_file_name
from pytcds.test_multi_runs.tcds_test_multi_runs_constants import APVES
from pytcds.test_multi_runs.tcds_test_multi_runs_constants import CPM
from pytcds.test_multi_runs.tcds_test_multi_runs_constants import FED_IDS_MISC
from pytcds.test_multi_runs.tcds_test_multi_runs_constants import FED_ID_CPM
from pytcds.test_multi_runs.tcds_test_multi_runs_constants import FED_ID_GT1
from pytcds.test_multi_runs.tcds_test_multi_runs_constants import FED_ID_GT2
from pytcds.test_multi_runs.tcds_test_multi_runs_constants import FED_ID_LPM
from pytcds.test_multi_runs.tcds_test_multi_runs_constants import ICIS
from pytcds.test_multi_runs.tcds_test_multi_runs_constants import LPM
from pytcds.test_multi_runs.tcds_test_multi_runs_constants import NUM_TRANSITIONS_MAX
from pytcds.test_multi_runs.tcds_test_multi_runs_constants import NUM_TRANSITIONS_MIN
from pytcds.test_multi_runs.tcds_test_multi_runs_constants import PIS
from pytcds.test_multi_runs.tcds_test_multi_runs_constants import SLEEP_BETWEEN_RUNS
from pytcds.test_multi_runs.tcds_test_multi_runs_constants import SLEEP_RUN_MAX
from pytcds.test_multi_runs.tcds_test_multi_runs_constants import SLEEP_RUN_MIN
from pytcds.utils.tcds_cmd_line_base import CmdLineBase
from pytcds.utils.tcds_command_runner import CommandRunner
from pytcds.utils.tcds_command_runner import add_cwd_to_cmds

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

def append_piece_to_cmds(cmds, piece):
    res = []
    for (cmd, doc) in cmds:
        full_cmd = cmd
        if cmd.endswith(".py"):
            full_cmd = "%s %s" % (cmd, piece)
        res.append((full_cmd, doc))
    # End of append_piece_to_cmds().
    return res

###############################################################################

class TCDSConfigForTestMultiRuns(CmdLineBase):

    MODE_CHOICES = ["cpm", "lpm", "ici"]

    def __init__(self, description=None, usage=None, epilog=None):
        super(TCDSConfigForTestMultiRuns, self).__init__(description, usage, epilog)
        self.mode = None
        # End of __init__().

    def setup_parser_custom(self):
        parser = self.parser
        # Add the choice of running mode.
        help_str = "Running mode"
        parser.add_argument("mode",
                            type=str,
                            action="store",
                            choices=TCDSConfigForTestMultiRuns.MODE_CHOICES,
                            help=help_str)
        # End of setup_parser_custom().

    def handle_args(self):
        super(TCDSConfigForTestMultiRuns, self).handle_args()
        # Extract the driving mode.
        self.mode = self.args.mode
        # End of handle_args().

    def main(self):
        sep_line = "-" * 70

        test_dir_name = os.path.dirname(constants_file_name)

        pm = None
        if self.mode == "lpm":
            pm = LPM
        elif self.mode == "cpm":
            pm = CPM

        #----------

        tmp = "".join(["{0:d}&{1:d}%".format(i[0], i[1]) for i in FED_IDS_MISC])
        pm_daq_on_off = '0'
        gt1_enabled_disabled = '1'
        gt2_enabled_disabled = '0'

        fed_enable_mask = "{0:d}&{1:s}%{2:d}&{3:s}%{4:d}&{5:s}%{6:d}&{7:s}%{8:s}".format(FED_ID_CPM,
                                                                                         pm_daq_on_off,
                                                                                         FED_ID_LPM,
                                                                                         pm_daq_on_off,
                                                                                         FED_ID_GT1,
                                                                                         gt1_enabled_disabled,
                                                                                         FED_ID_GT2,
                                                                                         gt2_enabled_disabled,
                                                                                         tmp)

        #----------

        fsm_ctrl_cmd = "../utils/tcds_fsm_control.py --setup-file {0:s} --location {1:s}".format(self.setup_file_name, self.location_name)
        fsm_poll_cmd = "../utils/tcds_fsm_poller.py --setup-file {0:s} --location {1:s}".format(self.setup_file_name, self.location_name)
        pi_force_tts_cmd = "../pi/tcds_pi_force_tts.py --setup-file {0:s} --location {1:s}".format(self.setup_file_name, self.location_name)
        pi_flipper_tts_cmd = "../pi/tcds_pi_fill_tts_spy_log.py --setup-file {0:s} --location {1:s}".format(self.setup_file_name, self.location_name)

        cmds = []

        #----------

        # Halt everything.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Halting all controllers'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = []
        if pm:
            targets = [pm]
        targets.extend(ICIS)
        targets.extend(APVES)
        targets.extend(PIS)
        for target in targets:
            cmds.append(("{0:s} {1:s} Halt".format(fsm_ctrl_cmd, target), ""))

        # Wait till everything is in the Halted state.
        targets = []
        if pm:
            targets = [pm]
        targets.extend(ICIS)
        targets.extend(APVES)
        targets.extend(PIS)
        for target in targets:
            cmds.append(("{0:s} {1:s} --wait-until Halted".format(fsm_poll_cmd, target), ""))

        #----------

        # Configure PM.
        if pm:
            no_beam_active_bit = ""
            if self.mode == 'cpm':
                no_beam_active_bit = " --no-beam-active"
            cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                         ("python -c \"print 'Configuring {0:s}'\"".format(self.mode.upper()), ""),
                         ("python -c \"print '{0:s}'\"".format(sep_line), "")])
            cmds.append(("{0:s} --fed-enable-mask='{1:s}' {2:s} {3:s} Configure {4:s}/hw_cfg_{5:s}.txt".format(fsm_ctrl_cmd, fed_enable_mask, no_beam_active_bit, pm, test_dir_name, self.mode), ""))
            # Wait till the PM is in the Configured state.
            cmds.append(("{0:s} {1:s} --wait-until Configured".format(fsm_poll_cmd, pm), ""))

        # Configure iCIs.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Configuring iCIs'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list(ICIS)
        for target in targets:
            tmp = ""
            if pm:
                tmp = "_under_{0:s}".format(self.mode)
            cmds.append(("{0:s} {1:s} Configure {2:s}/hw_cfg_ici{3:s}.txt".format(fsm_ctrl_cmd, target, test_dir_name, tmp), ""))

        # Wait till everything is in the Configured state.
        for target in targets:
            cmds.append(("{0:s} {1:s} --wait-until Configured".format(fsm_poll_cmd, target), ""))

        # Configure APVEs.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Configuring APVEs'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list(APVES)
        for target in targets:
            cmds.append(("{0:s} {1:s} Configure {2:s}/hw_cfg_apve.txt".format(fsm_ctrl_cmd, target, test_dir_name), ""))

        # Wait till everything is in the Configured state.
        for target in targets:
            cmds.append(("{0:s} {1:s} --wait-until Configured".format(fsm_poll_cmd, target), ""))

        # Configure PIs.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Configuring PIs'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list(PIS)
        for target in targets:
            cmds.append(("{0:s} --fed-enable-mask='{1:s}' {2:s} Configure {3:s}/hw_cfg_pi.txt".format(fsm_ctrl_cmd, fed_enable_mask, target, test_dir_name), ""))

        # Wait till everything is in the Configured state.
        for target in targets:
            cmds.append(("{0:s} {1:s} --wait-until Configured".format(fsm_poll_cmd, target), ""))

        #----------

        # Force PIs to be TTS READY.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Forcing PI TTS states to READY'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list(PIS)
        for target in targets:
            cmds.append(("{0:s} {1:s} READY".format(pi_force_tts_cmd, target), ""))

        #----------

        # Enable PIs.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Enabling PIs'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list(PIS)
        for target in targets:
            cmds.append(("{0:s} {1:s} --run-number RUNNUMBER Enable".format(fsm_ctrl_cmd, target), ""))

        # Wait till all PIs are in the Enabled state.
        for target in targets:
            cmds.append(("{0:s} {1:s} --wait-until Enabled".format(fsm_poll_cmd, target), ""))

        # Enable APVEs.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Enabling APVEs'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list(APVES)
        for target in targets:
            cmds.append(("{0:s} {1:s} --run-number RUNNUMBER Enable".format(fsm_ctrl_cmd, target), ""))

        # Wait till all APVEs are in the Enabled state.
        for target in targets:
            cmds.append(("{0:s} {1:s} --wait-until Enabled".format(fsm_poll_cmd, target), ""))

        # Enable iCIs.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Enabling iCIs'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list(ICIS)
        for target in targets:
            cmds.append(("{0:s} {1:s} --run-number RUNNUMBER Enable".format(fsm_ctrl_cmd, target), ""))

        # Wait till all ICIs are in the Enabled state.
        for target in targets:
            cmds.append(("{0:s} {1:s} --wait-until Enabled".format(fsm_poll_cmd, target), ""))

        # Enable PM (as last!).
        if pm:
            cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                         ("python -c \"print 'Enabling {0:s}'\"".format(self.mode.upper()), ""),
                         ("python -c \"print '{0:s}'\"".format(sep_line), "")])
            cmds.append(("{0:s} {1:s} --run-number RUNNUMBER Enable".format(fsm_ctrl_cmd, pm), ""))

        # Wait till also the PM is in the Enabled state.
        if pm:
            cmds.append(("{0:s} {1:s} --wait-until Enabled".format(fsm_poll_cmd, pm), ""))

        #----------

        # Wait for a semi-random time, while doing TTS transitions on
        # the first PI.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Biding time while flippering TTS...'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list([PIS[0]])
        for target in targets:
            cmds.append(("{0:s} {1:s} --num-transitions NUMTRANSITIONS --duration DURATION".format(pi_flipper_tts_cmd, target), ""))

        #----------

        # And now stop everything again.

        # Pause PM.
        if pm:
            cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                         ("python -c \"print 'Pausing {0:s}'\"".format(self.mode.upper()), ""),
                         ("python -c \"print '{0:s}'\"".format(sep_line), "")])
            cmds.append(("{0:s} {1:s} Pause".format(fsm_ctrl_cmd, pm), ""))
            # Wait till the PM is in the Paused state.
            cmds.append(("{0:s} {1:s} --wait-until Paused".format(fsm_poll_cmd, pm), ""))

        # Stop iCIs.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Stopping iCIs'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list(ICIS)
        for target in targets:
            cmds.append(("{0:s} {1:s} Stop".format(fsm_ctrl_cmd, target), ""))

        # Wait till everything is in the Configured state.
        for target in targets:
            cmds.append(("{0:s} {1:s} --wait-until Configured".format(fsm_poll_cmd, target), ""))

        # Stop APVEs.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Stopping APVEs'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list(APVES)
        for target in targets:
            cmds.append(("{0:s} {1:s} Stop".format(fsm_ctrl_cmd, target), ""))

        # Wait till everything is in the Configured state.
        for target in targets:
            cmds.append(("{0:s} {1:s} --wait-until Configured".format(fsm_poll_cmd, target), ""))

        # Stop PIs.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Stopping PIs'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list(PIS)
        for target in targets:
            cmds.append(("{0:s} {1:s} Stop".format(fsm_ctrl_cmd, target), ""))

        # Wait till everything is in the Configured state.
        for target in targets:
            cmds.append(("{0:s} {1:s} --wait-until Configured".format(fsm_poll_cmd, target), ""))

        # Stop PM.
        if pm:
            cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                         ("python -c \"print 'Stopping {0:s}'\"".format(self.mode.upper()), ""),
                         ("python -c \"print '{0:s}'\"".format(sep_line), "")])
            cmds.append(("{0:s} {1:s} Stop".format(fsm_ctrl_cmd, pm), ""))
            # Wait till the PM is in the Configured state.
            cmds.append(("{0:s} {1:s} --wait-until Configured".format(fsm_poll_cmd, pm), ""))

        # Useful comment at the end.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Iteration done'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])

        #----------

        cmds = add_cwd_to_cmds(cmds)
        if self.verbose:
            cmds = append_piece_to_cmds(cmds, '--verbose')

        #----------

        # NOTE: Now start an infinite loop repeating the above.
        # NOTE: Run number 1 billion is the starting point of the
        # DAQVal etc. special runs.
        run_number = int(1.e9)
        try:
            while True:
                # Yeah... This is not very pretty indeed...
                num_transitions = random.randint(NUM_TRANSITIONS_MIN, NUM_TRANSITIONS_MAX)
                run_duration = random.randrange(SLEEP_RUN_MIN, SLEEP_RUN_MAX)
                cmds_tmp = [(i.replace("RUNNUMBER", str(run_number)), j) for (i, j) in cmds]
                cmds_tmp = [(i.replace("NUMTRANSITIONS", str(num_transitions)), j) for (i, j) in cmds_tmp]
                cmds_tmp = [(i.replace("DURATION", str(run_duration)), j) for (i, j) in cmds_tmp]
                runner = CommandRunner(cmds_tmp)
                runner.run()
                # Sleep for a little while in between.
                time.sleep(SLEEP_BETWEEN_RUNS)
                run_number += 1

        except KeyboardInterrupt:
            # Halt everything.
            cmds = []
            cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                         ("python -c \"print 'Halting all controllers and quitting...'\"", ""),
                         ("python -c \"print '{0:s}'\"".format(sep_line), "")])
            runner = CommandRunner(cmds)
            runner.run()

            cmds = []
            targets = []
            if pm:
                targets = [pm]
            targets.extend(ICIS)
            targets.extend(APVES)
            targets.extend(PIS)
            for target in targets:
                cmds.append(("{0:s} {1:s} Halt".format(fsm_ctrl_cmd, target), ""))

            # Wait till everything is in the Halted state.
            targets = []
            if pm:
                targets = [pm]
            targets.extend(ICIS)
            targets.extend(APVES)
            targets.extend(PIS)
            for target in targets:
                cmds.append(("{0:s} {1:s} --wait-until Halted".format(fsm_poll_cmd, target), ""))

            runner = CommandRunner(cmds)
            done = False
            while not done:
                try:
                    runner.run()
                    done = True
                except Exception as err:
                    time.sleep(1)

        # End of main().

    # End of class TCDSConfigForTestMultiRuns.

###############################################################################

if __name__ == "__main__":

    description = "Master script to configure and enable an CPM/LPM " \
                  "driving several iCIs and PIs for multiple runs, semi-randomly."
    res = TCDSConfigForTestMultiRuns(description).run()
    print "Done"
    sys.exit(res)

###############################################################################
