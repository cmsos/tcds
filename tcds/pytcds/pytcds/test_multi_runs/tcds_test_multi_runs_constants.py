###############################################################################

# The addresses of the boards/applications to connect to.
CPM = "cpm"
LPM = "lpm-1"
ICIS = ["ici-11"]
APVES = ["apve-11"]
PIS = ["pi-11"]

# The FED id of the CPM/LPM, so we can disable the readout.
FED_ID_CPM = 1024
FED_ID_LPM = 0

# The FED id(s) of the GT, so we know if it is in the run or not (in
# which case we have to enable the corresponding trigger input).
FED_ID_GT1 = 1404
FED_ID_GT2 = 1405

# Some misc. FED ids, so we can enable/disable 'random' inputs on the
# PI.
FED_IDS_MISC = [[10, 2]]

# Sleep and run parameters:
# - Run duration (s).
SLEEP_RUN_MIN = 10
SLEEP_RUN_MAX = 100
# - Between runs (s).
SLEEP_BETWEEN_RUNS = 10

NUM_TRANSITIONS_MIN = 0
NUM_TRANSITIONS_MAX = 100

###############################################################################
