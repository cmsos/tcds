#!/usr/bin/env python

###############################################################################
## A little script to check the PI TTCSpy log for the expected
## B-go/B-command spacing.
##
## NOTE: This script relies completely on some devious details of the
## CPM and ICI configurations.
###############################################################################

import ConfigParser
import ast
import datetime
import json
import os
import pickle
import re
import sys
import urllib2

from collections import namedtuple

from pytcds.utils.tcds_cmd_line_base import CmdLineBase

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class UTC(datetime.tzinfo):
    """UTC"""

    def utcoffset(self, dt):
        return datetime.timedelta(0)

    def tzname(self, dt):
        return "UTC"

    def dst(self, dt):
        return datetime.timedelta(0)

utc = UTC()

###############################################################################

# 0: Entry number.
# 1: Entry type.
# 2: Data payload.
# 3: Orbit number.
# 4: BX number.
Entry = namedtuple('Entry', ['number', 'type', 'data', 'orbit', 'bx'])

###############################################################################

def pairwise(iterable):
    it = iter(iterable)
    a = next(it, None)

    for b in it:
        yield (a, b)
        a = b

###############################################################################

# def decode_delta(delta_str_in):

#     orbits = 0
#     bxs = 0
#     try:
#         if delta_str_in.find('orbit') > -1:
#             orbits = int(delta_str_in.split('orbit')[0].strip())
#         if delta_str_in.find('BX') > -1:
#             bxs = int(delta_str_in.split('BX')[0].split()[-1].strip())
#     except Exception, err:
#         pdb.set_trace()

#     # End of decode_delta().
#     return (orbits, bxs)

###############################################################################

def calc_delta((orbits0, bxs0), (orbits1, bxs1)):

    delta_orbits = orbits1 - orbits0
    delta_bxs = bxs1 - bxs0
    if delta_bxs < 0:
        delta_orbits -= 1
        delta_bxs += 3564
    elif delta_bxs >= 3564:
        delta_orbits += 1
        delta_bxs -= 3564

    # End of calc_delta().
    return (delta_orbits, delta_bxs)

###############################################################################

class Analyzer(CmdLineBase):

    def setup_parser_custom(self):
        # Add the data file choice.
        help_str = "The name of the captured data file to analyze."
        self.parser.add_argument("data_file",
                                 type=str,
                                 action="store",
                                 help=help_str)

        # End of setup_parser_custom().

    def handle_args(self):
        # NOTE: Do not call the parent handle_args() since we are not
        # requiring an IP address (unlike pretty much all other TCDS
        # scripts).

        self.ttcspy_file_name = self.args.data_file
        # End of handle_args().

    def load_setup(self):
        pass

    def main(self):

        # Timestamp.
        TIMESTAMP = datetime.datetime.now(utc)

        ttcspy_data_raw = None
        try:
            in_file = open(self.ttcspy_file_name, "r")
            ttcspy_data_raw = in_file.read()
            in_file.close()
        except IOError, err:
            msg = "Could not read file '{0:s}': '{1:s}'."
            self.error(msg.format(self.ttcspy_file_name, err))

        ttcspy_data = []
        regexp_str = "^Entry +([0-9]+): (.*) with data (0x[0-9a-f]+) at orbit +([0-9]+).* and BX +([0-9]+)$"
        regexp = re.compile(regexp_str)
        for line in ttcspy_data_raw.split(os.linesep):
            match = regexp.search(line)
            if match:
                # 0: Entry number.
                # 1: Entry type.
                # 2: Data payload (hex).
                # 3: Orbit number.
                # 4: BX number.
                entry_number = int(match.group(1))
                entry_type = match.group(2)
                data_payload = int(match.group(3), 16)
                orbit = int(match.group(4))
                bx = int(match.group(5))
                ttcspy_data.append(Entry(entry_number,
                                         entry_type,
                                         data_payload,
                                         orbit,
                                         bx))

        if not len(ttcspy_data):
            self.error("Nothing found in the TTCSpy capture data")

        #----------

        # Crunch through the data now...

        DATA_RESYNC = 0x5
        DATA_HARDRESET = 0x6
        FREQ_LHC_ORBIT = 11246

        hardresets = [x for x in ttcspy_data if x.data == DATA_HARDRESET]
        if not len(hardresets):
            # Not found(?)
            self.error("No HardResets found(?)")

        deltas = [(calc_delta((i.orbit, i.bx), (j.orbit, j.bx)), (i, j)) for (i, j) in pairwise(hardresets)]
        delta_min = min(deltas)
        delta_max = max(deltas)
        msg_base = "Minimum delta between two successive HardResets: {0:d} orbits and {1:d} BX, or {2:.1f} seconds"
        print msg_base.format(delta_min[0][0], delta_min[0][1], 1. * delta_min[0][0] / FREQ_LHC_ORBIT)
        msg_base = "Maximum delta between two successive HardResets: {0:d} orbits and {1:d} BX, or {2:.1f} seconds"
        print msg_base.format(delta_max[0][0], delta_max[0][1], 1. * delta_max[0][0] / FREQ_LHC_ORBIT)

        # Now check that these occur as HardReset pairs without
        # anything in between.
        delta_max_pairs = [i[1] for i in deltas if i == delta_max]
        for pair in delta_max_pairs:
            entry_number_lo = pair[0].number
            entry_number_hi = pair[1].number
            if ((entry_number_hi - entry_number_hi) != 1):
                print "WARNING: The following pair of HardResets is not contiguous," \
                    "even though it shows the maximum separation!"
                print "WARNING:   low entry:  {0:s}".format(pair[0])
                print "WARNING:   high entry: {0:s}".format(pair[1])

        #----------

        # End of main().

    # End of class Analyzer.

###############################################################################

if __name__ == "__main__":

    desc_str = "A little script to check the PI TTCSpy log" \
               " for the expected B-go/B-command spacing."

    res = Analyzer(desc_str).run()

    print "Done"
    sys.exit(res)

###############################################################################
