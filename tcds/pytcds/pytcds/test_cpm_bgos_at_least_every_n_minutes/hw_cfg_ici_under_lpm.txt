#----------------------------------------------------------------------
# iCI configuration file for LPM part of tests.
#----------------------------------------------------------------------

# Enabled inputs.
main.inselect.exclusive_lpm_ipm1_bgo_enable                              0x00000001
main.inselect.exclusive_lpm_ipm1_trigger_enable                          0x00000001
main.inselect.lpm_ipm1_orbit_select                                      0x00000001

# Disabled inputs.
main.inselect.combined_cyclic_bgo_enable                                 0x00000000
main.inselect.combined_cyclic_trigger_enable                             0x00000000
main.inselect.combined_software_bgo_enable                               0x00000000
main.inselect.combined_software_trigger_enable                           0x00000000
main.inselect.combined_achannel_trigger_enable                           0x00000000
main.inselect.combined_bchannel_trigger_enable                           0x00000000
main.inselect.combined_bunch_trigger_generator_trigger_enable            0x00000000
main.inselect.combined_l1a_in0_trigger_enable                            0x00000000
main.inselect.combined_l1a_in1_trigger_enable                            0x00000000
main.inselect.combined_trigger_generator_trigger_enable                  0x00000000
main.inselect.exclusive_cpm_bgo_enable                                   0x00000000
main.inselect.exclusive_cpm_trigger_enable                               0x00000000
main.inselect.cpm_orbit_select                                           0x00000000
main.inselect.exclusive_lpm_ipm2_bgo_enable                              0x00000000
main.inselect.exclusive_lpm_ipm2_trigger_enable                          0x00000000
main.inselect.lemo_orbit_select                                          0x00000000
main.inselect.lpm_ipm2_orbit_select                                      0x00000000
main.inselect.sequence_trigger_generator_enable                          0x00000000

# Cyclic generators 0-7 off.
cyclic_generator0.configuration.enabled                                  0x00000000
cyclic_generator1.configuration.enabled                                  0x00000000
cyclic_generator2.configuration.enabled                                  0x00000000
cyclic_generator3.configuration.enabled                                  0x00000000
cyclic_generator4.configuration.enabled                                  0x00000000
cyclic_generator5.configuration.enabled                                  0x00000000
cyclic_generator6.configuration.enabled                                  0x00000000
cyclic_generator7.configuration.enabled                                  0x00000000

# B-channel 0: LumiNibble.
bchannels.bchannel0.configuration.single                                 0x00000000
bchannels.bchannel0.configuration.double                                 0x00000000
bchannels.bchannel0.configuration.bx_sync                                0x00000000
bchannels.bchannel0.configuration.bx_or_delay                            0x00000000
bchannels.bchannel0.configuration.initial_prescale                       0x00000000
bchannels.bchannel0.configuration.prescale                               0x00000000
bchannels.bchannel0.configuration.postscale                              0x00000000
bchannels.bchannel0.configuration.repeat_cycle                           0x00000001
bchannels.bchannel0.ram                                                  0x00000000 0x00000000
bchannels.bchannel0.ram                                                  0x00000000 0x00000006

# B-channel 1: BC0.
bchannels.bchannel1.configuration.single                                 0x00000001
bchannels.bchannel1.configuration.double                                 0x00000000
bchannels.bchannel1.configuration.bx_sync                                0x00000000
bchannels.bchannel1.configuration.bx_or_delay                            0x00000000
bchannels.bchannel1.configuration.initial_prescale                       0x00000000
bchannels.bchannel1.configuration.prescale                               0x00000000
bchannels.bchannel1.configuration.postscale                              0x00000000
bchannels.bchannel1.configuration.repeat_cycle                           0x00000001
bchannels.bchannel1.ram                                                  0x00000001 0x00000000
bchannels.bchannel1.ram                                                  0x00000000 0x00000006

# B-channel 2: TestEnable.
bchannels.bchannel2.configuration.single                                 0x00000000
bchannels.bchannel2.configuration.double                                 0x00000000
bchannels.bchannel2.configuration.bx_sync                                0x00000000
bchannels.bchannel2.configuration.bx_or_delay                            0x00000000
bchannels.bchannel2.configuration.initial_prescale                       0x00000000
bchannels.bchannel2.configuration.prescale                               0x00000000
bchannels.bchannel2.configuration.postscale                              0x00000000
bchannels.bchannel2.configuration.repeat_cycle                           0x00000001
bchannels.bchannel2.ram                                                  0x00000002 0x00000000
bchannels.bchannel2.ram                                                  0x00000000 0x00000006

# B-channel 3: PrivateGap.
bchannels.bchannel3.configuration.single                                 0x00000000
bchannels.bchannel3.configuration.double                                 0x00000000
bchannels.bchannel3.configuration.bx_sync                                0x00000000
bchannels.bchannel3.configuration.bx_or_delay                            0x00000000
bchannels.bchannel3.configuration.initial_prescale                       0x00000000
bchannels.bchannel3.configuration.prescale                               0x00000000
bchannels.bchannel3.configuration.postscale                              0x00000000
bchannels.bchannel3.configuration.repeat_cycle                           0x00000001
bchannels.bchannel3.ram                                                  0x00000003 0x00000000
bchannels.bchannel3.ram                                                  0x00000000 0x00000006

# B-channel 4: PrivateOrbit.
bchannels.bchannel4.configuration.single                                 0x00000000
bchannels.bchannel4.configuration.double                                 0x00000000
bchannels.bchannel4.configuration.bx_sync                                0x00000000
bchannels.bchannel4.configuration.bx_or_delay                            0x00000000
bchannels.bchannel4.configuration.initial_prescale                       0x00000000
bchannels.bchannel4.configuration.prescale                               0x00000000
bchannels.bchannel4.configuration.postscale                              0x00000000
bchannels.bchannel4.configuration.repeat_cycle                           0x00000001
bchannels.bchannel4.ram                                                  0x00000004 0x00000000
bchannels.bchannel4.ram                                                  0x00000000 0x00000006

# B-channel 5: Resync.
bchannels.bchannel5.configuration.single                                 0x00000000
bchannels.bchannel5.configuration.double                                 0x00000000
bchannels.bchannel5.configuration.bx_sync                                0x00000000
bchannels.bchannel5.configuration.bx_or_delay                            0x00000000
bchannels.bchannel5.configuration.initial_prescale                       0x00000000
bchannels.bchannel5.configuration.prescale                               0x00000000
bchannels.bchannel5.configuration.postscale                              0x00000000
bchannels.bchannel5.configuration.repeat_cycle                           0x00000001
bchannels.bchannel5.ram                                                  0x00000005 0x00000000
bchannels.bchannel5.ram                                                  0x00000000 0x00000006

# B-channel 6: HardReset.
bchannels.bchannel6.configuration.single                                 0x00000000
bchannels.bchannel6.configuration.double                                 0x00000000
bchannels.bchannel6.configuration.bx_sync                                0x00000000
bchannels.bchannel6.configuration.bx_or_delay                            0x00000000
bchannels.bchannel6.configuration.initial_prescale                       0x00000000
bchannels.bchannel6.configuration.prescale                               0x00000000
bchannels.bchannel6.configuration.postscale                              0x00000000
bchannels.bchannel6.configuration.repeat_cycle                           0x00000001
bchannels.bchannel6.ram                                                  0x00000006 0x00000000
bchannels.bchannel6.ram                                                  0x00000000 0x00000006

# B-channel 7: EC0.
bchannels.bchannel7.configuration.single                                 0x00000000
bchannels.bchannel7.configuration.double                                 0x00000000
bchannels.bchannel7.configuration.bx_sync                                0x00000000
bchannels.bchannel7.configuration.bx_or_delay                            0x00000000
bchannels.bchannel7.configuration.initial_prescale                       0x00000000
bchannels.bchannel7.configuration.prescale                               0x00000000
bchannels.bchannel7.configuration.postscale                              0x00000000
bchannels.bchannel7.configuration.repeat_cycle                           0x00000001
bchannels.bchannel7.ram                                                  0x00000007 0x00000000
bchannels.bchannel7.ram                                                  0x00000000 0x00000006

# B-channel 8: OC0.
bchannels.bchannel8.configuration.single                                 0x00000000
bchannels.bchannel8.configuration.double                                 0x00000000
bchannels.bchannel8.configuration.bx_sync                                0x00000000
bchannels.bchannel8.configuration.bx_or_delay                            0x00000000
bchannels.bchannel8.configuration.initial_prescale                       0x00000000
bchannels.bchannel8.configuration.prescale                               0x00000000
bchannels.bchannel8.configuration.postscale                              0x00000000
bchannels.bchannel8.configuration.repeat_cycle                           0x00000001
bchannels.bchannel8.ram                                                  0x00000008 0x00000000
bchannels.bchannel8.ram                                                  0x00000000 0x00000006

# B-channel 9: Start.
bchannels.bchannel9.configuration.single                                 0x00000000
bchannels.bchannel9.configuration.double                                 0x00000000
bchannels.bchannel9.configuration.bx_sync                                0x00000000
bchannels.bchannel9.configuration.bx_or_delay                            0x00000000
bchannels.bchannel9.configuration.initial_prescale                       0x00000000
bchannels.bchannel9.configuration.prescale                               0x00000000
bchannels.bchannel9.configuration.postscale                              0x00000000
bchannels.bchannel9.configuration.repeat_cycle                           0x00000001
bchannels.bchannel9.ram                                                  0x00000009 0x00000000
bchannels.bchannel9.ram                                                  0x00000000 0x00000006

# B-channel 10: Stop.
bchannels.bchannel10.configuration.single                                0x00000000
bchannels.bchannel10.configuration.double                                0x00000000
bchannels.bchannel10.configuration.bx_sync                               0x00000000
bchannels.bchannel10.configuration.bx_or_delay                           0x00000000
bchannels.bchannel10.configuration.initial_prescale                      0x00000000
bchannels.bchannel10.configuration.prescale                              0x00000000
bchannels.bchannel10.configuration.postscale                             0x00000000
bchannels.bchannel10.configuration.repeat_cycle                          0x00000001
bchannels.bchannel10.ram                                                 0x0000000a 0x00000000
bchannels.bchannel10.ram                                                 0x00000000 0x00000006

# B-channel 11: Stop.
bchannels.bchannel11.configuration.single                                0x00000000
bchannels.bchannel11.configuration.double                                0x00000000
bchannels.bchannel11.configuration.bx_sync                               0x00000000
bchannels.bchannel11.configuration.bx_or_delay                           0x00000000
bchannels.bchannel11.configuration.initial_prescale                      0x00000000
bchannels.bchannel11.configuration.prescale                              0x00000000
bchannels.bchannel11.configuration.postscale                             0x00000000
bchannels.bchannel11.configuration.repeat_cycle                          0x00000001
bchannels.bchannel11.ram                                                 0x0000000b 0x00000000
bchannels.bchannel11.ram                                                 0x00000000 0x00000006

# B-channel 12: Stop.
bchannels.bchannel12.configuration.single                                0x00000000
bchannels.bchannel12.configuration.double                                0x00000000
bchannels.bchannel12.configuration.bx_sync                               0x00000000
bchannels.bchannel12.configuration.bx_or_delay                           0x00000000
bchannels.bchannel12.configuration.initial_prescale                      0x00000000
bchannels.bchannel12.configuration.prescale                              0x00000000
bchannels.bchannel12.configuration.postscale                             0x00000000
bchannels.bchannel12.configuration.repeat_cycle                          0x00000001
bchannels.bchannel12.ram                                                 0x0000000c 0x00000000
bchannels.bchannel12.ram                                                 0x00000000 0x00000006

# B-channel 13: Stop.
bchannels.bchannel13.configuration.single                                0x00000000
bchannels.bchannel13.configuration.double                                0x00000000
bchannels.bchannel13.configuration.bx_sync                               0x00000000
bchannels.bchannel13.configuration.bx_or_delay                           0x00000000
bchannels.bchannel13.configuration.initial_prescale                      0x00000000
bchannels.bchannel13.configuration.prescale                              0x00000000
bchannels.bchannel13.configuration.postscale                             0x00000000
bchannels.bchannel13.configuration.repeat_cycle                          0x00000001
bchannels.bchannel13.ram                                                 0x0000000d 0x00000000
bchannels.bchannel13.ram                                                 0x00000000 0x00000006

# B-channel 14: Stop.
bchannels.bchannel14.configuration.single                                0x00000000
bchannels.bchannel14.configuration.double                                0x00000000
bchannels.bchannel14.configuration.bx_sync                               0x00000000
bchannels.bchannel14.configuration.bx_or_delay                           0x00000000
bchannels.bchannel14.configuration.initial_prescale                      0x00000000
bchannels.bchannel14.configuration.prescale                              0x00000000
bchannels.bchannel14.configuration.postscale                             0x00000000
bchannels.bchannel14.configuration.repeat_cycle                          0x00000001
bchannels.bchannel14.ram                                                 0x0000000e 0x00000000
bchannels.bchannel14.ram                                                 0x00000000 0x00000006

# B-channel 15: Stop.
bchannels.bchannel15.configuration.single                                0x00000000
bchannels.bchannel15.configuration.double                                0x00000000
bchannels.bchannel15.configuration.bx_sync                               0x00000000
bchannels.bchannel15.configuration.bx_or_delay                           0x00000000
bchannels.bchannel15.configuration.initial_prescale                      0x00000000
bchannels.bchannel15.configuration.prescale                              0x00000000
bchannels.bchannel15.configuration.postscale                             0x00000000
bchannels.bchannel15.configuration.repeat_cycle                          0x00000001
bchannels.bchannel15.ram                                                 0x0000000f 0x00000000
bchannels.bchannel15.ram                                                 0x00000000 0x00000006

# B-data configuration.
bchannels.main.bdata_config.bdata_enable                                 0x00000000
bchannels.main.bdata_config.bdata_source_select                          0x00000000
bchannels.main.bdata_config.bdata_type                                   0x00000001
bchannels.main.bdata_config.ttcrx_address                                0x00000000
bchannels.main.bdata_config.target_address                               0x00000000
bchannels.main.bdata_config.is_external                                  0x00000001

#----------------------------------------------------------------------
