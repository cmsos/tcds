###############################################################################
## Some overall constants related to general TCDS business.
###############################################################################

# The number of bunch crossings in a single orbit.
FIRST_BX = 1
LAST_BX = 3564
NUM_BX_PER_ORBIT = (LAST_BX - FIRST_BX) + 1

# The number of data bits in a B-command.
NUM_DATA_BITS_IN_BCOMMAND = 8

##########

# Conventions for iPM/iCI numbering.
IPM = 0
ICI1 = 1
ICI2 = 2
ICI3 = 3
ICI4 = 4
ICI5 = 5
ICI6 = 6
ICI7 = 7
ICI8 = 8

ICI_NUMBERS = {
    "ipm" : IPM,
    "ici1" : ICI1,
    "ici2" : ICI2,
    "ici3" : ICI3,
    "ici4" : ICI4,
    "ici5" : ICI5,
    "ici6" : ICI6,
    "ici7" : ICI7,
    "ici8" : ICI8
    }
ICI_NAMES = dict([(j, i) for (i, j) in ICI_NUMBERS.iteritems()])

##########

# Mapping B-go numbers to more readable constants.
BGO_LUMINIBBLE = 0
BGO_BC0 = 1
BGO_TESTENABLE = 2
BGO_PRIVATEGAP = 3
BGO_PRIVATEORBIT = 4
BGO_RESYNC = 5
BGO_HARDRESET = 6
BGO_EC0 = 7
BGO_OC0 = 8
BGO_START = 9
BGO_STOP = 10
BGO_STARTOFGAP = 11
BGO_12 = 12
BGO_WARNINGTESTENABLE = 13
BGO_ROC_RESET = 14
BGO_PIX_RESYNC = 15

# Mapping of names to numbers and vice versa.
BGO_NUMBERS = {
    "LumiNibble" : BGO_LUMINIBBLE,
    "BC0" : BGO_BC0,
    "TestEnable" : BGO_TESTENABLE,
    "PrivateGap" : BGO_PRIVATEGAP,
    "PrivateOrbit" : BGO_PRIVATEORBIT,
    "Resync" : BGO_RESYNC,
    "HardReset" : BGO_HARDRESET,
    "EC0" : BGO_EC0,
    "OC0" : BGO_OC0,
    "Start" : BGO_START,
    "Stop" : BGO_STOP,
    "StartOfGap" : BGO_STARTOFGAP,
    "Bgo12" : BGO_12,
    "WarningTestEnable" : BGO_WARNINGTESTENABLE,
    "ROCRESET" : BGO_ROC_RESET,
    "PixResync" : BGO_PIX_RESYNC
}

BGO_NAMES = dict([(j, i) for (i, j) in BGO_NUMBERS.iteritems()])

##########

# The first semi-official numbering of the B-go sequences (or B-go
# trains). B-go sequence numbers 0-9 are reserved for global
# use. Among other things they contain the global-run start, stop,
# etc. sequences previously sent by the Global Trigger.
BGO_TRAIN_START = 0x1
BGO_TRAIN_STOP = 0x2
BGO_TRAIN_RESYNC = 0x3
BGO_TRAIN_HARDRESET = 0x4

STANDARD_BGO_SEQUENCES = {
    BGO_TRAIN_START : [
        BGO_RESYNC,
        BGO_OC0,
        BGO_START,
        BGO_EC0
    ],
    BGO_TRAIN_STOP : [
        BGO_STOP
    ],
    BGO_TRAIN_RESYNC : [
        BGO_RESYNC,
        BGO_EC0
    ],
    BGO_TRAIN_HARDRESET : [
        BGO_HARDRESET,
        BGO_RESYNC,
        BGO_EC0
    ]
}

# The mapping between B-go sequence names and numbers.
STANDARD_BGO_SEQUENCE_NUMBERS = {
    "start" : BGO_TRAIN_START,
    "stop" : BGO_TRAIN_STOP,
    "resync" : BGO_TRAIN_RESYNC,
    "hardreset" : BGO_TRAIN_HARDRESET
    }

##########

# This is the overall directory for all address table files.
ADDRESS_TABLE_DIR = "/opt/xdaq/etc/tcds/addresstables"

# The uhal address table for a bare FMC carrier (i.e., either a GLIB
# or an FC7).
ADDRESS_TABLE_FILE_NAME_BARE_CARRIER = "%s/%s" % \
                                       (ADDRESS_TABLE_DIR,
                                        "device_address_table_bare_carrier.xml")

# The uhal address table for the CPM T1/T2.
ADDRESS_TABLE_FILE_NAME_CPMT1 = "%s/%s" % \
                                (ADDRESS_TABLE_DIR,
                                "device_address_table_amc13_cpmt1.xml")
ADDRESS_TABLE_FILE_NAME_CPMT2 = "%s/%s" % \
                                (ADDRESS_TABLE_DIR,
                                 "device_address_table_amc13_cpmt2.xml")

# The uhal address table for the FC7.
ADDRESS_TABLE_FILE_NAME_FC7 = "%s/%s" % \
                              (ADDRESS_TABLE_DIR,
                               "device_address_table_fc7.xml")

# The uhal address table for the GLIB.
ADDRESS_TABLE_FILE_NAME_GLIB = "%s/%s" % \
                                 (ADDRESS_TABLE_DIR,
                                  "device_address_table_glib.xml")

##########

CRATE_TYPE_VME = "VME"
CRATE_TYPE_UTCA = "uTCA"

FMC_TYPE_UNKNOWN = 0
FMC_TYPE_EMPTY = -1
FMC_TYPE_1 = 1
FMC_TYPE_2 = 2
FMC_TYPE_3 = 3
FMC_TYPE_TTC = 10

BOARD_TYPE_UNKNOWN = 0
BOARD_TYPE_BARE_CARRIER = 1
BOARD_TYPE_GOLDEN = 2
BOARD_TYPE_AMC13 = 9
BOARD_TYPE_CPMT1 = 10
BOARD_TYPE_CPMT2 = 11
BOARD_TYPE_GLIB = 12
BOARD_TYPE_FC7 = 13
BOARD_TYPE_DUMT1 = 14
BOARD_TYPE_DUMT2 = 15
# BOARD_TYPE_CPM = 20
BOARD_TYPE_LPM = 21
BOARD_TYPE_PI = 23
BOARD_TYPE_PHAS = 40
BOARD_TYPE_TTCSPY = 50
BOARD_TYPE_PHASEMON = 60
BOARD_TYPE_VME = 100
BOARD_TYPE_RF2TTC = 110
BOARD_TYPE_RFRXD = 120
BOARD_TYPE_RFTXD = 121
BOARD_TYPE_BOBR = 130

BOARD_ID_AMC13 = "TCDS"
BOARD_ID_CPMT1 = "CPM1"
BOARD_ID_CPMT2 = "CPM2"
BOARD_ID_DUMT1 = "DUM1"
BOARD_ID_DUMT2 = "DUM2"
BOARD_ID_FC7 = "FC7 "
BOARD_ID_GLIB = "GLIB"
BOARD_ID_GOLDEN = "gold"
BOARD_ID_LPM = "LPM "
BOARD_ID_PI = "PI  "
BOARD_ID_PHAS = "PHAS"
BOARD_ID_TTCSPY = "TTCs"
BOARD_ID_PHASEMON = "PHAS"
BOARD_ID_VME = "VME"
BOARD_ID_RF2TTC = "RF2TTC"
BOARD_ID_RFRXD = "RFRXD"
BOARD_ID_RFTXD = "RFTXD"
BOARD_ID_BOBR = "BOBR"

# SFP 'names' (which depend on the FMC type and on the board the FMC
# sits on).
sfp_names = {
    BOARD_ID_LPM : {
        # TCDS FMC 2SFP LEMO.
        FMC_TYPE_1 : {
            1 : "DAQ0",
            2 : "DAQ1"
        },
        # TCDS FMC 8SFP.
        FMC_TYPE_3 : {
            1 : "ICI1",
            2 : "ICI2",
            3 : "ICI3",
            4 : "ICI4",
            5 : "ICI5",
            6 : "ICI6",
            7 : "ICI7",
            8 : "ICI8"
        }
    },
    BOARD_ID_PI : {
        # TCDS FMC 4SFP RJ45.
        FMC_TYPE_2 : {
            1 : "LPM-pri",
            2 : "LPM-sec",
            3 : "FED1",
            4 : "FED2"
        },
        # TCDS FMC 8SFP.
        FMC_TYPE_3 : {
            1 : "FED3",
            2 : "FED4",
            3 : "FED5",
            4 : "FED6",
            5 : "FED7",
            6 : "FED8",
            7 : "FED9",
            8 : "FED10"
        }
    },
    BOARD_ID_PHASEMON : {
        # TCDS FMC 2SFP LEMO.
        FMC_TYPE_1 : {
            1 : "L12-1",
            2 : "L12-2"
        },
        # TCDS FMC 8SFP.
        FMC_TYPE_3 : {
            1 : "L8-1",
            2 : "L8-2",
            3 : "L8-3",
            4 : "L8-4",
            5 : "L8-5",
            6 : "L8-6",
            7 : "L8-7",
            8 : "L8-8"
        }
    }
}

# A map of the number of SFPs on each of the TCDS FMCs.
num_sfps_on_tcds_fmc = {
    FMC_TYPE_1 : 2,
    FMC_TYPE_2 : 4,
    FMC_TYPE_3 : 8
}

# Mapping between SFP numbers (starting at 1, top to bottom, left to
# right) and the MUX bits on the FMCs. Depends on the FMC type, of
# course.
sfp_bit_mapping = {
    # TCDS FMC 2SFP LEMO.
    FMC_TYPE_1 : {
        # SFP_B, MUX bit 1.
        1 : 1,
        # SFP_A, MUX bit 0.
        2 : 0
    },
    # TCDS FMC 4SFP RJ45.
    FMC_TYPE_2 : {
        # SFP_D, MUX bit 3.
        1 : 3,
        # SFP_C, MUX bit 2.
        2 : 2,
        # SFP_B, MUX bit 1.
        3 : 1,
        # SFP_A, MUX bit 0.
        4 : 0
    },
    # TCDS FMC 8SFP.
    FMC_TYPE_3 : {
        # SFP_H, MUX bit 7.
        1 : 7,
        # SFP_G, MUX bit 6.
        2 : 6,
        # SFP_F, MUX bit 5.
        3 : 5,
        # SFP_E, MUX bit 4.
        4 : 4,
        # SFP_D, MUX bit 3.
        5 : 3,
        # SFP_C, MUX bit 2.
        6 : 2,
        # SFP_B, MUX bit 1.
        7 : 1,
        # SFP_A, MUX bit 0.
        8 : 0
    }
}

# Mapping from user logic system-ids to board types.
board_types = {
    # "gold" : BOARD_TYPE_GLIB,
    BOARD_ID_AMC13 : BOARD_TYPE_AMC13,
    BOARD_ID_CPMT1 : BOARD_TYPE_CPMT1,
    BOARD_ID_CPMT2 : BOARD_TYPE_CPMT2,
    BOARD_ID_DUMT1 : BOARD_TYPE_DUMT1,
    BOARD_ID_DUMT2 : BOARD_TYPE_DUMT2,
    BOARD_ID_FC7 : BOARD_TYPE_FC7,
    BOARD_ID_GLIB : BOARD_TYPE_GLIB,
    BOARD_ID_GOLDEN : BOARD_TYPE_GOLDEN,
    BOARD_ID_LPM : BOARD_TYPE_LPM,
    BOARD_ID_PI : BOARD_TYPE_PI,
    BOARD_ID_PHAS : BOARD_TYPE_PHAS,
    BOARD_ID_TTCSPY : BOARD_TYPE_TTCSPY,
    BOARD_ID_PHASEMON : BOARD_TYPE_PHASEMON,
    BOARD_ID_VME : BOARD_TYPE_VME,
    BOARD_ID_RF2TTC : BOARD_TYPE_RF2TTC,
    BOARD_ID_RFRXD : BOARD_TYPE_RFRXD,
    BOARD_ID_RFTXD : BOARD_TYPE_RFTXD,
    BOARD_ID_BOBR : BOARD_TYPE_BOBR
}

board_ids = {
    BOARD_TYPE_AMC13 : BOARD_ID_AMC13,
    BOARD_TYPE_CPMT1 : BOARD_ID_CPMT1,
    BOARD_TYPE_CPMT2 : BOARD_ID_CPMT2,
    BOARD_TYPE_DUMT1 : BOARD_ID_DUMT1,
    BOARD_TYPE_DUMT2 : BOARD_ID_DUMT2,
    BOARD_TYPE_FC7 : BOARD_ID_FC7,
    BOARD_TYPE_GLIB : BOARD_ID_GLIB,
    BOARD_TYPE_GOLDEN : BOARD_ID_GOLDEN,
    BOARD_TYPE_LPM : BOARD_ID_LPM,
    BOARD_TYPE_PI : BOARD_ID_PI,
    BOARD_TYPE_PHAS : BOARD_ID_PHAS,
    BOARD_TYPE_TTCSPY : BOARD_ID_TTCSPY,
    BOARD_TYPE_PHASEMON : BOARD_ID_PHASEMON,
    BOARD_TYPE_VME : BOARD_ID_VME,
    BOARD_TYPE_RF2TTC : BOARD_ID_RF2TTC,
    BOARD_TYPE_RFRXD : BOARD_ID_RFRXD,
    BOARD_TYPE_RFTXD : BOARD_ID_RFTXD,
    BOARD_TYPE_BOBR : BOARD_ID_BOBR
}

# Mapping between FMC module types and FMC names.
fmc_names = {
    FMC_TYPE_UNKNOWN : "unknown",
    FMC_TYPE_EMPTY : "not present",
    FMC_TYPE_1 : "TCDS-FMC-2SFP-LEMO",
    FMC_TYPE_2 : "TCDS-FMC-4SFP-RJ45",
    FMC_TYPE_3 : "TCDS-FMC-8SFP",
    FMC_TYPE_TTC : "TTC FMC"
}

# Mapping between board types and board names.
board_names = {
    BOARD_TYPE_UNKNOWN : "UNKNOWN",
    BOARD_TYPE_BARE_CARRIER : "BARE_CARRIER",
    BOARD_TYPE_AMC13 : "AMC13",
    BOARD_TYPE_CPMT1 : "CPMT1",
    BOARD_TYPE_CPMT2 : "CPMT2",
    BOARD_TYPE_DUMT1 : "DUMMYT1",
    BOARD_TYPE_DUMT2 : "DUMMYT2",
    BOARD_TYPE_FC7 : "FC7",
    BOARD_TYPE_GLIB : "GLIB",
    BOARD_TYPE_GOLDEN : "GOLDEN",
    # BOARD_TYPE_CPM : "CPM",
    BOARD_TYPE_LPM : "LPM",
    BOARD_TYPE_PI : "PI",
    BOARD_TYPE_PHAS : "PhaseMon",
    BOARD_TYPE_TTCSPY : "TTCSpy",
    BOARD_TYPE_PHASEMON : "PhaseMon",
    BOARD_TYPE_VME : "VME",
    BOARD_TYPE_RF2TTC : "RF2TTC",
    BOARD_TYPE_RFRXD : "RFRXD",
    BOARD_TYPE_RFTXD : "RFTXD",
    BOARD_TYPE_BOBR : "BOBR"
}

###############################################################################
