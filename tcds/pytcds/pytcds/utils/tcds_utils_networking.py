###############################################################################
## Utilities to help with IP- and networking-related things.
###############################################################################

import re
import socket

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

def resolve_network_address(network_address):
    """Resolve a string into an IP address or a host name."""

    res = extract_ip_address(network_address)
    if not res:
        res = host_ip(network_address)

    # End of resolve_network_address().
    return res

###############################################################################

def extract_ip_address(ip_address):
    """Extract and return an IP address from a string.

    Returns None if the string does not match an IP-address-like
    pattern.

    """

    ip_matches = re.match(r"^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$", ip_address)
    ip_address = None
    if ip_matches:
        ip_address = ip_matches.group(0)

    return ip_address

###############################################################################

def host_ip(ip_or_name):
    """Turn a host name into an IP address.

    Host names are turned into IP addresses, IP addresses are simply
    returned.

    """

    res = None
    # try:
    res = socket.gethostbyname(ip_or_name)
    # except Exception, err:
    #     pass

    # End of host_ip()
    return res

###############################################################################

def ip_range(ip_lo, ip_hi):
    """Build a range of IP addresses.

    Returns a list of IP addresses [ip_lo, ip_hi]. Some assumptions
    are made, and only works for IPv4.

    """

    if ip_lo > ip_hi:
        (ip_lo, ip_hi) = (ip_hi, ip_lo)

    start = list(map(int, ip_lo.split(".")))
    end = list(map(int, ip_hi.split(".")))
    temp = start
    ip_list = []

    ip_list.append(ip_lo)
    while temp != end:
        start[3] += 1
        for i in (3, 2, 1):
            if temp[i] == 256:
                temp[i] = 0
                temp[i-1] += 1
        ip_list.append(".".join(map(str, temp)))

    # End of ip_range().
    return ip_list

###############################################################################

def parse_ip_arguments(ip_arguments):
    """Parse a list of IP addresses and host name arguments.

    The input is assumed to be a list of arguments representing IP
    addresses and host names, separated by whitespace. Arguments with
    a colon in them are split at the colon and turned into an IP
    address range. At the end everything is combined into a list and
    returned as result.

    """

    # Look for arguments with a colon (':') in them. They specify
    # ranges.
    #ip_addresses = []
    ip_names = {}
    for ip_tmp in ip_arguments:
        ip_tmp_split = ip_tmp.split(":")
        ip_tmp_resolved = []
        for i in ip_tmp_split:
            try:
                ip_address = host_ip(i)
                ip_tmp_resolved.append((ip_address, i))
            except socket.gaierror:
                raise ValueError("Could not resolve '%s'." % i)
        if len(ip_tmp_resolved) < 2:
            ip = ip_tmp_resolved[0][0]
            ip_name = ip_tmp_resolved[0][1]
            # ip_addresses.append(ip)
            ip_names[ip] = ip_name
        elif len(ip_tmp_resolved) == 2:
            (ip_lo, ip_hi) = (ip_tmp_resolved[0][0], ip_tmp_resolved[1][0])
            (ip_name_lo, ip_name_hi) = (ip_tmp_resolved[0][1], ip_tmp_resolved[1][1])
            tmp_range = ip_range(ip_lo, ip_hi)
            # ip_addresses.extend(tmp_range)
            ip_names[ip_lo] = ip_name_lo
            ip_names[ip_hi] = ip_name_hi
            ip_names.update(dict(zip(tmp_range[1:-1], tmp_range[1:-1])))
        else:
            raise ValueError("Did not understand '%s'." % ip_tmp)

    # End of parse_ip_arguments().
    return ip_names

###############################################################################
