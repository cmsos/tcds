###############################################################################

import uhal

import sys
import time

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

# The prescaler value for the I2C bus speed. The I2C clock (kHz) =
# 62500/I2C_PRESCALER.
I2C_PRESCALER = 500

# Various I2C transaction result/status codes.
# NOTE: Any status different from busy and success implies a transaction failure.
# - Transaction not yet finished.
I2C_STATUS_BUSY = 0
# - Transaction finished successfully.
I2C_STATUS_SUCCESS = 1

###############################################################################

class I2CAccessor(object):
    """Helper class to provide read/write access to GLIB/FC7 I2C buses."""

    def __init__(self, device, bus_name, bus_select=0x0, verbose=False):
        self.device = device
        self.bus_name = bus_name
        self.bus_select = bus_select
        self.verbose = verbose
        # End of __init__().

    def __enter__(self):
        self.enable()
        return self
        # End of __enter__().

    def __exit__(self, type, value, traceback):
        self.disable()
        # End of __exit__().

    def enable(self):
        """Enable the I2C bus."""
        reg_base_name = "{0:s}.i2c.settings".format(self.bus_name)
        self.device.write("{0:s}.prescaler".format(reg_base_name), I2C_PRESCALER)
        bus_select_val = 0x0
        if self.bus_select:
            # ASSERT ASSERT ASSERT
            assert self.bus_select == 0x1
            # ASSERT ASSERT ASSERT end
            bus_select_val = self.bus_select
        self.device.write("{0:s}.bus_select".format(reg_base_name), bus_select_val)
        self.device.write("{0:s}.enable".format(reg_base_name), 0x1)
        # End of enable().

    def disable(self):
        """Switch off the I2C bus."""
        reg_base_name = "{0:s}.i2c.settings".format(self.bus_name)
        self.device.write("{0:s}.enable".format(reg_base_name), 0x0)
        # End of disable().

    def i2c(self, slave_addr, is_write, mem_addr, write_data, is_memory, m16b,
            text=None):

        reg_base_name = "{0:s}.i2c".format(self.bus_name)

        # ASSERT ASSERT ASSERT
        # These two together do not work. (Which makes sense, once you
        # understand how the 16-bit transfers work under the hood.
        assert not (m16b and is_memory), \
            "ERROR I2C does not handle transactions " \
            "with both 'm16b' and 'is_memory' set."
        # ASSERT ASSERT ASSERT end

        # Write a low strobe to prepare for the transaction.
        self.device.write("{0:s}.command.strobe".format(reg_base_name), 0x0)

        # Write all command details.
        self.device.write("{0:s}.command.slv_addr".format(reg_base_name), slave_addr)
        self.device.write("{0:s}.command.wr_en".format(reg_base_name), int(is_write))
        self.device.write("{0:s}.command.mem_addr".format(reg_base_name), mem_addr)
        self.device.write("{0:s}.command.wrdata".format(reg_base_name), write_data)
        self.device.write("{0:s}.command.is_mem".format(reg_base_name), int(is_memory))
        self.device.write("{0:s}.command.mode16b".format(reg_base_name), int(m16b))

        # Write a high strobe to trigger the transaction.
        self.device.write("{0:s}.command.strobe".format(reg_base_name), 0x1)

        i2c_status = self.device.read("{0:s}.reply.status".format(reg_base_name))

        # Wait for the I2C transaction to finish. Try at most for one second.
        max_num_tries = 100
        num_tries = 0
        while (i2c_status == I2C_STATUS_BUSY) and \
              (num_tries < max_num_tries):
            i2c_status = self.device.read("{0:s}.reply.status".format(reg_base_name))
            time.sleep(.01)
            num_tries += 1

        data = None
        if i2c_status == I2C_STATUS_SUCCESS:
            if m16b:
                data_lo = self.device.read("{0:s}.reply.rddata_lo".format(reg_base_name))
                data_hi = self.device.read("{0:s}.reply.rddata_hi".format(reg_base_name))
                data = (data_hi << 8) + data_lo
            else:
                data = self.device.read("{0:s}.reply.rddata_lo".format(reg_base_name))
        else:
            if i2c_status == I2C_STATUS_BUSY:
                raise RuntimeError("I2C transaction did not finish.")
            else:
                msg = "The i2c transaction failed (status 0x{0:x})."
                raise RuntimeError(msg.format(i2c_status))

        if self.verbose:
            i2c_action = "?"
            value = "?"
            if is_write:
                i2c_action = "write"
            else:
                i2c_action = "read"
            if i2c_status != I2C_STATUS_SUCCESS:
                value = "failed"
            else:
                if is_write:
                    value = "0x{0:x}".format(write_data)
                else:
                    value = "0x{0:x}".format(data)
            tmp_text = ""
            if text:
                tmp_text = " ({0:s})".format(text)
            full_cmd = self.device.read("{0:s}.command".format(reg_base_name))
            addr_str = "slave address 0x{0:x}".format(slave_addr)
            if is_memory:
                addr_str += ", memory address 0x{0:x}".format(mem_addr)
            msg = "0x{0:08x}: I2C action on bus {1:d} at {2:s}: {3:s} {4:s}{5:s}"
            print msg.format(full_cmd, self.bus_select, addr_str, i2c_action, value, tmp_text)

        # End of i2c()
        return data

    def read_register(self, slave_addr, text=None):
        return self.i2c(slave_addr, False, 0, 0, False, False, text)

    def write_register(self, slave_addr, data, text=None):
        return self.i2c(slave_addr, True, 0, data, False, False, text)

    def read_memory(self, slave_addr, mem_addr, text=None):
        return self.i2c(slave_addr, False, mem_addr, 0, True, False, text)

    def read_memory_16b(self, slave_addr, mem_addr, text=None):
        return self.i2c(slave_addr, False, mem_addr, 0, False, True, text)

    def write_memory(self, slave_addr, mem_addr, data, text=None):
        return self.i2c(slave_addr, True, mem_addr, data, True, False, text)

    def read_string(self, slave_addr, mem_addr, num_chars, text=None):
        # NOTE: This reads from a memory attached to I2C, not from a
        # series of registers.
        chars = []
        for i in xrange(num_chars):
            tmp = self.read_memory(slave_addr, mem_addr + i,
                                   "{0:s} ({1:d} of {2:d})".format(text, (i + 1), num_chars))
            chars.append(unichr(tmp))
        res = "".join(chars).strip()
        # End of read_string().
        return res

    # End of class I2CAccessor.

###############################################################################
