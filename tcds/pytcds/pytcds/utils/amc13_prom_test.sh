TARGET=$1

./tcds_amc13_prom_erase.py ${TARGET}
./tcds_amc13_prom_read.py ${TARGET} | tee ${TARGET}_empty.txt

./tcds_firmware_flash.py ${TARGET} header ~/work_in_progress/cms_tcds/firmware/cms_tcds/fw_images/fw_amc13_cpmt2_header.mcs.gz
./tcds_amc13_prom_read.py ${TARGET} | tee ${TARGET}_header.txt

./tcds_firmware_flash.py ${TARGET} golden ~/cpm_t2_v6_0_0_20181122.mcs
./tcds_amc13_prom_read.py ${TARGET} | tee ${TARGET}_header_golden.txt

./tcds_firmware_flash.py ${TARGET} t2 ~/cpm_t2_v6_0_0_20181122.mcs
./tcds_amc13_prom_read.py ${TARGET} | tee ${TARGET}_header_golden_t2.txt

./tcds_firmware_flash.py ${TARGET} t1 ~/work_in_progress/cms_tcds/firmware/cms_tcds/fw_images/fw_amc13_cpmt1_5_8_1_20170905.mcs.gz
./tcds_amc13_prom_read.py ${TARGET} | tee ${TARGET}_header_golden_t2_t1.txt
