#!/usr/bin/env python

###############################################################################
## Helper class to run multiple 'hack-like' steps as a sequence.
###############################################################################

import datetime
import sys
import time

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class Step(object):

    def __init__(self,
                 description,
                 function):
        self.description = description
        self.function = function
        # End of __init__().

    def run(self):
        print "  timestamp before action: {0:s} UTC".format(datetime.datetime.utcnow().isoformat())
        res = self.function()
        print "  timestamp after action:  {0:s} UTC".format(datetime.datetime.utcnow().isoformat())
        # End of run().
        return res

    # End of class Step.

###############################################################################

class StepSleep(Step):

    def __init__(self, sleep_time):
        description = "Sleeping for {0:d} second(s)...".format(sleep_time)
        function = self.sleep
        super(StepSleep, self).__init__(description, function)
        self.sleep_time = sleep_time
        # End of __init__().

    def sleep(self):
        time.sleep(self.sleep_time)
        # End of sleep().
        return True

    # End of class StepSleep.

###############################################################################

class StepPrintMessage(Step):

    def __init__(self, message):
        description = "Message"
        function = self.print_message
        super(StepPrintMessage, self).__init__(description, function)
        self.message = message
        # End of __init__().

    def print_message(self):
        print self.message
        # End of print_message().
        return True

    # End of class StepPrintMessage.

###############################################################################

class StepWaitForEnter(Step):

    def __init__(self, message=None):
        description = "Waypoint"
        function = self.wait_for_enter
        super(StepWaitForEnter, self).__init__(description, function)
        self.message = message
        # End of __init__().

    def wait_for_enter(self):
        message = self.message
        if message is None:
            message = "Press Enter to continue..."
        res = False
        try:
            raw_input(message)
            res = True
        except KeyboardInterrupt:
            print
        # End of wait_for_enter().
        return res

    # End of class StepWaitForEnter.

###############################################################################

class StepRunner(object):

    def __init__(self, steps):
        self.steps = steps
        self.sep_line = "-" * 80
        # End of __init__().

    def run(self):

        print self.sep_line
        for step in self.steps:
            timestamp = datetime.datetime.utcnow()
            print "{0:s} {1:s}".format(timestamp.isoformat(), step.description)
            res = step.run()
            print self.sep_line
            if not res:
                break

        # End of run().

    # End of class StepRunner.

###############################################################################

if __name__ == "__main__":

    steps = [
        StepSleep(1),
        StepSleep(2),
        StepSleep(3)
        ]

    runner = StepRunner(steps)
    runner.run()

    print "Done"

###############################################################################
