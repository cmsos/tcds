#!/usr/bin/env python

###############################################################################
## A minimal 'RunControl-like' script to help test the TCDS software.
###############################################################################

import os
import sys

from pytcds.utils.tcds_cmd_line_base import CmdLineBase
from pytcds.utils.tcds_utils_soap import build_xdaq_soap_command_message
from pytcds.utils.tcds_utils_soap import extract_xdaq_soap_fault
from pytcds.utils.tcds_utils_soap import send_xdaq_soap_message
from pytcds.utils.tcds_utils_soap import SOAP_PROTOCOL_VERSION_DEFAULT

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class TCDSController(CmdLineBase):

    SOAP_VERSION_CHOICES = ["soap11", "soap12"]

    # The (integer) identifier of the RunControl session 'owning' the
    # TCDS hardware.
    # RC_SESSION_ID = 42
    RC_SESSION_ID = 666
    # The (string) identifier of the RunControl session 'owning' the
    # TCDS hardware.
    # LEASE_OWNER_ID = "TEST_JEROEN"
    LEASE_OWNER_ID = "TEST_JEROEN_MAINTENANCE_MODE"
    # The URL of the RCMS state notification listener. When set to the
    # empty string: nothing is sent.
    # RCMS_URL = "http://pccms87:8080/test_fill_fail"
    RCMS_URL = ""

    COMMAND_PARAMETERS = {
        "Configure" : {
            "hardwareConfigurationString" : ("string", "")
        },
        "Enable" : {
            "runNumber" : ("unsignedInt", int(1.e9))
        },
        "Reconfigure" : {
            "hardwareConfigurationString" : ("string", "")
        },
    }

    # This is the list of allowed FSM commands.
    FSM_COMMANDS = [
        "ColdReset",
        "Configure",
        "Enable",
        "Halt",
        "Pause",
        "Reconfigure",
        "Resume",
        "Stop",
        "Test",
        "TTCHardReset",
        "TTCResync"
        ]

    def __init__(self, description=None, usage=None, epilog=None):
        super(TCDSController, self).__init__(description, usage, epilog)
        # The target and command come from the command line.
        self.command = None
        # The following are read from the config file.
        self.allowed_commands = []
        self.rc_session_id = TCDSController.RC_SESSION_ID
        self.lease_owner_id = TCDSController.LEASE_OWNER_ID
        self.rcms_url = TCDSController.RCMS_URL
        # SOAP version choice between 1.1 and 1.2.
        self.soap_protocol_version = SOAP_PROTOCOL_VERSION_DEFAULT
        self.command_parameters = TCDSController.COMMAND_PARAMETERS
        # End of __init__().

    def setup_parser_custom(self):
        # Start with the basic parser configuration.
        super(TCDSController, self).setup_parser_custom()

        # Add the choice between SOAP protocol v1.1 and v.12.
        help_str = "SOAP protocol version to use. " \
                   "Options: '{0:s}'. " \
                   "[default: '%(default)s']"
        help_str = help_str.format("', '".join(TCDSController.SOAP_VERSION_CHOICES))
        self.parser.add_argument("-s", "--soap-version",
                                 type=str,
                                 action="store",
                                 dest="soap_version",
                                 choices=TCDSController.SOAP_VERSION_CHOICES,
                                 default="soap11",
                                 help=help_str)

        # Add the choice for the primary/secondary TCDS system (for
        # PIControllers only).
        help_str = "Specify primary or secondary TCDS " \
                   "system connection (PIController 'Configure' only)."
        self.parser.add_argument("--use-primary-tcds",
                                 action="store_true",
                                 help=help_str)
        self.parser.add_argument("--use-secondary-tcds",
                                 action="store_true",
                                 help=help_str)

        # Add the FED-vector.
        help_str = "Specify 'FED enable mask' " \
                   "(CPMController, LPMController, and PIController 'Configure' only)."
        self.parser.add_argument("--fed-enable-mask",
                                 action="store",
                                 default=None,
                                 help=help_str)

        # Add the TTC partition map.
        help_str = "Specify 'TTC partition map' " \
                   "(CPMController, LPMController 'Configure' only)."
        self.parser.add_argument("--ttc-partition-map",
                                 action="store",
                                 default=None,
                                 help=help_str)

        # Add the skipPLLReset flag.
        help_str = "Specify skipPLLReset " \
                   "(PIController 'Configure' only)."
        self.parser.add_argument("--skip-pll-reset",
                                 action="store",
                                 default=None,
                                 help=help_str)

        # Add the 'no-beam-active' flag.
        help_str = "Specify 'no-beam-active' " \
                   "(CPMController 'Configure' only)."
        self.parser.add_argument("--no-beam-active",
                                 action="store_true",
                                 help=help_str)
        help_str = "Specify 'beam-active' " \
                   "(CPMController 'Configure' only)."
        self.parser.add_argument("--beam-active",
                                 action="store_true",
                                 help=help_str)

        # Add the run number.
        help_str = "Specify the run number " \
                   "('Enable' only)."
        self.parser.add_argument("--run-number",
                                 action="store",
                                 default=None,
                                 help=help_str)

        # Add the FSM command itself.
        help_str = "The FSM command to execute."
        self.parser.add_argument("fsm_command",
                                 type=str,
                                 action="store",
                                 choices=TCDSController.FSM_COMMANDS,
                                 help=help_str)

        # Add the optional name of the hardware configuration string.
        help_str = "The name of the file containing the " \
                   "hardware configuration string to use " \
                   "for the 'Configure' FSM command."
        self.parser.add_argument("hw_cfg_file",
                                 type=str,
                                 action="store",
                                 nargs='?',
                                 default=None,
                                 help=help_str)

        # End of setup_parser_custom().

    def handle_args(self):
        super(TCDSController, self).handle_args()

        # On the command line we require one target application and
        # one command, and optionally the name of a file containing
        # 'Configure' parameters.
        self.target = self.args.target
        self.command = self.args.fsm_command
        self.run_number = None
        if self.command == "Enable":
            self.run_number = self.args.run_number
        if self.args.hw_cfg_file:
            if self.command in ["Configure", "Reconfigure"]:
                # Replace the hardwareConfigurationString with the
                # contents of the file.
                file_name = self.args.hw_cfg_file
                if self.verbose:
                    msg = "Reading command parameters from file '{0:s}'."
                    print msg.format(file_name)
                params = None
                try:
                    tmp_file = open(file_name, "r")
                    params = "".join(tmp_file.readlines())
                    tmp = self.command_parameters[self.command]
                    tmp["hardwareConfigurationString"] = ("string", params)
                    tmp_file.close()
                except IOError, err:
                    msg = "Could not read command parameters " \
                          "from file '{0:s}': {1:s}."
                    self.error(msg.format(file_name, err))
            else:
                self.error("Only 'Configure' takes a third argument.")

        self.soap_version = self.args.soap_version
        use_primary = self.args.use_primary_tcds
        use_secondary = self.args.use_secondary_tcds
        self.use_primary_tcds = None
        if use_primary:
            self.use_primary_tcds = True
        elif use_secondary:
            self.use_primary_tcds = False

        self.fed_enable_mask = self.args.fed_enable_mask
        self.ttc_partition_map = self.args.ttc_partition_map
        self.skip_pll_reset = self.args.skip_pll_reset

        beam_active = self.args.beam_active
        no_beam_active = self.args.no_beam_active
        self.no_beam_active = None
        if beam_active:
            self.no_beam_active = False
        elif no_beam_active:
            self.no_beam_active = True

        # Add command-line driven SOAP command parameters.
        if not self.run_number is None:
            self.command_parameters[self.command]["runNumber"] = ("unsignedInt", self.run_number)
        if not self.use_primary_tcds is None:
            self.command_parameters[self.command]["usePrimaryTCDS"] = ("boolean", "false")
        if not self.fed_enable_mask is None:
            self.command_parameters[self.command]["fedEnableMask"] = ("string", self.fed_enable_mask)
        if not self.ttc_partition_map is None:
            self.command_parameters[self.command]["ttcPartitionMap"] = ("string", self.ttc_partition_map)
        if self.skip_pll_reset:
            self.command_parameters[self.command]["skipPLLReset"] = ("boolean", self.skip_pll_reset)
        if not self.no_beam_active is None:
            if self.no_beam_active:
                self.command_parameters[self.command]["noBeamActive"] = ("boolean", "true")
            else:
                self.command_parameters[self.command]["noBeamActive"] = ("boolean", "false")

        # End of handle_args().

    def main(self):

        # Check if the target and command names are valid.
        self.validate_target(target_type=CmdLineBase.TARGET_TYPE_SW)

        self.allowed_commands = TCDSController.FSM_COMMANDS
        if not self.command in self.allowed_commands:
            msg = "'{0:s}' is not a valid SOAP command. " \
                  "Valid commands are '{1:s}'."
            self.error(msg.format(self.command,
                                  "', '".join(self.allowed_commands)))

        #----------

        # Now actually do what we have been asked to do: send a SOAP
        # command.
        target_info = self.setup.sw_targets[self.target]
        host_name = target_info.host
        port_number = target_info.port
        lid_number = target_info.lid
        command_name = self.command
        parameters = self.command_parameters.get(command_name, None)
        soap_msg = build_xdaq_soap_command_message(self.rc_session_id,
                                                   self.lease_owner_id,
                                                   command_name,
                                                   self.rcms_url,
                                                   parameters,
                                                   self.soap_version)
        soap_reply = send_xdaq_soap_message(host_name,
                                            port_number,
                                            lid_number,
                                            soap_msg,
                                            soap_protocol_version=self.soap_version,
                                            verbose=self.verbose)
        soap_fault = extract_xdaq_soap_fault(soap_reply)
        if soap_fault:
            msg = "SOAP reply indicates a problem: '{0:s}'"
            self.error(msg.format(soap_fault))

        # End of main().

    # End of class TCDSController.

###############################################################################

if __name__ == "__main__":

    desc_str = "Minimal 'RunControl-like' script for use with the TCDS software."
    # usage_str = "%(prog)s device FSM_COMMAND [options] [hw cfg file]"
    usage_str = None
    # epilog_str_tmp = "Example: '{0:s} 192.168.0.170 Configure hw_cfg_ici.txt' " \
    #                  "will configure the device at IP address 192.168.0.170 " \
    #                  "with the contents of the register dump stored in " \
    #                  "the file hw_cfg_ici.txt."
    # epilog_str = epilog_str_tmp.format(os.path.basename(sys.argv[0]))
    epilog_str = None

    res = TCDSController(desc_str, usage_str, epilog_str).run()

    print "Done"
    sys.exit(res)

###############################################################################
