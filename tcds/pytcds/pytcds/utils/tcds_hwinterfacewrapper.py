###############################################################################
## Utility class related to the uhal library.
###############################################################################

from pytcds.utils.tcds_constants import board_names
from pytcds.utils.tcds_reginfo import RegisterInfo
from pytcds.utils.tcds_utils_misc import name_matches_pattern

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

# Just a little wrapper (for convenience) around the uhal.HwInterface
# class.
class HwInterfaceWrapper(object):

    def __init__(self, carrier_type, board_type, hw_interface, verbose=False):
        self.carrier_type = carrier_type
        self.board_type = board_type
        self.hw_interface = hw_interface
        self.verbose = verbose
        # End of __init__().

    # Pass on everything we don't know to the uhal.HwInterface.
    def __getattr__(self, name):
        res = getattr(self.hw_interface, name)
        # End of __getattr__().
        return res

    def board_type_str(self):
        res = board_names[self.board_type]
        # End of board_type_str().
        return res

    def carrier_type_str(self):
        res = board_names[self.carrier_type]
        # End of carrier_type_str().
        return res

    def create_reg_info_list(self, ignore_list=None, include_list=None, read_values=True):
        if ignore_list is None:
            ignore_list = []
        if include_list is None:
            include_list = []
        res = []
        node_names = self.hw_interface.getNodes()
        for node_name in node_names:
            add_this_node = True
            if ignore_list:
                add_this_node = add_this_node and not name_matches_pattern(node_name, ignore_list)
            if include_list:
                add_this_node = add_this_node and name_matches_pattern(node_name, include_list)
            if add_this_node:
                res.append(RegisterInfo.create_from_uhal_node(node_name,
                                                              self.hw_interface,
                                                              read_values))
        # End of create_reg_info_list().
        return res

    def get_client(self, *args, **kwargs):
        return self.hw_interface.getClient(*args, **kwargs)

    def get_nodes(self, *args, **kwargs):
        return self.hw_interface.getNodes(*args, **kwargs)

    def get_node(self, *args, **kwargs):
        try:
            tmp = self.hw_interface.getNode(*args, **kwargs)
        except Exception, err:
            pdb.set_trace()
        return tmp

    def _read(self, reg_name_or_address, num_words=1):
        # NOTE: A bit of trickery here around the handling of the
        # register 'name-or-address.' Type-checking is not
        # nice/Pythonic, but in this case there is no other way about
        # it.
        res = []
        if num_words == 1:
            if isinstance(reg_name_or_address, basestring):
                # A register name has been specified.
                node = self.hw_interface.getNode(reg_name_or_address)
                tmp_res = node.read()
            else:
                # A register address has been specified.
                client = self.hw_interface.getClient()
                tmp_res = client.read(reg_name_or_address)
            self.hw_interface.dispatch()
            res = [tmp_res.value()]
        elif num_words > 1:
            if isinstance(reg_name_or_address, basestring):
                # A register name has been specified.
                node = self.hw_interface.getNode(reg_name_or_address)
                tmp_res = node.readBlock(num_words)
            else:
                # A register address has been specified.
                client = self.hw_interface.getClient()
                tmp_res = client.readBlock(reg_name_or_address, num_words)
            self.hw_interface.dispatch()
            res = [i for i in tmp_res]
        # End of _read().
        return res

    def read(self, reg_name_or_address):
        return self._read(reg_name_or_address)[0]

    def read_block(self, reg_name_or_address, num_words=None):
        # NOTE: This is a little inconsistent. The variable is called
        # reg_name_or_address, but the assumption is that is specifies
        # a register name and not an address.
        if num_words is None:
            node = self.hw_interface.getNode(reg_name_or_address)
            num_words = node.getSize()
        return self._read(reg_name_or_address, num_words)

    def write(self, reg_name_or_address, values):
        # NOTE: A bit of trickery here around the handling of the
        # register 'name-or-address.' Type-checking is not
        # nice/Pythonic, but in this case there is no other way about
        # it.

        # # BUG BUG BUG
        # tmp = values
        # try:
        #     len(tmp)
        # except TypeError:
        #     tmp = [values]
        # node = self.hw_interface.getNode(reg_name_or_address)
        # print "DEBUG JGH {0:s} writing {1:s} to 0x{2:x} with mask 0x{3:0>8x}.".format(self.board_type_str(), " ".join(["0x{0:x}".format(i) for i in tmp]), node.getAddress(), node.getMask())
        #print "DEBUG JGH {0:s} writing {1:s} to 0x{2:x} ({3:s}).".format(self.board_type_str(), " ".join(["0x{0:x}".format(i) for i in tmp]), node.getAddress(), reg_name_or_address)
        # import pdb
        # if tmp[0] != 0 and reg_name_or_address.find("configuration.enabled") > -1:
        #     pdb.set_trace()
        # BUG BUG BUG end

        num_words = 1
        try:
            num_words = len(values)
        except TypeError:
            values = [values]

        if num_words == 1:
            if isinstance(reg_name_or_address, basestring):
                # A register name has been specified.
                node = self.hw_interface.getNode(reg_name_or_address)
                node.write(values[0])
            else:
                # A register address has been specified.
                client = self.hw_interface.getClient()
                client.write(reg_name_or_address, values[0])
        else:
            if isinstance(reg_name_or_address, basestring):
                # A register name has been specified.
                node = self.hw_interface.getNode(reg_name_or_address)
                node.writeBlock(values)
            else:
                # A register address has been specified.
                client = self.hw_interface.getClient()
                client.writeBlock(reg_name_or_address, values)
        self.hw_interface.dispatch()
        # End of write().

    def write_block(self, reg_name_or_address, values):
        return self.write(reg_name_or_address, values)

    # End of class HwInterfaceWrapper.

###############################################################################
