#!/usr/bin/env python 

###############################################################################
## Script to poll-and-wait for an FSM state change of an XDAQ
## controller.
###############################################################################

import sys
import time

from pytcds.utils.tcds_cmd_line_base import CmdLineBase
from pytcds.utils.tcds_utils_soap import build_xdaq_soap_parameterget_message
from pytcds.utils.tcds_utils_soap import extract_xdaq_parameterget_result
from pytcds.utils.tcds_utils_soap import extract_xdaq_soap_fault
from pytcds.utils.tcds_utils_soap import send_xdaq_soap_message
from pytcds.utils.tcds_utils_soap import SOAP_PROTOCOL_VERSION_DEFAULT

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class FSMPoller(CmdLineBase):

    SOAP_VERSION_CHOICES = ["soap11", "soap12"]
    # The polling interval (in seconds).
    POLL_INTERVAL = 1

    def __init__(self, description=None, usage=None, epilog=None):
        super(FSMPoller, self).__init__(description, usage, epilog)
        # SOAP version choice between 1.1 and 1.2.
        self.soap_protocol_version = SOAP_PROTOCOL_VERSION_DEFAULT
        # These come from the command line.
        self.wait_until = None
        self.wait_while = None
        # End of pre_hook().

    def setup_parser_custom(self):
        # Start with the basic parser configuration.
        super(FSMPoller, self).setup_parser_custom()

        # Add the choice between SOAP protocol v1.1 and v.12.
        help_str = "SOAP protocol version to use. " \
                   "Options: '{0:s}'. " \
                   "[default: '%default']"
        help_str = help_str.format("', '".join(FSMPoller.SOAP_VERSION_CHOICES))
        self.parser.add_argument("-s", "--soap-version",
                                 type=str,
                                 action="store",
                                 dest="soap_version",
                                 choices=FSMPoller.SOAP_VERSION_CHOICES,
                                 default="soap11",
                                 help=help_str)

        # The choices to 'wait-until' or 'wait-while' a certain FSM
        # state.
        help_str = "Wait until the application FSM state matches the given FSM state."
        self.parser.add_argument("--wait-until",
                                 action="store",
                                 help=help_str)
        help_str = "Wait while the application FSM state matches the given FSM state."
        self.parser.add_argument("--wait-while",
                                 action="store",
                                 help=help_str)
        # End of setup_parser_custom().

    def handle_args(self):
        super(FSMPoller, self).handle_args()
        self.soap_version = self.args.soap_version
        self.wait_until = None
        tmp = self.args.wait_until
        if (tmp):
            self.wait_until = [i.strip() for i in tmp.split(",")]
        self.wait_while = None
        tmp = self.args.wait_while
        if (tmp):
            self.wait_while = [i.strip() for i in tmp.split(",")]
        # End of handle_args().

    def main(self):

        # Check if the target and command names are valid.
        self.validate_target(target_type=CmdLineBase.TARGET_TYPE_SW)

        #----------

        # Now actually do what we have been asked to do: poll the
        # application's FSM state and wait.
        fsm_state = self.poll_fsm_state()
        if (self.wait_until is None) and (self.wait_while is None):
            print "FSM state: '{0:s}'.".format(fsm_state)
        else:
            if len(self.wait_until):
                if len(self.wait_until) > 1:
                    wait_until_str = "one of '{0:s}'".format(self.wait_until)
                else:
                    wait_until_str = "'{0:s}'".format(self.wait_until[0])
                while ((not (fsm_state in self.wait_until)) and \
                       (fsm_state != "Failed")):
                    msg = "{0:s}: FSM state = '{1:s}', waiting for {2:s}."
                    print msg.format(self.target, fsm_state, wait_until_str)
                    time.sleep(FSMPoller.POLL_INTERVAL)
                    fsm_state = self.poll_fsm_state()
            elif not self.wait_while is None:
                while ((fsm_state in self.wait_while) and \
                       (fsm_state != "Failed")):
                    msg = "FSM state = '{0:s}', waiting while '{1:s}'."
                    print msg.format(fsm_state, self.wait_while)
                    time.sleep(FSMPoller.POLL_INTERVAL)
                    fsm_state = self.poll_fsm_state()
            msg = "{0:s}: FSM state = '{1:s}'."
            print msg.format(self.target, fsm_state)
        # End of main().

    def poll_fsm_state(self):
        target_info = self.setup.sw_targets[self.target]
        host_name = target_info.host
        port_number = target_info.port
        lid_number = target_info.lid
        class_name = target_info.app_class
        soap_msg = build_xdaq_soap_parameterget_message(class_name,
                                                        "stateName",
                                                        "string",
                                                        self.soap_version)
        soap_reply = send_xdaq_soap_message(host_name,
                                            port_number,
                                            lid_number,
                                            soap_msg,
                                            soap_protocol_version=self.soap_version,
                                            verbose=self.verbose)
        soap_fault = extract_xdaq_soap_fault(soap_reply)
        fsm_state = None
        if soap_fault:
            msg = "SOAP reply indicates a problem: '{0:s}'"
            print >> sys.stderr, msg.format(soap_fault)
        else:
            # Process the result.
            fsm_state = extract_xdaq_parameterget_result(soap_reply, "stateName")
        # End of poll_fsm_state().
        return fsm_state

    # End of class FSMPoller.

###############################################################################

if __name__ == "__main__":

    desc_str = "Script to poll-and-wait for an FSM state change of an XDAQ controller."
    usage_str = "usage: %prog device [options]"

    res = FSMPoller(desc_str, usage_str).run()

    print "Done"
    sys.exit(res)

###############################################################################
