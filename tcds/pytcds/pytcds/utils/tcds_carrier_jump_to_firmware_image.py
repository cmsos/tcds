#!/usr/bin/env python

###############################################################################
## Utility to make a device specified by its IP address jump between
## the 'golden' and 'user' firmware images.
###############################################################################

import sys

import uhal

from pytcds.utils.tcds_cmd_line_base import CmdLineBase
from pytcds.utils.tcds_utils_hw_connect import get_carrier_hw
from pytcds.utils.tcds_utils_networking import resolve_network_address

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class FirmwareJumper(CmdLineBase):

    IMAGE_CHOICES = ["golden", "user", "test", "t1", "t2"]

    def __init__(self, description=None, usage=None, epilog=None):
        super(FirmwareJumper, self).__init__(description, usage, epilog)
        self.image_name = None
        # End of __init__().

    def setup_parser_custom(self):
        super(FirmwareJumper, self).setup_parser_custom()
        parser = self.parser

        # Add the image name argument.
        help_str = "Name of the firmware image to load"
        parser.add_argument("image_name",
                            type=str,
                            choices=FirmwareJumper.IMAGE_CHOICES,
                            help=help_str)
        # End of setup_parser_custom().

    def handle_args(self):
        super(FirmwareJumper, self).handle_args()
        # Extract the firmware image name.
        self.image_name = self.args.image_name
        # End of handle_args().

    def main(self):
        self.validate_target(target_type=CmdLineBase.TARGET_TYPE_HW)
        board_info = self.setup.hw_targets[self.target]
        network_address = board_info.dns_alias
        controlhub_address = board_info.controlhub
        carrier = get_carrier_hw(network_address, controlhub_address, verbose=self.verbose)
        if not carrier:
            self.error("Could not connect to {0:s}.".format(self.ip_address))

        # FC7-based targets and AMC13-based targets need different
        # treatments.
        if board_info.type == 'cpm-t1':
            msg = "AMC13-based devices are programmed via the T2, not the T1"
            raise ValueError(msg)
        elif board_info.type == 'cpm-t2':
            # NOTE: In this case the image_name really is the name of
            # the tongue to be rebooted (i.e. t1 or t2).
            if not self.image_name in ['t1', 't2']:
                msg = "AMC13-based devices only accept 't1' or 't2' as image choice"
                raise ValueError(msg)
        else:
            if not self.image_name in ['golden', 'user', 'test']:
                msg = "FC7-based devices only accept 'golden', 'user', or 'test' as image choice"
                raise ValueError(msg)

        if self.verbose:
            print "Jumping to {0:s} firmware image.".format(self.image_name)

        # We should anticipate some problems, like the board becoming
        # unreachable... Other problems should still pass through. For
        # the same reasons we want to disable logging here.
        uhal.disableLogging()
        try:
            carrier.load_firmware(self.image_name)
        except Exception as err:
            if err.message.find("no reply to status packet") < 0:
                raise
        # End of main().

    # End of class FirmwareJumper.

###############################################################################

if __name__ == "__main__":

    desc_str = "Helper script to make a TCDS hardware device " \
               "specified by its target name jump " \
               "between its 'golden and 'user' (and 'test') firmware images."
    # usage_str = "usage: %prog [OPTIONS] TARGET IMAGE_NAME"
    usage_str = None
    tmp = "', '".join(FirmwareJumper.IMAGE_CHOICES)
    epilog_str = "IMAGE_NAME: '{0:s}'.".format(tmp)

    res = FirmwareJumper(desc_str, usage_str, epilog_str).run()

    print "Done"
    sys.exit(res)

###############################################################################
