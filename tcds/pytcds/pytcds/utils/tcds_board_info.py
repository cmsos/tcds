#!/usr/bin/env python

###############################################################################
## Utility to dump some basic board information about a device
## specified by its IP address. Assuming, of course, that the device
## is something we recognize.
###############################################################################

import optparse
import re
import sys
import time

import uhal

from pytcds.utils.tcds_cmd_line_base import CmdLineBase
from pytcds.utils.tcds_constants import BOARD_ID_CPMT2
from pytcds.utils.tcds_constants import BOARD_ID_FC7
from pytcds.utils.tcds_constants import BOARD_ID_GLIB
from pytcds.utils.tcds_constants import BOARD_ID_LPM
from pytcds.utils.tcds_constants import BOARD_ID_PHASEMON
from pytcds.utils.tcds_constants import BOARD_ID_PI
from pytcds.utils.tcds_constants import BOARD_ID_TTCSPY
from pytcds.utils.tcds_constants import FMC_TYPE_UNKNOWN
from pytcds.utils.tcds_constants import fmc_names
from pytcds.utils.tcds_constants import num_sfps_on_tcds_fmc
from pytcds.utils.tcds_constants import sfp_names
from pytcds.utils.tcds_utils_hw_connect import get_carrier_hw
from pytcds.utils.tcds_utils_misc import format_eui
from pytcds.utils.tcds_utils_misc import format_ip_address
from pytcds.utils.tcds_utils_misc import format_mac_address

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class BoardInfoDumper(CmdLineBase):

    def __init__(self, description=None, usage=None, epilog=None):
        super(BoardInfoDumper, self).__init__(description, usage, epilog)
        self.brief = False
        # End of __init__().

    def setup_parser_custom(self):
        super(BoardInfoDumper, self).setup_parser_custom()
        # Add the --brief option.
        parser = self.parser
        parser.add_argument("-b", "--brief",
                            action="store_true",
                            dest="brief",
                            default=False,
                            help="Be brief")
        # End of setup_parser_custom().

    def handle_args(self):
        super(BoardInfoDumper, self).handle_args()
        self.brief = self.args.brief
        # End of handle_args().

    def main(self):
        # Now the assumption is, of course, that this is a
        # 'amc-rack-crate-slot' kinda DNS alias, or a proper uhal
        # board/connection name.
        is_valid_target = self.is_valid_target(target_type=CmdLineBase.TARGET_TYPE_HW)
        if is_valid_target:
            board_info = self.setup.hw_targets[self.target]
            network_address = board_info.address
            controlhub_address = board_info.controlhub
        else:
            str_base = "^amc-(s[0-9]{1}[a-z]{1}[0-9]{2})-([0-9]{2})"
            str_0 = str_base + "-(0[1-9]|1[0-2])$"
            str_1 = str_base + "-(0[1-9]|1[0-3])-(t[12])$"
            regex_0 = re.compile(str_0)
            regex_1 = re.compile(str_1)
            match = regex_0.match(self.target) or regex_1.match(self.target)
            is_valid_alias = not match is None
            if is_valid_alias:
                network_address = self.target
                tmp = network_address.split("-")
                rack = tmp[1]
                crate = tmp[2]
                controlhub_address = 'bridge-{0:s}-{1:s}'.format(rack, crate)
            else:
                msg = "It appears that '{0:s}' is neither a valid uhal target nor a valid DNS alias"
                self.error(msg.format(self.target))
        carrier = get_carrier_hw(network_address, controlhub_address, verbose=self.verbose)
        if not carrier:
            self.error("Could not connect to {0:s}.".format(self.target))

        print self.sep_line
        print "Board info for '{0:s}'".format(self.target)
        print self.sep_line
        board_id = carrier.get_board_id()
        print "  Board id:  {0:s}".format(board_id)
        print "  System id: {0:s}".format(carrier.get_system_id())
        user_system_id = carrier.get_user_system_id()
        print "  User system id: {0:s}".format(user_system_id)

        # BUG BUG BUG
        # This needs a little bit of cleanup.
        if True:
        # BUG BUG BUG end
           print "  System firmware version: {0:s}".format(carrier.get_firmware_version())
           print "  System firmware date:    {0:s}".format(carrier.get_firmware_date())
           if carrier.is_user_firmware_loaded():
               print "  User firmware version: {0:s}".format(carrier.get_firmware_version("user"))
               print "  User firmware date:    {0:s}".format(carrier.get_firmware_date("user"))

           # The GLIB and FC7 have a EUI-48, the AMC13 does not (but
           # the T2 has a serial number). The same holds for a lot of
           # this information.
           if user_system_id == BOARD_ID_CPMT2:
               print "  AMC13 T2 serial number: {0:d}".format(carrier.get_serial_number())
           if board_id in [BOARD_ID_FC7, BOARD_ID_GLIB]:
               print "  EUI-48: {0:s}".format(format_eui(carrier.get_eui()))
               print "  MAC-address: {0:s}".format(format_mac_address(carrier.get_mac_address()))

               if not self.brief:

                   # # Now for some FC7-specific things.
                   # if board_id == BOARD_ID_FC7:
                   #     carrier.read_voltages()

                   # Now for some FC7-specific things.
                   if board_id == BOARD_ID_FC7:
                       print
                       # NOTE: Voltages are read via I2C, using the
                       # system I2C bus of the FC7.
                       fmc_voltages = carrier.read_fmc_supply_voltages()
                       print "  FMC supply voltages:"
                       keys_fmc = fmc_voltages.keys()
                       keys_fmc.sort()
                       for fmc in keys_fmc:
                           print "    {0:s}".format(fmc.upper())
                           keys_voltages = fmc_voltages[fmc].keys()
                           keys_voltages.sort()
                           max_len = max([len(i) for i in keys_voltages])
                           for voltage in keys_voltages:
                               meas = fmc_voltages[fmc][voltage]
                               print "      {0:{1}s}: {2:.2f} V".format(voltage, max_len, meas)

                   print

                   # Dump per-FMC info.
                   # NOTE: These info are read via I2C, using the user
                   # I2C bus of the FC7/GLIB.

                   # NOTE: The LPM and PI have an I2C player that
                   # needs to be disabled in order to access the user
                   # I2C bus.
                   i2c_player_enabled = False
                   if user_system_id in [BOARD_ID_LPM, BOARD_ID_PI, BOARD_ID_PHASEMON]:
                       i2c_player_enabled = (carrier.read("user.i2c_player_ctrl.enable") == 0x1)
                       carrier.write("user.i2c_player_ctrl.enable", 0)
                       # carrier.write("user.i2c.settings.enable", 0)
                       i2c_settings = carrier.read("user.i2c.settings")
                       time.sleep(1)

                   fmc_locations = carrier.get_fmc_locations()

                   for fmc in fmc_locations:

                       fmc_present = carrier.is_fmc_present(fmc)
                       fmc_powered = False
                       presence_msg = "?"
                       powered_msg = "?"
                       if not fmc_present:
                           presence_msg = " not present"
                           powered_msg = ""
                       else:
                           presence_msg = " present"
                           fmc_powered = carrier.is_fmc_powered(fmc)
                           if fmc_powered:
                               powered_msg = " and powered"
                           else:
                               powered_msg = " but not powered"
                       print "  FMC {0:s}:{1:s}{2:s}".format(fmc, presence_msg, powered_msg)

                       # Check for the TCDS FMC type mounted on this FMC site.
                       # NOTE: Of course only works for TCDS FMCs. So skip
                       # e.g., the TTCSpy.
                       if user_system_id == BOARD_ID_TTCSPY:
                           print "    nothing more to report for the TTCSpy"
                           continue

                       # # NOTE: The PhaseMon does not have a TCDS FMC
                       # # (but a TTC FMC) in the L12 position.
                       # if (user_system_id == BOARD_ID_PHASEMON) and \
                       #    (fmc == "l12"):
                       #     print "    nothing more to report for the PhaseMon"
                       #     continue

                       if fmc_present and fmc_powered:

                           # The following only works for TCDS FMCs. The
                           # condition above will protect us from trying this for
                           # non-TCDS boards with TCDS firmware loaded. If the
                           # golden image is loaded, however, the following may
                           # fail.
                           identify_success = False
                           try:
                               # FMC module type.
                               fmc_type = carrier.read_fmc_type(fmc)
                               print "    type: {0:s}".format(fmc_names[fmc_type])
                               identify_success = (fmc_type != FMC_TYPE_UNKNOWN)

                           except RuntimeError, err:
                               msg = "    Could not identify the FMC " \
                                     "(which is normal for non-TCDS FMCs " \
                                     "or when on the golden firmware image)."
                               print msg

                           if identify_success:
                               try:
                                   # SFP status for all SFPs on this FMC.
                                   print "    SFP status:"
                                   for sfp_num in xrange(1, num_sfps_on_tcds_fmc[fmc_type] + 1):
                                       sfp_id = sfp_names[user_system_id][fmc_type][sfp_num]
                                       sfp_status_str = ""
                                       sfp_status = carrier.read_sfp_status(fmc, sfp_num)
                                       mod_abs = sfp_status["mod_abs"]
                                       rx_los = sfp_status["rx_los"]
                                       tx_dis = sfp_status["tx_dis"]
                                       tx_fault = sfp_status["tx_fault"]
                                       if mod_abs:
                                           sfp_status_str = "not present"
                                       else:
                                           tmp = []
                                           if tx_dis:
                                               tmp.append("TX disabled")
                                           else:
                                               tmp.append("TX enabled")
                                           tmp.append("RX lost: {0:d}".format(rx_los))
                                           tmp.append("TX fault: {0:d}".format(tx_fault))
                                           sfp_status_str = ", ".join(tmp)
                                       print "      {0:s}: {1:s}".format(sfp_id, sfp_status_str)

                                       if self.verbose:
                                           if not mod_abs:
                                               vendor_info = carrier.read_sfp_vendor_info(fmc, sfp_num)
                                               for (key, val) in vendor_info.iteritems():
                                                   print "          {0:s}: '{1:s}'".format(key, val)
                                               diagnostic_info = carrier.read_sfp_diagnostic_info(fmc, sfp_num)
                                               for (key, val) in diagnostic_info.iteritems():
                                                   print "          {0:s}: '{1:.1f}'".format(key, val)

                               except RuntimeError, err:
                                   msg = "    Could not obtain (some of) " \
                                         "this FMC's SFP information :'{0:s}'."
                                   print msg.format(err)

                   if i2c_player_enabled:
                       carrier.write("user.i2c.settings", i2c_settings)
                       # carrier.write("user.i2c.settings.enable", 1)
                       carrier.write("user.i2c_player_ctrl.enable", 1)

        print self.sep_line

    # End of class BoardInfoDumper.

###############################################################################

if __name__ == "__main__":

    desc_str = "Helper script to dump some basic board info " \
        "of a TCDS IPbus device specified by its IP address."

    res = BoardInfoDumper(desc_str).run()

    print "Done"
    sys.exit(res)

###############################################################################
