###############################################################################
## Utilities related to pre-LS1 VME boards.
##
## NOTE: This is a bit of a hack, but it was the easiest way to
## support VME boards in addition to uTCA boards.
###############################################################################

import pyvme
from pytcds.utils.tcds_constants import board_names
from pytcds.utils.tcds_constants import BOARD_ID_VME
from pytcds.utils.tcds_constants import BOARD_TYPE_VME
from pytcds.utils.tcds_reginfo import RegisterInfo
from pytcds.utils.tcds_utils_misc import name_matches_pattern

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class VME(object):

    def __init__(self, board_type, vme_chain, vme_unit, vme_slot, base_address, nodes, verbose=False):
        self.carrier_type = BOARD_TYPE_VME
        self.board_type = board_type
        self.vme_chain = vme_chain
        self.vme_unit = vme_unit
        self.slot_number = vme_slot
        self.base_address = base_address
        self.nodes = nodes
        self.verbose = verbose
        # End of __init__().

    def board_type_str(self):
        res = board_names[self.board_type]
        # End of board_type_str().
        return res

    def carrier_type_str(self):
        res = board_names[self.carrier_type]
        # End of carrier_type_str().
        return res

    def uri(self):
        res = "chain {0:d}, unit {1:d}, slot #{2:d}".format(self.vme_chain, self.vme_unit, self.slot_number)
        # End of uri().
        return res

    def get_board_id(self):
        res = BOARD_ID_VME
        # End of get_board_id().
        return res

    def get_system_id(self):
        res = board_names[self.board_type]
        # End of get_system_id().
        return res

    def get_user_system_id(self):
        res = self.get_system_id()
        # End of get_user_system_id().
        return res

    def get_firmware_version(self, which=None):
        # NOTE: The legacy VME boards don't have separate system and
        # user firmware images.
        raise NotImplementedError("VME.get_firmware_version() is not implemented.")
        # End of get_firmware_version()
        return None

    def get_firmware_date(self, which=None):
        # NOTE: The legacy VME boards don't have separate system and
        # user firmware images.
        raise NotImplementedError("VME.get_firmware_date() is not implemented.")
        # End of get_firmware_date()
        return None

    def is_user_firmware_loaded(self):
        # NOTE: The legacy VME boards don't have separate system and
        # user firmware images.
        # End of is_user_firmware_loaded().
        return True

    def get_nodes(self):
        res = self.nodes.keys()
        # End of get_nodes().
        return res

    def get_node(self, node_name):
        res = self.nodes[node_name]
        # End of get_node().
        return res

    def create_reg_info_list(self, ignore_list=None, include_list=None, read_values=True):
        if ignore_list is None:
            ignore_list = []
        if include_list is None:
            include_list = []
        res = []
        node_names = self.nodes.keys()
        for node_name in node_names:
            add_this_node = True
            if ignore_list:
                add_this_node = not name_matches_pattern(node_name, ignore_list)
            if include_list:
                add_this_node = add_this_node and name_matches_pattern(node_name, include_list)
            if add_this_node:
                res.append(RegisterInfo.create_from_hal_node(node_name, self, read_values))
        # End of create_reg_info_list().
        return res

    def _read(self, reg_name_or_address, num_words=1):
        # NOTE: A bit of trickery here around the handling of the
        # register 'name-or-address.' Type-checking is not
        # nice/Pythonic, but in this case there is no other way about
        # it.
        res = []
        if num_words == 1:
            address = None
            mask = None
            if isinstance(reg_name_or_address, basestring):
                # A register name has been specified.
                node = self.get_node(reg_name_or_address)
                address = node.address
                address_modifier = node.address_modifier
                width = node.width
                mask = node.mask
            else:
                # A register address has been specified.
                address = reg_name_or_address
                # In this case: no clue what to use as address
                # modifier or width, so let's pick some
                # hopefully-adequate values.
                address_modifier = 0x09
                width = 1
                # NOTE: In this case there is no way to perform a
                # masked read.
                mask = None
            bus = pyvme.VME(self.vme_chain, self.vme_unit)
            data = bus.read_register(self.base_address + address,
                                     address_modifier,
                                     width)
            if not mask is None:
                data &= mask
                helper = mask
                while ((helper & 0x1) == 0x0):
                    data >>= 1
                    helper >>= 1
            res = [data]
        else:
            pdb.set_trace()
        #     if isinstance(reg_name_or_address, basestring):
        #         # A register name has been specified.
        #         node = self.hw_interface.getNode(reg_name_or_address)
        #         tmp_res = node.readBlock(num_words)
        #     else:
        #         # A register address has been specified.
        #         client = self.hw_interface.getClient()
        #         tmp_res = client.readBlock(reg_name_or_address, num_words)
        #     self.hw_interface.dispatch()
        #     res = [i for i in tmp_res]
        # End of _read().
        return res

    def read(self, reg_name_or_address):
        return self._read(reg_name_or_address)[0]

    def write(self, reg_name_or_address, values):
        # NOTE: A bit of trickery here around the handling of the
        # register 'name-or-address.' Type-checking is not
        # nice/Pythonic, but in this case there is no other way about
        # it.

        num_words = 1
        try:
            num_words = len(values)
        except TypeError:
            pass
        if num_words == 1:
            address = None
            mask = None
            if isinstance(reg_name_or_address, basestring):
                # A register name has been specified.
                node = self.get_node(reg_name_or_address)
                address = node.address
                address_modifier = node.address_modifier
                width = node.width
                mask = node.mask
            else:
                # A register address has been specified.
                address = reg_name_or_address
                # In this case: no clue what to use as address
                # modifier or width, so let's pick some
                # hopefully-adequate values.
                address_modifier = 0x09
                width = 1
                # In this case: no clue what to use as mask. So let's
                # not use a mask at all.
                mask = None
            data = values
            if not mask is None:
                helper = mask
                while ((helper & 0x1) == 0x0):
                    data <<= 1
                    helper >>= 1
                data &= mask
            bus = pyvme.VME(self.vme_chain, self.vme_unit)
            # Now do read-modify-write.
            ori_data = bus.read_register(self.base_address + address,
                                         address_modifier,
                                         width)
            part0 = ori_data & ~mask
            part1 = data & mask
            new_data = part0 | part1
            bus.write_register(self.base_address + address,
                               address_modifier,
                               width,
                               new_data)
        else:
            import pdb
            pdb.set_trace()
        #     if isinstance(reg_name_or_address, basestring):
        #         # A register name has been specified.
        #         node = self.hw_interface.getNode(reg_name_or_address)
        #         node.writeBlock(values)
        #     else:
        #         # A register address has been specified.
        #         client = self.hw_interface.getClient()
        #         client.writeBlock(reg_name_or_address, values)
        # End of write().

    # End of class VME.

###############################################################################

class TCDSVME(VME):

    def __init__(self, board_type, vme_chain, vme_unit, vme_slot, base_address, nodes, verbose=False):
        super(TCDSVME, self).__init__(board_type, vme_chain, vme_unit, vme_slot, base_address, nodes, verbose)
        # End of __init__().

    # End of class TCDSVME.

###############################################################################
