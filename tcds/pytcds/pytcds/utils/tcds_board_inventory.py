#!/usr/bin/env python

###############################################################################
## Utility to gather and dump an inventory of the boards in a series
## of racks and/or crates.
###############################################################################

import commands
import re
import sys

from pytcds.utils.tcds_cmd_line_base import CmdLineBase
from pytcds.utils.tcds_constants import BOARD_ID_CPMT1
from pytcds.utils.tcds_constants import BOARD_ID_CPMT2
from pytcds.utils.tcds_constants import BOARD_ID_DUMT1
from pytcds.utils.tcds_constants import BOARD_ID_DUMT2
from pytcds.utils.tcds_constants import BOARD_ID_GOLDEN
from pytcds.utils.tcds_constants import BOARD_ID_VME

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

# Some constants.

# # At P5, TCDS boards are present in racks S1E02 and S1E03.
# RACK_NAMES = {
#     'tcdsp5' : ['S1E02', 'S1E03'],
#     'tcdslab' : ['LAB']
#     }

# # Crate names of the crates, per rack.
# CRATE_NAMES = {
#     'S1E02' : ['10', '18', '27', '36'],
#     'S1E03' : ['10', '18', '27', '36'],
#     'LAB' : ['TOP']
#     }

AMC_NUM_MIN = 1
AMC_NUM_MAX = 12
AMC_SUFFIXES = ["{0:02d}".format(i) for i in xrange(AMC_NUM_MIN, AMC_NUM_MAX + 1)]
AMC_SUFFIXES.extend(["13-t1", "13-t2"])

REGEX_BOARD_ID = re.compile("Board id:\s*([^\s]*)")
REGEX_BOARD_TYPE = re.compile("User system id:\s*([^\s]*)")
REGEX_EUI48 = re.compile("EUI-48:\s*([\dA-F]{2}-[\dA-F]{2}-[\dA-F]{2}-[\dA-F]{2}-[\dA-F]{2}-[\dA-F]{2})",
                         re.IGNORECASE)
REGEX_MAC = re.compile("MAC-address:\s*([\dA-F]{2}:[\dA-F]{2}:[\dA-F]{2}:[\dA-F]{2}:[\dA-F]{2}:[\dA-F]{2})",
                       re.IGNORECASE)
REGEX_SYSTEM_VERSION = re.compile("System firmware version:\s*([^\s]*)")
REGEX_USER_VERSION = re.compile("User firmware version:\s*([^\s]*)")
REGEX_CPM_T2_SERIAL = re.compile("AMC13 T2 serial number:\s*([^\s]*)")

###############################################################################

class BoardInfo(object):

    def __init__(self, amc_name, board_info_output, aliases=None):
        self._amc_name = amc_name
        self._raw_output = board_info_output
        self._aliases = []
        if aliases:
            self._aliases = aliases
        # End of __init__().

    def __lt__(self, other):
        res = self.amc_name() < other.amc_name()
        # End of __lt__().
        return res

    def is_board_present(self):
        msg0 = "ERROR - Timeout (1000 milliseconds) occurred for UDP receive from target"
        msg1 = "The ControlHub did not receive any response"
        res0 = (self._raw_output.find(msg0) < 0)
        res1 = (self._raw_output.find(msg1) < 0)
        # End of is_board_present().
        return (res0 and res1)

    def amc_name(self):
        res = self._amc_name
        # End of amc_name().
        return res

    def aliases(self):
        res = self._aliases
        # End of aliases().
        return res

    def rack(self):
        res = self._amc_name.split("-")[1]
        # End of rack().
        return res

    def crate_number(self):
        res = self._amc_name.split("-")[2]
        # End of crate_number().
        return res

    def slot_number(self):
        res = self._amc_name.split("-")[3]
        # End of slot_number().
        return res

    def board_id(self):
        match_board_id = re.search(REGEX_BOARD_ID, self._raw_output)
        try:
            res = match_board_id.group(1)
        except Exception, err:
            import pdb
            pdb.set_trace()
        # End of board_id().
        return res

    def board_type(self):
        match_board_type = re.search(REGEX_BOARD_TYPE, self._raw_output)
        try:
            res = match_board_type.group(1)
        except Exception, err:
            import pdb
            pdb.set_trace()
        # End of board_type().
        return res

    def eui48(self):
        res = "n/a"
        if (not self.board_type() in [BOARD_ID_CPMT1, BOARD_ID_CPMT2,
                                      BOARD_ID_DUMT1, BOARD_ID_DUMT2]):
            # This only works for GLIBs and FC7s.
            match_eui48 = re.search(REGEX_EUI48, self._raw_output)
            try:
                res = match_eui48.group(1)
            except Exception, err:
                import pdb
                pdb.set_trace()
        # End of eui48().
        return res

    def mac_address(self):
        res = "n/a"
        if (not self.board_type() in [BOARD_ID_CPMT1, BOARD_ID_CPMT2,
                                      BOARD_ID_DUMT1, BOARD_ID_DUMT2]):
            # This only works for GLIBs and FC7s.
            match_mac = re.search(REGEX_MAC, self._raw_output)
            try:
                res = match_mac.group(1)
            except Exception, err:
                import pdb
                pdb.set_trace()
        # End of mac_address().
        return res

    def serial_number(self):
        res = "n/a"
        # This does not work for GLIBs and FC7s, and for the AMC13s
        # the T2 carries the serial number of the full assembly.
        if self.board_type() == BOARD_ID_CPMT2:
            match_serial = re.search(REGEX_CPM_T2_SERIAL, self._raw_output)
            try:
                res = match_serial.group(1)
            except Exception, err:
                import pdb
                pdb.set_trace()
        # End of serial_number().
        return res

    def system_firmware_version(self):
        match_version = re.search(REGEX_SYSTEM_VERSION, self._raw_output)
        try:
            res = match_version.group(1)
        except Exception, err:
            import pdb
            pdb.set_trace()
        # End of system_firmware_version().
        return res

    def user_firmware_version(self):
        # NOTE: If we're on the golden image, there is no user
        # firmware version. Same case for the CPM.
        res = "?"
        board_type = self.board_type()
        if not board_type in [BOARD_ID_GOLDEN]:
            match_version = re.search(REGEX_USER_VERSION, self._raw_output)
            try:
                res = match_version.group(1)
            except Exception, err:
                import pdb
                pdb.set_trace()
        # End of user_firmware_version().
        return res

    # End of class BoardInfo.

###############################################################################

class BoardInventory(CmdLineBase):

    OUTPUT_FORMAT_CHOICES = ["txt", "csv"]

    def __init__(self, description=None, usage=None, epilog=None):
        super(BoardInventory, self).__init__(description, usage, epilog)
        self.output_format = None
        # End of __init__().

    def setup_parser_custom(self):
        # Add the choice of the output format.
        help_str = "Output format to use. " \
                   "Options: '{0:s}'. " \
                   "[default: '%(default)s']"
        help_str = help_str.format("', '".join(BoardInventory.OUTPUT_FORMAT_CHOICES))
        parser = self.parser
        parser.add_argument("--format",
                            type=str,
                            action="store",
                            dest="output_format",
                            choices=BoardInventory.OUTPUT_FORMAT_CHOICES,
                            default="txt",
                            help=help_str)
        # End of setup_parser_custom().

    def handle_args(self):
        super(BoardInventory, self).handle_args()
        self.output_format = self.args.output_format
        # End of handle_args().

    def main(self):

        # Scan all known boards.
        print "Scanning all known VME and uTCA boards in the '{0:s}' TCDS setup".format(self.setup.identifier)
        results_known_utca = {}
        results_known_vme = {}
        boards = self.setup.boards
        for board in sorted(boards, key=lambda i: i.identifier):
            identifier = board.identifier
            if self.verbose:
                print "    {0:s}".format(identifier)

            board_info = self.get_board_info(identifier)
            if board.crate_type == "uTCA":
                results_known_utca[identifier] = board_info
            else:
                results_known_vme[identifier] = board_info

        # Now scan 'unspecified' slots in known racks/crates. Just in
        # case someone has plugged in something new/spare/etc.
        print "Scanning for 'unknown' uTCA boards in known racks/crates"
        results_unknown_boards = {}
        known_amc_names = [i.address for i in boards]
        racks = self.setup.racks
        crates = self.setup.utca_crates
        for rack in sorted(racks):
            if self.verbose:
                print "  rack {0:s}".format(rack)

            tmp = [i[1] for i in crates if i[0] == rack]
            for crate in sorted(tmp):
                if self.verbose:
                    print "    crate {0:s}".format(crate)

                for amc_suffix in AMC_SUFFIXES:
                    amc_name = "amc-{0:s}-{1:s}-{2:s}".format(rack, crate, amc_suffix).lower()
                    if not amc_name in known_amc_names:
                        if self.verbose:
                            print "      {0:s}".format(amc_name)
                        board_info = self.get_board_info(amc_name)
                        # Only keep track of newly found boards.
                        if board_info.is_board_present():
                            results_unknown_boards[amc_name] = board_info

        # Organise and print all results.
        print self.sep_line
        print "Known VME boards:"
        print self.sep_line
        self.print_results(results_known_vme)

        print self.sep_line
        print "Known uTCA boards:"
        print self.sep_line
        self.print_results(results_known_utca)

        print self.sep_line
        print "Unknown uTCA boards in known racks/crates:"
        print self.sep_line
        self.print_results(results_unknown_boards)
        # End of main().

    def print_results(self, results):
        table_data = []
        for board_info in sorted(results.values()):
            amc_name = board_info.amc_name()
            aliases = board_info.aliases()
            alias_str = ""
            if aliases:
                alias_str = " (" + ", ".join(aliases) + ")"
            comment = ""
            board_id = "?"
            board_type = "?"
            eui48 = "?"
            mac_address = "?"
            serial_number = "?"
            system_fw = "?"
            user_fw = "?"
            if board_info.is_board_present():
                board_id = board_info.board_id()
                board_type = board_info.board_type()
                if (not board_info.board_id() == BOARD_ID_VME):
                    eui48 = board_info.eui48()
                    mac_address = board_info.mac_address()
                    serial_number = board_info.serial_number()
                system_fw = board_info.system_firmware_version()
                user_fw = board_info.user_firmware_version()
            else:
                comment = "No board present (or board unreachable)"

            if self.output_format == "txt":
                table_data.append(["{0:s}{1:s}".format(board_info.amc_name(), alias_str),
                                   comment,
                                   "{0:s}-{1:s}".format(board_id, board_type),
                                   "EUI-48={0:s}".format(eui48),
                                   "MAC={0:s}".format(mac_address),
                                   "serial #={0:s}".format(str(serial_number)),
                                   "system fw={0:s}".format(system_fw),
                                   "user fw={0:s}".format(user_fw)])
            elif self.output_format == "csv":
                table_data.append([board_info.rack(),
                                   board_info.crate_number(),
                                   board_info.slot_number(),
                                   board_id,
                                   board_type,
                                   eui48,
                                   mac_address,
                                   str(serial_number),
                                   system_fw,
                                   user_fw])

        if len(table_data):
            col_widths = [max([len(j[i]) for j in table_data]) for i in xrange(len(table_data[0]))]
            for (i, line) in enumerate(table_data):
                tmp = ["{{line[{0:d}]:{{col_widths[{0:d}]}}s}}".format(i) \
                       for i in xrange(len(line)) \
                       if (col_widths[i] != 0)]
                msg = "{0:s}: {1:s}".format(tmp[0], ", ".join(tmp[1:]))
                print msg.format(col_widths=col_widths, line=line)
        else:
            print "No boards found"
        # End of print_results().

    def get_board_info(self, identifier):
        # Get the (TCDS-specific) board info.
        cmd = "./tcds_board_info.py" \
              " --brief" \
              " --setup-file={0:s}" \
              " --location={1:s}" \
              " {2:s}".format(self.setup_file_name, self.location_name, identifier)
        (status, output) = commands.getstatusoutput(cmd)
        msg = None
        if status != 0:
            if output.find("does not sound like a valid network address") > -1:
                msg = "The network does not know '{0:s}'. " \
                      "Are you sure you are using the correct location?"
                self.error(msg.format(identifier))
            elif (output.find("deadline has passed") > -1) or \
                 (output.find("The ControlHub did not receive any response") > -1):
                # No response. Probably no board in this slot.
                msg = "no board in this slot"
            else:
                import pdb
                pdb.set_trace()
                msg = "Something went wrong obtaining board info for '{0:s}': '{1:s}'."
                self.error(msg.format(identifier, output))

        res = BoardInfo(identifier, output)

        # End of get_board_info().
        return res

    # End of class BoardInventory.

###############################################################################

if __name__ == "__main__":

    desc_str = "Helper script to gather and dump an inventory " \
               "of all installed TCDS boards."
    res = BoardInventory(desc_str).run()

    print "Done"
    sys.exit(res)

###############################################################################
