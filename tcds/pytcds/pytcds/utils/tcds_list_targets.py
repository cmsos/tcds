#!/usr/bin/env python

###############################################################################
## Utility to dump a list of all targets of a given type (e.g., LPM, ICI).
###############################################################################

import re
import sys

from pytcds.utils.tcds_cmd_line_base import CmdLineBase
from pytcds.utils.tcds_utils_setup_description import hw_sorter_helper

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class TargetLister(CmdLineBase):

    TARGET_CHOICES = ['hardware', 'software']

    def __init__(self, description=None, usage=None, epilog=None):
        super(TargetLister, self).__init__(description, usage, epilog)
        self.target_type = None
        self.target_species = None
        self.target_id_filter = None
        # End of __init__().

    def setup_parser_custom(self):
        # super(TargetLister, self).setup_parser_custom()
        parser = self.parser
        # Add the choice of the target type.
        help_str = "Target type. " \
                   "Options: '{0:s}'. " \
                   "[default: hardware]"
        help_str = help_str.format("', '".join(TargetLister.TARGET_CHOICES))
        parser.add_argument("--target-type",
                            type=str,
                            action="store",
                            choices=TargetLister.TARGET_CHOICES,
                            default='hardware',
                            help=help_str)
        # Add the choice of the target species.
        help_str = "Target type. " \
                   "(E.g., cpm-t1, LPMController, etc.) " \
                   "[default: all]"
        parser.add_argument("--target-species",
                            type=str,
                            action="store",
                            default='all',
                            help=help_str)
        # Add an overall filtering option.
        help_str = "Filter by target identifier using a regular expression " \
                   "[default: '.*']"
        parser.add_argument("--target-filter",
                            type=str,
                            action="store",
                            default='.*',
                            help=help_str)
        # End of setup_parser_custom().

    def handle_args(self):
        super(TargetLister, self).handle_args()

        # Handle the target type choice.
        self.target_type = self.args.target_type.lower()
        self.target_species = self.args.target_species
        self.target_id_filter = self.args.target_filter
        if self.verbose:
            msg = "Listing target type(s) '{0:s}', species '{1:s}'"
            print msg.format(self.target_type, self.target_species)

        # End of handle_args().

    def main(self):

        targets = []
        if self.target_type == "hardware":
            # Hardware targets.
            targets = [j for (i, j) in self.setup.hw_targets.iteritems()]
            if self.target_species != "all":
                targets = [i for i in targets if i.type in self.target_species]
            regex = re.compile(self.target_id_filter)
            targets = [i for i in targets if regex.match(i.identifier)]
            targets.sort(key=hw_sorter_helper)
        elif self.target_type == "software":
            # Software targets.
            targets = [j for (i, j) in self.setup.sw_targets.iteritems()]
            if self.target_species != "all":
                targets = [i for i in targets if i.app_class.split(':')[-1] in self.target_species]
            regex = re.compile(self.target_id_filter)
            targets = [i for i in targets if regex.match(i.identifier)]
            targets.sort()

        print " ".join([i.identifier for i in targets])

        # End of main().

    # End of class TargetLister.

###############################################################################

if __name__ == "__main__":

    desc_str = "Helper script to dump a list of targets of the specified type."
    res = TargetLister(desc_str).run()

    # print "Done"
    sys.exit(res)

###############################################################################
