#!/usr/bin/env python

###############################################################################
## Configure the clocking circuitry to use the signal from the
## on-board crystal (or whatever is connected to the corresponding
## trace) as the 40 MHz TTC clock.
###############################################################################

import sys

from pytcds.utils.tcds_utils_hw_connect import get_carrier_hw
from pytcds.utils.tcds_cmd_line_base import CmdLineBase

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class ClockCfg(CmdLineBase):

    def main(self):
        self.validate_target(target_type=CmdLineBase.TARGET_TYPE_HW)
        board_info = self.setup.hw_targets[self.target]
        network_address = board_info.dns_alias
        controlhub_address = board_info.controlhub
        carrier = get_carrier_hw(network_address, controlhub_address, verbose=self.verbose)
        if not carrier:
            self.error("Could not connect to {0:s}.".format(self.ip_address))

        if self.verbose:
            print "Switching TTC clock source to on-board XTAL."

        carrier.choose_clock_source("xtal")
        # End of main().

    # End of class ClockCfg.

###############################################################################

if __name__ == "__main__":

    res = ClockCfg().run()
    print "Done"
    sys.exit(res)

###############################################################################
