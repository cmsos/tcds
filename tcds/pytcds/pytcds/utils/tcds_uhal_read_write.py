#!/usr/bin/env python

###############################################################################
## Low-level developer tool to read/write arbitrary register values in
## TCDS uhal devices.
###############################################################################

# TODO TODO TODO
# Need to write some documentation and some help strings for the options.
# TODO TODO TODO end

import optparse
import sys

import uhal

from pytcds.utils.tcds_constants import BOARD_TYPE_UNKNOWN
from pytcds.utils.tcds_utils_hw_id import connect_to_identified_board
from pytcds.utils.tcds_utils_hw_id import identify_board
from pytcds.utils.tcds_utils_misc import parse_as_int
from pytcds.utils.tcds_utils_misc import uint32_to_string
from pytcds.utils.tcds_utils_networking import extract_ip_address

try:
    import debug_hook
    import pdb
except ImportError:
    pass

MODE_READ = 0
MODE_WRITE = 1

###############################################################################

def format_register_contents(value, fmt):
    res = None
    if fmt == "h":
        res = "0x{0:08x}".format(value)
    elif fmt == "c":
        res = uint32_to_string(value)
    else:
        res = "{0:d}".format(value)
    # End of format_register_contents().
    return res

###############################################################################

if __name__ == "__main__":

    desc_str = "Hacker script to access all register contents " \
        "of an IPbus device specified by its IP address."
    usage_str = "usage: %prog IP"
    arg_parser = optparse.OptionParser(description=desc_str,
                                       usage=usage_str)
    arg_parser.add_option("-a", "--address",
                          action="store",
                          dest="address",
                          type="string",
                          default="0x0")
    arg_parser.add_option("-m", "--mask",
                          action="store",
                          dest="mask",
                          type="int",
                          default=0xffffffff)
    arg_parser.add_option("-o", "--offset",
                          action="store",
                          dest="offset",
                          type="int",
                          default=0x0)
    arg_parser.add_option("-r", "--read",
                          action="store_true",
                          dest="read",
                          default=False)
    arg_parser.add_option("-s", "--size",
                          action="store",
                          dest="size",
                          type="int",
                          default=1)
    arg_parser.add_option("-v", "--verbose",
                          action="store_true",
                          dest="verbose",
                          default=False)
    arg_parser.add_option("-w", "--write",
                          action="store_true",
                          dest="write",
                          default=False)

    (opts, args) = arg_parser.parse_args()
    address = opts.address
    mask = opts.mask
    offset = opts.offset
    read = opts.read
    num_words = opts.size
    verbose = opts.verbose
    write = opts.write

    mode = None
    if (read and write):
        arg_parser.error("Can't both read and write.")
    else:
        if read:
            mode = MODE_READ
        elif write:
            mode = MODE_WRITE
        else:
            # Nothing specified. Fall back to read mode.
            mode = MODE_READ

    # Take at least one argument.
    if len(args) < 1:
        arg_parser.error("At least one argument is required: " \
                         "the IP address of the device.")

    # The first argument has to sound like an IP address.
    tmp_ip_address = args[0]
    ip_address = extract_ip_address(tmp_ip_address)
    if not ip_address:
        msg = "'{0:s}' does not sound like an IP address.".format(tmp_ip_address)
        arg_parser.error(msg)

    # If in write mode, all additional arguments (and then we expect
    # at least one) are taken as values to be written to the hardware.
    new_values = None
    if mode == MODE_WRITE:
        tmp = args[1:]
        if not tmp:
            arg_parser.error("In write mode at least one value to write is needed.")
        else:
            new_values = [parse_as_int(i) for i in tmp]

    ##########

    if not verbose:
        uhal.setLogLevelTo(uhal.LogLevel.WARNING)

    ##########

    board_type = identify_board(ip_address, verbose=verbose)
    if board_type is BOARD_TYPE_UNKNOWN:
        print >> sys.stderr, "Could not identify the board."
        sys.exit(1)
    dev = connect_to_identified_board(board_type, ip_address, verbose=verbose)
    if dev is None:
        print >> sys.stderr, "Could not connect to the board."
        sys.exit(1)

    ##########

    # See if we have been given an register specification as a number
    # or as a name.
    address_tmp = None
    try:
        address_tmp = parse_as_int(address)
    except (ValueError, AttributeError):
        try:
            address_tmp = dev.getNode(address).getAddress()
        except Exception:
            print >> sys.stderr, \
                "Could not resolve '{0:s}' to a register.".format(address)
            sys.exit(1)

    address = address_tmp
    full_address = address + offset
    if mode == MODE_READ:
        raw_values = dev.read_block(full_address, num_words)
        tmp_values = [(i & mask) for i in raw_values]
        msg = "Read from address 0x{0:08x} + 0x{1:08x} = 0x{2:08x}:"
        print msg.format(address, offset, full_address)
        for fmt in ["h", "c"]:
            tmp = [format_register_contents(i, fmt) for i in tmp_values]
            print "  {0:s}".format(", ".join(tmp))
    elif mode == MODE_WRITE:
        dev.write(full_address, new_values)
        msg = "Wrote to address 0x{0:08x} + 0x{1:08x} = 0x{1:08x}:"
        print msg.format(address, offset, full_address)
        for fmt in ["h", "c"]:
            tmp = [format_register_contents(i, fmt) for i in new_values]
            print "  {0:s}".format(", ".join(tmp))
        raw_values = dev.read_block(full_address, num_words)
        tmp_values = [(i & mask) for i in raw_values]
        print "Read back:"
        for fmt in ["h", "c"]:
            tmp = [format_register_contents(i, fmt) for i in tmp_values]
            print "  {0:s}".format(", ".join(tmp))
    else:
        # ASSERT ASSERT ASSERT
        assert False, \
            "ERROR Interesting: mode is neither MODE_READ nor MODE_WRITE."
        # ASSERT ASSERT ASSERT end

    ##########

    print "Done"

###############################################################################
