###############################################################################
## Utilities related to the Boston AMC13 board.
###############################################################################

from pytcds.utils.tcds_amc13_flasher import AMC13Flasher
from pytcds.utils.tcds_baseboard import BaseBoard
from pytcds.utils.tcds_constants import BOARD_TYPE_CPMT1
from pytcds.utils.tcds_constants import BOARD_TYPE_CPMT2

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class AMC13T1(BaseBoard):

    def __init__(self, carrier_type, board_type, hw_interface, verbose=False):
        # ASSERT ASSERT ASSERT
        assert carrier_type == BOARD_TYPE_CPMT1
        # ASSERT ASSERT ASSERT end
        super(AMC13T1, self).__init__(carrier_type, board_type, hw_interface, verbose)
        self.user_fw_base_name = "amc13t1"
        # End of __init__().

    def get_firmware_version(self, which="system"):
        """ Read and return the firmware version."""
        if which == "user":
            fw_version = "n/a"
        else:
            fw_ver_major = self.read("{0:s}.firmware_id.ver_major".format(which))
            fw_ver_minor = self.read("{0:s}.firmware_id.ver_minor".format(which))
            fw_ver_build = self.read("{0:s}.firmware_id.ver_build".format(which))
            fw_version = "{0:d}.{1:d}.{2:d}"
            fw_version = fw_version.format(fw_ver_major, fw_ver_minor, fw_ver_build)
        # End of get_firmware_version().
        return fw_version

    def get_firmware_date(self, which="system"):
        """ Read and return the firmware date."""
        if which == "user":
            fw_date = "n/a"
        else:
            fw_date_yy = self.read("{0:s}.firmware_id.date_yy".format(which))
            fw_date_mm = self.read("{0:s}.firmware_id.date_mm".format(which))
            fw_date_dd = self.read("{0:s}.firmware_id.date_dd".format(which))
            fw_date = "20{0:d}-{1:d}-{2:d}"
            fw_date = fw_date.format(fw_date_yy, fw_date_mm, fw_date_dd)
        # End of get_firmware_date().
        return fw_date

    def is_user_firmware_loaded(self):
        # End of is_user_firmware_loaded().
        return True

    def write_firmware(self, image_name, image_file_name, verbose=None):
        """Dummy. Programming T1 firmware happens through the T2."""
        msg = "Cannot directly program the CPM T1 firmware. Try via the T2."
        raise ValueError(msg)
        # End of write_firmware().

    # End of class AMC13T1.

###############################################################################

class AMC13T2(BaseBoard):

    def __init__(self, carrier_type, board_type, hw_interface, verbose=False):
        # ASSERT ASSERT ASSERT
        assert carrier_type == BOARD_TYPE_CPMT2
        # ASSERT ASSERT ASSERT end
        super(AMC13T2, self).__init__(carrier_type, board_type, hw_interface, verbose)
        self.user_fw_base_name = "amc13t2"
        # End of __init__().

    def get_serial_number(self):
        reg_name = "cpmt2.t2_serial_number"
        serial_number = self.read(reg_name)
        # End of get_serial_number().
        return serial_number

    def get_firmware_version(self, which="system"):
        """ Read and return the firmware version."""
        if which == "user":
            fw_version = "n/a"
        else:
            fw_ver_major = self.read("{0:s}.firmware_id.ver_major".format(which))
            fw_ver_minor = self.read("{0:s}.firmware_id.ver_minor".format(which))
            fw_ver_build = self.read("{0:s}.firmware_id.ver_build".format(which))
            fw_version = "{0:d}.{1:d}.{2:d}"
            fw_version = fw_version.format(fw_ver_major, fw_ver_minor, fw_ver_build)
        # End of get_firmware_version().
        return fw_version

    def get_firmware_date(self, which="system"):
        """ Read and return the firmware date."""
        if which == "user":
            fw_date = "n/a"
        else:
            fw_date_yy = self.read("{0:s}.firmware_id.date_yy".format(which))
            fw_date_mm = self.read("{0:s}.firmware_id.date_mm".format(which))
            fw_date_dd = self.read("{0:s}.firmware_id.date_dd".format(which))
            fw_date = "20{0:d}-{1:d}-{2:d}"
            fw_date = fw_date.format(fw_date_yy, fw_date_mm, fw_date_dd)
        # End of get_firmware_date().
        return fw_date

    def load_firmware(self, image_name):
        # NOTE: The image_name is a bit of a cheat, for the
        # AMC13-based devices. In reality this is the name of the
        # tongue to reboot.
        reg_name = "cpmt2.control.reload_" + image_name
        self.write(reg_name, 0)
        self.write(reg_name, 1)
        self.write(reg_name, 0)
        # End of load_firmware().

    def is_user_firmware_loaded(self):
        # End of is_user_firmware_loaded().
        return True

    def write_firmware(self, image_name, image_file_name, verbose=None):
        """Programs a firmware image from a .mcs file."""
        if verbose is None:
            verbose = self.verbose

        if not image_name in ["t1", "t2", "golden", "header"]:
            msg = "Image name '{0:s}' is not a valid CPM firmware image name. " \
                  "Try 't1' or 't2'.".format(image_name)
            raise ValueError(msg)

        if not image_file_name.endswith(".mcs") and \
           not image_file_name.endswith(".mcs.gz"):
            msg = "CPM firmware needs to be in the format of a .mcs(.gz) file. " \
                  "File '{0:s}' is not a .mcs file.".format(image_file_name)
            raise ValueError(msg)

        if verbose:
            msg = "Reading firmware image from file '{0:s}', writing image '{1:s}'."
            print msg.format(image_file_name, image_name)

        flasher = AMC13Flasher(self)
        flasher.write(image_name, image_file_name)
        # End of write_firmware().

    # End of class AMC13T2.

###############################################################################

class TCDSAMC13T1(AMC13T1):

    def __init__(self, carrier_type, board_type, hw_interface, verbose=False):
        # ASSERT ASSERT ASSERT
        assert carrier_type == BOARD_TYPE_CPMT1, \
            "ERROR Expected carrier type {0:d}, received {1:d}.".format(BOARD_TYPE_CPMT1,
                                                                        carrier_type)
        # ASSERT ASSERT ASSERT end
        super(TCDSAMC13T1, self).__init__(carrier_type, board_type, hw_interface, verbose)
        # End of __init__().

    # End of class TCDSAMC13T1.

###############################################################################

class TCDSAMC13T2(AMC13T2):

    def __init__(self, carrier_type, board_type, hw_interface, verbose=False):
        # ASSERT ASSERT ASSERT
        assert carrier_type == BOARD_TYPE_CPMT2, \
            "ERROR Expected carrier type {0:d}, received {1:d}.".format(BOARD_TYPE_CPMT2,
                                                                        carrier_type)
        # ASSERT ASSERT ASSERT end
        super(TCDSAMC13T2, self).__init__(carrier_type, board_type, hw_interface, verbose)
        # End of __init__().

    # End of class TCDSAMC13T2.

###############################################################################
