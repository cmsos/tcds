###############################################################################
## Several get_xxx_hw() methods.
#################################################################################

from pytcds.bobr.tcds_bobr import get_bobr_hw
from pytcds.rf2ttc.tcds_rf2ttc import get_rf2ttc_hw
from pytcds.rfrxd.tcds_rfrxd import get_rfrxd_hw
from pytcds.rftxd.tcds_rftxd import get_rftxd_hw
from pytcds.utils.tcds_carrier import Carrier
from pytcds.utils.tcds_constants import ADDRESS_TABLE_FILE_NAME_CPMT1
from pytcds.utils.tcds_constants import ADDRESS_TABLE_FILE_NAME_CPMT2
from pytcds.utils.tcds_constants import ADDRESS_TABLE_FILE_NAME_BARE_CARRIER
from pytcds.utils.tcds_constants import ADDRESS_TABLE_FILE_NAME_FC7
from pytcds.utils.tcds_constants import ADDRESS_TABLE_FILE_NAME_GLIB
from pytcds.utils.tcds_constants import BOARD_ID_AMC13
from pytcds.utils.tcds_constants import BOARD_ID_BOBR
from pytcds.utils.tcds_constants import BOARD_ID_CPMT1
from pytcds.utils.tcds_constants import BOARD_ID_CPMT2
from pytcds.utils.tcds_constants import BOARD_ID_DUMT1
from pytcds.utils.tcds_constants import BOARD_ID_DUMT2
from pytcds.utils.tcds_constants import BOARD_ID_GLIB
from pytcds.utils.tcds_constants import BOARD_ID_FC7
from pytcds.utils.tcds_constants import BOARD_ID_RF2TTC
from pytcds.utils.tcds_constants import BOARD_ID_RFRXD
from pytcds.utils.tcds_constants import BOARD_ID_RFTXD
from pytcds.utils.tcds_constants import BOARD_TYPE_CPMT1
from pytcds.utils.tcds_constants import BOARD_TYPE_CPMT2
from pytcds.utils.tcds_constants import BOARD_TYPE_DUMT1
from pytcds.utils.tcds_constants import BOARD_TYPE_DUMT2
from pytcds.utils.tcds_constants import BOARD_TYPE_BARE_CARRIER
from pytcds.utils.tcds_constants import BOARD_TYPE_FC7
from pytcds.utils.tcds_constants import BOARD_TYPE_GLIB
from pytcds.utils.tcds_constants import BOARD_TYPE_GOLDEN
from pytcds.utils.tcds_amc13 import TCDSAMC13T1
from pytcds.utils.tcds_amc13 import TCDSAMC13T2
from pytcds.utils.tcds_fc7 import TCDSFC7
from pytcds.utils.tcds_glib import TCDSGlib
from pytcds.utils.tcds_utils_uhal import get_hw_tca

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

def get_bare_carrier_hw(address_or_name,
                        controlhub_address,
                        verbose=False,
                        hw_cls=Carrier):

    carrier = get_hw_tca(BOARD_TYPE_BARE_CARRIER,
                         BOARD_TYPE_GOLDEN,
                         ADDRESS_TABLE_FILE_NAME_BARE_CARRIER,
                         address_or_name,
                         controlhub_address,
                         verbose,
                         hw_cls)

    # End of get_bare_carrier_hw().
    return carrier

###############################################################################

def get_carrier_hw(address_or_name,
                   controlhub_address,
                   verbose=False,
                   hw_cls=Carrier):

    board_id = None
    tmp = address_or_name.upper()
    if tmp.startswith("VME"):
        board_id = tmp.split(":")[1]
    else:
        bare_carrier = get_bare_carrier_hw(address_or_name,
                                           controlhub_address,
                                           verbose,
                                           hw_cls)
        board_id = bare_carrier.get_board_id()

    carrier = None
    if board_id == BOARD_ID_GLIB:
        carrier = get_glib_hw(address_or_name,
                              controlhub_address,
                              verbose)
    elif board_id == BOARD_ID_FC7:
        carrier = get_fc7_hw(address_or_name,
                             controlhub_address,
                             verbose)
    elif board_id == BOARD_ID_AMC13:
        system_id = bare_carrier.get_system_id()
        if system_id == BOARD_ID_CPMT1 or \
           system_id == BOARD_ID_DUMT1:
            carrier = get_amc13t1_hw(address_or_name,
                                     controlhub_address,
                                     verbose)
        elif system_id == BOARD_ID_CPMT2 or \
             system_id == BOARD_ID_DUMT2:
            carrier = get_amc13t2_hw(address_or_name,
                                     controlhub_address,
                                     verbose)
    elif board_id == BOARD_ID_BOBR:
        # For VME boards:
        # - The address is abused for the VME slot number.
        vme_slot = int(address_or_name.split(":")[-1])
        # - The controlhub address is abused for the VME chain and
        #   unit numbers.
        vme_chain = 0
        vme_unit = 0
        if controlhub_address:
            vme_chain = int(controlhub_address.split(":")[0])
            vme_unit = int(controlhub_address.split(":")[1])
        carrier = get_bobr_hw(vme_chain,
                              vme_unit,
                              vme_slot,
                              verbose)
    elif board_id == BOARD_ID_RF2TTC:
        # For VME boards:
        # - The address is abused for the VME slot number.
        vme_slot = int(address_or_name.split(":")[-1])
        # - The controlhub address is abused for the VME chain and
        #   unit numbers.
        vme_chain = 0
        vme_unit = 0
        if controlhub_address:
            vme_chain = int(controlhub_address.split(":")[0])
            vme_unit = int(controlhub_address.split(":")[1])
        carrier = get_rf2ttc_hw(vme_chain,
                                vme_unit,
                                vme_slot,
                                verbose)
    elif board_id == BOARD_ID_RFRXD:
        # For VME boards:
        # - The address is abused for the VME slot number.
        vme_slot = int(address_or_name.split(":")[-1])
        # - The controlhub address is abused for the VME chain and
        #   unit numbers.
        vme_chain = 0
        vme_unit = 0
        if controlhub_address:
            vme_chain = int(controlhub_address.split(":")[0])
            vme_unit = int(controlhub_address.split(":")[1])
        carrier = get_rfrxd_hw(vme_chain,
                               vme_unit,
                               vme_slot,
                               verbose)
    elif board_id == BOARD_ID_RFTXD:
        # For VME boards:
        # - The address is abused for the VME slot number.
        vme_slot = int(address_or_name.split(":")[-1])
        # - The controlhub address is abused for the VME chain and
        #   unit numbers.
        vme_chain = 0
        vme_unit = 0
        if controlhub_address:
            vme_chain = int(controlhub_address.split(":")[0])
            vme_unit = int(controlhub_address.split(":")[1])
        carrier = get_rftxd_hw(vme_chain,
                               vme_unit,
                               vme_slot,
                               verbose)
    else:
        assert False, "Unknown board id: '{0:s}'".format(board_id)

    # End of get_carrier_hw().
    return carrier

###############################################################################

def get_amc13t1_hw(address_or_name,
                   controlhub_address,
                   verbose=False,
                   hw_cls=TCDSAMC13T1):

    amc13t1 = get_hw_tca(BOARD_TYPE_CPMT1,
                         BOARD_TYPE_GOLDEN,
                         ADDRESS_TABLE_FILE_NAME_CPMT1,
                         address_or_name,
                         controlhub_address,
                         verbose,
                         hw_cls)

    # End of get_amc13t1_hw().
    return amc13t1

###############################################################################

def get_amc13t2_hw(address_or_name,
                   controlhub_address,
                   verbose=False,
                   hw_cls=TCDSAMC13T2):

    amc13t2 = get_hw_tca(BOARD_TYPE_CPMT2,
                         BOARD_TYPE_GOLDEN,
                         ADDRESS_TABLE_FILE_NAME_CPMT2,
                         address_or_name,
                         controlhub_address,
                         verbose,
                         hw_cls)

    # End of get_amc13t2_hw().
    return amc13t2

###############################################################################

def get_fc7_hw(address_or_name,
               controlhub_address,
               verbose=False,
               hw_cls=TCDSFC7):

    fc7 = get_hw_tca(BOARD_TYPE_FC7,
                     BOARD_TYPE_GOLDEN,
                     ADDRESS_TABLE_FILE_NAME_FC7,
                     address_or_name,
                     controlhub_address,
                     verbose,
                     hw_cls)

    # End of get_fc7_hw().
    return fc7

###############################################################################

def get_glib_hw(address_or_name,
                controlhub_address,
                verbose=False,
                hw_cls=TCDSGlib):

    glib = get_hw_tca(BOARD_TYPE_GLIB,
                      BOARD_TYPE_GOLDEN,
                      ADDRESS_TABLE_FILE_NAME_GLIB,
                      address_or_name,
                      controlhub_address,
                      verbose,
                      hw_cls)

    # End of get_glib_hw().
    return glib

#################################################################################
