###############################################################################
## Some miscellaneous utilities used in the TCDS scripts.
###############################################################################

import sys

from pytcds.bobr.tcds_bobr import get_bobr_hw
from pytcds.rf2ttc.tcds_rf2ttc import get_rf2ttc_hw
from pytcds.rfrxd.tcds_rfrxd import get_rfrxd_hw
from pytcds.rftxd.tcds_rftxd import get_rftxd_hw
from pytcds.utils.tcds_constants import ADDRESS_TABLE_DIR
from pytcds.utils.tcds_constants import BOARD_TYPE_AMC13
from pytcds.utils.tcds_constants import BOARD_TYPE_BOBR
from pytcds.utils.tcds_constants import BOARD_TYPE_CPMT1
from pytcds.utils.tcds_constants import BOARD_TYPE_CPMT2
from pytcds.utils.tcds_constants import BOARD_TYPE_FC7
from pytcds.utils.tcds_constants import BOARD_TYPE_GLIB
from pytcds.utils.tcds_constants import BOARD_TYPE_GOLDEN
from pytcds.utils.tcds_constants import BOARD_TYPE_RF2TTC
from pytcds.utils.tcds_constants import BOARD_TYPE_RFRXD
from pytcds.utils.tcds_constants import BOARD_TYPE_RFTXD
from pytcds.utils.tcds_constants import BOARD_TYPE_UNKNOWN
from pytcds.utils.tcds_constants import BOARD_TYPE_VME
from pytcds.utils.tcds_constants import board_names
from pytcds.utils.tcds_constants import board_types
from pytcds.utils.tcds_utils_hw_connect import get_carrier_hw
from pytcds.utils.tcds_utils_uhal import get_hw_tca

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

def identify_carrier(ip_address, controlhub_address, verbose=False):

    # First step: assume the thing is something we know and read its
    # board ID.
    if verbose:
        print "Connecting to device as a generic FMC carrier or VME board."
    try:
        carrier = get_carrier_hw(ip_address, controlhub_address, verbose=verbose)
    except Exception, err:
        if verbose:
            print >> sys.stderr, \
                "ERROR Could not set up the connection to the hardware: '{0:s}'.".format(err)
        return BOARD_TYPE_UNKNOWN

    try:
        board_id = carrier.get_board_id()
    except Exception, err:
        if verbose:
            print >> sys.stderr, "ERROR Could not read the board ID " \
                "(there may be no board at this address, " \
                "or the board is not a GLIB/FC7)."
        return BOARD_TYPE_UNKNOWN

    carrier_type = board_types.get(board_id, BOARD_TYPE_UNKNOWN)
    if not carrier_type in [BOARD_TYPE_GLIB,
                            BOARD_TYPE_FC7,
                            BOARD_TYPE_AMC13,
                            BOARD_TYPE_VME]:
        if verbose:
            print >> sys.stderr, \
                "Sorry, this is not a GLIB/FC7/AMC13-based device. Nothing I can do..."
        return BOARD_TYPE_UNKNOWN

    # End of identify_carrier().
    return carrier_type

###############################################################################

def identify_board(ip_address, controlhub_address, verbose=False):

    carrier_type = BOARD_TYPE_UNKNOWN
    board_type = BOARD_TYPE_UNKNOWN

    # First identify the carrier board.
    carrier_type = identify_carrier(ip_address, controlhub_address, verbose)
    if carrier_type != BOARD_TYPE_UNKNOWN:

        # Reconnect to the board.
        # NOTE: No error handling. Assume this still works after the
        # above call.
        carrier = get_carrier_hw(ip_address, controlhub_address, verbose=verbose)

        # Now try to get the user logic system-id.
        # NOTE: In case of an empty carrier firmware (e.g., a golden
        # image) this will read 'gold'.
        user_system_id = carrier.get_user_system_id()

        try:
            board_type = board_types[user_system_id]
        except KeyError:
            if verbose:
                print >> sys.stderr, \
                    "This appears to be a GLIB/FC7/CPM-based device " \
                    "with unknown user logic: '{0:s}'.".format(user_system_id)

    if verbose:
        if carrier_type != BOARD_TYPE_UNKNOWN:
            carrier_name = board_names[carrier_type]
            print "Carrier identified as {0:s}.".format(carrier_name)
        if board_type != BOARD_TYPE_UNKNOWN:
            board_name = board_names[board_type]
            print "Board identified as {0:s}.".format(board_name)

    # End of identify_board().
    return (carrier_type, board_type)

###############################################################################

def connect_to_identified_board(carrier_type,
                                board_type,
                                ip_address,
                                controlhub_address,
                                verbose=False):
    # NOTE: This method is intended to be called with the result of
    # identify_board().

    # ASSERT ASSERT ASSERT
    assert carrier_type != BOARD_TYPE_UNKNOWN
    assert board_type != BOARD_TYPE_UNKNOWN
    # ASSERT ASSERT ASSERT end

    carrier_name = board_names[carrier_type]
    board_name = board_names[board_type]
    carrier_name_tmp = carrier_name.lower()
    board_name_tmp = ""
    if board_type != BOARD_TYPE_GOLDEN:
        board_name_tmp = "_{0:s}".format(board_name.lower())
    extension = "xml"
    if carrier_type == BOARD_TYPE_VME:
        extension = "txt"
    tmp = "file://{0:s}/device_address_table_{1:s}{2:s}.{3:s}"
    address_table_file_name = tmp.format(ADDRESS_TABLE_DIR,
                                         carrier_name_tmp,
                                         board_name_tmp,
                                         extension)

    dev = None
    if verbose:
        print "Connecting to device as a {0:s}-{1:s}.".format(carrier_name, board_name)
    try:
        if carrier_type == BOARD_TYPE_VME:
            # For VME boards:
            # - The address is abused for the VME slot number.
            vme_slot = int(ip_address.split(":")[-1])
            # - The controlhub address is abused for the VME chain and
            #   unit numbers.
            vme_chain = 0
            vme_unit = 0
            if controlhub_address:
                vme_chain = int(controlhub_address.split(":")[0])
                vme_unit = int(controlhub_address.split(":")[1])
            if board_type == BOARD_TYPE_BOBR:
                dev = get_bobr_hw(vme_chain,
                                  vme_unit,
                                  vme_slot,
                                  verbose)
            elif board_type == BOARD_TYPE_RF2TTC:
                dev = get_rf2ttc_hw(vme_chain,
                                    vme_unit,
                                    vme_slot,
                                    verbose)
            elif board_type == BOARD_TYPE_RFRXD:
                dev = get_rfrxd_hw(vme_chain,
                                   vme_unit,
                                   vme_slot,
                                   verbose)
            elif board_type == BOARD_TYPE_RFTXD:
                dev = get_rftxd_hw(vme_chain,
                                   vme_unit,
                                   vme_slot,
                                   verbose)
            else:
                print >> sys.stderr, \
                    "ERROR Don't know what to make of board type {0:d}".format(board_type)
                return None
        else:
            dev = get_hw_tca(carrier_type,
                             board_type,
                             address_table_file_name,
                             ip_address,
                             controlhub_address,
                             verbose=verbose)
    except Exception, err:
        print >> sys.stderr, \
            "ERROR Could not set up the connection to the hardware: '{0:s}'.".format(err)
        return None

    # End of connect_to_identified_board().
    return dev

###############################################################################
