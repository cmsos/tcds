###############################################################################
## Helper methods etc. for use with the GLIB platform flash holding the
## firmware.
##
## Note: This is not very clean code. Error checking/handling does not
## really exist.
###############################################################################

import time
import signal

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

# Constants related to the firmware images.

# The minimum and maximum block numbers.
FLASH_BLOCK_HI = 130
FLASH_BLOCK_LO = 0

# The 'golden' image area.
FLASH_BLOCK_GOLDEN_HI = 130
FLASH_BLOCK_GOLDEN_LO = 67

# The 'user' image area.
FLASH_BLOCK_USER_HI = 66
FLASH_BLOCK_USER_LO = 11

FLASH_LIMITS = {
    "golden" : [FLASH_BLOCK_GOLDEN_LO, FLASH_BLOCK_GOLDEN_HI],
    "user" : [FLASH_BLOCK_USER_LO, FLASH_BLOCK_USER_HI]
    }

##########

# Flash commands. See p. 12 (table 6) of the Xilinx DS617 doc (v3.0.1).
FLASH_COMMAND_BLOCK_LOCK_CONFIRM = 0x01
FLASH_COMMAND_SET_CONFIGURATION_REGISTER_CONFIRM = 0x3
FLASH_COMMAND_ALTERNATIVE_PROGRAM_SETUP = 0x10
FLASH_COMMAND_BLOCK_ERASE_SETUP = 0x20
FLASH_COMMAND_BLOCK_LOCKDOWN_CONFIRM = 0x2f
FLASH_COMMAND_PROGRAM_SETUP = 0x40
FLASH_COMMAND_CLEAR_STATUS_REGISTER = 0x50
FLASH_COMMAND_BLOCK_LOCK_SETUP = 0x60
FLASH_COMMAND_BLOCK_UNLOCK_SETUP = 0x60
FLASH_COMMAND_BLOCK_LOCKDOWN_SETUP = 0x60
FLASH_COMMAND_SET_CONFIGURATION_REGISTER_SETUP = 0x60
FLASH_COMMAND_READ_STATUS_REGISTER = 0x70
FLASH_COMMAND_BUFFER_ENHANCED_FACTORY_PROGRAM_SETUP = 0x80
FLASH_COMMAND_READ_ELECTRONIC_SIGNATURE = 0x90
FLASH_COMMAND_READ_CFI_QUERY = 0x98
FLASH_COMMAND_PROGRAM_SUSPEND = 0xb0
FLASH_COMMAND_ERASE_SUSPEND = 0xb0
FLASH_COMMAND_BLANK_CHECK_SETUP = 0xbc
FLASH_COMMAND_PROTECTION_REGISTER_PROGRAM = 0xc0
FLASH_COMMAND_BLANK_CHECK_CONFIRM = 0xcb
FLASH_COMMAND_PROGRAM_RESUME = 0xd0
FLASH_COMMAND_ERASE_RESUME = 0xd0
FLASH_COMMAND_BLOCK_ERASE_CONFIRM = 0xd0
FLASH_COMMAND_BLOCK_UNLOCK_CONFIRM = 0xd0
FLASH_COMMAND_BUFFER_PROGRAM_CONFIRM = 0xd0
FLASH_COMMAND_BUFFER_ENHANCED_FACTORY_PROGRAM_CONFIRM = 0xd0
FLASH_COMMAND_BUFFER_PROGRAM = 0xe8
FLASH_COMMAND_READ_ARRAY = 0xff

# The number of words in the flash electronic signature (from table 9
# on p. 19 or Xilinx DS617).
SIZE_FLASH_ELECTRONIC_SIGNATURE = 265

# The offset (from the same table as the above) of the block
# protection code in the electronic signature.
OFFSET_BLOCK_PROTECTION = 0x2

# Flash status codes.
FLASH_STATUS_READY = 0x80

###############################################################################

class GlibFlashMem(object):

    # NOTE: In this class all hardware access is implemented in terms
    # of straight IPbus access methods. This is:
    # a) because the FLASH access seems to be relatively
    #    timing-critical, and
    # b) to shave off some of the method forwarding overhead.

    # The timeout for flash memory transactions, in seconds.
    TIMEOUT_VAL = 5

    @staticmethod
    def _handle_timeout(signum, frame):
        raise Exception("Flash memory transaction timed out.")
        # End of _handle_timeout().

    @staticmethod
    def bank_num_for_block(block_num):
        # DEBUG DEBUG DEBUG
        assert FLASH_BLOCK_LO <= block_num <= FLASH_BLOCK_HI
        # DEBUG DEBUG DEBUG end
        res = None
        if block_num <= 10:
            res = 0
        else:
            res = ((block_num - 11) / 8) + 1
        # End of bank_num_for_block.
        return res

    def __init__(self, glib):
        self.hw = glib.hw
        # End of __init__().

    def __enter__(self):
        self.enable()
        self.switch_to_async_io()
        return self
        # End of __enter__().

    def __exit__(self, type, value, traceback):
        self.switch_to_sync_io()
        self.disable()
        # End of __exit__().

    def enable(self):
        """Enable the flash memory (as opposed to the SRAM2)."""
        self.hw.getNode("glib.ctrl_sram.flash_select").write(0x1)
        self.hw.dispatch()
        # End of enable().

    def disable(self):
        """Disable the flash memory (and switch back to the SRAM2)."""
        self.hw.getNode("glib.ctrl_sram.flash_select").write(0x0)
        self.hw.dispatch()
        # End of disable().

    def get_electronic_signature(self, bank_num):
        reg_name = "glib.flash.bank{0:d}".format(bank_num)
        self.hw.getNode(reg_name).write(FLASH_COMMAND_READ_ELECTRONIC_SIGNATURE)
        tmp = self.hw.getNode(reg_name).readBlock(SIZE_FLASH_ELECTRONIC_SIGNATURE)
        self.hw.dispatch()
        res = [i for i in tmp]
        # End of get_electronic_signature().
        return res

    def clear_status(self, bank_num):
        reg_name = "glib.flash.bank{0:d}".format(bank_num)
        self.hw.getNode(reg_name).write(FLASH_COMMAND_CLEAR_STATUS_REGISTER)
        self.hw.dispatch()
        # End of clear_status().

    def get_status(self, bank_num):
        reg_name = "glib.flash.bank{0:d}".format(bank_num)
        self.hw.getNode(reg_name).write(FLASH_COMMAND_READ_STATUS_REGISTER)
        tmp = self.hw.getNode(reg_name).read()
        self.hw.dispatch()
        res = tmp.value()
        # End of get_status().
        return res

    def wait_for_status(self, bank_num, desired_status):
        # NOTE: Watch the timeout handling.
        status = None
        signal.signal(signal.SIGALRM, GlibFlashMem._handle_timeout)
        signal.alarm(GlibFlashMem.TIMEOUT_VAL)
        try:
            while status != desired_status:
                status = self.get_status(bank_num)
                time.sleep(.5)
        finally:
            signal.alarm(0)
        # End of wait_for_status().

    def get_lock_status(self, block_num):
        bank_num = GlibFlashMem.bank_num_for_block(block_num)
        reg_name = "glib.flash.bank{0:d}.block{1:d}".format(bank_num, block_num)
        self.hw.getNode(reg_name).write(FLASH_COMMAND_READ_ELECTRONIC_SIGNATURE)
        tmp = self.hw.getNode(reg_name).read_block(OFFSET_BLOCK_PROTECTION + 1)
        self.hw.dispatch()
        res = tmp[-1]
        # End of get_status().
        return res

    def get_block_size(self, block_num):
        bank_num = GlibFlashMem.bank_num_for_block(block_num)
        reg_name = "glib.flash.bank{0:d}.block{1:d}".format(bank_num, block_num)
        res = self.hw.get_node(reg_name).getSize()
        # End of block_size().
        return res

    def read_block(self, block_num):
        bank_num = GlibFlashMem.bank_num_for_block(block_num)
        reg_name = "glib.flash.bank{0:d}.block{1:d}".format(bank_num, block_num)
        self.clear_status(bank_num)
        self.hw.getNode(reg_name).write(FLASH_COMMAND_READ_ARRAY)
        block_size = self.get_block_size(block_num)
        tmp = self.hw.getNode(reg_name).readBlock(block_size)
        self.hw.dispatch()
        res = [i for i in tmp]
        # End of read_block().
        return res

    def write_block(self, block_num, block_data):
        # NOTE: This is a tricky method. IPbus in principle does not
        # allow write access to arbitrary (read: offset) memory
        # addresses, so we use the underlying ClientInterface instead.
        bank_num = GlibFlashMem.bank_num_for_block(block_num)
        reg_name = "glib.flash.bank{0:d}.block{1:d}".format(bank_num, block_num)
        block_size = len(block_data)
        # DEBUG DEBUG DEBUG
        assert block_size <= self.hw.get_node(reg_name).getSize()
        # DEBUG DEBUG DEBUG end

        # These flash thingies can write blocks using an internal
        # buffer of 32 words.
        buffer_size = 32
        ind_lo = 0
        words_left = block_size
        block_address = self.hw.get_node(reg_name).getAddress()
        offset = 0x0
        while words_left > 0:
            self.clear_status(bank_num)
            self.hw.getNode(reg_name).write(FLASH_COMMAND_BUFFER_PROGRAM)
            # NOTE: Read the Xilinx instructions: to send N words,
            # write N-1 during the setup sequence.
            tmp_size = min(words_left, buffer_size)
            tmp = block_data[ind_lo:ind_lo + tmp_size]
            assert len(tmp) == tmp_size
            self.hw.getNode(reg_name).write(len(tmp) - 1)
            self.hw.getClient().writeBlock(block_address + offset, tmp)
            self.hw.getNode(reg_name).write(FLASH_COMMAND_BUFFER_PROGRAM_CONFIRM)
            self.hw.dispatch()
            words_left -= len(tmp)
            ind_lo += len(tmp)
            offset += len(tmp)
        # End of write_block().

    def erase_block(self, block_num):
        bank_num = GlibFlashMem.bank_num_for_block(block_num)
        reg_name = "glib.flash.bank{0:d}.block{1:d}".format(bank_num, block_num)
        self.clear_status(bank_num)
        for cmd in [FLASH_COMMAND_BLOCK_ERASE_SETUP,
                    FLASH_COMMAND_BLOCK_ERASE_CONFIRM]:
            self.hw.getNode(reg_name).write(cmd)
        self.hw.dispatch()
        self.wait_for_status(bank_num, FLASH_STATUS_READY)
        # End of erase_block().

    def switch_to_async_io(self):
        for cmd in [FLASH_COMMAND_SET_CONFIGURATION_REGISTER_SETUP,
                    FLASH_COMMAND_SET_CONFIGURATION_REGISTER_CONFIRM]:
            self.hw.getNode("glib.flash.cfg_io_mode_async").write(cmd)
        self.hw.dispatch()
        # End of switch_to_async_io().

    def switch_to_sync_io(self):
        for cmd in [FLASH_COMMAND_SET_CONFIGURATION_REGISTER_SETUP,
                    FLASH_COMMAND_SET_CONFIGURATION_REGISTER_CONFIRM]:
            self.hw.getNode("glib.flash.cfg_io_mode_sync").write(cmd)
        self.hw.dispatch()
        # End of switch_to_sync_io().

    def unlock_block(self, block_num):
        bank_num = GlibFlashMem.bank_num_for_block(block_num)
        reg_name = "glib.flash.bank{0:d}.block{1:d}".format(bank_num, block_num)
        self.clear_status(bank_num)
        for cmd in [FLASH_COMMAND_BLOCK_UNLOCK_SETUP,
                    FLASH_COMMAND_BLOCK_UNLOCK_CONFIRM]:
            self.hw.getNode(reg_name).write(cmd)
        self.hw.dispatch()
        # End of unlock_block().

    def lock_block(self, block_num):
        bank_num = GlibFlashMem.bank_num_for_block(block_num)
        reg_name = "glib.flash.bank{0:d}.block{1:d}".format(bank_num, block_num)
        self.clear_status(bank_num)
        for cmd in [FLASH_COMMAND_BLOCK_LOCK_SETUP,
                    FLASH_COMMAND_BLOCK_LOCK_CONFIRM]:
            self.hw.getNode(reg_name).write(cmd)
        self.hw.dispatch()
        # End of lock_block().

    # End of class GlibFlashMem.

###############################################################################
