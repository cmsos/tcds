#!/usr/bin/env python

###############################################################################
## Low-level developer tool to read/write arbitrary register values in
## TCDS uhal devices.
###############################################################################

# TODO TODO TODO
# Properly filter input characters in the various input dialogs.
# TODO TODO TODO end

import curses
import curses.ascii
import fnmatch
import itertools
import optparse
import os
import string
import sys

import uhal

from pytcds.utils.tcds_cmd_line_base import CmdLineBase
from pytcds.utils.tcds_constants import BOARD_TYPE_UNKNOWN
from pytcds.utils.tcds_utils_hw_id import connect_to_identified_board
from pytcds.utils.tcds_utils_hw_id import identify_board
from pytcds.utils.tcds_utils_misc import name_matches_pattern
from pytcds.utils.tcds_utils_misc import parse_as_int
from pytcds.utils.tcds_utils_misc import uint32_to_string
from pytcds.utils.tcds_utils_misc import uint8_to_char
from pytcds.utils.tcds_utils_networking import resolve_network_address

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

def hide_cursor():
    try:
        curses.curs_set(0)
    except curses.error:
        pass
    # End of hide_cursor().

def show_cursor():
    try:
        curses.curs_set(1)
    except curses.error:
        pass
    # End of show_cursor().

###############################################################################

class HwHacker(object):

    SCROLL_DIR_UP = -1
    SCROLL_DIR_DOWN = 1

    SORT_MODE_BY_NAME = 0
    SORT_MODE_BY_ADDRESS = 1
    SORT_MODES = [SORT_MODE_BY_NAME, SORT_MODE_BY_ADDRESS]

    VIEW_MODE_DEC = 10
    VIEW_MODE_HEX = 16
    VIEW_MODE_BIN = 2
    VIEW_MODE_CHAR = 100
    VIEW_MODES = [VIEW_MODE_HEX, VIEW_MODE_BIN, VIEW_MODE_DEC, VIEW_MODE_CHAR]

    # NOTE: The margins include one character for the border.
    MARGIN_Y = 1
    MARGIN_X = 2

    # Some registers we just don't want to see. The flash memory
    # access registers, for example, are way too big to handle in an
    # ad hoc way.
    IGNORE_LIST = []
    # Always ignore all the flash memory.
    IGNORE_LIST.append("^.*flash.*bank.*$")

    def __init__(self, scr, hw):
        self.pad_may_need_resizing = True
        self._raw_cache = None
        self._cache = None
        self.invalidate_cache()
        self.invalidate_raw_cache()
        self.scr = scr
        self.pad = None
        self.hw = hw
        self.filter_pattern = ".*"
        self.sort_modes = itertools.cycle(HwHacker.SORT_MODES)
        self.view_modes = itertools.cycle(HwHacker.VIEW_MODES)
        self.sort_mode = self.sort_modes.next()
        self.view_mode = self.view_modes.next()
        self.verbose = False
        self.top_line = 0
        self.current_line = self.top_line
        self.run()
        # End of __init__().

    @property
    def win_height(self):
        res = self.scr.getmaxyx()[0]
        # End of win_height().
        return res

    @property
    def win_width(self):
        res = self.scr.getmaxyx()[1]
        # End of win_width().
        return res

    @property
    def num_lines_visible(self):
        res = self.win_height - 2 * HwHacker.MARGIN_Y
        # End of num_lines_visible().
        return res

    @property
    def num_columns_visible(self):
        res = self.win_width - 2 * HwHacker.MARGIN_X
        # End of num_columns_visible().
        return res

    @property
    def bottom_line(self):
        tmp = self.top_line + self.num_lines_visible - 1
        res = min(tmp, len(self._cache) - 1)
        # End of bottom_line().
        return res

    def window(self, height, width, message='', title=''):
        height = min(self.win_height, height)
        width  = min(self.win_width, width)
        pos_y = (self.win_height - height) / 2
        pos_x = (self.win_width  - width) / 2
        win = curses.newwin(height, width, pos_y, pos_x)
        win.box()
        win.bkgd(' ', curses.A_REVERSE + curses.A_BOLD)

        if width >= len(title) and title != '':
            win.addstr(0, 1, " " + title + " ")

        pos_y = 1
        for line in message.split("\n"):
            if pos_y < height - 1:
                win.addstr(pos_y, 2, line)
                pos_y += 1
            else:
                # Do not write outside of frame border
                win.addstr(pos_y, 2, " More... ")
                break

        # End of window
        return win

    def dialog_register_edit(self, message, contents=""):
        width  = self.win_width - 4
        text_width = width - 4

        # Split the contents into lines.
        # NOTE: The assumption here is that all lines have equal length.
        contents_split = contents.split("\n")
        # ASSERT ASSERT ASSERT
        assert len(set([len(i) for i in contents_split])) == 1
        # ASSERT ASSERT ASSERT end
        height = min(len(contents_split) + 2, self.win_height - 4)
        max_len_contents = len(str(len(contents_split)))
        is_multiline = (len(contents_split) > 1)
        top_row = 0
        prev_row = None
        cur_row = 0
        left_col = 0
        if is_multiline:
            left_col = 2 * max_len_contents + 2
        cur_line = None
        cur_col = None
        win = self.window(height, width, title=message)
        win.keypad(1)
        show_cursor()

        while True:
            for i in xrange(height - 2):
                offset_str = ""
                if is_multiline:
                    offset_str = "{1:{0:d}d}/{2:d} ".format(max_len_contents,
                                                            (top_row + i + 1),
                                                            len(contents_split))
                contents_str = contents_split[top_row + i]
                win.addstr(i + 1, 2, " " * text_width)
                win.addstr(i + 1, 2, "{0:s}{1:s}".format(offset_str, contents_str))
            if cur_row != prev_row:
                cur_line = contents_split[top_row + cur_row]
                prev_row = cur_row
            if cur_col is None:
                cur_col = left_col + len(cur_line)
            win.move(cur_row + 1, cur_col + 2)

            # NOTE: This is a bit of a weird construct, but this works
            # around a bug in getkey(). Otherwise there is no way to
            # catch the KeyboardInterrupt.
            c = ""
            try:
                try:
                    c = win.getkey()
                except:
                    pass
            except KeyboardInterrupt:
                curses.flushinp()
                hide_cursor()
                return None

            if c == "KEY_LEFT":
                if cur_col > left_col:
                    cur_col -= 1
            elif c == "KEY_RIGHT":
                if (cur_col - left_col) < len(cur_line):
                    cur_col += 1
            elif c == "KEY_UP":
                if cur_row > 0:
                    cur_row -= 1
                else:
                    if top_row > 0:
                        top_row -= 1
            elif c == "KEY_DOWN":
                if cur_row < (height - 3):
                    cur_row += 1
                else:
                    bottom_row = top_row + height - 1
                    if bottom_row < len(contents_split) + 1:
                        top_row += 1
            elif c == "KEY_BACKSPACE":
                if cur_col > left_col:
                    ind = cur_col - left_col
                    tmp = cur_line[:ind - 1]
                    if cur_col < len(cur_line):
                        tmp += cur_line[ind:]
                    cur_line = tmp
                    contents_split[top_row + cur_row] = cur_line
                    cur_col -= 1
            elif c == "KEY_DC":
                ind = cur_col - left_col
                if ind < len(cur_line):
                    cur_line = cur_line[:ind] + cur_line[ind + 1:]
                    contents_split[top_row + cur_row] = cur_line
            elif c == "KEY_HOME":
                top_row = 0
                cur_row = 0
                cur_col = left_col
            elif c == "KEY_END":
                top_row = len(contents_split) - height + 2
                cur_row = height - 3
                if is_multiline:
                    cur_col = left_col
                else:
                    cur_col = len(contents_split[top_row + cur_row])
            elif c == "KEY_NPAGE":
                top_row += 10
                bottom_row = top_row + height - 1
                if bottom_row > len(contents_split) + 1:
                    top_row -= 10
            elif c == "KEY_PPAGE":
                top_row -= 10
                if top_row < 0:
                    top_row = 0
            elif c == "\n":
                # NOTE: Substitute zeros for empty values.
                for (i, val) in enumerate(contents_split):
                    if not len(val):
                        contents_split[i] = "0"
                hide_cursor()
                return "\n".join(contents_split)
            elif c in string.printable:
                if len(cur_line) != text_width:
                    tmp = cur_line[:cur_col] + c
                    if cur_col < len(cur_line):
                        tmp += cur_line[cur_col:]
                    cur_line = tmp
                    contents_split[top_row + cur_row] = cur_line
                    cur_col += 1
            elif ord(c) == 27:
                # Escape was pressed.
                break

        # End of dialog_register_edit().
        return None

    def dialog_text_input(self, message, contents=""):
        width  = self.win_width - 4
        text_width = width - 4
        height = 4
        win = self.window(height, width, message=message)
        win.keypad(1)
        show_cursor()

        cur_col = len(contents)
        while True:
            page = cur_col / text_width
            contents_visible = contents[text_width * page:text_width * (page + 1)]
            display_cur_col = cur_col - text_width * page
            win.move(height - 2, 2)
            win.addstr(height - 2, 2, "_" * text_width)
            win.addstr(height - 2, 2, contents_visible)
            win.move(height - 2, display_cur_col + 2)

            # NOTE: This is a bit of a weird construct, but this works
            # around a bug in getkey(). Otherwise there is no way to
            # catch the KeyboardInterrupt.
            try:
                try:
                    c = win.getkey()
                except:
                    pass
            except KeyboardInterrupt:
                curses.flushinp()
                hide_cursor()
                return None

            if c == "KEY_RIGHT":
                if cur_col < len(contents):
                    cur_col += 1
            elif c == "KEY_LEFT":
                if cur_col > 0:
                    cur_col -= 1
            elif c == "KEY_BACKSPACE":
                if cur_col > 0:
                    tmp = contents[:cur_col - 1]
                    if cur_col < len(contents):
                        tmp += contents[cur_col:]
                    contents = tmp
                    cur_col -= 1
            elif c == "KEY_DC":
                if cur_col < len(contents):
                    contents = contents[:cur_col] + contents[cur_col + 1:]
            elif c == "KEY_HOME":
                cur_col = 0
            elif c == "KEY_END":
                cur_col = len(contents)
            elif c == "\n":
                hide_cursor()
                return contents
            elif c in string.printable:
                tmp = contents[:cur_col] + c
                if cur_col < len(contents):
                    tmp += contents[cur_col:]
                contents = tmp
                cur_col += 1
            elif ord(c) == 27:
                # Escape was pressed.
                break

        # End of dialog_text_input().
        return None

    def toggle_sort_mode(self):
        self.pad_may_need_resizing = True
        self.invalidate_cache()
        self.sort_mode = self.sort_modes.next()
        # End of toggle_sort_mode().

    def toggle_view_mode(self):
        self.pad_may_need_resizing = True
        self.invalidate_cache()
        self.view_mode = self.view_modes.next()
        # End of toggle_view_mode().

    def toggle_verbose(self):
        self.pad_may_need_resizing = True
        self.invalidate_cache()
        self.verbose = not self.verbose
        # End of toggle_verbose().

    def invalidate_cache(self):
        self._cache = None
        # End of invalidate_cache().

    def invalidate_raw_cache(self):
        self._raw_cache = None
        # End of invalidate_raw_cache().

    def apply_filter(self, list_in):
        res = list_in
        pattern = self.filter_pattern
        if pattern:
            if pattern[0] == "!":
                # res = [i for i in list_in if not fnmatch.fnmatch(i.name, pattern[1:])]
                res = [i for i in list_in if not name_matches_pattern(i.name, [pattern[1:]])]
            else:
                # res = [i for i in list_in if fnmatch.fnmatch(i.name, pattern)]
                res = [i for i in list_in if name_matches_pattern(i.name, [pattern])]
        # End of apply_filter().
        return res

    def format_register_contents(self, reg_info):
        # The format in which the register values are shown depends on
        # the current view mode.
        # NOTE: Note the exceptional handling of write-only registers.
        # NOTE: Note the exceptional handling of the VIEW_MODE_CHAR view mode.
        res = []
        if reg_info.contents == [None]:
            if reg_info.is_readable():
                res.append("could not access this register")
            else:
                res.append("?")
        elif reg_info.is_readable():
            if self.view_mode == HwHacker.VIEW_MODE_CHAR:
                if (len(reg_info.contents) == 1):
                    try:
                        if (reg_info.width("b") == 8):
                            # Single character.
                            res.append(uint8_to_char(reg_info.contents[0]))
                        elif (reg_info.width("b") == 16):
                            # Two characters packed into 16 bits.
                            pass
                            # res.append(uint32_to_string((reg_info.contents[0] & 0xf0) >> 8))
                            #res.append(uint32_to_string(reg_info.contents[0] & 0x0f))
                        elif (reg_info.width("b") == 24):
                            # Three characters packed into 16 bits.
                            pass
                            # res.append(uint32_to_string((reg_info.contents[0] & 0xf00) >> 16))
                            # res.append(uint32_to_string((reg_info.contents[0] & 0x0f0) >> 8))
                            # res.append(uint32_to_string(reg_info.contents[0] & 0x00f))
                        elif (reg_info.width("b") == 32):
                            # Four characters packed into a single, 32-bit
                            # word.
                            res.append(uint32_to_string(reg_info.contents[0]))
                    except Exception, err:
                        import pdb
                        pdb.set_trace()
            else:
                fmt_str = None
                if self.view_mode == HwHacker.VIEW_MODE_DEC:
                    fmt_str = "{0:{1}d}"
                    fmt_chr = "d"
                elif self.view_mode == HwHacker.VIEW_MODE_HEX:
                    fmt_str = "0x{0:0{1}x}"
                    fmt_chr = "h"
                elif self.view_mode == HwHacker.VIEW_MODE_BIN:
                    fmt_str = "0b{0:0{1}b}"
                    fmt_chr = "b"
                tmp_contents = reg_info.contents
                res = [fmt_str.format(i, reg_info.width(fmt_chr)) for i in tmp_contents]
        # End of format_register_contents().
        return res

    def build_line(self, reg_info):
        res_pieces = []

        # Add the read/write flags.
        rw_str = ""
        if reg_info.is_readable():
            rw_str += "R"
        else:
            rw_str += " "
        if reg_info.is_writable():
            rw_str += "W"
        else:
            rw_str += " "
        res_pieces.append(rw_str)

        # Add the register type.
        type_str = ""
        if reg_info.is_hierarchical():
            type_str = "HIERARCHICAL"
        else:
            if reg_info.is_block():
                type_str = "BLOCK"
            else:
                type_str = "SINGLE"
        res_pieces.append("{0:<12s}".format(type_str))

        # Add the address and the mask only in verbose mode.
        if self.verbose:
            res_pieces.append("0x{0:08x}".format(reg_info.address))
            res_pieces.append("0x{0:08x}".format(reg_info.mask))

        # Of course show the register value. The format depends on the
        # current view mode.
        # NOTE: For hierarchical nodes it is no use to show a register value.
        if not reg_info.is_hierarchical():
            tmp = self.format_register_contents(reg_info)
            if len(tmp) == 1:
                res_pieces.append(tmp[0])
            elif len(tmp):
                res_pieces.append(" ".join([tmp[0], "...", tmp[-1]]))

        res = " ".join(res_pieces)
        # En of build_line().
        return res

    def run(self):
        self.init_curses()
        self.update_screen()
        self.update()
        self.run_input_handler()
        # End of run().

    def init_curses(self):
        curses.use_default_colors()
        hide_cursor()
        # End of init_curses().

    def update(self):
        self.update_content()
        self.update_screen()
        self.update_pad()
        curses.doupdate()
        # End of update().

    def update_content(self):
        if not self._raw_cache:
            # Mark the fact that we're doing something.
            self.scr.addstr(self.win_height - 1, HwHacker.MARGIN_X, "Reading from the hardware...")
            self.scr.refresh()
            self._raw_cache = self.hw.create_reg_info_list(ignore_list=HwHacker.IGNORE_LIST,
                                                           include_list=[self.filter_pattern])
            self.invalidate_cache()

        if not self._cache:
            # Filter which registers should be displayed.
            self._cache = self.apply_filter(self._raw_cache)

            # Remove hierarchical registers.
            self._cache = [i for i in self._cache if not i.is_hierarchical()]

            # Sort the registers to display.
            if self.sort_mode == HwHacker.SORT_MODE_BY_ADDRESS:
                self._cache.sort(key=lambda reg_info: (reg_info.address, -reg_info.mask, reg_info.name))
            elif self.sort_mode == HwHacker.SORT_MODE_BY_NAME:
                self._cache.sort(key=lambda reg_info: (reg_info.name))

            max_len = 10
            if self._cache:
                max_len = max([len(i.name) for i in self._cache])
            self.content = ["{1:<{0:d}s} {2:s}".format(max_len, i.name, self.build_line(i)) for i in self._cache]
        # End of update_content().

    def update_screen(self):
        self.scr.erase()
        self.scr.box()
        title = "{0:s} ({1:s})".format(self.hw.board_type_str(), self.hw.uri())
        self.scr.addstr(0, HwHacker.MARGIN_X, title)
        self.scr.noutrefresh()
        # End of update_screen().

    def init_pad(self):
        self.num_lines = 1
        self.num_columns = 0
        if self.content:
            self.num_lines = len(self.content)
            self.num_columns = max([len(i) for i in self.content])
        self.pad = curses.newpad(self.num_lines, self.num_columns + 1)
        self.pad.keypad(1)
        self.pad_may_need_resizing = False
        # End of init_pad().

    def update_pad(self):
        if (self.pad is None) or self.pad_may_need_resizing:
            self.init_pad()
        else:
            self.pad.erase()
        for (i, line) in enumerate(self.content):
            attr = curses.A_NORMAL
            if i == self.current_line:
                attr = curses.A_STANDOUT
            self.pad.addstr(i, 0, line, attr)
            if len(line) > self.num_columns_visible:
                marker = "->"
                self.pad.addstr(i, self.num_columns_visible - len(marker), marker, attr)
        num_lines = self.win_height
        num_cols = self.win_width
        self.pad.noutrefresh(self.top_line, 0,
                             HwHacker.MARGIN_Y, HwHacker.MARGIN_X,
                             num_lines - HwHacker.MARGIN_Y - 1, num_cols - HwHacker.MARGIN_X - 1)
        # End of update_pad()

    def run_input_handler(self):
        while True:

            # NOTE: This is a bit of a weird construct, but this works
            # around a bug in getkey(). Otherwise there is no way to
            # catch the KeyboardInterrupt.
            try:
                try:
                    input = self.pad.getkey()
                except:
                    pass
            except KeyboardInterrupt:
                break

            if input == "KEY_DOWN":
                self.scroll(HwHacker.SCROLL_DIR_DOWN, 1)
            elif input == "KEY_UP":
                self.scroll(HwHacker.SCROLL_DIR_UP, 1)
            elif input == "KEY_NPAGE":
                self.scroll(HwHacker.SCROLL_DIR_DOWN, self.num_lines_visible)
            elif input == "KEY_PPAGE":
                self.scroll(HwHacker.SCROLL_DIR_UP, self.num_lines_visible)
            elif input == "KEY_HOME":
                self.scroll_home()
            elif input == "KEY_END":
                self.scroll_end()

            elif input == "e":
                reg_info = self._cache[self.current_line]
                if reg_info.is_writable() and \
                        not reg_info.is_hierarchical():
                    reg_name = reg_info.name
                    msg = "Edit {0:s} register contents".format(reg_name)
                    tmp_contents = self.format_register_contents(reg_info)
                    contents_str_prev = "\n".join(tmp_contents)
                    contents_str_new = self.dialog_register_edit(msg, contents_str_prev)
                    if not contents_str_new is None:
                        tmp = contents_str_new.split("\n")
                        contents_new = [parse_as_int(i) for i in tmp]

                        if len(contents_new) == 1:
                            self.hw.write(reg_name, contents_new[0])
                        else:
                            self.hw.write_block(reg_name, contents_new)

                        self.invalidate_raw_cache()
                        # TODO TODO TODO
                        # NOTE: Should also correctly handle read-only and
                        # write-only (!) registers.
                        # NOTE: Should also take care with the register mask.
                        # TODO TODO TODO end

            elif input == "f":
                msg = "Filter register names by:"
                filter_prev = self.filter_pattern
                filter_new = self.dialog_text_input(msg, filter_prev)
                if (filter_new != filter_prev) and (not filter_new is None):
                    self.filter_pattern = filter_new
                    self.invalidate_raw_cache()
                    self.pad_may_need_resizing = True
                    self.top_line = 0
                    self.current_line = self.top_line

            elif input == "h":
                self.show_help()

            elif input == "q":
                break
            elif input == "r":
                self.invalidate_raw_cache()
            elif input == "s":
                self.toggle_sort_mode()
            elif input == "t":
                self.toggle_view_mode()
            elif input == "v":
                self.toggle_verbose()

            self.update()
        # End of run_input_handler().

    def scroll(self, direction, step):
        if direction == HwHacker.SCROLL_DIR_DOWN:
            if self.current_line + step <= self.bottom_line:
                self.current_line += step
            else:
                tmp_step = min(step, self.num_lines - self.bottom_line - 1)
                self.top_line += tmp_step
                if tmp_step != step:
                    self.current_line = self.bottom_line
                else:
                    self.current_line += step
        elif direction == HwHacker.SCROLL_DIR_UP:
            if self.current_line - step >= self.top_line:
                self.current_line -= step
            else:
                tmp_step = min(step, self.top_line)
                self.top_line -= tmp_step
                if tmp_step != step:
                    self.current_line = self.top_line
                else:
                    self.current_line -= step
        else:
            # ASSERT ASSERT ASSERT
            assert false
            # ASSERT ASSERT ASSERT end
        # End of scroll().

    def scroll_home(self):
        self.top_line = 0
        self.current_line = self.top_line
        # End of scroll_home().

    def scroll_end(self):
        self.top_line = self.num_lines - self.num_lines_visible
        self.current_line = self.num_lines - 1
        # End of scroll_end().

    def show_help(self):
        # TODO TODO TODO
        # This needs some actual implementation...
        help_text = ""
        # TODO TODO TODO end
        width  = self.win_width
        height = self.win_height
        title = "TCDS HwHacker Help"
        win = self.window(height, width, help_text, title)
        while True:
            if win.getkey():
                break
        # End of show_help().

    # End of class HwHacker.

###############################################################################

class HwHackerRunner(CmdLineBase):

    def __init__(self, description=None, usage=None, epilog=None):
        super(HwHackerRunner, self).__init__(description, usage, epilog)
        # End of __init__().

    def main(self):
        # NOTE: In order to make this thing robust (against, for
        # example, bad address tables) we have to 1) handle exceptions
        # properly in the code, but also 2) suppress complaints from
        # uhal cluttering up the user interface.
        uhal.disableLogging()

        self.validate_target(target_type=CmdLineBase.TARGET_TYPE_HW)
        board_info = self.setup.hw_targets[self.target]
        network_address = board_info.address
        controlhub_address = board_info.controlhub
        # carrier = get_carrier_hw(network_address, controlhub_address, verbose=self.verbose)

        (carrier_type, board_type) = identify_board(network_address,
                                                    controlhub_address,
                                                    verbose=self.verbose)
        if (carrier_type == BOARD_TYPE_UNKNOWN) and \
           (board_type == BOARD_TYPE_UNKNOWN):
            self.error("Could not identify the board.")
        dev = connect_to_identified_board(carrier_type,
                                          board_type,
                                          network_address,
                                          controlhub_address,
                                          verbose=self.verbose)
        if dev is None:
            self.error("Could not connect to the board.")

        try:
            curses.wrapper(HwHacker, dev)
        except Exception, err:
            self.error("Something went wrong: '{0:s}'.".format(err))

        # End of main().

    # End of class HwHackerRunner.

###############################################################################

if __name__ == "__main__":

    desc_str = "Hacker script to access the register contents of a TCDS IPbus device."

    esc_delay_ori = os.environ.get('ESCDELAY')
    # os.environ.setdefault('ESCDELAY', '25')

    res = HwHackerRunner(desc_str).run()

    if esc_delay_ori:
        os.environ.setdefault('ESCDELAY', esc_delay_ori)

    print "Done"
    sys.exit(res)

###############################################################################
