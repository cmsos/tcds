#!/usr/bin/env python

###############################################################################
## Check for the presence of the 40 MHz TTC clock to the user firmware.
###############################################################################

import sys

from pytcds.utils.tcds_utils_hw_connect import get_carrier_hw
from pytcds.utils.tcds_cmd_line_base import CmdLineBase

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class CheckTTCClock(CmdLineBase):

    def main(self):
        carrier = get_carrier_hw(self.ip_address, verbose=self.verbose)
        if not carrier:
            self.error("Could not connect to {0:s}.".format(self.ip_address))

        if self.verbose:
            print "Checking for presence of TTC clock."

        res = carrier.read("user.ttc_clock_mon.ttc_clock_up")
        if res != 0x1:
            raise RuntimeError("No TTC clock found on {0:s}.".format(self.network_address))

        # End of main().

    # End of class CheckTTCClock.

###############################################################################

if __name__ == "__main__":

    res = CheckTTCClock().run()
    print "Done"
    sys.exit(res)

###############################################################################
