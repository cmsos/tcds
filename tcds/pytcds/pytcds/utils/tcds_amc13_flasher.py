#!/usr/bin/env python

import math
import struct
import time

from collections import namedtuple

from pytcds.utils.tcds_constants import BOARD_ID_CPMT2
from pytcds.utils.tcds_constants import BOARD_ID_DUMT2
from pytcds.utils.tcds_utils_mcs import MCSFile

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

# Keep all the details about the PROM holding the firmware images in
# some fake class structure. Not super pretty...

class PROMInfoBase(object):

    def __init__(self):
        # The PROM component ID.
        self.name = "unknown"
        # The number of 32-bit IPbus words per PROM page.
        self.page_size = 0
        # The number of pages in a PROM sector.
        self.sector_size = 0
        # The total number of sectors in the PROM.
        self.prom_size = 0

        # Command to read the JEDEC identifier from the PROM.
        self.prom_command_read_identifier = 0x9f
        self.num_bytes_to_read_after_read_identifier = 3

        # Command to read the status register.
        self.prom_command_read_status_register1 = 0x05
        self.prom_command_read_status_register2 = 0x35
        self.prom_command_read_status_register3 = 0x15
        self.num_bytes_to_read_after_read_status_register = 1

        # Commands to enable/disable write access.
        self.prom_command_write_enable = 0x06
        self.num_bytes_to_read_after_write_enable = 0
        self.prom_command_write_disable = 0x04
        self.num_bytes_to_read_after_write_disable = 0

        # Command for a PROM page write.
        self.prom_command_page_program = 0x02
        # The page write command should be followed by three address
        # bytes, and then followed by the actual data to be written.
        self.num_bytes_to_write_after_page_program = 3

        # Command to read data (at higher speed).
        self.prom_command_read_data_bytes_fast = 0x0b
        # The read data bytes at higher speed command is followed by three
        # address bytes, and then followed by the number of bytes to be
        # read.
        self.num_bytes_to_write_after_read_data_bytes_fast = 4

        # Command to erase the full chip.
        self.prom_command_chip_erase = 0xc7
        self.num_bytes_to_write_after_chip_erase = 0

        # Command to erase a full PROM sector/block.
        # - For the M25P128 this is the command 'sector erase'.
        # - For the W25Q128JV this is the command 'block erase (64KB)'.
        self.prom_command_sector_erase = 0xd8
        # The sector/block erase command should be followed by three
        # address bytes.
        self.num_bytes_to_write_after_sector_erase = 3

        # The PROMs used 'erase' by setting bits from 0 to 1, so let's
        # pad with 1s.
        self.padding_byte = 0xff
        tmp = 0x0
        for i in xrange(AMC13Flasher.NUM_BYTES_PER_WORD):
            tmp <<= AMC13Flasher.NUM_BITS_PER_BYTE
            tmp |= self.padding_byte
        self.padding_word = tmp
        # End of __init__().

    # End of class PROMInfoBase.

# This one is present on all AMC13s.
class PROMInfoM25(PROMInfoBase):

    def __init__(self):
        super(PROMInfoM25, self).__init__()
        self.name = "M25P128"
        # - PROM pages are 64 (32-bit IPbus) words long (=256 bytes).
        self.page_size = 64
        # - PROM sectors are 1024 pages long (=2Mb).
        self.sector_size = 1024
        # - The full chip contains 65536 pages, or 64 sectors.
        self.prom_size = 64
        # End of __init__().

    # End of class PROMInfoM25.

# This one is present on the CPMs.
class PROMInfoW25(PROMInfoBase):

    def __init__(self):
        super(PROMInfoW25, self).__init__()
        self.name = "W25Q128JV"
        # - PROM pages are 64 (32-bit IPbus) words long (=256 bytes).
        self.page_size = 64
        # - PROM sectors are 256 pages long (=64KB).
        self.sector_size = 256
        # - The full chip contains 65536 pages, so 256 sectors.
        self.prom_size = 256
        # End of __init().

    # End of class PROMInfoW25.

###############################################################################

class AMC13Flasher(object):

    # Bytes are made up of eight bits.
    NUM_BITS_PER_BYTE = 8

    # Each 32-bit IPbus word is four bytes long.
    NUM_BYTES_PER_WORD = 4
    NUM_BITS_PER_WORD = NUM_BYTES_PER_WORD * NUM_BITS_PER_BYTE

    # These are the different AMC13/CPM models/generations.
    CARRIER_MODEL_UNKNOWN = 0
    CARRIER_MODEL_AMC13_ORI = 1
    CARRIER_MODEL_AMC13_NEW = 2
    CARRIER_MODEL_CPM = 3

    def __init__(self, amc13):
        self._amc13 = amc13
        # ASSERT ASSERT ASSERT
        # The AMC13 flash PROM interface passes through the T2, not
        # through the T1.
        assert (amc13.get_system_id() == BOARD_ID_CPMT2) or \
            (amc13.get_system_id() == BOARD_ID_DUMT2), \
            "ERROR AMC13 firmware programming goes through the T2, not the T1."
        # ASSERT ASSERT ASSERT end
        self._carrier_model = self._identify_carrier()
        assert (self._carrier_model != AMC13Flasher.CARRIER_MODEL_UNKNOWN), \
            "ERROR Failed to determine if this is an old/new AMC13, or a CPM."
        # ASSERT ASSERT ASSERT end
        if self._carrier_model == AMC13Flasher.CARRIER_MODEL_CPM:
            self._prom_info = PROMInfoW25()
        else:
            self._prom_info = PROMInfoM25()
        # End of __init__().

    def _identify_carrier(self):
        # There are basically three generations of AMC13/CPMs:
        # - Original:
        #   T1 FPGA: K325
        #   T2 FPGA: LX25
        #   These are identified by their serial number: < 47.
        #   NOTE: Serial numbers are nine bits wide.
        # - New-T2:
        #   T1 FPGA: K325
        #   T2 FPGA: LX45
        #   These are identified by their serial number: > 46.
        #   NOTE: Serial numbers are nine bits wide.
        # - CPM-style:
        #   T1 FPGA: K410
        #   T2 FPGA: LX45
        #   These are identified by a dedicated 'is_cpm' flag (which
        #   more or less serves as a tenth bit of the serial number).
        #   NOTE: Serial numbers are nine bits wide.
        #   NOTE: In order to support the larger images required for
        #         the K410, these T2s are equipped with a second PROM.

        carrier_model = AMC13Flasher.CARRIER_MODEL_UNKNOWN

        is_cpm = self._amc13.read("cpmt2.t2_identifier.is_cpm")

        if is_cpm:
            carrier_model = AMC13Flasher.CARRIER_MODEL_CPM
        else:
            t2_serial_number = self._amc13.read("cpmt2.t2_identifier.serial_number")
            if t2_serial_number < 47:
                carrier_model = AMC13Flasher.CARRIER_MODEL_AMC13_ORI
            else:
                carrier_model = AMC13Flasher.CARRIER_MODEL_AMC13_NEW

        # End of _identify_carrier().
        return carrier_model

    def _find_prom_location(self, image_name):
        """Determine the target PROM and address offset for the specified firmware image."""

        prom_num = None
        offset = None

        if (image_name == "header"):
            prom_num = 0
            offset = 0x00000000
        elif (image_name == "golden"):
            prom_num = 0
            if self._carrier_model == AMC13Flasher.CARRIER_MODEL_AMC13_ORI:
                offset = 0x00100000
            else:
                offset = 0x00080000
        elif (image_name == "t2"):
            prom_num = 0
            offset = 0x00200000
        elif (image_name == "t1"):
            if self._carrier_model == AMC13Flasher.CARRIER_MODEL_CPM:
                # NOTE: In this case the T1 image goes into the second
                # PROM, so this 0x0 is not the same as the 0x0 for the
                # header above.
                prom_num = 1
                offset = 0x00000000
            else:
                prom_num = 0
                offset = 0x00400000
        else:
            # ASSERT ASSERT ASSERT
            assert False, "ERROR image_name '{0:s}' not understood.".format(image_name)
            # ASSERT ASSERT ASSERT end

        # End of _find_prom_location().
        return (prom_num, offset)

    def _is_flash_busy(self):
        tmp = self._amc13.read("cpmt2.flash.control.flash_busy")
        flash_busy = (tmp != 0)
        # End of _is_flash_busy().
        return flash_busy

    def _execute_command(self, prom_num, command):
        command_tmp = command
        if prom_num:
            command_tmp |= 0x80000000
        self._amc13.write("cpmt2.flash.control.flash_cmd", command_tmp)
        while self._is_flash_busy():
            time.sleep(1.e-3)
        # End of _execute_command().

    def _read_prom_status_register(self, prom_num, status_register_num=1):
        if status_register_num == 1:
            prom_command = self._prom_info.prom_command_read_status_register1
        elif status_register_num == 2:
            prom_command = self._prom_info.prom_command_read_status_register2
        elif status_register_num == 3:
            prom_command = self._prom_info.prom_command_read_status_register3
        self._amc13.write("cpmt2.flash.flash_wbuf",
                          prom_command << 24)
        self._execute_command(prom_num,
                              self._prom_info.num_bytes_to_read_after_read_status_register)
        tmp_prom_status = self._amc13.read("cpmt2.flash.flash_rbuf")
        # NOTE: In order to align the one-byte result nicely with a
        # 32-bit word, we shift it a bit further to the right.
        num_bits_to_shift = AMC13Flasher.NUM_BITS_PER_WORD \
            - ((self._prom_info.num_bytes_to_read_after_read_status_register %
                AMC13Flasher.NUM_BITS_PER_WORD)
               * AMC13Flasher.NUM_BITS_PER_BYTE)
        prom_status = (tmp_prom_status >> num_bits_to_shift)
        # End of _read_prom_status_register().
        return prom_status

    def _read_prom_identification(self, prom_num):
        # The returned (JEDEC) identifier contains three bytes:
        # - a manufacturer ID,
        # - a memory type, and
        # - a capacity specifier.
        self._amc13.write("cpmt2.flash.flash_wbuf",
                          self._prom_info.prom_command_read_identifier << 24)
        self._execute_command(prom_num,
                              self._prom_info.num_bytes_to_read_after_read_identifier)
        tmp_prom_id = self._amc13.read("cpmt2.flash.flash_rbuf")
        # NOTE: In order to align the three-byte result nicely with a
        # 32-bit word, we shift it one more byte to the right.
        num_bits_to_shift = AMC13Flasher.NUM_BITS_PER_WORD \
            - ((self._prom_info.num_bytes_to_read_after_read_identifier %
                AMC13Flasher.NUM_BITS_PER_WORD)
               * AMC13Flasher.NUM_BITS_PER_BYTE)
        prom_id = (tmp_prom_id >> num_bits_to_shift)
        # End of _read_prom_identification().
        return prom_id

    def _enable_write(self, prom_num):
        self._amc13.write("cpmt2.flash.flash_wbuf",
                          self._prom_info.prom_command_write_enable << 24)
        self._execute_command(prom_num,
                              self._prom_info.num_bytes_to_read_after_write_enable)
        # End of _enable_write().

    def _disable_write(self, prom_num):
        self._amc13.write("cpmt2.flash.flash_wbuf",
                          self._prom_info.prom_command_write_disable << 24)
        self._execute_command(prom_num,
                              self._prom_info.num_bytes_to_read_after_write_disable)
        # End of _disable_write().

    def _wait_for_write_done(self, prom_num):
        read_done = False
        while not read_done:
            prom_status = self._read_prom_status_register(prom_num)
            # print "DEBUG JGH PROM {0:d} status = 0x{1:x}".format(prom_num, prom_status)
            # The first bit is the 'write in progress' bit.
            tmp = prom_status & 0x1
            read_done = (tmp == 0x0)
            time.sleep(1.e-3)
        # End of _wait_for_write_done().

    def _erase_sector(self, prom_num, sector_address):
        self._enable_write(prom_num)
        self._amc13.write("cpmt2.flash.flash_wbuf",
                          (self._prom_info.prom_command_sector_erase << 24) | sector_address)
        self._execute_command(prom_num,
                              self._prom_info.num_bytes_to_write_after_sector_erase)
        self._wait_for_write_done(prom_num)
        self._disable_write(prom_num)
        # End of _erase_sector().

    def _read_page(self, prom_num, page_address):
        self._amc13.write("cpmt2.flash.flash_wbuf",
                          (self._prom_info.prom_command_read_data_bytes_fast << 24) | page_address)
        num_bytes = self._prom_info.num_bytes_to_write_after_read_data_bytes_fast \
            + (self._prom_info.page_size * AMC13Flasher.NUM_BYTES_PER_WORD)
        self._execute_command(prom_num, num_bytes)
        res = self._amc13.read_block("cpmt2.flash.flash_rbuf",
                                     self._prom_info.page_size)
        # End of _read_page().
        return res

    def _write_page(self, prom_num, page_address, page_data):
        self._enable_write(prom_num)
        data = [(self._prom_info.prom_command_page_program << 24) | page_address]
        data.extend(page_data)
        self._amc13.write_block("cpmt2.flash.flash_wbuf", data)
        num_bytes_to_write = self._prom_info.num_bytes_to_write_after_page_program \
            + len(page_data * AMC13Flasher.NUM_BYTES_PER_WORD)
        self._execute_command(prom_num, num_bytes_to_write)
        self._wait_for_write_done(prom_num)
        self._disable_write(prom_num)
        # End of _write_page().

    def _erase_chip(self, prom_num):
        self._enable_write(prom_num)
        self._amc13.write("cpmt2.flash.flash_wbuf",
                          self._prom_info.prom_command_chip_erase << 24)
        self._execute_command(prom_num,
                              self._prom_info.num_bytes_to_write_after_chip_erase)
        self._wait_for_write_done(prom_num)
        self._disable_write(prom_num)
        # End of _erase_chip().

    def _read_chip(self, prom_num):
        image_offset = 0x00000000
        flash_data = []
        helper = 0
        num_pages_read = 0
        num_pages = self._prom_info.sector_size * self._prom_info.prom_size

        for page_num in xrange(num_pages):
            page_address = image_offset \
                + page_num * \
                self._prom_info.page_size * \
                AMC13Flasher.NUM_BYTES_PER_WORD
            page_data = self._read_page(prom_num, page_address)
            num_pages_read += 1
            if num_pages_read >= helper:
                perc = 100. * num_pages_read / num_pages
                print "{0:5.1f}% read".format(perc)
                helper += math.floor(1. * num_pages / 10)
                helper = min(helper, num_pages)
            flash_data.extend(page_data)

        # End of _read_chip().
        return flash_data

    def read_prom(self):
        prom_nums = [0]
        if self._carrier_model == AMC13Flasher.CARRIER_MODEL_CPM:
            prom_nums.append(1)
        flash_data = {}
        for prom_num in prom_nums:
            print "PROM {0:d}".format(prom_num)
            flash_data[prom_num] = self._read_chip(prom_num)
        # End of read_prom().
        return flash_data

    def verify(self, image_name, image_file_name):
        mcs_parser = MCSFile(image_file_name)
        mcs_parser.parse_file()
        # NOTE: The image data from the bitstream is a list of bytes
        # (i.e., not words).
        image_data_bytes = mcs_parser.bitstream()
        num_bytes = len(image_data_bytes)
        # Unpack the image data into 32-bit words.
        fmt = ">{0:d}I".format(num_bytes // AMC13Flasher.NUM_BYTES_PER_WORD)
        image_data_words = list(struct.unpack(fmt, image_data_bytes))
        num_words = num_bytes / AMC13Flasher.NUM_BYTES_PER_WORD
        num_pages = int(math.ceil(1. * num_words / self._prom_info.page_size))
        (prom_num, image_offset) = self._find_prom_location(image_name)
        flash_data = []
        helper = 0
        num_pages_read = 0
        for page_num in xrange(num_pages):
            page_address = image_offset + page_num * self._prom_info.page_size * AMC13Flasher.NUM_BYTES_PER_WORD
            page_data = self._read_page(page_address)
            num_pages_read += 1
            if num_pages_read >= helper:
                perc = 100. * num_pages_read / num_pages
                print "{0:5.1f}% read".format(perc)
                helper += math.floor(1. * num_pages / 10)
                helper = min(helper, num_pages)
            flash_data.extend(page_data)
        # Do the comparison of the part of the data that exists both
        # in the MCS file and in the PROM.
        # ASSERT ASSERT ASSERT
        # Of course the PROM data should be equal to or larger than
        # the file data. Right?
        assert (len(image_data_words) <= len(flash_data)), \
            "ERROR Is the MCS file larger than the PROM?"
        # ASSERT ASSERT ASSERT end
        tmp_flash = flash_data[:len(image_data_words)]
        tmp_mcs = image_data_words
        if tmp_flash != tmp_mcs:
            msg = "Found a difference between the '{0:s}' PROM image " \
                "and MCS file '{1:s}'."
            raise RuntimeError(msg.format(image_name, image_file_name))
        # End of verify().

    def write(self, image_name, image_file_name):
        mcs_parser = MCSFile(image_file_name)
        mcs_parser.parse_file()
        # NOTE: The image data from the bitstream is a list of bytes
        # (i.e., not words).
        image_data_bytes = mcs_parser.bitstream()
        num_bytes = len(image_data_bytes)
        # Unpack the image data into 32-bit words.
        fmt = ">{0:d}I".format(num_bytes // AMC13Flasher.NUM_BYTES_PER_WORD)
        image_data_words = list(struct.unpack(fmt, image_data_bytes))
        # Now pad with zero to a multiple of a firmware page.
        padding_length = (self._prom_info.page_size - (len(image_data_words) % self._prom_info.page_size))
        image_data_words.extend([self._prom_info.padding_word] * padding_length)
        num_pages = len(image_data_words) / self._prom_info.page_size
        (prom_num, image_offset) = self._find_prom_location(image_name)

        # First erase enough sectors to make room for the image.
        # NOTE: Erasing happens per sector, programming happens per
        # page.
        num_sectors = int(math.ceil(1. * num_pages / self._prom_info.sector_size))
        helper = 0
        num_sectors_erased = 0
        num_bytes_per_sector = self._prom_info.sector_size * \
                               self._prom_info.page_size * \
                               AMC13Flasher.NUM_BYTES_PER_WORD
        for sector_num in xrange(num_sectors):
            # NOTE: The sector (start) address is in bytes, so needs
            # some computation.
            sector_address = image_offset + (sector_num * num_bytes_per_sector)
            self._erase_sector(prom_num, sector_address)
            num_sectors_erased += 1
            if num_sectors_erased >= helper:
                perc = 100. * num_sectors_erased / num_sectors
                print "{0:5.1f}% erased".format(perc)
                helper += math.floor(1. * num_sectors / 10)
                helper = min(helper, num_sectors)

        # Now program the new image.
        helper = 0
        num_pages_written = 0
        for page_num in xrange(num_pages):
            page_address = image_offset \
                + page_num * \
                self._prom_info.page_size * \
                AMC13Flasher.NUM_BYTES_PER_WORD
            ind_lo = page_num * self._prom_info.page_size
            ind_hi = (page_num + 1) * self._prom_info.page_size
            page_data = image_data_words[ind_lo:ind_hi]
            self._write_page(prom_num, page_address, page_data)
            num_pages_written += 1
            if num_pages_written >= helper:
                perc = 100. * num_pages_written / num_pages
                print "{0:5.1f}% written".format(perc)
                helper += math.floor(1. * num_pages / 10)
                helper = min(helper, num_pages)

        # End of write().

    def erase_prom(self):
        prom_nums = [0]
        if self._carrier_model == AMC13Flasher.CARRIER_MODEL_CPM:
            prom_nums.append(1)
        for prom_num in prom_nums:
            print "PROM {0:d}".format(prom_num)
            self._erase_chip(prom_num)
        # End of erase_prom().

    # End of class AMC13Flasher.

###############################################################################
