###############################################################################
## Utilities related to the FC7 FMC-carrier board.
###############################################################################

import gzip
import struct
import time

from pytcds.utils.tcds_carrier import Carrier
from pytcds.utils.tcds_constants import BOARD_TYPE_FC7
from pytcds.utils.tcds_fc7_mmc_interface import PipeDream
from pytcds.utils.tcds_i2c import I2CAccessor
from pytcds.utils.tcds_utils_misc import chunkify

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

# The time to wait after a firmware (re)load, in seconds.
# NOTE: When connecting directly to the board, one second is enough
# for the board to load the firmware and become responsive again. (Any
# attempt to communicate with the board before that time will lock it
# up completely.) When connecting through the controlhub there is a
# cache with a lifetime of 15 seconds. Only after 15 seconds of
# silence will the cache be destroyed and the next connection
# successfull. Another possibility is to try a dummy read/write and
# let that time-out. The side-effect will be the destruction of the
# cache associated with this connection.
TIME_WAIT_FIRMWARE_LOAD = 1

# Some magic constants related to cross-point switches and clock routing.
# These are the different sources of the clock, basically.
# - Select FCLKA (the clock coming from the backplane).
TTC_XPOINT_A_SRC_FCLKA = 0x0
# - Select the on-board default clock (xtal/coax).
TTC_XPOINT_A_SRC_XTAL_OR_COAX = 0x3

# Two select values for the CDCE xpoint switch (U42):
# - select the 240 MHz clock from CDCE u0,
CDCE_XPOINT_SRC_240MHZ = 0x0
# - select the 160 MHz clock from CDCE u1.
CDCE_XPOINT_SRC_160MHZ = 0x1

##########

# The duration of the 'settle time' for CDCE resyncs (in seconds).
SLEEP_TIME_CDCE_RESYNC = .5

# The duration of the propagation time of the I2C broadcast to the
# LTC2990 chips.
SLEEP_TIME_LTC2990_BROADCAST = .1

SLEEP_TIME_LTC2990_MEASUREMENT = .1

##########

# The I2C address of the EEPROM with the EUI-48.
# NOTE: Should be something like a 24AA025E48.
I2C_ADDRESS_EUI_EEPROM = 0x50

# This is the I2C address of the EEPROM storing the raw MAC and IP
# addresses.
I2C_ADDRESS_MAC_AND_IP_EEPROM = 0x50

# The address of the I2C multiplexer switching between the different
# SFP status types on the TCDS FMCs and the FMC module type register.
# NOTE: Adresses depend on the FMC number.
I2C_ADDRESS_MUX_SFP_STATUS = {
    "l8" : 0x77,
    "l12" : 0x74
}

# The address of the I2C multiplexer switching between the different
# SFPs on the TCDS FMCs.
# NOTE: Adresses depend on the FMC number.
I2C_ADDRESS_MUX_SFP_INFO_REGISTERS = {
    "l8" : 0x73,
    "l12" : 0x70
}

# L12 is the top FMC, L8 the bottom.
FMC_NAMES = ["l12", "l8"]

FMC_I2C_BUS_SELECTORS = {
    "l8" : 1,
    "l12" : 0
}

###############################################################################

class FC7(Carrier):

    def __init__(self, carrier_type, board_type, hw_interface, verbose=False):
        # ASSERT ASSERT ASSERT
        assert carrier_type == BOARD_TYPE_FC7
        # ASSERT ASSERT ASSERT end
        super(FC7, self).__init__(carrier_type, board_type, hw_interface, verbose)
        self.user_fw_base_name = "fc7"
        # End of __init__().

    def get_firmware_version(self, which="system"):
        """ Read and return the system firmware version."""
        fw_ver_major = self.read("{0:s}.firmware_id.ver_major".format(which))
        fw_ver_minor = self.read("{0:s}.firmware_id.ver_minor".format(which))
        fw_ver_build = self.read("{0:s}.firmware_id.ver_build".format(which))
        fw_version = "{0:d}.{1:d}.{2:d}"
        fw_version = fw_version.format(fw_ver_major, fw_ver_minor, fw_ver_build)
        # End of get_firmware_version().
        return fw_version

    def get_firmware_date(self, which="system"):
        """ Read and return the system firmware date."""
        fw_date_yy = self.read("{0:s}.firmware_id.date_yy".format(which))
        fw_date_mm = self.read("{0:s}.firmware_id.date_mm".format(which))
        fw_date_dd = self.read("{0:s}.firmware_id.date_dd".format(which))
        fw_date = "20{0:d}-{1:d}-{2:d}"
        fw_date = fw_date.format(fw_date_yy, fw_date_mm, fw_date_dd)
        # End of get_firmware_date().
        return fw_date

    def choose_clock_source(self, source):
        # Configure the cross-point switches defining the clocking
        # scheme.
        src_val = None
        tmp_source = source.lower()
        if tmp_source == "fclka":
            src_val = TTC_XPOINT_A_SRC_FCLKA
        elif tmp_source == "front_input":
            src_val = TTC_XPOINT_A_SRC_XTAL_OR_COAX
            self.write("fc7.ctrl.osc_coax_sel", 0x0)
        elif tmp_source == "xtal":
            src_val = TTC_XPOINT_A_SRC_XTAL_OR_COAX
            self.write("fc7.ctrl.osc_coax_sel", 0x1)
        else:
            # ASSERT ASSERT ASSERT
            assert False, "Clock source '{0:s}' not understood.".format(source)
            # ASSERT ASSERT ASSERT end

        # Switch all inputs of the master xpoint switch to the same
        # input.
        self.write("fc7.ctrl.master_xpoint_s1", src_val)
        self.write("fc7.ctrl.master_xpoint_s2", src_val)
        self.write("fc7.ctrl.master_xpoint_s3", src_val)
        self.write("fc7.ctrl.master_xpoint_s4", src_val)

        # TODO TODO TODO
        # This may need some documentation.
        self.write("fc7.ctrl.cdce_xpoint_s2", CDCE_XPOINT_SRC_160MHZ)
        self.write("fc7.ctrl.cdce_xpoint_s3", CDCE_XPOINT_SRC_160MHZ)
        self.write("fc7.ctrl.cdce_xpoint_s4", CDCE_XPOINT_SRC_160MHZ)
        self.write("fc7.ctrl.cdce_refsel", 0x0)
        # TODO TODO TODO end

        # self.cdce_resync()

        # Reset the (firmware) TTC-clock PLL.
        self.write("user.ttc_clock_pll_reset", 0x1)
        self.write("user.ttc_clock_pll_reset", 0x0)

        # End of choose_clock_source().

    def cdce_resync(self):
        # Obtain control over the CDCE.
        self.write("fc7.ctrl.cdce_ctrl_sel", 1)

        # Power-cycle the CDCE.
        self.write("fc7.ctrl.cdce_powerup", 0)
        self.write("fc7.ctrl.cdce_powerup", 1)
        time.sleep(SLEEP_TIME_CDCE_RESYNC)

        # Trigger a resync of the CDCE.
        self.write("fc7.ctrl.cdce_sync", 0)
        self.write("fc7.ctrl.cdce_sync", 1)
        time.sleep(SLEEP_TIME_CDCE_RESYNC)

        # Release control of the CDCE.
        self.write("fc7.ctrl.cdce_ctrl_sel", 0)
        # End of cdce_resync().

    def perform_powerup_sequence(self):
        # NOTE: Nothing is set to zero or switched off explicitly at
        # the start of this method. So it's safe to call this method
        # multiple times. (I.e., this method should not introduce any
        # glitches.)

        # NOTE: There are FC7s with and without adjustable voltage
        # settings. For the FC7s without, the I2C access to the
        # digital potentiometers will fail (since they do not exist).

        # Check all FMC suply voltages.
        tmp = self.check_fmc_supply_voltages()
        if not tmp[0]:
            # Set the FMC VADJ supply voltage.
            self.set_fmc_vadj()

        # Check all FMC suply voltages.
        tmp = self.check_fmc_supply_voltages()
        if not tmp[0]:
            raise RuntimeError(tmp[1])

        # Enable the FMCs if present.
        self.powerup_fmcs()

        # Verify that the FMCs report power-good back to the carrier.
        for fmc in FMC_NAMES:
            fmc_present = self.read("fc7.status.fmc_{0:s}_present".format(fmc))
            if fmc_present:
                reg_name = "fc7.status.fmc_{0:s}_power_good_m2c".format(fmc)
                power_good = self.read(reg_name)
                if not power_good:
                    msg_tmp = "FMC {0:s} reports power is not good."
                    raise RuntimeError(msg_tmp.format(fmc))

        # End of perform_powerup_sequence().

    def powerup_fmcs(self):
        for fmc in FMC_NAMES:
            fmc_present = self.read("fc7.status.fmc_{0:s}_present".format(fmc))
            reg_name = "fc7.ctrl2.fmc_{0:s}_power_enable".format(fmc)
            if fmc_present:
                self.write(reg_name, 0x1)
            else:
                self.write(reg_name, 0x0)
        self.write("fc7.ctrl2.fmc_power_good_c2m", 0x1)
        # End of powerup_fmcs().

    def powerdown_fmcs(self):
        self.write("fc7.ctrl2.fmc_power_good_c2m", 0x0)
        for fmc in FMC_NAMES:
            reg_name = "fc7.ctrl2.fmc_{0:s}_power_enable".format(fmc)
            self.write(reg_name, 0x0)
        # End of powerup_fmcs().

    def get_fmc_locations(self):
        # End of get_fmc_locations().
        return FMC_NAMES

    def get_fmc_i2c_bus_select(self, fmc):
        bus_select = FMC_I2C_BUS_SELECTORS[fmc]
        # End of get_fmc_i2c_bus_select().
        return bus_select

    def get_fmc_i2c_address_mux_sfp_status(self, fmc):
        i2c_address = I2C_ADDRESS_MUX_SFP_STATUS[fmc]
        # End of get_fmc_i2c_address_mux_sfp_status().
        return i2c_address

    def get_fmc_i2c_address_mux_sfp_info_registers(self, fmc):
        i2c_address = I2C_ADDRESS_MUX_SFP_INFO_REGISTERS[fmc]
        # End of get_fmc_i2c_address_mux_sfp_info_registers().
        return i2c_address

    def is_fmc_powered(self, fmc):
        reg_name = "fc7.ctrl2.fmc_{0:s}_power_enable".format(fmc)
        tmp = self.read(reg_name)
        res = (tmp == 0x1)
        # End of is_fmc_powered().
        return res

    # # BUG BUG BUG
    # # Working on this.
    # def read_voltages(self):
    #     # All based on the FC7 R0 schematics:
    #     #   https://svnweb.cern.ch/cern/wsvn/ph-ese/be/fc7/trunk/fc7/doc/fc7_r0_schematics.pdf

    #     # U82-V1, I2C bus power_mon1, I2C address 0b1001100.
    #     V_1V = 1
    #     V_1V8 = 2
    #     V_2V5 = 3
    #     V_3V3 = 4
    #     V_5V = 5
    #     V_12V = 6
    #     # NOTE: The 'AB' voltages are the GTX supply voltages.
    #     V_1VAB = 11
    #     V_1V2AB = 12
    #     V_1V8AB = 13
    #     V_VADJL8 = 21
    #     V_VADJL12 = 22
    #     # NOTE: The following voltages are measured _on_ the FMCs.
    #     # BUG BUG BUG
    #     # TODO
    #     # BUG BUG BUG end

    #     # The bus selection is done using U37. The bus number is the
    #     # U37 channel number.
    #     I2C_BUSSES_LTC2990 = {
    #         V_1V : 1
    #         }

    #     I2C_ADDRESSES_LTC2990 = {
    #         V_1V : 0x4c
    #         }

    #     VOLTAGE_LABELS = {
    #         V_1V : "1V"
    #         }

    #     # U37, PCA9548A.
    #     I2C_ADDRESS_POWERMON_MUX = 0x70

    #     # See the LTC2990 data sheet for details.
    #     I2C_ADDRESS_LTC2990_BROADCAST = 0x77
    #     LTC2990_REG_ADDRESS_STATUS = 0x0
    #     LTC2990_REG_ADDRESS_CTRL = 0x1
    #     LTC2990_REG_ADDRESS_TRIGGER = 0x2
    #     LTC2990_REG_ADDRESS_V1_MSB = 0x6
    #     LTC2990_REG_ADDRESS_V1_LSB = 0x7
    #     LTC2990_REG_ADDRESS_V2_MSB = 0x8
    #     LTC2990_REG_ADDRESS_V2_LSB = 0x9
    #     LTC2990_REG_ADDRESS_V3_MSB = 0xa
    #     LTC2990_REG_ADDRESS_V3_LSB = 0xb
    #     LTC2990_REG_ADDRESS_V4_MSB = 0xc
    #     LTC2990_REG_ADDRESS_V4_LSB = 0xd
    #     LTC2990_CTRL_SINGLE_ACQ_SINGLE = 0x40
    #     LTC2990_CTRL_MEAS_MODE_ALL_VOLTAGES = 0x1f
    #     LTC_ADC_STEP = 5. / 2**14

    #     #----------

    #     # BUG BUG BUG
    #     # Should be promoted to class-level...
    #     def read_ltc2990_raw_voltages(i2c_address_ltc2990):
    #         # NOTE: Watch out with the addressing. This loop works
    #         # because of the way the four voltage measurements are
    #         # stored in the LTC2990.
    #         # See the LTC2990 data sheet for details.
    #         res_tmp = []
    #         with I2CAccessor(self, "system", 0) as i2c:
    #             for reg_address in xrange(LTC2990_REG_ADDRESS_V1_MSB,
    #                                       LTC2990_REG_ADDRESS_V4_LSB + 1):
    #                 i2c.write_register(i2c_address_ltc2990, reg_address)
    #                 tmp = i2c.read_register(i2c_address_ltc2990)
    #                 res_tmp.append(tmp)
    #         # Glue together the LSBs and MSBs.
    #         res = []
    #         for i in xrange(len(res_tmp) / 2):
    #             ind = 2 * i
    #             # First check the top bit: the data-valid bit.
    #             if (res_tmp[ind] & 0x80) == 0:
    #                 print "BUG JGH ", res_tmp[ind] & 0x80
    #                 pdb.set_trace()
    #             # Mask out the top two bits: the data-valid bit and
    #             # the sign bit.
    #             part1 = (res_tmp[ind] & 0x3f) << 8
    #             part2 = res_tmp[ind + 1]
    #             res.append(part1 + part2)
    #         # End of read_ltc2990_raw_voltages().
    #         return res
    #     # BUG BUG BUG end

    #     #----------

    #     # Let's get to work.
    #     voltage = V_1V

    #     with I2CAccessor(self, "system", 0) as i2c:
    #         # Select the correct I2C bus.
    #         channel_num = I2C_BUSSES_LTC2990[voltage]
    #         print "DEBUG JGH selecting powermon I2C bus"
    #         i2c.write_register(I2C_ADDRESS_POWERMON_MUX, (0x1 << channel_num))

    #         # Configure the LTC2990 chip for single-acquisition
    #         # four-voltage measurements.
    #         ctrl = LTC2990_CTRL_SINGLE_ACQ_SINGLE | LTC2990_CTRL_MEAS_MODE_ALL_VOLTAGES
    #         print "DEBUG JGH configuring LTC2990"
    #         i2c.write_memory(I2C_ADDRESSES_LTC2990[voltage],
    #                          LTC2990_REG_ADDRESS_CTRL,
    #                          ctrl)

    #         # Trigger a measurement.
    #         print "DEBUG JGH triggering LTC2990"
    #         i2c.write_memory(I2C_ADDRESSES_LTC2990[voltage],
    #                          LTC2990_REG_ADDRESS_TRIGGER,
    #                          0x1)

    #         # Wait a bit, and hope the measurement is done afterwards.
    #         time.sleep(SLEEP_TIME_LTC2990_MEASUREMENT)

    #     # Read the raw measurement result.
    #     print "DEBUG JGH reading out LTC2990"
    #     raw_meas = read_ltc2990_raw_voltages(I2C_ADDRESSES_LTC2990[voltage])
    #     pdb.set_trace()

    #     # Convert the raw measurement result.

    #     #----------

    #     # End of read_voltages().
    #     return res
    # # BUG BUG BUG end

    def read_fmc_supply_voltages(self):
        """Take a synchronous measurement of all FMC supply voltages."""

        # The I2C address of U18, the LTC2990 connected to L8_VADJ.
        # - V2: L8_VADJ
        # - V4: +3V3
        I2C_ADDRESS_LTC2990_FMCL8_A = 0x4c
        # The I2C address of U83, the LTC2990 connected to the 12V for FMC-L8.
        # - V2: +12V
        I2C_ADDRESS_LTC2990_FMCL8_B = 0x4d

        # The I2C address of U73, the LTC2990 connected to L12_VADJ.
        # - V2: L12_VADJ
        # - V4: +3V3
        I2C_ADDRESS_LTC2990_FMCL12_A = 0x4e
        # The I2C address of U74, the LTC2990 connected to the 12V for FMC-L8.
        # - V2: +12V
        I2C_ADDRESS_LTC2990_FMCL12_B = 0x4f

        # See the LTC2990 data sheet for details.
        I2C_ADDRESS_LTC2990_BROADCAST = 0x77
        LTC2990_REG_ADDRESS_STATUS = 0x0
        LTC2990_REG_ADDRESS_CTRL = 0x1
        LTC2990_REG_ADDRESS_TRIGGER = 0x2
        LTC2990_REG_ADDRESS_V1_MSB = 0x6
        LTC2990_REG_ADDRESS_V1_LSB = 0x7
        LTC2990_REG_ADDRESS_V2_MSB = 0x8
        LTC2990_REG_ADDRESS_V2_LSB = 0x9
        LTC2990_REG_ADDRESS_V3_MSB = 0xa
        LTC2990_REG_ADDRESS_V3_LSB = 0xb
        LTC2990_REG_ADDRESS_V4_MSB = 0xc
        LTC2990_REG_ADDRESS_V4_LSB = 0xd
        LTC2990_CTRL_SINGLE_ACQ_SINGLE = 0x40
        LTC2990_CTRL_MEAS_MODE_ALL_VOLTAGES = 0x1f
        LTC_ADC_STEP = 5. / 2**14

        ctrl = LTC2990_CTRL_SINGLE_ACQ_SINGLE | LTC2990_CTRL_MEAS_MODE_ALL_VOLTAGES
        with I2CAccessor(self, "system", 0) as i2c:
            # Configure all four voltage measurement chips for
            # single-acquisition four-voltage measurements.
            i2c.write_memory(I2C_ADDRESS_LTC2990_BROADCAST,
                             LTC2990_REG_ADDRESS_CTRL,
                             ctrl)
            # Trigger a measurement on all four chips.
            # NOTE: This broadcast needs a bit of time to take effect.
            i2c.write_memory(I2C_ADDRESS_LTC2990_BROADCAST,
                             LTC2990_REG_ADDRESS_TRIGGER,
                             0x1)
        time.sleep(SLEEP_TIME_LTC2990_BROADCAST)

        # BUG BUG BUG
        # Should be promoted to class-level...
        def read_ltc2990_raw_voltages(i2c_address_ltc2990):
            # NOTE: Watch out with the addressing. This loop works
            # because of the way the four voltage measurements are
            # stored in the LTC2990.
            # See the LTC2990 data sheet for details.
            res_tmp = []
            with I2CAccessor(self, "system", 0) as i2c:
                for reg_address in xrange(LTC2990_REG_ADDRESS_V1_MSB,
                                          LTC2990_REG_ADDRESS_V4_LSB + 1):
                    i2c.write_register(i2c_address_ltc2990, reg_address)
                    tmp = i2c.read_register(i2c_address_ltc2990)
                    res_tmp.append(tmp)
            # Glue together the LSBs and MSBs.
            res = []
            for i in xrange(len(res_tmp) / 2):
                ind = 2 * i
                # First check the top bit: the data-valid bit.
                if (res_tmp[ind] & 0x80) == 0:
                    print "BUG JGH ", res_tmp[ind] & 0x80
                    pdb.set_trace()
                # Mask out the top two bits: the data-valid bit and
                # the sign bit.
                part1 = (res_tmp[ind] & 0x3f) << 8
                part2 = res_tmp[ind + 1]
                res.append(part1 + part2)
            # End of read_ltc2990_raw_voltages().
            return res
        # BUG BUG BUG end

        # Obtain the raw voltage measurements for FMC-L8.
        raw_fmcl8_a = read_ltc2990_raw_voltages(I2C_ADDRESS_LTC2990_FMCL8_A)
        raw_fmcl8_b = read_ltc2990_raw_voltages(I2C_ADDRESS_LTC2990_FMCL8_B)
        # Convert from raw measurements to voltages using the
        # values of the external voltage dividers.
        # - For all four inputs to U18: (4.7 + 4.7) / 4.7 = 2.
        div_scaler_a = 2.
        fmcl8_a = [(div_scaler_a * i * LTC_ADC_STEP) for i in raw_fmcl8_a]
        # - For all four inputs to U83: (4.7 + 1.18) / 1.18 = 4.9831.
        div_scaler_b = (4.7 + 1.18) / 1.18
        fmcl8_b = [(div_scaler_b * i * LTC_ADC_STEP) for i in raw_fmcl8_b]

        # Now follow the same procedure for FMC-L12.
        raw_fmcl12_a = read_ltc2990_raw_voltages(I2C_ADDRESS_LTC2990_FMCL12_A)
        raw_fmcl12_b = read_ltc2990_raw_voltages(I2C_ADDRESS_LTC2990_FMCL12_B)
        fmcl12_a = [(div_scaler_a * i * LTC_ADC_STEP) for i in raw_fmcl12_a]
        fmcl12_b = [(div_scaler_b * i * LTC_ADC_STEP) for i in raw_fmcl12_b]

        # Build a nice dictionary containing all results in an
        # understandable format.
        res = {
            "fmc-l8" : {
                "3v3" : fmcl8_a[3],
                "12v" : fmcl8_b[1],
                "vadj" : fmcl8_a[1]
                },
            "fmc-l12" : {
                "3v3" : fmcl12_a[3],
                "12v" : fmcl12_b[1],
                "vadj" : fmcl12_a[1]
                }
        }

        # End of read_fmc_supply_voltages().
        return res

    def check_fmc_supply_voltages(self):
        """Checks if all FMC supply voltages are within their tolerances."""
        FMC_VOLTAGE_VALUES = {
            "3v3" : 3.3,
            "12v" : 12.,
            "vadj" : 2.5
            }
        # The fractional tolerance on the FMC voltages.
        FMC_VOLTAGE_TOLERANCES = {
            "3v3" : .02,
            "12v" : .05,
            "vadj" : .05
            }
        all_ok = True
        msg = None
        voltages = self.read_fmc_supply_voltages()
        fmc_keys = voltages.keys()
        msg = None
        msg_lines = []
        for fmc in fmc_keys:
            for (voltage, volt_meas) in voltages[fmc].iteritems():
                volt_expected = FMC_VOLTAGE_VALUES[voltage]
                tolerance = FMC_VOLTAGE_TOLERANCES[voltage]
                if (volt_meas < (1. - tolerance) * volt_expected) or \
                   (volt_meas > (1. + tolerance) * volt_expected):
                    all_ok = False
                    msg_tmp = "FMC supply voltage {0:s} {1:s} not correct: " \
                              "{2:.2f} V instead of {3:.2f} V +/- {4:.1f}%."
                    msg_lines.append(msg_tmp.format(fmc, voltage,
                                                    volt_meas, volt_expected,
                                                    100. * tolerance))
        msg = " ".join(msg_lines)
        res = (all_ok, msg)
        # End of check_fmc_supply_voltages().
        return res

    def set_fmc_vadj(self):
        # The adjustable voltage required by the TCDS FMCs is
        # determined by the fact that LVDS signals are used. Ergo: we
        # need vadj = 2V5.

        # The I2C address of the digitial potentiometer regulating
        # VADJ for both FMCs.
        # - FMC-L8 VADJ connected to B1 (channel 1).
        # - FMC-L12 VADJ connected to B2 (channel 2).
        I2C_ADDRESS_AD5172 = 0x2f

        # The correct channel instruction bits for FMC-L8/L12.
        AD5172_INST_L8 = 0x0
        AD5172_INST_L12 = 0x80
        # The AND mask for the AD5172 instruction byte.
        # See the data sheet for details.
        AD5172_INST_AND_MASK = 0x88

        # Magic number representing the correct voltage.
        P2V5 = 0xff - 0x2b

        with I2CAccessor(self, "system", 0) as i2c:
            # Set VADJ for FMC-L8.
            instr = AD5172_INST_L8 & AD5172_INST_AND_MASK
            i2c.write_memory(I2C_ADDRESS_AD5172, instr, P2V5)
            # Set VADJ for FMC-L12.
            instr = AD5172_INST_L12 & AD5172_INST_AND_MASK
            i2c.write_memory(I2C_ADDRESS_AD5172, instr, P2V5)
        # End of set_fmc_vadj().

    def determine_sd_file_name(self, image_name):
        """Returns the file name corresponding to the given image.

        This method returns the name of the file on the SD card
        corresponding to the image name given.

        """

        sd_file_name = None
        if image_name == "golden":
            sd_file_name = "GoldenImage.bin"
        elif image_name == "user":
            sd_file_name = "UserImage.bin"
        elif image_name == "test":
            sd_file_name = "TestImage.bin"
        else:
            # ASSERT ASSERT ASSERT
            msg = "ERROR Unknown firmware image: '{0:s}'.".format(image_name)
            assert False, msg
            # ASSERT ASSERT ASSERT end

        # End of determine_image_name()
        return sd_file_name

    def read_firmware(self, image_name, image_file_name, verbose=None):
        """Reads a firmware image from an FC7 SD card."""
        if verbose is None:
            verbose = self.verbose

        if not image_name in ["golden", "user", "test"]:
            msg = "Image name '{0:s}' is not a valid FC7 firmware image name. " \
                  "Try 'golden' or 'user' (or 'test').".format(image_name)
            raise ValueError(msg)

        if not image_file_name.endswith(".bin") and \
           not image_file_name.endswith(".bin.gz"):
            msg = "FC7 firmware needs to be in the format of a .bin(.gz) file. " \
                  "File '{0:s}' is not a .bin file.".format(image_file_name)
            raise ValueError(msg)

        image_data = self._read_firmware_image(image_name, verbose)
        if verbose:
            msg = "Writing firmware image to file '{0:s}'."
            print msg.format(image_file_name)
        open_file = open
        if image_file_name.endswith(".gz"):
            open_file = gzip.open
        image_file = open_file(image_file_name, "wb")
        pdb.set_trace()
        image_data_raw = [struct.pack(">I", i) for i in image_data]
        pdb.set_trace()
        image_file.write(''.join(image_data_raw))
        image_file.close()
        # End of read_firmware().

    def _read_firmware_image(self, image_name, verbose=None):
        if verbose is None:
            verbose = self.verbose

        # Figure out the name of the file on the SD card.
        sd_file_name = self.determine_sd_file_name(image_name)
        if verbose:
            msg = "Reading '{0:s}' firmware image from SD card."
            print msg.format(sd_file_name)

        # And now: action!
        with PipeDream(self) as sd_reader:
            # Read the image data from the SD card.
            image_data = sd_reader.read_sd_file(sd_file_name)
        # End of _read_firmware_image().
        return image_data

    def write_firmware(self, image_name, image_file_name, verbose=None):
        """Programs a firmware image from a .bin (or .bin.gz) file."""
        if verbose is None:
            verbose = self.verbose

        if not image_name in ["golden", "user", "test"]:
            msg = "Image name '{0:s}' is not a valid FC7 firmware image name. " \
                  "Try 'golden' or 'user' (or 'test').".format(image_name)
            raise ValueError(msg)

        if not image_file_name.endswith(".bin") and \
           not image_file_name.endswith(".bin.gz"):
            msg = "FC7 firmware needs to be in the format of a .bin(.gz) file. " \
                  "File '{0:s}' is not a .bin file.".format(image_file_name)
            raise ValueError(msg)

        if verbose:
            msg = "Reading firmware image from file '{0:s}'."
            print msg.format(image_file_name)
        open_file = open
        if image_file_name.endswith(".gz"):
            open_file = gzip.open
        image_file = open_file(image_file_name, "rb")
        data_raw = image_file.read()
        image_file.close()
        image_data = [struct.unpack(">I", i)[0] for i in chunkify(data_raw, 4)]
        self._write_firmware_image(image_name, image_data, verbose)
        # End of write_firmware().

    def _write_firmware_image(self, image_name, image_data, verbose=None):
        if verbose is None:
            verbose = self.verbose

        # Figure out the name to give the file on the SD card.
        sd_file_name = self.determine_sd_file_name(image_name)
        if verbose:
            msg = "Writing firmware image to SD card as '{0:s}'."
            print msg.format(sd_file_name)

        # And now: action!
        with PipeDream(self) as sd_reader:
            # Delete the file currently on the SD card (if it exists).
            try:
                sd_reader.delete_sd_file(sd_file_name)
            except RuntimeError:
                # File could not be deleted. Probably does not exist.
                pass
            # Write the new data to the SD card.
            sd_reader.write_sd_file(sd_file_name, image_data)
        # End of _write_firmware_image().

    def load_firmware(self, image_name):
        sd_file_name = self.determine_sd_file_name(image_name)
        with PipeDream(self) as mmc_pipe:
            mmc_pipe.reboot_fpga(sd_file_name)
        # Give the board a little bit of time to recover.
        time.sleep(TIME_WAIT_FIRMWARE_LOAD)
        # End of load_firmware().

    def list_firmware_images(self):
        with PipeDream(self) as mmc_pipe:
            res = mmc_pipe.list_sd_files();
        # End of list_firmware_images().
        return res

    def reset_ram(self):
        self.write("fc7.ddr3_ctrl.reset", 0x0)
        self.write("fc7.ddr3_ctrl.reset", 0x1)
        # BUG BUG BUG
        # This is a bit dodgy. What if this never finishes?
        ram_ready = self.read("fc7.ddr3_status.initial_calibration_complete")
        while not ram_ready:
            time.sleep(.1)
            ram_ready = self.read("fc7.ddr3_status.initial_calibration_complete")
        # BUG BUG BUG end
        # End of reset_ram().

    # End of class FC7.

###############################################################################

class TCDSFC7(FC7):

    def __init__(self, carrier_type, board_type, hw_interface, verbose=False):
        # ASSERT ASSERT ASSERT
        assert carrier_type == BOARD_TYPE_FC7
        # ASSERT ASSERT ASSERT end
        super(TCDSFC7, self).__init__(carrier_type, board_type, hw_interface, verbose)
        # End of __init__().

    # End of class TCDSFC7.

###############################################################################
