#!/usr/bin/env python

###############################################################################
## Utility to build a uhal connections (XML) file based on a TCDS
## hardware description (JSON) file.
###############################################################################

import os
import sys

from xml.dom import minidom
from xml.etree import ElementTree as ET

from pytcds.utils.tcds_cmd_line_base import CmdLineBase
from pytcds.utils.tcds_utils_setup_description import hw_sorter_helper

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

address_table_path = "${XDAQ_ROOT}/etc/tcds/addresstables/"
address_table_file_names = {
    "cpm-t1": "device_address_table_amc13_cpmt1.xml",
    "cpm-t2": "device_address_table_amc13_cpmt2.xml",
    "lpm": "device_address_table_fc7_lpm.xml",
    "ipm": "device_address_table_fc7_lpm.xml",
    "ici": "device_address_table_fc7_lpm.xml",
    "apve": "device_address_table_fc7_lpm.xml",
    "pi": "device_address_table_fc7_pi.xml",
    "phasemon": "device_address_table_fc7_phasemon.xml"
}

###############################################################################

def prettify(elem):

    rough_string = ET.tostring(elem, 'utf-8')
    reparsed = minidom.parseString(rough_string)
    res = reparsed.toprettyxml(indent="  ", encoding='utf-8')

    # End of prettify().
    return res

###############################################################################

class Builder(CmdLineBase):

    def setup_parser_custom(self):
        pass
        # End of setup_parser_custom().

    def main(self):

        out_file_dirname = os.path.dirname(self.setup_file_name)
        out_file_name = os.path.join(out_file_dirname, "tcds_connections.xml")
        sep_line = " " + "+" * 10 + " "

        print("Writing TCDS uhal connections file to '{0:s}'".format(out_file_name))

        #----------

        doc = ET.Element('connections')

        doc.append(ET.Comment(sep_line))
        doc.append(ET.Comment("WARNING: This file is auto-generated. Do not edit!"))
        doc.append(ET.Comment(sep_line))

        #----------

        targets = self.setup.hw_targets.values()
        for target in sorted(targets, key=hw_sorter_helper):

            # VME stuff does not matter for the UHAL connections file.
            if hasattr(target, 'vme_chain'):
                continue

            tmp = ET.SubElement(doc, "connection")

            tmp.set("id", target.identifier)

            board = target.board
            if not board:
                pdb.set_trace()
            tmp.set("uri",
                    "chtcp-2.0://" + board.controlhub + ":10203?target=" + board.dns_alias + ":50001")

            address_table_file_name = address_table_file_names[target.type]
            tmp.set("address_table",
                    "file://" + address_table_path + address_table_file_name)

        #----------

        doc.append(ET.Comment(sep_line))

        #----------

        with open(out_file_name, 'w') as out_file:
            out_file.write(prettify(doc))

        # End of main().

    # End of class Builder.

###############################################################################

if __name__ == "__main__":

    desc_str = "Utility to build a uhal connections (XML) file based on a TCDS " \
               "hardware description (JSON) file."
    res = Builder(desc_str).run()

    print("Done")
    sys.exit(res)

###############################################################################
