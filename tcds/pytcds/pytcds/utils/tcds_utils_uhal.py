###############################################################################
## Utilities related to the uhal library.
###############################################################################

import uhal

from pytcds.utils.tcds_constants import board_names
from pytcds.utils.tcds_hwinterfacewrapper import HwInterfaceWrapper
from pytcds.utils.tcds_utils_networking import host_ip

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

def build_uhal_uri(ip_address, controlhub_address=None):
    uri = None

    if controlhub_address is None:
        uri = "ipbusudp-2.0://{0:s}:50001".format(ip_address)
    else:
        uri_tmp = "chtcp-2.0://{0:s}:10203?target={1:s}:50001"
        uri = uri_tmp.format(controlhub_address, ip_address)

    # End of build_uhal_uri().
    return uri

###############################################################################

def get_log_level():
    log_level = None
    for (i, level) in uhal.LogLevel.values.iteritems():
        if uhal.LoggingIncludes(level):
            log_level = level
            break
    # End of get_log_leve().
    return log_level

###############################################################################

def get_hw_tca_base(carrier_type,
                    board_type,
                    address_table_file_name,
                    address_or_name,
                    controlhub_address=None,
                    verbose=False,
                    hw_cls=None):

    if hw_cls is None:
        hw_cls = HwInterfaceWrapper

    ip_address = host_ip(address_or_name)
    board_name = board_names[board_type]

    if verbose:
        if ip_address == address_or_name:
            msg = "Connecting to {0:s} at IP address {1:s}."
            print msg.format(board_name, ip_address)
        else:
            msg = "Connecting to {0:s} '{1:s}' at IP address {2:s}."
            print msg.format(board_name, address_or_name, ip_address)

    uri = build_uhal_uri(ip_address, controlhub_address)
    if not address_table_file_name.startswith("file://"):
        address_table_file_name = "file://{0:s}".format(address_table_file_name)
    try:
        board_tmp = uhal.getDevice(board_name,
                                   uri,
                                   address_table_file_name)
    except Exception, err:
        msg = "Failed to connect to '{0:s}': '{1:s}'".format(uri, err)
        raise RuntimeError(msg)

    board = hw_cls(carrier_type, board_type, board_tmp, verbose)

    # End of get_hw_tca().
    return board

###############################################################################

def get_hw_tca(carrier_type,
               board_type,
               address_table_file_name,
               address_or_name,
               controlhub_address=None,
               verbose=False,
               hw_cls=None):

    board = get_hw_tca_base(carrier_type,
                            board_type,
                            address_table_file_name,
                            address_or_name,
                            controlhub_address,
                            verbose,
                            hw_cls)

    # Try to read from the board. Does not really matter where. Just
    # to see if there really is something out there. The
    # system.board_id register is supposed to exist in all TCDS
    # firmware though, so it's a safe bet.
    try:
        node_name = "system.board_id"
        node = board.get_node(node_name)
        node.read()
        board.dispatch()
    except Exception, err:
        controlhub_str = ""
        if controlhub_address:
            controlhub_str = " (using controlhub at IP address {0:s})".format(controlhub_address)
        msg = "Could not connect to the hardware at IP address {0:s}{1:s}: '{2:s}'."
        msg = msg.format(address_or_name, controlhub_str, str(err))
        raise RuntimeError(msg)

    # End of get_hw_tca().
    return board

###############################################################################
