#!/usr/bin/env python

###############################################################################
## Switch on/off the I2C player on LPMs, PIs, and PhaseMons.
###############################################################################

import sys

from pytcds.utils.tcds_cmd_line_base import CmdLineBase
from pytcds.utils.tcds_constants import BOARD_ID_LPM
from pytcds.utils.tcds_constants import BOARD_ID_PHASEMON
from pytcds.utils.tcds_constants import BOARD_ID_PI
from pytcds.utils.tcds_utils_hw_connect import get_carrier_hw
from pytcds.utils.tcds_utils_networking import resolve_network_address

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class I2CSwitcher(CmdLineBase):

    STATE_CHOICES = ["on", "off"]

    def __init__(self, description=None, usage=None, epilog=None):
        super(I2CSwitcher, self).__init__(description, usage, epilog)
        self.state = None
        # End of __init__().

    def setup_parser_custom(self):
        super(I2CSwitcher, self).setup_parser_custom()
        parser = self.parser

        # Add the target SFP state argument.
        help_str = "The target I2C player state"
        parser.add_argument("player_state",
                            type=str,
                            choices=I2CSwitcher.STATE_CHOICES,
                            help=help_str)
        # End of setup_parser_custom().

    def handle_args(self):
        super(I2CSwitcher, self).handle_args()
        # Extract the target state.
        self.state = self.args.player_state
        # End of handle_args().

    def main(self):
        self.validate_target(target_type=CmdLineBase.TARGET_TYPE_HW)
        board_info = self.setup.hw_targets[self.target]
        network_address = board_info.dns_alias
        controlhub_address = board_info.controlhub
        carrier = get_carrier_hw(network_address, controlhub_address, verbose=self.verbose)
        if not carrier:
            self.error("Could not connect to {0:s}.".format(self.ip_address))

        if self.verbose:
            print "Switching the I2C player {0:s}.".format(self.state)

        user_system_id = carrier.get_user_system_id()
        if not user_system_id in [BOARD_ID_LPM, BOARD_ID_PI, BOARD_ID_PHASEMON]:
            self.error("This board does not have an I2C player.")

        tmp = 1
        if self.state == "off":
            tmp = 0
        carrier.write("user.i2c_player_ctrl.enable", tmp)
        # End of main().

    # End of class I2CSwitcher.

###############################################################################

if __name__ == "__main__":

    desc_str = "Switch on/off the I2C player on LPMs, PIs, and PhaseMons."

    res = I2CSwitcher(desc_str).run()

    print "Done"
    sys.exit(res)

###############################################################################
