###############################################################################
## Utilities to handle Xilinx .mcs firmware image files (used for GLIB
## firmware images).
## Some info on the format of these Intel MCS-86 Hexadecimal Object files can
## be found at the following links:
##   http://www.xilinx.com/support/documentation/application_notes/xapp518-isp-bpi-prom-virtex-6-pcie.pdf
##   http://en.wikipedia.org/wiki/Intel_HEX
##   http://www.keil.com/support/docs/1584/
###############################################################################

import binascii
import gzip
import os
import struct

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

NUM_BYTES_PER_MCS_RECORD = 16

MCS_RECORD_TYPE_DATA = 0
MCS_RECORD_TYPE_EOF = 1
# Extended Segment Address record.
MCS_RECORD_TYPE_ESA = 2
# Start Segment Address record.
MCS_RECORD_TYPE_SSA = 3
# Extended Linear Address record.
MCS_RECORD_TYPE_ELA = 4
# Start Linear Address record.
ALLOWED_MCDS_RECORD_TYPES = [MCS_RECORD_TYPE_DATA, \
                             MCS_RECORD_TYPE_EOF, \
                             MCS_RECORD_TYPE_ELA]

# The Xilinx PROMs set all bits to one when erasing. So when looking
# for empty PROM space, one looks for bytes looking like this.
PROM_DEFAULT_BYTE = 0xff

###############################################################################

class MCSRecord(object):

    # NOTE: All hex strings are handles in lowercase.

    @classmethod
    def from_raw_record(cls, raw_record, address_offset=0x0):
        record = cls()
        record._init_from_raw_record(raw_record, address_offset)
        # End of from_raw_record().
        return record

    @classmethod
    def from_contents(cls, record_type, record_data, address):
        record = cls()
        record._init_from_contents(record_type, record_data, address)
        # End of from_contents().
        return record

    def __init__(self):
        self.raw_record = None
        self.record_len = None
        self.record_address = None
        self.record_type = None
        self.record_data = None
        self.checksum = None
        self.address_offset = None
        # End of __init__().

    def _init_from_raw_record(self, raw_record, address_offset):
        self.raw_record = raw_record.lower()
        self.address_offset = address_offset
        # End of _init_from_raw_record().

    def _init_from_contents(self, record_type, record_data, address):
        self.record_type = record_type
        self.record_data = record_data
        self.record_len = len(self.record_data)
        address_offset = (address & 0xffff0000) >> 16
        record_address = (address & 0xffff)
        self.record_address = record_address
        self.address_offset = address_offset
        # End of _init_from_contents().

    def address(self):
        # The upper two bytes originate (possibly) from an ELA record
        # received earlier. The lower two bytes are specified in the
        # record itself.
        res = self.address_offset * 0x10000 + self.record_address
        # End of address().
        return res

    def bitstream(self):
        res = None
        if self.record_type == MCS_RECORD_TYPE_DATA:
            res = struct.pack("%dB" % len(self.record_data), *self.record_data)
        # End of bitstream().
        return res

    def calc_checksum(self):
        checksum_tmp = self.record_len
        address_byte_0 = (self.record_address & 0xff)
        address_byte_1 = (self.record_address & 0xff00) >> 8
        checksum_tmp += address_byte_0 + address_byte_1
        checksum_tmp += self.record_type
        checksum_tmp += sum(self.record_data)
        checksum = (0x100 - checksum_tmp) & 0xff
        # End of calc_checksum().
        return checksum

    def decode(self):
        line = self.raw_record.strip()
        if line[0] != ":":
            raise ValueError("MCS record is invalid: '%s' does not start with ':'." % line)
        if len(line) < 11:
            raise ValueError("MCS record is invalid: '%s' is less than 11 characters long." % line)
        self.record_len = struct.unpack("B", binascii.unhexlify(line[1:3]))[0]
        self.record_address = struct.unpack(">H", binascii.unhexlify(line[3:7]))[0]
        self.record_type = struct.unpack("B", binascii.unhexlify(line[7:9]))[0]
        if not self.record_type in ALLOWED_MCDS_RECORD_TYPES:
            raise ValueError("MCS record '%s' contains unrecognized record type: %d." % \
                             (line, self.record_type))
        record_data_tmp = struct.unpack("%dB" % self.record_len, binascii.unhexlify(line[9:-2]))
        self.record_data = list(record_data_tmp)
        if (len(self.record_data) != self.record_len):
            raise ValueError("MCS record '%s' contains an incorrect number of data characters " \
                             "(%d instead of %d)." % \
                             (line, len(self.record_data), self.record_len))
        self.checksum = struct.unpack("B", binascii.unhexlify(line[-2:]))[0]

        calculated_checksum = self.calc_checksum()
        if calculated_checksum != self.checksum:
            raise ValueError("Calculated checksum (0x%x) for record '%s' does not match." % \
                             (calculated_checksum, line))

        # End of decode().

    def encode(self):
        pieces = []
        # Add the record length.
        pieces.append(struct.pack("B", self.record_len))
        # Add the record address.
        # NOTE: Only the lower two bytes go into the record
        # itself. The upper two bytes go into a separate ELA record.
        pieces.append(struct.pack(">H", self.record_address & 0xffff))
        # Add the record type.
        pieces.append(struct.pack("B", self.record_type))
        # Add the actual record data.
        pieces.append(struct.pack("%dB" % len(self.record_data), *self.record_data))
        # Add the checksum.
        pieces.append(struct.pack("B", self.calc_checksum()))
        # Finally combine everything and prepend the colon.
        pieces = [binascii.hexlify(i) for i in pieces]
        pieces.insert(0, ":")
        self.raw_record = "".join(pieces)
        # End of encode().

    def trim(self):
        """Remove trailing 'default bytes.'"""
        data_reversed = self.record_data[::-1]
        tmp = [(i == PROM_DEFAULT_BYTE) for i in data_reversed]
        ind = len(tmp) - tmp.index(False)
        self.record_data = self.record_data[:ind]
        self.record_len = len(self.record_data)
        # End of encode().

    # End of class MCSRecord.

###############################################################################

class MCSFile(object):

    def __init__(self, mcs_file_name):
        self.mcs_file_name = mcs_file_name
        self.data = None
        self._parsed = False
        self._raw_data = None
        # End of __init__().

    def bitstream(self):
        res = "".join([i.bitstream() for i in self.data if (not i.bitstream() is None)])
        # End of bitstream().
        return res

    def load_file(self):
        if self._raw_data is None:
            open_file = open
            if self.mcs_file_name.endswith(".gz"):
                open_file = gzip.open
            mcs_file = open_file(self.mcs_file_name, "rb")
            self._raw_data = mcs_file.readlines()
            mcs_file.close()
        # End of load_file().

    def parse_file(self):
        if not self._parsed:
            self.load_file()

        address_offset = 0
        self.data = []
        for line in self._raw_data:
            record = MCSRecord.from_raw_record(line, address_offset)
            record.decode()
            if record.record_type == 4:
                # This is an ELA record -> store the record's address
                # for use later on as an address offset.
                address_offset = record.record_data
            self.data.append(record)
        # DEBUG DEBUG DEBUG
        assert len([i for i in self.data if i.record_type == 1]) == 1, \
            "ERROR Found more or less than one EOF record."
        assert self.data[-1].record_type == 1, \
            "ERROR The last record is not an EOF record."
        # DEBUG DEBUG DEBUG end
        self._parsed = True
        # End of parse_file().

    # End of class MCSFile.

###############################################################################

class BitStream(object):

    def __init__(self, raw_data):
        self._parsed = False
        self.data = None
        self._raw_data = raw_data
        # End of __init__().

    def mcs_records(self):
        if not self._parsed:
            self.parse()
        res = os.linesep.join([i.raw_record for i in self.data])
        res += os.linesep
        # End of mcs_records().
        return res

    def mcs_records_trimmed(self):
        """Returns a trimmed version of the MCS records.

        Removes all trailing empty records and trims the end of the
        last record.

        """
        if not self._parsed:
            self.parse()
        # NOTE: Careful to cut off the EOF record and to later attach
        # it again.
        eof_record = self.data[-1]
        # The following is a bit of trickery to find the last
        # non-empty data record (i.e., the last data record with
        # record_data != 0xff...).
        # NOTE: What an empty record looks like (i.e., all ones) is
        # determined by the fact that the Xilinx PROMs set all bits to
        # one when erasing.
        tmp = [(set(i.record_data) != set([PROM_DEFAULT_BYTE])) and \
               (i.record_type == MCS_RECORD_TYPE_DATA) \
               for i in self.data[:-1]]
        tmp_cp = tmp
        tmp = tmp[::-1]
        ind_lo = 0
        ind_hi = len(tmp) - 1 - tmp.index(True)
        records = [i.raw_record for i in self.data[ind_lo:ind_hi + 1]]

        # If necessary, trim the end of the last data record. Otherwise a
        #   :045AF00000040000AE
        # would be stored as
        #   :105AF00000040000FFFFFFFFFFFFFFFFFFFFFFFFAE
        last_record_raw = records[-1]
        last_record = MCSRecord.from_raw_record(last_record_raw, 0)
        last_record.decode()
        last_record.trim()
        last_record.encode()
        records[-1] = last_record.raw_record

        # Re-append the EOF record.
        records.append(eof_record.raw_record)
        res = os.linesep.join(records)
        res += os.linesep
        # End of mcs_records_trimmed().
        return res

    def parse(self):
        self.data = []
        # Each record in an mcs file contains at most 16 bytes, so we
        # start by breaking up the bitstream data into blocks of 16
        # bytes.
        bitstream_blocks = [self._raw_data[i:i + NUM_BYTES_PER_MCS_RECORD] \
                            for i in range(0, len(self._raw_data), NUM_BYTES_PER_MCS_RECORD)]

        address = 0x0
        address_offset = None
        for block in bitstream_blocks:
            rec = MCSRecord.from_contents(MCS_RECORD_TYPE_DATA, block, address)
            rec.encode()
            if rec.address_offset != address_offset:
                tmp = struct.pack(">H", rec.address_offset)
                address_data = list(struct.unpack("2B", tmp))
                rec_ela = MCSRecord.from_contents(MCS_RECORD_TYPE_ELA, address_data, 0x0)
                rec_ela.encode()
                self.data.append(rec_ela)
                address_offset = rec.address_offset
            self.data.append(rec)
            address += rec.record_len
        # Append the EOF record.
        rec_eof = MCSRecord.from_contents(MCS_RECORD_TYPE_EOF, [], 0x0)
        rec_eof.encode()
        self.data.append(rec_eof)
        self._parsed = True
        # End of parse().

    # End of class BitStream.

###############################################################################
