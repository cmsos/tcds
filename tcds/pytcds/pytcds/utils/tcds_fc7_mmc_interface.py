###############################################################################
## Helper methods etc. for use with the 'dma-pipe' connecting the FPGA
## to the MMC on the FC7. Used, for example, to access the SD card
## from the FPGA.
###############################################################################

import time
import datetime

from pytcds.utils.tcds_utils_misc import chunkify
from pytcds.utils.tcds_utils_misc import format_timedelta
from pytcds.utils.tcds_utils_misc import string_to_uint32
from pytcds.utils.tcds_utils_misc import uint32_to_string

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

# The various commands understood by the MMC firmware.
# See also:
#   https://svnweb.cern.ch/trac/cactus/browser/trunk/cactusupgrades/components/imperial_mmc/src/common/avr32_dma_pipe.c

PIPE_CMD_ENTERSECUREMODE = 0x00000000
PIPE_CMD_SETTEXTSPACE = 0x00000001
PIPE_CMD_SETDUMMYSENSOR = 0x00000002
PIPE_CMD_FILEFROMSD = 0x00000009
PIPE_CMD_FILETOSD = 0x00000010
PIPE_CMD_REBOOTFPGA = 0x00000011
PIPE_CMD_DELETEFROMSD = 0x00000012
PIPE_CMD_LISTFILESONSD = 0x00000013
PIPE_CMD_NUCLEARRESET = 0x00000021
PIPE_CMD_GETTEXTSPACE = 0x00000022

# On the SD card, files are stored with fixed size: 40000 clusters or
# 512 bytes each. This makes for 40000 * 512 / 4 = 5120000 32-bit
# IPbus words.
SD_FILE_SIZE = 40000 * 512 / 4

SLEEP_TIME = .01
TEXT_SPACE_SIZE = 31
QUIP = "RuleBritannia"

###############################################################################

class PipeDream(object):
    """The DMA-pipe connecting the FPGA to the MMC on the FC7."""

    def __init__(self, hw, verbose=False):
        self._hw = hw
        self.verbose = verbose
        self._space_available_from_fpga_to_mmc = -1
        self._space_available_from_mmc_to_fpga = -1
        self._data_available_from_fpga_to_mmc = -1
        self._data_available_from_mmc_to_fpga = -1
        self._update_counters()
        # End of __init__().

    def __enter__(self):
        self._flush()
        return self
        # End of __enter__().

    def __exit__(self, type, value, traceback):
        self._flush()
        # End of __exit__().

    def _fifo_size(self):
        fifo_size = self._hw.get_node("fc7.mmc_interface.fifo").getSize()
        # End of _fifo_size().
        return fifo_size

    def _send(self, command, data_in=None):
        data = [command]
        if data_in is None:
            data_in = []
        data.append(len(data_in))
        data.extend(data_in)
        self._update_counters()
        while (self._space_available_from_fpga_to_mmc < len(data)):
            time.sleep(SLEEP_TIME)
            self._update_counters()
        self._hw.write_block("fc7.mmc_interface.fifo", data)
        # End of _send().

    def _receive(self):
        self._update_counters()
        while (self._data_available_from_mmc_to_fpga < 2):
            time.sleep(SLEEP_TIME)
            self._update_counters()
        tmp = self._hw.read_block("fc7.mmc_interface.fifo", 2)
        num_words_available = tmp[1]
        data = None
        if (num_words_available > 0):
            while (self._data_available_from_mmc_to_fpga < num_words_available):
                time.sleep(SLEEP_TIME)
                self._update_counters()
            data = self._hw.read_block("fc7.mmc_interface.fifo",
                                       num_words_available)
        if tmp[0] != 0:
            msg = self._reinterpret_as_string(data)
            raise RuntimeError(msg)
        # End of _receive().
        return data

    def _flush(self):
        self._update_counters()
        if self._data_available_from_mmc_to_fpga:
            num_words_available = self._data_available_from_mmc_to_fpga
            self._hw.read_block("fc7.mmc_interface.fifo",
                                num_words_available)
        # End of flush().

    def _update_counters(self):
        hw = self._hw.hw.hw_interface
        tmp0 = hw.getNode("fc7.mmc_interface.fpga_to_mmc_counters").read()
        tmp1 = hw.getNode("fc7.mmc_interface.mmc_to_fpga_counters").read()
        hw.dispatch()
        fpga_to_mmc_count = tmp0.value()
        mmc_to_fpga_count = tmp1.value()
        # print "DEBUG JGH _update_counters()"
        # print "DEBUG JGH   fpga_to_mmc_count = {0:d}".format(fpga_to_mmc_count)
        # print "DEBUG JGH   mmc_to_fpga_count = {0:d}".format(mmc_to_fpga_count)
        fifo_size = self._fifo_size()
        tmp = (((fpga_to_mmc_count >> 16 ) & 0x0000ffff) - \
               ((fpga_to_mmc_count >> 1) & 0x00007fff) + \
               1) % fifo_size
        self._data_available_from_fpga_to_mmc = tmp
        self._space_available_from_fpga_to_mmc = fifo_size - \
                                                 self._data_available_from_fpga_to_mmc - \
                                                 1
        # print "DEBUG JGH   _data_available_from_fpga_to_mmc = {0:d}".format(self._data_available_from_fpga_to_mmc)
        # print "DEBUG JGH   _space_available_from_fpga_to_mmc = {0:d}".format(self._space_available_from_fpga_to_mmc)
        tmp = (((mmc_to_fpga_count >> 1) & 0x00007fff) - \
               ((mmc_to_fpga_count >> 16) & 0x0000ffff) + \
               1) % fifo_size
        self._data_available_from_mmc_to_fpga = tmp
        self._space_available_mmc_to_fpga = fifo_size - \
                                            self._data_available_from_mmc_to_fpga - \
                                            1
        # print "DEBUG JGH   _data_available_from_mmc_to_fpga = {0:d}".format(self._data_available_from_mmc_to_fpga)
        # print "DEBUG JGH   _space_available_from_mmc_to_fpga = {0:d}".format(self._space_available_from_mmc_to_fpga)
        # End of _update_counters().

    def _reinterpret_as_list_of_file_names(self, list_of_uint32):
        chunks = chunkify(list_of_uint32, 8)
        tmp = [self._reinterpret_as_string(i) for i in chunks]
        res = [i.strip() for i in tmp]
        # End of _reinterpret_as_list_of_file_names().
        return res

    def _reinterpret_as_string(self, list_of_uint32):
        data_in = list_of_uint32
        tmp = [uint32_to_string(i) for i in data_in]
        res = "".join(tmp)
        # End of _reinterpret_as_string().
        return res

    def _reinterpret_as_list_of_uint32(self, string_in):
        res = [string_to_uint32(i) for i in chunkify(string_in, 4)]
        # End of _reinterpret_as_list_of_uint32().
        return res

    def _write_to_text_space(self, text):
        if len(text) > TEXT_SPACE_SIZE:
            msg = "Size of string '{0:s}' " \
                  "exceeds maximum text space size of {1:d}."
            raise ValueError(msg.format(text, TEXT_SPACE_SIZE))
        data = self._reinterpret_as_list_of_uint32(text)
        self._send(PIPE_CMD_SETTEXTSPACE, data)
        # Use _receive() to check for errors.
        res = self._receive()
        if res:
            print res
        # End of _write_to_text_space().

    def _read_from_text_space(self):
        self._send(PIPE_CMD_GETTEXTSPACE)
        tmp = self._receive()
        res = self._reinterpret_as_string(tmp)
        return res
        # End of _write_to_text_space().

    def _clear_text_space(self):
        self._write_to_text_space("")
        # End of _clear_text_space().

    def _enter_secure_mode(self, quip=None):
        if quip is None:
            quip = QUIP
        if len(quip) > TEXT_SPACE_SIZE:
            msg = "Size of string '{0:s}' " \
                  "exceeds maximum text space size of {1:d}."
            raise ValueError(msg.format(quip, TEXT_SPACE_SIZE))
        data = self._reinterpret_as_list_of_uint32(quip)
        self._send(PIPE_CMD_ENTERSECUREMODE, data)
        # Use _receive() to check for errors.
        res = self._receive()
        if res:
            print res
        # End of _enter_secure_mode().

    def list_sd_files(self):
        self._send(PIPE_CMD_LISTFILESONSD)
        tmp = self._receive()
        res = self._reinterpret_as_list_of_file_names(tmp)
        # End of list_sd_files().
        return res

    def read_sd_file(self, file_name):
        # Write the file name in the pipe text area.
        self._write_to_text_space(file_name)
        # Trigger the file read.
        self._send(PIPE_CMD_FILEFROMSD)
        # Wait for the pipe to tell us how many words it's going to
        # send us.
        self._update_counters()
        while (self._data_available_from_mmc_to_fpga < 2):
            time.sleep(SLEEP_TIME)
            self._update_counters()
        tmp = self._hw.read_block("fc7.mmc_interface.fifo", 2)
        if tmp[0] != 0:
            msg = self._reinterpret_as_string(res)
            raise RuntimeError(msg)
        num_words_total = tmp[1]
        num_words_left = num_words_total
        res = []
        num_words_read = 0
        helper = 0
        while num_words_left:
            tmp = None
            self._update_counters()
            if (num_words_left < self._data_available_from_mmc_to_fpga):
                tmp = self._hw.read_block("fc7.mmc_interface.fifo", num_words_left)
                num_words_left = 0
            else:
                tmp = self._hw.read_block("fc7.mmc_interface.fifo", self._data_available_from_mmc_to_fpga)
                num_words_left -= self._data_available_from_mmc_to_fpga
            res.extend(tmp)
            num_words_read += len(tmp)
            if num_words_read >= helper:
                perc = 100. * num_words_read / num_words_total
                print "{0:5.1f}% read".format(perc)
                helper += num_words_total / 20
        file_data = res
        # Use _receive() to check for errors.
        res = self._receive()
        if res:
            print res
        # End of read_sd_file().
        return file_data

    def write_sd_file(self, file_name, file_data):
        time_begin = datetime.datetime.utcnow()
        if self.verbose:
            print "Writing data"
        # ASSERT ASSERT ASSERT
        assert len(file_data) <= SD_FILE_SIZE
        # ASSERT ASSERT ASSERT end
        data = file_data
        if len(data) < SD_FILE_SIZE:
            data.extend([0xffffffff] * (SD_FILE_SIZE - len(data)))
        num_words_total = len(data)
        # Write the file name in the pipe text area.
        self._write_to_text_space(file_name)
        # Trigger the file write (by sending the correct command and
        # the buffer size).
        # pdb.set_trace()
        self._hw.write_block("fc7.mmc_interface.fifo",
                             [PIPE_CMD_FILETOSD, num_words_total])
        # Write the rest of the file.
        num_words_left = num_words_total
        current = 0
        helper = 0
        # it = 0
        while num_words_left:
            # if it > 10:
            #     pdb.set_trace()
            # it += 1
            # print "DEBUG JGH ----------"
            # print "DEBUG JGH Words left: {0:d}".format(num_words_left)
            self._update_counters()
            # If the MMC has something to say to us, something went
            # wrong.
            if self._data_available_from_mmc_to_fpga:
                # print "DEBUG JGH !!! Data to be read from MMC to FPGA -> breaking !!!"
                # pdb.set_trace()
                # tmp = self._receive()
                # pdb.set_trace()
                break
            while not self._space_available_from_fpga_to_mmc:
                time.sleep(SLEEP_TIME)
                self._update_counters()
            num_words_this_time = 0
            if (self._space_available_from_fpga_to_mmc >= num_words_left):
                # Whatever is left will fit in this iteration.
                num_words_this_time = num_words_left
            else:
                # Not everything fits in this iteration. Take as much
                # as fits.
                num_words_this_time = self._space_available_from_fpga_to_mmc
            # print "DEBUG JGH num_words_this_time = {0:d}".format(num_words_this_time)
            data_this_time = file_data[current:current+num_words_this_time]
            current += num_words_this_time
            # ASSERT ASSERT ASSERT
            assert (len(data_this_time) == num_words_this_time)
            # ASSERT ASSERT ASSERT end
            num_words_left -= len(data_this_time)
            self._hw.write_block("fc7.mmc_interface.fifo", data_this_time)
            num_words_written = num_words_total - num_words_left
            # print "DEBUG JGH words written so far: {0:d}".format(num_words_written)
            # print "DEBUG JGH   --> words left: {0:d}".format(num_words_left)
            if num_words_written >= helper:
                perc = 100. * num_words_written / num_words_total
                print "{0:5.1f}% written".format(perc)
                helper += num_words_total / 20
                # print "DEBUG JGH helper = {0:d}".format(helper)
        # print "DEBUG JGH No more words left"

        # Use _receive() to check for errors.
        res = self._receive()
        if res:
            print res
        time_end = datetime.datetime.utcnow()
        time_delta = time_end - time_begin
        print "Image writing (to FC7 SD card) took {0:s}".format(format_timedelta(time_delta))
        # End of write_sd_file().

    def reboot_fpga(self, file_name=None):
        """Reboot the FPA from the give SD-card file name."""

        if file_name:
            self._write_to_text_space(file_name)
        self._enter_secure_mode()

        self._send(PIPE_CMD_REBOOTFPGA)
        res = self._receive()
        if res:
            print res
        # End of reboot_fpga().

    def delete_sd_file(self, file_name):
        # Write the file name in the pipe text area.
        self._write_to_text_space(file_name)
        # Enter secure mode.
        self._enter_secure_mode()
        # Trigger the file deletion.
        self._send(PIPE_CMD_DELETEFROMSD)
        # Use _receive() to check for errors.
        res = self._receive()
        if res:
            print res
        # End of delete_sd_file().

    # End of class PipeDream.

###############################################################################
