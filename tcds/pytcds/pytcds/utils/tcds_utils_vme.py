###############################################################################
## Utilities related to the CAEN VME library (and the pyvme wrapper).
###############################################################################

import re

from pytcds.utils.tcds_constants import board_names

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class NodeInfo(object):

    def __init__(self, node_name, address, address_modifier, width, mask, readable, writable):
        self.node_name = node_name
        self.address = address
        self.address_modifier = address_modifier
        self.width = width
        self.mask = mask
        self.readable = readable
        self.writable = writable
        # End of __init__().

    # End of class NodeInfo.

###############################################################################

class AddressTableReader(object):

    def __init__(self, address_table_file_name):
        self.address_table_file_name = address_table_file_name
        # End of __init__().

    def get_nodes(self):
        in_file = open(self.address_table_file_name, "r")
        lines = in_file.readlines()
        in_file.close()
        nodes = {}
        pattern = "^(.+?) +([0-9]+?) +([0-9]+?) +([0-9a-f]+?) +([0-9a-f]+?) +([01]) +([01])$"
        regex = re.compile(pattern)
        for line in lines:
            line = line.strip()
            if line.startswith("*"):
                # Comment line -> skip.
                continue
            if not len(line):
                # Empty line -> skip.
                continue
            match = regex.match(line)
            if not match:
                msg = "Did not understand VME address table line '{0:s}' " \
                      "from file '{1:s}'".format(line, self.address_table_file_name)
                raise ValueError(msg)
            node_name = match.group(1)
            address = int(match.group(4), 16)
            address_modifier = int(match.group(2), 16)
            width = int(match.group(3))
            mask = int(match.group(5), 16)
            readable = match.group(6) == "1"
            writable = match.group(7) == "1"
            nodes[node_name] = NodeInfo(node_name, address, address_modifier, width, mask, readable, writable)
        # End of get_nodes().
        return nodes

    # End of class AddressTableReader.

###############################################################################

def get_hw_vme(board_type,
               address_table_file_name,
               slot_number,
               vme_chain=0,
               vme_unit=0,
               verbose=False,
               hw_cls=None):

    board_name = board_names[board_type]

    if verbose:
        msg = "Connecting to {0:s} in VME slot {1:s}."
        print msg.format(board_name, slot_number)

    reader = AddressTableReader(address_table_file_name)
    nodes = reader.get_nodes()
    board = hw_cls(board_type, vme_chain, vme_unit, slot_number, nodes, verbose)

    # End of get_hw_vme().
    return board

###############################################################################
