#!/usr/bin/env python

###############################################################################
## A helper script to go through all partitions and check if the ICIs
## are correctly configured (B-channel wise).
###############################################################################

import sys
import time

from pytcds.lpm.tcds_lpm import get_lpm_hw
from pytcds.pi.tcds_pi import get_pi_hw
from pytcds.utils.tcds_cmd_line_base import CmdLineBase
from pytcds.utils.tcds_utils_setup_description import sorter_helper

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

NUM_B_CHANNELS = 16
B_CHANNEL_NUMBER_LUMINIBBLE = 0
B_CHANNEL_NUMBER_BC0 = 1
B_CHANNEL_NUMBER_TESTENABLE = 2
B_CHANNEL_NUMBER_PRIVATEGAP = 3
B_CHANNEL_NUMBER_PRIVATEORBIT = 4
B_CHANNEL_NUMBER_RESYNC = 5
B_CHANNEL_NUMBER_HARDRESET = 6
B_CHANNEL_NUMBER_EC0 = 7
B_CHANNEL_NUMBER_OC0 = 8
B_CHANNEL_NUMBER_START = 9
B_CHANNEL_NUMBER_STOP = 10
B_CHANNEL_NUMBER_STARTOFGAP = 11
B_CHANNEL_NUMBER_BGO12 = 12
B_CHANNEL_NUMBER_WTE = 13
B_CHANNEL_NUMBER_BGO14 = 14
B_CHANNEL_NUMBER_BGO15 = 15

B_CHANNEL_NAMES = {
    B_CHANNEL_NUMBER_LUMINIBBLE : "LumiNibble",
    B_CHANNEL_NUMBER_BC0 : "BC0",
    B_CHANNEL_NUMBER_TESTENABLE : "TestEnable",
    B_CHANNEL_NUMBER_PRIVATEGAP : "PrivateGap",
    B_CHANNEL_NUMBER_PRIVATEORBIT : "PrivateOrbit",
    B_CHANNEL_NUMBER_RESYNC : "Resync",
    B_CHANNEL_NUMBER_HARDRESET : "HardReset",
    B_CHANNEL_NUMBER_EC0 : "EC0",
    B_CHANNEL_NUMBER_OC0 : "OC0",
    B_CHANNEL_NUMBER_START : "Start",
    B_CHANNEL_NUMBER_STOP : "Stop",
    B_CHANNEL_NUMBER_STARTOFGAP : "StartOfGap",
    B_CHANNEL_NUMBER_BGO12 : "HCALQIEReset",
    B_CHANNEL_NUMBER_WTE : "WarningTestEnable",
    B_CHANNEL_NUMBER_BGO14 : "PixelROCReset",
    B_CHANNEL_NUMBER_BGO15 : "PixelResync"
}

CPM_EMISSION_BX = {}

CPM_EMISSION_BX[B_CHANNEL_NUMBER_LUMINIBBLE] = 2256
CPM_EMISSION_BX[B_CHANNEL_NUMBER_BC0] = 3540
CPM_EMISSION_BX[B_CHANNEL_NUMBER_TESTENABLE] = 3283
CPM_EMISSION_BX[B_CHANNEL_NUMBER_PRIVATEGAP] = 2000
CPM_EMISSION_BX[B_CHANNEL_NUMBER_PRIVATEORBIT] = 0
CPM_EMISSION_BX[B_CHANNEL_NUMBER_RESYNC] = 2000
CPM_EMISSION_BX[B_CHANNEL_NUMBER_HARDRESET] = 2000
CPM_EMISSION_BX[B_CHANNEL_NUMBER_EC0] = 2000
CPM_EMISSION_BX[B_CHANNEL_NUMBER_OC0] = 2000
CPM_EMISSION_BX[B_CHANNEL_NUMBER_START] = 2000
CPM_EMISSION_BX[B_CHANNEL_NUMBER_STOP] = 2000
CPM_EMISSION_BX[B_CHANNEL_NUMBER_STARTOFGAP] = 3440
CPM_EMISSION_BX[B_CHANNEL_NUMBER_BGO12] = 3000
CPM_EMISSION_BX[B_CHANNEL_NUMBER_WTE] = 2886
CPM_EMISSION_BX[B_CHANNEL_NUMBER_BGO14] = 3000
CPM_EMISSION_BX[B_CHANNEL_NUMBER_BGO15] = 2000

GOOD = 0
BAD = 1
IGNORED = 2

###############################################################################

class BChannelInfo(object):
    def __init__(self,
                 b_channel_number,
                 is_single,
                 is_double,
                 is_block,
                 b_command,
                 emission_bx,
                 b_channel_ram):

        self.b_channel_number = b_channel_number
        self.is_single = is_single
        self.is_double = is_double
        self.is_block = is_block
        self.b_command = b_command
        # NOTE: Some subsystems delay their BC0 to the next orbit...
        self.raw_emission_bx = emission_bx
        self.emission_bx = self.raw_emission_bx
        while (self.emission_bx > 3564):
            self.emission_bx -= 3564
        self.b_channel_ram = b_channel_ram
        # End of __init__().

    def b_channel_mode_str(self):
        res = "unknown"
        is_single = self.is_single
        is_double = self.is_double
        is_block = self.is_block
        if (is_single + is_double + is_block) == 0:
            res = "off"
        elif (is_single + is_double + is_block) > 1:
            res = "misconfigured (i.e., multiple emission modes enabled)"
        else:
            if is_single:
                res = "single"
            elif is_double:
                res = "double"
            elif is_block:
                res = "block"
        # End of b_channel_mode_str().
        return res

    # End of class BChannelInfo.

###############################################################################

def get_b_channel_info(ici_info, ici, b_channel_number):
    reg_name_base = "ici{0:d}.bchannels.bchannel{1:d}".format(ici_info.ici_number, b_channel_number)

    tmp_is_single = ici.read("{0:s}.configuration.single".format(reg_name_base))
    tmp_is_double = ici.read("{0:s}.configuration.double".format(reg_name_base))
    tmp_is_block = ici.read("{0:s}.configuration.block".format(reg_name_base))
    is_single = (tmp_is_single == 0x1)
    is_double = (tmp_is_double == 0x1)
    is_block = (tmp_is_block == 0x1)

    tmp_is_bx_sync = ici.read("{0:s}.configuration.bx_sync".format(reg_name_base))
    is_bx_sync = (tmp_is_bx_sync == 0x1)
    bx_or_delay = ici.read("{0:s}.configuration.bx_or_delay".format(reg_name_base))
    emission_bx = bx_or_delay
    if not is_bx_sync:
        emission_bx += CPM_EMISSION_BX[b_channel_number]

    tmp_ram = ici.read_block("{0:s}.ram".format(reg_name_base), 4)
    b_command = tmp_ram[0]
    b_channel_info = BChannelInfo(b_channel_number,
                                  is_single,
                                  is_double,
                                  is_block,
                                  b_command,
                                  emission_bx,
                                  tmp_ram)
    # End of get_b_channel_info().
    return b_channel_info

###############################################################################

class TCDSChecker(CmdLineBase):

    def setup_parser_custom(self):
        pass
        # End of setup_parser_custom().

    def main(self):

        # Find all ICIs, in order to determine a list of partitions.
        hardware = [j for (i, j) in self.setup.hw_targets.iteritems()]
        all_icis = [i for i in hardware if i.type == "ici"]

        # BUG BUG BUG
        # For the moment, let's skip all the secondary ICIs.
        target_icis = [i for i in all_icis \
                       if (i.identifier.find("-sec") < 0)]
        # BUG BUG BUG end

        target_icis.sort(key=sorter_helper)

        results = {}
        for target_ici in target_icis:
            print target_ici.identifier
            partition_name = target_ici.identifier.split("-")[1]

            # if not partition_name in ["totdet", "tottrg", "ctpps"]:
            #     continue

            results[partition_name] = []

            try:

                board_info_ici = self.setup.hw_targets[target_ici.identifier]
                network_address_ici = board_info_ici.dns_alias
                controlhub_address_ici = board_info_ici.controlhub

                ici = get_lpm_hw(network_address_ici,
                                 controlhub_address_ici,
                                 verbose=self.verbose)
                if not ici:
                    raise RuntimeError("Could not connect to {0:s}.".format(network_address_ici))

                #----------

                # Get the configuration of all B-channels on this ICI.
                b_channel_infos = {}
                for b_channel_number in xrange(NUM_B_CHANNELS):
                    b_channel_infos[b_channel_number] = get_b_channel_info(target_ici, ici, b_channel_number)

                #----------

                # Now do some checks, on all B-channels.
                for (b_channel_number, b_channel_info) in sorted(b_channel_infos.iteritems()):
                    is_single = b_channel_info.is_single
                    is_double = b_channel_info.is_double
                    is_block = b_channel_info.is_block
                    if (is_single + is_double + is_block) > 1:
                        tmp = []
                        if is_single:
                            tmp.append("single")
                        if is_double:
                            tmp.append("double")
                        if is_block:
                            tmp.append("block")
                        msg_base = "B-channel {0:d} ({1:s})" \
                                   " is misconfigured." \
                                   " Multiple emission modes are enabled:" \
                                   " {2:s}."
                        msg = msg_base.format(b_channel_number,
                                              B_CHANNEL_NAMES[b_channel_number],
                                              ", ".join(tmp))
                        results[partition_name].append((BAD, target_ici.identifier, msg))
                    else:
                        msg_base = "B-channel {0:d} ({1:s})" \
                                   " is configured in '{2:s}' emission mode."
                        msg = msg_base.format(b_channel_number,
                                              B_CHANNEL_NAMES[b_channel_number],
                                              b_channel_info.b_channel_mode_str())
                        results[partition_name].append((GOOD, target_ici.identifier, msg))

                    ram = b_channel_info.b_channel_ram
                    if (is_single and ram[2:] != [0, 6]) or \
                       (not is_single and ram[2:] != [0, 6]):
                        msg_base = "Found unexpected B-channel {0:d} ({1:s})" \
                                   " RAM contents: {2:s}."
                        msg = msg_base.format(b_channel_number,
                                              B_CHANNEL_NAMES[b_channel_number],
                                              str(ram))
                        results[partition_name].append((BAD, target_ici.identifier, msg))
                    else:
                        msg_base = "B-channel {0:d} ({1:s}) RAM correctly configured."
                        msg = msg_base.format(b_channel_number,
                                              B_CHANNEL_NAMES[b_channel_number])
                        results[partition_name].append((GOOD, target_ici.identifier, msg))

                #----------

                problem_found = False
                tmp_b_commands = {}
                for b_channel_number in xrange(NUM_B_CHANNELS):
                    is_single = b_channel_info.is_single
                    is_double = b_channel_info.is_double
                    is_block = b_channel_info.is_block
                    if is_single or is_double or is_block:
                        tmp_b_command = b_channel_infos[b_channel_number].b_command
                        try:
                            tmp_b_commands[tmp_b_command].append(b_channel_number)
                        except KeyError:
                            tmp_b_commands[tmp_b_command] = [b_channel_number]
                for (b_command, b_channels) in tmp_b_commands.iteritems():
                    if len(b_channels) > 1:
                        problem_found = True
                        msg_base = "The same B-command (0x{0:x})" \
                                   " is issued by multiple B-channels:" \
                                   " {1:s}."
                        msg = msg_base.format(b_command, str(b_channels))
                        results[partition_name].append((BAD, target_ici.identifier, msg))
                if not problem_found:
                    msg = "No B-channel cross-configurations found."
                    results[partition_name].append((GOOD, target_ici.identifier, msg))

            #----------

            except Exception as err:
                results[partition_name] = [(0, "?", "ERROR: " + str(err))]

            #----------

            time.sleep(1)

            #----------

        # Dump results.
        print self.sep_line
        print "Results by partition:"
        print self.sep_line
        max_len_ident = max([len(i) for i in results.keys()])
        for (ident, res) in sorted(results.iteritems()):
            summary = "UNKNOWN"
            tmp = set([i[0] for i in res])
            all_ok = (tmp == set([GOOD]))
            ignored = (tmp == set([IGNORED]))
            if all_ok:
                summary = "OK"
            elif ignored:
                summary = "IGNORED"
            else:
                summary = "BAD"
            print "  {1:{0:d}s}: {2:s}".format(max_len_ident, ident, summary)
            max_len_name = max([len(i[1]) for i in res])
            for (flag, name, err_line) in res:
                flag_str = "UNKNOWN"
                if flag == GOOD:
                    flag_str = "OK"
                elif flag == BAD:
                    flag_str = "BAD"
                tmp = "  {1:{0:d}s}: {2:3s} - {4:{3:d}s} - {5:s}"
                print tmp.format(max_len_ident, "",
                                 flag_str,
                                 max_len_name, name,
                                 err_line)
        print self.sep_line

        # End of main().

    # End of class TCDSChecker.

###############################################################################

if __name__ == "__main__":

    desc_str = "A helper script" \
               " to go through all partitions" \
               " and check if the ICIs are correctly configured" \
               " (B-channel wise)."
    res = TCDSChecker(desc_str).run()

    print "Done"
    sys.exit(res)

###############################################################################
