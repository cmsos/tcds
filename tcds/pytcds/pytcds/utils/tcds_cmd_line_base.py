#!/usr/bin/env python

###############################################################################
## Base class for command-line utilities.
###############################################################################

import argparse
import glob
import os
import socket
import sys
import traceback

# Special treatment for uhal: if we find it, we reduce its log-level,
# otherwise just carry on. (This allows the same base class to be used
# also for non-uhal purposes.)
try:
    import uhal
except ImportError:
    pass

from tcds_utils_networking import resolve_network_address
from tcds_utils_setup_description import load_setups

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class CmdLineBase(object):
    """Base class for TCDS command-line utilities.

    This class provides a default 'verbose' flag, and expects a single
    command-line argument: the target to act on.

    """

    TARGET_TYPE_ANY = 0
    TARGET_TYPE_HW = 1
    TARGET_TYPE_SW = 2

    def __init__(self, description=None, usage=None, epilog=None):
        self.sep_line = "-" * 80
        self.description = description
        self.usage = usage
        # if not self.usage:
        #     self.usage = "%(prog)s [options] TARGET"
        self.epilog = epilog
        self.location_name = None
        self.setup_file_name = None
        self.setup = None
        self.target = None
        self.verbose = False
        # self.pre_hook()
        self.setup_parser_base()
        self.setup_parser_custom()
        # self.post_hook()
        # End of __init__().

    # def pre_hook(self):
    #     pass
    #     # End of pre_hook().

    # def post_hook(self):
    #     pass
    #     # End of post_hook().

    def setup_parser_base(self):
        parser = argparse.ArgumentParser(description=self.description,
                                         usage=self.usage,
                                         epilog=self.epilog)

        # Add the 'verbose' flag.
        parser.add_argument("-v", "--verbose",
                            action="store_true",
                            dest="verbose",
                            default=False)

        # Add the choice of the location.
        help_str = "TCDS location (i.e. XDAQ zone name). " \
                   "[default: Best guess based on XDAQ XaaS.]"
        parser.add_argument("--location",
                            action="store",
                            default=None,
                            help=help_str)

        # Add the option to choose a custom setup description file.
        help_str = "Setup description file to use " \
                   "(overrides location-based description file)"
        parser.add_argument("--setup-file",
                            action="store",
                            default=None,
                            help=help_str)

        self.parser = parser
        # End of setup_parser_base().

    def setup_parser_custom(self):
        parser = self.parser
        # Add the target argument.
        parser.add_argument("target",
                            action="store",
                            help="The target to operate on")
        # End of setup_parser_custom().

    def run(self):
        self.args = self.parser.parse_args()
        self.handle_args()
        try:
            if self.verbose:
                uhal.setLogLevelTo(uhal.LogLevel.INFO)
            else:
                uhal.setLogLevelTo(uhal.LogLevel.WARNING)
        except Exception:
            pass
        try:
            self.load_setup()
        except Exception, err:
            msg = "Something went wrong " \
                  "loading the hardware setup description: '{0:s}'."
            self.error(msg.format(err))
        try:
            res = self.main()
        except Exception, err:
            if self.verbose:
                print >> sys.stderr, traceback.print_exc()
            msg = "Something went wrong " \
                  "executing the main method: '{0:s}'."
            self.error(msg.format(err))
        # End of run().
        return res

    def handle_args(self):
        self.verbose = self.args.verbose

        # Handle the target, if there is supposed to be one.
        try:
            self.target = self.args.target
        except AttributeError:
            self.target = None

        # Handle the location.
        self.location_name = self.args.location
        if self.location_name:
            if self.verbose:
                print "Using specified location '{0:s}'".format(self.location_name)
        else:
            # Find a list of all XDAQ zones installed on this machine.
            # NOTE: This is the way the xdaqd script does it too.
            glob_list = glob.glob('/etc/xdaq.d/*.zone')
            # Ignore anything that does not sound like a TCDS zone.
            tmp = [os.path.basename(i) for i in glob_list]
            zone_names = [os.path.splitext(i)[0] for i in tmp]
            tcds_zone_names = [i for i in zone_names if i.startswith('tcds')]
            if len(tcds_zone_names) != 1:
                if not len(tcds_zone_names):
                    msg = "Found no XDAQ XaaS zone installed on this machine?" \
                          " (Trying to determine the TCDS location....)"
                else:
                    msg = "Found multiple XDAQ XaaS zone installed on this machine ({0:s})?"
                    msg = msg.format(", ".join(tcds_zone_names))
                self.error(msg)
            self.location_name = tcds_zone_names[0]
            if self.verbose:
                print "Using best-guess location '{0:s}'".format(self.location_name)

        # Handle a possible custom setup file.
        # NOTE: This overrides the location-based setup file.
        if self.args.setup_file:
            self.setup_file_name = os.path.expanduser(self.args.setup_file)
        if self.setup_file_name:
            if self.verbose:
                print "Using specified setup file '{0:s}'".format(self.setup_file_name)
        else:
            # The default location of the setup description file
            # assumes that this description file has been deployed as
            # part of the TCDS XaaS.
            tmp0 = os.path.join('/opt/xdaq/share', self.location_name)
            tmp0 = os.path.join(tmp0, 'etc')
            tmp1 = 'tcds_setup.json'
            self.setup_file_name = os.path.join(tmp0, tmp1)
            if self.verbose:
                print "Using location-based setup file '{0:s}'".format(self.setup_file_name)
        # End of handle_args().

    def load_setup(self):
        tmp = load_setups(self.setup_file_name, no_checks=True)
        try:
            self.setup = tmp[self.location_name]
        except KeyError:
            msg = "Could not find description for location '{0:s}' in file '{1:s}'."
            raise RuntimeError(msg.format(self.location_name, self.setup_file_name))
        # End of load_setup().

    def validate_target(self, target=None, target_type=TARGET_TYPE_ANY):
        if not target:
            target = self.target
        search_hw = False
        search_sw = False
        found_hw = False
        found_sw = False
        search_hw = target_type in [CmdLineBase.TARGET_TYPE_ANY,
                                    CmdLineBase.TARGET_TYPE_HW]
        search_sw = target_type in [CmdLineBase.TARGET_TYPE_ANY,
                                    CmdLineBase.TARGET_TYPE_SW]
        if search_hw:
            hw_targets = self.setup.hw_targets.keys()
            found_hw = target in hw_targets
        if search_sw:
            sw_targets = self.setup.sw_targets.keys()
            found_sw = target in sw_targets
        if not found_hw and not found_sw:
            msg = "Could not find target '{0:s}'.".format(target)
            if search_hw:
                msg += " Valid hardware targets are '{0:s}'." \
                       .format("', '".join(sorted(hw_targets)))
            if search_sw:
                msg += " Valid software targets are '{0:s}'." \
                       .format("', '".join(sorted(sw_targets)))
            raise ValueError(msg)
        # End of validate_target().

    def is_valid_target(self, target=None, target_type=TARGET_TYPE_ANY):
        res = True
        try:
            self.validate_target(target, target_type)
        except ValueError:
            res = False
        # End of is_valid_target().
        return res

    def main(self):
        """This is the main method. This is where things happen."""
        # End of main().

    def error(self, msg):
        print >> sys.stderr, msg
        sys.exit(1)
        # End of error().

    def warning(self, msg):
        print >> sys.stderr, msg
        # End of warning().

    def get_script_dir(self):
        res = os.path.dirname(os.path.realpath(__file__))
        # End of get_script_dir().
        return res

    def build_python_cmd(self, cmd_base):
        cmd = 'python {0:s}'.format(os.path.join(self.get_script_dir(), cmd_base))
        if self.verbose:
            cmd += ' --verbose'
        # End of build_cmd().
        return cmd

    # End of class CmdLineBase.

###############################################################################

if __name__ == "__main__":

    res = CmdLineBase().run()

    print "Done"
    sys.exit(res)

###############################################################################
