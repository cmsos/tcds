#!/usr/bin/env python

import sys
from xml.dom import minidom
import urllib2

import pdb
try:
    import debug_hook
except ImportError:
    pass

######################################################################

def ParseSoapReplyToString(soapMsg):

    # DEBUG DEBUG DEBUG
    # Some assumptions are made on the format of the SOAP message we
    # got back from XDAQ.
    assert soapMsg.childNodes[0].tagName.lower() == "soap-env:envelope"
    assert soapMsg.childNodes[0].childNodes[0].tagName.lower() == "soap-env:header"
    assert soapMsg.childNodes[0].childNodes[1].tagName.lower() == "soap-env:body"
    assert len(soapMsg.childNodes[0].childNodes[1].childNodes) == 1
    # DEBUG DEBUG DEBUG end

    replyNode = soapMsg.childNodes[0].childNodes[1].childNodes[0]
    replyTmp = replyNode.tagName

    # DEBUG DEBUG DEBUG
    assert replyTmp.startswith("xdaq:")
    # DEBUG DEBUG DEBUG end

    result = "unknown"
    reply = replyTmp[5:]
    if reply == "Fault":
        # Parse the details of the failure from the SOAP message.
        faultNode = replyNode.getElementsByTagName("xdaq:FaultCode")[0]
        faultCode = faultNode.childNodes[0].nodeValue
        faultNode = replyNode.getElementsByTagName("xdaq:FaultString")[0]
        faultString = faultNode.childNodes[0].nodeValue
        result = "{0:s}: {1:s}, {2:s}".format(reply, faultCode, faultString)

    # End of ParseSoapReplyToString().
    return result

######################################################################

if __name__ == "__main__":

    # Some overall definitions.
    envelopeURL = "http://schemas.xmlsoap.org/soap/envelope/"
    encodingURL = "http://schemas.xmlsoap.org/soap/encoding/"
    # Defined in XDAQ:
    #   https://svnweb.cern.ch/trac/cmsos/browser/trunk/daq/xdaq/include/xdaq/NamespaceURI.h
    xdaqNamespaceURI = "urn:xdaq-soap:3.0"

    sepLine = "-" * 50

    ##########

    # This is the main DOM document.
    doc = minidom.Document()

    # Create and attach the SOAP envelope.
    envelope = doc.createElementNS(envelopeURL, "soap-env:Envelope")
    envelope.setAttribute("xmlns:soap-env", envelopeURL)
    envelope.setAttribute("soap-env:encodingStyle", encodingURL)
    doc.appendChild(envelope)

    # Create the header and attach it to the envelope.
    header = doc.createElement("soap-env:Header")
    envelope.appendChild(header)

    # Create the SOAP body and attach it to the envelope.
    body = doc.createElement("soap-env:Body")
    envelope.appendChild(body)

    # Create and attach the body elements.
    command = "Configure"
    cmdElement = doc.createElement("xdaq:{0:s}".format(command))
    cmdElement.setAttribute("xmlns:xdaq", xdaqNamespaceURI)
    body.appendChild(cmdElement)

    soapMessage = doc.toxml()

    print sepLine
    print "Raw SOAP message to be sent:"
    print sepLine
    print doc.toprettyxml()
    print sepLine

    ##########

    host = "http://cmstcdslab.cern.ch:1972"
    className = "glib::GlibController"
    instanceNumber = 0

    msg = "Sending SOAP message to {0:s} instance {1:d} on host {2:s}"
    print msg.format(className, instanceNumber, host)

    headers = {
        "Content-Location" : "urn:xdaq-application:class={0:s},instance={1:d}".format(className, instanceNumber),
        "Content-type" : "application/soap+xml",
        "Content-length" : len(soapMessage)
        }
    req = urllib2.Request(host, soapMessage, headers)
    result = None
    try:
        reply = urllib2.urlopen(req)
        result = reply.read()
    except urllib2.HTTPError, err:
        print >> sys.stderr, \
            "ERROR Problem sending SOAP message."
        print >> sys.stderr, \
            "ERROR The server could not fullfill the request. Error code: {0:d}".format(err.code)
    except urllib2.URLError, err:
        print >> sys.stderr, \
            "ERROR Problem sending SOAP message."
        print >> sys.stderr, \
            "ERROR Failed to reach the server:"
        print >> sys.stderr, str(err)

    ##########

    response = minidom.parseString(result)

    print sepLine
    print "Raw SOAP response:"
    print sepLine
    print response.toprettyxml()
    print sepLine

    print "Received reply: {0:s}".format(ParseSoapReplyToString(response))

    ##########

    print "Done"

######################################################################
