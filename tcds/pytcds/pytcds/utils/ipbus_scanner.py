#!/usr/bin/env python

######################################################################
## Usage:
##
## Loops over all IP addresses in the specified range. Send a standard
## IPbus 'byte-order' message and see if there is a response that
## looks like an IPbus one. Then print a list of all found IPbus
## devices.
##
## NOTE: There is only a very rough check on the response of the
## device. The fact that the thing responds to an IPbus packet is a
## dead give-away though.
######################################################################

# NOTE: It would be nice to move from optparse to argparse, but we
# have to stay compatible with Python versions < 2.7 on SLC5.
import optparse
import signal
import socket
import struct
import sys

from tcds_utils_networking import parse_ip_arguments

try:
    import debug_hook
    import pdb
except ImportError:
    pass

######################################################################

# The byte-order identification code for IPbus 1.3.
IPBUS13_TYPE_BYTE_ORDER = 0x1f

# The byte-order identification code for IPbus 2.0.
IPBUS20_TYPE_BYTE_ORDER = 0xf

# The packet type for IPbus 2.0 status messages.
IPBUS20_PACKET_TYPE_STATUS = 0x1

IPBUS_VERSIONS = [1.3, 2.0]

######################################################################

# Define a signal handler to gracefully exit on ctrl-c.
def sigh(signal, frame):
    print "Detected ctrl-c: exiting."
    sys.exit()

######################################################################

# def IPStringToInt(ip_str):
#     ip_int = struct.unpack("!I", socket.inet_pton(socket.AF_INET, ip_str))[0]
#     return ip_int

# ######################################################################

# def IPIntToString(ip_int):
#     ip_str = socket.inet_ntop(socket.AF_INET, struct.pack('!I', ip_int))
#     return ip_str

######################################################################

def ipbus_packet_13(transaction_id, transaction_len, transaction_type,
                    is_request=True):
    vers = 1
    ipbus_word = (vers << 28) + \
                 (transaction_id << 17) + \
                 (transaction_len << 8) + \
                 (transaction_type << 3) + \
                 ((not is_request) << 2)

    res0 = chr((ipbus_word & 0xff000000) >> 24)
    res1 = chr((ipbus_word & 0x00ff0000) >> 16)
    res2 = chr((ipbus_word & 0x0000ff00) >> 8)
    res3 = chr(ipbus_word & 0x000000ff)
    res = "".join([res0, res1, res2, res3])

    # End of ipbus_packet_13().
    return res

######################################################################

def ipbus_header_20(transaction_id, transaction_len, transaction_type, info_code):
    vers = 2
    ipbus_word = (vers << 28) + \
                 (transaction_id << 16) + \
                 (transaction_len << 8) + \
                 (transaction_type << 4) + \
                 info_code

    res0 = chr((ipbus_word & 0xff000000) >> 24)
    res1 = chr((ipbus_word & 0x00ff0000) >> 16)
    res2 = chr((ipbus_word & 0x0000ff00) >> 8)
    res3 = chr(ipbus_word & 0x000000ff)
    res = "".join([res0, res1, res2, res3])

    # End of ipbus_header_20().
    return res

######################################################################

# Construct a byte-order transaction request.
# NOTE: The communication format changed between IPbus versions.
def byte_order_transaction_request(ipbus_version):

    res = None
    if ipbus_version == 1.3:
        res = ipbus_packet_13(0, 0, IPBUS13_TYPE_BYTE_ORDER)
    elif ipbus_version == 2.0:
        res = ipbus_header_20(0, 0, IPBUS20_TYPE_BYTE_ORDER, IPBUS20_PACKET_TYPE_STATUS)
        for i in xrange(15):
            res = res + 4 * chr(0)
    else:
        # DEBUG DEBUG DEBUG
        assert False, "IPbus version {0:.1f} not implemented.".format(ipbus_version)
        # DEBUG DEBUG DEBUG end

    # End of byte_order_transaction_request().
    return res

######################################################################

if __name__ == "__main__":

    desc_str = "Scan a give IP range for the presence of IPbus devices."
    usage_str = "usage: %prog IP"
    arg_parser = optparse.OptionParser(description=desc_str,
                                       usage=usage_str)
    arg_parser.add_option("--port-number", action="store",
                          default=50001,
                          help="Port number to use [default: %default]")
    arg_parser.add_option("--timeout", action="store",
                          default=.1,
                          help="Connection timeout in seconds " \
                          "[default: %default]")
    arg_parser.add_option("--verbose", action="store_true",
                          default=False)
    (options, args) = arg_parser.parse_args()

    # The port number to use when scanning.
    port_number = options.port_number
    # The connection timeout in seconds.
    timeout = options.timeout
    verbose = options.verbose

    # All arguments are interpreted as IP addresses or ranges.
    if len(args) < 1:
        print >> sys.stderr, \
            "ERROR Need at least one IP address/host name to scan."
        sys.exit(1)
    ip_addresses_tmp = args
    try:
        ip_list = parse_ip_arguments(ip_addresses_tmp)
    except ValueError, err:
        print >> sys.stderr, "ERROR {0:s}".format(err.args[0])
        sys.exit(1)
    ip_addresses = ip_list.keys()
    ip_addresses.sort()

    msg = "Scanning hosts/IP addresses {0:s} for IPbus devices."
    print msg.format(", ".join(ip_addresses_tmp))
    print "Type ctrl-c any time to exit."

    # Install the ctrl-c signal handler.
    signal.signal(signal.SIGINT, sigh)

    ipbus_device_ips = []
    for ip in ip_addresses:
        host = ip_list[ip]
        if verbose:
            print "Trying {0:s}".format(host)

        # Try each of the various versions of the IPbus protocol.
        for ipbus_version in IPBUS_VERSIONS:

            try:
                s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                s.connect((host, port_number))
                s.settimeout(timeout)
            except socket.error, err:
                print "{0:-15s} : {1:s}".format(host, err.args[1])
                break

            # Initiate an IPbus "byte order" transaction.
            ipbus_request = byte_order_transaction_request(ipbus_version)
            data = ""
            try:
                s.sendall(ipbus_request)
                data = s.recv(1024)
            except socket.timeout:
                pass
            except socket.error, err:
                print "{0:-15s} : {1:s}".format(host, err.args[1])
                break
            s.close()
            r = ""
            if len(data):
                if (len(data) > 3) and \
                       (data[0:3] == ipbus_request[0:3]):
                    ipbus_device_ips.append((host, ipbus_version))
                    r = " IPbus device"
                    msg = "{0:-15s} : IPbus device (IPbus {1:.1f}-like)"
                    print msg.format(host, ipbus_version)

                # No need to try any other IPbus versions. Just break
                # the loop.
                break

    ipbus_device_ips.sort()
    if len(ipbus_device_ips):
        print "Found {0:d} IPbus clients:".format(len(ipbus_device_ips))
        for (ip, version) in ipbus_device_ips:
            print "  {0:s}: IPbus {1:.1f}-like".format(ip, version)
    else:
        print "Found no IPbus devices."

    print "Done"

######################################################################
