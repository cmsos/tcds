#!/usr/bin/env python

###############################################################################
## A little script to send SOAP messages to TCDS applications.
###############################################################################

import os
import sys

from pytcds.utils.tcds_cmd_line_base import CmdLineBase
from pytcds.utils.tcds_utils_soap import build_xdaq_soap_command_message
from pytcds.utils.tcds_utils_soap import extract_xdaq_soap_fault
from pytcds.utils.tcds_utils_soap import send_xdaq_soap_message
from pytcds.utils.tcds_utils_soap import SOAP_PROTOCOL_VERSION_DEFAULT

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class SOAPSender(CmdLineBase):

    SOAP_VERSION_CHOICES = ["soap11", "soap12"]

    # The (string) identifier of the RunControl session 'owning' the
    # TCDS hardware.
    RC_SESSION_ID = 0
    LEASE_OWNER_ID = "PM-ME"

    def __init__(self, description=None, usage=None, epilog=None):
        super(SOAPSender, self).__init__(description, usage, epilog)
        # The target and command come from the command line.
        self.command = None
        self.command_parameters = None
        self.rc_session_id = SOAPSender.RC_SESSION_ID
        self.lease_owner_id = SOAPSender.LEASE_OWNER_ID
        # SOAP version choice between 1.1 and 1.2.
        self.soap_protocol_version = SOAP_PROTOCOL_VERSION_DEFAULT
        # End of __init__().

    def setup_parser_custom(self):
        # Start with the basic parser configuration.
        super(SOAPSender, self).setup_parser_custom()

        # Add the choice between SOAP protocol v1.1 and v.12.
        help_str = "SOAP protocol version to use. " \
                   "Options: '{0:s}'. " \
                   "[default: '%(default)s']"
        help_str = help_str.format("', '".join(SOAPSender.SOAP_VERSION_CHOICES))
        self.parser.add_argument("-s", "--soap-version",
                                 type=str,
                                 action="store",
                                 dest="soap_version",
                                 choices=SOAPSender.SOAP_VERSION_CHOICES,
                                 default="soap11",
                                 help=help_str)

        # Add the name of the SOAP command to send.
        help_str = "SOAP command to send."
        self.parser.add_argument("command",
                                 type=str,
                                 action="store",
                                 help=help_str)

        # Add the optional SOAP command parameters.
        help_str = "SOAP command parameters to send."
        self.parser.add_argument("parameters",
                                 type=str,
                                 action="store",
                                 nargs='*',
                                 help=help_str)

        # End of setup_parser().

    def handle_args(self):
        super(SOAPSender, self).handle_args()

        # On the command line we require one target application and
        # one command.
        self.target = self.args.target
        self.command = self.args.command
        tmp = self.args.parameters
        self.command_parameters = self.parse_command_parameters(tmp)

        self.soap_version = self.args.soap_version
        # End of handle_args().

    def main(self):

        # Check if the target name is valid.
        self.validate_target(target_type=CmdLineBase.TARGET_TYPE_SW)

        #----------

        # Now actually do what we have been asked to do: send a SOAP
        # command.
        target_info = self.setup.sw_targets[self.target]
        host_name = target_info.host
        port_number = target_info.port
        lid_number = target_info.lid
        command_name = self.command
        parameters = self.command_parameters
        soap_msg = build_xdaq_soap_command_message(self.rc_session_id,
                                                   self.lease_owner_id,
                                                   command_name,
                                                   None,
                                                   parameters,
                                                   self.soap_version)
        soap_reply = send_xdaq_soap_message(host_name,
                                            port_number,
                                            lid_number,
                                            soap_msg,
                                            soap_protocol_version=self.soap_version,
                                            verbose=self.verbose)
        soap_fault = extract_xdaq_soap_fault(soap_reply)
        if soap_fault:
            msg = "SOAP reply indicates a problem: '{0:s}'"
            self.error(msg.format(soap_fault.encode('utf-8')))
        # End of main().

    def parse_command_parameters(self, raw_strings):
        # Expected format: something along the lines of
        # 'bgoNumber:unsignedInt:1 a:string:b'.
        res = {}
        for par_chunk in raw_strings:
            pieces = par_chunk.split(":")
            if len(pieces) != 3:
                msg = "Failed to interpret '{0:s}' as parameter setting."
                self.error(msg.format(par_chunk))
            par_name = pieces[0]
            par_type = pieces[1]
            par_val = pieces[2]
            res[par_name] = (par_type, par_val)
        # End of parse_command_parameters().
        return res

    # End of class SOAPSender.

###############################################################################

if __name__ == "__main__":

    desc_str = "Script to send SOAP messages to TCDS applications."

    res = SOAPSender(desc_str).run()

    print "Done"
    sys.exit(res)

###############################################################################
