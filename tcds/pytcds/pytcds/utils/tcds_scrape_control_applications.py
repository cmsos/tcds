#!/usr/bin/env python

###############################################################################
## A small helper script to scrape a given parameter from all TCDS
## control applications.
###############################################################################

import json
import os
import sys
import time
import urllib2

from collections import namedtuple

from pytcds.utils.tcds_cmd_line_base import CmdLineBase

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

AppInfo = namedtuple('AppInfo', ['service', 'context'])

###############################################################################

class TCDSScraper(CmdLineBase):

    def __init__(self, description=None, usage=None, epilog=None):
        super(TCDSScraper, self).__init__(description, usage, epilog)
        # BUG BUG BUG
        # LAS URL hard-coded for the moment... Could be determined
        # based on the location information, actually...
        self.las_url = "tcds-xaas.cms:9945/urn:xdaq-application:service=xmaslas2g"
        # BUG BUG BUG end

        # BUG BUG BUG
        # For the moment also the parameter to query is
        # hard-coded... This should come from the command line,
        # really.

        # # Run number.
        # self.parameter_str = "Application configuration:Run number"

        # Is the PI TTS history logging enabled?
        self.parameter_str = "Application configuration:TTS history logging enabled"

        # # Check if B-channel 12 is off or not.
        # self.parameter_str = "itemset-bchannel12-config:Emission mode"

        # BUG BUG BUG end
        # End of __init__().

    def setup_parser_custom(self):
        # # Start with the basic parser configuration.
        # super(TCDSScraper, self).setup_parser_custom()
        pass
        # End of setup_parser_custom().

    # def handle_args(self):
    #     super(TCDSScraper, self).handle_args()
    #     # End of handle_args().

    def main(self):

        # Use the tcds_common flash list to build a list of all TCDS
        # control applications.
        applications = {}
        url = "http://" + self.las_url + "/retrieveCollection?fmt=json&flash=urn:xdaq-flashlist:tcds_common"
        response = urllib2.urlopen(url)
        json_data = response.read()
        data = json.loads(json_data)
        rows = data['table']['rows']
        for row in rows:
            service = row['service']
            context = row['context']
            applications[service] = AppInfo(service, context)

        # Now loop over all applications and scrape for some
        # parameters.
        service_names = applications.keys()
        max_len = max([len(i) for i in service_names])
        parameter_str_steps = self.parameter_str.split(':')
        for (service, app_info) in sorted(applications.iteritems()):
            context = app_info.context
            url = context + "/urn:xdaq-application:service=" + service + "/update"
            response = urllib2.urlopen(url)
            json_data = response.read()
            data = json.loads(json_data)
            tmp = data
            try:
                for step in parameter_str_steps:
                    tmp = tmp[step]
            except KeyError:
                tmp = None
            print "{1:{0:d}s}: {2:s}".format(max_len, service, tmp)
            time.sleep(.5)

        # End of main().

    # End of class TCDSScraper.

###############################################################################

if __name__ == "__main__":

    desc_str = "A small helper script " \
               "to scrape a given parameter " \
               "from all TCDS control applications."

    res = TCDSScraper(desc_str).run()

    print "Done"
    sys.exit(res)

###############################################################################
