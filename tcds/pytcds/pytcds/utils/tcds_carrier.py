###############################################################################
## The Carrier class handles all generic FMC-carrier business.
## NOTE: Carrier inherits from BaseBoard, which handles everything
## that the AMC13 (in its guise as CPM) has in common with the
## GLIB/FC7.
###############################################################################

from pytcds.utils.tcds_baseboard import BaseBoard
from pytcds.utils.tcds_constants import sfp_bit_mapping
from pytcds.utils.tcds_hwinterfacewrapper import HwInterfaceWrapper
from pytcds.utils.tcds_i2c import I2CAccessor

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

# The address of the TCDS FMC module type register.
I2C_ADDRESS_FMC_TYPE = 0x38

# The address of the SFP status registers on the TCDS FMCs.
I2C_ADDRESS_SFP_STATUS_REGISTERS = 0x38

# The address of the SFP vendor-info registers on the TCDS FMCs.
I2C_ADDRESS_SFP_VENDOR_INFO = 0x50

# The address of the extended SFP diagnostics.
I2C_ADDRESS_SFP_DIAGNOSTICS = 0x51

MUX_CHANNELS_SFP_STATUS = {
    "mod_abs" : 1,
    "tx_dis" : 2,
    "tx_fault" : 3,
    "rx_los" : 4
}

SFP_STATUS_TYPES = [
    "mod_abs",
    "tx_dis",
    "tx_fault",
    "rx_los"
]

# This is the I2C address of a 'fake' I2C device implemented in
# firmware. It provides access to the MAC address and IP address
# information presented to the IPbus firmware.
I2C_ADDRESS_MAC_AND_IP_FIRMWARE = 0x7e

# Some enums.
MAC_ADDRESS_FROM_EEPROM = 0
MAC_ADDRESS_FROM_SLAVE = 1
MAC_ADDRESS_FROM_USER_FW = 2
IP_ADDRESS_FROM_EEPROM = 3
IP_ADDRESS_FROM_SLAVE = 4
IP_ADDRESS_FROM_USER_FW = 5

###############################################################################

class Carrier(BaseBoard):

    def __init__(self, carrier_type, board_type, hw_interface, verbose=False):
        self.hw = HwInterfaceWrapper(carrier_type, board_type, hw_interface)
        self.verbose = verbose
        # This one should be filled in by descendant classes.
        self.user_fw_base_name = None
        # End of __init__().

    #----------

    def get_user_system_id(self):
        res = self.get_system_id("user")
        # End of get_user_system_id().
        return res

    #----------

    def get_eui(self):
        """Reads the 48-bit unique identifier of the board.

        The firmware actually reads this information from an EEPROM
        upon startup and stores it for easy access via IPbus.

        """
        tmp_hi = self.read("system.eui.hi")
        tmp_lo = self.read("system.eui.lo")
        eui_bytes = [
            ((tmp_hi & 0x0000ff00) >> 8),
            (tmp_hi & 0x000000ff),
            ((tmp_lo & 0xff000000) >> 24),
            ((tmp_lo & 0x00ff0000) >> 16),
            ((tmp_lo & 0x0000ff00) >> 8),
            (tmp_lo & 0x000000ff)
        ]
        eui48 = tuple(eui_bytes)
        # End of get_eui().
        return eui48

    def get_mac_address(self):
        """Reads the MAC address of the board.

        The firmware actually reads this information from an EEPROM
        upon startup and stores it for easy access via IPbus.

        """
        tmp_hi = self.read("system.mac_address.hi")
        tmp_lo = self.read("system.mac_address.lo")
        mac_address_bytes = [
            ((tmp_hi & 0x0000ff00) >> 8),
            (tmp_hi & 0x000000ff),
            ((tmp_lo & 0xff000000) >> 24),
            ((tmp_lo & 0x00ff0000) >> 16),
            ((tmp_lo & 0x0000ff00) >> 8),
            (tmp_lo & 0x000000ff)
        ]
        mac_address = tuple(mac_address_bytes)
        # End of get_mac_address().
        return mac_address

    def get_serial_number(self):
        raise NotImplementedError("Carrier.get_serial_number() is not implemented.")
        # End of get_serial_number().
        return serial_number

    def get_fmc_locations(self):
        raise NotImplementedError("Carrier.get_fmc_locations() is not implemented.")
        # End of get_fmc_locations().

    def get_fmc_i2c_bus_select(self, fmc):
        raise NotImplementedError("Carrier.get_fmc_i2c_bus_select() is not implemented.")
        # End of get_fmc_i2c_bus_select().

    def get_fmc_i2c_address_mux_sfp_status(self, fmc):
        raise NotImplementedError("Carrier.get_fmc_i2c_address_mux_sfp_status() is not implemented.")
        # End of get_fmc_i2c_address_mux_sfp_status().

    def get_fmc_i2c_address_mux_sfp_info_registers(self, fmc):
        raise NotImplementedError("Carrier.get_fmc_i2c_address_mux_sfp_info_registers() is not implemented.")
        # End of get_fmc_i2c_address_mux_sfp_info_registers().

    #----------

    def perform_powerup_sequence(self):
        """Performs everything necessary to prepare the board for use."""
        # The default behavior is to do nothing.
        pass
        # End of perform_powerup_sequence().

    def is_fmc_powered(self, fmc):
        raise NotImplementedError("Carrier.is_fmc_powered() is not implemented.")
        # End of is_fmc_powered().
        return res

    def is_fmc_present(self, fmc):
        reg_name = "{0:s}.status.fmc_{1:s}_present"
        reg_name = reg_name.format(self.user_fw_base_name, fmc)
        tmp = self.read(reg_name)
        res = (tmp == 0x1)
        # End of is_fmc_present().
        return res

    #----------

    def read_fmc_type(self, fmc):
        module_type = None

        reg_name = "{0:s}.status.fmc_{1:s}_present"
        reg_name = reg_name.format(self.user_fw_base_name, fmc)
        fmc_present = self.read(reg_name)
        if fmc_present:
            bus_select = self.get_fmc_i2c_bus_select(fmc)
            i2c_address = self.get_fmc_i2c_address_mux_sfp_status(fmc)
            channel_num = 0x0
            msg = "Select mux channel {0:d}: module type".format(channel_num)
            with I2CAccessor(self, "user", bus_select) as i2c:
                i2c.write_register(i2c_address,
                                   0x1 << channel_num,
                                   msg)
                module_type = i2c.read_register(I2C_ADDRESS_FMC_TYPE,
                                                "Read module type")

        # End of read_fmc_type().
        return module_type

    #----------

    # The mapping of the SFP number to the corresponding channel on
    # the I2C register. The physical SFPs are numbered 1-8, top to
    # bottom, left to right. The register bits [0:7] respectively
    # correspond to SFP lines A-H on the PCB, which run in the
    # opposite direction.
    # NOTE: Of course the real 'problem' here is that this mapping
    # depends on the FMC type.
    def _sfp_number_to_register_bit(self, fmc, sfp_num):
        fmc_type = self.read_fmc_type(fmc)
        bit_num = sfp_bit_mapping[fmc_type][sfp_num]
        # End of _sfp_number_to_register_bit().
        return bit_num

    #----------

    def read_sfp_status(self, fmc, sfp_num):
        bit_num = self._sfp_number_to_register_bit(fmc, sfp_num)
        tmp = self._read_fmc_sfp_status(fmc)
        res = {}
        for (key, val) in tmp.iteritems():
            res[key] = ((val & (0x1 << bit_num)) != 0x0)
        # End of read_sfp_status().
        return res

    #----------

    def _read_fmc_sfp_status(self, fmc):
        res = {}
        bus_select = self.get_fmc_i2c_bus_select(fmc)
        i2c_address = self.get_fmc_i2c_address_mux_sfp_status(fmc)
        with I2CAccessor(self, "user", bus_select) as i2c:
            for status_type in SFP_STATUS_TYPES:
                channel_num = MUX_CHANNELS_SFP_STATUS[status_type]
                msg = "Select mux channel {0:d}: {1:s}".format(channel_num, status_type)
                i2c.write_register(i2c_address,
                                   0x1 << channel_num,
                                   msg)
                status = i2c.read_register(I2C_ADDRESS_SFP_STATUS_REGISTERS,
                                           "Read {0:s}".format(status_type))
                # print "DEBUG JGH fmc '{0:s}' -> '{1:s}': 0x{2:08x}".format(fmc, status_type, status)
                res[status_type] = status
        # End of _read_fmc_sfp_status().
        return res

    #----------

    def read_sfp_vendor_info(self, fmc, sfp_num):
        # This reads straight from the SFP. The memory locations and
        # string lengths come straigh from the specification:
        # 'INF-8074i Specification for SFP (Small Formfactor
        # Pluggable) Transceiver'
        # ftp://ftp.seagate.com/sff/INF-8074.PDF

        keys = ["vendor name",
                "vendor part-number",
                "vendor revision",
                "vendor serial-number"]

        # These define the starting addresses and lengths of the
        # various bits of information.
        mem_info = {"vendor name" : (20, 16),
                    "vendor part-number" : (40, 16),
                    "vendor revision" : (56, 4),
                    "vendor serial-number" : (68, 16)}
        bus_select = self.get_fmc_i2c_bus_select(fmc)
        i2c_address = self.get_fmc_i2c_address_mux_sfp_info_registers(fmc)
        bit_num = self._sfp_number_to_register_bit(fmc, sfp_num)
        res = {}
        with I2CAccessor(self, "user", bus_select) as i2c:
            for key in keys:
                i2c.write_register(i2c_address,
                                   0x1 << bit_num,
                                   "Select SFP-info mux")
                msg = "Read {0:s}".format(key)
                tmp = i2c.read_string(I2C_ADDRESS_SFP_VENDOR_INFO,
                                      mem_info[key][0],
                                      mem_info[key][1],
                                      msg)
                # Convert from unicode to ISO-8859-1 (as per the SFP
                # specs).
                # NOTE: If this does not work, something is wrong.
                try:
                    tmp = tmp.decode("ISO-8859-1")
                except UnicodeEncodeError:
                    tmp = "Unknown, UTF-8 version looks like '{0:s}'".format(tmp.encode("utf-8"))
                res[key] = tmp
        # End of read_sfp_vendor_info().
        return res

    #----------

    def read_sfp_diagnostic_info(self, fmc, sfp_num):
        # This reads straight from the SFP. The memory locations etc.
        # come straight from the specification: 'SFF-8472
        # Specification for Diagnostic Monitoring Interface for
        # Optical Transceivers'
        # ftp://ftp.seagate.com/sff/SFF-8472.PDF

        bus_select = self.get_fmc_i2c_bus_select(fmc)
        i2c_address = self.get_fmc_i2c_address_mux_sfp_info_registers(fmc)
        bit_num = self._sfp_number_to_register_bit(fmc, sfp_num)

        # NOTE: So far only 'internal calibration' diagnostics results
        # are implemented. So if bit 5 of address 92 is not set, we
        # don't know what to do.
        with I2CAccessor(self, "user", bus_select) as i2c:
            i2c.write_register(i2c_address,
                               0x1 << bit_num,
                               "Select SFP-info mux")
            msg = "Check internal/external calibration flag"
            tmp = i2c.read_memory(I2C_ADDRESS_SFP_VENDOR_INFO,
                                  92,
                                  msg)
            internal_calibration = ((tmp & 0x20) != 0x0)
            if not internal_calibration:
                msg = "Diagnostic information is only implemented " \
                      "for SFPs with internally-calibrated measurements."
                raise RuntimeError(msg)

        # These define the starting addresses of the various bits of
        # information, and the transformation/decoding function.
        mem_info = {
            "temperature (C)" : (96, self._transform_temp),
            "Vcc (V)" : (98, self._transform_vcc),
            "TX bias current (uA)" : (100, self._transform_itx),
            "TX power (mW)" : (102, self._transform_power),
            "RX power (mW)" : (104, self._transform_power)
        }
        res = {}
        with I2CAccessor(self, "user", bus_select) as i2c:
            for (key, (address, transform)) in mem_info.iteritems():
                i2c.write_register(i2c_address,
                                   0x1 << bit_num,
                                   "Select SFP-info mux")
                msg = "Read {0:s}".format(key)
                tmp = i2c.read_memory_16b(I2C_ADDRESS_SFP_DIAGNOSTICS,
                                          address,
                                          msg)
                if transform:
                    tmp = transform(tmp)
                res[key] = tmp
        # End of read_sfp_diagnostic_info().
        return res

    def _transform_temp(self, raw_in):
        # Simply take the MSB, and forget about the decimal part. The
        # result is the approximate temperature in degrees Cesius.
        res = 1. * (raw_in >> 8)
        # End of _transform_tmp().
        return res

    def _transform_vcc(self, raw_in):
        # The supply voltage is represented as a 16-bit unsigned
        # integer with the voltage defined as the full 16-bit value (0
        # - 65535) with LSB equal to 100 uVolt. The result is in Volt.
        res = raw_in  * 100.e-6
        # End of _transform_vcc().
        return res

    def _transform_itx(self, raw_in):
        # The TX bias current is represented as a 16-bit unsigned
        # integer with the current defined as the full 16-bit value (0
        # - 65535) with LSB equal to 2 uA. The result is in uA.
        res = raw_in * 2.e-6 * 1.e3
        # End of _transform_itx.
        return res

    def _transform_power(self, raw_in):
        # The TX/RX power is represented as a 16-bit unsigned integer
        # with the power defined as the full 16-bit value (0 - 65535)
        # with LSB equal to 0.1 uW. The result is in mW.
        res = raw_in * .1e-6 * 1.e3
        # End of _transform_power.
        return res

    #----------

    def switch_fmc_sfps(self, state, fmc=None):

        # Check the input.
        new_state = state.lower()
        # ASSERT ASSERT ASSERT
        assert new_state in ["on", "off"]
        # ASSERT ASSERT ASSERT end

        fmcs = None
        if not fmc is None:
            fmcs = [fmc]
        else:
            fmcs = self.get_fmc_locations()

        state_val = 0xff
        if new_state == "on":
            state_val = 0x0

        for fmc in fmcs:
            if self.is_fmc_present(fmc):
                bus_select = self.get_fmc_i2c_bus_select(fmc)
                with I2CAccessor(self, "user", bus_select) as i2c:
                    i2c_address = self.get_fmc_i2c_address_mux_sfp_status(fmc)
                    channel_num = MUX_CHANNELS_SFP_STATUS["tx_dis"]
                    msg = "Select mux channel {0:d}: tx_dis".format(channel_num)
                    i2c.write_register(i2c_address,
                                       0x1 << channel_num,
                                       msg)
                    i2c.write_register(I2C_ADDRESS_SFP_STATUS_REGISTERS,
                                       state_val,
                                       "Write tx_dis = 0x{0:x}".format(state_val))

        # End of switch_fmc_sfps().

    #----------

    def load_firmware(self, image_name=None, recover_connection=True):
        raise NotImplementedError("Carrier.load_firmware is virtual.")
        # End of load_firmware().

    def reload_firmware(self):
        self.load_firmware()
        # End of reload_firmware().

    def is_user_firmware_loaded(self):
        res = (self.get_system_id("user") != "gold")
        # End of is_user_firmware_loaded().
        return res

    def ensure_user_firmware_loaded(self):
        if not self.is_user_firmware_loaded():
            self.load_firmware("user")
        # End of ensure_user_firmware_loaded().

    # End of class Carrier.

###############################################################################
