#!/usr/bin/env python

import json
import re

from pytcds.extern.json_minify import json_minify
from pytcds.utils.tcds_constants import CRATE_TYPE_UTCA
from pytcds.utils.tcds_constants import CRATE_TYPE_VME

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################


# def ends_in_number(str_in):
#     return bool(get_tail_number(str_in))


# def get_tail_number(str_in):
#     return re.split('[^\d]', str_in)[-1]


# def get_pre_tail_number(str_in):
#     res = str_in
#     if ends_in_number(str_in):
#         tail_number = get_tail_number(str_in)
#         res = str_in[:-len(tail_number)]
#     return res


def list_duplicates(input_list):
    seen = set()
    dups = set()
    for item in input_list:
        if item in seen:
            dups.add(item)
        else:
            seen.add(item)
    return list(dups)


def dict_raise_on_duplicates(ordered_pairs):
    """Reject duplicate keys."""
    d = {}
    for (k, v) in ordered_pairs:
        if k in d:
            raise ValueError("Found duplicate key: {0:s}".format(k))
        else:
            d[k] = v
    return d


def hw_sorter_helper(hardware):
    type_order = {
        'cpm-t1' : 1,
        'cpm-t2' : 2,
        'lpm' : 3,
        'ipm' : 4,
        'ici' : 5,
        'apve' : 6,
        'pi' : 7
    }
    order = type_order.get(hardware.type, 99)
    identifier = hardware.identifier
    # End of hw_sorter_helper().
    return (order, identifier)


def sw_sorter_helper(hardware):
    identifier = hardware.identifier
    # End of sw_sorter_helper().
    return identifier


# Gratefully taken from
#   http://stackoverflow.com/questions/241327/python-snippet-to-remove-c-and-c-comments
def comment_remover(text):
    def replacer(match):
        s = match.group(0)
        if s.startswith('/'):
            return " " # note: a space and not an empty string
        else:
            return s
    pattern = re.compile(
        r'//.*?$|/\*.*?\*/|\'(?:\\.|[^\\\'])*\'|"(?:\\.|[^\\"])*"',
        re.DOTALL | re.MULTILINE
    )
    return re.sub(pattern, replacer, text)

###############################################################################


class Software(object):

    def __init__(self, identifier, data):
        self._identifier = identifier
        self._data = data

    @property
    def identifier(self):
        return self._identifier

    @property
    def data(self):
        return self._data

    @property
    def host(self):
        return self._data['host']

    @property
    def port(self):
        return int(self._data['port'])

    @property
    def lid(self):
        return int(self._data['lid'])

    @property
    def app_class(self):
        return self._data['application']

    @property
    def hardware(self):
        return self._data['hardware']

    @property
    def url(self):
        tmp = "http://{0:s}:{1:d}/urn:xdaq-application:lid={2:d}"
        return tmp.format(self.host, self.port, self.lid)

    @property
    def fed_id(self):
        res = int(self._data['fed_id'])
        return res

    # End of class Software.


###############################################################################


class Hardware(object):

    def __init__(self, identifier, data):
        self._identifier = identifier
        self._data = data

    @property
    def identifier(self):
        return self._identifier

    @property
    def data(self):
        return self._data

    @property
    def type(self):
        return self._identifier.split('-')[0].lower()

    @property
    def description(self):
        return self._data['desc']

    @property
    def rack(self):
        # Convention: rack names are all-uppercase.
        rack_id = self._data['rack']
        res = rack_id.upper()
        return res

    @property
    def crate(self):
        # Convention: crate names are all-uppercase.
        crate_id = self._data['crate']
        res = crate_id.upper()
        return crate_id

    @property
    def slot(self):
        slot_id = int(self._data['slot'])
        return slot_id

    @property
    def location(self):
        # Convention: location names are all-uppercase.
        rack_id = self.rack
        crate_id = self.crate
        slot_id = self.slot
        res = '{0:s}-{1:s}-{2:02d}'.format(rack_id, crate_id, slot_id)
        res = res.upper()
        return res

    @property
    def address(self):
        raise NotImplementedError

    @property
    def crate_type(self):
        raise NotImplementedError

    # End of class Hardware.


class HardwareVME(Hardware):

    def __init__(self, identifier, data):
        super(HardwareVME, self).__init__(identifier, data)
        self.vme_chain = 0
        self.vme_unit = 0

    @property
    def address(self):
        res = self._address()
        return res

    def _address(self):
        # Convention: all-lowercase.
        res = 'vme:{0:s}:{1:d}'.format(self.type, self.slot)
        res = res.lower()
        return res

    @property
    def crate_type(self):
        return CRATE_TYPE_VME

    @property
    # BUG BUG BUG
    # Of course this should not be called controlhub...
    def controlhub(self):
        # NOTE: For the VME boards the controlhub address is abused to
        # encode the VME controller chain and unit.
        res = '{0:d}:{1:d}'.format(self.vme_chain, self.vme_unit)
        return res
    # BUG BUG BUG end

    # End of class HardwareVME.


class HardwareUTCA(Hardware):

    def __init__(self, identifier, data):
        super(HardwareUTCA, self).__init__(identifier, data)

    @property
    def address(self):
        res = self.dns_alias
        return res

    @property
    def crate_type(self):
        return CRATE_TYPE_UTCA

    @property
    def dns_alias(self):
        # Convention: DNS aliases are all-lowercase.
        location = self.location
        res = 'amc-{0:s}'.format(location)
        res = res.lower()
        return res

    @property
    def controlhub(self):
        # Convention: controlhubs can be found based on the crate. For
        # example: the controlhub for crate s1e02-10 is called
        # bridge-s1e02-10.
        # Convention: all-lowercase, like (other) DNS aliases.
        res = 'bridge-{0:s}-{1:s}'.format(self.rack, self.crate)
        res = res.lower()
        return res

    @property
    def board(self):
        return self

    # End of class HardwareUTCA.


class TCDSSetup(object):

    def __init__(self, identifier, boards, applications, no_checks=False, verbose=False):
        self._identifier = identifier
        self._boards = boards
        self.applications = applications
        self.no_checks = no_checks
        self.verbose = verbose
        self.create_structure()

    @property
    def identifier(self):
        return self._identifier

    @property
    def racks(self):
        tmp = [i.rack for i in self.boards]
        res = list(set(tmp))
        return res

    @property
    def crates(self):
        tmp = [(i.rack, i.crate) for i in self.boards]
        res = list(set(tmp))
        return res

    @property
    def utca_crates(self):
        tmp = [(i.rack, i.crate) for i in self.boards if isinstance(i, HardwareUTCA)]
        res = list(set(tmp))
        return res

    @property
    def vme_crates(self):
        tmp = [(i.rack, i.crate) for i in self.boards if isinstance(i, HardwareVME)]
        res = list(set(tmp))
        return res

    @property
    def boards(self):
        return [i for i in self.hw_targets.values() if not i.type in ['ici', 'apve']]

    @property
    def pis(self):
        return [i[1] for i in self._boards.iteritems() if i[1].type == 'pi']

    @property
    def icis(self):
        return [i[1] for i in self._boards.iteritems() if i[1].type == 'ici']

    @property
    def apves(self):
        return [i[1] for i in self._boards.iteritems() if i[1].type == 'apve']

    @property
    def ipms(self):
        return [i[1] for i in self._boards.iteritems() if i[1].type == 'ipm']

    @property
    def lpms(self):
        return [i[1] for i in self._boards.iteritems() if i[1].type == 'lpm']

    @property
    def cpms(self):
        return [i[1] for i in self._boards.iteritems() if i[1].type == 'cpm']

    @property
    def others(self):
        return [i[1] for i in self._boards.iteritems() \
                if not i[1].type in ['cpm', 'lpm', 'ipm', 'ici', 'apve', 'pi']]

    @property
    def hw_targets(self):
        res = {}
        # NOTE: CPMs are special: they have two targets.
        cpms = self.cpms
        t1s = [CPMT1(i.identifier, i.data, 't1') for i in cpms]
        t2s = [CPMT1(i.identifier, i.data, 't2') for i in cpms]
        for tmp in [self.pis, self.icis, self.apves, self.ipms, self.lpms, self.others, t1s, t2s]:
            res.update(dict(zip([i.identifier for i in tmp], tmp)))
        return res

    @property
    def sw_targets(self):
        return self.applications

    def create_structure(self):

        # Check that all PIs have unique locations.
        if self.verbose:
            "Checking for duplicate PI locations"
        pis = [i for i in self._boards.values() if i.type == 'pi']
        pi_locations = [i.location for i in pis]
        duplicate_locations = list_duplicates(pi_locations)
        if len(duplicate_locations):
            tmp_str = ""
            for loc in sorted(duplicate_locations):
                duplicate_pis = [i for i in pis if i.location == loc]
                tmp = [i.identifier for i in duplicate_pis]
                if len(tmp_str):
                    tmp_str += ", "
                tmp_str += "{0:s} for '{1:s}'".format(loc, "', '".join(tmp))
            msg = "Found oversubscribed PI locations: {0:s}"
            raise ValueError(msg.format(tmp_str))

        #----------

        # Check that all LPMs have unique locations.
        if self.verbose:
            "Checking for duplicate LPM locations"
        lpm_locations = [i.location for i in self.lpms]
        duplicate_locations = list_duplicates(lpm_locations)
        if len(duplicate_locations):
            tmp_str = ""
            for loc in sorted(duplicate_locations):
                duplicate_lpms = [i for i in self.lpms \
                                  if i.location == loc]
                tmp = [i.identifier for i in duplicate_lpms]
                if len(tmp_str):
                    tmp_str += ", "
                tmp_str += "{0:s} for '{1:s}'".format(loc, "', '".join(tmp))
            msg = "Found oversubscribed LPM locations: {0:s}"
            raise ValueError(msg.format(tmp_str))

        #----------

        # Check that all CPMs have unique locations.
        if self.verbose:
            "Checking for duplicate CPM locations"
        cpm_locations = [i.location for i in self.cpms]
        duplicate_locations = list_duplicates(cpm_locations)
        if len(duplicate_locations):
            tmp_str = ""
            for loc in sorted(duplicate_locations):
                duplicate_cpms = [i for i in self.cpms if i.location == loc]
                tmp = [i.identifier for i in duplicate_cpms]
                if len(tmp_str):
                    tmp_str += ", "
                tmp_str += "{0:s} for '{1:s}'".format(loc, "', '".join(tmp))
            msg = "Found oversubscribed CPM locations: {0:s}"
            raise ValueError(msg.format(tmp_str))

        #----------

        # The 'smallest functional piece' of the TCDS is a partition,
        # consisting of one or more PIs. So we start building our
        # hardware description by looking for all PIs.
        partitions = {}
        if self.verbose:
            print "Looking for partitions based on PIs"

        for pi in pis:
            pi_id = pi.identifier
            partition_id = '-'.join(pi_id.split('-')[1:])
            # # NOTE: Here we have to handle partitions with more than one
            # # PI, these are expected to be called 'pi-xxx1', 'pi-xxx2',
            # # etc.
            # multi_pi_partition = False
            # if ends_in_number(partition_id):
            #     partition_id = get_pre_tail_number(partition_id)
            #     multi_pi_partition = True
            if not len(partition_id):
                msg = "PI id '{0:s}' leads to empty partition id"
                raise ValueError(msg.format(pi_id))
            if self.verbose:
                print "  Found partition with id '{0:s}'.".format(partition_id)
            partition = None
            # if multi_pi_partition:
            try:
                partition = partitions[partition_id]
            except KeyError:
                pass
            if not partition:
                partition = Partition(partition_id)
            partition.add_pi(pi)
            partitions[partition_id] = partition

        self.partitions = partitions

        #----------

        # The next step: find the ICIs connected to all PIs.
        if self.verbose:
            print "Looking for ICIs connected to PIs"

        icis_used = []
        for (partition_id, partition) in sorted(partitions.items()):
            for (pi_id, pi) in sorted(partition.pis.items()):
                ici_id = pi.id_ici_pri
                if ici_id and ici_id.lower() == 'none':
                    ici_id = None
                if ici_id:
                    try:
                        ici_pri = self._boards[ici_id]
                    except KeyError:
                        msg = "Failed to find ICI '{0:s}' connected to PI '{1:s}'"
                        raise ValueError(msg.format(ici_id, pi_id))
                    if self.verbose:
                        print "  Found primary ICI '{0:s}' connected to PI '{1:s}'" \
                            .format(ici_pri.identifier, pi_id)
                    ici_pri.add_pi(pi)
                    pi.set_ici_pri(ici_pri)
                    icis_used.append(ici_pri)
                ici_id = pi.id_ici_sec
                if ici_id and ici_id.lower() == 'none':
                    ici_id = None
                if ici_id:
                    try:
                        ici_sec = self._boards[ici_id]
                    except KeyError:
                        msg = "Failed to find ICI '{0:s}' connected to PI '{1:s}'"
                        raise ValueError(msg.format(ici_id, pi_id))
                    if self.verbose:
                        print "  Found secondary ICI '{0:s}' connected to PI '{1:s}'" \
                            .format(ici_sec.identifier, pi_id)
                    ici_sec.add_pi(pi)
                    pi.set_ici_sec(ici_sec)
                    icis_used.append(ici_sec)

        #----------

        if not self.no_checks:
            # We should have no unused ICIs left.
            ici_ids = [i.identifier for i in self.icis]
            used_ici_ids = set([i.identifier for i in icis_used])
            if sorted(used_ici_ids) != sorted(ici_ids):
                # ASSERT ASSERT ASSERT
                assert len(used_ici_ids) < len(ici_ids)
                # ASSERT ASSERT ASSERT end
                tmp = [i for i in ici_ids if not i in used_ici_ids]
                if len(tmp) > 1:
                    msg = "ICIs '{0:s}' are not mapped to PIs"
                else:
                    msg = "ICI '{0:s}' is not mapped to a PI"
                raise ValueError(msg.format("', '".join(sorted(tmp))))

        #----------

        # The next step: find the APVEs associated with all ICIs.
        if self.verbose:
            print "Looking for APVEs associated with ICIs"

        apves_used = []
        for (partition_id, partition) in sorted(partitions.items()):
            for (pi_id, pi) in sorted(partition.pis.items()):
                # Loop over primary and secondary ICI for each PI.
                for ici in [pi.ici_pri, pi.ici_sec]:
                    if ici:
                        tmp = [i for i in self.apves \
                               if ((i.id_lpm == ici.id_lpm) \
                                   and (i.ici_number == ici.ici_number))]
                        if len(tmp):
                            apve = tmp[0]
                            if self.verbose:
                                print "  Found APVE '{0:s}' connected to ICI '{1:s}'" \
                                    .format(apve.identifier, ici.identifier)
                            ici.set_apve(apve)
                            apve.set_ici(ici)
                            apves_used.append(apve)

        #----------

        if not self.no_checks:
            # We should have no unused APVEs left.
            apve_ids = [i.identifier for i in self.apves]
            used_apve_ids = set([i.identifier for i in apves_used])
            if sorted(used_apve_ids) != sorted(apve_ids):
                # ASSERT ASSERT ASSERT
                assert len(used_apve_ids) < len(apve_ids)
                # ASSERT ASSERT ASSERT end
                tmp = [i for i in apve_ids if not i in used_apve_ids]
                if len(tmp) > 1:
                    msg = "APVEs '{0:s}' are not mapped to ICIs"
                else:
                    msg = "APVE '{0:s}' is not mapped to an ICI"
                raise ValueError(msg.format("', '".join(sorted(tmp))))

        #----------

        # Next step: find the LPMs for each of the ICIs and APVEs.
        if self.verbose:
            print "Looking for the LPMs associated with all ICIs and APVEs"

        lpms_used = []
        for (partition_id, partition) in sorted(partitions.items()):
            for (pi_id, pi) in sorted(partition.pis.items()):
                # Loop over primary and secondary ICI for each PI.
                for ici in [pi.ici_pri, pi.ici_sec]:
                    if ici:
                        tmp = [i for i in self.lpms \
                               if (i.identifier == ici.id_lpm)]
                        try:
                            lpm = tmp[0]
                        except IndexError:
                            msg = "Failed to find LPM '{0:s}' associated with ICI '{1:s}'"
                            raise ValueError(msg.format(ici.id_lpm, ici.identifier))
                        if self.verbose:
                            print "  Found LPM '{0:s}' associated with ICI '{1:s}'" \
                                .format(lpm.identifier, ici.identifier)
                        ici.set_lpm(lpm)
                        lpm.add_ici(ici)
                        lpms_used.append(lpm)

        #----------

        # We should have no unused LPMs left. (It makes no sense to
        # have LPMs that are not associated with any ICIs.)
        lpm_ids = [i.identifier for i in self.lpms]
        used_lpm_ids = set([i.identifier for i in lpms_used])
        # HACK: Exclude dummies from the check.
        tmp_lpm_ids = [i for i in lpm_ids if i.find("dummy") < 0]
        if sorted(used_lpm_ids) != sorted(tmp_lpm_ids):
            # ASSERT ASSERT ASSERT
            assert len(used_lpm_ids) < len(lpm_ids)
            # ASSERT ASSERT ASSERT end
            tmp = [i for i in lpm_ids if not i in used_lpm_ids]
            msg = "LPMs '{0:s}' are not associated with any ICIs"
            #raise ValueError(msg.format("', '".join(tmp)))

        #----------

        ipms = {}
        if self.verbose:
            print "Looking for iPMs"

        ipms = [i for i in self._boards.values() if i.type == 'ipm']

        #----------

        # Next step: find the LPMs for each of the IPMs.
        if self.verbose:
            print "Looking for the LPMs associated with all IPMs"

        lpms_used = []
        for ipm in sorted(ipms):
            tmp = [i for i in self.lpms \
                   if (i.identifier == ipm.id_lpm)]
            try:
                lpm = tmp[0]
            except IndexError:
                msg = "Failed to find LPM '{0:s}' associated with IPM '{1:s}'"
                raise ValueError(msg.format(ipm.id_lpm, ipm.identifier))
            if self.verbose:
                print "  Found LPM '{0:s}' associated with IPM '{1:s}'" \
                    .format(lpm.identifier, ipm.identifier)
            ipm.set_lpm(lpm)
            lpm.add_ipm(ipm)
            lpms_used.append(lpm)

        #----------

        if not self.no_checks:
            # We should have no unused LPMs left.
            lpm_ids = [i.identifier for i in self.lpms]
            used_lpm_ids = set([i.identifier for i in lpms_used])
            # HACK: Exclude dummies from the check.
            tmp_lpm_ids = [i for i in lpm_ids if i.find("dummy") < 0]
            if sorted(used_lpm_ids) != sorted(tmp_lpm_ids):
                # ASSERT ASSERT ASSERT
                assert len(used_lpm_ids) < len(lpm_ids)
                # ASSERT ASSERT ASSERT end
                tmp = [i for i in lpm_ids if not i in used_lpm_ids]
                msg = "LPMs '{0:s}' are not associated with any IPM"
                raise ValueError(msg.format("', '".join(tmp)))

        #----------

        # Next step: find the CPM corresponding to each LPM.
        if self.verbose:
            print "Looking for the CPMs associated with all LPMs"

        cpms_used = []
        for (partition_id, partition) in sorted(partitions.items()):
            for (pi_id, pi) in sorted(partition.pis.items()):
                # Loop over primary and secondary ICI for each PI.
                for ici in [pi.ici_pri, pi.ici_sec]:
                    if ici:
                        lpm = ici.lpm
                        tmp = [i for i in self.cpms \
                               if ((i.rack == lpm.rack) \
                                   and (i.crate == lpm.crate))]
                        if tmp:
                            cpm = tmp[0]
                            if self.verbose:
                                print "  Found CPM '{0:s}' associated with LPM '{1:s}'" \
                                    .format(cpm.identifier, lpm.identifier)
                            lpm.set_cpm(cpm)
                            cpm.add_lpm(lpm)
                            cpms_used.append(cpm)

        #----------

        if not self.no_checks:
            # We should have no unused CPMs left.
            cpm_ids = [i.identifier for i in self.cpms]
            used_cpm_ids = set([i.identifier for i in cpms_used])
            # HACK: Exclude dummies from the check.
            tmp_cpm_ids = [i for i in cpm_ids if i.find("dummy") < 0]
            if sorted(used_cpm_ids) != sorted(tmp_cpm_ids):
                # ASSERT ASSERT ASSERT
                assert len(used_cpm_ids) < len(cpm_ids)
                # ASSERT ASSERT ASSERT end
                tmp = [i for i in cpm_ids if not i in used_cpm_ids]
                msg = "CPMs '{0:s}' are not associated with LPMs"
                raise ValueError(msg.format("', '".join(tmp)))

        # End of create_structure().

    # End of class TCDSSetup.


class Partition(object):

    def __init__(self, identifier):
        # NOTE: The agreement with RunControl is that partition names
        # are written in all-uppercase.
        self._identifier = identifier.upper()
        self.pis = {}

    def add_pi(self, pi):
        self.pis[pi.identifier] = pi

    # End of class Partition.


class CPM(HardwareUTCA):

    def __init__(self, identifier, data):
        super(CPM, self).__init__(identifier, data)
        self.lpms = {}

    def add_lpm(self, lpm):
        self.lpms[lpm.identifier] = lpm

    # End of class CPM.


class CPMT1(CPM):

    def __init__(self, identifier, data, tongue):
        super(CPMT1, self).__init__(identifier, data)
        self.tongue = tongue

    @property
    def identifier(self):
        res = "{0:s}-{1:s}".format(super(CPMT1, self).identifier, self.tongue)
        res = res.lower()
        return res

    @property
    def type(self):
        res = "{0:s}-{1:s}".format(super(CPMT1, self).type, self.tongue)
        res = res.lower()
        return res

    @property
    def dns_alias(self):
        # Convention: DNS aliases are all-lowercase.
        res = "{0:s}-{1:s}".format(super(CPMT1, self).dns_alias, self.tongue)
        res = res.lower()
        return res

    # End of class CPMT1.


class LPM(HardwareUTCA):

    def __init__(self, identifier, data):
        super(LPM, self).__init__(identifier, data)
        self.cpm = None
        self.ipms = {}
        self.icis = {}

    def set_cpm(self, cpm):
        self.cpm = cpm

    def add_ipm(self, ipm):
        self.ipms[ipm.identifier] = ipm

    def add_ici(self, ici):
        self.icis[ici.identifier] = ici

    # End of class LPM.


class IPM(HardwareUTCA):

    def __init__(self, identifier, data):
        super(IPM, self).__init__(identifier, data)
        self.lpm = None

    def set_lpm(self, lpm):
        self.lpm = lpm

    @property
    def id_lpm(self):
        # try:
        res = self._data['lpm']
        # except KeyError:
        #     res = None
        return res

    @property
    def ipm_number(self):
        # try:
        res = int(self._data['ipm_number'])
        # except KeyError:
        #     res = None
        return res

    @property
    def rack(self):
        res = self.lpm.rack
        return res

    @property
    def crate(self):
        res = self.lpm.crate
        return res

    @property
    def slot(self):
        res = self.lpm.slot
        return res

    @property
    def board(self):
        return self.lpm

    # End of class IPM.


class ICI(HardwareUTCA):

    def __init__(self, identifier, data):
        super(ICI, self).__init__(identifier, data)
        self.lpm = None
        self.apve = None
        self.pis = {}

    def set_lpm(self, lpm):
        self.lpm = lpm

    def set_apve(self, apve):
        self.apve = apve

    def add_pi(self, pi):
        self.pis[pi.identifier] = pi

    @property
    def id_lpm(self):
        # try:
        res = self._data['lpm']
        # except KeyError:
        #     res = None
        return res

    @property
    def ici_number(self):
        # try:
        res = int(self._data['ici_number'])
        # except KeyError:
        #     res = None
        return res

    @property
    def rack(self):
        res = self.lpm.rack
        return res

    @property
    def crate(self):
        res = self.lpm.crate
        return res

    @property
    def slot(self):
        res = self.lpm.slot
        return res

    @property
    def board(self):
        return self.lpm

    # End of class ICI.


class APVE(HardwareUTCA):

    def __init__(self, identifier, data):
        super(APVE, self).__init__(identifier, data)
        self.ici = None

    def set_ici(self, ici):
        self.ici = ici

    @property
    def id_lpm(self):
        # try:
        res = self._data['lpm']
        # except KeyError:
        #     res = None
        return res

    @property
    def ici_number(self):
        # try:
        res = int(self._data['ici_number'])
        # except KeyError:
        #     res = None
        return res

    @property
    def rack(self):
        res = self.lpm.rack
        return res

    @property
    def crate(self):
        res = self.lpm.crate
        return res

    @property
    def slot(self):
        res = self.lpm.slot
        return res

    @property
    def board(self):
        return self.ici.lpm

    # End of class APVE.


class PI(HardwareUTCA):

    def __init__(self, identifier, data):
        super(PI, self).__init__(identifier, data)
        self.ici_pri = None
        self.ici_sec = None

    @property
    def id_ici_pri(self):
        try:
            res = self._data['ici-pri']
        except KeyError:
            res = None
        return res

    @property
    def id_ici_sec(self):
        try:
            res = self._data['ici-sec']
        except KeyError:
            res = None
        return res

    def set_ici_pri(self, ici):
        self.ici_pri = ici

    def set_ici_sec(self, ici):
        self.ici_sec = ici

    # End of class PI.


class PHASEMON(HardwareUTCA):

    pass

    # End of class PHASEMON.


class BOBR(HardwareVME):

    pass

    # End of class BOBR.


class RF2TTC(HardwareVME):

    pass

    # End of class RF2TTC.


class RFRXD(HardwareVME):

    pass

    # End of class RFRXD.


###############################################################################


def parse_setup(setup_data, no_checks=False, verbose=False):

    res = {}
    location_names = setup_data.keys()
    for location_name in location_names:

        # Step 1: find all the hardware.
        hardware = []
        for (i, j) in setup_data[location_name]['hardware'].iteritems():
            tmp = Hardware(i, j)
            class_name = tmp.type.upper()
            try:
                class_tmp = globals()[class_name]
            except KeyError:
                msg = "Found unknown class '{0:s}' in setup description."
                raise RuntimeError(msg.format(class_name));
            hardware.append(class_tmp(i, j))

        # hardware = [Hardware(i, j) for (i, j) in setup_data[location_name]['hardware'].iteritems()]
        # pis = [PI(i.identifier, i.data) for i in hardware if i.type == 'pi']
        # icis = [ICI(i.identifier, i.data) for i in hardware if i.type == 'ici']
        # apves = [APVE(i.identifier, i.data) for i in hardware if i.type == 'apve']
        # lpms = [LPM(i.identifier, i.data) for i in hardware if i.type == 'lpm']
        # cpms = [CPM(i.identifier, i.data) for i in hardware if i.type == 'cpm']

        pis = [i for i in hardware if i.type == 'pi']
        icis = [i for i in hardware if i.type == 'ici']
        apves = [i for i in hardware if i.type == 'apve']
        lpms = [i for i in hardware if i.type == 'lpm']
        cpms = [i for i in hardware if i.type == 'cpm']

        others = [class_tmp(i.identifier, i.data) for i in hardware \
                  if not i.type in ['cpm', 'lpm', 'ici', 'apve', 'pi']]
        # hardware_targets = {}
        # for tmp in [pis, icis, apves, lpms, cpms, others]:
        #     hardware_targets.update(dict(zip([i.identifier for i in tmp], tmp)))
        hardware_targets = dict([(i.identifier, i) for i in hardware])

        # Step 2: find all the software.
        software = [Software(i, j) for (i, j) in setup_data[location_name]['software'].iteritems()]
        applications = dict(zip([i.identifier for i in software], software))

        # Step 3: turn everything into a TCDSSetup object.
        tcds_setup = TCDSSetup(location_name, hardware_targets, applications, no_checks, verbose)
        res[location_name] = tcds_setup

    #----------

    # End of parse_setup().
    return res


###############################################################################


def read_setup(setup_file_name):
    with open(setup_file_name, 'r') as in_file:
        # Let's assume that this file is not huge, and we can
        # simply read the whole thing at once.
        raw_data = in_file.read()

        # Pre-process the JSON to remove comments etc.
        raw_data = comment_remover(raw_data)

    try:
        # NOTE: The object_pairs_hook argument needs Python
        # 2.7. But it is very nice to check for duplicate keys
        # if we can.
        setup_data = json.loads(raw_data,
                                object_pairs_hook=dict_raise_on_duplicates)
    except TypeError:
        setup_data = json.loads(raw_data)

    return setup_data


###############################################################################


def load_setups(setup_file_name, no_checks=False, verbose=False):

    if verbose:
        msg = "Reading setup from JSON file '{0:s}'"
        print msg.format(setup_file_name)
    try:
        setup_data = read_setup(setup_file_name)
    except Exception, err:
        msg = "Problem reading setup description JSON file '{0:s}': '{1:s}'"
        raise ValueError(msg.format(setup_file_name, str(err)))

    try:
        tcds_setups = parse_setup(setup_data, no_checks, verbose)
    except Exception, err:
        msg = "Problem parsing setup description from JSON file '{0:s}': '{1:s}'"
        raise ValueError(msg.format(setup_file_name, str(err)))

    # End of load_setups().
    return tcds_setups

###############################################################################
