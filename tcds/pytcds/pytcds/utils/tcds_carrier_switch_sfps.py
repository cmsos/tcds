#!/usr/bin/env python

###############################################################################
## Switch on/off all SFPs on all (present) FMCs.
###############################################################################

import sys

from pytcds.utils.tcds_utils_hw_connect import get_carrier_hw
from pytcds.utils.tcds_utils_networking import resolve_network_address
from pytcds.utils.tcds_cmd_line_base import CmdLineBase

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class SFPSwitcher(CmdLineBase):

    STATE_CHOICES = ["on", "off"]

    def __init__(self, description=None, usage=None, epilog=None):
        super(SFPSwitcher, self).__init__(description, usage, epilog)
        self.state = None
        # End of __init__().

    def setup_parser_custom(self):
        super(SFPSwitcher, self).setup_parser_custom()
        parser = self.parser

        # Add the target SFP state argument.
        help_str = "The target SFP state"
        parser.add_argument("sfp_state",
                            type=str,
                            choices=SFPSwitcher.STATE_CHOICES,
                            help=help_str)
        # End of setup_parser_custom().

    def handle_args(self):
        super(SFPSwitcher, self).handle_args()
        # Extract the target SFP state.
        self.state = self.args.sfp_state
        # End of handle_args().

    def main(self):
        self.validate_target(target_type=CmdLineBase.TARGET_TYPE_HW)
        board_info = self.setup.hw_targets[self.target]
        network_address = board_info.dns_alias
        controlhub_address = board_info.controlhub
        carrier = get_carrier_hw(network_address, controlhub_address, verbose=self.verbose)
        if not carrier:
            self.error("Could not connect to {0:s}.".format(self.ip_address))

        if self.verbose:
            print "Switching all SFPs on all FMCs {0:s}.".format(self.state)

        carrier.switch_fmc_sfps(self.state)
        # End of main().

    # End of class SFPSwitcher.

###############################################################################

if __name__ == "__main__":

    desc_str = "Enable/disable all SFPs on both FMCs " \
               "on an FMC carrier with TCDS firmware."

    res = SFPSwitcher(desc_str).run()

    print "Done"
    sys.exit(res)

###############################################################################
