#!/usr/bin/env python

###############################################################################

import os
import socket
import sys

import pdb

###############################################################################

def natcat(hostname, port, content):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((hostname, port))
    s.sendall(content)
    s.sendall("exit\r\n")
    # s.shutdown(socket.SHUT_WR)
    data = []
    while True:
        tmp = s.recv(1024)
        if not len(tmp):
            break
        data.append(tmp)
    s.close()
    res = "".join(data)
    # End of natcat().
    return res

###############################################################################

if __name__ == "__main__":

    args = sys.argv
    if len(args) < 2:
        print >> sys.stderr, \
            "ERROR Expected two arguments: the MCH IP address and a command to execute"
        sys.exit(1)
    elif ("-h" in args) or ("--help" in args):
        print "Expecting two arguments: the MCH IP address and a command to execute"
        sys.exit(0)

    mch_address = args[1]

    file_name = args[2]
    commands = []
    if os.path.isfile(file_name):
        in_file = open(file_name, "r")
        commands = in_file.readlines()
        in_file.close()
    else:
        commands = args[2:]

    for cmd in commands:
        if not cmd.endswith("\r\n"):
            cmd = "%s\r\n" % cmd
        data = natcat(mch_address, 23, cmd)
        print data

    print "Done"

###############################################################################
