#!/usr/bin/env python

###############################################################################
## Helper class to run multiple command-line commands as a sequence.
###############################################################################

import os
import shlex
import subprocess
import sys
import time

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

def add_cwd_to_cmds(cmds):
    this_script = os.path.realpath(sys.argv[0])
    cwd = os.path.dirname(this_script)
    res = []
    for (cmd, doc) in cmds:
        full_cmd = cmd
        if cmd.endswith(".py"):
            full_cmd = os.path.join(cwd, cmd)
        res.append((full_cmd, doc))
    # End of add_cwd_to_cmds().
    return res

###############################################################################

def filter_output(output_raw):

    # BUG BUG BUG
    # Filter out all the noise from uhal about address overlaps.
    tmp_output = output_raw.split(os.linesep)
    tmp_output = [i for i in tmp_output if (i.find("overlap") < 0)]
    # Filter out some of my own verbose logging as well.
    tmp_output = [i for i in tmp_output if (i != "Done")]
    tmp_output = [i for i in tmp_output if (i.find("Connecting to") < 0)]
    # Remove empty lines.
    tmp_output = [i for i in tmp_output if len(i) > 0]
    res = os.linesep.join(tmp_output)
    # BUG BUG BUG end

    # End of filter_output().
    return res

###############################################################################

class CommandRunner(object):

    def __init__(self, cmds_and_docs):
        # The parameter cmds_and_docs is a list of tuples, each tuple
        # containing the command string to run and an optional
        # documentation string to send to the screen (or None).
        self.cmds_and_docs = cmds_and_docs
        # End of __init__().

    def run(self):
        res = 1

        for (cmd, doc) in self.cmds_and_docs:
            if doc is None:
                print "Running '{0:s}'.".format(cmd)
            else:
                if doc:
                    print doc

            proc = subprocess.Popen(cmd,
                                    shell=True,
                                    stdout=subprocess.PIPE,
                                    stderr=subprocess.PIPE,
                                    bufsize=0)
            while True:
                next_line = proc.stdout.readline()
                if not len(next_line) and \
                   not proc.poll() is None:
                    break

                next_line = filter_output(next_line)
                if len(next_line):
                    print next_line

            status = proc.returncode
            tmp = proc.communicate()
            stderr = tmp[1]

            if status != 0:
                msg_base = "Something went wrong executing '{0:s}': '{1:s}'."
                msg = msg_base.format(cmd, stderr.strip())
                print >> sys.stderr, msg
                break

        else:
            res = 0

        # End of run().
        return res

    # End of class CommandRunner.

###############################################################################

if __name__ == "__main__":

    cmds = [
        ("sleep 1", None),
        ("sleep 2", "Sleeping for 2 seconds.")
        ]

    cmds = add_cwd_to_cmds(cmds)

    runner = CommandRunner(cmds)
    runner.run()

    print "Done"

###############################################################################
