#!/usr/bin/env python

###############################################################################
## Developer tool to check the default hardware configurations against
## the address tables.
###############################################################################

import sys

import uhal

from pytcds.utils.tcds_cmd_line_base import CmdLineBase
from pytcds.utils.tcds_constants import BOARD_TYPE_AMC13
from pytcds.utils.tcds_constants import BOARD_TYPE_CPMT1
from pytcds.utils.tcds_utils_uhal import get_hw_tca_base

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

XDAQ_ROOT = "/opt/xdaq"

long_device_names = {
    "cpm" : "amc13_cpmt1",
    }

device_reg_name_prefixes = {
    "cpm" : "cpmt1.ipm"
    }

device_reg_name_ignore_lists = {
    "cpm" : [
        ".*\.resets\.?.*",
        ".*\.gtx_resets\..*"
    ]
    }

###############################################################################

class Checker(CmdLineBase):

    def setup_parser_custom(self):
        pass
        # End of setup_parser_custom().

    def main(self):

        device_name = "cpm"

        device_name_long = long_device_names[device_name]
        device_reg_name_prefix = device_reg_name_prefixes[device_name]
        device_ignore_list = device_reg_name_ignore_lists[device_name]

        #----------

        # Load the device address table.
        print "Loading the device address table..."
        tmp_address_table_file_name = "file://{0:s}/etc/tcds/addresstables/device_address_table_{1:s}.xml"
        address_table_file_name = tmp_address_table_file_name.format(XDAQ_ROOT, device_name_long)
        # NOTE: We don't actually intend to connect to the device. We
        # just need to make sure that uhal does not throw any
        # exceptions.
        address_or_name = "localhost"
        hw = get_hw_tca_base(BOARD_TYPE_AMC13,
                             BOARD_TYPE_CPMT1,
                             address_table_file_name,
                             address_or_name)

        #----------

        # Load the list of device registers based on the address
        # table.
        print "Building the register list from the device address table..."
        node_list = hw.create_reg_info_list(ignore_list=device_ignore_list,
                                            read_values=False)

        # Clean up the node list by removing nodes with children.
        # NOTE: This assumes that top-level nodes are not used in the
        # default hardware configuration, but that the individual
        # child nodes are used instead. (Which would be good
        # practice.)
        print "Cleaning up the register list (removing nested nodes etc.)..."
        tmp_node_list = []
        for node in node_list:
            node_name = node.name
            tmp = [i for i in node_list if i.name.startswith(node_name)]
            if len(tmp) == 1:
                tmp_node_list.append(node)
        node_list = tmp_node_list

        #----------

        # Load the default device hardware configuration.
        print "Loading the default device configuration information..."
        tmp_default_hw_cfg_file_name = "{0:s}/etc/tcds/{1:s}/hw_cfg_default_{1:s}.txt"
        default_hw_cfg_file_name = tmp_default_hw_cfg_file_name.format(XDAQ_ROOT,
                                                                       device_name)
        with open(default_hw_cfg_file_name, 'r') as in_file:
            tmp_default_hw_cfg = in_file.readlines()
        default_hw_cfg = []
        for line in tmp_default_hw_cfg:
            line_stripped = line.strip()
            if line_stripped and \
               not line_stripped.startswith("#"):
                reg_name = line.strip().split()[0].strip()
                default_hw_cfg.append(reg_name)

        #----------

        # Now do some comparing.
        print "Checking the default device configuration contents..."
        for reg_info in sorted(node_list,
                               key=lambda reg_info: reg_info.name):
            if not reg_info.is_hierarchical() \
               and reg_info.is_writable():
                reg_name = reg_info.name
                reg_name = reg_name.replace(device_reg_name_prefix + ".", "")
                if not reg_name in default_hw_cfg:
                    msg_base = "Register {0:s} is not present" \
                               " in the default {1:s} hardware configuration."
                    msg = msg_base.format(reg_name, device_name)
                    print msg
                # else:
                #     print "Found"

        # End of main().

    # End of class Checker.

###############################################################################

if __name__ == "__main__":

    desc_str = "Developer tool to check the default hardware configurations" \
               " against the address tables."

    res = Checker(desc_str).run()

    print "Done"
    sys.exit(res)

###############################################################################
