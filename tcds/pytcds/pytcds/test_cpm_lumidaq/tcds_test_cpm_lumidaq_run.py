#!/usr/bin/env python

###############################################################################
## Master script to configure and enable an CPM driving several iCIs
## and PIs and check the lumiDAQ synchronisation data on the TTC
## fibre.
###############################################################################

import os
import sys

from pytcds.test_cpm_lumidaq.tcds_test_cpm_lumidaq_constants import __file__ as constants_file_name
from pytcds.test_cpm_lumidaq.tcds_test_cpm_lumidaq_constants import APVES
from pytcds.test_cpm_lumidaq.tcds_test_cpm_lumidaq_constants import CPM
from pytcds.test_cpm_lumidaq.tcds_test_cpm_lumidaq_constants import FED_ID_CPM
from pytcds.test_cpm_lumidaq.tcds_test_cpm_lumidaq_constants import FED_ID_GT1
from pytcds.test_cpm_lumidaq.tcds_test_cpm_lumidaq_constants import FED_ID_GT2
from pytcds.test_cpm_lumidaq.tcds_test_cpm_lumidaq_constants import ICIS
from pytcds.test_cpm_lumidaq.tcds_test_cpm_lumidaq_constants import PIS
from pytcds.utils.tcds_cmd_line_base import CmdLineBase
from pytcds.utils.tcds_command_runner import add_cwd_to_cmds
from pytcds.utils.tcds_command_runner import CommandRunner

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

def append_piece_to_cmds(cmds, piece):
    res = []
    for (cmd, doc) in cmds:
        full_cmd = cmd
        if cmd.endswith(".py"):
            full_cmd = "%s %s" % (cmd, piece)
        res.append((full_cmd, doc))
    # End of append_piece_to_cmds().
    return res

###############################################################################

class TCDSConfigForTestCPMLumiDAQ(CmdLineBase):

    def setup_parser_custom(self):
        # NOTE: Skip the default, since we don't need an argument.
        pass
        # End of setup_parser_custom().

    def main(self):
        sep_line = "-" * 70

        test_dir_name = os.path.dirname(constants_file_name)
        #----------

        pm_daq_on_off = '0'
        gt1_enabled_disabled = '0'
        gt2_enabled_disabled = '0'

        fed_enable_mask = "{0:d}&{1:s}%{2:d}&{3:s}%{4:d}&{5:s}%".format(FED_ID_CPM,
                                                                        pm_daq_on_off,
                                                                        FED_ID_GT1,
                                                                        gt1_enabled_disabled,
                                                                        FED_ID_GT2,
                                                                        gt2_enabled_disabled)

        #----------

        fsm_ctrl_cmd = "../utils/tcds_fsm_control.py --setup-file {0:s} --location {1:s}" \
            .format(self.setup_file_name, self.location_name)
        fsm_poll_cmd = "../utils/tcds_fsm_poller.py --setup-file {0:s} --location {1:s}" \
            .format(self.setup_file_name, self.location_name)
        pi_force_ready_cmd = "../pi/tcds_pi_force_ready.py --setup-file {0:s} --location {1:s}" \
            .format(self.setup_file_name, self.location_name)
        # retri_disable_cmd = "../cpm/tcds_cpm_disable_retri.py"
        # backpressure_disable_cmd = "../cpm/tcds_cpm_disable_backpressure.py"
        read_ttcspy_log_cmd = "../pi/tcds_pi_read_ttcspy_log.py --setup-file {0:s} --location {1:s}" \
            .format(self.setup_file_name, self.location_name)
        analyse_cmd = "./tcds_analyse_ttcspy_lumidaq.py"

        cmds = []

        #----------

        # Halt everything.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Halting all controllers'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = [CPM]
        targets.extend(ICIS)
        targets.extend(APVES)
        targets.extend(PIS)
        for target in targets:
            cmds.append(("{0:s} {1:s} Halt".format(fsm_ctrl_cmd, target), ""))

        # Wait till everything is in the Halted state.
        targets = [CPM]
        targets.extend(ICIS)
        targets.extend(APVES)
        targets.extend(PIS)
        for target in targets:
            cmds.append(("{0:s} {1:s} --wait-until Halted".format(fsm_poll_cmd, target), ""))

        #----------

        # Configure CPM.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Configuring CPM'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        cmds.append(("{0:s} --fed-enable-mask='{1:s}' --no-beam-active {2:s} Configure {3:s}/hw_cfg_cpm.txt".format(fsm_ctrl_cmd, fed_enable_mask, CPM, test_dir_name), ""))

        # Wait till the CPM is in the Configured state.
        cmds.append(("{0:s} {1:s} --wait-until Configured".format(fsm_poll_cmd, CPM), ""))

        # Configure iCIs.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Configuring iCIs'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list(ICIS)
        for target in targets:
            cmds.append(("{0:s} {1:s} Configure hw_cfg_ici.txt".format(fsm_ctrl_cmd, target), ""))

        # Wait till everything is in the Configured state.
        for target in targets:
            cmds.append(("{0:s} {1:s} --wait-until Configured".format(fsm_poll_cmd, target), ""))

        # Configure APVEs.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Configuring APVEs'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list(APVES)
        for target in targets:
            cmds.append(("{0:s} {1:s} Configure hw_cfg_apve.txt".format(fsm_ctrl_cmd, target), ""))

        # Wait till everything is in the Configured state.
        for target in targets:
            cmds.append(("{0:s} {1:s} --wait-until Configured".format(fsm_poll_cmd, target), ""))

        # Configure PIs.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Configuring PIs'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list(PIS)
        for target in targets:
            cmds.append(("{0:s} --fed-enable-mask='{1:s}' {2:s} Configure hw_cfg_pi.txt".format(fsm_ctrl_cmd, fed_enable_mask, target), ""))

        # Wait till everything is in the Configured state.
        for target in targets:
            cmds.append(("{0:s} {1:s} --wait-until Configured".format(fsm_poll_cmd, target), ""))

        #----------

        # # Disable the ReTri on the CPM.
        # cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
        #              ("python -c \"print 'Disabling the ReTri on the CPM'\"", ""),
        #              ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        # cmds.append(("{0:s} cpmt1".format(retri_disable_cmd), ""))

        # # Disable the DAQ backpressure on the CPM.
        # cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
        #              ("python -c \"print 'Disabling the DAQ backpressure on the CPM'\"", ""),
        #              ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        # cmds.append(("{0:s} cpmt1".format(backpressure_disable_cmd), ""))

        #----------

        # # Force PIs to be TTS READY.
        # cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
        #              ("python -c \"print 'Forcing PI TTS states to READY'\"", ""),
        #              ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        # targets = list(PIS)
        # for target in targets:
        #     cmds.append(("{0:s} {1:s}".format(pi_force_ready_cmd, target), ""))

        #----------

        # Enable PIs.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Enabling PIs'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list(PIS)
        for target in targets:
            cmds.append(("{0:s} {1:s} Enable".format(fsm_ctrl_cmd, target), ""))

        # Wait till everything is in the Enabled state.
        for target in targets:
            cmds.append(("{0:s} {1:s} --wait-until Enabled".format(fsm_poll_cmd, target), ""))

        # Enable APVEs.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Enabling APVEs'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list(APVES)
        for target in targets:
            cmds.append(("{0:s} {1:s} Enable".format(fsm_ctrl_cmd, target), ""))

        # Wait till everything is in the Enabled state.
        for target in targets:
            cmds.append(("{0:s} {1:s} --wait-until Enabled".format(fsm_poll_cmd, target), ""))

        # Enable iCIs.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Enabling iCIs'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list(ICIS)
        for target in targets:
            cmds.append(("{0:s} {1:s} Enable".format(fsm_ctrl_cmd, target), ""))

        # Wait till everything is in the Enabled state.
        for target in targets:
            cmds.append(("{0:s} {1:s} --wait-until Enabled".format(fsm_poll_cmd, target), ""))

        # Enable CPM (as last!).
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Enabling CPM'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        cmds.append(("{0:s} {1:s} Enable".format(fsm_ctrl_cmd, CPM), ""))

        #----------

        # Wait a little.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Practicing patience...'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        # cmds.append(("python -c \"import time; time.sleep(300)\"", ""))
        cmds.append(("python -c \"import time; time.sleep(10)\"", ""))

        #----------

        # Check the TTCSpy log in all PIs.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Checking the TTCSpy log in all PIs'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list(PIS)
        for target in targets:
            file_name = "{0:s}_ttcspy_log.txt".format(target)
            cmds.append(("{0:s} {1:s} &> {2:s}".format(read_ttcspy_log_cmd, target, file_name), ""))

        #----------

        # Analyse the results.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Analysing results'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = PIS
        for target in targets:
            cmds.extend([("python -c \"print '  {0:s}'\"".format(target), "")])
            file_name = "{0:s}_ttcspy_log.txt".format(target)
            cmds.append(("{0:s} {1:s}".format(analyse_cmd, file_name), ""))

        #----------

        cmds = add_cwd_to_cmds(cmds)
        if self.verbose:
            cmds = append_piece_to_cmds(cmds, "--verbose")

        runner = CommandRunner(cmds)
        runner.run()

        # End of main().

    # End of class TCDSConfigForTestCPMLumiDAQ.

###############################################################################

if __name__ == "__main__":

    description = "Master script to look at the lumiDAQ synchronisation " \
                  "markers on the TTC fibre."
    res = TCDSConfigForTestCPMLumiDAQ(description).run()
    print "Done"
    sys.exit(res)

###############################################################################
