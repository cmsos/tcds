#!/usr/bin/env python

###############################################################################
## A little script to check the TTCSpy log for BRILDAQ markers.
###############################################################################

import ast
import ConfigParser
import os
import re
import urllib2
import json
import sys

from pytcds.utils.tcds_cmd_line_base import CmdLineBase

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class Analyzer(CmdLineBase):

    def setup_parser_custom(self):
        # # Start with the basic parser configuration.
        # super(Analyzer, self).setup_parser_custom()

        # Add the data file choice.
        help_str = "The name of the captured data file to analyze."
        self.parser.add_argument("data_file",
                                 type=str,
                                 action="store",
                                 help=help_str)

        # End of setup_parser_custom().

    def handle_args(self):
        # NOTE: Do not call the parent handle_args() since we are not
        # requiring an IP address (unlike pretty much all other TCDS
        # scripts).

        self.ttcspy_file_name = self.args.data_file
        # End of handle_args().

    def load_setup(self):
        pass

    def main(self):

        ttcspy_data_raw = None

        try:
            in_file = open(self.ttcspy_file_name, "r")
            ttcspy_data_raw = in_file.read()
            in_file.close()
        except IOError, err:
            msg = "Could not read file '{0:s}': '{1:s}'."
            self.error(msg.format(self.ttcspy_file_name, err))

        ttcspy_data = []
        regexp = re.compile("^Entry +([0-9]+):" \
                            " Addressed command" \
                            " \(with address (0x[0-9a-f]+)/0x([0-9a-f]+) (.*?)\)" \
                            " with data (0x[0-9a-f]+).*$")
        for line in ttcspy_data_raw.split(os.linesep):
            match = regexp.search(line)
            if match:
                # 0: Entry number.
                # 1: Address.
                # 2: Sub-address.
                # 3: Addressing mode.
                # 4: Data payload.
                entry_number = int(match.group(1))
                data_payload = int(match.group(5), 16)
                address = int(match.group(2), 16)
                sub_address = int(match.group(3), 16)
                addressing_mode = match.group(4)
                ttcspy_data.append((entry_number,
                                    data_payload,
                                    address,
                                    sub_address,
                                    addressing_mode))

        if not len(ttcspy_data):
            self.error("No BRILDAQ data found in the TTCSpy capture data")

        #----------

        # Group the received markers into groups with the same data
        # type.
        # NOTE: The data type is encoded in the four MSB of the
        # sub-address.
        markers = []
        tmp = []
        data_type_prev = None
        for entry in ttcspy_data:
            data_type = (entry[3] & 0xf0) >> 4
            if data_type == data_type_prev:
                tmp.append(entry)
            else:
                if len(tmp):
                    markers.append(tmp)
                tmp = [entry]
            data_type_prev = data_type
        if len(tmp):
            markers.append(tmp)

        #----------

        data_type_strings = {
            0x0 : "reserved 0",
            0x1 : "nibble number",
            0x2 : "section number",
            0x3 : "run number",
            0x4 : "fill number",
            0x5 : "reserved 5",
            0x6 : "reserved 6",
            0x7 : "reserved 7",
            0x8 : "reserved 8",
            0x9 : "reserved 9",
            0xa : "reserved 0xa",
            0xb : "reserved 0xb",
            0xc : "reserved 0xc",
            0xd : "reserved 0xd",
            0xe : "reserved 0xe",
            0xf : "reserved 0xf"
            }

        # Process the markers.
        for marker in markers:
            data_types = [((i[3] & 0xf0) >> 4) for i in marker]
            assert (len(set(data_types)) == 1)
            data_type = data_types[0]
            incomplete = (len(marker) != 4)
            tmp = "complete"
            if incomplete:
                tmp = "incomplete"
            data_type_str = data_type_strings[data_type]
            data_indices = [(i[3] & 0xf) for i in marker]
            assert data_indices == [0x0, 0x1, 0x2, 0x3]
            data_words = [i[1] for i in marker]
            data = data_words[0] + \
                   (data_words[1] << 8) + \
                   (data_words[2] << 16) + \
                   (data_words[3] << 24)
            # NOTE: Little assumption here: the nibble number is
            # always the first thing that arrives in a new data
            # series. (Verified against the firmware, but the firmware
            # can change.)
            if data_type_str == 'nibble number':
                print "--------------------"
            print "Found {0:s} {1:s}: {2:d}".format(tmp, data_type_str, data)

        #----------

        # End of main().

    # End of class Analyzer.

###############################################################################

if __name__ == "__main__":

    description = "A little script to check the TTCSpy log for BRILDAQ markers."
    res = Analyzer(description).run()
    print "Done"
    sys.exit(res)

###############################################################################
