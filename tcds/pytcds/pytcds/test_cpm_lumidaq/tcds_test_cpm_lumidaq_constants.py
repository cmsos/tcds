###############################################################################

# The addresses of the boards/applications to connect to.
CPM = "cpm"
ICIS = ["ici-11"]
APVES = ["apve-11"]
PIS = ["pi-11"]

# The FED id of the CPM, so we can disable the readout.
FED_ID_CPM = 1024

# The FED id(s) of the GT, so we know if it is in the run or not (in
# which case we have to enable the corresponding trigger input).
FED_ID_GT1 = 1404
FED_ID_GT2 = 1405

###############################################################################
