#!/usr/bin/env python

from setuptools import setup
import os

# WORKAROUND WORKAROUND WORKAROUND
# The following line makes setuptools work on AFS. In AFS hard-links
# between directories are not allowed, making the build fail with a
# complaint like 'error: Invalid cross-device link.' Removing os.link
# makes setuptools make copies instead of hard-links.
del os.link
# WORKAROUND WORKAROUND WORKAROUND end

VERSION_STRING = os.environ['PACKAGE_VER_MAJOR'] + \
                 '.' + \
                 os.environ['PACKAGE_VER_MINOR'] + \
                 '.' + \
                 os.environ['PACKAGE_VER_PATCH']

NAME_STRING = 'cmsos-' + \
              os.environ['PROJECT_NAME'] + \
              '-' + \
              os.environ['PACKAGE_NAME']

setup(name = NAME_STRING,
      version = VERSION_STRING,
      description = "Python utilities for the CMS TCDS software",
      author = "Jeroen Hegeman",
      author_email = "jeroen.hegeman@cern.ch",
      url = "https://twiki.cern.ch/twiki/bin/view/CMS/TcdsNotes",
      packages = [
          'pytcds',
          'pytcds.apve',
          'pytcds.bobr',
          'pytcds.cpm',
          'pytcds.extern',
          'pytcds.lpm',
          'pytcds.pi',
          'pytcds.rf2ttc',
          'pytcds.rfrxd',
          'pytcds.rftxd',
          'pytcds.stress_test_tools',
          'pytcds.test_ferol40',
          'pytcds.utils'
      ],
      scripts = [
          'pytcds/cpm/tcds_cpm_reset_tts_counters.py',
          'pytcds/stress_test_tools/tcds_configure_pm_for_stress_test.py',
          'pytcds/test_ferol40/tcds_test_ferol40_run.py',
          'pytcds/utils/tcds_board_info.py',
          'pytcds/utils/tcds_hw_hacker.py'
      ],
      package_data = {
          'pytcds.stress_test_tools': ['*.txt'],
          'pytcds.test_ferol40': ['hw_cfg_*.txt']
      }
)
