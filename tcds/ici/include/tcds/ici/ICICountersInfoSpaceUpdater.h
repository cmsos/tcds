#ifndef _tcds_ici_ICICountersInfoSpaceUpdater_h_
#define _tcds_ici_ICICountersInfoSpaceUpdater_h_

#include "tcds/utils/HwInfoSpaceUpdaterBase.h"

namespace tcds {
  namespace utils {
    class InfoSpaceHandler;
    class InfoSpaceItem;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace ici {

    class TCADeviceICI;

    class ICICountersInfoSpaceUpdater : public tcds::utils::HwInfoSpaceUpdaterBase
    {

    public:
      ICICountersInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                  TCADeviceICI const& hw);
      virtual ~ICICountersInfoSpaceUpdater();

      virtual bool updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                       tcds::utils::InfoSpaceHandler* const infoSpaceHandler);

    protected:
      tcds::ici::TCADeviceICI const& getHw();

    };

  } // namespace ici
} // namespace tcds

#endif // _tcds_ici_ICICountersInfoSpaceUpdater_h_
