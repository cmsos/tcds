#ifndef _tcds_ici_ICICountersInfoSpaceHandler_h_
#define _tcds_ici_ICICountersInfoSpaceHandler_h_

#include <string>

#include "tcds/utils/InfoSpaceHandler.h"

namespace xdaq {
  class Application;
}

namespace tcds {
  namespace utils {
    class InfoSpaceUpdater;
    class Monitor;
    class WebServer;
  }
}

namespace tcds {
  namespace ici {

    class ICICountersInfoSpaceHandler : public tcds::utils::InfoSpaceHandler
    {

    public:
      ICICountersInfoSpaceHandler(xdaq::Application& xdaqApp,
                                  tcds::utils::InfoSpaceUpdater* updater);

    protected:
      virtual void registerItemSetsWithMonitor(tcds::utils::Monitor& monitor);
      virtual void registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                 tcds::utils::Monitor& monitor,
                                                 std::string const& forceTabName="");

    };

  } // namespace ici
} // namespace tcds

#endif // _tcds_ici_ICICountersInfoSpaceHandler_h_
