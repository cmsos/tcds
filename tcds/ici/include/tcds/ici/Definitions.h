#ifndef _tcds_ici_Definitions_h_
#define _tcds_ici_Definitions_h_

#include <stdint.h>

namespace tcds {
  namespace definitions {

    // B-channel timing modes: delayed or BX-synchronous.
    enum BCHANNEL_TIMING_MODE {
      BCHANNEL_TIMING_DELAYED=0,
      BCHANNEL_TIMING_BX_SYNC=1
    };

    // The number of cyclic generators in the iCI.
    unsigned int const kICICyclicGenNumMin = 0;
    unsigned int const kICICyclicGenNumMax = 7;
    unsigned int const kNumCyclicGensPerICI = kICICyclicGenNumMax - kICICyclicGenNumMin + 1;

    // The number of 'classic' B-channels: as many as there are
    // 'classic' B-gos: 16 (to first order).
    unsigned int const kBchannelNumMin = 0;
    unsigned int const kBchannelNumMax = 15;

    // The number of 'TCDS-style' multi-B-channels: 3, each spanning
    // 16 of the new B-gos.
    unsigned int const kMultiBchannelNumMin = 0;
    unsigned int const kMultiBchannelNumMax = 2;
    unsigned int const kBchannelInMultiBchannelNumMin = 0;
    unsigned int const kBchannelInMultiBchannelNumMax = 15;

  } // namespace definitions
} // namespace tcds

#endif // _tcds_ici_Definitions_h_
