#ifndef _tcds_ici_SFPInfoSpaceHandler_h_
#define _tcds_ici_SFPInfoSpaceHandler_h_

#include <string>

#include "tcds/hwutilstca/SFPInfoSpaceHandlerBase.h"
#include "tcds/hwlayertca/Definitions.h"

namespace tcds {
  namespace utils {
    class Monitor;
    class WebServer;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace hwutilstca {
    class SFPInfoSpaceUpdater;
  }
}

namespace tcds {
  namespace ici {

    class SFPInfoSpaceHandler : public tcds::hwutilstca::SFPInfoSpaceHandlerBase
    {

    public:
      SFPInfoSpaceHandler(tcds::utils::XDAQAppBase& xdaqApp,
                          tcds::hwutilstca::SFPInfoSpaceUpdater* updater);
      virtual ~SFPInfoSpaceHandler();

      tcds::utils::XDAQAppBase& getOwnerApplication() const;

    protected:
      virtual void registerItemSetsWithMonitor(tcds::utils::Monitor& monitor);
      virtual void registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                 tcds::utils::Monitor& monitor,
                                                 std::string const& forceTabName="");

    protected:
      tcds::definitions::SFP_LPM sfpId_;

    };

  } // namespace ici
} // namespace tcds

#endif // _tcds_ici_SFPInfoSpaceHandler_h_
