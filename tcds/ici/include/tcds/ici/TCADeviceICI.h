#ifndef _tcds_ici_TCADeviceICI_h_
#define _tcds_ici_TCADeviceICI_h_

#include <stdint.h>
#include <string>
#include <vector>

#include "tcds/hwlayer/DeviceBase.h"
#include "tcds/hwlayer/RegisterInfo.h"
#include "tcds/hwlayertca/TCADeviceBase.h"
#include "tcds/utils/Definitions.h"

namespace tcds {
  namespace ici {

    /**
     * Implementation of the internal TTCci (ICI) functionality. The
     * ICI is implemented as a firmware block inside the LPM (which is
     * based on an FC7 carrier board).
     */
    class TCADeviceICI : public tcds::hwlayertca::TCADeviceBase
    {

    public:

      /**
       * @note
       * The TCADeviceICI need an iCI number. The TCADeviceICI
       * connects to the physical LPM hardware, but only acts upon the
       * iCI (firmware) instance with the given number. The caveat is
       * that at the time the TCADeviceICI object is instantiated in
       * the ICIController, the XDAQ configuration has not yet been
       * loaded, so the iCI number is not yet known. Hence the (ugly)
       * setICINumber() method.
       */

      TCADeviceICI();
      virtual ~TCADeviceICI();

      unsigned short iciNumber() const;
      void setICINumber(unsigned short const iciNumber);

      void enableCPML1AFastTrack() const;
      void enableCPMBRILDAQData() const;
      void enableTTSLinkAutoAlignMode() const;
      void configureTriggerAlignment() const;
      void configureBChannelAlignment() const;

      void stop() const;

      void enableTrigger() const;
      void disableTrigger() const;

      void enableCyclicGenerator(unsigned int const genNumber) const;
      void disableCyclicGenerator(unsigned int const genNumber) const;

      tcds::definitions::BCHANNEL_MODE getBchannelMode(tcds::definitions::BGO_NUM const bchannelNumber) const;

      void sendL1A() const;
      void enableBchannel(tcds::definitions::BGO_NUM const bgoNumber,
                          tcds::definitions::BCHANNEL_MODE const bchannelMode) const;
      void disableBchannel(tcds::definitions::BGO_NUM const bgoNumber) const;
      void sendBgo(tcds::definitions::BGO_NUM const bgoNumber) const;
      void sendBCommand(tcds::definitions::BCOMMAND_TYPE const bcommandType,
                        tcds::definitions::bcommand_data_t const data,
                        tcds::definitions::bcommand_address_t const address,
                        tcds::definitions::bcommand_address_t const subAddress=0x0,
                        tcds::definitions::BCOMMAND_ADDRESS_TYPE const addressType=tcds::definitions::BCOMMAND_ADDRESS_TYPE_INTERNAL) const;

      // Initialize a single cyclic generator.
      void initCyclicGenerator(unsigned int const genNumber) const;
      // Initialize all cyclic generators and all B-channels at once.
      // NOTE: For almost all use cases this is the desired behaviour.
      void initCyclicGenerators() const;
      void initCyclicGeneratorsAndBchannels() const;

      void resetCounters() const;
      void resetStatus() const;

      std::vector<uint32_t> readBchannelRAM(tcds::definitions::BGO_NUM const bchannelNumber) const;

    protected:
      virtual std::string regNamePrefixImpl() const;

      virtual bool bootstrapDoneImpl() const;
      virtual void runBootstrapImpl() const;

      virtual tcds::hwlayer::DeviceBase::RegContentsVec readHardwareConfigurationImpl(tcds::hwlayer::RegisterInfo::RegInfoVec const& regInfos) const;

    private:
      unsigned short iciNumber_;

    };

  } // namespace ici
} // namespace tcds

#endif // _tcds_ici_TCADeviceICI_h_
