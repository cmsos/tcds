#ifndef _tcds_ici_WebTableBchannelRAM_h
#define _tcds_ici_WebTableBchannelRAM_h

#include <cstddef>
#include <string>

#include "tcds/utils/WebObject.h"

namespace tcds {
  namespace utils {
    class Monitor;
  }
}

namespace tcds {
  namespace ici {

    class WebTableBchannelRAM : public tcds::utils::WebObject
    {

    public:
      WebTableBchannelRAM(std::string const& name,
                          std::string const& description,
                          tcds::utils::Monitor const& monitor,
                          std::string const& itemSetName,
                          std::string const& tabName,
                          size_t const colSpan);

      std::string getHTMLString() const;

    };

  } // namespace ici
} // namespace tcds

#endif // _tcds_ici_WebTableBchannelRAM_h
