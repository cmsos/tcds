#ifndef _tcds_ici_ConfigurationInfoSpaceHandler_h_
#define _tcds_ici_ConfigurationInfoSpaceHandler_h_

#include "tcds/hwutilstca/ConfigurationInfoSpaceHandlerTCA.h"

namespace xdaq {
  class Application;
}

namespace tcds {
  namespace utils {
    class Monitor;
  }
}

namespace tcds {
  namespace ici {

    class ConfigurationInfoSpaceHandler :
      public tcds::hwutilstca::ConfigurationInfoSpaceHandlerTCA
    {

    public:
      ConfigurationInfoSpaceHandler(xdaq::Application& xdaqApp);

    protected:
      virtual void registerItemSetsWithMonitor(tcds::utils::Monitor& monitor);

    };

  } // namespace ici
} // namespace tcds

#endif // _tcds_ici_ConfigurationInfoSpaceHandler_h_
