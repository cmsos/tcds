#ifndef _tcds_ici_ICITTSInfoSpaceHandler_h_
#define _tcds_ici_ICITTSInfoSpaceHandler_h_

#include <string>

#include "tcds/utils/InfoSpaceHandler.h"

namespace xdaq {
  class Application;
}

namespace tcds {
  namespace utils {
    class InfoSpaceUpdater;
    class Monitor;
    class WebServer;
  }
}

namespace tcds {
  namespace ici {

    class ICITTSInfoSpaceHandler : public tcds::utils::InfoSpaceHandler
    {

    public:
      ICITTSInfoSpaceHandler(xdaq::Application& xdaqApp,
                            tcds::utils::InfoSpaceUpdater* updater);
      virtual ~ICITTSInfoSpaceHandler();

    protected:
      virtual void registerItemSetsWithMonitor(tcds::utils::Monitor& monitor);
      virtual void registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                 tcds::utils::Monitor& monitor,
                                                 std::string const& forceTabName="");

    };

  } // namespace ici
} // namespace tcds

#endif // _tcds_ici_ICITTSInfoSpaceHandler_h_
