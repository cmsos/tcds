#ifndef _tcds_ici_Utils_h_
#define _tcds_ici_Utils_h_

#include <string>

#include "tcds/ici/Definitions.h"
#include "tcds/hwlayertca/Definitions.h"

namespace tcds {
  namespace ici {

    // Mapping of some B-channel-related types to strings.
    /* std::string BchannelEmissionModeToString(tcds::definitions::BCHANNEL_EMISSION_MODE const mode); */
    std::string BchannelTimingModeToString(tcds::definitions::BCHANNEL_TIMING_MODE const mode);

    void verifyCyclicGeneratorNumber(unsigned int const genNumber);

    tcds::definitions::SFP_LPM iciSFPID(unsigned int const iciNumber);

  } // namespace ici
} // namespace tcds

#endif // _tcds_ici_Utils_h_
