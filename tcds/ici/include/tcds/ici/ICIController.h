#ifndef _tcds_ici_ICIController_h_
#define _tcds_ici_ICIController_h_

#include <memory>

#include "toolbox/Event.h"
#include "xdaq/Application.h"

#include "tcds/ici/TCADeviceICI.h"
#include "tcds/utils/RegCheckResult.h"
#include "tcds/utils/SOAPCmdBase.h"
#include "tcds/utils/SOAPCmdDisableBChannel.h"
#include "tcds/utils/SOAPCmdDisableCyclicGenerator.h"
#include "tcds/utils/SOAPCmdDumpHardwareState.h"
#include "tcds/utils/SOAPCmdEnableBChannel.h"
#include "tcds/utils/SOAPCmdEnableCyclicGenerator.h"
#include "tcds/utils/SOAPCmdInitCyclicGenerator.h"
#include "tcds/utils/SOAPCmdInitCyclicGenerators.h"
#include "tcds/utils/SOAPCmdReadHardwareConfiguration.h"
#include "tcds/utils/SOAPCmdSendBCommand.h"
#include "tcds/utils/SOAPCmdSendBgo.h"
#include "tcds/utils/SOAPCmdSendL1A.h"
#include "tcds/utils/XDAQAppWithFSMBasic.h"

namespace xdaq {
  class ApplicationStub;
}

namespace tcds {
  namespace hwlayer {
    class RegisterInfo;
  }
}

namespace tcds {
  namespace hwutilstca {
    class CyclicGensInfoSpaceHandler;
    class CyclicGensInfoSpaceUpdater;
    class HwIDInfoSpaceHandlerTCA;
    class HwIDInfoSpaceUpdaterTCA;
    class SFPInfoSpaceUpdater;
  }
}

namespace tcds {
  namespace ici {

    class HwStatusInfoSpaceHandler;
    class HwStatusInfoSpaceUpdater;
    class ICIBchannelInfoSpaceHandler;
    class ICIBchannelInfoSpaceUpdater;
    class ICIBdataInfoSpaceHandler;
    class ICIBdataInfoSpaceUpdater;
    class ICICountersInfoSpaceHandler;
    class ICICountersInfoSpaceUpdater;
    class ICIInfoSpaceHandler;
    class ICIInfoSpaceUpdater;
    class ICIMultiBchannelInfoSpaceHandler;
    class ICITTSInfoSpaceHandler;
    class SFPInfoSpaceHandler;

    class ICIController : public tcds::utils::XDAQAppWithFSMBasic
    {

    public:
      XDAQ_INSTANTIATOR();

      ICIController(xdaq::ApplicationStub* stub);
      virtual ~ICIController();

      unsigned short iciNumber() const;

    protected:
      virtual void setupInfoSpaces();

      /**
       * Access the hardware pointer as TCADeviceICI&.
       */
      virtual TCADeviceICI& getHw() const;

      virtual void hwConnectImpl();
      virtual void hwReleaseImpl();

      virtual void configureActionImpl(toolbox::Event::Reference event);
      virtual void enableActionImpl(toolbox::Event::Reference event);
      virtual void stopActionImpl(toolbox::Event::Reference event);
      virtual void zeroActionImpl(toolbox::Event::Reference event);

      virtual void hwCfgInitializeImpl();
      virtual void hwCfgFinalizeImpl();

      virtual tcds::utils::RegCheckResult isRegisterAllowed(tcds::hwlayer::RegisterInfo const& regInfo) const;

    private:
      // Various InfoSpaces and their InfoSpaceUpdaters.
      std::unique_ptr<tcds::hwutilstca::CyclicGensInfoSpaceUpdater> cyclicGensInfoSpaceUpdaterP_;
      std::unique_ptr<tcds::hwutilstca::CyclicGensInfoSpaceHandler> cyclicGensInfoSpaceP_;
      std::unique_ptr<tcds::hwutilstca::HwIDInfoSpaceUpdaterTCA> hwIDInfoSpaceUpdaterP_;
      std::unique_ptr<tcds::hwutilstca::HwIDInfoSpaceHandlerTCA> hwIDInfoSpaceP_;
      std::unique_ptr<tcds::ici::HwStatusInfoSpaceUpdater> hwStatusInfoSpaceUpdaterP_;
      std::unique_ptr<tcds::ici::HwStatusInfoSpaceHandler> hwStatusInfoSpaceP_;
      std::unique_ptr<ICIInfoSpaceUpdater> iciInfoSpaceUpdaterP_;
      std::unique_ptr<ICIInfoSpaceHandler> iciInfoSpaceP_;
      std::unique_ptr<ICIBchannelInfoSpaceUpdater> iciBchannelInfoSpaceUpdaterP_;
      std::unique_ptr<ICIBchannelInfoSpaceHandler> iciBchannelInfoSpaceP_;
      std::unique_ptr<ICIBdataInfoSpaceUpdater> iciBdataInfoSpaceUpdaterP_;
      std::unique_ptr<ICIBdataInfoSpaceHandler> iciBdataInfoSpaceP_;
      std::unique_ptr<ICICountersInfoSpaceUpdater> iciCountersInfoSpaceUpdaterP_;
      std::unique_ptr<ICICountersInfoSpaceHandler> iciCountersInfoSpaceP_;
      std::unique_ptr<ICIBchannelInfoSpaceUpdater> iciMultiBchannelInfoSpaceUpdaterP_;
      std::unique_ptr<ICIMultiBchannelInfoSpaceHandler> iciMultiBchannelInfoSpaceP_;
      std::unique_ptr<ICITTSInfoSpaceHandler> iciTTSInfoSpaceP_;
      std::unique_ptr<tcds::ici::SFPInfoSpaceHandler> sfpInfoSpaceP_;
      std::unique_ptr<tcds::hwutilstca::SFPInfoSpaceUpdater> sfpInfoSpaceUpdaterP_;

      // The SOAP commands.
      template<typename> friend class tcds::utils::SOAPCmdBase;
      template<typename> friend class tcds::utils::SOAPCmdDisableBChannel;
      template<typename> friend class tcds::utils::SOAPCmdDisableCyclicGenerator;
      template<typename> friend class tcds::utils::SOAPCmdDumpHardwareState;
      template<typename> friend class tcds::utils::SOAPCmdEnableBChannel;
      template<typename> friend class tcds::utils::SOAPCmdEnableCyclicGenerator;
      template<typename> friend class tcds::utils::SOAPCmdInitCyclicGenerator;
      template<typename> friend class tcds::utils::SOAPCmdInitCyclicGenerators;
      template<typename> friend class tcds::utils::SOAPCmdReadHardwareConfiguration;
      template<typename> friend class tcds::utils::SOAPCmdSendBCommand;
      template<typename> friend class tcds::utils::SOAPCmdSendBgo;
      template<typename> friend class tcds::utils::SOAPCmdSendL1A;
      tcds::utils::SOAPCmdDisableBChannel<ICIController> soapCmdDisableBChannel_;
      tcds::utils::SOAPCmdDisableCyclicGenerator<ICIController> soapCmdDisableCyclicGenerator_;
      tcds::utils::SOAPCmdDumpHardwareState<ICIController> soapCmdDumpHardwareState_;
      tcds::utils::SOAPCmdEnableBChannel<ICIController> soapCmdEnableBChannel_;
      tcds::utils::SOAPCmdEnableCyclicGenerator<ICIController> soapCmdEnableCyclicGenerator_;
      tcds::utils::SOAPCmdInitCyclicGenerator<ICIController> soapCmdInitCyclicGenerator_;
      tcds::utils::SOAPCmdInitCyclicGenerators<ICIController> soapCmdInitCyclicGenerators_;
      tcds::utils::SOAPCmdReadHardwareConfiguration<ICIController> soapCmdReadHardwareConfiguration_;
      tcds::utils::SOAPCmdSendBCommand<ICIController> soapCmdSendBCommand_;
      tcds::utils::SOAPCmdSendBgo<ICIController> soapCmdSendBgo_;
      tcds::utils::SOAPCmdSendL1A<ICIController> soapCmdSendL1A_;

      void makeSafe() const;

    };

  } // namespace ici
} // namespace tcds

#endif // _tcds_ici_ICIController_h_
