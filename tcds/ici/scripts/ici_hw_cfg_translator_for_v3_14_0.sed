#!/bin/sed -f

# Usage:
#   ./ici_hw_cfg_translator_for_v3_14_0.sed < ici_hw_cfg_ori.txt > ici_hw_cfg_new.txt

# First replace tabs with spaces, in case someone tried those...
s/\t/        /g

# We now have two iPMs in the LPM, so some specialisation is required.
# NOTE: The below looks clumsy, but it tries to keep the alignment
# intact where possible.

s/exclusive_lpm_trigger_enable      /exclusive_lpm_ipm1_trigger_enable /
s/exclusive_lpm_trigger_enable /exclusive_lpm_ipm1_trigger_enable /
s/exclusive_lpm2_trigger_enable     /exclusive_lpm_ipm2_trigger_enable /
s/exclusive_lpm2_trigger_enable /exclusive_lpm_ipm2_trigger_enable /

s/exclusive_lpm_bgo_enable      /exclusive_lpm_ipm1_bgo_enable /
s/exclusive_lpm_bgo_enable /exclusive_lpm_ipm1_bgo_enable /
s/exclusive_lpm2_bgo_enable     /exclusive_lpm_ipm2_bgo_enable /
s/exclusive_lpm2_bgo_enable /exclusive_lpm_ipm2_bgo_enable /

s/lpm_orbit_select      /lpm_ipm1_orbit_select /
s/lpm_orbit_select /lpm_ipm1_orbit_select /
s/lpm2_orbit_select     /lpm_ipm2_orbit_select /
s/lpm2_orbit_select /lpm_ipm2_orbit_select /
