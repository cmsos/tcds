#include "tcds/ici/ICITTSInfoSpaceHandler.h"

#include "tcds/utils/Definitions.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/WebServer.h"
#include "tcds/utils/WebTableTTS.h"

tcds::ici::ICITTSInfoSpaceHandler::ICITTSInfoSpaceHandler(xdaq::Application& xdaqApp,
                                                        tcds::utils::InfoSpaceUpdater* updater) :
  tcds::utils::InfoSpaceHandler(xdaqApp, "tcds-ici-tts-info", updater)
{
  // The TTS state of the detector partition, as received from the PI.
  createUInt32("main.ici_status.tts.from_partition",
               tcds::definitions::TTS_STATE_UNKNOWN_TCDS,
               "tts_state");

  // The TTS state of the associated APVE, if that exists.
  createUInt32("main.ici_status.tts.from_apve",
               tcds::definitions::TTS_STATE_UNKNOWN_TCDS,
               "tts_state");
}

tcds::ici::ICITTSInfoSpaceHandler::~ICITTSInfoSpaceHandler()
{
}

void
tcds::ici::ICITTSInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  std::string const itemSetName = "itemset-ici-tts-states";
  monitor.newItemSet(itemSetName);

  // The TTS state of the detector partition, as received from the PI.
  monitor.addItem(itemSetName,
                  "main.ici_status.tts.from_partition",
                  "Detector partition",
                  this,
                  "The TTS state of the detector partition, as received from the PI.");

  // The TTS state of the associated APVE, if that exists.
  monitor.addItem(itemSetName,
                  "main.ici_status.tts.from_apve",
                  "APVE",
                  this,
                  "The TTS state of the associated APVE, if that exists.");
}

void
tcds::ici::ICITTSInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                tcds::utils::Monitor& monitor,
                                                                std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "TTS" : forceTabName;

  webServer.registerTab(tabName,
                        "TTS information",
                        1);
  //----------

  std::string const itemSetName = "itemset-ici-tts-states";
  webServer.registerWebObject<tcds::utils::WebTableTTS>("TTS states",
                                                        "Over-all TTS information",
                                                        monitor,
                                                        itemSetName,
                                                        tabName,
                                                        1);
}
