#include "tcds/ici/BchannelRAMContents.h"

#include <cassert>
#include <iomanip>
#include <sstream>
#include <cstddef>

tcds::ici::BchannelRAMContents::BchannelRAMContents(std::vector<uint32_t> const dataIn) :
  rawData_(dataIn)
{
  // Each B-channel RAM entry consists of a control-word and a
  // data-word.
  // ASSERT ASSERT ASSERT
  assert ((dataIn.size() % 2) == 0);
  // ASSERT ASSERT ASSERT end
  size_t numEntries = dataIn.size() / 2;
  entries_.reserve(numEntries);
  entries_.clear();
  std::vector<uint32_t>::const_iterator it = dataIn.begin();
  while (it < dataIn.end())
    {
      BchannelRAMEntry newEntry(*(it + 1), *it);
      entries_.push_back(newEntry);
      it += 2;
    }
}

std::string
tcds::ici::BchannelRAMContents::getJSONString() const
{
  std::stringstream res;

  res << "[";
  size_t index = 0;
  for (std::vector<BchannelRAMEntry>::const_iterator it = entries_.begin();
       it != entries_.end();
       ++it)
    {
      res << "{";

      // The entry number.
      res << "\"Entry\": \"" << std::dec << index << "\"";
      res << ", ";

      // The control-word.
      res << "\"ControlWord\": \"0x"
          << std::hex << std::setw(8) << std::setfill('0') << it->ctrlWord() << "\"";
      res << ", ";

      // The data-word.
      res << "\"DataWord\": \"0x"
          << std::hex << std::setw(8) << std::setfill('0') << it->dataWord() << "\"";
      res << ", ";

      // The command.
      std::string commandStr = "Unknown";
      if (it->isDoNotEmit())
        {
          commandStr = "None";
        }
      else
        {
          if (it->isAchannelCommand())
            {
              commandStr = "A-channel command";
            }
          else if (it->isBchannelCommand())
            {
              if (it->isBchannelCommandShort())
                {
                  commandStr = "Broadcast B-channel command";
                }
              else if (it->isBchannelCommandLong())
                {
                  commandStr = "Addressed B-channel command";
                }
              else
                {
                  commandStr = "Unknown type of B-channel command";
                }
            }
          else if (it->isEndOfSequence())
            {
              commandStr = "End-of-sequence";
            }
        }
      res << "\"Command\": \"" << commandStr << "\"";
      res << ", ";

      // The command details.
      std::stringstream detailsStr;
      if (it->isAchannelCommand() || it->isBchannelCommand())
        {
          // The command data.
          detailsStr << "Data: 0x" << std::hex << std::setw(2) << std::setfill('0') << int(it->bcommandData());
          if (it->isBchannelCommand())
            {
              // The address, sub-address and internal/external
              // addressing flag (for long B-commands).
              std::stringstream addressStr;
              if (it->isBchannelCommandLong())
                {
                  uint16_t address = it->bcommandAddress();
                  uint8_t subAddress = it->bcommandSubaddress();
                  addressStr << std::hex
                             << std::setfill('0')
                             << "0x"
                             << std::setw(sizeof(address) * 2)
                             << address
                             << "/"
                             << std::hex
                             << std::setfill('0')
                             << "0x"
                             << std::setw(sizeof(subAddress) * 2)
                             << int(subAddress);
                  if (it->bcommandIsAddressExternal())
                    {
                      addressStr << " external";
                    }
                  else
                    {
                      addressStr << " internal";
                    }
                  detailsStr << ", Address: " << addressStr.str();
                }
              // else
              //   {
              //     addressStr << "n/a";
              //   }
              // detailsStr << ", Address: " << addressStr.str();
            }
        }
      // else
      //   {
      //     detailsStr << "n/a";
      //   }
      res << "\"CommandDetails\": \"" << detailsStr.str() << "\"";
      res << ", ";

      // Entry details.
      detailsStr.str("");
      if (it->isEndOfSequence())
        {
          detailsStr << "End-of-sequence";
        }

      if (!it->isDoNotEmit())
        {
          if (it->dynamicPostscale() != 0)
            {
              if (!detailsStr.str().empty()) {
                detailsStr << "; ";
              }
              detailsStr << "Postscale: " << it->dynamicPostscale();
            }
          if (it->isBchannelCommandLong() &&
              !it->bcommandIsReservedBit16Correct())
            {
              if (!detailsStr.str().empty()) {
                detailsStr << "; ";
              }
              detailsStr << "Invalid long TTC command: reserved bit 16 should be set to 1.";
            }
        }
      res << "\"EntryDetails\": \"" << detailsStr.str() << "\"";

      res << "}";

      if (it->isEndOfSequence())
        {
          // Done with this B-channel
          break;
        }

      if (it != (entries_.end() - 1))
        {
          res << ", ";
        }
      ++index;
    }
  res << "]";

  return res.str();
}
