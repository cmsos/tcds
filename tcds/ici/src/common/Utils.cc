#include "tcds/ici/Utils.h"

#include <cassert>

#include "toolbox/string.h"
#include "xcept/Exception.h"

#include "tcds/exception/Exception.h"
#include "tcds/ici/Definitions.h"

// std::string
// tcds::ici::BchannelEmissionModeToString(tcds::definitions::BCHANNEL_EMISSION_MODE const mode)
// {
//   std::string res = "UNKNOWN";
//   switch (mode)
//     {
//     case tcds::definitions::BCHANNEL_EMISSION_SINGLE:
//       res = "single";
//       break;
//     case tcds::definitions::BCHANNEL_EMISSION_DOUBLE:
//       res = "double";
//       break;
//     default:
//       // ASSERT ASSERT ASSERT
//       assert (false);
//       // ASSERT ASSERT ASSERT end
//       break;
//     }
//   return res;
// }

std::string
tcds::ici::BchannelTimingModeToString(tcds::definitions::BCHANNEL_TIMING_MODE const mode)
{
  std::string res = "UNKNOWN";
  switch (mode)
    {
    case tcds::definitions::BCHANNEL_TIMING_DELAYED:
      res = "delayed";
      break;
    case tcds::definitions::BCHANNEL_TIMING_BX_SYNC:
      res = "fixed-BX";
      break;
    default:
      // ASSERT ASSERT ASSERT
      assert (false);
      // ASSERT ASSERT ASSERT end
      break;
    }
  return res;
}

void
tcds::ici::verifyCyclicGeneratorNumber(unsigned int const genNumber)
{
  if ((genNumber < tcds::definitions::kICICyclicGenNumMin) ||
      (genNumber > tcds::definitions::kICICyclicGenNumMax))
    {
      std::string const msg = toolbox::toString("Generator number %d falls outside "
                                                "the allowed range of [%d, %d].",
                                                genNumber,
                                                tcds::definitions::kICICyclicGenNumMin,
                                                tcds::definitions::kICICyclicGenNumMax);
      XCEPT_RAISE(tcds::exception::ValueError, msg.c_str());
    }
}

tcds::definitions::SFP_LPM
tcds::ici::iciSFPID(unsigned int const iciNumber)
{
  tcds::definitions::SFP_LPM res;
  switch (iciNumber)
    {
    case 1:
      res = tcds::definitions::SFP_LPM_ICI1;
      break;
    case 2:
      res = tcds::definitions::SFP_LPM_ICI2;
      break;
    case 3:
      res = tcds::definitions::SFP_LPM_ICI3;
      break;
    case 4:
      res = tcds::definitions::SFP_LPM_ICI4;
      break;
    case 5:
      res = tcds::definitions::SFP_LPM_ICI5;
      break;
    case 6:
      res = tcds::definitions::SFP_LPM_ICI6;
      break;
    case 7:
      res = tcds::definitions::SFP_LPM_ICI7;
      break;
    case 8:
      res = tcds::definitions::SFP_LPM_ICI8;
      break;
    default:
      // ASSERT ASSERT ASSERT
      assert (false);
      // ASSERT ASSERT ASSERT end
      break;
    }

  return res;
}
