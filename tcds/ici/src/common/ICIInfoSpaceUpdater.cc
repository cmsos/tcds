#include "tcds/ici/ICIInfoSpaceUpdater.h"

#include "tcds/ici/TCADeviceICI.h"
#include "tcds/utils/InfoSpaceItem.h"

tcds::ici::ICIInfoSpaceUpdater::ICIInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                                    TCADeviceICI const& hw) :
  tcds::utils::HwInfoSpaceUpdaterBase(xdaqApp, hw)
{
}

tcds::ici::ICIInfoSpaceUpdater::~ICIInfoSpaceUpdater()
{
}

bool
tcds::ici::ICIInfoSpaceUpdater::updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                                    tcds::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  bool updated = false;
  tcds::ici::TCADeviceICI const& hw = getHw();
  if (hw.isHwConnected())
    {
      if (!updated)
        {
          updated = tcds::utils::HwInfoSpaceUpdaterBase::updateInfoSpaceItem(item, infoSpaceHandler);
        }
    }

  if (updated)
    {
      item.setValid();
    }
  else
    {
      item.setInvalid();
    }

  return updated;
}

tcds::ici::TCADeviceICI const&
tcds::ici::ICIInfoSpaceUpdater::getHw()
{
  return dynamic_cast<tcds::ici::TCADeviceICI const&>(tcds::utils::HwInfoSpaceUpdaterBase::getHw());
}
