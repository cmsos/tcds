#include "tcds/ici/ICIMultiBchannelInfoSpaceHandler.h"

#include <stdint.h>

#include "toolbox/string.h"

#include "tcds/ici/Definitions.h"
#include "tcds/ici/Utils.h"
#include "tcds/utils/Definitions.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/Utils.h"
#include "tcds/utils/WebServer.h"

tcds::ici::ICIMultiBchannelInfoSpaceHandler::ICIMultiBchannelInfoSpaceHandler(xdaq::Application& xdaqApp,
                                                                              tcds::utils::InfoSpaceUpdater* updater) :
  InfoSpaceHandler(xdaqApp, "tcds-icicontroller-multibchannels", updater)
{
  for (unsigned int multiBchannelNum = tcds::definitions::kMultiBchannelNumMin;
       multiBchannelNum <= tcds::definitions::kMultiBchannelNumMax;
       ++multiBchannelNum)
    {
      // Configuration settings.
      std::string nameBase =
        toolbox::toString("multibchannels.multibchannel%d.configuration", multiBchannelNum);
      createUInt32(nameBase + ".bx_sync");
      createUInt32(nameBase + ".bx_or_delay");

      for (unsigned int bgoNumInt = tcds::definitions::kBchannelInMultiBchannelNumMin;
           bgoNumInt <= tcds::definitions::kBchannelInMultiBchannelNumMax;
           ++bgoNumInt)
        {
          int const bgoNum = (tcds::definitions::kBchannelInMultiBchannelNumMax - tcds::definitions::kBchannelInMultiBchannelNumMin + 1) * multiBchannelNum + (tcds::definitions::kBgoNumOriMax - tcds::definitions::kBgoNumOriMin + 1) + bgoNumInt;
          createBool(nameBase + toolbox::toString(".bgo_selection.bgo%d",
                                                  bgoNum),
                     "",
                     "enabled/disabled");
        }

      // RAM/B-commands.
      nameBase = toolbox::toString("multibchannels.multibchannel%d.ram", multiBchannelNum);
      for (unsigned int bgoNumInt = tcds::definitions::kBchannelInMultiBchannelNumMin;
           bgoNumInt <= tcds::definitions::kBchannelInMultiBchannelNumMax;
           ++bgoNumInt)
        {
          int const bgoNum = (tcds::definitions::kBchannelInMultiBchannelNumMax - tcds::definitions::kBchannelInMultiBchannelNumMin + 1) * multiBchannelNum + (tcds::definitions::kBgoNumOriMax - tcds::definitions::kBgoNumOriMin + 1) + bgoNumInt;
          createUInt32(nameBase + toolbox::toString(".bgo%d", bgoNum),
                       0,
                       "x8");
        }

      // Status items.
      nameBase = toolbox::toString("multibchannels.multibchannel%d.status", multiBchannelNum);

      for (unsigned int bgoNumInt = tcds::definitions::kBchannelInMultiBchannelNumMin;
           bgoNumInt <= tcds::definitions::kBchannelInMultiBchannelNumMax;
           ++bgoNumInt)
        {
          int const bgoNum = (tcds::definitions::kBchannelInMultiBchannelNumMax - tcds::definitions::kBchannelInMultiBchannelNumMin + 1) * multiBchannelNum + (tcds::definitions::kBgoNumOriMax - tcds::definitions::kBgoNumOriMin + 1) + bgoNumInt;
          createUInt32(nameBase + toolbox::toString(".bgo_counters.bgo%d", bgoNum));
          createUInt32(nameBase + toolbox::toString(".bgo_when_busy_counters.bgo%d", bgoNum));
          createUInt32(nameBase + toolbox::toString(".cancel_counters.bgo%d", bgoNum));
        }

    }
}

void
tcds::ici::ICIMultiBchannelInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  for (int multiBchannelNum = tcds::definitions::kBgoNumMin;
       multiBchannelNum <= tcds::definitions::kBgoNumMax;
       ++multiBchannelNum)
    {
      // Configuration settings.
      std::string itemSetName = toolbox::toString("itemset-multibchannel%d-config", multiBchannelNum);
      monitor.newItemSet(itemSetName);
      std::string nameBase =
        toolbox::toString("multibchannels.multibchannel%d.configuration", multiBchannelNum);
      monitor.addItem(itemSetName,
                      nameBase + ".bx_sync",
                      "Timing mode",
                      this);
      monitor.addItem(itemSetName,
                      nameBase + ".bx_or_delay",
                      "Timing setting (BX/orbit)",
                      this,
                      "For BX-synchronous mode this setting is specified in BX."
                      "For delay mode this setting is specified in orbits.");
      for (unsigned int bgoNumInt = tcds::definitions::kBchannelInMultiBchannelNumMin;
           bgoNumInt <= tcds::definitions::kBchannelInMultiBchannelNumMax;
           ++bgoNumInt)
        {
          int const bgoNum = (tcds::definitions::kBchannelInMultiBchannelNumMax - tcds::definitions::kBchannelInMultiBchannelNumMin + 1) * multiBchannelNum + (tcds::definitions::kBgoNumOriMax - tcds::definitions::kBgoNumOriMin + 1) + bgoNumInt;
          monitor.addItem(itemSetName,
                          nameBase + toolbox::toString(".bgo_selection.bgo%d", bgoNum),
                          toolbox::toString("Trigger on B-go %d", bgoNum),
                          this);
        }

      // RAM/B-commands.
      itemSetName = toolbox::toString("itemset-multibchannel%d-ram", multiBchannelNum);
      monitor.newItemSet(itemSetName);
      for (unsigned int bgoNumInt = tcds::definitions::kBchannelInMultiBchannelNumMin;
           bgoNumInt <= tcds::definitions::kBchannelInMultiBchannelNumMax;
           ++bgoNumInt)
        {
          int const bgoNum = (tcds::definitions::kBchannelInMultiBchannelNumMax - tcds::definitions::kBchannelInMultiBchannelNumMin + 1) * multiBchannelNum + (tcds::definitions::kBgoNumOriMax - tcds::definitions::kBgoNumOriMin + 1) + bgoNumInt;
          monitor.addItem(itemSetName,
                          toolbox::toString("multibchannels.multibchannel%d.ram.bgo%d",
                                            multiBchannelNum,
                                            bgoNum),
                          toolbox::toString("B-go %d B-command",
                                            bgoNum),
                          this);
        }

      // Status items.
      itemSetName = toolbox::toString("itemset-multibchannel%d-requestcounters", multiBchannelNum);
      monitor.newItemSet(itemSetName);
      nameBase = toolbox::toString("multibchannels.multibchannel%d.status", multiBchannelNum);

      for (unsigned int bgoNumInt = tcds::definitions::kBchannelInMultiBchannelNumMin;
           bgoNumInt <= tcds::definitions::kBchannelInMultiBchannelNumMax;
           ++bgoNumInt)
        {
          int const bgoNum = (tcds::definitions::kBchannelInMultiBchannelNumMax - tcds::definitions::kBchannelInMultiBchannelNumMin + 1) * multiBchannelNum + (tcds::definitions::kBgoNumOriMax - tcds::definitions::kBgoNumOriMin + 1) + bgoNumInt;
          monitor.addItem(itemSetName,
                          nameBase + toolbox::toString(".bgo_counters.bgo%d",
                                                       bgoNum),
                          toolbox::toString("# B-go %d requested",
                                            bgoNum),
                          this);
        }
      itemSetName = toolbox::toString("itemset-multibchannel%d-cancelcounters", multiBchannelNum);
      monitor.newItemSet(itemSetName);
      nameBase = toolbox::toString("multibchannels.multibchannel%d.status", multiBchannelNum);

      for (unsigned int bgoNumInt = tcds::definitions::kBchannelInMultiBchannelNumMin;
           bgoNumInt <= tcds::definitions::kBchannelInMultiBchannelNumMax;
           ++bgoNumInt)
        {
          int const bgoNum = (tcds::definitions::kBchannelInMultiBchannelNumMax - tcds::definitions::kBchannelInMultiBchannelNumMin + 1) * multiBchannelNum + (tcds::definitions::kBgoNumOriMax - tcds::definitions::kBgoNumOriMin + 1) + bgoNumInt;
          monitor.addItem(itemSetName,
                          nameBase + toolbox::toString(".bgo_when_busy_counters.bgo%d",
                                                       bgoNum),
                          toolbox::toString("# B-go %d requested while busy",
                                            bgoNum),
                          this);
          monitor.addItem(itemSetName,
                          nameBase + toolbox::toString(".cancel_counters.bgo%d",
                                                       bgoNum),
                          toolbox::toString("# B-go %d cancelled",
                                            bgoNum),
                          this);
        }
    }
}

void
tcds::ici::ICIMultiBchannelInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                           tcds::utils::Monitor& monitor,
                                                                           std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "Multi-B-channels" : forceTabName;

  webServer.registerTab(tabName,
                        "Multi-B-channel configuration information."
                        " NOTE: These are experimental, and not intended for production (yet).",
                        4);

  std::string name;
  std::string desc = "";
  for (unsigned int multiBchannelNum = tcds::definitions::kMultiBchannelNumMin;
       multiBchannelNum <= tcds::definitions::kMultiBchannelNumMax;
       ++multiBchannelNum)
    {
      // Configuration items.
      name = toolbox::toString("Multi-B-channel %d configuration", multiBchannelNum);
      webServer.registerTable(name,
                              desc,
                              monitor,
                              toolbox::toString("itemset-multibchannel%d-config", multiBchannelNum),
                              tabName);

      // RAM/B-commands.
      name = toolbox::toString("Multi-B-channel %d RAM contents", multiBchannelNum);
      webServer.registerTable(name,
                              desc,
                              monitor,
                              toolbox::toString("itemset-multibchannel%d-ram", multiBchannelNum),
                              tabName);

      // Status items.
      name = toolbox::toString("Multi-B-channel %d request counters", multiBchannelNum);
      webServer.registerTable(name,
                              desc,
                              monitor,
                              toolbox::toString("itemset-multibchannel%d-requestcounters", multiBchannelNum),
                              tabName);
      name = toolbox::toString("Multi-B-channel %d cancellation counters", multiBchannelNum);
      webServer.registerTable(name,
                              desc,
                              monitor,
                              toolbox::toString("itemset-multibchannel%d-cancelcounters", multiBchannelNum),
                              tabName);
    }
}

std::string
tcds::ici::ICIMultiBchannelInfoSpaceHandler::formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const
{
  std::string res = tcds::utils::escapeAsJSONString(tcds::utils::InfoSpaceHandler::kInvalidItemString);
  if (item->isValid())
    {
      std::string name = item->name();
      if (toolbox::endsWith(name, ".bx_sync"))
        {
          uint32_t const value = getUInt32(name);
          tcds::definitions::BCHANNEL_TIMING_MODE const valueEnum =
            static_cast<tcds::definitions::BCHANNEL_TIMING_MODE>(value);
          res = tcds::utils::escapeAsJSONString(tcds::ici::BchannelTimingModeToString(valueEnum));
        }
      else
        {
          // For everything else simply call the generic formatter.
          res = InfoSpaceHandler::formatItem(item);
        }
    }
  return res;
}
