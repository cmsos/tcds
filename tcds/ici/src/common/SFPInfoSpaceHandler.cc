#include "tcds/ici/SFPInfoSpaceHandler.h"

#include <cstddef>

#include "toolbox/string.h"

#include "tcds/hwlayertca/Definitions.h"
#include "tcds/hwlayertca/Utils.h"
#include "tcds/hwutilstca/SFPInfoSpaceUpdater.h"
#include "tcds/ici/Utils.h"
#include "tcds/utils/ConfigurationInfoSpaceHandler.h"
#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/WebServer.h"
#include "tcds/utils/XDAQAppBase.h"

tcds::ici::SFPInfoSpaceHandler::SFPInfoSpaceHandler(tcds::utils::XDAQAppBase& xdaqApp,
                                                    tcds::hwutilstca::SFPInfoSpaceUpdater* updater) :
  tcds::hwutilstca::SFPInfoSpaceHandlerBase(xdaqApp, updater)
{
  unsigned short const iciNumber =
    getOwnerApplication().getConfigurationInfoSpaceHandler().getUInt32("iciNumber");
  sfpId_ = tcds::ici::iciSFPID(iciNumber);

  // The SFP connecting the ICI to the PI.
  std::string const name = tcds::hwlayertca::sfpToRegName(sfpId_);
  std::string const infoSpaceHandlerName = toolbox::toString("tcds-sfp-info-ici-%s",
                                                             name.c_str());

  tcds::utils::InfoSpaceHandler* handler =
    new tcds::utils::InfoSpaceHandler(xdaqApp, infoSpaceHandlerName, updater);
  infoSpaceHandlers_[name] = handler;
  createSFPChannel(handler,
                   tcds::definitions::kSFP_TYPE_LPM_ICI,
                   sfpId_,
                   name);
}

tcds::ici::SFPInfoSpaceHandler::~SFPInfoSpaceHandler()
{
  // Cleanup of InfoSpaceHandlers happens in the MultiInfoSpaceHandler
  // base class.
}

tcds::utils::XDAQAppBase&
tcds::ici::SFPInfoSpaceHandler::getOwnerApplication() const
{
  return static_cast<tcds::utils::XDAQAppBase&>(tcds::hwutilstca::SFPInfoSpaceHandlerBase::getOwnerApplication());
}

void
tcds::ici::SFPInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  // The SFP connecting the ICI to the PI.
  std::string const name = tcds::hwlayertca::sfpToShortName(sfpId_);
  std::string const itemSetName = toolbox::toString("itemset-sfp-status-%s",
                                                    name.c_str());
  registerSFPChannel(tcds::hwlayertca::sfpToRegName(sfpId_), itemSetName, monitor);
}

void
tcds::ici::SFPInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                              tcds::utils::Monitor& monitor,
                                                              std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "SFP" : forceTabName;
  size_t const numColumns = 1;

  webServer.registerTab(tabName, "SFP monitoring", numColumns);

  //----------

  // The SFP connecting the ICI to the PI.
  std::string const name = tcds::hwlayertca::sfpToShortName(sfpId_);
  std::string const itemSetName = toolbox::toString("itemset-sfp-status-%s",
                                                    name.c_str());
  webServer.registerTable(tcds::hwlayertca::sfpToLongName(sfpId_),
                          "",
                          monitor,
                          itemSetName,
                          tabName);
}
