#include "tcds/ici/ICIBdataInfoSpaceHandler.h"

#include <stdint.h>

#include "tcds/utils/Definitions.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/Utils.h"
#include "tcds/utils/WebServer.h"

tcds::ici::ICIBdataInfoSpaceHandler::ICIBdataInfoSpaceHandler(xdaq::Application& xdaqApp,
                                                              tcds::utils::InfoSpaceUpdater* updater) :
  InfoSpaceHandler(xdaqApp, "tcds-icicontroller-bdata", updater)
{
  // The configuration settings.
  createBool("bchannels.main.bdata_config.bdata_enable", "", "enabled/disabled");
  createUInt32("bchannels.main.bdata_config.bdata_source_select");
  createUInt32("bchannels.main.bdata_config.bdata_type");
  createUInt32("bchannels.main.bdata_config.ttcrx_address", 0x0, "hex");
  createUInt32("bchannels.main.bdata_config.target_address", 0x0, "hex");
  createUInt32("bchannels.main.bdata_config.is_external");

  // The B-data spy.
  createUInt32("bchannels.main.bdata_spy.ttcrx_address", 0x0, "hex");
  createUInt32("bchannels.main.bdata_spy.target_address", 0x0, "hex");
  createUInt32("bchannels.main.bdata_spy.is_external");
  createUInt32("bchannels.main.bdata_spy.data", 0x0, "hex");
}

void
tcds::ici::ICIBdataInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  // The configuration settings.
  monitor.newItemSet("itemset-bdata-config");
  monitor.addItem("itemset-bdata-config",
                  "bchannels.main.bdata_config.bdata_enable",
                  "B-data transmission",
                  this);
  monitor.addItem("itemset-bdata-config",
                  "bchannels.main.bdata_config.bdata_source_select",
                  "B-data source",
                  this);
  monitor.addItem("itemset-bdata-config",
                  "bchannels.main.bdata_config.bdata_type",
                  "B-data transmission type",
                  this);
  monitor.addItem("itemset-bdata-config",
                  "bchannels.main.bdata_config.ttcrx_address",
                  "TTCrx address",
                  this);
  monitor.addItem("itemset-bdata-config",
                  "bchannels.main.bdata_config.target_address",
                  "Target address",
                  this);
  monitor.addItem("itemset-bdata-config",
                  "bchannels.main.bdata_config.is_external",
                  "Addressing mode",
                  this);

  // The B-data spy.
  monitor.newItemSet("itemset-bdata-spy");
  monitor.addItem("itemset-bdata-spy",
                  "bchannels.main.bdata_spy.ttcrx_address",
                  "TTCrx address",
                  this);
  monitor.addItem("itemset-bdata-spy",
                  "bchannels.main.bdata_spy.target_address",
                  "target address",
                  this);
  monitor.addItem("itemset-bdata-spy",
                  "bchannels.main.bdata_spy.is_external",
                  "Addressing mode",
                  this);
  monitor.addItem("itemset-bdata-spy",
                  "bchannels.main.bdata_spy.data",
                  "Last sent B-data value",
                  this);
}

void
tcds::ici::ICIBdataInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                   tcds::utils::Monitor& monitor,
                                                                   std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "B-data" : forceTabName;

  webServer.registerTab(tabName,
                        "B-data configuration and monitoring information",
                        3);
  webServer.registerTable("Configuration",
                          "B-data configuration",
                          monitor,
                          "itemset-bdata-config",
                          tabName);
  webServer.registerTable("Spy",
                          "B-data spy information",
                          monitor,
                          "itemset-bdata-spy",
                          tabName);
}

std::string
tcds::ici::ICIBdataInfoSpaceHandler::formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const
{
  // Specialized formatting rules for the enums.
  std::string res = tcds::utils::escapeAsJSONString(tcds::utils::InfoSpaceHandler::kInvalidItemString);
  if (item->isValid())
    {
      std::string name = item->name();
      if (name == "bchannels.main.bdata_config.bdata_source_select")
        {
          uint32_t const value = getUInt32(name);
          tcds::definitions::BDATA_SOURCE valueEnum =
            static_cast<tcds::definitions::BDATA_SOURCE>(value);
          res = tcds::utils::escapeAsJSONString(tcds::utils::BdataSourceToString(valueEnum));
        }
      else if (name == "bchannels.main.bdata_config.bdata_type")
        {
          uint32_t const value = getUInt32(name);
          tcds::definitions::BCOMMAND_TYPE valueEnum =
            static_cast<tcds::definitions::BCOMMAND_TYPE>(value);
          res = tcds::utils::escapeAsJSONString(tcds::utils::BcommandTypeToString(valueEnum));
        }
      else if ((name == "bchannels.main.bdata_config.is_external") ||
          (name == "bchannels.main.bdata_spy.is_external"))
        {
          uint32_t const value = getUInt32(name);
          tcds::definitions::BCOMMAND_ADDRESS_TYPE valueEnum =
            static_cast<tcds::definitions::BCOMMAND_ADDRESS_TYPE>(value);
          res = tcds::utils::escapeAsJSONString(tcds::utils::BcommandAddressingTypeToString(valueEnum));
        }
      else
        {
          // For everything else simply call the generic formatter.
          res = InfoSpaceHandler::formatItem(item);
        }
    }
  return res;
}
