#include "tcds/ici/ICIBdataInfoSpaceUpdater.h"

#include "tcds/ici/TCADeviceICI.h"
#include "tcds/utils/InfoSpaceItem.h"

tcds::ici::ICIBdataInfoSpaceUpdater::ICIBdataInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                                              TCADeviceICI const& hw) :
  tcds::utils::HwInfoSpaceUpdaterBase(xdaqApp, hw)
{
}

tcds::ici::ICIBdataInfoSpaceUpdater::~ICIBdataInfoSpaceUpdater()
{
}

bool
tcds::ici::ICIBdataInfoSpaceUpdater::updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                                         tcds::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  bool updated = false;
  tcds::ici::TCADeviceICI const& hw = getHw();
  if (hw.isHwConnected())
    {
      if (!updated)
        {
          updated = tcds::utils::HwInfoSpaceUpdaterBase::updateInfoSpaceItem(item, infoSpaceHandler);
        }
    }

  if (updated)
    {
      item.setValid();
    }
  else
    {
      item.setInvalid();
    }

  return updated;
}

tcds::ici::TCADeviceICI const&
tcds::ici::ICIBdataInfoSpaceUpdater::getHw()
{
  return dynamic_cast<tcds::ici::TCADeviceICI const&>(tcds::utils::HwInfoSpaceUpdaterBase::getHw());

}
