#include "tcds/ici/ConfigurationInfoSpaceHandler.h"

#include "tcds/hwutilstca/SFPMonitoringConfigHelpers.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Monitor.h"

tcds::ici::ConfigurationInfoSpaceHandler::ConfigurationInfoSpaceHandler(xdaq::Application& xdaqApp) :
  tcds::hwutilstca::ConfigurationInfoSpaceHandlerTCA(xdaqApp)
{
  createUInt32("iciNumber", 1, "", tcds::utils::InfoSpaceItem::NOUPDATE, true);

  // SFP monitoring parameters.
  tcds::hwutilstca::SFPMonitoringConfigCreateItems(this);
}

void
tcds::ici::ConfigurationInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  tcds::hwutilstca::ConfigurationInfoSpaceHandlerTCA::registerItemSetsWithMonitor(monitor);
  monitor.addItem("Application configuration",
                  "iciNumber",
                  "iCI number",
                  this);

  // SFP monitoring parameters.
  tcds::hwutilstca::SFPMonitoringRegisterItemsWithMonitor(this, monitor);
}
