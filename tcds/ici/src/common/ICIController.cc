#include "tcds/ici/ICIController.h"

#include <string>

#include "log4cplus/loggingmacros.h"
#include "toolbox/Event.h"
#include "toolbox/string.h"
#include "xcept/Exception.h"
#include "xdaq/exception/ApplicationInstantiationFailed.h"

#include "tcds/exception/Exception.h"
#include "tcds/hwlayer/DeviceBase.h"
#include "tcds/hwlayer/RegisterInfo.h"
#include "tcds/hwutilstca/CyclicGensInfoSpaceHandler.h"
#include "tcds/hwutilstca/CyclicGensInfoSpaceUpdater.h"
#include "tcds/hwutilstca/HwIDInfoSpaceHandlerTCA.h"
#include "tcds/hwutilstca/HwIDInfoSpaceUpdaterTCA.h"
#include "tcds/hwutilstca/SFPInfoSpaceUpdater.h"
#include "tcds/hwutilstca/Utils.h"
#include "tcds/ici/ConfigurationInfoSpaceHandler.h"
#include "tcds/ici/Definitions.h"
#include "tcds/ici/HwStatusInfoSpaceHandler.h"
#include "tcds/ici/HwStatusInfoSpaceUpdater.h"
#include "tcds/ici/ICIBchannelInfoSpaceHandler.h"
#include "tcds/ici/ICIBchannelInfoSpaceUpdater.h"
#include "tcds/ici/ICIBdataInfoSpaceHandler.h"
#include "tcds/ici/ICIBdataInfoSpaceUpdater.h"
#include "tcds/ici/ICICountersInfoSpaceHandler.h"
#include "tcds/ici/ICICountersInfoSpaceUpdater.h"
#include "tcds/ici/ICIInfoSpaceHandler.h"
#include "tcds/ici/ICIInfoSpaceUpdater.h"
#include "tcds/ici/ICIMultiBchannelInfoSpaceHandler.h"
#include "tcds/ici/ICITTSInfoSpaceHandler.h"
#include "tcds/ici/SFPInfoSpaceHandler.h"
#include "tcds/ici/TCADeviceICI.h"
#include "tcds/utils/LogMacros.h"
#include "tcds/utils/XDAQAppBase.h"

XDAQ_INSTANTIATOR_IMPL(tcds::ici::ICIController)

tcds::ici::ICIController::ICIController(xdaq::ApplicationStub* stub)
try
  :
  tcds::utils::XDAQAppWithFSMBasic(stub, std::unique_ptr<tcds::hwlayer::DeviceBase>(new tcds::ici::TCADeviceICI())),
    soapCmdDisableBChannel_(*this),
    soapCmdDisableCyclicGenerator_(*this),
    soapCmdDumpHardwareState_(*this),
    soapCmdEnableBChannel_(*this),
    soapCmdEnableCyclicGenerator_(*this),
    soapCmdInitCyclicGenerator_(*this),
    soapCmdInitCyclicGenerators_(*this),
    soapCmdReadHardwareConfiguration_(*this),
    soapCmdSendBCommand_(*this),
    soapCmdSendBgo_(*this),
    soapCmdSendL1A_(*this)
  {
    // Create the InfoSpace holding all configuration information.
    cfgInfoSpaceP_ =
      std::unique_ptr<tcds::ici::ConfigurationInfoSpaceHandler>(new tcds::ici::ConfigurationInfoSpaceHandler(*this));

    // Make sure the correct default hardware configuration file is found.
    cfgInfoSpaceP_->setString("defaultHwConfigurationFilePath",
                              "${XDAQ_ROOT}/etc/tcds/ici/hw_cfg_default_ici.txt");
  }
catch (tcds::exception::Exception const& err)
  {
    std::string msgBase = "Something went wrong instantiating the ICIController application";
    std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.what());
    XCEPT_RAISE(xdaq::exception::ApplicationInstantiationFailed, msg.c_str());
  }

tcds::ici::ICIController::~ICIController()
{
  hwRelease();
}

void
tcds::ici::ICIController::setupInfoSpaces()
{
  // Make sure the configuration settings are up-to-date.
  cfgInfoSpaceP_->readInfoSpace();

  // Instantiate all hardware-related InfoSpaceHandlers and InfoSpaceUpdaters.
  cyclicGensInfoSpaceUpdaterP_ = std::unique_ptr<tcds::hwutilstca::CyclicGensInfoSpaceUpdater>(new tcds::hwutilstca::CyclicGensInfoSpaceUpdater(*this, getHw()));
  cyclicGensInfoSpaceP_ =
    std::unique_ptr<tcds::hwutilstca::CyclicGensInfoSpaceHandler>(new tcds::hwutilstca::CyclicGensInfoSpaceHandler(*this, cyclicGensInfoSpaceUpdaterP_.get(), tcds::definitions::kNumCyclicGensPerICI));
  hwIDInfoSpaceUpdaterP_ =
    std::unique_ptr<tcds::hwutilstca::HwIDInfoSpaceUpdaterTCA>(new tcds::hwutilstca::HwIDInfoSpaceUpdaterTCA(*this, getHw()));
  hwIDInfoSpaceP_ =
    std::unique_ptr<tcds::hwutilstca::HwIDInfoSpaceHandlerTCA>(new tcds::hwutilstca::HwIDInfoSpaceHandlerTCA(*this, hwIDInfoSpaceUpdaterP_.get()));
  hwStatusInfoSpaceUpdaterP_ =
    std::unique_ptr<tcds::ici::HwStatusInfoSpaceUpdater>(new tcds::ici::HwStatusInfoSpaceUpdater(*this, getHw()));
  hwStatusInfoSpaceP_ =
    std::unique_ptr<tcds::ici::HwStatusInfoSpaceHandler>(new tcds::ici::HwStatusInfoSpaceHandler(*this, hwStatusInfoSpaceUpdaterP_.get()));
  iciInfoSpaceUpdaterP_ =
    std::unique_ptr<ICIInfoSpaceUpdater>(new ICIInfoSpaceUpdater(*this, getHw()));
  iciInfoSpaceP_ =
    std::unique_ptr<ICIInfoSpaceHandler>(new ICIInfoSpaceHandler(*this, iciInfoSpaceUpdaterP_.get()));
  iciBchannelInfoSpaceUpdaterP_ =
    std::unique_ptr<ICIBchannelInfoSpaceUpdater>(new ICIBchannelInfoSpaceUpdater(*this, getHw()));
  iciBchannelInfoSpaceP_ =
    std::unique_ptr<ICIBchannelInfoSpaceHandler>(new ICIBchannelInfoSpaceHandler(*this, iciBchannelInfoSpaceUpdaterP_.get()));
  iciBdataInfoSpaceUpdaterP_ =
    std::unique_ptr<ICIBdataInfoSpaceUpdater>(new ICIBdataInfoSpaceUpdater(*this, getHw()));
  iciBdataInfoSpaceP_ =
    std::unique_ptr<ICIBdataInfoSpaceHandler>(new ICIBdataInfoSpaceHandler(*this, iciBdataInfoSpaceUpdaterP_.get()));
  iciCountersInfoSpaceUpdaterP_ =
    std::unique_ptr<ICICountersInfoSpaceUpdater>(new ICICountersInfoSpaceUpdater(*this, getHw()));
  iciCountersInfoSpaceP_ =
    std::unique_ptr<ICICountersInfoSpaceHandler>(new ICICountersInfoSpaceHandler(*this, iciCountersInfoSpaceUpdaterP_.get()));
  iciMultiBchannelInfoSpaceUpdaterP_ =
    std::unique_ptr<ICIBchannelInfoSpaceUpdater>(new ICIBchannelInfoSpaceUpdater(*this, getHw()));
  iciMultiBchannelInfoSpaceP_ =
    std::unique_ptr<ICIMultiBchannelInfoSpaceHandler>(new ICIMultiBchannelInfoSpaceHandler(*this, iciMultiBchannelInfoSpaceUpdaterP_.get()));
  iciTTSInfoSpaceP_ =
    std::unique_ptr<ICITTSInfoSpaceHandler>(new ICITTSInfoSpaceHandler(*this, iciInfoSpaceUpdaterP_.get()));
  sfpInfoSpaceUpdaterP_ =
    std::unique_ptr<tcds::hwutilstca::SFPInfoSpaceUpdater>(new tcds::hwutilstca::SFPInfoSpaceUpdater(*this, getHw()));
  sfpInfoSpaceP_ =
    std::unique_ptr<SFPInfoSpaceHandler>(new SFPInfoSpaceHandler(*this, sfpInfoSpaceUpdaterP_.get()));

  // Register all InfoSpaceItems with the Monitor and the WebServer.
  cfgInfoSpaceP_->registerItemSets(monitor_, webServer_);
  appStateInfoSpace_.registerItemSets(monitor_, webServer_);
  hwIDInfoSpaceP_->registerItemSets(monitor_, webServer_);
  hwStatusInfoSpaceP_->registerItemSets(monitor_, webServer_);
  sfpInfoSpaceP_->registerItemSets(monitor_, webServer_);
  iciInfoSpaceP_->registerItemSets(monitor_, webServer_);
  iciBchannelInfoSpaceP_->registerItemSets(monitor_, webServer_);
  iciMultiBchannelInfoSpaceP_->registerItemSets(monitor_, webServer_);
  iciBdataInfoSpaceP_->registerItemSets(monitor_, webServer_);
  cyclicGensInfoSpaceP_->registerItemSets(monitor_, webServer_);
  iciTTSInfoSpaceP_->registerItemSets(monitor_, webServer_);
  iciCountersInfoSpaceP_->registerItemSets(monitor_, webServer_);
}

tcds::ici::TCADeviceICI&
tcds::ici::ICIController::getHw() const
{
  return static_cast<tcds::ici::TCADeviceICI&>(*hwP_.get());
}

void
tcds::ici::ICIController::hwConnectImpl()
{
  tcds::hwutilstca::tcaDeviceHwConnectImpl(*cfgInfoSpaceP_, getHw());
}

void
tcds::ici::ICIController::hwReleaseImpl()
{
  getHw().hwRelease();
}

void
tcds::ici::ICIController::configureActionImpl(toolbox::Event::Reference event)
{
  // Extract the iCI number from the configuration and propagate that
  // into our hardware device instance.
  unsigned short const iciNumber = cfgInfoSpaceP_->getUInt32("iciNumber");
  getHw().setICINumber(iciNumber);

  // Then do what we normally do.
  tcds::utils::XDAQAppWithFSMBasic::configureActionImpl(event);
}

void
tcds::ici::ICIController::enableActionImpl(toolbox::Event::Reference event)
{
  // Enable triggers.
  getHw().enableTrigger();
}

void
tcds::ici::ICIController::stopActionImpl(toolbox::Event::Reference event)
{
  tcds::utils::XDAQAppWithFSMBasic::stopActionImpl(event);

  makeSafe();
}

void
tcds::ici::ICIController::zeroActionImpl(toolbox::Event::Reference event)
{
  tcds::ici::TCADeviceICI& hw = getHw();

  hw.resetStatus();
  hw.resetCounters();
  hw.initCyclicGeneratorsAndBchannels();
}

void
tcds::ici::ICIController::hwCfgInitializeImpl()
{
  // Check for the presence of the TTC clock. Without TTC clock it is
  // no use continuing (and apart from that, some registers will not
  // be accessible).
  if (!(getHw().isTTCClockUp() && getHw().isTTCClockStable()))
    {
      std::string const msg = "Could not configure the hardware: no TTC clock present, or clock not stable.";
      ERROR(msg);
      XCEPT_RAISE(tcds::exception::TTCClockProblem, msg.c_str());
    }

  //----------

  // Configure and enable the 'TTC-stream phase monitoring.'
  // NOTE: This really measures phases between clocks, not w.r.t. the
  // real TTC stream, so this can be enabled before selecting the TTC
  // stream source.
  getHw().enablePhaseMonitoring();

  //----------

  // Configure and enable the 'TTC-stream phase monitoring.'
  // NOTE: This really measures phases between clocks, not the real
  // TTC stream.
  getHw().enablePhaseMonitoring();

  //----------

  makeSafe();
}

void
tcds::ici::ICIController::hwCfgFinalizeImpl()
{
  // Switch on the fast-track for triggers from the LPM.
  getHw().enableCPML1AFastTrack();

  // Configure the BRILDAQ information from the CPM on the backplane.
  getHw().enableCPMBRILDAQData();

  // Configure the internal adjustment of the cyclic triggers.
  getHw().configureTriggerAlignment();

  // Configure the internal adjustment of the B-channels.
  getHw().configureBChannelAlignment();

  // Make sure the trigger rules are enabled.
  getHw().writeRegister("main.inselect.trigger_rule_checker_disable", 0x0);

  // Make sure that triggers are not accidentally inhibited.
  getHw().writeRegister("main.inselect.define_l1a_in1_as_trigger_inhibit", 0x0);

  // There is a certain bit in the B-data configuration that _has_ to
  // be set in order to produce valid long B-command frames.
  getHw().writeRegister("bchannels.main.bdata_config.reserved_keep_set_to_one", 0x1);

  // Make sure that the align mode for the PI-to-LPM TTS links is set
  // to automatic.
  // NOTE: This is an LPM-wide setting that applies to all incoming
  // PI-to-LPM TTS links. It can only be set at Configure time of the
  // ICIController, though, since there is no guarantee the
  // LPMController is ever configured.
  getHw().enableTTSLinkAutoAlignMode();
}

tcds::utils::RegCheckResult
tcds::ici::ICIController::isRegisterAllowed(tcds::hwlayer::RegisterInfo const& regInfo) const
{
  // Start by applying the default selection.
  tcds::utils::RegCheckResult res = tcds::utils::XDAQAppBase::isRegisterAllowed(regInfo);

  // The overall trigger_enable register is handled by the XDAQ
  // control application.
  res = (res && !toolbox::endsWith(regInfo.name(), ".trigger_enable"))
    ? res : tcds::utils::kRegCheckResultDisallowed;

  // One is not supposed to disable the trigger rules.
  res = (res && !toolbox::endsWith(regInfo.name(), ".trigger_rule_checker_disable"))
    ? res : tcds::utils::kRegCheckResultDisallowed;

  // One is not supposed to disable the triggers.
  res = (res && !toolbox::endsWith(regInfo.name(), ".define_l1a_in1_as_trigger_inhibit"))
    ? res : tcds::utils::kRegCheckResultDisallowed;

  // One is not supposed to use the LPM external inputs directly in
  // the ICI. (Only to be used via the LPMController.)
  res = (res && !toolbox::endsWith(regInfo.name(), ".combined_l1a_in0_trigger_enable"))
    ? res : tcds::utils::kRegCheckResultDisallowed;
  res = (res && !toolbox::endsWith(regInfo.name(), ".combined_l1a_in1_trigger_enable"))
    ? res : tcds::utils::kRegCheckResultDisallowed;

  return res;
}

unsigned short
tcds::ici::ICIController::iciNumber() const
{
  return getHw().iciNumber();
}

void
tcds::ici::ICIController::makeSafe() const
{
  // This method puts the hardware in a 'safe' state for subsystems:
  // all triggers are blocked, and all non-permanent cyclic generators
  // are disabled.

  // Send an 'internal B-go Stop', just to the ICI itself, in order to
  // block triggers and stop non-permanent cyclics.
  getHw().stop();

  // Disable the triggers completely. This makes sure that a subsystem
  // leaving the run after stopping will not inadvertently still
  // receive L1As. Triggers will be re-enabled in the 'Enable'
  // transition.
  getHw().disableTrigger();
}
