#include "tcds/ici/TCADeviceICI.h"

#include <cassert>
#include <memory>
#include <sstream>
#include <vector>

#include "toolbox/string.h"

#include "tcds/hwlayertca/BootstrapHelper.h"
#include "tcds/hwlayertca/TCACarrierBase.h"
#include "tcds/hwlayertca/TCACarrierFC7.h"
#include "tcds/ici/Utils.h"

tcds::ici::TCADeviceICI::TCADeviceICI() :
  TCADeviceBase(std::unique_ptr<tcds::hwlayertca::TCACarrierBase>(new tcds::hwlayertca::TCACarrierFC7(hwDevice_))),
  iciNumber_(0)
{
}

tcds::ici::TCADeviceICI::~TCADeviceICI()
{
}

unsigned short
tcds::ici::TCADeviceICI::iciNumber() const
{
  return iciNumber_;
}

void
tcds::ici::TCADeviceICI::setICINumber(unsigned short const iciNumber)
{
  iciNumber_ = iciNumber;
}

void
tcds::ici::TCADeviceICI::enableCPML1AFastTrack() const
{
  std::string const regName = "lpm_main.cpm_l1a_fast_track";
  hwDevice_.writeRegister(regName, 0x1);
}

void
tcds::ici::TCADeviceICI::enableCPMBRILDAQData() const
{
  hwDevice_.writeRegister("lpm_main.brildaq_sync_control.cpm_master", 0x1);
  hwDevice_.writeRegister("lpm_main.brildaq_sync_control.lpm_master", 0x0);
  hwDevice_.writeRegister("lpm_main.brildaq_sync_control.enable_backplane_data", 0x1);
}

void
tcds::ici::TCADeviceICI::enableTTSLinkAutoAlignMode() const
{
  hwDevice_.writeRegister("lpm_main.tts_serdes_ctrl.align_mode", 0x0);
}

void
tcds::ici::TCADeviceICI::configureTriggerAlignment() const
{
  // NOTE: This value is a firmware characteristic. It depends on the
  // implementation, but not on the build.
  std::string const regName = "main.int_fw_alignment_settings.cyclic_trigger_adjust";
  writeRegister(regName, 0x63);
}

void
tcds::ici::TCADeviceICI::configureBChannelAlignment() const
{
  // NOTE: This value is a firmware characteristic. It depends on the
  // implementation, but not on the build.
  std::string const regName = "main.int_fw_alignment_settings.bchannel_adjust";
  writeRegister(regName, 0x6);
}

void
tcds::ici::TCADeviceICI::stop() const
{
  // NOTE: This reset register auto-clears.
  std::string const regName = "main.resets.internal_bgo_stop";
  writeRegister(regName, 0x1);
}

void
tcds::ici::TCADeviceICI::enableTrigger() const
{
  std::string const regName = "main.inselect.trigger_enable";
  writeRegister(regName, 0x1);
}

void
tcds::ici::TCADeviceICI::disableTrigger() const
{
  std::string const regName = "main.inselect.trigger_enable";
  writeRegister(regName, 0x0);
}

void
tcds::ici::TCADeviceICI::enableCyclicGenerator(unsigned int const genNumber) const
{
  // First check if the number matches an existing generator.
  verifyCyclicGeneratorNumber(genNumber);

  // Now do as we were asked.
  std::string const regName = toolbox::toString("cyclic_generator%d.configuration.enabled",
                                                genNumber);
  writeRegister(regName, 1);
}

void
tcds::ici::TCADeviceICI::disableCyclicGenerator(unsigned int const genNumber) const
{
  // First check if the number matches an existing generator.
  verifyCyclicGeneratorNumber(genNumber);

  // Now do as we were asked.
  std::string const regName = toolbox::toString("cyclic_generator%d.configuration.enabled",
                                                genNumber);
  writeRegister(regName, 0);
}

std::string
tcds::ici::TCADeviceICI::regNamePrefixImpl() const
{
  // All the iCI registers start with 'iciX.', where 'X' is the iCI
  // number in the LPM (i.e., [1, 8]).
  std::stringstream regNamePrefix;
  regNamePrefix << "ici" << iciNumber() << ".";
  return regNamePrefix.str();
}

bool
tcds::ici::TCADeviceICI::bootstrapDoneImpl() const
{
  tcds::hwlayertca::BootstrapHelper h(*this);
  return h.bootstrapDone();
}

void
tcds::ici::TCADeviceICI::runBootstrapImpl() const
{
  tcds::hwlayertca::BootstrapHelper h(*this);
  h.runBootstrap();
  // Reset the backplane link to the CPM.
  hwDevice_.writeRegister("lpm_main.backplane_tts_control.gtx_reset", 0x0);
  hwDevice_.writeRegister("lpm_main.backplane_tts_control.gtx_reset", 0x1);
  hwDevice_.writeRegister("lpm_main.backplane_tts_control.gtx_reset", 0x0);
}

tcds::definitions::BCHANNEL_MODE
tcds::ici::TCADeviceICI::getBchannelMode(tcds::definitions::BGO_NUM const bchannelNumber) const
{
  tcds::definitions::BCHANNEL_MODE res = tcds::definitions::BCHANNEL_MODE_MISCONFIGURED;

  std::string const regNameBase =
    toolbox::toString("bchannels.bchannel%d.configuration.", bchannelNumber);
  bool const isSingle = readRegister(regNameBase + "single");
  bool const isDouble = readRegister(regNameBase + "double");

  if (!isSingle && !isDouble)
    {
      res = tcds::definitions::BCHANNEL_MODE_OFF;
    }
  else if (isSingle && isDouble)
    {
      res = tcds::definitions::BCHANNEL_MODE_MISCONFIGURED;
    }
  else if (isSingle)
    {
      res = tcds::definitions::BCHANNEL_MODE_SINGLE;
    }
  else if (isDouble)
    {
      res = tcds::definitions::BCHANNEL_MODE_DOUBLE;
    }

  return res;
}

void
tcds::ici::TCADeviceICI::sendL1A() const
{
  std::string const regName = "main.ipbus_requests";
  uint32_t const regVal = tcds::definitions::SOFTWARE_REQUEST_TYPE_L1A;
  writeRegister(regName, regVal);
}

void
tcds::ici::TCADeviceICI::enableBchannel(tcds::definitions::BGO_NUM const bgoNumber,
                                        tcds::definitions::BCHANNEL_MODE const bchannelMode) const
{
  // ASSERT ASSERT ASSERT
  assert ((bgoNumber >= tcds::definitions::kBgoNumMin) &&
          (bgoNumber <= tcds::definitions::kBgoNumMax));
  assert ((bchannelMode == tcds::definitions::BCHANNEL_MODE_SINGLE) ||
          (bchannelMode == tcds::definitions::BCHANNEL_MODE_DOUBLE));
  // ASSERT ASSERT ASSERT end

  std::string regNameBase = toolbox::toString("bchannels.bchannel%d.configuration", bgoNumber);
  std::string regName = "";
  if (bchannelMode == tcds::definitions::BCHANNEL_MODE_SINGLE)
    {
      writeRegister(regNameBase + ".single", 0x1);
      writeRegister(regNameBase + ".double", 0x0);
      writeRegister(regNameBase + ".block", 0x0);
    }
  else if (bchannelMode == tcds::definitions::BCHANNEL_MODE_DOUBLE)
    {
      writeRegister(regNameBase + ".single", 0x0);
      writeRegister(regNameBase + ".double", 0x1);
      writeRegister(regNameBase + ".block", 0x0);
    }
  else if (bchannelMode == tcds::definitions::BCHANNEL_MODE_BLOCK)
    {
      writeRegister(regNameBase + ".single", 0x0);
      writeRegister(regNameBase + ".double", 0x0);
      writeRegister(regNameBase + ".block", 0x1);
    }
}

void
tcds::ici::TCADeviceICI::disableBchannel(tcds::definitions::BGO_NUM const bgoNumber) const
{
  // ASSERT ASSERT ASSERT
  assert ((bgoNumber >= tcds::definitions::kBgoNumMin) &&
          (bgoNumber <= tcds::definitions::kBgoNumMax));
  // ASSERT ASSERT ASSERT end

  // Set both the 'single' and 'double' B-channel mode flags to 0 to
  // switch the channel off.
  std::string const regNameBase = toolbox::toString("bchannels.bchannel%d.configuration", bgoNumber);
  writeRegister(regNameBase + ".single", 0x0);
  writeRegister(regNameBase + ".double", 0x0);
  writeRegister(regNameBase + ".block", 0x0);
}

void
tcds::ici::TCADeviceICI::sendBgo(tcds::definitions::BGO_NUM const bgoNumber) const
{
  // ASSERT ASSERT ASSERT
  assert ((bgoNumber >= tcds::definitions::kBgoNumMin) &&
          (bgoNumber <= tcds::definitions::kBgoNumMax));
  // ASSERT ASSERT ASSERT end
  std::string const regName = "main.ipbus_requests";
  uint32_t const regVal = tcds::definitions::SOFTWARE_REQUEST_TYPE_BGO + bgoNumber;
  writeRegister(regName, regVal);
}

void
tcds::ici::TCADeviceICI::sendBCommand(tcds::definitions::BCOMMAND_TYPE const bcommandType,
                                      tcds::definitions::bcommand_data_t const data,
                                      tcds::definitions::bcommand_address_t const address,
                                      tcds::definitions::bcommand_address_t const subAddress,
                                      tcds::definitions::BCOMMAND_ADDRESS_TYPE const addressType) const
{
  // Bits [31:18]: TTCrx address.
  // Bit 17: 0->internal, 1->external addressing.
  // Bit 16: reserved, keep to 1.
  // Bits [15:8]: internal/external address.
  // Bits [7:0]: B-command data.

  // ASSERT ASSERT ASSERT
  assert ((data >= tcds::definitions::BCOMMAND_DATA_MIN) &&
          (data <= tcds::definitions::BCOMMAND_DATA_MAX));
  assert ((address >= tcds::definitions::BCOMMAND_ADDRESS_MIN) &&
          (address <= tcds::definitions::BCOMMAND_ADDRESS_MAX));
  assert ((subAddress >= tcds::definitions::BCOMMAND_SUBADDRESS_MIN) &&
          (subAddress <= tcds::definitions::BCOMMAND_SUBADDRESS_MAX));
  assert ((addressType == tcds::definitions::BCOMMAND_ADDRESS_TYPE_INTERNAL) ||
          (addressType == tcds::definitions::BCOMMAND_ADDRESS_TYPE_EXTERNAL));
  // ASSERT ASSERT ASSERT end

  std::string regName = "bchannels.main.ipbus_requests";
  uint32_t regVal = 0x0;
  switch (bcommandType)
    {
    case tcds::definitions::BCOMMAND_TYPE_BROADCAST:
      regName += ".bchannel_short";
      regVal = (data & 0xff);
      break;
    case tcds::definitions::BCOMMAND_TYPE_ADDRESSED:
      regName += ".bchannel_long";
      regVal = ((address & 0xfffc0000) << 18) +
        ((addressType == tcds::definitions::BCOMMAND_ADDRESS_TYPE_EXTERNAL) << 17) +
        0x10000 +
        ((subAddress & 0xff) << 8) +
        (data & 0xff);
      break;
    default:
      // ASSERT ASSERT ASSERT
      assert (false);
      // ASSERT ASSERT ASSERT end
      break;
    }

  writeRegister(regName, regVal);
}

void
tcds::ici::TCADeviceICI::initCyclicGenerator(unsigned int const genNumber) const
{
  // First check if the number matches an existing generator.
  verifyCyclicGeneratorNumber(genNumber);

  // Any write to this register will trigger the initialisation of
  // (only) the specified cyclic generator.
  std::string const regName = toolbox::toString("main.resets.cyclic_generators_init.generator%d",
                                                genNumber);
  writeRegister(regName, 0x1);
}

void
tcds::ici::TCADeviceICI::initCyclicGenerators() const
{
  initCyclicGeneratorsAndBchannels();
}

void
tcds::ici::TCADeviceICI::initCyclicGeneratorsAndBchannels() const
{
  // Any write to this register will trigger the initialisation of the
  // cyclic generators.
  // NOTE: This also resets the B-channels so they start from the
  // first entry.
  writeRegister("main.resets.cyclic_generators_and_bchannels_init", 0x1);
}

void
tcds::ici::TCADeviceICI::resetCounters() const
{
  // NOTE: This resets all counters, except for the orbit counter. The
  // orbit counter itself is reset (only) by the OC0 B-go.
  std::vector<std::string> counterResetRegs;
  counterResetRegs.push_back("main.resets.event_counter_reset");
  counterResetRegs.push_back("main.resets.bgo_counter_reset");
  counterResetRegs.push_back("main.resets.trigger_rules_cancel_counter_reset");
  counterResetRegs.push_back("main.resets.bchannels_counters_reset");

  for (std::vector<std::string>::const_iterator i = counterResetRegs.begin();
       i != counterResetRegs.end();
       ++i)
    {
      writeRegister(*i, 0x1);
    }
}

void
tcds::ici::TCADeviceICI::resetStatus() const
{
  // NOTE: Any write to this register will clear all latched status
  // bits.
  writeRegister("main.ici_status", 0x0);
}

tcds::hwlayer::DeviceBase::RegContentsVec
tcds::ici::TCADeviceICI::readHardwareConfigurationImpl(tcds::hwlayer::RegisterInfo::RegInfoVec const& regInfos) const
{
  tcds::hwlayer::DeviceBase::RegContentsVec res =
    tcds::hwlayertca::TCADeviceBase::readHardwareConfigurationImpl(regInfos);

  // This bit filters out all B-channel RAM entries _after_ the entry
  // that has the end-of-sequence bit set in the control word.
  // BUG BUG BUG
  // Magic number in the wrong place here!
  uint32_t const kBChannelRAMEndOfSequence = 0x4;
  // BUG BUG BUG end
  for (tcds::hwlayer::DeviceBase::RegContentsVec::iterator i = res.begin();
       i != res.end();
       ++i)
    {
      std::string const name = i->first.name();
      std::vector<uint32_t>& vals = i->second;
      if ((name.compare(name.size() - 4, 4, ".ram") == 0) &&
          (name.find("bchannel") != std::string::npos))
        {
          std::vector<uint32_t>::iterator i = vals.begin();
          while (i < vals.end())
            {
              if ((*(i+1) & kBChannelRAMEndOfSequence) != 0x0)
                {
                  break;
                }
              i += 2;
            }
          if (i < vals.end())
            {
              vals.erase(i + 2, vals.end());
            }
        }
    }

  return res;
}

std::vector<uint32_t>
tcds::ici::TCADeviceICI::readBchannelRAM(tcds::definitions::BGO_NUM const bchannelNumber) const
{
  std::string const regName = toolbox::toString("bchannels.bchannel%d.ram", bchannelNumber);
  std::vector<uint32_t> res = readBlock(regName);
  return res;
}
