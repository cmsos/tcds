#include "tcds/ici/HwStatusInfoSpaceHandler.h"

tcds::ici::HwStatusInfoSpaceHandler::HwStatusInfoSpaceHandler(xdaq::Application& xdaqApp,
                                                              tcds::utils::InfoSpaceUpdater* updater) :
  HwStatusInfoSpaceHandlerTCA(xdaqApp, "tcds-hw-status-ici", updater)
{
}

tcds::ici::HwStatusInfoSpaceHandler::~HwStatusInfoSpaceHandler()
{
}
