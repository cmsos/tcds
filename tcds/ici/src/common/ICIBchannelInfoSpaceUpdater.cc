#include "tcds/ici/ICIBchannelInfoSpaceUpdater.h"

#include <sstream>
#include <cstddef>
#include <stdint.h>
#include <vector>

#include "toolbox/string.h"

#include "tcds/ici/BchannelRAMContents.h"
#include "tcds/ici/TCADeviceICI.h"
#include "tcds/utils/Definitions.h"
#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Utils.h"

tcds::ici::ICIBchannelInfoSpaceUpdater::ICIBchannelInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                                                    TCADeviceICI const& hw) :
  tcds::utils::HwInfoSpaceUpdaterBase(xdaqApp, hw)
{
}

tcds::ici::ICIBchannelInfoSpaceUpdater::~ICIBchannelInfoSpaceUpdater()
{
}

bool
tcds::ici::ICIBchannelInfoSpaceUpdater::updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                                            tcds::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  bool updated = false;
  tcds::ici::TCADeviceICI const& hw = getHw();
  if (hw.isHwConnected())
    {
      std::string name = item.name();
      tcds::utils::InfoSpaceItem::UpdateType updateType = item.updateType();
      if (updateType == tcds::utils::InfoSpaceItem::PROCESS)
        {
          // The 'PROCESS' update type means that there is something
          // special to the variable. Figure out what to do based on the
          // variable name.
          if (toolbox::endsWith(name, ".mode"))
            {
              tcds::definitions::BGO_NUM bchannelNumber = extractBchannelNumber(name);
              tcds::definitions::BCHANNEL_MODE bchannelMode = hw.getBchannelMode(bchannelNumber);
              uint32_t const newVal = bchannelMode;
              infoSpaceHandler->setUInt32(name, newVal);
              updated = true;
            }
          else if (toolbox::endsWith(name, ".ram"))
            {
              std::vector<uint32_t> const ramContentsRaw = hw.readBlock(name);
              BchannelRAMContents const ramContents(ramContentsRaw);
              std::string const newVal = ramContents.getJSONString();
              infoSpaceHandler->setString(name, newVal);
              updated = true;
            }
        }
      if (!updated)
        {
          updated = tcds::utils::HwInfoSpaceUpdaterBase::updateInfoSpaceItem(item, infoSpaceHandler);
        }
    }

  if (updated)
    {
      item.setValid();
    }
  else
    {
      item.setInvalid();
    }

  return updated;
}

tcds::ici::TCADeviceICI const&
tcds::ici::ICIBchannelInfoSpaceUpdater::getHw()
{
  return dynamic_cast<tcds::ici::TCADeviceICI const&>(tcds::utils::HwInfoSpaceUpdaterBase::getHw());

}

tcds::definitions::BGO_NUM
tcds::ici::ICIBchannelInfoSpaceUpdater::extractBchannelNumber(std::string const& regName) const
{
  // TODO TODO TODO
  // This could be done in a nicer way.
  std::string const patLo = "bchannels.bchannel";
  size_t const posLo = regName.find(patLo);
  std::string const patHi = ".";
  size_t const posHi = regName.find(patHi, posLo + patLo.size());
  std::stringstream tmp;
  tmp << regName.substr(posLo + patLo.size(), posHi);
  int bchannelNumTmp;
  tmp >> bchannelNumTmp;
  tcds::definitions::BGO_NUM bchannelNumber =
    static_cast<tcds::definitions::BGO_NUM>(bchannelNumTmp);
  // TODO TODO TODO end
  return bchannelNumber;
}
