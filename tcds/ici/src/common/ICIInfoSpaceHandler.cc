#include "tcds/ici/ICIInfoSpaceHandler.h"

#include "tcds/utils/Monitor.h"
#include "tcds/utils/WebServer.h"

tcds::ici::ICIInfoSpaceHandler::ICIInfoSpaceHandler(xdaq::Application& xdaqApp,
                                                    tcds::utils::InfoSpaceUpdater* updater) :
  InfoSpaceHandler(xdaqApp, "tcds-icicontroller-main", updater)
{
  // The overall trigger enable/disable flag.
  createBool("main.inselect.trigger_enable", false, "true/false");

  // Trigger sources.
  createBool("main.inselect.sequence_trigger_generator_enable", false, "enabled/disabled");
  createBool("main.inselect.exclusive_cpm_trigger_enable", false, "enabled/disabled");
  createBool("main.inselect.exclusive_lpm_ipm1_trigger_enable", false, "enabled/disabled");
  createBool("main.inselect.exclusive_lpm_ipm2_trigger_enable", false, "enabled/disabled");
  createBool("main.inselect.combined_bchannel_trigger_enable", false, "enabled/disabled");
  createBool("main.inselect.combined_software_trigger_enable", false, "enabled/disabled");
  // createBool("main.inselect.combined_l1a_in0_trigger_enable", false, "enabled/disabled");
  // createBool("main.inselect.combined_l1a_in1_trigger_enable", false, "enabled/disabled");
  createBool("main.inselect.combined_cyclic_trigger_enable", false, "enabled/disabled");
  createBool("main.inselect.combined_trigger_generator_trigger_enable", false, "enabled/disabled");
  createBool("main.inselect.combined_bunch_trigger_generator_trigger_enable", false, "enabled/disabled");
  createBool("main.inselect.combined_achannel_trigger_enable", false, "enabled/disabled");

  // B-go sources.
  createBool("main.inselect.exclusive_cpm_bgo_enable", false, "enabled/disabled");
  createBool("main.inselect.exclusive_lpm_ipm1_bgo_enable", false, "enabled/disabled");
  createBool("main.inselect.exclusive_lpm_ipm2_bgo_enable", false, "enabled/disabled");
  createBool("main.inselect.combined_software_bgo_enable", false, "enabled/disabled");
  createBool("main.inselect.combined_cyclic_bgo_enable", false, "enabled/disabled");

  // Orbit/BC0 sources.
  createBool("main.inselect.cpm_orbit_select", false, "enabled/disabled");
  createBool("main.inselect.lpm_ipm1_orbit_select", false, "enabled/disabled");
  createBool("main.inselect.lpm_ipm2_orbit_select", false, "enabled/disabled");
  createBool("main.inselect.lemo_orbit_select", false, "enabled/disabled");
}

tcds::ici::ICIInfoSpaceHandler::~ICIInfoSpaceHandler()
{
}

void
tcds::ici::ICIInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  // Trigger sources.
  monitor.newItemSet("itemset-triggerinputs");
  monitor.addItem("itemset-triggerinputs",
                  "main.inselect.trigger_enable",
                  "Trigger enabled",
                  this);
  monitor.addItem("itemset-triggerinputs",
                  "main.inselect.exclusive_cpm_trigger_enable",
                  "Trigger source: CPM",
                  this);
  monitor.addItem("itemset-triggerinputs",
                  "main.inselect.exclusive_lpm_ipm1_trigger_enable",
                  "Trigger source: LPM - iPM1",
                  this);
  monitor.addItem("itemset-triggerinputs",
                  "main.inselect.exclusive_lpm_ipm2_trigger_enable",
                  "Trigger source: LPM - iPM2",
                  this);
  // monitor.addItem("itemset-triggerinputs",
  //                 "main.inselect.combined_l1a_in0_trigger_enable",
  //                 "Trigger source: front-panel input 0",
  //                 this);
  // monitor.addItem("itemset-triggerinputs",
  //                 "main.inselect.combined_l1a_in1_trigger_enable",
  //                 "Trigger source: front-panel input 1",
  //                 this);
  monitor.addItem("itemset-triggerinputs",
                  "main.inselect.combined_software_trigger_enable",
                  "Trigger source: IPbus/SOAP",
                  this);
  monitor.addItem("itemset-triggerinputs",
                  "main.inselect.combined_cyclic_trigger_enable",
                  "Trigger source: cyclic generators",
                  this);
  monitor.addItem("itemset-triggerinputs",
                  "main.inselect.combined_trigger_generator_trigger_enable",
                  "Trigger source: trigger-generator",
                  this);
  monitor.addItem("itemset-triggerinputs",
                  "main.inselect.combined_bunch_trigger_generator_trigger_enable",
                  "Trigger source: bunch-trigger",
                  this);
  monitor.addItem("itemset-triggerinputs",
                  "main.inselect.sequence_trigger_generator_enable",
                  "Trigger source: sequence-generator",
                  this);
  monitor.addItem("itemset-triggerinputs",
                  "main.inselect.combined_achannel_trigger_enable",
                  "Trigger source: A-channel",
                  this);
  monitor.addItem("itemset-triggerinputs",
                  "main.inselect.combined_bchannel_trigger_enable",
                  "Trigger source: B-channel",
                  this);

  // B-go sources.
  monitor.newItemSet("itemset-bgoinputs");
  monitor.addItem("itemset-bgoinputs",
                  "main.inselect.exclusive_cpm_bgo_enable",
                  "B-go source: CPM",
                  this);
  monitor.addItem("itemset-bgoinputs",
                  "main.inselect.exclusive_lpm_ipm1_bgo_enable",
                  "B-go source: LPM - iPM1",
                  this);
  monitor.addItem("itemset-bgoinputs",
                  "main.inselect.exclusive_lpm_ipm2_bgo_enable",
                  "B-go source: LPM - iPM2",
                  this);
  monitor.addItem("itemset-bgoinputs",
                  "main.inselect.combined_software_bgo_enable",
                  "B-go source: IPbus/SOAP",
                  this);
  monitor.addItem("itemset-bgoinputs",
                  "main.inselect.combined_cyclic_bgo_enable",
                  "B-go source: cyclic generators",
                  this);

  // Orbit/BC0 sources.
  monitor.newItemSet("itemset-orbitinputs");
  monitor.addItem("itemset-orbitinputs",
                  "main.inselect.cpm_orbit_select",
                  "Orbit source: CPM",
                  this);
  monitor.addItem("itemset-orbitinputs",
                  "main.inselect.lpm_ipm1_orbit_select",
                  "Orbit source: LPM - iPM1",
                  this);
  monitor.addItem("itemset-orbitinputs",
                  "main.inselect.lpm_ipm2_orbit_select",
                  "Orbit source: LPM - iPM2",
                  this);
  monitor.addItem("itemset-orbitinputs",
                  "main.inselect.lemo_orbit_select",
                  "Orbit source: lemo from TTC-mi",
                  this);
}

void
tcds::ici::ICIInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                              tcds::utils::Monitor& monitor,
                                                              std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "Input sources" : forceTabName;

  webServer.registerTab(tabName,
                        "iCI input selection",
                        3);
  webServer.registerTable("Trigger inputs",
                          "Trigger source selection",
                          monitor,
                          "itemset-triggerinputs",
                          tabName);
  webServer.registerTable("B-go inputs",
                          "B-go source selection",
                          monitor,
                          "itemset-bgoinputs",
                          tabName);
  webServer.registerTable("Orbit inputs",
                          "Orbit/BC0 source selection. "
                          "If no source is enabled, "
                          "the internal orbit generator will be used.",
                          monitor,
                          "itemset-orbitinputs",
                          tabName);
}
