#include "tcds/ici/ICIBchannelInfoSpaceHandler.h"

#include <stdint.h>

#include "toolbox/string.h"

#include "tcds/ici/Definitions.h"
#include "tcds/ici/Utils.h"
#include "tcds/ici/WebTableBchannelRAM.h"
#include "tcds/utils/Definitions.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/Utils.h"
#include "tcds/utils/WebServer.h"

tcds::ici::ICIBchannelInfoSpaceHandler::ICIBchannelInfoSpaceHandler(xdaq::Application& xdaqApp,
                                                                    tcds::utils::InfoSpaceUpdater* updater) :
  InfoSpaceHandler(xdaqApp, "tcds-icicontroller-bchannels", updater)
{
  for (unsigned int bchannelNum = tcds::definitions::kBchannelNumMin;
       bchannelNum <= tcds::definitions::kBchannelNumMax;
       ++bchannelNum)
    {
      // Configuration items.
      std::string nameBase =
        toolbox::toString("bchannels.bchannel%d.configuration", bchannelNum);
      createUInt32(nameBase + ".mode", 0, "", tcds::utils::InfoSpaceItem::PROCESS);
      createUInt32(nameBase + ".bx_sync");
      createUInt32(nameBase + ".bx_or_delay");
      createUInt32(nameBase + ".prescale");
      createUInt32(nameBase + ".initial_prescale");
      createUInt32(nameBase + ".postscale");
      createBool(nameBase + ".repeat_cycle",
                 "",
                 "true/false");
      createUInt32(nameBase + ".double_bcommand_delay");

      // RAM.
      createString(toolbox::toString("bchannels.bchannel%d.ram", bchannelNum));

      // Status items.
      nameBase = toolbox::toString("bchannels.bchannel%d.status", bchannelNum);
      createUInt32(nameBase + ".bgo_counter");
      createUInt32(nameBase + ".bgo_when_busy_counter");
      createUInt32(nameBase + ".cancel_counter");
      createBool(nameBase + ".sequence_has_ended",
                 "",
                 "true/false");
    }
}

void
tcds::ici::ICIBchannelInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  for (unsigned int bchannelNum = tcds::definitions::kBchannelNumMin;
       bchannelNum <= tcds::definitions::kBchannelNumMax;
       ++bchannelNum)
    {
      // B-channel configuration settings.
      std::string itemSetName = toolbox::toString("itemset-bchannel%d-config", bchannelNum);
      monitor.newItemSet(itemSetName);
      std::string nameBase =
        toolbox::toString("bchannels.bchannel%d.configuration", bchannelNum);
      monitor.addItem(itemSetName,
                      nameBase + ".mode",
                      "Emission mode",
                      this);
      monitor.addItem(itemSetName,
                      nameBase + ".bx_sync",
                      "Timing mode",
                      this);
      monitor.addItem(itemSetName,
                      nameBase + ".bx_or_delay",
                      "Timing setting (BX/orbit)",
                      this,
                      "For BX-synchronous mode this setting is specified in BX."
                      "For delay mode this setting is specified in orbits.");
      monitor.addItem(itemSetName,
                      nameBase + ".prescale",
                      "Prescale",
                      this);
      monitor.addItem(itemSetName,
                      nameBase + ".initial_prescale",
                      "Initial prescale",
                      this);
      monitor.addItem(itemSetName,
                      nameBase + ".postscale",
                      "Postscale",
                      this);
      monitor.addItem(itemSetName,
                      nameBase + ".repeat_cycle",
                      "Repeat cycle?",
                      this);
      monitor.addItem(itemSetName,
                      nameBase + ".double_bcommand_delay",
                      "Double B-command inter-command delay (BX)",
                      this);

      // B-channel RAM contents.
      itemSetName = toolbox::toString("itemset-bchannel%d-ram", bchannelNum);
      monitor.newItemSet(itemSetName);
      monitor.addItem(itemSetName,
                      toolbox::toString("bchannels.bchannel%d.ram", bchannelNum),
                      "RAM contents",
                      this);

      // B-channel status.
      itemSetName = toolbox::toString("itemset-bchannel%d-status", bchannelNum);
      monitor.newItemSet(itemSetName);
      nameBase = toolbox::toString("bchannels.bchannel%d.status", bchannelNum);
      monitor.addItem(itemSetName,
                      nameBase + ".bgo_counter",
                      "# Requested",
                      this);
      monitor.addItem(itemSetName,
                      nameBase + ".bgo_when_busy_counter",
                      "# Requested while busy",
                      this);
      monitor.addItem(itemSetName,
                      nameBase + ".cancel_counter",
                      "# Cancelled",
                      this);
      monitor.addItem(itemSetName,
                      nameBase + ".sequence_has_ended",
                      "RAM entries finished",
                      this);
    }
}

void
tcds::ici::ICIBchannelInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                      tcds::utils::Monitor& monitor,
                                                                      std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "B-channels" : forceTabName;

  webServer.registerTab(tabName,
                        "B-channel configuration information",
                        4);

  std::string name;
  std::string desc = "";
  for (unsigned int bchannelNum = tcds::definitions::kBchannelNumMin;
       bchannelNum <= tcds::definitions::kBchannelNumMax;
       ++bchannelNum)
    {
      std::string bchannelNameString =
        tcds::utils::formatBchannelNameString(static_cast<tcds::definitions::BGO_NUM>(bchannelNum));
      name = toolbox::toString("%s configuration", bchannelNameString.c_str());
      webServer.registerTable(name,
                              desc,
                              monitor,
                              toolbox::toString("itemset-bchannel%d-config", bchannelNum),
                              tabName);
      name = toolbox::toString("%s RAM contents", bchannelNameString.c_str());
      webServer.registerWebObject<WebTableBchannelRAM>(name,
                                                       desc,
                                                       monitor,
                                                       toolbox::toString("itemset-bchannel%d-ram", bchannelNum),
                                                       tabName,
                                                       2);
      name = toolbox::toString("%s status", bchannelNameString.c_str());
      webServer.registerTable(name,
                              desc,
                              monitor,
                              toolbox::toString("itemset-bchannel%d-status", bchannelNum),
                              tabName);
    }
}

std::string
tcds::ici::ICIBchannelInfoSpaceHandler::formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const
{
  std::string res = tcds::utils::escapeAsJSONString(tcds::utils::InfoSpaceHandler::kInvalidItemString);
  if (item->isValid())
    {
      std::string name = item->name();
      if (toolbox::endsWith(name, ".mode"))
        {
          uint32_t const value = getUInt32(name);
          tcds::definitions::BCHANNEL_MODE const valueEnum =
            static_cast<tcds::definitions::BCHANNEL_MODE>(value);
          res = tcds::utils::escapeAsJSONString(tcds::utils::bchannelModeToString(valueEnum));
        }
      else if (toolbox::endsWith(name, ".bx_sync"))
        {
          uint32_t const value = getUInt32(name);
          tcds::definitions::BCHANNEL_TIMING_MODE const valueEnum =
            static_cast<tcds::definitions::BCHANNEL_TIMING_MODE>(value);
          res = tcds::utils::escapeAsJSONString(tcds::ici::BchannelTimingModeToString(valueEnum));
        }
      else if (toolbox::endsWith(name, ".ram"))
        {
          // Special in the sense that this is something that needs to
          // be interpreted as a proper JavaScript object, so let's
          // not add any more double quotes.
          res = getString(name);
        }
      else
        {
          // For everything else simply call the generic formatter.
          res = InfoSpaceHandler::formatItem(item);
        }
    }
  return res;
}
