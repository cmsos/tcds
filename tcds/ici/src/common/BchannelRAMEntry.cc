#include "tcds/ici/BchannelRAMEntry.h"

#include "tcds/utils/Utils.h"

tcds::ici::BchannelRAMEntry::BchannelRAMEntry(uint32_t const ctrlWord, uint32_t const dataWord) :
  ctrlWord_(ctrlWord),
  dataWord_(dataWord)
{
}

uint32_t
tcds::ici::BchannelRAMEntry::ctrlWord() const
{
  return ctrlWord_;
}

uint32_t
tcds::ici::BchannelRAMEntry::dataWord() const
{
  return dataWord_;
}

bool
tcds::ici::BchannelRAMEntry::isDoNotEmit() const
{
  return ((ctrlWord_ & ctrlWordMaskDoNotEmit_) != 0);
}

bool
tcds::ici::BchannelRAMEntry::isEndOfSequence() const
{
  return ((ctrlWord_ & ctrlWordMaskEndOfSequence_) != 0);
}

bool
tcds::ici::BchannelRAMEntry::isAchannelCommand() const
{
  return (!isDoNotEmit() &&
          ((ctrlWord_ & ctrlWordMaskCommandType_) != 0));
}

bool
tcds::ici::BchannelRAMEntry::isBchannelCommand() const
{
  return (!isDoNotEmit() &&
          !isAchannelCommand());
}

bool
tcds::ici::BchannelRAMEntry::isBchannelCommandShort() const
{
  return (isBchannelCommand() &&
          !isBchannelCommandLong());
}

bool
tcds::ici::BchannelRAMEntry::isBchannelCommandLong() const
{
  return (isBchannelCommand() &&
          ((ctrlWord_ & ctrlWordMaskBcommandType_) != 0));
}

uint16_t
tcds::ici::BchannelRAMEntry::dynamicPostscale() const
{
  unsigned int numTrailingZeros = tcds::utils::countTrailingZeros(ctrlWordMaskDynamicPostscale_);
  return ((ctrlWord_ & ctrlWordMaskDynamicPostscale_) >> numTrailingZeros);
}

uint8_t
tcds::ici::BchannelRAMEntry::bcommandData() const
{
  unsigned int numTrailingZeros = tcds::utils::countTrailingZeros(dataWordMaskBcommandData_);
  return ((dataWord_ & dataWordMaskBcommandData_) >> numTrailingZeros);
}

uint16_t
tcds::ici::BchannelRAMEntry::bcommandAddress() const
{
  unsigned int numTrailingZeros = tcds::utils::countTrailingZeros(dataWordMaskBcommandAddress_);
  return ((dataWord_ & dataWordMaskBcommandAddress_) >> numTrailingZeros);
}

uint8_t
tcds::ici::BchannelRAMEntry::bcommandSubaddress() const
{
  unsigned int numTrailingZeros = tcds::utils::countTrailingZeros(dataWordMaskBcommandSubaddress_);
  return ((dataWord_ & dataWordMaskBcommandSubaddress_) >> numTrailingZeros);
}

bool
tcds::ici::BchannelRAMEntry::bcommandIsAddressExternal() const
{
  return ((dataWord_ & dataWordMaskBcommandIsAddressExternal_) != 0);
}

bool
tcds::ici::BchannelRAMEntry::bcommandIsReservedBit16Correct() const
{
  // This bit 16 should be set to 1, according to the TTC protocol.
  return ((dataWord_ & dataWordMaskBcommandIsReservedBit16Correct_) != 0);
}
