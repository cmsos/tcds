#include "tcds/ici/ICICountersInfoSpaceHandler.h"

#include <cstddef>
#include <string>

#include "toolbox/string.h"

#include "tcds/ici/Definitions.h"
#include "tcds/utils/Definitions.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/Utils.h"
#include "tcds/utils/WebServer.h"

tcds::ici::ICICountersInfoSpaceHandler::ICICountersInfoSpaceHandler(xdaq::Application& xdaqApp,
                                                                    tcds::utils::InfoSpaceUpdater* updater) :
  InfoSpaceHandler(xdaqApp, "tcds-ici-counters", updater)
{
  // Trigger counter.
  // NOTE: This is the counter that does _not_ reset upon EC0.
  createUInt32("main.total_event_counter");
  // NOTE: This is the counter that _does_ reset upon EC0.
  createUInt32("main.event_counter");

  // Orbit counter.
  createUInt32("main.orbit_counter");

  // All the B-channel counters.
  for (int bgoNum = tcds::definitions::kBgoNumOriMin;
       bgoNum <= tcds::definitions::kBgoNumOriMax;
       ++bgoNum)
    {
      std::string const nameBase =
        toolbox::toString("bchannels.bchannel%d.status", bgoNum);
      createUInt32(nameBase + ".bgo_counter");
      createUInt32(nameBase + ".bgo_when_busy_counter");
      createUInt32(nameBase + ".cancel_counter");
    }

  // All the multi-B-channel counters.
  for (unsigned int multiBchannelNum = tcds::definitions::kMultiBchannelNumMin;
       multiBchannelNum <= tcds::definitions::kMultiBchannelNumMax;
       ++multiBchannelNum)
    {
      std::string const nameBase =
        toolbox::toString("multibchannels.multibchannel%d.status", multiBchannelNum);

      for (unsigned int bgoNumInt = tcds::definitions::kBchannelInMultiBchannelNumMin;
           bgoNumInt <= tcds::definitions::kBchannelInMultiBchannelNumMax;
           ++bgoNumInt)
        {
          int const bgoNum = (tcds::definitions::kBchannelInMultiBchannelNumMax - tcds::definitions::kBchannelInMultiBchannelNumMin + 1) * multiBchannelNum + (tcds::definitions::kBgoNumOriMax - tcds::definitions::kBgoNumOriMin + 1) + bgoNumInt;
          createUInt32(nameBase + toolbox::toString(".bgo_counters.bgo%d", bgoNum));
          createUInt32(nameBase + toolbox::toString(".bgo_when_busy_counters.bgo%d", bgoNum));
          createUInt32(nameBase + toolbox::toString(".cancel_counters.bgo%d", bgoNum));
        }
    }
}

void
tcds::ici::ICICountersInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  // Trigger counter.
  monitor.newItemSet("itemset-trigger-counter");
  monitor.addItem("itemset-trigger-counter",
                  "main.total_event_counter",
                  "Total # L1As in the current run",
                  this,
                  "This counter resets upon OC0 and on explicit software request");
  monitor.addItem("itemset-trigger-counter",
                  "main.event_counter",
                  "# L1As since the last EC0",
                  this,
                  "This counter resets upon EC0 and on explicit software request");

  // Orbit counter.
  monitor.newItemSet("itemset-orbit-counter");
  monitor.addItem("itemset-orbit-counter",
                  "main.orbit_counter",
                  "Orbit counter",
                  this,
                  "NOTE: The orbit counter is reset only by the OC0 B-go, "
                  "and not in the ICIController 'Zeroing' transition. "
                  "This means that in a run the orbit counter "
                  "will be slightly lower than the BC0 count. "
                  "(The BC0 counter is reset only in the 'Zeroing' transition.)");

  // All the B-channel counters.
  for (int bgoNum = tcds::definitions::kBgoNumOriMin;
       bgoNum <= tcds::definitions::kBgoNumOriMax;
       ++bgoNum)
    {
      std::string const itemSetName =
        toolbox::toString("itemset-bgo%d-counters", bgoNum);
      monitor.newItemSet(itemSetName);
      std::string const nameBase =
        toolbox::toString("bchannels.bchannel%d.status", bgoNum);
      monitor.addItem(itemSetName,
                      nameBase + ".bgo_counter",
                      toolbox::toString("# B-go %d requested", bgoNum),
                      this,
                      "The number of B-gos (mapping to this B-channel)"
                      " requested since the last 'Zeroing' transition.");
      monitor.addItem(itemSetName,
                      nameBase + ".bgo_when_busy_counter",
                      toolbox::toString("# B-go %d requested while busy", bgoNum),
                      this);
      monitor.addItem(itemSetName,
                      nameBase + ".cancel_counter",
                      toolbox::toString("# B-go cancelled", bgoNum),
                      this);
    }

  // All the multi-B-channel counters.
  for (unsigned int multiBchannelNum = tcds::definitions::kMultiBchannelNumMin;
       multiBchannelNum <= tcds::definitions::kMultiBchannelNumMax;
       ++multiBchannelNum)
    {
      std::string const nameBase =
        toolbox::toString("multibchannels.multibchannel%d.status", multiBchannelNum);

      std::string itemSetName =
        toolbox::toString("itemset-multibchannel%d-requestcounters-extra", multiBchannelNum);
      monitor.newItemSet(itemSetName);
      for (unsigned int bgoNumInt = tcds::definitions::kBchannelInMultiBchannelNumMin;
           bgoNumInt <= tcds::definitions::kBchannelInMultiBchannelNumMax;
           ++bgoNumInt)
        {
          int const bgoNum = (tcds::definitions::kBchannelInMultiBchannelNumMax - tcds::definitions::kBchannelInMultiBchannelNumMin + 1) * multiBchannelNum + (tcds::definitions::kBgoNumOriMax - tcds::definitions::kBgoNumOriMin + 1) + bgoNumInt;
          monitor.addItem(itemSetName,
                          nameBase + toolbox::toString(".bgo_counters.bgo%d", bgoNum),
                          toolbox::toString("# B-go %d requested", bgoNum),
                          this,
                          "The number of B-gos (mapping to this multi-B-channel)"
                          " requested since the last 'Zeroing' transition.");
        }
      itemSetName = toolbox::toString("itemset-multibchannel%d-cancelcounters-extra", multiBchannelNum);
      monitor.newItemSet(itemSetName);
      for (unsigned int bgoNumInt = tcds::definitions::kBchannelInMultiBchannelNumMin;
           bgoNumInt <= tcds::definitions::kBchannelInMultiBchannelNumMax;
           ++bgoNumInt)
        {
          int const bgoNum = (tcds::definitions::kBchannelInMultiBchannelNumMax - tcds::definitions::kBchannelInMultiBchannelNumMin + 1) * multiBchannelNum + (tcds::definitions::kBgoNumOriMax - tcds::definitions::kBgoNumOriMin + 1) + bgoNumInt;
          monitor.addItem(itemSetName,
                          nameBase + toolbox::toString(".bgo_when_busy_counters.bgo%d", bgoNum),
                          toolbox::toString("# B-go %d requested while busy", bgoNum),
                          this);
          monitor.addItem(itemSetName,
                          nameBase + toolbox::toString(".cancel_counters.bgo%d", bgoNum),
                          toolbox::toString("# B-go %d cancelled", bgoNum),
                          this);
        }
    }
}

void
tcds::ici::ICICountersInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                      tcds::utils::Monitor& monitor,
                                                                      std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "Counters" : forceTabName;
  size_t const colSpan = 12;

  webServer.registerTab(tabName,
                        "All kinds of counters",
                        colSpan);

  webServer.registerTable("Trigger counters",
                          "",
                          monitor,
                          "itemset-trigger-counter",
                          tabName,
                          colSpan);

  webServer.registerTable("Orbit counter",
                          "",
                          monitor,
                          "itemset-orbit-counter",
                          tabName,
                          colSpan);

  for (int bgoNum = tcds::definitions::kBgoNumOriMin;
       bgoNum <= tcds::definitions::kBgoNumOriMax;
       ++bgoNum)
    {
      std::string const itemSetName =
        toolbox::toString("itemset-bgo%d-counters", bgoNum);
      std::string bgoNameString =
        tcds::utils::formatBgoNameString(static_cast<tcds::definitions::BGO_NUM>(bgoNum));
      webServer.registerTable(toolbox::toString("B-channel %d counters", bgoNum),
                              bgoNameString,
                              monitor,
                              itemSetName,
                              tabName,
                              3);
    }

  for (unsigned int multiBchannelNum = tcds::definitions::kMultiBchannelNumMin;
       multiBchannelNum <= tcds::definitions::kMultiBchannelNumMax;
       ++multiBchannelNum)
    {
      unsigned int const numBchannels =
        tcds::definitions::kBchannelNumMax - tcds::definitions::kBchannelNumMin + 1;
      unsigned int const numBchannelsPerMultiBchannel =
        tcds::definitions::kBchannelInMultiBchannelNumMax - tcds::definitions::kBchannelInMultiBchannelNumMin + 1;
      unsigned int const bgoMin = numBchannels + (multiBchannelNum * numBchannelsPerMultiBchannel);
      unsigned int const bgoMax = numBchannels + ((multiBchannelNum + 1) * numBchannelsPerMultiBchannel - 1);
      std::string const multiBGoNameString = toolbox::toString("B-gos %d to %d", bgoMin, bgoMax);

      std::string itemSetName =
        toolbox::toString("itemset-multibchannel%d-requestcounters-extra", multiBchannelNum);
      webServer.registerTable(toolbox::toString("Multi-B-channel %d request counters", multiBchannelNum),
                              multiBGoNameString,
                              monitor,
                              itemSetName,
                              tabName,
                              2);
      itemSetName = toolbox::toString("itemset-multibchannel%d-cancelcounters-extra", multiBchannelNum);
      webServer.registerTable(toolbox::toString("Multi-B-channel %d cancellation counters", multiBchannelNum),
                              multiBGoNameString,
                              monitor,
                              itemSetName,
                              tabName,
                              2);
    }
}
