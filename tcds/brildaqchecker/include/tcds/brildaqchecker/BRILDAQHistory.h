#ifndef _tcds_brildaqchecker_BRILDAQHistory_h_
#define _tcds_brildaqchecker_BRILDAQHistory_h_

#include <string>
#include <vector>

#include "xdata/Table.h"

namespace tcds {
  namespace brildaqchecker {

    class BRILDAQHistory
    {

    public:
      BRILDAQHistory(std::vector<xdata::Table> const& dataIn);
      ~BRILDAQHistory();

      std::string getJSONString() const;

    private:
      // This constructor is not supposed to be used.
      BRILDAQHistory();

      // These are the raw entries.
      std::vector<xdata::Table> nibbles_;

    };

  } // namespace brildaqchecker
} // namespace tcds

#endif // _tcds_brildaqchecker_BRILDAQHistory_h_
