#ifndef _tcds_brildaqchecker_BRILDAQDataInfoSpaceUpdater_h_
#define _tcds_brildaqchecker_BRILDAQDataInfoSpaceUpdater_h_

#include "tcds/utils/InfoSpaceUpdater.h"

namespace tcds {
  namespace utils {
    class InfoSpaceHandler;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace brildaqchecker {

    class BRILDAQListener;

    class BRILDAQDataInfoSpaceUpdater : public tcds::utils::InfoSpaceUpdater
    {

    public:
      BRILDAQDataInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                  tcds::brildaqchecker::BRILDAQListener const& brildaqListener);
      virtual ~BRILDAQDataInfoSpaceUpdater();

    protected:
      virtual void updateInfoSpaceImpl(tcds::utils::InfoSpaceHandler* const infoSpaceHandler);

    private:
      tcds::brildaqchecker::BRILDAQListener const& brildaqListener_;

    };

  } // namespace brildaqchecker
} // namespace tcds

#endif // _tcds_brildaqchecker_BRILDAQDataInfoSpaceUpdater_h_
