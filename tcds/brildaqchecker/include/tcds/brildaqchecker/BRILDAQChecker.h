#ifndef _tcds_brildaqchecker_BRILDAQChecker_h_
#define _tcds_brildaqchecker_BRILDAQChecker_h_

#include <memory>

#include "xdaq/Application.h"

#include "tcds/brildaqchecker/BRILDAQListener.h"
#include "tcds/utils/XDAQAppBase.h"

namespace xdaq {
  class ApplicationStub;
}

namespace xdata {
  class Event;
}

namespace tcds {
  namespace brildaqchecker {

    class BRILDAQDataInfoSpaceHandler;
    class BRILDAQDataInfoSpaceUpdater;

    class BRILDAQChecker : public tcds::utils::XDAQAppBase
    {

    public:
      XDAQ_INSTANTIATOR();

      BRILDAQChecker(xdaq::ApplicationStub* stub);
      virtual ~BRILDAQChecker();

    protected:
      virtual void setupInfoSpaces();

      virtual void actionPerformed(xdata::Event& event);

      // Dummy methods in this case.
      virtual void hwConnectImpl();
      virtual void hwReleaseImpl();
      virtual void hwConfigureImpl();

      tcds::brildaqchecker::BRILDAQListener brildaqListener_;

    private:
      // Various InfoSpaces and their InfoSpaceUpdaters.
      std::unique_ptr<tcds::brildaqchecker::BRILDAQDataInfoSpaceUpdater> brildaqDataInfoSpaceUpdaterP_;
      std::unique_ptr<tcds::brildaqchecker::BRILDAQDataInfoSpaceHandler> brildaqDataInfoSpaceP_;

    };

  } // namespace brildaqchecker
} // namespace tcds

#endif // _tcds_brildaqchecker_BRILDAQChecker_h_
