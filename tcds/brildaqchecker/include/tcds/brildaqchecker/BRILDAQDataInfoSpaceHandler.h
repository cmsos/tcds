#ifndef _tcds_brildaqchecker_BRILDAQDataInfoSpaceHandler_h_
#define _tcds_brildaqchecker_BRILDAQDataInfoSpaceHandler_h_

#include <string>

#include "tcds/utils/InfoSpaceHandler.h"

namespace tcds {
  namespace utils {
    class InfoSpaceUpdater;
    class Monitor;
    class WebServer;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace brildaqchecker {

    class BRILDAQDataInfoSpaceHandler : public tcds::utils::InfoSpaceHandler
    {

    public:
      BRILDAQDataInfoSpaceHandler(tcds::utils::XDAQAppBase& xdaqApp,
                                  tcds::utils::InfoSpaceUpdater* updater);
      virtual ~BRILDAQDataInfoSpaceHandler();

    protected:
      virtual void registerItemSetsWithMonitor(tcds::utils::Monitor& monitor);
      virtual void registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                 tcds::utils::Monitor& monitor,
                                                 std::string const& forceTabName="");

      virtual std::string formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const;

    };

  } // namespace brildaqchecker
} // namespace tcds

#endif // _tcds_brildaqchecker_BRILDAQDataInfoSpaceHandler_h_
