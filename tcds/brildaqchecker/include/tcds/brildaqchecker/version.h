#ifndef _tcds_brildaqchecker_version_h_
#define _tcds_brildaqchecker_version_h_

#include <functional>
#include <set>
#include <string>

#include "config/PackageInfo.h"

// !!! Edit these lines to reflect the latest package version. !!!
#define TCDS_BRILDAQCHECKER_VERSION_MAJOR 4
#define TCDS_BRILDAQCHECKER_VERSION_MINOR 15
#define TCDS_BRILDAQCHECKER_VERSION_PATCH 0

// If any previous versions available:
// #define TCDS_BRILDAQCHECKER_PREVIOUS_VERSIONS "3.8.0,3.8.1"
// else:
#undef TCDS_BRILDAQCHECKER_PREVIOUS_VERSIONS

//
// Template macros and boilerplate code.
//
#define TCDS_BRILDAQCHECKER_VERSION_CODE PACKAGE_VERSION_CODE(TCDS_BRILDAQCHECKER_VERSION_MAJOR,TCDS_BRILDAQCHECKER_VERSION_MINOR,TCDS_BRILDAQCHECKER_VERSION_PATCH)
#ifndef TCDS_BRILDAQCHECKER_PREVIOUS_VERSIONS
#define TCDS_BRILDAQCHECKER_FULL_VERSION_LIST PACKAGE_VERSION_STRING(TCDS_BRILDAQCHECKER_VERSION_MAJOR,TCDS_BRILDAQCHECKER_VERSION_MINOR,TCDS_BRILDAQCHECKER_VERSION_PATCH)
#else
#define TCDS_BRILDAQCHECKER_FULL_VERSION_LIST TCDS_BRILDAQCHECKER_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(TCDS_BRILDAQCHECKER_VERSION_MAJOR,TCDS_BRILDAQCHECKER_VERSION_MINOR,TCDS_BRILDAQCHECKER_VERSION_PATCH)
#endif

namespace tcds {
  namespace brildaqchecker {
    const std::string project = "tcds";
    const std::string package = "brildaqchecker";
    const std::string versions = TCDS_BRILDAQCHECKER_FULL_VERSION_LIST;
    const std::string description = "CMS software for the TCDS BRILDAQChecker.";
    const std::string authors = "Jeroen Hegeman";
    const std::string summary = "CMS software for the TCDS BRILDAQChecker.";
    const std::string link = "https://twiki.cern.ch/twiki/bin/view/CMS/TcdsNotes";
    config::PackageInfo getPackageInfo();
    void checkPackageDependencies();
    std::set<std::string, std::less<std::string> > getPackageDependencies();
  }
}

#endif
