#include "tcds/brildaqchecker/BRILDAQListener.h"

#include "log4cplus/loggingmacros.h"

#include "b2in/nub/Method.h"
#include "toolbox/BSem.h"
#include "toolbox/string.h"
#include "toolbox/mem/Reference.h"
#include "xcept/Exception.h"
#include "xdata/exception/Exception.h"
#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "xdata/exdr/Serializer.h"
#include "xdata/Properties.h"
#include "xdata/Table.h"

#include "tcds/exception/Exception.h"
#include "tcds/utils/ConfigurationInfoSpaceHandler.h"
#include "tcds/utils/LockGuard.h"
#include "tcds/utils/XDAQAppBase.h"

tcds::brildaqchecker::BRILDAQListener::BRILDAQListener(tcds::utils::XDAQAppBase& xdaqApp) :
  tcds::utils::TCDSObject(xdaqApp),
  eventing::api::Member(&xdaqApp),
  busName_("undefined"),
  topicName_("undefined"),
  lock_(toolbox::BSem::FULL)
{
}

tcds::brildaqchecker::BRILDAQListener::~BRILDAQListener()
{
  stop();
}

void
tcds::brildaqchecker::BRILDAQListener::start()
{
  clearHistory();

  busName_ =
    getOwnerApplication().getConfigurationInfoSpaceHandler().getString("busName");
  topicName_ =
    getOwnerApplication().getConfigurationInfoSpaceHandler().getString("topicName");

  try
    {
      // Bind the eventing callback and subscribe to our topic.
      b2in::nub::deferredbind(eventing::api::Member::getOwnerApplication(),
                              this,
                              &tcds::brildaqchecker::BRILDAQListener::onMessage);
      getEventingBus(busName_).subscribe(topicName_);
      LOG4CPLUS_INFO(getOwnerApplication().getApplicationLogger(),
                     toolbox::toString("Successfully subscribed to topic '%s' on bus '%s'.",
                                       topicName_.c_str(),
                                       busName_.c_str()));
    }
  catch (xcept::Exception& err)
    {
      std::string const msg =
        toolbox::toString("Failed to subscribe to BRILDAQ eventing. Bus name '%s', topic name '%s'. Problem: '%s'.",
                          busName_.c_str(),
                          topicName_.c_str(),
                          err.what());
      XCEPT_RETHROW(tcds::exception::BRILDAQProblem,
                    msg,
                    err);
    }
}

void
tcds::brildaqchecker::BRILDAQListener::stop()
{
  getEventingBus(busName_).unsubscribe(topicName_);
  LOG4CPLUS_INFO(getOwnerApplication().getApplicationLogger(),
                 toolbox::toString("Unsubscribed from topic '%s' on bus '%s'.",
                                   topicName_.c_str(),
                                   busName_.c_str()));
}

void
tcds::brildaqchecker::BRILDAQListener::clearHistory()
{
  tcds::utils::LockGuard<tcds::utils::Lock> guardedLock(lock_);

  nibbles_.clear();
}

tcds::brildaqchecker::BRILDAQSnapshot
tcds::brildaqchecker::BRILDAQListener::snapshot() const
{
  // Lock to prevent being interrupted by updates.
  tcds::utils::LockGuard<tcds::utils::Lock> guardedLock(lock_);

  BRILDAQSnapshot snapshot(nibbles_);

  return snapshot;
}

void
tcds::brildaqchecker::BRILDAQListener::onMessage(toolbox::mem::Reference* ref, xdata::Properties& plist)
{
  // Kinda course locking, but okay.
  tcds::utils::LockGuard<tcds::utils::Lock> guardedLock(lock_);

  // The eventing business can send 'data' as well as 'control'
  // messages, so we need to check if we actually received something
  // interesting or not.
  std::string const action = plist.getProperty("urn:b2in-eventing:action");
  if (action == "notify")
    {
      // std::string const topic = plist.getProperty("urn:b2in-eventing:topic");

      // BUG BUG BUG
      // Should probably raise trouble if ref == 0.
      if (ref != 0)
        {
          xdata::Table table;
          xdata::exdr::FixedSizeInputStreamBuffer inBuffer(static_cast<char*>(ref->getDataLocation()),
                                                           ref->getDataSize());
          xdata::exdr::Serializer serializer;
          try
            {
              serializer.import(&table, &inBuffer);
              nibbles_.push_back(table);
              while (nibbles_.size() > kMaxHistoryLength)
                {
                  nibbles_.pop_front();
                }
            }
          catch (xdata::exception::Exception& err)
            {
              std::string const msgBase = "Failed to deserialize incoming BRILDAQ table";
              std::string const msg =
                toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
              XCEPT_RETHROW(tcds::exception::RuntimeProblem, msg, err);
            }
        }
    }

  // Clean up.
  if (ref != 0)
    {
      ref->release();
    }
}
