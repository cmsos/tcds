#include "tcds/brildaqchecker/BRILDAQHistory.h"

#include <iomanip>
#include <sstream> 
#include <cstddef>
#include <stdint.h>

#include "toolbox/TimeVal.h"
#include "toolbox/string.h"
#include "xdata/Float.h"
#include "xdata/Serializable.h"
#include "xdata/TimeVal.h"
#include "xdata/UnsignedInteger32.h"

#include "tcds/utils/Definitions.h"
#include "tcds/utils/Utils.h"

tcds::brildaqchecker::BRILDAQHistory::BRILDAQHistory(std::vector<xdata::Table> const& dataIn) :
  nibbles_(dataIn)
{
}

tcds::brildaqchecker::BRILDAQHistory::~BRILDAQHistory()
{
}

std::string
tcds::brildaqchecker::BRILDAQHistory::getJSONString() const
{
  std::stringstream res;
  res << std::dec;

  res << "[";
  size_t index = 0;
  for (std::vector<xdata::Table>::const_iterator it = nibbles_.begin();
       it != nibbles_.end();
       ++it)
    {
      std::vector<std::string> const columns = it->getColumns();

      res << "{";

      for (std::vector<std::string>::const_iterator col = columns.begin();
           col != columns.end();
           ++col)
        {
          if (col != columns.begin())
            {
              res << ", ";
            }

          std::string valStr = it->getValueAt(0, *col)->toString();
          xdata::Serializable* tmp = it->getValueAt(0, *col);

          // Is this an xdata::TimeVal?
          xdata::TimeVal* const val = dynamic_cast<xdata::TimeVal*>(tmp);
          if (val)
            {
              valStr = tcds::utils::formatTimestamp(*val, toolbox::TimeVal::gmt, true);
            }
          else
            {
              if (toolbox::tolower(*col).find("bstsignalstatus") != std::string::npos)
                {
                  // Nicely format the BST signal/acquisition status.
                  xdata::UnsignedInteger32* const val = dynamic_cast<xdata::UnsignedInteger32*>(tmp);
                  tcds::definitions::BST_SIGNAL_STATUS const valueEnum =
                    static_cast<tcds::definitions::BST_SIGNAL_STATUS>(uint32_t(*val));
                  valStr = tcds::utils::bstStatusToString(valueEnum);
                }
              else if (toolbox::tolower(*col).find("deadtime") != std::string::npos)
                {
                  // Nicely format the deadtime percentages.
                  xdata::Float* const val = dynamic_cast<xdata::Float*>(tmp);
                  std::stringstream tmpStr;
                  tmpStr << std::fixed << std::setprecision(1) << *val;
                  valStr = tmpStr.str();
                }
            }

          res << tcds::utils::escapeAsJSONString(*col)
              << ": "
              << tcds::utils::escapeAsJSONString(valStr);
        }

      res << "}";
      if (index != (nibbles_.size() - 1))
        {
          res << ", ";
        }
      ++index;
    }
  res << "]";

  return res.str();
}
