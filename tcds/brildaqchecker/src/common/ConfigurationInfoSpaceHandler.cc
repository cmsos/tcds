#include "tcds/brildaqchecker/ConfigurationInfoSpaceHandler.h"

#include <string>

#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/WebServer.h"

tcds::brildaqchecker::ConfigurationInfoSpaceHandler::ConfigurationInfoSpaceHandler(xdaq::Application& xdaqApp) :
  tcds::utils::ConfigurationInfoSpaceHandler(xdaqApp)
{
  // The name of the BRILDAQ eventing bus.
  createString("busName",
               "brildata",
               "",
               tcds::utils::InfoSpaceItem::NOUPDATE,
               true);

  // The name of the TCDS BRILDAQ topic.
  createString("topicName",
               "tcds_brildaq_data",
               "",
               tcds::utils::InfoSpaceItem::NOUPDATE,
               true);
}

void
tcds::brildaqchecker::ConfigurationInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  std::string itemSetName = "itemset-app-config";
  monitor.newItemSet(itemSetName);
  monitor.addItem(itemSetName,
                  "monitoringInterval",
                  "Monitoring update interval",
                  this);

  itemSetName = "itemset-brildaq-config";
  monitor.newItemSet(itemSetName);
  monitor.addItem(itemSetName,
                  "busName",
                  "BRILDAQ eventing bus name",
                  this);
  monitor.addItem(itemSetName,
                  "topicName",
                  "BRILDAQ eventing topic name",
                  this);
}

void
tcds::brildaqchecker::ConfigurationInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                                   tcds::utils::Monitor& monitor,
                                                                                   std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "Configuration" : forceTabName;

  webServer.registerTab(tabName,
                        "Configuration parameters",
                        2);
  webServer.registerTable("Application configuration",
                          "Application configuration parameters",
                          monitor,
                          "itemset-app-config",
                          tabName);
  webServer.registerTable("BRILDAQ configuration",
                          "BRILDAQ eventing configuration parameters",
                          monitor,
                          "itemset-brildaq-config",
                          tabName);
}
