#include "tcds/brildaqchecker/BRILDAQSnapshot.h"

#include <algorithm>
#include <iterator>

tcds::brildaqchecker::BRILDAQSnapshot::BRILDAQSnapshot(std::deque<xdata::Table> const& nibbles) :
  nibbles_(nibbles)
{
}

tcds::brildaqchecker::BRILDAQSnapshot::~BRILDAQSnapshot()
{
}

std::vector<xdata::Table>
tcds::brildaqchecker::BRILDAQSnapshot::recentNibbles() const
{
  std::vector<xdata::Table> res;
  res.clear();
  std::copy(nibbles_.begin(), nibbles_.end(), std::back_inserter(res));
  return res;
}
