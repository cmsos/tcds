#include "tcds/brildaqchecker/BRILDAQDataInfoSpaceHandler.h"

#include "tcds/brildaqchecker/WebTableBRILDAQHistory.h"
#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/Utils.h"
#include "tcds/utils/WebServer.h"
#include "tcds/utils/XDAQAppBase.h"

tcds::brildaqchecker::BRILDAQDataInfoSpaceHandler::BRILDAQDataInfoSpaceHandler(tcds::utils::XDAQAppBase& xdaqApp,
                                                                               tcds::utils::InfoSpaceUpdater* updater) :
  InfoSpaceHandler(xdaqApp, "tcds-brildaqchecker-brildaqdata", updater)
{
  createString("brildaq_history");
}

tcds::brildaqchecker::BRILDAQDataInfoSpaceHandler::~BRILDAQDataInfoSpaceHandler()
{
}

void
tcds::brildaqchecker::BRILDAQDataInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  monitor.newItemSet("itemset-brildaq-history");
  monitor.addItem("itemset-brildaq-history",
                  "brildaq_history",
                  "Recent TCDS BRILDAQ data",
                  this);
}

void
tcds::brildaqchecker::BRILDAQDataInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                                 tcds::utils::Monitor& monitor,
                                                                                 std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "BRILDAQ data" : forceTabName;

  webServer.registerTab(tabName,
                        "Recent TCDS BRILDAQ data",
                        1);

  //----------

  webServer.registerWebObject<WebTableBRILDAQHistory>("BRILDAQ history",
                                                      "Recent TCDS BRILDAQ data",
                                                      monitor,
                                                      "itemset-brildaq-history",
                                                      tabName,
                                                      3);
}

std::string
tcds::brildaqchecker::BRILDAQDataInfoSpaceHandler::formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const
{
  std::string res = tcds::utils::escapeAsJSONString(tcds::utils::InfoSpaceHandler::kInvalidItemString);
  if (item->isValid())
    {
      std::string name = item->name();
      if (name == "brildaq_history")
        {
          // Special in the sense that this is something that needs to
          // be interpreted as a proper JavaScript object, so let's
          // not add any more double quotes.
          res = getString(name);
        }
      else
        {
          // For everything else simply call the generic formatter.
          res = InfoSpaceHandler::formatItem(item);
        }
    }
  return res;
}
