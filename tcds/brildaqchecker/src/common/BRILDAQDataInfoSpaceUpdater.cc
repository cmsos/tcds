#include "tcds/brildaqchecker/BRILDAQDataInfoSpaceUpdater.h"

#include <string>

#include "tcds/brildaqchecker/BRILDAQHistory.h"
#include "tcds/brildaqchecker/BRILDAQListener.h"
#include "tcds/brildaqchecker/BRILDAQSnapshot.h"
#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/XDAQAppBase.h"

tcds::brildaqchecker::BRILDAQDataInfoSpaceUpdater::BRILDAQDataInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                                                               tcds::brildaqchecker::BRILDAQListener const& brildaqListener) :
  tcds::utils::InfoSpaceUpdater(xdaqApp),
  brildaqListener_(brildaqListener)
{
}

tcds::brildaqchecker::BRILDAQDataInfoSpaceUpdater::~BRILDAQDataInfoSpaceUpdater()
{
}

void
tcds::brildaqchecker::BRILDAQDataInfoSpaceUpdater::updateInfoSpaceImpl(tcds::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  BRILDAQSnapshot const snapshot = brildaqListener_.snapshot();

  //----------

  // Prepare a table-able version of recently-received data.
  BRILDAQHistory const brildaqHistory(snapshot.recentNibbles());
  std::string const newVal = brildaqHistory.getJSONString();
  infoSpaceHandler->setString("brildaq_history", newVal);
  infoSpaceHandler->setValid();

  //----------

  // Now sync everything from the cache to the InfoSpace itself.
  infoSpaceHandler->writeInfoSpace();
}
