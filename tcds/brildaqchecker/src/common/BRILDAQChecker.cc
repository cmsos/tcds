#include "tcds/brildaqchecker/BRILDAQChecker.h"

#include <memory>
#include <string>

#include "toolbox/string.h"
#include "xcept/Exception.h"
#include "xdata/Event.h"
#include "xdaq/exception/ApplicationInstantiationFailed.h"

#include "tcds/brildaqchecker/BRILDAQDataInfoSpaceHandler.h"
#include "tcds/brildaqchecker/BRILDAQDataInfoSpaceUpdater.h"
#include "tcds/brildaqchecker/ConfigurationInfoSpaceHandler.h"
#include "tcds/exception/Exception.h"
#include "tcds/hwlayer/DeviceBase.h"

XDAQ_INSTANTIATOR_IMPL(tcds::brildaqchecker::BRILDAQChecker)

tcds::brildaqchecker::BRILDAQChecker::BRILDAQChecker(xdaq::ApplicationStub* stub)
try
  :
  tcds::utils::XDAQAppBase(stub, std::unique_ptr<tcds::hwlayer::DeviceBase>()),
    brildaqListener_(*this)
      {
        // Create the InfoSpace holding all configuration information.
        cfgInfoSpaceP_ =
          std::unique_ptr<tcds::brildaqchecker::ConfigurationInfoSpaceHandler>(new tcds::brildaqchecker::ConfigurationInfoSpaceHandler(*this));
      }
catch (tcds::exception::Exception const& err)
  {
    std::string msgBase = "Something went wrong instantiating the BRILDAQChecker application";
    std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.what());
    XCEPT_RAISE(xdaq::exception::ApplicationInstantiationFailed, msg.c_str());
  }

tcds::brildaqchecker::BRILDAQChecker::~BRILDAQChecker()
{
  brildaqListener_.stop();
}

void
tcds::brildaqchecker::BRILDAQChecker::setupInfoSpaces()
{
  // Make sure the configuration settings are up-to-date.
  cfgInfoSpaceP_->readInfoSpace();

  brildaqDataInfoSpaceUpdaterP_ = std::unique_ptr<tcds::brildaqchecker::BRILDAQDataInfoSpaceUpdater>(new tcds::brildaqchecker::BRILDAQDataInfoSpaceUpdater(*this, brildaqListener_));
  brildaqDataInfoSpaceP_ =
    std::unique_ptr<tcds::brildaqchecker::BRILDAQDataInfoSpaceHandler>(new tcds::brildaqchecker::BRILDAQDataInfoSpaceHandler(*this, brildaqDataInfoSpaceUpdaterP_.get()));

  // We don't have an FSM, so no state either.
  appStateInfoSpace_.setString("stateName", "n/a");
  // Similar for the hardware lease.
  appStateInfoSpace_.setString("hwLeaseOwnerId", "n/a");

  // Register all InfoSpaceItems with the Monitor.
  cfgInfoSpaceP_->registerItemSets(monitor_, webServer_);
  appStateInfoSpace_.registerItemSets(monitor_, webServer_);
  brildaqDataInfoSpaceP_->registerItemSets(monitor_, webServer_);
}

void
tcds::brildaqchecker::BRILDAQChecker::actionPerformed(xdata::Event& event)
{
  tcds::utils::XDAQAppBase::actionPerformed(event);

  // NOTE: After the application has been instantiated and its default
  // settings loaded, the urn:xdaq-event:setDefaultValues event is
  // fired. The urn:xdaq-event:configuration-loaded event is fired
  // after the configuration of the whole XDAQ executive
  // (applications, endpoints, etc.) has been done.
  if (event.type() == "urn:xdaq-event:setDefaultValues")
    {
      try
        {
          brildaqListener_.start();
        }
      catch (xcept::Exception& err)
        {
          appStateInfoSpace_.setFSMState("Failed", err.what());
        }
    }
}

void
tcds::brildaqchecker::BRILDAQChecker::hwConnectImpl()
{
}

void
tcds::brildaqchecker::BRILDAQChecker::hwReleaseImpl()
{
}

void
tcds::brildaqchecker::BRILDAQChecker::hwConfigureImpl()
{
}
