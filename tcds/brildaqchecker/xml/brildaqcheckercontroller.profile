<?xml version="1.0"?>

<!--
The order of specification will determine the sequence of instantiation.
All modules are loaded prior to instantiation of plugins.
-->

<xp:Profile
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/"
    xmlns:xp="http://xdaq.web.cern.ch/xdaq/xsd/2005/XMLProfile-11">

  <!-- Executive. -->
  <xp:Application heartbeat="false" class="executive::Application" id="0" group="profile" service="executive" network="local" logpolicy="inherit">
    <properties xmlns="urn:xdaq-application:Executive" xsi:type="soapenc:Struct">
      <logUrl xsi:type="xsd:string">console</logUrl>
      <logLevel xsi:type="xsd:string">INFO</logLevel>
    </properties>
  </xp:Application>
  <xp:Module>${XDAQ_ROOT}/lib/libb2innub.so</xp:Module>
  <xp:Module>${XDAQ_ROOT}/lib/libexecutive.so</xp:Module>

  <xp:Application class="pt::utcp::Application" id="20" instance="0" network="local" heartbeat="false" logpolicy="inherit">
    <properties xmlns="urn:xdaq-application:pt::utcp::Application" xsi:type="soapenc:Struct">
      <maxBlockSize xsi:type="xsd:unsignedInt">4096</maxBlockSize>
      <committedPoolSize xsi:type="xsd:double">0x100000</committedPoolSize>
      <ioQueueSize xsi:type="xsd:unsignedInt">1024</ioQueueSize>
      <autoConnect xsi:type="xsd:boolean">false</autoConnect>
      <protocol xsi:type="xsd:string">utcp</protocol>
      <maxReceiveBuffers xsi:type="xsd:unsignedInt">4</maxReceiveBuffers>
    </properties>
  </xp:Application>
  <xp:Module>/opt/xdaq/lib/libtcpla.so</xp:Module>
  <xp:Module>/opt/xdaq/lib/libptutcp.so</xp:Module>

  <xp:Application heartbeat="false" class="pt::http::PeerTransportHTTP" id="1" group="profile" network="local" logpolicy="inherit">
    <properties xmlns="urn:xdaq-application:pt::http::PeerTransportHTTP" xsi:type="soapenc:Struct">
      <documentRoot xsi:type="xsd:string">${XDAQ_DOCUMENT_ROOT}</documentRoot>
      <aliasName xsi:type="xsd:string">/directory</aliasName>
      <aliasPath xsi:type="xsd:string">${XDAQ_SETUP_ROOT}/${XDAQ_ZONE}</aliasPath>
      <httpHeaderFields xsi:type="soapenc:Array" soapenc:arrayType="xsd:ur-type[3]">
        <item xsi:type="soapenc:Struct" soapenc:position="[0]">
          <name xsi:type="xsd:string">Access-Control-Allow-Origin</name>
          <value xsi:type="xsd:string">*</value>
        </item>
        <item xsi:type="soapenc:Struct" soapenc:position="[1]">
          <name xsi:type="xsd:string">Access-Control-Allow-Methods</name>
          <value xsi:type="xsd:string">POST, GET, OPTIONS</value>
        </item>
        <item xsi:type="soapenc:Struct" soapenc:position="[2]">
          <name xsi:type="xsd:string">Access-Control-Allow-Headers</name>
          <value xsi:type="xsd:string">x-requested-with</value>
        </item>
      </httpHeaderFields>
      <expiresByType xsi:type="soapenc:Array" soapenc:arrayType="xsd:ur-type[7]">
        <item xsi:type="soapenc:Struct" soapenc:position="[0]">
          <name xsi:type="xsd:string">image/png</name>
          <value xsi:type="xsd:string">PT4300H</value>
        </item>
        <item xsi:type="soapenc:Struct" soapenc:position="[1]">
          <name xsi:type="xsd:string">image/jpg</name>
          <value xsi:type="xsd:string">PT4300H</value>
        </item>
        <item xsi:type="soapenc:Struct" soapenc:position="[2]">
          <name xsi:type="xsd:string">image/gif</name>
          <value xsi:type="xsd:string">PT4300H</value>
        </item>
        <item xsi:type="soapenc:Struct" soapenc:position="[3]">
          <name xsi:type="xsd:string">application/x-shockwave-flash</name>
          <value xsi:type="xsd:string">PT120H</value>
        </item>
        <item xsi:type="soapenc:Struct" soapenc:position="[4]">
          <name xsi:type="xsd:string">application/font-woff</name>
          <value xsi:type="xsd:string">PT8600H</value>
        </item>
        <item xsi:type="soapenc:Struct" soapenc:position="[5]">
          <name xsi:type="xsd:string">text/css</name>
          <value xsi:type="xsd:string">PT1H</value>
        </item>
        <item xsi:type="soapenc:Struct" soapenc:position="[6]">
          <name xsi:type="xsd:string">text/javascript</name>
          <value xsi:type="xsd:string">PT1H</value>
        </item>
      </expiresByType>
    </properties>
  </xp:Application>
  <xp:Module>${XDAQ_ROOT}/lib/libpthttp.so</xp:Module>

  <xp:Application heartbeat="false" class="pt::fifo::PeerTransportFifo" id="8" group="profile" network="local" logpolicy="inherit" />
  <xp:Module>${XDAQ_ROOT}/lib/libptfifo.so</xp:Module>

  <!-- HyperDAQ. -->
  <xp:Application heartbeat="false" class="hyperdaq::Application" id="3" group="profile" service="hyperdaq" network="local" logpolicy="inherit"/>
  <xp:Module>${XDAQ_ROOT}/lib/libhyperdaq.so</xp:Module>

  <!-- XMem probe. -->
  <xp:Application class="xmem::probe::Application" id="7" network="local" logpolicy="inherit"/>
  <xp:Module>${XDAQ_ROOT}/lib/libxmemprobe.so</xp:Module>
  <xp:Endpoint protocol="utcp" service="b2in" hostname="localhost" port="1911" maxport="1970" autoscan="true" network="local" smode="select" rmode="select" nonblock="true" sndTimeout="2000" rcvTimeout="2000"/>

  <!-- Heartbeat probe. -->
  <xp:Application heartbeat="true" class="xmas::heartbeat::probe::Application" id="4" network="local" group="sentinel" service="heartbeatprobe" logpolicy="inherit">
    <properties xmlns="urn:xdaq-application:xmas::heartbeat::probe::Application" xsi:type="soapenc:Struct">
      <heartbeatdURL xsi:type="xsd:string">utcp://localhost:2435</heartbeatdURL>
      <heartbeatWatchdog xsi:type="xsd:string">PT10S</heartbeatWatchdog>
    </properties>
  </xp:Application>
  <xp:Module>${XDAQ_ROOT}/lib/libb2inutils.so</xp:Module>
  <xp:Module>${XDAQ_ROOT}/lib/libwsaddressing.so</xp:Module>
  <xp:Module>${XDAQ_ROOT}/lib/libxmasutils.so</xp:Module>
  <xp:Module>${XDAQ_ROOT}/lib/libxmasheartbeatprobe.so</xp:Module>

  <!-- XMAS probe. -->
  <xp:Application heartbeat="true" class="xmas::probe::Application" id="5" network="local" group="xmas" service="xmasprobe" logpolicy="inherit">
    <properties xmlns="urn:xdaq-application:xmas::probe::Application" xsi:type="soapenc:Struct">
      <autoConfigure xsi:type="xsd:boolean">true</autoConfigure>
      <autoConfSearchPath xsi:type="xsd:string">${XDAQ_ROOT}/share/${XDAQ_ZONE}/sensor</autoConfSearchPath>
      <sensordURL xsi:type="xsd:string">utcp://localhost:2433</sensordURL>
    </properties>
  </xp:Application>
  <xp:Module>${XDAQ_ROOT}/lib/libb2inutils.so</xp:Module>
  <xp:Module>${XDAQ_ROOT}/lib/libwsaddressing.so</xp:Module>
  <xp:Module>${XDAQ_ROOT}/lib/libxmasutils.so</xp:Module>
  <xp:Module>${XDAQ_ROOT}/lib/libxmasprobe.so</xp:Module>

  <!-- Sentinel probe. -->
  <xp:Application heartbeat="true" class="sentinel::probe::Application" id="6" network="local" group="sentinel" service="sentinelprobe" logpolicy="inherit">
    <properties xmlns="urn:xdaq-application:sentinel::probe::Application" xsi:type="soapenc:Struct">
      <sentineldURL xsi:type="xsd:string">utcp://localhost:2434</sentineldURL>
    </properties>
  </xp:Application>
  <xp:Module>${XDAQ_ROOT}/lib/libb2inutils.so</xp:Module>
  <xp:Module>${XDAQ_ROOT}/lib/libsentinelutils.so</xp:Module>
  <xp:Module>${XDAQ_ROOT}/lib/libsentinelprobe.so</xp:Module>

  <!-- Tracer Probe -->
  <xp:Application heartbeat="true" class="tracer::probe::Application" id="36" network="local" group="xmas" service="tracerprobe" logpolicy="inherit">
    <properties xmlns="urn:xdaq-application:tracer::probe::Application" xsi:type="soapenc:Struct">
      <jelFileName xsi:type="xsd:string">http://cmstcdslab.cern.ch:9966/directory/etc/tracerfilter.xml</jelFileName>
    </properties>
  </xp:Application>
  <xp:Module>${XDAQ_ROOT}/lib/libb2inutils.so</xp:Module>
  <xp:Module>${XDAQ_ROOT}/lib/libwsaddressing.so</xp:Module>
  <xp:Module>${XDAQ_ROOT}/lib/libxmasutils.so</xp:Module>
  <xp:Module>${XDAQ_ROOT}/lib/libtracerprobe.so</xp:Module>

  <!-- Eventing stuff for the BRILDAQ. -->
  <xp:Endpoint protocol="utcp" service="b2in" rmode="select" hostname="137.138.49.61" port="1978" network="brildaq_network" sndTimeout="0" rcvTimeout="0" affinity="RCV:P,SND:W,DSR:W,DSS:W" singleThread="true" publish="true"/>

  <!-- Eventing subscriber. -->
  <xp:Application class="eventing::core::Subscriber" id="15" instance="0" network="brildaq_network" group="b2in" service="eventing-subscriber" bus="brildata" logpolicy="inherit">
    <properties xmlns="urn:xdaq-application:eventing::core::Subscriber" xsi:type="soapenc:Struct">
      <eventings xsi:type="soapenc:Array" soapenc:arrayType="xsd:ur-type[1]">
        <item xsi:type="xsd:string" soapenc:position="[0]">utcp://137.138.49.61:1977</item>
      </eventings>
    </properties>
  </xp:Application>
  <xp:Module>${XDAQ_ROOT}/lib/libb2inutils.so</xp:Module>
  <xp:Module>${XDAQ_ROOT}/lib/libeventingcore.so</xp:Module>

</xp:Profile>
