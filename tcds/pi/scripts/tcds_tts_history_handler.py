#!/usr/bin/env python

######################################################################

# NOTE: This script combines several 'top-level' classes, as well as a
# series of utilities in a single file. This was done on purpose in
# order to keep everything in a single file for ease of deployment.

# NOTE: The XDAQ applications creating the TTS history files run as
# root, so the history files are owned by root. This means that this
# script has to run as root as well. On the other hand, the filer area
# is owned by a normal user (in order to avoid ending up with the
# files owned by nfs:nobody on the filer). Thereore this script drops
# root privileges when copying the files to the destination area.

######################################################################

import abc
import argparse
import ConfigParser
import datetime
import fcntl
import fnmatch
import glob
import gzip
import logging
import logging.handlers
import os
import re
import shutil
import smtplib
import socket
import stat
import subprocess
import sys

from collections import namedtuple
from email.mime.text import MIMEText

# NOTE: In some cases pytz will be present (e.g., in the lab), but in
# general it is not part of Python 2 distributions.
try:
    from datetime import timezone
    utc = timezone.utc
except ImportError:
    class UTC(datetime.tzinfo):
        def utcoffset(self, dt):
            return timedelta(0)
        def tzname(self, dt):
            return "UTC"
        def dst(self, dt):
            return timedelta(0)
utc = UTC()

import pdb

try:
    import debug_hook
    import pdb
except ImportError:
    pass

######################################################################

CRASH_LOG_SMTP_SERVER = "localhost"
CRASH_LOG_MAIL_FROM = "jeroen.hegeman@cern.ch"
CRASH_LOG_MAIL_TO = ["jeroen.hegeman@cern.ch"]

SCRIPT_NAME = os.path.basename(__file__)
DEFAULT_CFG_FILE_NAME = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                     SCRIPT_NAME.replace(".py", ".cfg"))

LOGGING_FMT = '%(asctime)s - %(name)s - %(process)d - %(levelname)-8s - %(message)s'

######################################################################

class FileInfo(object):

    STATUS_FILE_FOUND = 1
    STATUS_FILE_LOCKED = 2
    STATUS_FILE_ZIPPED = 3
    STATUS_FILE_MOVED = 4
    STATUS_FILE_UNLOCKABLE = 5
    STATUS_FILE_PROBLEMATIC = 6

    def __init__(self, name, status, size_raw, size_zipped, msg):
        self.name = name
        self.status = status
        self.size_raw = size_raw
        self.size_zipped = size_zipped
        self.msg = msg
        # End of __init__().

    # End of class FileInfo.

######################################################################

# A method to help us trigger a logging email when things crash.
def crash_hook(exception_type, value, back_trace):
    import traceback
    trace_str = "  ".join(traceback.format_exception(exception_type, value, back_trace))
    tmp = "Crash of {0:s}: {1:s}{2:s}"
    # Trigger our log email.
    script_name = os.path.basename(__file__)
    logger.critical(tmp.format(script_name, os.linesep, trace_str))
    # And continue the usual way.
    sys.__excepthook__(exception_type, value, back_trace)
    # End of crash_hook().

######################################################################

def get_host_aliases(host_name):
    # NOTE: This uses the aliasname.sh script of the CMS sysadmins.
    res = []
    try:
        tmp = subprocess.check_output(["aliasname.sh", "-a", host_name])
        res = [i.strip() for i in tmp.split()]
    except Exception:
        pass
    # End of get_host_aliases().
    return res

######################################################################

def get_formatted_host_name(socket):
    host_name = socket.getfqdn()
    host_aliases = get_host_aliases(host_name)
    res = host_name
    if host_aliases:
        res = "{0:s} ({1:s})".format(host_name, ", ".join(host_aliases))
    # End of get_formatted_host_name().
    return res

######################################################################

# def partial(f, *args, **kwargs):
#     def wrapped(*args2, **kwargs2):
#         return f(*args, *args2, **kwargs, **kwargs2)
#     return wrapped

######################################################################

def determine_dir_owner(dir_name):
    tmp = os.stat(dir_name)
    user = tmp.st_uid
    group = tmp.st_gid
    # End of _determine_destination_owner().
    return (user, group)

######################################################################

def find_files_recursively(top_dir_name):
    file_names = []
    for (root, dir_names, file_names_tmp) in os.walk(top_dir_name):
        for file_name in file_names_tmp:
            file_names.append(os.path.join(root, file_name))
    # End of find_files_recursively().
    return file_names

######################################################################

def find_broken_links_recursively(top_dir_name):
    broken_links = []
    for (root, dir_names, file_names) in os.walk(top_dir_name):
        for file_name in file_names:
            path = os.path.join(root, file_name)
            # Select only symlinks.
            if os.path.islink(path):
                # Follow symlinks to their target.
                target_path = os.readlink(path)
                # Resolve relative target paths.
                if not os.path.isabs(target_path):
                    target_path = os.path.join(os.path.dirname(path), target_path)
                if not os.path.exists(target_path):
                    broken_links.append(path)
    # End of find_broken_links_recursively()
    return broken_links

######################################################################

def find_empty_subdirs_recursively(top_dir_name):
    # NOTE: This will _not_ list top_dir_name as empty directory, even
    # if it is empty.
    empty_dirs = []
    for (root, dir_names, file_names) in os.walk(top_dir_name):
        if not dir_names and not file_names:
            if not root == top_dir_name:
                empty_dirs.append(root)
    # End of find_empty_subdirs_recursively().
    return empty_dirs

######################################################################

def get_disk_space_info(top_dir_name):
    fs_info = os.statvfs(top_dir_name)
    fs_block_size = fs_info.f_frsize
    size_in_bytes = fs_block_size * fs_info.f_blocks
    free_bytes = fs_block_size * fs_info.f_bfree
    avail_bytes = fs_block_size * fs_info.f_bavail
    num_inodes = fs_info.f_files
    num_inodes_free = fs_info.f_ffree
    res = (size_in_bytes, free_bytes, avail_bytes, num_inodes, num_inodes_free)
    # End of get_disk_space_info().
    return res

######################################################################

def format_disk_space_string(number_of_bytes):
    disk_space_units = [
        (1024**1, "KiB"),
        (1024**2, "MiB"),
        (1024**3, "GiB"),
        (1024**4, "TiB")
    ]
    for (threshold, unit_str) in reversed(disk_space_units):
        if number_of_bytes >= threshold:
            tmp = 1. * number_of_bytes / threshold
            res = "{0:.2f} {1:s}".format(tmp, unit_str)
            break

    # End of format_disk_space_string().
    return res

######################################################################

def format_timedelta(delta):
    periods = [
        ('year',        60*60*24*365),
        ('month',       60*60*24*30),
        ('day',         60*60*24),
        ('hour',        60*60),
        ('minute',      60),
        ('second',      1)
    ]

    seconds = int(delta.total_seconds())
    pieces = []
    for (period_name, period_seconds) in periods:
        if seconds >= period_seconds:
            (period_value, seconds) = divmod(seconds, period_seconds)
            if period_value == 1:
                pieces.append("{0:d} {1:s}".format(period_value, period_name))
            else:
                pieces.append("{0:d} {1:s}s".format(period_value, period_name))

    res = ", ".join(pieces)

    if not res:
        res = "less than a second"

    # End of format_timedelta().
    return res

######################################################################

class OneShotSMTPHandler(logging.handlers.SMTPHandler):

    def __init__(self, mailhost, fromaddr, toaddrs, subject,
                 credentials=None, secure=None):
        super(OneShotSMTPHandler, self).__init__(mailhost,
                                                 fromaddr,
                                                 toaddrs,
                                                 subject,
                                                 credentials,
                                                 secure)
        self.records = []
        # End of __init__().

    def emit(self, record):
        self.records.append(record)
        # End of emit().

    def flush(self):
        if len(self.records):
            try:
                import smtplib
                from email.utils import formatdate
                port = self.mailport
                if not port:
                    port = smtplib.SMTP_PORT
                smtp = smtplib.SMTP(self.mailhost, port, timeout=self._timeout)
                record_lines = []
                for record in self.records:
                    tmp = self.format(record)
                    record_lines.append(tmp)
                msg_tmp = "From: {0:s}" \
                           + os.linesep \
                           + "To: {1:s}" \
                           + os.linesep \
                           + "Subject: {2:s}" \
                           + os.linesep \
                           + "Date: {3:s}" \
                           + os.linesep \
                           + "{4:s}"
                msg = msg_tmp.format(self.fromaddr,
                                      ",".join(self.toaddrs),
                                      self.getSubject(record),
                                      formatdate(),
                                      os.linesep.join(record_lines))
                if self.username:
                    if self.secure is not None:
                        smtp.ehlo()
                        smtp.starttls(*self.secure)
                        smtp.ehlo()
                    smtp.login(self.username, self.password)
                smtp.sendmail(self.fromaddr, self.toaddrs, msg)
                smtp.quit()
            except (KeyboardInterrupt, SystemExit):
                raise
            except:
                self.handleError(record)
        # End of flush().

    # End of OneShotSMTPHandler().

######################################################################

class FileLocker(object):

    LOCK_FILE_PREFIX = "."
    LOCK_FILE_SUFFIX = ".lock"

    @staticmethod
    def create_lock_file_name(file_name):
        file_base_name = os.path.basename(file_name)
        file_path_name = os.path.dirname(file_name)
        lock_file_base_name = FileLocker.LOCK_FILE_PREFIX \
                              + file_base_name \
                              + FileLocker.LOCK_FILE_SUFFIX
        lock_file_name = os.path.join(file_path_name, lock_file_base_name)
        # End of create_lock_file_name().
        return lock_file_name

    def __init__(self, lock_file_name):
        self._lock_file_name = lock_file_name
        self._lock_file = None
        # End of __init__().

    def __enter__(self):
        self._get_lock_file()
        # End of __enter__().

    def __exit__(self, type, value, traceback):
        if (self._lock_file):
            os.remove(self._lock_file_name)
            os.close(self._lock_file)
        # End of __exit__().

    def _get_lock_file(self):

        lock_file_path = self._lock_file_name
        lock_file_flags = os.O_WRONLY | os.O_CREAT | os.O_EXCL
        lock_file_mode = stat.S_IWUSR | stat.S_IWGRP | stat.S_IWOTH

        # Create lock file.
        # See also: https://stackoverflow.com/a/15015748/832230.
        umask_ori = os.umask(0)
        try:
            self._lock_file = os.open(lock_file_path, lock_file_flags, lock_file_mode)
            # Mark it as ours.
            os.write(self._lock_file, os.path.basename(__file__))
        except Exception as err:
            # Failed to create lock file. This probably simply means
            # it already exists, and therefore we should keep our
            # hands off.
            msg_base = "Failed to create lock file '{0:s}': '{1:s}'."
            raise RuntimeError(msg_base.format(lock_file_path, str(err)))
        finally:
            os.umask(umask_ori)

        # Once the file exists, try locking the file.
        try:
            # Try to get an exclusive lock, in non-blocking mode.
            fcntl.lockf(self._lock_file, fcntl.LOCK_EX | fcntl.LOCK_NB)
        except IOError as err:
            # Failed to lock.
            msg_base = "Failed to obtain lock file '{0:s}': '{1:s}'."
            raise RuntimeError(msg_base.format(lock_file_path, str(err)))

        # End of get_lock_file().

     # End of class FileLocker.

######################################################################

class DroppedPrivs(object):

    def __init__(self, euid, egid):
        self._euid = euid
        self._egid = egid
        # End of __init__().

    def __enter__(self):
        self._drop_privs()
        # End of __enter__().

    def __exit__(self, type, value, traceback):
        self._regain_privs()
        # End of __exit__().

    def _drop_privs(self):
        self._stored_egid = os.getegid()
        self._stored_euid = os.geteuid()
        os.setegid(self._egid)
        os.seteuid(self._euid)
        # End of _drop_privs.

    def _regain_privs(self):
        os.setegid(self._stored_egid)
        os.seteuid(self._stored_euid)
        # End of _regain_privs.

    # End of class DroppedPrivs.

######################################################################

class TTSLogFileName(object):

    PI_TTS_LOG_FILE_NAME_PREFIX = "pi_tts_log"
    PI_TTS_LOG_FILE_NAME_EXTENSION = "txt"
    PI_TTS_LOG_FILE_NAME_PATTERN = PI_TTS_LOG_FILE_NAME_PREFIX \
                                   + "*" \
                                   + "." \
                                   + PI_TTS_LOG_FILE_NAME_EXTENSION
    PI_TTS_LOG_FILE_NAME_TIMESTAMP_FMT = "%Y%m%dT%H%M%SZ"
    PI_TTS_LOG_FILE_NAME_TIMESTAMP_FMT_REGEX = "[0-9]{8}T[0-9]{6}Z"

    def __init__(self, log_file_name):
        self.log_file_name = log_file_name
        self._dict = None
        # End of __init__().

    def _parse(self):
        tmp = "^.*{0:s}_(.*)_(run[0-9]+)_(file[0-9]+)_({1:s}).*\.{2:s}(\.gz)?$"
        regex_str = tmp.format(TTSLogFileName.PI_TTS_LOG_FILE_NAME_PREFIX,
                               TTSLogFileName.PI_TTS_LOG_FILE_NAME_TIMESTAMP_FMT_REGEX,
                               TTSLogFileName.PI_TTS_LOG_FILE_NAME_EXTENSION)
        regex = re.compile(regex_str)
        match = regex.match(self.log_file_name)
        if not match:
            msg_base = "Failed to parse TTS history file name '{0:s}'."
            msg = msg_base.format(self.log_file_name)
            raise ValueError(msg)
        service_str = match.group(1)
        run_number_str = match.group(2)
        run_number = int(run_number_str.replace("run", ""))
        file_number_str = match.group(3)
        file_number = int(file_number_str.replace("file", ""))
        timestamp_str = match.group(4)
        timestamp = datetime.datetime.strptime(timestamp_str,
                                               TTSLogFileName.PI_TTS_LOG_FILE_NAME_TIMESTAMP_FMT)
        timestamp.replace(tzinfo=utc)
        self._dict = {
            'dir_name': os.path.dirname(self.log_file_name),
            'prefix': TTSLogFileName.PI_TTS_LOG_FILE_NAME_PREFIX,
            'service': service_str,
            'run_number': run_number,
            'file_number': file_number,
            'timestamp': timestamp,
            'extension': TTSLogFileName.PI_TTS_LOG_FILE_NAME_EXTENSION
        }
        # End of _parse().

    def __getattr__(self, name):
        if not self._dict:
            self._parse()
        res = self._dict[name]
        # End of __getattr__().
        return res

    def __str__(self):
        res = self.log_file_name
        # End of __str__().
        return res

    # End of class TTSLogFileName.

######################################################################

class TTSLogLockFileName(object):

    PI_TTS_LOG_LOCK_FILE_NAME_PATTERN = FileLocker.LOCK_FILE_PREFIX \
                                        + TTSLogFileName.PI_TTS_LOG_FILE_NAME_PATTERN \
                                        + FileLocker.LOCK_FILE_SUFFIX

    def __init__(self, lock_file_name):
        self.lock_file_name = lock_file_name
        self.log_file_name = None
        self._dict = None
        # End of __init__().

    def _parse(self):
        tmp = self.lock_file_name
        pos = tmp.find(FileLocker.LOCK_FILE_PREFIX)
        if pos >= 0:
            tmp = tmp[pos+1:]
        if tmp.endswith(FileLocker.LOCK_FILE_SUFFIX):
            tmp = tmp[:len(tmp) - len(FileLocker.LOCK_FILE_SUFFIX)]
        self.log_file_name = TTSLogFileName(tmp)
        # End of _parse().

    def __getattr__(self, name):
        if not self._dict:
            self._parse()
        res = self.log_file_name.__getattr__(name)
        # End of __getattr__().
        return res

    def __str__(self):
        res = self.lock_file_name
        # End of __str__().
        return res

    # End of class TTSLogLockFileName.

######################################################################

class LogHandlerBase(object):
    __metaclass__ = abc.ABCMeta

    def __init__(self, debug_mode=False, force_mode=False):
        self._sep_line = "-" * 50
        self._summary_lines = []
        self.debug_mode = debug_mode
        self.force_mode = force_mode
        self.config_file_name = DEFAULT_CFG_FILE_NAME
        # End of __init__().

    def _preload_config(self):
        # NOTE: The configuration contains the logging details, so
        # until we're sure we have managed to load the configuration,
        # we just throw an exception and let the crash handler handle
        # things.
        defaults = {
            "misc": {"log_level": "INFO"}
            }
        config = ConfigParser.ConfigParser()
        for (section_name, section_contents) in defaults.iteritems():
            config.add_section(section_name)
            for (key, val) in section_contents.iteritems():
                config.set(section_name, key, str(val))
        try:
            with open(self.config_file_name) as config_file:
                config.readfp(config_file)
        except Exception as err:
            msg = "Failed to open configuration file: '{0:s}'."
            raise RuntimeError(msg.format(str(err)))
        self._config = config

        #----------

        # Some miscellaneous things to sort out.
        tmp = self.get_config_par("misc", "log_level")
        numeric_level = getattr(logging, tmp.upper(), None)
        if not isinstance(numeric_level, int):
            raise ValueError("Invalid log level: '{0:s}.".format(tmp))
        self._log_level = numeric_level

        # End of _preload_config().

    def _load_config(self):

        # Figure out if, and where, to send summary emails
        self.summary_mail_to = []
        try:
            self.summary_mail_to = self.get_config_par("summary", "summary_mail_to")
        except (ConfigParser.NoSectionError, ConfigParser.NoOptionError):
            pass
        try:
            self.summary_mail_from = self.get_config_par("summary", "summary_mail_from")
        except (ConfigParser.NoSectionError, ConfigParser.NoOptionError):
            if self.summary_mail_to:
                self.summary_mail_from = self.summary_mail_to.split(",")[0].strip()
        try:
            self.summary_mail_smtp_server = self.get_config_par("summary", "summary_mail_smtp_server")
        except (ConfigParser.NoSectionError, ConfigParser.NoOptionError):
            self.summary_mail_smtp_server = "localhost"

        # End of _load_config().

    def _setup_logging(self):
        # This sets up the 'normal' logging for the class (as opposed
        # to the 'emergency' logging in case of a crash).
        logger = logging.getLogger(self.class_name())
        logger.setLevel(self._log_level)

        # A console handler for while debugging.
        if self.debug_mode:
            handler = logging.StreamHandler()
            handler.setLevel(logging.DEBUG)
            formatter = logging.Formatter(LOGGING_FMT)
            handler.setFormatter(formatter)
            logger.addHandler(handler)

        # A simple file handler for in production.
        # NOTE: Log file rotation is handled externally, by logrotate,
        # as it is for all other log files.
        log_file_path_name = "/var/log"
        script_name = self.script_name()
        script_name_without_ext = os.path.splitext(script_name)[0]
        class_name = self.class_name()
        log_file_base_name = "{0:s}_{1:s}.log".format(script_name_without_ext,
                                                      class_name.lower())
        log_file_name = os.path.join(log_file_path_name, log_file_base_name)
        handler = logging.FileHandler(log_file_name)

        formatter = logging.Formatter(LOGGING_FMT)
        handler.setFormatter(formatter)
        logger.addHandler(handler)
        self._logger = logger
        # End of _setup_logging().

    def script_name(self):
        res = os.path.basename(__file__)
        # End of script_name().
        return res

    def class_name(self):
        res = self.__class__.__name__
        # End of class_name().
        return res

    def log_debug(self, msg):
        self._logger.debug(msg)
        # End of log_debug().

    def log_info(self, msg):
        self._logger.info(msg)
        # End of log_info().

    def log_warn(self, msg):
        self._logger.warn(msg)
        # End of log_warn().

    def log_error(self, msg):
        self._logger.error(msg)
        # End of log_error().

    def log_critical(self, msg):
        self._logger.critical(msg)
        # End of log_critical().

    def open_log(self):
        self.log_info(self._sep_line)
        script_name = self.script_name()
        class_name = self.class_name()
        host_name = get_formatted_host_name(socket)
        msg_tmp = "Starting {0:s} ({1:s}) on {2:s}."
        msg = msg_tmp.format(script_name, class_name, host_name)
        self.log_info(msg)
        # End of open_log().

    def close_log(self):
        script_name = self.script_name()
        class_name = self.class_name()
        host_name = get_formatted_host_name(socket)
        msg_tmp = "Finished {0:s} ({1:s}) on {2:s}."
        msg = msg_tmp.format(script_name, class_name, host_name)
        self.log_info(msg)
        self.log_info(self._sep_line)
        # End of close_log().

    def get_config_par(self, section, option):
        # End of get_config_par().
        return self._config.get(section, option)

    @abc.abstractmethod
    def main(self):
        "The main method. Of course to be implemented by subclasses."
        # End of main().

    def run(self):
        # Load the (first part of the) configuration.
        self._preload_config()

        # Then: setup the logging business.
        self._setup_logging()

        #----------

        self.open_log()

        # And then load the rest of the configuration.
        msg_base = "Reading configuration from file '{0:s}'."
        msg = msg_base.format(self.config_file_name)
        self.log_info(msg)
        try:
            self._load_config()
        except Exception as err:
            msg_base = "Failed to load configuration: '{0:s}'."
            msg = msg_base.format(str(err))
            self.log_critical(msg)
            self.close_log()
            raise RuntimeError(msg)

        self.log_debug("Configuration loaded, and logging set up.")

        #----------

        time_before = datetime.datetime.utcnow()
        res = self.main()
        time_after = datetime.datetime.utcnow()

        if len(self._summary_lines):
            time_spent = time_after - time_before
            self._summary_lines.append("Time spent: {0:s}.".format(format_timedelta(time_spent)))

        for line in self._summary_lines:
            self.log_info(line)

        #----------

        if self.summary_mail_to and self._summary_lines:
            msg = MIMEText(os.linesep.join(self._summary_lines))
            # script_name = os.path.basename(__file__)
            class_name = self.class_name()
            host_name = get_formatted_host_name(socket)
            tmp = "Summary mail from TCDS TTS history handler '{0:s}' on {1:s}"
            msg['Subject'] = tmp.format(class_name, host_name)
            msg['From'] = self.summary_mail_from
            msg['To'] = self.summary_mail_to
            try:
                server = smtplib.SMTP(self.summary_mail_smtp_server)
                server.sendmail(self.summary_mail_from,
                                self.summary_mail_to,
                                msg.as_string())
                server.quit()
            except Exception as err:
                msg = "Failed to send summary email: '{0:s}'."
                self.log_error(msg.format(str(err)))

        #----------

        self.log_info("All done.")
        self.close_log()

        # End of run().
        return res

    # End of class LogHandlerBase.

######################################################################

class LogMover(LogHandlerBase):

    def _load_config(self):
        super(LogMover, self)._load_config()

        # Figure out from where to where files should be moved.
        try:
            tmp_src = self.get_config_par("paths", "source_path")
        except (ConfigParser.NoSectionError, ConfigParser.NoOptionError):
            msg = "Failed to find section 'paths'," \
                  + " option 'source_path' in config file."
            raise RuntimeError(msg)
        try:
            tmp_dst = self.get_config_par("paths", "destination_path")
        except (ConfigParser.NoSectionError, ConfigParser.NoOptionError):
            msg = "Failed to find section 'paths'," \
                  + " option 'destination_path' in config file."
            raise RuntimeError(msg)
        self.path_src = os.path.realpath(tmp_src)
        self.path_dst = os.path.realpath(tmp_dst)

        # End of _load_config().

    def create_destination_area(self, dst_area):
        # Make sure the destination area exists.
        path_dst_full = os.path.join(self.path_dst, dst_area)
        try:
            os.makedirs(path_dst_full)
            self.log_debug("  Successfully created destination area.")
        except Exception as err:
            self.log_debug("  Successfully detected existing destination area.")
        # End of create_destination_area().

    def update_links(self, dst_file_name):
        tmp = TTSLogFileName(dst_file_name)

        # One structure based on the date embedded in the file name.
        datestamp = tmp.timestamp.date()
        year = datestamp.year
        month = datestamp.month
        day = datestamp.day
        year_str = str(year)
        month_str = "{0:02d}".format(month)
        day_str = "{0:02d}".format(day)
        sub_dir_names = [self.path_dst, "by_date", year_str, month_str, day_str]
        sub_dir_name = os.path.join(*sub_dir_names)
        self.create_destination_area(sub_dir_name)
        dst_file_base_name = os.path.basename(dst_file_name)
        link_name = os.path.join(sub_dir_name, dst_file_base_name)
        try:
            os.symlink(dst_file_name, link_name)
        except OSError as err:
            # Upon manual (re)insertion it can happen that the link
            # already exists.
            if str(err).find("File exists") < 0:
                raise

        # One structure based on the application service name embedded
        # in the file name.
        service_str = tmp.service
        sub_dir_names = [self.path_dst, "by_service", service_str]
        sub_dir_name = os.path.join(*sub_dir_names)
        self.create_destination_area(sub_dir_name)
        dst_file_base_name = os.path.basename(dst_file_name)
        link_name = os.path.join(sub_dir_name, dst_file_base_name)
        try:
            os.symlink(dst_file_name, link_name)
        except OSError as err:
            # Upon manual (re)insertion it can happen that the link
            # already exists.
            if str(err).find("File exists") < 0:
                raise

        # End of update_links()

    def main(self):

        self.log_info("TTS log file source path: '{0:s}'.".format(self.path_src))
        self.log_info("TTS log destination path: '{0:s}'.".format(self.path_dst))

        # Check: both the source and the destination area are expected
        # to exist already.
        if not (os.path.exists(self.path_src) and os.path.isdir(self.path_src)):
            msg_base = "Source path '{0:s}' does not exist."
            msg = msg_base.format(self.path_src)
            self.log_critical(msg)
            self.close_log()
            raise RuntimeError(msg)
        if not (os.path.exists(self.path_dst) and os.path.isdir(self.path_dst)):
            msg_base = "Destination path '{0:s}' does not exist."
            msg = msg_base.format(self.path_dst)
            self.log_critical(msg)
            self.close_log()
            raise RuntimeError(msg)

        # Build a list of files to consider for transport.
        in_file_names = glob.glob(os.path.join(self.path_src,
                                               TTSLogFileName.PI_TTS_LOG_FILE_NAME_PATTERN))
        self.log_info("Found {0:d} file(s) to move.".format(len(in_file_names)))

        # Now process all files one by one.
        file_data = {}
        for in_file_name in sorted(in_file_names):
            self.log_debug("Processing file '{0:s}'.".format(in_file_name))
            file_data[in_file_name] = FileInfo(in_file_name,
                                               FileInfo.STATUS_FILE_FOUND,
                                               0,
                                               0,
                                               "File found")
            # Check if we can get a lock on this file.
            in_file_base_name = os.path.basename(in_file_name)
            lock_file_base_name = FileLocker.create_lock_file_name(in_file_name)
            lock_file_name = os.path.join(self.path_src, lock_file_base_name)
            self.log_debug("  Lock file name: '{0:s}'.".format(lock_file_name))
            try:
                with FileLocker(lock_file_name) as lock_file:
                    self.log_debug("  Successfully obtained file lock.")
                    size_raw = os.stat(in_file_name).st_size
                    file_data[in_file_name] = FileInfo(in_file_name,
                                                       FileInfo.STATUS_FILE_LOCKED,
                                                       size_raw,
                                                       0,
                                                       "Lock obtained")

                    try:
                        # Zip up the input file.
                        zip_file_base_name = in_file_base_name + ".gz"
                        zip_file_name = os.path.join(self.path_src, zip_file_base_name)
                        with open(in_file_name) as src, \
                             gzip.open(zip_file_name, 'wb') as dst:
                            dst.writelines(src)
                        self.log_debug("  Successfully created zipped input file.")
                        size_zipped = os.stat(zip_file_name).st_size
                        file_data[in_file_name] = FileInfo(in_file_name,
                                                           FileInfo.STATUS_FILE_ZIPPED,
                                                           size_raw,
                                                           size_zipped,
                                                           "File zipped")

                        # Determine where the file should be moved to.
                        # NOTE: The destination area is subdivided
                        # into sub-areas based on the run number. This
                        # follows the approach that is also used in
                        # EOS to store the data.
                        # - Run numbers with at most 9 digits (e.g.,
                        #   123456789) are stored in sub-areas
                        #   123/456/789.
                        # - Run numbers with more digits must be
                        #   special runs and are stored underneath a
                        #   'special' sub-area.
                        tmp = TTSLogFileName(in_file_name)
                        run_number = tmp.run_number
                        run_number_str = "{0:09d}".format(run_number)
                        if len(run_number_str) > 9:
                            dst_area = os.path.join("special_run_numbers", run_number_str)
                        else:
                            tmp = [run_number_str[i:i + 3] \
                                   for i in range(0, len(run_number_str), 3)]
                            dst_area = os.path.join(*tmp)

                        (uid, gid) = determine_dir_owner(self.path_dst)

                        # Make sure the destination area exists.
                        path_dst_full = os.path.join(*[self.path_dst, "by_run_number", dst_area])
                        with DroppedPrivs(uid, gid) as dropped_privs:
                            self.create_destination_area(path_dst_full)

                        # Move the zipped file over to the destination.
                        # NOTE: This is a copy + delete, because the
                        # two steps need different permissions.
                        dst_file_name = os.path.join(path_dst_full, zip_file_base_name)
                        with DroppedPrivs(uid, gid) as dropped_privs:
                            shutil.copy(zip_file_name, dst_file_name)
                        os.remove(zip_file_name)
                        self.log_debug("  Successfully moved zip file to destination.")
                        file_data[in_file_name] = FileInfo(in_file_name,
                                                           FileInfo.STATUS_FILE_MOVED,
                                                           size_raw,
                                                           size_zipped,
                                                           "File zipped and moved")

                        # Now do some extra stuff: create a separate
                        # directory structure (populated with links to
                        # the real files) that can help find specific
                        # log files.
                        with DroppedPrivs(uid, gid) as dropped_privs:
                            self.update_links(dst_file_name)

                        # Remove the original (unzipped) input file.
                        # NOTE: This should be the last step, because
                        # it is the presence of the original file that
                        # will trigger a next attempt, if necessary.
                        os.remove(in_file_name)
                        self.log_debug("  Successfully removed original input file.")

                    except Exception as err:
                        # Cleanup the zip file, if it's there.
                        try:
                            os.remove(zip_file_name)
                        except:
                            pass
                        msg_base = "  Problem processing file '{0:s}': '{1:s}'."
                        self.log_error(msg_base.format(in_file_name, str(err)))
                        file_data[in_file_name] = FileInfo(in_file_name,
                                                           FileInfo.STATUS_FILE_PROBLEMATIC,
                                                           0,
                                                           0,
                                                           "File problematic: {0:s}".format(str(err)))

            except Exception as err:
                msg_base = "  Failed to obtain lock on file '{0:s}': '{1:s}'."
                err_str = str(err)
                err_detail = err_str
                file_exists = err_str.find("File exists") > -1
                if file_exists:
                    err_detail = "File is locked by another process."
                    file_data[in_file_name] = FileInfo(in_file_name,
                                                       FileInfo.STATUS_FILE_UNLOCKABLE,
                                                       0,
                                                       0,
                                                       "File locked")

                else:
                    file_data[in_file_name] = FileInfo(in_file_name,
                                                       FileInfo.STATUS_FILE_PROBLEMATIC,
                                                       0,
                                                       0,
                                                       "File problematic (can't obtain lock): {0:s}".format(str(err)))
                self.log_warn(msg_base.format(in_file_name, err_detail))
                if not file_exists:
                    raise

        #----------

        if len(file_data):
            # NOTE: In case there were only locked files, let's not
            # report anything. That situation is the status quo, so
            # let's save ourselves some emails.
            tmp = [i for i in file_data.values() if (i.status != FileInfo.STATUS_FILE_UNLOCKABLE)]
            if len(tmp) or self.debug_mode:
                self._summary_lines.append("Processed {0:d} files:".format(len(file_data)))
                max_len_file_name = max((len(i) for i in file_data.keys()))
                for (file_name, file_info) in sorted(file_data.iteritems()):
                    size_raw = file_info.size_raw / 1024.
                    size_zipped = file_info.size_zipped / 1024.
                    file_msg = file_info.msg
                    msg_base = "  {1:{0:d}s}: {2:s}"
                    if file_info.status == FileInfo.STATUS_FILE_MOVED:
                        # This means we successfully processed the file.
                        file_msg_tmp = " -> plain size/zipped size = {0:6.1f}/{1:6.2f} kB ({2:.1f}%)."
                        fraction = 100. * size_zipped / size_raw
                        file_msg_tmp = file_msg_tmp.format(size_raw, size_zipped, fraction)
                        file_msg += file_msg_tmp
                    self._summary_lines.append(msg_base.format(max_len_file_name, file_name, file_msg))
            else:
                self.log_info("Found only locked files. Skipping sending of summary email.")

        # End of main().
        return 0

    # End of class LogMover.

######################################################################

class LogCleaner(LogHandlerBase):

    def _load_config(self):
        super(LogCleaner, self)._load_config()

        # Figure out which area we need to clean.
        try:
            tmp_dst = self.get_config_par("paths", "destination_path")
        except (ConfigParser.NoSectionError, ConfigParser.NoOptionError):
            msg = "Failed to find section 'paths'," \
                  + " option 'destination_path' in config file."
            raise RuntimeError(msg)
        self.path_dst = os.path.realpath(tmp_dst)

        # Figure out how strictly we have to clean.
        try:
            tmp_min_free_space_size = self.get_config_par("cleaner", "min_free_space")
        except (ConfigParser.NoSectionError, ConfigParser.NoOptionError):
            msg = "Failed to find section 'cleaner'," \
                  + " option 'min_free_space' in config file."
            raise RuntimeError(msg)
        try:
            tmp_min_free_space_fraction = self.get_config_par("cleaner", "min_free_space_fraction")
        except (ConfigParser.NoSectionError, ConfigParser.NoOptionError):
            msg = "Failed to find section 'cleaner'," \
                  + " option 'min_free_space_fraction' in config file."
            raise RuntimeError(msg)
        try:
            tmp_min_free_inode_fraction = self.get_config_par("cleaner", "min_free_inode_fraction")
        except (ConfigParser.NoSectionError, ConfigParser.NoOptionError):
            msg = "Failed to find section 'cleaner'," \
                  + " option 'min_free_inode_fraction' in config file."
            raise RuntimeError(msg)

        try:
            min_free_space_size = int(tmp_min_free_space_size)
        except Exception as err:
            msg_base = "The value {0:s} for the configuration parameter " \
                       "min_free_space does not sound right."
            msg = msg_base.format(tmp_min_free_space_size)
            raise ValueError(msg)

        try:
            min_free_space_fraction = float(tmp_min_free_space_fraction)
        except Exception as err:
            msg_base = "The value {0:s} for the configuration parameter " \
                       "min_free_space_fraction does not sound right."
            msg = msg_base.format(tmp_min_free_space_fraction)
            raise ValueError(msg)

        try:
            min_free_inode_fraction = float(tmp_min_free_inode_fraction)
        except Exception as err:
            msg_base = "The value {0:s} for the configuration parameter " \
                       "min_free_inode_fraction does not sound right."
            msg = msg_base.format(tmp_min_free_inode_fraction)
            raise ValueError(msg)

        if (min_free_space_fraction <= 0) or (min_free_space_fraction >= 1):
            msg_base = "The value {0:s} for the configuration parameter " \
                       "min_free_space_fraction does not sound right."
            msg = msg_base.format(tmp_min_free_space_fraction)
            raise ValueError(msg)

        if (min_free_inode_fraction <= 0) or (min_free_inode_fraction >= 1):
            msg_base = "The value {0:s} for the configuration parameter " \
                       "min_free_inode_fraction does not sound right."
            msg = msg_base.format(tmp_min_free_inode_fraction)
            raise ValueError(msg)

        self.min_free_space_size = min_free_space_size
        self.min_free_space_fraction = min_free_space_fraction
        self.min_free_inode_fraction = min_free_inode_fraction

        # End of _load_config().

    def main(self):

        self.log_info("TTS log destination path: '{0:s}'.".format(self.path_dst))

        # Check: the destination area is expected to exist already.
        if not (os.path.exists(self.path_dst) and os.path.isdir(self.path_dst)):
            msg_base = "Destination path '{0:s}' does not exist."
            msg = msg_base.format(self.path_dst)
            self.log_critical(msg)
            self.close_log()
            raise RuntimeError(msg)

        # Let's figure out how much space there is free on the
        # destination area.
        tmp = get_disk_space_info(self.path_dst)
        dst_size_in_bytes = tmp[0]
        dst_avail_bytes = tmp[2]
        dst_size_in_inodes = tmp[3]
        dst_avail_inodes = tmp[4]

        dst_size_str = format_disk_space_string(dst_size_in_bytes)
        self.log_info("TTS history destination area size: {0:s}.".format(dst_size_str))
        dst_avail_str = format_disk_space_string(dst_avail_bytes)
        msg_base = "TTS history destination area available space: {0:s} ({1:.1f}% of space, {2:.1f}% of inodes)."
        msg = msg_base.format(dst_avail_str,
                              100. * dst_avail_bytes / dst_size_in_bytes,
                              100. * dst_avail_inodes / dst_size_in_inodes)
        self.log_info(msg)

        #----------

        # Check if we need to clean anything or not.
        min_free_space_size_str = format_disk_space_string(self.min_free_space_size)
        msg_base = "Minimum required free space: {0:s}, {1:.1f}% of disk size, and {2:.1f}% of inodes."
        msg = msg_base.format(min_free_space_size_str,
                              100. * self.min_free_space_fraction,
                              100. * self.min_free_inode_fraction)
        self.log_info(msg)

        cleanup_needed = self.is_cleanup_needed()
        if not cleanup_needed:
            self.log_info("No need for cleanup.")
        else:
            self.log_info("Need to perform cleanup.")

        cleanup_steps = [
            # Step 1: Remove all files from special runs (i.e., DAQVal
            # etc.) from more than 24 hours ago.
            (lambda: self.cleanup_special_run_files(older_than=datetime.timedelta(days=1)),
             "Remove 'DAQVal etc.' files from more than a day old."),
            # Step 2: Remove files from normal runs (i.e., miniDAQ and
            # central DAQ), run by run, starting from the oldest run,
            # and leave all files less than a week old.
            (lambda: self.cleanup_normal_run_files(older_than=datetime.timedelta(days=7)),
             "Remove files from normal runs from at least seven days old, run by run, starting at the oldest run."),
            # Step 3: Remove all files from special runs (i.e., DAQVal
            # etc.) from more than 1 hour ago.
            (lambda: self.cleanup_special_run_files(older_than=datetime.timedelta(hours=1)),
             "Remove 'DAQVal etc.' files from more than an hour old."),
            # Step 4: Remove files from normal runs (i.e., miniDAQ and
            # central DAQ), run by run, starting from the oldest run,
            # and leave all files less than a day old.
            (lambda: self.cleanup_normal_run_files(older_than=datetime.timedelta(hours=1)),
             "Remove files from normal runs from at least an hour old, run by run, starting at the oldest run."),
            ]
        files_removed = {}
        if cleanup_needed:
            for (step_num, (cleaner_func, cleaner_func_desc)) in enumerate(cleanup_steps):
                self.log_info("Cleanup step {0:d}: '{1:s}'.".format(step_num, cleaner_func_desc))

                files_removed_this_step = cleaner_func()
                files_removed[step_num] = files_removed_this_step

                cleanup_needed = self.is_cleanup_needed()
                cleanup_done = not cleanup_needed
                if cleanup_done:
                    break

            if not cleanup_needed:
                self.log_info("  Successfully cleaned up enough space.")
            else:
                self.log_error("Failed to clean up enough space.")
                tmp = get_disk_space_info(self.path_dst)
                dst_size_in_bytes = tmp[0]
                dst_avail_bytes = tmp[2]
                dst_size_in_inodes = tmp[3]
                dst_avail_inodes = tmp[4]
                dst_avail_str = format_disk_space_string(dst_avail_bytes)
                msg_base = "TTS history destination area available space: {0:s} ({1:.1f}% of space, {2:.1f}% of inodes)."
                msg = msg_base.format(dst_avail_str,
                                      100. * dst_avail_bytes / dst_size_in_bytes,
                                      100. * dst_avail_inodes / dst_size_in_inodes)
                self.log_error(msg)
                min_free_space_size_str = format_disk_space_string(self.min_free_space_size)
                msg_base = "Minimum required free space: {0:s} and {1:.1f}% of disk size."
                msg = msg_base.format(min_free_space_size_str, 100. * self.min_free_space_fraction)
                self.log_error(msg)

        #----------

        # Clean up dead symlinks after the file removal.
        if sum([len(i) for i in files_removed.values()]):
            self.log_info("Checking for broken symlinks after removing files.")
            broken_links = find_broken_links_recursively(self.path_dst)
            msg_base = "  Found {0:d} broken symlinks."
            msg = msg_base.format(len(broken_links))
            self.log_info(msg)
            (uid, gid) = determine_dir_owner(self.path_dst)
            with DroppedPrivs(uid, gid) as dropped_privs:
                for link_name in broken_links:
                    self.log_info("    {0:s}".format(link_name))
                    os.remove(link_name)
            found_broken_links = (len(broken_links) != 0)
            if found_broken_links:
                self.log_info("  Successfully removed all broken symlinks.")

        # Clean up empty directories after the file removal and
        # symlink cleanup.
        if sum([len(i) for i in files_removed.values()]):
            self.log_info("Checking for empty directories after removing files.")
            empty_dirs = find_empty_subdirs_recursively(self.path_dst)
            msg_base = "  Found {0:d} empty directories."
            msg = msg_base.format(len(empty_dirs))
            self.log_info(msg)
            found_empty_dirs = False
            if empty_dirs:
                found_empty_dirs = True
                self.log_info("  Recursively removing empty directories.")
                pass_num = 1
                # NOTE: This is recursive, because one pass could
                # remove all sub-directories and files at a given
                # level, thereby emptying a directory one level up.
                while empty_dirs:
                    msg_base = "    Removing {0:d} empty directories."
                    msg = msg_base.format(len(empty_dirs))
                    self.log_info(msg)
                    for dir_name in empty_dirs:
                        self.log_info("      {0:s}".format(dir_name))
                        os.rmdir(dir_name)
                    empty_dirs = find_empty_subdirs_recursively(self.path_dst)
            if found_empty_dirs:
                self.log_info("  Successfully removed all empty directories.")

        #----------

        for (step_num, files_removed_in_step) in files_removed.iteritems():
            msg_base = "Cleanup step {0:d}: '{1:s}' -> removed {2:d} files."
            msg = msg_base.format(step_num, cleanup_steps[step_num][1], len(files_removed_in_step))
            self._summary_lines.append(msg)

        tmp = get_disk_space_info(self.path_dst)
        dst_size_in_bytes = tmp[0]
        dst_avail_bytes = tmp[2]
        dst_size_in_inodes = tmp[3]
        dst_avail_inodes = tmp[4]
        dst_avail_str = format_disk_space_string(dst_avail_bytes)
        msg_base = "TTS history destination area available space: {0:s} ({1:.1f}% of space, {2:.1f}% of inodes)."
        msg = msg_base.format(dst_avail_str,
                              100. * dst_avail_bytes / dst_size_in_bytes,
                              100. * dst_avail_inodes / dst_size_in_inodes)
        self._summary_lines.append(msg)
        if self.is_cleanup_needed():
            self._summary_lines.append("Disk space requirements were NOT met, " \
                                       "even after cleanup (attempt).")

        # End of main().
        return 0

    def _is_size_exceeded(self, disk_space_info):
        dst_size_in_bytes = disk_space_info[0]
        dst_avail_bytes = disk_space_info[2]
        is_size_exceeded = (dst_avail_bytes < self.min_free_space_size)
        # End of _is_size_exceeded().
        return is_size_exceeded

    def _is_size_fraction_exceeded(self, disk_space_info):
        dst_size_in_bytes = disk_space_info[0]
        dst_avail_bytes = disk_space_info[2]
        fraction_avail = 1. * dst_avail_bytes / dst_size_in_bytes
        is_size_fraction_exceeded = (fraction_avail < self.min_free_space_fraction)
        # End of _is_size_fraction_exceeded().
        return is_size_fraction_exceeded

    def _is_inode_fraction_exceeded(self, disk_space_info):
        num_inodes = disk_space_info[3]
        num_inodes_free = disk_space_info[4]
        fraction_avail = 1. * num_inodes_free / num_inodes
        is_inode_fraction_exceeded = (fraction_avail < self.min_free_inode_fraction)

        # End of _is_inode_fraction_exceeded().
        return is_inode_fraction_exceeded

    def is_cleanup_needed(self):
        disk_space_info = get_disk_space_info(self.path_dst)
        cleanup_needed = self._is_size_exceeded(disk_space_info) or \
                         self._is_size_fraction_exceeded(disk_space_info) or \
                         self._is_inode_fraction_exceeded(disk_space_info)
        # End of is_cleanup_needed().
        return cleanup_needed

    def cleanup_special_run_files(self, older_than):
        # Find all files from special runs (i.e., DAQVal etc.) from
        # more than older_than time ago.
        msg_base = "    Looking for 'special run' files older than {0:s}."
        msg = msg_base.format(str(older_than))
        self.log_debug(msg)
        path = os.path.join(*[self.path_dst,
                              "by_run_number",
                              "special_run_numbers"])
        special_run_files = find_files_recursively(path)
        msg_base = "    Found {0:d} 'special run' files to consider for removal."
        msg = msg_base.format(len(special_run_files))
        self.log_debug(msg)

        max_delta_timestamp = older_than
        now = datetime.datetime.utcnow()
        now.replace(tzinfo=utc)
        special_run_files_to_remove = []
        for file_name in special_run_files:
            tmp = TTSLogFileName(file_name)
            if (now - tmp.timestamp) > max_delta_timestamp:
                special_run_files_to_remove.append(file_name)

        msg_base = "    Found {0:d} 'special run' files matching criteria to remove."
        msg = msg_base.format(len(special_run_files_to_remove))
        self.log_debug(msg)

        # Remove those files.
        (uid, gid) = determine_dir_owner(path)
        with DroppedPrivs(uid, gid) as dropped_privs:
            for file_name in special_run_files_to_remove:
                os.remove(file_name)
        files_removed = special_run_files_to_remove

        # End of cleanup_special_run_files().
        return files_removed

    def cleanup_normal_run_files(self, older_than):
        # Find the oldest run in the destination area.
        msg_base = "    Looking for the oldest 'normal run' older than {0:s}."
        msg = msg_base.format(str(older_than))
        self.log_debug(msg)
        path = os.path.join(*[self.path_dst,
                              "by_run_number"])
        file_names_tmp = find_files_recursively(path)
        # NOTE: Filter out the 'special run' files.
        file_names = [i for i in file_names_tmp \
                      if i.find("special_run_numbers") < 0]
        run_numbers_tmp = [TTSLogFileName(i).run_number for i in file_names]
        run_numbers = list(set(run_numbers_tmp))
        msg_base = "    Found {0:d} 'normal run' files spanning {1:d} runs."
        msg = msg_base.format(len(file_names), len(run_numbers))
        self.log_debug(msg)

        files_removed = []
        for earliest_run in sorted(run_numbers):
            msg_base = "    Oldest 'normal run' found: {0:d}."
            msg = msg_base.format(earliest_run)
            self.log_debug(msg)

            # Find the corresponding files.
            earliest_run_files = [i for i in file_names \
                                  if TTSLogFileName(i).run_number == earliest_run]

            # Filter by older_than.
            max_delta_timestamp = older_than
            now = datetime.datetime.utcnow()
            now.replace(tzinfo=utc)
            files_to_remove = []
            for file_name in earliest_run_files:
                tmp = TTSLogFileName(file_name)
                if (now - tmp.timestamp) > max_delta_timestamp:
                    files_to_remove.append(file_name)

            if files_to_remove:
                msg_base = "    Found {0:d} 'normal run' files corresponding to run {1:d} to remove."
                msg = msg_base.format(len(files_to_remove), earliest_run)
            else:
                msg_base = "    Files from 'normal run' {0:d} are not old enough to remove."
                msg = msg_base.format(earliest_run)
            self.log_debug(msg)

            # Remove those files.
            (uid, gid) = determine_dir_owner(path)
            with DroppedPrivs(uid, gid) as dropped_privs:
                for file_name in files_to_remove:
                    os.remove(file_name)
            files_removed.extend(files_to_remove)

            cleanup_needed = self.is_cleanup_needed()
            cleanup_done = not cleanup_needed
            if cleanup_done:
                break

        # End of cleanup_normal_run_files().
        return files_removed

    # End of class LogCleaner.

######################################################################

class LogChecker(LogHandlerBase):

    def _load_config(self):
        super(LogChecker, self)._load_config()

        # Figure out which area we need to check.
        try:
            tmp_src = self.get_config_par("paths", "source_path")
        except (ConfigParser.NoSectionError, ConfigParser.NoOptionError):
            msg = "Failed to find section 'paths'," \
                  + " option 'source_path' in config file."
            raise RuntimeError(msg)
        self.path_src = os.path.realpath(tmp_src)

        # End of _load_config().

    def main(self):

        self.log_info("TTS log file source path: '{0:s}'.".format(self.path_src))

        # Check: the destination area is expected to exist already.
        if not (os.path.exists(self.path_src) and os.path.isdir(self.path_src)):
            msg_base = "Source path '{0:s}' does not exist."
            msg = msg_base.format(self.path_src)
            self.log_critical(msg)
            self.close_log()
            raise RuntimeError(msg)

        #----------

        # Check for 'suspicious' locked files.
        #   1) Find all lock files in the source area.
        lock_file_names = glob.glob(os.path.join(self.path_src,
                                                 TTSLogLockFileName.PI_TTS_LOG_LOCK_FILE_NAME_PATTERN))
        self.log_debug("Found {0:d} lock file(s).".format(len(lock_file_names)))
        #  2) Check if there are services for which there are multiple
        #     lock files.
        tmp = [TTSLogLockFileName(i) for i in lock_file_names]
        stats = {}
        for i in tmp:
            try:
                stats[i.service].append(i.lock_file_name)
            except KeyError:
                stats[i.service] = [i.lock_file_name]

        suspicious_services = {}
        for (service_name, lock_file_names) in stats.iteritems():
            if len(lock_file_names) != 1:
                suspicious_services[service_name] = lock_file_names

        if len(suspicious_services):
            # Sort suspicious files in order.
            for (service_name, lock_file_names) in suspicious_services.iteritems():
                tmp = [TTSLogLockFileName(i) for i in lock_file_names]
                tmp.sort(key=lambda x: (x.service, x.run_number, x.file_number, x.timestamp))
                suspicious_services[service_name] = tmp

            for (service_name, lock_file_names) in suspicious_services.iteritems():
                msg_base = "Found a suspicious number of lock files for service {0:s}:"
                msg = msg_base.format(service_name)
                self.log_warn(msg)
                self._summary_lines.append(msg)
                for (i, lock_file_name) in enumerate(lock_file_names):
                    if self.force_mode and (i != (len(lock_file_names) - 1)):
                        # Remove stale lock file.
                        msg = "  {0:s} -> removing stale lock file".format(lock_file_name)
                        os.remove(str(lock_file_name))
                    else:
                        msg = "  {0:s}".format(lock_file_name)
                    self.log_warn(msg)
                    self._summary_lines.append(msg)
        else:
            self._summary_lines.append("Found no suspicious lock files.")

        #----------

        # End of main().
        return 0

    # End of class LogChecker.

######################################################################

if __name__ == '__main__':

    # First: setup and install our 'emergency' logger.
    # NOTE: This is really last-resort. It only catches uncaught
    # exceptions at top-level.
    script_name = os.path.basename(__file__)
    host_name = get_formatted_host_name(socket)
    logger_name = script_name + "_crash_logger"
    logger = logging.getLogger(logger_name)
    logger.setLevel(logging.CRITICAL)
    cmd_line = " ".join(sys.argv)
    subject_base = "Problem report mail from {0:s} ('{1:s}') on {2:s}"
    subject = subject_base.format(script_name, cmd_line, host_name)
    mail_handler = OneShotSMTPHandler(
        CRASH_LOG_SMTP_SERVER,
        CRASH_LOG_MAIL_FROM,
        CRASH_LOG_MAIL_TO,
        subject
    )
    mail_handler.setLevel(logging.CRITICAL)
    formatter = logging.Formatter(LOGGING_FMT)
    mail_handler.setFormatter(formatter)
    logger.addHandler(mail_handler)

    #----------

    # Then: register our own crash hook.
    sys.excepthook = crash_hook

    #----------

    # Two little helpers to enable us to catch argparser problems.
    class ArgumentParserError(Exception):
        pass

    class ThrowingArgumentParser(argparse.ArgumentParser):
        def error(self, message):
            raise ArgumentParserError(message)

    #----------

    # Now we can get to work.
    res = -999
    parser = ThrowingArgumentParser()
    parser.add_argument("--debug",
                        action='store_true')
    parser.add_argument("--force",
                        action='store_true',
                        help="Remove lock files that look stale")
    parser.add_argument("mode",
                        choices=["move", "clean", "check"],
                        help="Operation mode")
    try:
        args = parser.parse_args()
    except Exception as err:
        logger.critical("Failed to parse command line: '{0:s}'.".format(str(err)))
        res = -888
    else:
        debug_mode = args.debug
        force_mode = args.force
        mode = args.mode
        runner = None
        if mode == "move":
            runner = LogMover(debug_mode=debug_mode)
        elif mode == "clean":
            runner = LogCleaner(debug_mode=debug_mode)
        elif mode == "check":
            runner = LogChecker(debug_mode=debug_mode, force_mode=force_mode)
        else:
            logger.critical("Unknown running mode: '{0:s}'.".format(mode))
            res = -777
        if runner:
            try:
                res = runner.run()
            except (RuntimeError, ValueError) as err:
                logger.critical("Encountered a problem: '{0:s}'.".format(str(err)))
                res = -666
            except Exception as err:
                logger.critical("Encountered a problem: '{0:s}'.".format(str(err)))
                import traceback
                (exception_type, exception_value, back_trace) = sys.exc_info()
                tmp = traceback.format_exception(exception_type, exception_value, back_trace)
                trace_str = "  ".join(tmp)
                logger.critical(trace_str)
                res = -555

    # Let's treat non-zero return codes the same way we do crashes.
    if res != 0:
        logger.critical("Return code was {0:d}.".format(res))

    #----------

    # logger.info("Done")
    sys.exit(res)

######################################################################
