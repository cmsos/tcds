#include "tcds/pi/TTSLogHistory.h"

#include <map>
#include <sstream>
#include <stdint.h>
#include <utility>
#include <vector>

#include "toolbox/string.h"

#include "tcds/pi/Definitions.h"
#include "tcds/pi/TTSLogEntry.h"
#include "tcds/pi/Utils.h"
#include "tcds/utils/ConfigurationInfoSpaceHandler.h"
#include "tcds/utils/Definitions.h"
#include "tcds/utils/Utils.h"

tcds::pi::TTSLogHistory::TTSLogHistory()
{
  // Set reasonable defaults for all configuration parameters.
  std::map<tcds::definitions::TTS_SOURCE, std::string> ttsChannelLabels;
  for (unsigned int channel = tcds::definitions::TTS_CHANNEL_MIN;
       channel <= tcds::definitions::TTS_CHANNEL_MAX;
       ++channel)
    {
      tcds::definitions::TTS_SOURCE const tmp =
        static_cast<tcds::definitions::TTS_SOURCE>(channel);
      std::string const channelLabel = formatTTSChannelLabel(tmp);
      ttsChannelLabels[tmp] = channelLabel;
    }
  setTTSChannelLabels(ttsChannelLabels);

  setHistSize(1000);
}

tcds::pi::TTSLogHistory::~TTSLogHistory()
{
}

void
tcds::pi::TTSLogHistory::updateConfiguration(tcds::utils::ConfigurationInfoSpaceHandler const& cfgInfoSpace)
{
  std::map<tcds::definitions::TTS_SOURCE, std::string> ttsChannelLabels;
  for (unsigned int channel = tcds::definitions::TTS_CHANNEL_MIN;
       channel <= tcds::definitions::TTS_CHANNEL_MAX;
       ++channel)
    {
      tcds::definitions::TTS_SOURCE const tmp =
        static_cast<tcds::definitions::TTS_SOURCE>(channel);
      std::string const channelLabel = tcds::pi::formatTTSChannelLabel(tmp, cfgInfoSpace);
      ttsChannelLabels[tmp] = channelLabel;
    }
  setTTSChannelLabels(ttsChannelLabels);

  setHistSize(cfgInfoSpace.getUInt32("ttsHistSizeLive"));
}

void
tcds::pi::TTSLogHistory::setTTSChannelLabels(std::map<tcds::definitions::TTS_SOURCE,
                                             std::string> const& ttsChannelLabels)
{
  ttsChannelLabels_ = ttsChannelLabels;
}

void
tcds::pi::TTSLogHistory::setHistSize(size_t const histSize)
{
  histSize_ = histSize;
  trim();
}

void
tcds::pi::TTSLogHistory::addEntry(tcds::pi::TTSLogEntry const& logEntry)
{
  entries_.push_back(logEntry);
  trim();
}

void
tcds::pi::TTSLogHistory::clear()
{
  entries_.clear();
}

std::string
tcds::pi::TTSLogHistory::getJSONString() const
{
  std::string const colIdOrbit = "orbit";
  std::string const colIdBX = "bx";
  std::string const colIdTTSChannel = "tts";
  std::string const colIdSync = "sync";
  std::string const colIdComment = "comment";

  //----------

  // Column names/definitions.
  std::vector<std::pair<std::string, std::string> > colNames;

  // The orbit number of this TTS state change.
  colNames.push_back(std::make_pair(colIdOrbit, "Orbit"));

  // The BX number of the this TTS state change.
  colNames.push_back(std::make_pair(colIdBX, "BX"));

  // The current TTS states of all channels (i.e., after this
  // transition).
  for (unsigned int channel = tcds::definitions::TTS_CHANNEL_MIN;
       channel <= tcds::definitions::TTS_CHANNEL_MAX;
       ++channel)
    {
      std::string const channelName =
        tcds::pi::ttsChannelToRegName(static_cast<tcds::definitions::TTS_SOURCE>(channel));
      std::string const channelLabel =
        ttsChannelLabels_.at(static_cast<tcds::definitions::TTS_SOURCE>(channel));
      colNames.push_back(std::make_pair(colIdTTSChannel + "_" + channelName, channelLabel));
    }

  // A note about what triggered the most recent orbit-counter reset
  // (i.e., what this entry is synchronised to).
  colNames.push_back(std::make_pair(colIdSync, "Sync"));

  // An optional comment.
  colNames.push_back(std::make_pair(colIdComment, "Comment"));

  //----------

  std::stringstream res;

  // Start.
  res << "{";

  //----------

  // Column names.
  res << tcds::utils::escapeAsJSONString("columns") << ": {";

  for (std::vector<std::pair<std::string, std::string> >::const_iterator it = colNames.begin();
       it != colNames.end();
       ++it)
    {
      res << tcds::utils::escapeAsJSONString(it->first)
          << ": "
          << tcds::utils::escapeAsJSONString(it->second);
      if (it != (colNames.end() - 1))
        {
          res << ", ";
        }
    }

  res << "}";

  //----------

  res << ", ";

  //----------

  // Column data.
  res << tcds::utils::escapeAsJSONString("data") << ": [";

  for (std::deque<TTSLogEntry>::const_iterator it = entries_.begin();
       it != entries_.end();
       ++it)
    {
      res << "{";

      // The orbit number of this TTS state change.
      res << tcds::utils::escapeAsJSONString(colIdOrbit)
          << ": "
          << tcds::utils::escapeAsJSONString(toolbox::toString("%lu", it->orbitNumber()));
      res << ", ";

      // The BX number of the this TTS state change.
      res << tcds::utils::escapeAsJSONString(colIdBX)
          << ": "
          << tcds::utils::escapeAsJSONString(toolbox::toString("%u", it->bxNumber()));
      res << ", ";

      // The current TTS states of all channels (i.e., after this
      // transition).
      for (unsigned int channel = tcds::definitions::TTS_CHANNEL_MIN;
           channel <= tcds::definitions::TTS_CHANNEL_MAX;
           ++channel)
        {
          uint8_t const ttsVal = it->ttsVal(static_cast<tcds::definitions::TTS_SOURCE>(channel));
          std::string const channelName =
            tcds::pi::ttsChannelToRegName(static_cast<tcds::definitions::TTS_SOURCE>(channel));
          tcds::definitions::TTS_STATE const ttsState = static_cast<tcds::definitions::TTS_STATE>(ttsVal);
          res << tcds::utils::escapeAsJSONString(colIdTTSChannel + "_" + channelName)
              << ": "
              << tcds::utils::escapeAsJSONString(tcds::utils::TTSStateToString(ttsState, false));
          res << ", ";
        }

      // A note about what triggered the most recent orbit-counter
      // reset (i.e., what this entry is synchronised to).
      res << tcds::utils::escapeAsJSONString(colIdSync)
          << ": "
          << tcds::utils::escapeAsJSONString(OC0ReasonToString(it->getLatestOC0Reason()));
      res << ", ";

      // An optional comment.
      res << tcds::utils::escapeAsJSONString(colIdComment)
          << ": "
          << tcds::utils::escapeAsJSONString(it->detailedComment(ttsChannelLabels_));

      res << "}";
      if (it != (entries_.end() - 1))
        {
          res << ", ";
        }
    }

  res << "]";

  //----------

  // Done.
  res << "}";

  return res.str();
}

size_t
tcds::pi::TTSLogHistory::numEntries() const
{
  return entries_.size();
}

void
tcds::pi::TTSLogHistory::trim()
{
  while (numEntries() > histSize_)
    {
      entries_.pop_front();
    }
}
