#include "tcds/pi/PIController.h"

#include <stdint.h>
#include <string>
#include <unistd.h>

#include "toolbox/string.h"
#include "xcept/Exception.h"
#include "xdaq/exception/ApplicationInstantiationFailed.h"

#include "tcds/exception/Exception.h"
#include "tcds/hwlayer/DeviceBase.h"
#include "tcds/hwlayer/RegisterInfo.h"
#include "tcds/hwutilstca/HwIDInfoSpaceHandlerTCA.h"
#include "tcds/hwutilstca/HwIDInfoSpaceUpdaterTCA.h"
#include "tcds/hwutilstca/SFPInfoSpaceUpdater.h"
#include "tcds/hwutilstca/Utils.h"
#include "tcds/pi/ConfigurationInfoSpaceHandler.h"
#include "tcds/pi/Definitions.h"
#include "tcds/pi/HwStatusInfoSpaceHandler.h"
#include "tcds/pi/HwStatusInfoSpaceUpdater.h"
#include "tcds/pi/PITTCInfoSpaceHandler.h"
#include "tcds/pi/PITTCInfoSpaceUpdater.h"
#include "tcds/pi/PITTSInfoSpaceHandler.h"
#include "tcds/pi/PITTSInfoSpaceUpdater.h"
#include "tcds/pi/PITTSLinksInfoSpaceHandler.h"
#include "tcds/pi/PITTSLinksInfoSpaceUpdater.h"
#include "tcds/pi/SFPInfoSpaceHandler.h"
#include "tcds/pi/TCADevicePI.h"
#include "tcds/pi/TTCSpyInfoSpaceHandler.h"
#include "tcds/pi/TTCSpyInfoSpaceUpdater.h"
#include "tcds/pi/TTSLogInfoSpaceHandler.h"
#include "tcds/pi/TTSLogInfoSpaceUpdater.h"
#include "tcds/pi/TTXLogHelper.h"
#include "tcds/pi/Utils.h"
#include "tcds/utils/LogMacros.h"
#include "tcds/utils/SOAPUtils.h"
#include "tcds/utils/Utils.h"

XDAQ_INSTANTIATOR_IMPL(tcds::pi::PIController)

tcds::pi::PIController::PIController(xdaq::ApplicationStub* stub)
try
  :
  tcds::utils::XDAQAppWithFSMBasic(stub, std::unique_ptr<tcds::hwlayer::DeviceBase>(new tcds::pi::TCADevicePI())),
    soapCmdConfigureTTCSpy_(*this),
    soapCmdDisableTTCSpy_(*this),
    soapCmdDumpHardwareState_(*this),
    soapCmdEnableTTCSpy_(*this),
    soapCmdReadHardwareConfiguration_(*this),
    soapCmdResetTTCSpyLog_(*this),
    piDAQLoop_(*this)
      {
        // Create the InfoSpace holding all configuration information.
        cfgInfoSpaceP_ =
          std::unique_ptr<tcds::pi::ConfigurationInfoSpaceHandler>(new tcds::pi::ConfigurationInfoSpaceHandler(*this));

        // Make sure the correct default hardware configuration file is found.
        cfgInfoSpaceP_->setString("defaultHwConfigurationFilePath",
                                  "${XDAQ_ROOT}/etc/tcds/pi/hw_cfg_default_pi.txt");
      }
catch (tcds::exception::Exception const& err)
  {
    std::string msgBase = "Something went wrong instantiating the PIController application";
    std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.what());
    XCEPT_RAISE(xdaq::exception::ApplicationInstantiationFailed, msg.c_str());
  }

tcds::pi::PIController::~PIController()
{
  hwRelease();
}

void
tcds::pi::PIController::setupInfoSpaces()
{
  // Make sure the configuration settings are up-to-date.
  cfgInfoSpaceP_->readInfoSpace();

  // Instantiate all hardware-dependent InfoSpaceHandlers and InfoSpaceUpdaters.
  hwIDInfoSpaceUpdaterP_ =
    std::unique_ptr<tcds::hwutilstca::HwIDInfoSpaceUpdaterTCA>(new tcds::hwutilstca::HwIDInfoSpaceUpdaterTCA(*this, getHw()));
  hwIDInfoSpaceP_ =
    std::unique_ptr<tcds::hwutilstca::HwIDInfoSpaceHandlerTCA>(new tcds::hwutilstca::HwIDInfoSpaceHandlerTCA(*this, hwIDInfoSpaceUpdaterP_.get()));
  hwStatusInfoSpaceUpdaterP_ =
    std::unique_ptr<tcds::pi::HwStatusInfoSpaceUpdater>(new tcds::pi::HwStatusInfoSpaceUpdater(*this, getHw()));
  hwStatusInfoSpaceP_ =
    std::unique_ptr<tcds::pi::HwStatusInfoSpaceHandler>(new tcds::pi::HwStatusInfoSpaceHandler(*this, hwStatusInfoSpaceUpdaterP_.get()));
  piInputInfoSpaceUpdaterP_ =
    std::unique_ptr<PITTCInfoSpaceUpdater>(new PITTCInfoSpaceUpdater(*this, getHw()));
  piInputInfoSpaceP_ =
    std::unique_ptr<PITTCInfoSpaceHandler>(new PITTCInfoSpaceHandler(*this, piInputInfoSpaceUpdaterP_.get()));
  piTTSInfoSpaceUpdaterP_ =
    std::unique_ptr<PITTSInfoSpaceUpdater>(new PITTSInfoSpaceUpdater(*this, getHw()));
  piTTSInfoSpaceP_ =
    std::unique_ptr<PITTSInfoSpaceHandler>(new PITTSInfoSpaceHandler(*this, piTTSInfoSpaceUpdaterP_.get()));
  piTTSLinksInfoSpaceUpdaterP_ =
    std::unique_ptr<PITTSLinksInfoSpaceUpdater>(new PITTSLinksInfoSpaceUpdater(*this, getHw()));
  piTTSLinksInfoSpaceP_ =
    std::unique_ptr<PITTSLinksInfoSpaceHandler>(new PITTSLinksInfoSpaceHandler(*this, piTTSLinksInfoSpaceUpdaterP_.get()));
  sfpInfoSpaceUpdaterP_ =
    std::unique_ptr<tcds::hwutilstca::SFPInfoSpaceUpdater>(new tcds::hwutilstca::SFPInfoSpaceUpdater(*this, getHw()));
  sfpInfoSpaceP_ =
    std::unique_ptr<SFPInfoSpaceHandler>(new SFPInfoSpaceHandler(*this, sfpInfoSpaceUpdaterP_.get()));
  ttcspyInfoSpaceUpdaterP_ =
    std::unique_ptr<TTCSpyInfoSpaceUpdater>(new TTCSpyInfoSpaceUpdater(*this, getHw(), piDAQLoop_));
  ttcspyInfoSpaceP_ =
    std::unique_ptr<TTCSpyInfoSpaceHandler>(new TTCSpyInfoSpaceHandler(*this, ttcspyInfoSpaceUpdaterP_.get()));
  ttslogInfoSpaceUpdaterP_ =
    std::unique_ptr<TTSLogInfoSpaceUpdater>(new TTSLogInfoSpaceUpdater(*this, getHw(), piDAQLoop_));
  ttslogInfoSpaceP_ =
    std::unique_ptr<TTSLogInfoSpaceHandler>(new TTSLogInfoSpaceHandler(*this, ttslogInfoSpaceUpdaterP_.get()));

  // Register all InfoSpaceItems with the Monitor.
  cfgInfoSpaceP_->registerItemSets(monitor_, webServer_);
  appStateInfoSpace_.registerItemSets(monitor_, webServer_);
  hwIDInfoSpaceP_->registerItemSets(monitor_, webServer_);
  hwStatusInfoSpaceP_->registerItemSets(monitor_, webServer_);
  sfpInfoSpaceP_->registerItemSets(monitor_, webServer_);
  piInputInfoSpaceP_->registerItemSets(monitor_, webServer_);
  piTTSInfoSpaceP_->registerItemSets(monitor_, webServer_);
  piTTSLinksInfoSpaceP_->registerItemSets(monitor_, webServer_);
  ttcspyInfoSpaceP_->registerItemSets(monitor_, webServer_);
  ttslogInfoSpaceP_->registerItemSets(monitor_, webServer_);

  // Force a write of all TTS InfoSpaces. This triggers an XMAS update
  // in order to populate the flashlists such that other applications
  // can pick them up.
  piTTSInfoSpaceP_->writeInfoSpace(true);

  // Make sure that either the output directory for the TTS history
  // files already exists, or that it can be created.
  // Not super pretty to do this here, but it allows us to blindly
  // assume its existence later on.
  ensureOutputDirExists(cfgInfoSpaceP_->getString("ttsHistPath"));
}

tcds::pi::TCADevicePI&
tcds::pi::PIController::getHw() const
{
  return static_cast<tcds::pi::TCADevicePI&>(*hwP_.get());
}

void
tcds::pi::PIController::hwConnectImpl()
{
  tcds::hwutilstca::tcaDeviceHwConnectImpl(*cfgInfoSpaceP_, getHw());
}

void
tcds::pi::PIController::hwReleaseImpl()
{
  getHw().hwRelease();
}

void
tcds::pi::PIController::hwCfgInitializeImpl()
{
  tcds::pi::TCADevicePI& hw = getHw();

  // If asked to do so, start with a reset of the TTC PLL.
  bool const resetPLL = !(cfgInfoSpaceP_->getBool("skipPLLReset"));
  if (resetPLL)
    {
      hw.resetPLL();
    }
  else
    {
      std::string const msg = "NOTE: Skipping PLL reset upon explicit request "
        "(i.e., the skipPLLReset flag is set in the Configure command).";
      WARN(msg);
      appStateInfoSpace_.addHistoryItem(msg);
    }

  //----------

  // Check for the presence of the TTC clock. Without TTC clock it is
  // no use continuing (and apart from that, some registers will not
  // be accessible).
  if (!(hw.isTTCClockUp() && hw.isTTCClockStable()))
    {
      std::string const msg = "Could not configure the hardware: no TTC clock present, or clock not stable.";
      ERROR(msg);
      XCEPT_RAISE(tcds::exception::TTCClockProblem, msg.c_str());
    }

  //----------

  // Check that both MMCMs used by the phase measurements are locked.
  if (!hw.isPhaseMonLocked())
    {
      std::string const msg =
        "Could not configure the hardware: PhaseMon MMCMs are not locked."
        " Possible TTC/reference clock problem.";
      ERROR(msg);
      XCEPT_RAISE(tcds::exception::TTCClockProblem, msg.c_str());
    }

  //----------

  // Configure and enable the 'TTC-stream phase monitoring.'
  // NOTE: This really measures phases between clocks, not w.r.t. the
  // real TTC stream, so this can be enabled before selecting the TTC
  // stream source.
  hw.enablePhaseMonitoring();

  //----------

  // Enable the TTC decoder.
  // NOTE: This has to be done before the 'TTC stream alignment' check
  // (in hwCfgFinalizeImpl()). With the TTC decoder disabled, the
  // stream alignment flag is always zero.
  hw.enableTTCDecoder();

  //----------

  // Switch the TTS input idelay alignment to automatic mode and
  // release all alignment strobes.
  hw.writeRegister("tts_serdes_ctrl.align_mode", 0);
  for (unsigned int fedInputNum = 1; fedInputNum <= tcds::definitions::kNumFEDs; ++fedInputNum)
    {
      std::string const fedStr = toolbox::toString("fed%d", fedInputNum);
      hw.writeRegister("tts_idelay_ctrl.tap_ld." + fedStr, 0);
      hw.writeRegister("tts_idelay_ctrl.align_strobe." + fedStr, 0);
    }
}

void
tcds::pi::PIController::hwCfgFinalizeImpl()
{
  tcds::pi::TCADevicePI& hw = getHw();

  //----------

  // Make the magic idelay setting that will allow the incoming TTC
  // stream to be aligned and locked.
  std::vector<std::string> priSec;
  priSec.push_back("pri");
  priSec.push_back("sec");
  uint32_t const idelayVal = 0x07;
  for (std::vector<std::string>::const_iterator it = priSec.begin();
       it != priSec.end();
       ++it)
    {
      hw.writeRegister("ttc_idelay_ctrl.ttc_idelay_tap_lpm_" + *it, idelayVal);
      hw.writeRegister("ttc_idelay_ctrl.ttc_idelay_ld_lpm_" + *it, 0x1);
      hw.writeRegister("ttc_idelay_ctrl.ttc_idelay_ld_lpm_" + *it, 0x0);
    }

  //----------

  // Select the correct LPM input TTC source: primary system or
  // secondary system.
  uint32_t tmp = cfgInfoSpaceP_->getBool("usePrimaryTCDS");
  tcds::definitions::LPM_SOURCE lpmSource = tcds::definitions::LPM_SOURCE_PRIMARY;
  if (!tmp)
    {
      lpmSource = tcds::definitions::LPM_SOURCE_SECONDARY;
    }
  hw.selectLPMSource(lpmSource);

  //----------

  // Check that the incoming TTC stream from the LPM is aligned.

  // NOTE: We may have to give it some time to run and check,
  // otherwise this fails when the LPM is not yet fully configured.
  bool isAligned = getHw().isTTCStreamAligned();
  int const maxNumTries = 10;
  int numTries = 0;
  while (!isAligned && (numTries < maxNumTries))
    {
      ::sleep(1);
      isAligned = getHw().isTTCStreamAligned();
      ++numTries;
    }
  if (!isAligned)
    {
      std::string const msg = "Could not configure the hardware: incoming TTC stream not aligned.";
      ERROR(msg);
      XCEPT_RAISE(tcds::exception::TTCStreamProblem, msg.c_str());
    }

  //----------

  // Now follows the configuration of the TTS inputs based on the
  // configuration mode.

  // First step: switch off all inputs.
  hw.disableTTSInputs();

  // Second step: figure out which inputs to enable, and enable those.
  tmp = cfgInfoSpaceP_->getUInt32("configurationMode");
  tcds::definitions::PICONTROLLER_CONFIG_MODE configMode =
    static_cast<tcds::definitions::PICONTROLLER_CONFIG_MODE>(tmp);

  if (configMode == tcds::definitions::PICONTROLLER_CONFIG_MODE_NOTTS)
    {
      // Nothing to be done in this case. Leave all TTS inputs disabled.
    }
  else if (configMode == tcds::definitions::PICONTROLLER_CONFIG_MODE_LEGACY_FMM)
    {
      // Legacy-FMM configuration mode: enable only the RJ45 TTS
      // input.
      hw.writeRegister("tts_source_enable.rj45", 0x1);
    }
  else if ((configMode == tcds::definitions::PICONTROLLER_CONFIG_MODE_AMC13) ||
           (configMode == tcds::definitions::PICONTROLLER_CONFIG_MODE_MIXED))
    {
      std::string const fedEnableMask = cfgInfoSpaceP_->getString("fedEnableMask");

      // Enable optical TTS inputs based on the fedEnableMask.
      for (unsigned int fedInput = tcds::definitions::TTS_SOURCE_MIN;
           fedInput <= tcds::definitions::TTS_SOURCE_MAX;
           fedInput++)
        {
          if (fedInput != tcds::definitions::TTS_SOURCE_RJ45)
            {
              uint32_t const fedId =
                cfgInfoSpaceP_->getUInt32(toolbox::toString("fedIdInput%d", fedInput));
              if (tcds::utils::isFedInFedEnableMask(fedId, fedEnableMask))
                {
                  // Found the FED connected to the current input in
                  // the fedEnableMask.
                  if (tcds::utils::doesFedDoTTS(fedId, fedEnableMask))
                    {
                      // FED has an active TTS output -> enable our
                      // corresponding input.
                      hw.enableTTSInput(static_cast<tcds::definitions::TTS_SOURCE>(fedInput));
                    }
                }
            }
        }

      if (configMode == tcds::definitions::PICONTROLLER_CONFIG_MODE_AMC13)
        {
          // AMC13-FED mode. Enable only those optical TTS inputs
          // matching a FED in the fedEnableMask. So nothing special
          // needed in this case.
        }
      else
        {
          // Special mixed mode to support commissioning. Enable also
          // the RJ45 TTS input based on the fedEnableMask.
          std::vector<uint32_t> fmmFEDIds = cfgInfoSpaceP_->getUInt32Vec("fmmFEDs");

          for (std::vector<uint32_t>::const_iterator fmmFEDId = fmmFEDIds.begin();
               fmmFEDId != fmmFEDIds.end();
               ++fmmFEDId)
            {
              if (tcds::utils::isFedInFedEnableMask(*fmmFEDId, fedEnableMask))
                {
                  // Found the FED connected to the current input in
                  // the fedEnableMask.
                  if (tcds::utils::doesFedDoTTS(*fmmFEDId, fedEnableMask))
                    {
                      // FED has an active TTS output -> enable our
                      // RJ45 input.
                      hw.enableTTSInput(tcds::definitions::TTS_SOURCE_RJ45);
                      break;
                    }
                }
            }
        }
    }
  else
    {
      std::string msgBase = "Unknown PIController configuration mode value";
      std::string msg = toolbox::toString("%s: '%d'.", msgBase.c_str(), tmp);
      XCEPT_RAISE(tcds::exception::ConfigurationProblem, msg.c_str());
    }

  // And of course we have to enable the TTS output to the LPM.
  // NOTE: This is one of the registers that are reset as side-effect
  // of the TTC-clock PLL reset.
  hw.writeRegister("tts_output_control.tts_out_enable", 0x1);

  //----------

  // The initial orbit counter value to be used after an OC0 is in
  // principle programmable. But let's stick to the definition of 0.
  hw.writeRegister("sync.oc0_reset_val.hi", 0x0);
  hw.writeRegister("sync.oc0_reset_val.lo", 0x0);

  // ----------

  // Configure and enable the built-in TTC spy.
  hw.disableTTCSpy();
  // NOTE: The TTC spy runs in single-shot mode, and the software
  // updater will reset and restart the logging every time the log is
  // full.
  hw.setTTCSpyBufferMode(tcds::pi::TTXLogHelper::LOGGING_BUFFER_MODE_SINGLESHOT);
  hw.setTTCSpyBufferSizeToMax();
  hw.resetTTCSpy();
  // Enable the TTCSpy.
  hw.enableTTCSpy();

  //----------

  // Configure and enable the TTS logging.
  hw.disableTTSLog();
  // NOTE: The TTS logging runs in single-shot mode, and the software
  // updater will reset and restart the logging every time the log is
  // full. That way at least the software can flag situations in which
  // some entries were missed by the logger.
  hw.setTTSLogBufferMode(tcds::pi::TTXLogHelper::LOGGING_BUFFER_MODE_SINGLESHOT);
  hw.setTTSLogBufferSizeToMax();
  hw.resetTTSLog();
  hw.enableTTSLog();

  // ----------

  // Reset the orbit length checker.
  hw.resetOrbitChecker();
}

void
tcds::pi::PIController::configureActionImpl(toolbox::Event::Reference event)
{
  // NOTE: Watch out with the juggling of the TTC/TTS DAQ loop.
  piDAQLoop_.stop();

  tcds::utils::XDAQAppWithFSMBasic::configureActionImpl(event);

  // NOTE: In this case we pass a 'false' value to start() such that
  // the TTS history logging is not done to file (since we're between
  // runs).
  piDAQLoop_.start(false);
}

void
tcds::pi::PIController::enableActionImpl(toolbox::Event::Reference event)
{
  // Restart the dedicated acquisition loop for the TTC and TTS logging.
  // NOTE: In this case we pass a 'true' value to start() such that
  // the TTS history logging is done to file.
  piDAQLoop_.stop();
  piDAQLoop_.start(true);

  // Reset the orbit counter in the PI. This should normally (also) be
  // done by the OC0 in the TTC stream (sent as part of the Start
  // sequence), but in case people have not configured that
  // (correctly) at least this generates an orbit counter reset close
  // to the start of the run.
  getHw().resetOrbitCounter();

  tcds::utils::XDAQAppWithFSMBasic::enableActionImpl(event);
}

void
tcds::pi::PIController::haltActionImpl(toolbox::Event::Reference event)
{
  // NOTE: Of course one has to first stop the TTC/TTS DAQ loop,
  // before calling XDAQAppWithFSMBasic::haltActionImpl(), which
  // closes the connection to the hardware.
  piDAQLoop_.stop();

  tcds::utils::XDAQAppWithFSMBasic::haltActionImpl(event);
}

void
tcds::pi::PIController::stopActionImpl(toolbox::Event::Reference event)
{
  tcds::utils::XDAQAppWithFSMBasic::stopActionImpl(event);

  // Restart the dedicated acquisition loop for the TTC and TTS
  // logging.
  // NOTE: In this case we pass a 'false' value to start() such that
  // the TTS history logging is not done to file (since we're between
  // runs).
  piDAQLoop_.stop();
  piDAQLoop_.start(false);
}

void
tcds::pi::PIController::zeroActionImpl(toolbox::Event::Reference event)
{
  // Restart the TTS log and the TTC spy.
  piDAQLoop_.clearHistory();

  // Zero the TTC stream alignment monitoring.
  getHw().writeRegister("ttc_decoder.control.ttc_stream_unalign_count_reset", 0x0);
  getHw().writeRegister("ttc_decoder.control.ttc_stream_unalign_count_reset", 0x1);
  getHw().writeRegister("ttc_decoder.control.ttc_stream_unalign_count_reset", 0x0);
}

tcds::utils::RegCheckResult
tcds::pi::PIController::isRegisterAllowed(tcds::hwlayer::RegisterInfo const& regInfo) const
{
  // Start by applying the default selection.
  tcds::utils::RegCheckResult res = tcds::utils::XDAQAppWithFSMBasic::isRegisterAllowed(regInfo);

  // The initial orbit counter value to be used after an OC0 is in
  // principle programmable. But the user/configuration should
  // definitely not play with that.
  res = (res && !toolbox::endsWith(regInfo.name(), "oc0_reset_val"))
    ? res : tcds::utils::kRegCheckResultDisallowed;

  // The TTC spy and TTS log reset register should not be handled by
  // the user configuration. (This makes no sense anyway.)
  res = (res && !toolbox::endsWith(regInfo.name(), ".reset"))
    ? res : tcds::utils::kRegCheckResultDisallowed;

  // The TTC spy TTS log enable/disable is done by the software, not
  // by configuration.
  res = (res && (regInfo.name() != "ttc_spy.control.enabled"))
    ? res : tcds::utils::kRegCheckResultDisallowed;
  res = (res && (regInfo.name() != "tts_log.control.enabled"))
    ? res : tcds::utils::kRegCheckResultDisallowed;

  // These are some more registers that the user config should not
  // fiddle with.
  res = (res && (regInfo.name() != "ttc_spy.control.max_num_entries"))
    ? res : tcds::utils::kRegCheckResultDisallowed;
  res = (res && (regInfo.name() != "ttc_spy.control.logging_buffer_mode"))
    ? res : tcds::utils::kRegCheckResultDisallowed;
  res = (res && (regInfo.name() != "tts_log.control.max_num_entries"))
    ? res : tcds::utils::kRegCheckResultDisallowed;
  res = (res && (regInfo.name() != "tts_log.control.logging_buffer_mode"))
    ? res : tcds::utils::kRegCheckResultDisallowed;

  // This one we'll handle ourselves. If switched off, this also locks
  // the 'ttc_stream_aligned' down to 0.
  res = (res && (regInfo.name() != "ttc_decoder.control.enable"))
    ? res : tcds::utils::kRegCheckResultDisallowed;

  // The enabling/disabling of the different TTS inputs is done by the
  // software (and depends on the configuration mode).
  res = (res && (regInfo.name().find("tts_source_enable") == std::string::npos))
    ? res : tcds::utils::kRegCheckResultDisallowed;

  // // The TTS output should never be disabled. So we'll handle that one
  // // in software too.
  // res = res && (regInfo.name().find("tts_out_enable") == std::string::npos);

  // The user/configuration should not really meddle with the TTS
  // output control either...
  res = (res && (regInfo.name().find("tts_output_control") == std::string::npos))
    ? res : tcds::utils::kRegCheckResultDisallowed;

  // The idelay settings are handled by the software, and should not
  // be fiddled with.
  res = (res && (regInfo.name().find("idelay") == std::string::npos))
    ? res : tcds::utils::kRegCheckResultDisallowed;

  return res;
}

std::vector<tcds::utils::FSMSOAPParHelper>
tcds::pi::PIController::expectedFSMSoapParsImpl(std::string const& commandName) const
{
  // Define what we expect in terms of parameters for each SOAP FSM
  // command.
  std::vector<tcds::utils::FSMSOAPParHelper> params =
    tcds::utils::XDAQAppWithFSMBasic::expectedFSMSoapParsImpl(commandName);

  // Configure for the PIController takes the usual
  // 'xdaq:hardwareConfigurationString', and 'optionally'
  // 'xdaq:fedEnableMask', 'xdaq:usePrimaryTCDS', and
  // 'xdaq:skipPLLReset'. So only the latter are special.
  if ((commandName == "Configure") ||
      (commandName == "Reconfigure"))
    {
      // In legacy configuration mode the fedEnableMask is not needed,
      // but in all other configuration modes it is.
      bool needFEDEnableMask = false;
      uint32_t const tmp = cfgInfoSpaceP_->getUInt32("configurationMode");
      tcds::definitions::PICONTROLLER_CONFIG_MODE configMode =
        static_cast<tcds::definitions::PICONTROLLER_CONFIG_MODE>(tmp);
      if ((configMode == tcds::definitions::PICONTROLLER_CONFIG_MODE_AMC13) ||
          (configMode == tcds::definitions::PICONTROLLER_CONFIG_MODE_MIXED))
        {
          needFEDEnableMask = true;
        }
      params.push_back(tcds::utils::FSMSOAPParHelper(commandName, "fedEnableMask", needFEDEnableMask));
      params.push_back(tcds::utils::FSMSOAPParHelper(commandName, "usePrimaryTCDS", false));

      // The skipPLLReset parameter is optional but allowed, although
      // it is strongly discouraged to actually use it.
      params.push_back(tcds::utils::FSMSOAPParHelper(commandName, "skipPLLReset", false));
    }
  return params;
}

void
tcds::pi::PIController::loadSOAPCommandParameterImpl(xoap::MessageReference const& msg,
                                                     tcds::utils::FSMSOAPParHelper const& param)
{
  // NOTE: The assumption is that when entering this method, the
  // command-to-parameter mapping, required parameter presence,
  // etc. have all been checked already. This method only concerns the
  // actual loading of the parameters.

  std::string const parName = param.parameterName();

  if (parName == "fedEnableMask")
    {
      // Import 'xdaq:fedEnableMask'.
      std::string fedEnableMask = "";
      if (tcds::utils::soap::hasSOAPCommandParameter(msg, parName))
        {
          fedEnableMask = tcds::utils::soap::extractSOAPCommandParameterString(msg, parName);
        }
      cfgInfoSpaceP_->setString(parName, fedEnableMask);
    }
  else if (parName == "usePrimaryTCDS")
    {
      bool usePrimaryTCDS = true;
      if (tcds::utils::soap::hasSOAPCommandParameter(msg, parName))
        {
          usePrimaryTCDS = tcds::utils::soap::extractSOAPCommandParameterBool(msg, parName);
        }
      cfgInfoSpaceP_->setBool(parName, usePrimaryTCDS);
    }
  else if (parName == "skipPLLReset")
    {
      bool skipPLLReset = false;
      if (tcds::utils::soap::hasSOAPCommandParameter(msg, parName))
        {
          skipPLLReset = tcds::utils::soap::extractSOAPCommandParameterBool(msg, parName);
        }
      cfgInfoSpaceP_->setBool(parName, skipPLLReset);
    }
  else
    {
      // For other commands: use the usual approach.
      tcds::utils::XDAQAppWithFSMBasic::loadSOAPCommandParameterImpl(msg, param);
    }
}
