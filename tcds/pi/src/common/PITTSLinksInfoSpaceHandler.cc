#include "tcds/pi/PITTSLinksInfoSpaceHandler.h"

#include <stdint.h>

#include "toolbox/string.h"

#include "tcds/pi/Definitions.h"
#include "tcds/utils/ConfigurationInfoSpaceHandler.h"
#include "tcds/utils/Definitions.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/WebServer.h"
#include "tcds/utils/XDAQAppBase.h"

namespace xdaq {
  class Application;
}

tcds::pi::PITTSLinksInfoSpaceHandler::PITTSLinksInfoSpaceHandler(xdaq::Application& xdaqApp,
                                                                 tcds::utils::InfoSpaceUpdater* updater) :
  tcds::utils::InfoSpaceHandler(xdaqApp, "tcds-pi-tts-link_status", updater)
{
  tcds::utils::ConfigurationInfoSpaceHandler const& cfgInfoSpace =
    getOwnerApplication().getConfigurationInfoSpaceHandler();

  // Pre-build all input labels. The label is based on the FED number
  // from the XDAQ configuration. Unset FED numbers default to 4095,
  // which is assumed to represent an unused input.
  for (unsigned int fedInputNum = tcds::definitions::kFEDNumMin;
       fedInputNum <= tcds::definitions::kFEDNumMax;
       ++fedInputNum)
    {
      std::string const name = toolbox::toString("fed%d", fedInputNum);
      uint32_t const fedId =
        cfgInfoSpace.getUInt32(toolbox::toString("fedIdInput%d", fedInputNum));
      std::string label = toolbox::toString("Input %d (unused)", fedInputNum);
      if (fedId != tcds::definitions::kImpossibleFEDId)
        {
          label = toolbox::toString("FED %d", fedId);
        }
      inputNames_.push_back(name);
      inputLabels_[name] = label;
    }

  // The RJ45 TTS input is easy to label.
  std::string const name = "rj45";
  inputNames_.push_back(name);
  inputLabels_[name] = "FMM input";

  //----------

  // Turn the input labels into InfoSpace items so we can use them too.
  for (std::map<std::string, std::string>::const_iterator i = inputLabels_.begin();
       i != inputLabels_.end();
       ++i)
    {
      std::string const name = "inputlabel_" + i->first;
      createString(name,
                   i->second,
                   "",
                   tcds::utils::InfoSpaceItem::NOUPDATE,
                   true);
    }

  //----------

  // All individual link monitoring info.
  for (unsigned int fedInputNum = tcds::definitions::kFEDNumMin;
       fedInputNum <= tcds::definitions::kFEDNumMax;
       ++fedInputNum)
    {
      // Link aligned flag.
      std::string name = toolbox::toString("tts_link_status.fed%d.aligned", fedInputNum);
      createBool(name, false, "true/false");

      // SFP RX loss of signal flag.
      name = toolbox::toString("tts_link_status.fed%d.sfp_rxlos", fedInputNum);
      createBool(name, false, "true/false");

      // Invalid 8b10b character count.
      name = toolbox::toString("tts_link_status.fed%d.invalid_8b10b_char_count", fedInputNum);
      createUInt32(name, 0);

      // Invalid 8b value count.
      name = toolbox::toString("tts_link_status.fed%d.invalid_8b_value_count", fedInputNum);
      createUInt32(name, 0);
    }
}

tcds::pi::PITTSLinksInfoSpaceHandler::~PITTSLinksInfoSpaceHandler()
{
}

tcds::utils::XDAQAppBase&
tcds::pi::PITTSLinksInfoSpaceHandler::getOwnerApplication() const
{
  return static_cast<tcds::utils::XDAQAppBase&>(tcds::utils::InfoSpaceHandler::getOwnerApplication());
}

void
tcds::pi::PITTSLinksInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  // All individual link monitoring info.
  for (unsigned int fedInputNum = tcds::definitions::kFEDNumMin;
       fedInputNum <= tcds::definitions::kFEDNumMax;
       ++fedInputNum)
    {
      std::string const itemSetName = toolbox::toString("itemset-tts-link-status-%d", fedInputNum);
      monitor.newItemSet(itemSetName);

      // FED label.
      std::string itemName = toolbox::toString("inputlabel_fed%d", fedInputNum);
      monitor.addItem(itemSetName,
                      itemName,
                      "FED connection",
                      this);

      // Link aligned flag.
      itemName = toolbox::toString("tts_link_status.fed%d.aligned", fedInputNum);
      monitor.addItem(itemSetName,
                      itemName,
                      "Link aligned",
                      this);

      // SFP RX loss of signal flag.
      itemName = toolbox::toString("tts_link_status.fed%d.sfp_rxlos", fedInputNum);
      monitor.addItem(itemSetName,
                      itemName,
                      "SFP input signal lost",
                      this);


      // Invalid 8b10b character count.
      itemName = toolbox::toString("tts_link_status.fed%d.invalid_8b10b_char_count", fedInputNum);
      monitor.addItem(itemSetName,
                      itemName,
                      "Invalid 10b character count",
                      this,
                      "The number of invalid 8b/10b characters received on this FED TTS link. Should be zero under normal conditions.");

      // Invalid 8b value count.
      itemName = toolbox::toString("tts_link_status.fed%d.invalid_8b_value_count", fedInputNum);
      monitor.addItem(itemSetName,
                      itemName,
                      "Invalid 8b value count",
                      this,
                      "The number of invalid (8-bit) TTS values received on this FED TTS link during the latest alignment procedure. May be non-zero, but should be a fixed value.");
    }
}

void
tcds::pi::PITTSLinksInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                    tcds::utils::Monitor& monitor,
                                                                    std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "TTS link status" : forceTabName;

  webServer.registerTab(tabName,
                        "Information on the optical TTS links from the AMC13 FEDs to the PI",
                        5);
  //----------

  // All individual link monitoring info.
  for (unsigned int fedInputNum = tcds::definitions::kFEDNumMin;
       fedInputNum <= tcds::definitions::kFEDNumMax;
       ++fedInputNum)
    {
      std::string const itemSetName = toolbox::toString("itemset-tts-link-status-%d", fedInputNum);
      webServer.registerTable(toolbox::toString("Input %d", fedInputNum),
                              "",
                              monitor,
                              itemSetName,
                              tabName);
    }
}
