#include "tcds/pi/TTCSpyInfoSpaceHandler.h"

#include <cassert>
#include <stdint.h>

#include "tcds/pi/TCADevicePI.h"
#include "tcds/pi/WebTableTTCSpyLog.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/Utils.h"
#include "tcds/utils/WebServer.h"

tcds::pi::TTCSpyInfoSpaceHandler::TTCSpyInfoSpaceHandler(xdaq::Application& xdaqApp,
                                                         tcds::utils::InfoSpaceUpdater* updater) :
  InfoSpaceHandler(xdaqApp, "tcds-ttcspy", updater)
{
  // The various TTCSpy logging settings.
  createBool("ttc_spy.control.enabled", false, "true/false");
  createUInt32("ttc_spy.control.logging_mode");
  createUInt32("ttc_spy.control.trigger_combination_operator");

  createBool("ttc_spy.trigger_mask.l1a", false, "true/false");
  createBool("ttc_spy.trigger_mask.brc_all", false, "true/false");
  createBool("ttc_spy.trigger_mask.add_all", false, "true/false");
  createBool("ttc_spy.trigger_mask.brc_bc0", false, "true/false");
  createBool("ttc_spy.trigger_mask.brc_ec0", false, "true/false");

  createBool("ttc_spy.trigger_mask.brc_dddd_all", false, "true/false");
  createBool("ttc_spy.trigger_mask.brc_tt_all", false, "true/false");

  createBool("ttc_spy.trigger_mask.brc_zero_data", false, "true/false");
  createBool("ttc_spy.trigger_mask.adr_zero_data", false, "true/false");

  createBool("ttc_spy.trigger_mask.err_com", false, "true/false");

  createUInt32("ttc_spy.trigger_mask.brc_dddd", 0, "x");
  createUInt32("ttc_spy.trigger_mask.brc_tt", 0, "x");

  createUInt32("ttc_spy.trigger_mask.brc_val0", 0, "x");
  createUInt32("ttc_spy.trigger_mask.brc_val1", 0, "x");
  createUInt32("ttc_spy.trigger_mask.brc_val2", 0, "x");
  createUInt32("ttc_spy.trigger_mask.brc_val3", 0, "x");
  createUInt32("ttc_spy.trigger_mask.brc_val4", 0, "x");
  createUInt32("ttc_spy.trigger_mask.brc_val5", 0, "x");

  // The TTCSpy log.
  createString("ttcspy_log");
}

void
tcds::pi::TTCSpyInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  // The various TTCSpy logging settings.
  monitor.newItemSet("itemset-ttcspylogging");

  monitor.addItem("itemset-ttcspylogging",
                  "ttc_spy.control.enabled",
                  "Logging enabled",
                  this);
  monitor.addItem("itemset-ttcspylogging",
                  "ttc_spy.control.logging_mode",
                  "Logging mode",
                  this);

  monitor.newItemSet("itemset-ttcspytrigger");

  monitor.addItem("itemset-ttcspytrigger",
                  "ttc_spy.control.trigger_combination_operator",
                  "Logging trigger-term combination operator",
                  this);

  monitor.addItem("itemset-ttcspytrigger",
                  "ttc_spy.trigger_mask.l1a",
                  "Trigger on L1As",
                  this);
  monitor.addItem("itemset-ttcspytrigger",
                  "ttc_spy.trigger_mask.brc_all",
                  "Trigger on all broadcast B-commands",
                  this);
  monitor.addItem("itemset-ttcspytrigger",
                  "ttc_spy.trigger_mask.add_all",
                  "Trigger on all addressed B-commands",
                  this);
  monitor.addItem("itemset-ttcspytrigger",
                  "ttc_spy.trigger_mask.brc_bc0",
                  "Trigger on broadcast B-commands with the BC0 bit set",
                  this);
  monitor.addItem("itemset-ttcspytrigger",
                  "ttc_spy.trigger_mask.brc_ec0",
                  "Trigger on broadcast B-commands with the EC0 bit set",
                  this);

  monitor.addItem("itemset-ttcspytrigger",
                  "ttc_spy.trigger_mask.brc_dddd_all",
                  "Trigger on all broadcast B-commands with non-zero user-data bits",
                  this);
  monitor.addItem("itemset-ttcspytrigger",
                  "ttc_spy.trigger_mask.brc_tt_all",
                  "Trigger on all broadcast B-commands with non-zero test-data bits",
                  this);

  monitor.addItem("itemset-ttcspytrigger",
                  "ttc_spy.trigger_mask.brc_zero_data",
                  "Trigger on all broadcast B-commands with zero data payload",
                  this);
  monitor.addItem("itemset-ttcspytrigger",
                  "ttc_spy.trigger_mask.adr_zero_data",
                  "Trigger on all addressed B-commands with zero data payload",
                  this);

  monitor.addItem("itemset-ttcspytrigger",
                  "ttc_spy.trigger_mask.err_com",
                  "Trigger on all communication errors",
                  this);

  monitor.addItem("itemset-ttcspytrigger",
                  "ttc_spy.trigger_mask.brc_dddd",
                  "Trigger on broadcast B-commands with this value in the user-data bits (if non-zero)",
                  this);
  monitor.addItem("itemset-ttcspytrigger",
                  "ttc_spy.trigger_mask.brc_tt",
                  "Trigger on broadcast B-commands with this value in the test-data bits (if non-zero)",
                  this);

  monitor.addItem("itemset-ttcspytrigger",
                  "ttc_spy.trigger_mask.brc_val0",
                  "Trigger on broadcast B-commands with this value in the (8-bit) data (1 of 6)",
                  this);
  monitor.addItem("itemset-ttcspytrigger",
                  "ttc_spy.trigger_mask.brc_val1",
                  "Trigger on broadcast B-commands with this value in the (8-bit) data (2 of 6)",
                  this);
  monitor.addItem("itemset-ttcspytrigger",
                  "ttc_spy.trigger_mask.brc_val2",
                  "Trigger on broadcast B-commands with this value in the (8-bit) data (3 of 6)",
                  this);
  monitor.addItem("itemset-ttcspytrigger",
                  "ttc_spy.trigger_mask.brc_val3",
                  "Trigger on broadcast B-commands with this value in the (8-bit) data (4 of 6)",
                  this);
  monitor.addItem("itemset-ttcspytrigger",
                  "ttc_spy.trigger_mask.brc_val4",
                  "Trigger on broadcast B-commands with this value in the (8-bit) data (5 of 6)",
                  this);
  monitor.addItem("itemset-ttcspytrigger",
                  "ttc_spy.trigger_mask.brc_val5",
                  "Trigger on broadcast B-commands with this value in the (8-bit) data (6 of 6)",
                  this);

  // The TTCSpy log.
  monitor.newItemSet("itemset-ttcspylog");
  monitor.addItem("itemset-ttcspylog",
                  "ttcspy_log",
                  "TTCSpy log contents",
                  this);
}

void
tcds::pi::TTCSpyInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                tcds::utils::Monitor& monitor,
                                                                std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "TTCSpy" : forceTabName;

  webServer.registerTab(tabName,
                        "The TTCSpy configuration settings"
                        " and the recent TTCSpy log contents."
                        " NOTE: The TTCSpy is _not_ intended to be deadtime-free."
                        " It runs in 'repeated single-shot' mode:"
                        " it repeatedly fills up its buffer,"
                        " reads out, and then continues logging."
                        " In between iterations, potential log entries may be lost.",
                        1);
  webServer.registerTable("TTCSpy logging settings",
                          "TTCSpy logging settings",
                          monitor,
                          "itemset-ttcspylogging",
                          tabName);
  webServer.registerTable("TTCSpy trigger settings",
                          "TTCSpy trigger settings",
                          monitor,
                          "itemset-ttcspytrigger",
                          tabName);
  webServer.registerWebObject<WebTableTTCSpyLog>("TTCSpy log",
                                                 "TTCSpy log contents: entries are identified by their orbit and BX numbers. The 'delta' column shows the time difference between each entry and the previous entry in 'orbits:BXs' form.",
                                                 monitor,
                                                 "itemset-ttcspylog",
                                                 tabName);
}

std::string
tcds::pi::TTCSpyInfoSpaceHandler::formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const
{
  // Specialized formatting rules for the TTCSpy logging settings enums.
  std::string res = tcds::utils::escapeAsJSONString(tcds::utils::InfoSpaceHandler::kInvalidItemString);
  if (item->isValid())
    {
      std::string name = item->name();
      if (name == "ttc_spy.control.logging_mode")
        {
          uint32_t val = getUInt32(name);
          switch (val)
            {
            case tcds::pi::TCADevicePI::TTC_LOGGING_MODE_ONLY:
              res = "LogOnly";
              break;
            case tcds::pi::TCADevicePI::TTC_LOGGING_MODE_EXCEPT:
              res = "LogAllExcept";
              break;
            default:
              // ASSERT ASSERT ASSERT
              assert (false);
              // ASSERT ASSERT ASSERT end
              break;
            }
          res = tcds::utils::escapeAsJSONString(res);
        }
      else if (name == "ttc_spy.control.trigger_combination_operator")
        {
          uint32_t val = getUInt32(name);
          switch (val)
            {
            case tcds::pi::TCADevicePI::TTC_LOGGING_LOGIC_OR:
              res = "OR";
              break;
            case tcds::pi::TCADevicePI::TTC_LOGGING_LOGIC_AND:
              res = "AND";
              break;
            default:
              // ASSERT ASSERT ASSERT
              assert (false);
              // ASSERT ASSERT ASSERT end
              break;
            }
          res = tcds::utils::escapeAsJSONString(res);
        }
      else if (name == "ttcspy_log")
        {
          // Special in the sense that this is something that needs to
          // be interpreted as a proper JavaScript object, so let's
          // not add any more double quotes.
          res = getString(name);
        }
      else
        {
          // For everything else simply call the generic formatter.
          res = InfoSpaceHandler::formatItem(item);
        }
    }
  return res;
}
