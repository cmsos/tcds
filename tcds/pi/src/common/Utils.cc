#include "tcds/pi/Utils.h"

#include <stdint.h>

#include "toolbox/string.h"
#include "xcept/Exception.h"

#include "tcds/exception/Exception.h"
#include "tcds/pi/Definitions.h"
#include "tcds/utils/ConfigurationInfoSpaceHandler.h"
#include "tcds/utils/Definitions.h"
#include "tcds/utils/Utils.h"

bool
tcds::pi::isTTSInput(tcds::definitions::TTS_SOURCE const channel)
{
  bool const res = ((channel == tcds::definitions::TTS_SOURCE_RJ45)
                    || (channel == tcds::definitions::TTS_SOURCE_FED1)
                    || (channel == tcds::definitions::TTS_SOURCE_FED2)
                    || (channel == tcds::definitions::TTS_SOURCE_FED3)
                    || (channel == tcds::definitions::TTS_SOURCE_FED4)
                    || (channel == tcds::definitions::TTS_SOURCE_FED5)
                    || (channel == tcds::definitions::TTS_SOURCE_FED6)
                    || (channel == tcds::definitions::TTS_SOURCE_FED7)
                    || (channel == tcds::definitions::TTS_SOURCE_FED8)
                    || (channel == tcds::definitions::TTS_SOURCE_FED9)
                    || (channel == tcds::definitions::TTS_SOURCE_FED10));
  return res;
}

bool
tcds::pi::isTTSOutput(tcds::definitions::TTS_SOURCE const channel)
{
  bool const res = (channel == tcds::definitions::TTS_SOURCE_OUTPUT_FROM_PI);
  return res;
}

std::string
tcds::pi::ttsOutputModeToString(tcds::definitions::PI_TTS_OUTPUT_MODE const mode)
{
  std::string res;

  switch (mode)
    {
    case tcds::definitions::PI_TTS_OUTPUT_MODE_FORCED:
      res = "forced";
      break;
    case tcds::definitions::PI_TTS_OUTPUT_MODE_OR:
      res = "TTS OR";
      break;
    default:
      XCEPT_RAISE(tcds::exception::ValueError,
                  toolbox::toString("Number %d is not a valid PI TTS output mode.",
                                    mode));
      break;
    }

  return res;
}

std::string
tcds::pi::configModeToString(tcds::definitions::PICONTROLLER_CONFIG_MODE const mode)
{
  std::string res;

  switch (mode)
    {
    case tcds::definitions::PICONTROLLER_CONFIG_MODE_LEGACY_FMM:
      res = "legacy-FMM";
      break;
    case tcds::definitions::PICONTROLLER_CONFIG_MODE_AMC13:
      res = "AMC13-FED";
      break;
    case tcds::definitions::PICONTROLLER_CONFIG_MODE_MIXED:
      res = "mixed";
      break;
    case tcds::definitions::PICONTROLLER_CONFIG_MODE_NOTTS:
      res = "no-TTS";
      break;
    default:
      XCEPT_RAISE(tcds::exception::ValueError,
                  toolbox::toString("Number %d is not a valid PI configuration mode.",
                                    mode));
      break;
    }

  return res;
}

std::map<tcds::definitions::TTS_SOURCE, std::string>
tcds::pi::ttsChannelRegNameMap()
{
  std::map<tcds::definitions::TTS_SOURCE, std::string> regNames;

  regNames[tcds::definitions::TTS_SOURCE_FED1] = "fed1";
  regNames[tcds::definitions::TTS_SOURCE_FED2] = "fed2";
  regNames[tcds::definitions::TTS_SOURCE_FED3] = "fed3";
  regNames[tcds::definitions::TTS_SOURCE_FED4] = "fed4";
  regNames[tcds::definitions::TTS_SOURCE_FED5] = "fed5";
  regNames[tcds::definitions::TTS_SOURCE_FED6] = "fed6";
  regNames[tcds::definitions::TTS_SOURCE_FED7] = "fed7";
  regNames[tcds::definitions::TTS_SOURCE_FED8] = "fed8";
  regNames[tcds::definitions::TTS_SOURCE_FED9] = "fed9";
  regNames[tcds::definitions::TTS_SOURCE_FED10] = "fed10";
  regNames[tcds::definitions::TTS_SOURCE_RJ45] = "rj45";
  regNames[tcds::definitions::TTS_SOURCE_OUTPUT_FROM_OR] = "or";
  regNames[tcds::definitions::TTS_SOURCE_OUTPUT_FROM_FORCE_REGISTER] = "force";
  regNames[tcds::definitions::TTS_SOURCE_OUTPUT_FROM_PI] = "out";

  return regNames;
}

std::string
tcds::pi::ttsChannelToRegName(tcds::definitions::TTS_SOURCE const channel)
{
  std::map<tcds::definitions::TTS_SOURCE, std::string> regNames = ttsChannelRegNameMap();
  std::map<tcds::definitions::TTS_SOURCE, std::string>::const_iterator i = regNames.find(channel);
  if (i == regNames.end())
    {
      XCEPT_RAISE(tcds::exception::ValueError,
                  toolbox::toString("Number %d is not a valid TTS channel (for register name lookups).",
                                    channel));
    }
  return i->second;
}

std::map<tcds::definitions::OC0_REASON, std::string>
tcds::pi::OC0ReasonMap()
{
  std::map<tcds::definitions::OC0_REASON, std::string> reasons;

  reasons[tcds::definitions::OC0_REASON_UNKNOWN] = "unknown";
  reasons[tcds::definitions::OC0_REASON_RESET] = "TTS logger reset";
  reasons[tcds::definitions::OC0_REASON_BGO] = "OC0 B-go";
  reasons[tcds::definitions::OC0_REASON_SW] = "software reset";
  reasons[tcds::definitions::OC0_REASON_NA] = "n/a";

  return reasons;
}

std::string
tcds::pi::OC0ReasonToString(tcds::definitions::OC0_REASON const reason)
{
  std::map<tcds::definitions::OC0_REASON, std::string> reasons = OC0ReasonMap();
  std::map<tcds::definitions::OC0_REASON, std::string>::const_iterator i = reasons.find(reason);
  if (i == reasons.end())
    {
      XCEPT_RAISE(tcds::exception::ValueError,
                  toolbox::toString("Number %d is not a valid 'OC0 reason' (for 'OC0 reason' string lookups).",
                                    reason));
    }
  return i->second;
}

std::map<tcds::definitions::TTS_SOURCE, std::string>
tcds::pi::ttsChannelMap()
{
  std::map<tcds::definitions::TTS_SOURCE, std::string> ttsMap;

  ttsMap[tcds::definitions::TTS_SOURCE_FED1] = "FED input 1";
  ttsMap[tcds::definitions::TTS_SOURCE_FED2] = "FED input 2";
  ttsMap[tcds::definitions::TTS_SOURCE_FED3] = "FED input 3";
  ttsMap[tcds::definitions::TTS_SOURCE_FED4] = "FED input 4";
  ttsMap[tcds::definitions::TTS_SOURCE_FED5] = "FED input 5";
  ttsMap[tcds::definitions::TTS_SOURCE_FED6] = "FED input 6";
  ttsMap[tcds::definitions::TTS_SOURCE_FED7] = "FED input 7";
  ttsMap[tcds::definitions::TTS_SOURCE_FED8] = "FED input 8";
  ttsMap[tcds::definitions::TTS_SOURCE_FED9] = "FED input 9";
  ttsMap[tcds::definitions::TTS_SOURCE_FED10] = "FED input 10";

  ttsMap[tcds::definitions::TTS_SOURCE_RJ45] = "FMM input";

  ttsMap[tcds::definitions::TTS_SOURCE_OUTPUT_FROM_OR] = "OR output";
  ttsMap[tcds::definitions::TTS_SOURCE_OUTPUT_FROM_FORCE_REGISTER] = "Force-TTS value";
  ttsMap[tcds::definitions::TTS_SOURCE_OUTPUT_FROM_PI] = "PI output";

  return ttsMap;
}

std::string
tcds::pi::formatTTSSourceLabel(tcds::definitions::TTS_SOURCE const source)
{
  return formatTTSChannelLabel(source);
}

std::string
tcds::pi::formatTTSSourceLabel(tcds::definitions::TTS_SOURCE const source,
                               tcds::utils::ConfigurationInfoSpaceHandler const& cfgInfoSpace)
{
  return formatTTSChannelLabel(source, cfgInfoSpace);
}

std::string
tcds::pi::formatTTSChannelLabel(tcds::definitions::TTS_SOURCE const channel)
{
  std::map<tcds::definitions::TTS_SOURCE, std::string> channelNames = ttsChannelMap();
  std::map<tcds::definitions::TTS_SOURCE, std::string>::const_iterator i = channelNames.find(channel);
  if (i == channelNames.end())
    {
      XCEPT_RAISE(tcds::exception::ValueError,
                  toolbox::toString("Number %d is not a valid TTS channel (for channel label mappings)).",
                                    channel));
    }
  return i->second;
}

std::string
tcds::pi::formatTTSChannelLabel(tcds::definitions::TTS_SOURCE const channel,
                                tcds::utils::ConfigurationInfoSpaceHandler const& cfgInfoSpace)
{
  // The TTS source/channel label is based on the FED number from the
  // XDAQ configuration. Unset FED numbers default to kImpossibleFEDId
  // (= 4095), which is assumed to represent an unused input.
  std::string label = "UNKNOWN";
  if ((tcds::definitions::TTS_SOURCE_MIN <= channel) &&
      (channel <= tcds::definitions::TTS_SOURCE_MAX))
    {
      uint32_t const tmp = cfgInfoSpace.getUInt32("configurationMode");
      tcds::definitions::PICONTROLLER_CONFIG_MODE configMode =
        static_cast<tcds::definitions::PICONTROLLER_CONFIG_MODE>(tmp);

      // First treat all the TTS sources.
      if (channel == tcds::definitions::TTS_SOURCE_RJ45)
        {
          // The RJ45 input from the FMM output.
          // NOTE: The RJ45 input is only used in certain cases. As
          // in: in legacy mode, or in mixed mode when there is at
          // least one FED connected via the FMM.
          label = formatTTSChannelLabel(static_cast<tcds::definitions::TTS_SOURCE>(channel));
          if (configMode == tcds::definitions::PICONTROLLER_CONFIG_MODE_AMC13)
            {
              label += " (unused)";
            }
          else if (configMode == tcds::definitions::PICONTROLLER_CONFIG_MODE_MIXED)
            {
              std::vector<uint32_t> fmmFEDIds = cfgInfoSpace.getUInt32Vec("fmmFEDs");
              if (fmmFEDIds.size() < 1)
                {
                  label += " (unused)";
                }
            }
          else if (configMode == tcds::definitions::PICONTROLLER_CONFIG_MODE_NOTTS)
            {
              label += " (unused)";
            }
        }
      else
        {
          // An AMC13 FED input.
          if (configMode == tcds::definitions::PICONTROLLER_CONFIG_MODE_LEGACY_FMM)
            {
              label = formatTTSChannelLabel(static_cast<tcds::definitions::TTS_SOURCE>(channel));
              label += " (unused)";
            }
          else if ((configMode == tcds::definitions::PICONTROLLER_CONFIG_MODE_AMC13) ||
                   (configMode == tcds::definitions::PICONTROLLER_CONFIG_MODE_MIXED))
            {
              uint32_t const fedId =
                cfgInfoSpace.getUInt32(toolbox::toString("fedIdInput%d", channel));
              if (fedId == tcds::definitions::kImpossibleFEDId)
                {
                  label = formatTTSChannelLabel(static_cast<tcds::definitions::TTS_SOURCE>(channel));
                  label += " (unused)";
                }
              else
                {
                  label = toolbox::toString("FED %d", fedId);
                }
            }
          else if (configMode == tcds::definitions::PICONTROLLER_CONFIG_MODE_NOTTS)
            {
              label = formatTTSChannelLabel(static_cast<tcds::definitions::TTS_SOURCE>(channel));
              label += " (unused)";
            }
        }
    }
  else
    {
      // Then treat all the 'calculated' TTS values.
      label = formatTTSChannelLabel(static_cast<tcds::definitions::TTS_SOURCE>(channel));
    }
  return label;
}

void
tcds::pi::ensureOutputDirExists(std::string const& path)
{
  // Check if the output directory exists, or can be made.
  if (!tcds::utils::pathIsDir(path))
    {
      // Either the path does not exist, or it is not a directory.
      if (tcds::utils::pathExists(path))
        {
          // Path exists but is not a directory. Nothing we can do
          // about that.
          std::string const msg = toolbox::toString("Problem with PI TTS log output directory '%s': path exists but is not a directory.",
                                                    path.c_str());
          XCEPT_RAISE(tcds::exception::RuntimeProblem, msg.c_str());
        }
      else
        {
          // Path simply does not exist. Let's try to create a
          // directory.
          tcds::utils::makeDir(path);

          // Now check that it worked.
          if (!tcds::utils::pathIsDir(path))
            {
              // Failed to create our output directory. Give up.
              std::string const msg = toolbox::toString("Failed to create PI TTS log output directory '%s'",
                                                        path.c_str());
              XCEPT_RAISE(tcds::exception::RuntimeProblem, msg.c_str());
            }
        }
    }
}
