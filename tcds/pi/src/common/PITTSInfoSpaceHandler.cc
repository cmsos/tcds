#include "tcds/pi/PITTSInfoSpaceHandler.h"

#include <stdint.h>

#include "toolbox/string.h"

#include "tcds/pi/PITTSAuxInfoSpaceHandler.h"
#include "tcds/pi/Utils.h"
#include "tcds/utils/Definitions.h"
#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/Utils.h"
#include "tcds/utils/WebServer.h"
#include "tcds/utils/WebTableTTS.h"
#include "tcds/utils/XDAQAppBase.h"

namespace xdaq {
  class Application;
}

namespace tcds {
  namespace utils {
    class ConfigurationInfoSpaceHandler;
  }
}

tcds::pi::PITTSInfoSpaceHandler::PITTSInfoSpaceHandler(xdaq::Application& xdaqApp,
                                                       tcds::utils::InfoSpaceUpdater* updater) :
  tcds::utils::MultiInfoSpaceHandler(xdaqApp, updater)
{
  tcds::utils::ConfigurationInfoSpaceHandler const& cfgInfoSpace =
    getOwnerApplication().getConfigurationInfoSpaceHandler();

  // Pre-build all input labels. The label is based on the FED number
  // from the XDAQ configuration. Unset FED numbers default to
  // kImpossibleFEDId (= 4095), which is assumed to represent an
  // unused input.
  for (unsigned int source = tcds::definitions::TTS_SOURCE_MIN;
       source <= tcds::definitions::TTS_SOURCE_MAX;
       ++source)
    {
      tcds::definitions::TTS_SOURCE const channel = static_cast<tcds::definitions::TTS_SOURCE>(source);
      std::string const name = ttsChannelToRegName(channel);
      std::string const label = formatTTSSourceLabel(channel, cfgInfoSpace);
      inputNames_[channel] = name;
      inputLabels_[channel] = label;
    }

  //----------

  // This InfoSpaceHandler is used for 'all the stuff that does not
  // fit elsewhere.'
  tcds::utils::InfoSpaceHandler* handler =
    new tcds::pi::PITTSAuxInfoSpaceHandler(xdaqApp, "tcds-pi-l1a-blocker-cfg", updater);
  infoSpaceHandlers_["misc"] = handler;

  //----------

  // The overall partition controller state (in the firmware).
  handler = infoSpaceHandlers_["misc"];
  std::string name = "tts_output_control.tts_out_enable";
  handler->createUInt32(name, 0, "");

  // The TTS output state.
  // NOTE: This is the actual output value. Depending on the
  // tts_out_enable settings this may either come from the TTS OR, or
  // simply be forced to a certain value.
  handler = new tcds::utils::InfoSpaceHandler(xdaqApp, "tcds-pi-tts-info-top", updater);
  name = "tts_outgoing.tts_out";
  infoSpaceHandlers_[name] = handler;
  createTTSChannel(handler,
                   "Top-level TTS",
                   tcds::utils::ttsChannelTypeToString(tcds::definitions::TTS_CHANNEL_TOP_LEVEL),
                   0,
                   name);

  //----------

  // All individual TTS input values.
  for (unsigned int source = tcds::definitions::TTS_SOURCE_MIN;
       source <= tcds::definitions::TTS_SOURCE_MAX;
       ++source)
    {
      tcds::definitions::TTS_SOURCE const channel = static_cast<tcds::definitions::TTS_SOURCE>(source);
      tcds::definitions::TTS_CHANNEL_TYPE const channelType =
        (channel == tcds::definitions::TTS_SOURCE_RJ45) ?
        tcds::definitions::TTS_CHANNEL_RJ45 : tcds::definitions::TTS_CHANNEL_FED;

      std::string const inputName = inputNames_[channel];
      std::string const name = toolbox::toString("tts_incoming_masked.%s",
                                                 inputName.c_str());

      tcds::utils::InfoSpaceHandler* handler =
        new tcds::utils::InfoSpaceHandler(xdaqApp,
                                          toolbox::toString("tcds-pi-tts-info-%s",
                                          inputName.c_str()),
                                          updater);
      infoSpaceHandlers_[name] = handler;
      createTTSChannel(handler,
                       inputLabels_[channel],
                       tcds::utils::ttsChannelTypeToString(channelType),
                       source,
                       name);
    }

  //----------

  // Link error flags for all inputs.
  handler = infoSpaceHandlers_["misc"];
  for (unsigned int source = tcds::definitions::TTS_SOURCE_MIN;
       source <= tcds::definitions::TTS_SOURCE_MAX;
       ++source)
    {
      tcds::definitions::TTS_SOURCE const channel = static_cast<tcds::definitions::TTS_SOURCE>(source);
      if (channel != tcds::definitions::TTS_SOURCE_RJ45)
        {
          std::string const inputName = inputNames_[channel];
          std::string const name = toolbox::toString("tts_incoming_unmasked.%s_rx_error",
                                                     inputName.c_str());
          handler->createBool(name, 0, "");
        }
    }
}

tcds::pi::PITTSInfoSpaceHandler::~PITTSInfoSpaceHandler()
{
}

tcds::utils::XDAQAppBase&
tcds::pi::PITTSInfoSpaceHandler::getOwnerApplication() const
{
  return static_cast<tcds::utils::XDAQAppBase&>(tcds::utils::MultiInfoSpaceHandler::getOwnerApplication());
}

void
tcds::pi::PITTSInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  // Over-all TTS enable flag and final-OR output value.
  std::string itemSetName = "Top-level TTS";
  monitor.newItemSet(itemSetName);
  monitor.addItem(itemSetName,
                  "tts_output_control.tts_out_enable",
                  "TTS output mode",
                  infoSpaceHandlers_.at("misc"),
                  "TTS state output mode. The output state can either come"
                  " from the TTS OR of the incoming TTS states, or be forced to a fixed value.");
  monitor.addItem(itemSetName,
                  "tts_source_value",
                  "TTS output value",
                  infoSpaceHandlers_.at("tts_outgoing.tts_out"),
                  "The actual TTS output state propagated to the LPM.");

  //----------

  // All individual TTS input values.
  itemSetName = "TTS input values";
  monitor.newItemSet(itemSetName);
  for (unsigned int source = tcds::definitions::TTS_SOURCE_MIN;
       source <= tcds::definitions::TTS_SOURCE_MAX;
       ++source)
    {
      tcds::definitions::TTS_SOURCE const channel = static_cast<tcds::definitions::TTS_SOURCE>(source);
      std::string const inputName = inputNames_[channel];
      std::string const name = toolbox::toString("tts_incoming_masked.%s",
                                                 inputName.c_str());
      monitor.addItem(itemSetName,
                      "tts_source_value",
                      inputLabels_[channel],
                      infoSpaceHandlers_.at(name));
    }

  //----------

  // Link error flags for all inputs.
  itemSetName = "TTS link status";
  monitor.newItemSet(itemSetName);
  for (unsigned int source = tcds::definitions::TTS_SOURCE_MIN;
       source <= tcds::definitions::TTS_SOURCE_MAX;
       ++source)
    {
      tcds::definitions::TTS_SOURCE const channel = static_cast<tcds::definitions::TTS_SOURCE>(source);
      if (channel != tcds::definitions::TTS_SOURCE_RJ45)
        {
          std::string const inputName = inputNames_[channel];
          std::string const name = toolbox::toString("tts_incoming_unmasked.%s_rx_error",
                                                     inputName.c_str());
          monitor.addItem(itemSetName,
                          name,
                          toolbox::toString("%s RX error", inputLabels_[channel].c_str()),
                          infoSpaceHandlers_.at("misc"));
        }
    }
}

void
tcds::pi::PITTSInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                               tcds::utils::Monitor& monitor,
                                                               std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "TTS" : forceTabName;

  webServer.registerTab(tabName,
                        "TTS information",
                        2);
  //----------

  // Top-level TTS configuration and state.
  webServer.registerWebObject<tcds::utils::WebTableTTS>("Top-level TTS",
                                                        "Over-all TTS information",
                                                        monitor,
                                                        "Top-level TTS",
                                                        tabName,
                                                        2);

  //----------

  // All individual FED TTS input values.
  std::string itemSetName = "TTS input values";
  webServer.registerWebObject<tcds::utils::WebTableTTS>(itemSetName,
                                                        "",
                                                        monitor,
                                                        itemSetName,
                                                        tabName);

  //----------

  // Link error flags for all inputs.
  itemSetName = "TTS link status";
  webServer.registerTable(itemSetName,
                          "",
                          monitor,
                          itemSetName,
                          tabName);
}

void
tcds::pi::PITTSInfoSpaceHandler::createTTSChannel(tcds::utils::InfoSpaceHandler* const handler,
                                                  std::string const sourceLabel,
                                                  std::string const sourceType,
                                                  unsigned int const idNumber,
                                                  std::string const regName)
{
  // A human-readable label for this TTS channel.
  handler->createString("tts_source_label",
                        sourceLabel,
                        "",
                        tcds::utils::InfoSpaceItem::NOUPDATE);

  // An identifier string indicating the source type of this TTS
  // channel.
  handler->createString("tts_source_type",
                        sourceType,
                        "",
                        tcds::utils::InfoSpaceItem::NOUPDATE);

  // The source ID/FED number of the source of this TTS channel.
  handler->createUInt32("tts_source_id_number",
                        idNumber,
                        "",
                        tcds::utils::InfoSpaceItem::NOUPDATE);

  // The actual TTS value for this TTS channel.
  handler->createUInt32("tts_source_value",
                        tcds::definitions::TTS_STATE_UNKNOWN_TCDS,
                        "tts_state",
                        tcds::utils::InfoSpaceItem::HW32,
                        false,
                        regName);
}
