#include "tcds/pi/WebTableTTSLog.h"

#include <cassert>
#include <sstream>
#include <string>

#include "tcds/utils/Monitor.h"

tcds::pi::WebTableTTSLog::WebTableTTSLog(std::string const& name,
                                         std::string const& description,
                                         tcds::utils::Monitor const& monitor,
                                         std::string const& itemSetName,
                                         std::string const& tabName,
                                         size_t const colSpan) :
  tcds::utils::WebObject(name, description, monitor, itemSetName, tabName, colSpan)
{
}

std::string
tcds::pi::WebTableTTSLog::getHTMLString() const
{
  // NOTE: This has been written to work with the new XDAQ12-style
  // HyperDAQ tabs and doT.js.

  std::stringstream res;

  res << "<div class=\"tcds-item-table-wrapper\">"
      << "\n";

  res << "<p class=\"tcds-item-table-title\">"
      << getName()
      << "</p>";
  res << "\n";

  res << "<p class=\"tcds-item-table-description\">"
      << getDescription()
      << "</p>";
  res << "\n";

  tcds::utils::Monitor::StringPairVector items =
    monitor_.getFormattedItemSet(itemSetName_);
  // ASSERT ASSERT ASSERT
  assert (items.size() == 1);
  // ASSERT ASSERT ASSERT end

  // NOTE: This one is kinda special. Mainly due to the fact that this
  // table can become incredibly long. Therefore this is handled
  // completely in JavaScript (with the help of a cool, virtual
  // scrolling grid plugin).
  res << "<div id=\"ttslog-placeholder\"></div>"
      << "\n";

  return res.str();
}
