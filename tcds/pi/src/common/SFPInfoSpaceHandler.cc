#include "tcds/pi/SFPInfoSpaceHandler.h"

#include <cstddef>

#include "toolbox/string.h"

#include "tcds/hwlayertca/Definitions.h"
#include "tcds/hwutilstca/SFPInfoSpaceUpdater.h"
#include "tcds/hwlayertca/Utils.h"
#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/WebServer.h"
#include "tcds/utils/XDAQAppBase.h"

tcds::pi::SFPInfoSpaceHandler::SFPInfoSpaceHandler(tcds::utils::XDAQAppBase& xdaqApp,
                                                   tcds::hwutilstca::SFPInfoSpaceUpdater* updater) :
  tcds::hwutilstca::SFPInfoSpaceHandlerBase(xdaqApp, updater)
{
  // The SFPs connecting to the primary and secondary LPMs.
  for (unsigned int source = tcds::definitions::SFP_PI_LPM_MIN;
       source <= tcds::definitions::SFP_PI_LPM_MAX;
       ++source)
    {
      tcds::definitions::SFP_PI const sourceEnum =
        static_cast<tcds::definitions::SFP_PI>(source);
      std::string const name = tcds::hwlayertca::sfpToRegName(sourceEnum);
      std::string const infoSpaceHandlerName = toolbox::toString("tcds-sfp-info-pi-%s",
                                                                 name.c_str());

      tcds::utils::InfoSpaceHandler* handler =
        new tcds::utils::InfoSpaceHandler(xdaqApp, infoSpaceHandlerName, updater);
      infoSpaceHandlers_[name] = handler;
      createSFPChannel(handler,
                       tcds::definitions::kSFP_TYPE_PI_LPM,
                       source,
                       name);
    }

  // The SFPs connecting to the FEDs.
  for (unsigned int source = tcds::definitions::SFP_PI_FED_MIN;
       source <= tcds::definitions::SFP_PI_FED_MAX;
       ++source)
    {
      tcds::definitions::SFP_PI const sourceEnum =
        static_cast<tcds::definitions::SFP_PI>(source);
      std::string const name = tcds::hwlayertca::sfpToRegName(sourceEnum);
      std::string const infoSpaceHandlerName = toolbox::toString("tcds-sfp-info-pi-%s",
                                                                 name.c_str());

      tcds::utils::InfoSpaceHandler* handler =
        new tcds::utils::InfoSpaceHandler(xdaqApp, infoSpaceHandlerName, updater);
      infoSpaceHandlers_[name] = handler;
      createSFPChannel(handler,
                       tcds::definitions::kSFP_TYPE_PI_FED,
                       source,
                       name);
    }
}

tcds::pi::SFPInfoSpaceHandler::~SFPInfoSpaceHandler()
{
  // Cleanup of InfoSpaceHandlers happens in the MultiInfoSpaceHandler
  // base class.
}

void
tcds::pi::SFPInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  // The SFPs connecting to the primary and secondary LPMs.
  for (unsigned int source = tcds::definitions::SFP_PI_LPM_MIN;
       source <= tcds::definitions::SFP_PI_LPM_MAX;
       ++source)
    {
      tcds::definitions::SFP_PI const sourceEnum =
        static_cast<tcds::definitions::SFP_PI>(source);
      std::string const name = tcds::hwlayertca::sfpToShortName(sourceEnum);
      std::string const itemSetName = toolbox::toString("itemset-sfp-status-%s",
                                                        name.c_str());
      registerSFPChannel(tcds::hwlayertca::sfpToRegName(sourceEnum), itemSetName, monitor);
    }

  // The SFPs connecting to the FEDs.
  for (unsigned int source = tcds::definitions::SFP_PI_FED_MIN;
       source <= tcds::definitions::SFP_PI_FED_MAX;
       ++source)
    {
      tcds::definitions::SFP_PI const sourceEnum =
        static_cast<tcds::definitions::SFP_PI>(source);
      std::string const name = tcds::hwlayertca::sfpToShortName(sourceEnum);
      std::string const itemSetName = toolbox::toString("itemset-sfp-status-%s",
                                                        name.c_str());
      registerSFPChannel(tcds::hwlayertca::sfpToRegName(sourceEnum), itemSetName, monitor);
    }
}

void
tcds::pi::SFPInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                             tcds::utils::Monitor& monitor,
                                                             std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "SFPs" : forceTabName;
  size_t const numColumns = 5;

  webServer.registerTab(tabName, "SFP monitoring", numColumns);

  //----------

  // The SFPs connecting to the primary and secondary LPMs.
  for (unsigned int source = tcds::definitions::SFP_PI_LPM_MIN;
       source <= tcds::definitions::SFP_PI_LPM_MAX;
       ++source)
    {
      tcds::definitions::SFP_PI const sourceEnum =
        static_cast<tcds::definitions::SFP_PI>(source);
      std::string const name = tcds::hwlayertca::sfpToShortName(sourceEnum);
      std::string const itemSetName = toolbox::toString("itemset-sfp-status-%s",
                                                        name.c_str());
      webServer.registerTable(tcds::hwlayertca::sfpToLongName(sourceEnum),
                              "",
                              monitor,
                              itemSetName,
                              tabName);
    }

  size_t numSFPs = (tcds::definitions::SFP_PI_LPM_MAX -
                    tcds::definitions::SFP_PI_LPM_MIN) + 1;
  size_t numLeft = numSFPs % numColumns;
  if (numLeft)
    {
      size_t const numSpacers = numColumns - numLeft;
      webServer.registerSpacer("Spacer",
                               "",
                               monitor,
                               "",
                               tabName,
                               numSpacers);
    }

  //----------

  // The SFPs connecting to the FEDs.
  for (unsigned int source = tcds::definitions::SFP_PI_FED_MIN;
       source <= tcds::definitions::SFP_PI_FED_MAX;
       ++source)
    {
      tcds::definitions::SFP_PI const sourceEnum =
        static_cast<tcds::definitions::SFP_PI>(source);
      std::string const name = tcds::hwlayertca::sfpToShortName(sourceEnum);
      std::string const itemSetName = toolbox::toString("itemset-sfp-status-%s",
                                                        name.c_str());
      webServer.registerTable(tcds::hwlayertca::sfpToLongName(sourceEnum),
                              "",
                              monitor,
                              itemSetName,
                              tabName);
    }

  numSFPs = (tcds::definitions::SFP_PI_FED_MAX -
             tcds::definitions::SFP_PI_FED_MIN) + 1;
  numLeft = numSFPs % numColumns;
  if (numLeft)
    {
      size_t const numSpacers = numColumns - numLeft;
      webServer.registerSpacer("Spacer",
                               "",
                               monitor,
                               "",
                               tabName,
                               numSpacers);
    }
}
