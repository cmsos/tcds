#include "tcds/pi/PITTSLinksInfoSpaceUpdater.h"

#include "tcds/hwlayertca/TCADeviceBase.h"
#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/InfoSpaceItem.h"

tcds::pi::PITTSLinksInfoSpaceUpdater::PITTSLinksInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                                                 tcds::hwlayertca::TCADeviceBase const& hw) :
  tcds::utils::HwInfoSpaceUpdaterBase(xdaqApp, hw)
{
}

bool
tcds::pi::PITTSLinksInfoSpaceUpdater::updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                                          tcds::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  tcds::hwlayer::DeviceBase const& hw = getHw();
  bool updated = false;
  if (hw.isReadyForUse())
    {
      if (!updated)
        {
          updated = tcds::utils::HwInfoSpaceUpdaterBase::updateInfoSpaceItem(item, infoSpaceHandler);
        }
    }

  if (updated)
    {
      item.setValid();
    }
  else
    {
      item.setInvalid();
    }

  return updated;
}
