#include "tcds/pi/TTSLogEntry.h"

#include <cassert>

#include "toolbox/string.h"
#include "xcept/Exception.h"

#include "tcds/exception/Exception.h"
#include "tcds/pi/Utils.h"

tcds::pi::TTSLogEntry::TTSLogEntry(std::string const& comment) :
  comment_(comment)
{
  // NOTE: Each entry can be accompanied by a simple text
  // comment. Entries with orbit number 0 and BX number 0 are used as
  // 'pure comments.'
  std::vector<uint32_t> const tmp(kNumWordsPerLogEntry, 0x0);
  addData(tmp);
}

tcds::pi::TTSLogEntry::TTSLogEntry(std::vector<uint32_t> const dataIn,
                                   std::string const& comment) :
  comment_(comment)
{
  addData(dataIn);
}

void
tcds::pi::TTSLogEntry::addData(std::vector<uint32_t> const dataIn)
{
  if (dataIn.size() != kNumWordsPerLogEntry)
    {
      XCEPT_RAISE(tcds::exception::SoftwareProblem,
                  toolbox::toString("Got more or less data than expected "
                                    "(%d words instead of %d) "
                                    "for a single TTS log entry.",
                                    dataIn.size(), kNumWordsPerLogEntry));
    }

  // NOTE: The order of words is 'inverted.' See the documentation of
  // the log entry format in TTSLogEntry.h.
  data_.clear();
  for (std::vector<uint32_t>::const_reverse_iterator it = dataIn.rbegin();
       it != dataIn.rend();
       ++it)
    {
      std::vector<bool> tmp = intToVecBool<uint32_t>(*it);
      data_.insert(data_.begin(), tmp.begin(), tmp.end());
    }

  //----------

  // We have to keep the mapping of the locations of the TTS values in
  // the log entries somewhere...
  bitLocationMap_[tcds::definitions::TTS_SOURCE_FED1] = std::make_pair(kBitPosTTSValFED1Lo, kBitPosTTSValFED1Hi);
  bitLocationMap_[tcds::definitions::TTS_SOURCE_FED2] = std::make_pair(kBitPosTTSValFED2Lo, kBitPosTTSValFED2Hi);
  bitLocationMap_[tcds::definitions::TTS_SOURCE_FED3] = std::make_pair(kBitPosTTSValFED3Lo, kBitPosTTSValFED3Hi);
  bitLocationMap_[tcds::definitions::TTS_SOURCE_FED4] = std::make_pair(kBitPosTTSValFED4Lo, kBitPosTTSValFED4Hi);
  bitLocationMap_[tcds::definitions::TTS_SOURCE_FED5] = std::make_pair(kBitPosTTSValFED5Lo, kBitPosTTSValFED5Hi);
  bitLocationMap_[tcds::definitions::TTS_SOURCE_FED6] = std::make_pair(kBitPosTTSValFED6Lo, kBitPosTTSValFED6Hi);
  bitLocationMap_[tcds::definitions::TTS_SOURCE_FED7] = std::make_pair(kBitPosTTSValFED7Lo, kBitPosTTSValFED7Hi);
  bitLocationMap_[tcds::definitions::TTS_SOURCE_FED8] = std::make_pair(kBitPosTTSValFED8Lo, kBitPosTTSValFED8Hi);
  bitLocationMap_[tcds::definitions::TTS_SOURCE_FED9] = std::make_pair(kBitPosTTSValFED9Lo, kBitPosTTSValFED9Hi);
  bitLocationMap_[tcds::definitions::TTS_SOURCE_FED10] = std::make_pair(kBitPosTTSValFED10Lo, kBitPosTTSValFED10Hi);
  bitLocationMap_[tcds::definitions::TTS_SOURCE_RJ45] = std::make_pair(kBitPosTTSValRJ45Lo, kBitPosTTSValRJ45Hi);
  bitLocationMap_[tcds::definitions::TTS_SOURCE_OUTPUT_FROM_OR] = std::make_pair(kBitPosTTSValORLo, kBitPosTTSValORHi);
  bitLocationMap_[tcds::definitions::TTS_SOURCE_OUTPUT_FROM_FORCE_REGISTER] = std::make_pair(kBitPosTTSValForceLo, kBitPosTTSValForceHi);
  bitLocationMap_[tcds::definitions::TTS_SOURCE_OUTPUT_FROM_PI] = std::make_pair(kBitPosTTSValOutLo, kBitPosTTSValOutHi);
}

bool
tcds::pi::TTSLogEntry::isBaselineEntry() const
{
  // When the TTS logger (firmware) is started, it always logs a
  // baseline entry. From then on, only TTS state changes trigger
  // additional entries. This method retuns true for the first case,
  // and false for the second case.
  return isBitSet(kBitPosBaselineEntry);
}

bool
tcds::pi::TTSLogEntry::latestOC0WasSW() const
{
  return isBitSet(kBitPosLatestOC0WasSw);
}

bool
tcds::pi::TTSLogEntry::latestOC0WasBgo() const
{
  return isBitSet(kBitPosLatestOC0WasBgo);
}

tcds::definitions::OC0_REASON
tcds::pi::TTSLogEntry::getLatestOC0Reason() const
{
  tcds::definitions::OC0_REASON res = tcds::definitions::OC0_REASON_UNKNOWN;

  if (isPureComment())
    {
      res = tcds::definitions::OC0_REASON_NA;
    }
  else
    {
      bool const triggeredByBgo = latestOC0WasBgo();
      bool const triggeredBySW = latestOC0WasSW();

      if (triggeredByBgo && triggeredBySW)
        {
          // This should not happen, if the firmware does the right thing.
          res = tcds::definitions::OC0_REASON_UNKNOWN;
        }
      else if (triggeredByBgo)
        {
          res = tcds::definitions::OC0_REASON_BGO;
        }
      else if (triggeredBySW)
        {
          res = tcds::definitions::OC0_REASON_SW;
        }
      else
        {
          res = tcds::definitions::OC0_REASON_RESET;
        }
    }

  return res;
}

uint64_t
tcds::pi::TTSLogEntry::orbitNumber() const
{
  std::vector<bool> const tmpVec = getBits(kBitPosOrbitLo, kBitPosOrbitHi);
  uint64_t const res = vecBoolToUInt<uint64_t>(tmpVec);
  return res;
}

uint16_t
tcds::pi::TTSLogEntry::bxNumber() const
{
  std::vector<bool> const tmpVec = getBits(kBitPosBXNumLo, kBitPosBXNumHi);
  uint16_t const res = vecBoolToUInt<uint16_t>(tmpVec);
  return res;
}

uint16_t
tcds::pi::TTSLogEntry::ttsVal(tcds::definitions::TTS_SOURCE const channel) const
{
  size_t const bitLocLo = bitLocationMap_.at(channel).first;
  size_t const bitLocHi = bitLocationMap_.at(channel).second;
  std::vector<bool> tmpVec = getBits(bitLocLo, bitLocHi);
  uint16_t res = vecBoolToUInt<uint16_t>(tmpVec);
  return res;
}

bool
tcds::pi::TTSLogEntry::isEnabled(tcds::definitions::TTS_SOURCE const channel) const
{
  size_t const bitLocTmp = bitLocationMap_.at(channel).first;
  // NOTE: A bit cheeky, maybe, but let's use the first FED channel to
  // find the offset.
  size_t const tmp0 = kBitPosTTSValFED1Lo;
  size_t const tmp1 = kBitPosTTSEnabledFED1;
  size_t const offset = tmp1 - tmp0;
  size_t const bitLoc = bitLocTmp + offset;
  bool const res = isBitSet(bitLoc);
  return res;
}

bool
tcds::pi::TTSLogEntry::isForced(tcds::definitions::TTS_SOURCE const channel) const
{
  size_t const bitLocTmp = bitLocationMap_.at(channel).first;
  // NOTE: A bit cheeky, maybe, but let's use the first FED channel to
  // find the offset.
  size_t const tmp0 = kBitPosTTSValFED1Lo;
  size_t const tmp1 = kBitPosTTSForcedFED1;
  size_t const offset = tmp1 - tmp0;
  size_t const bitLoc = bitLocTmp + offset;
  bool const res = isBitSet(bitLoc);
  return res;
}

bool
tcds::pi::TTSLogEntry::isAligned(tcds::definitions::TTS_SOURCE const channel) const
{
  size_t const bitLocTmp = bitLocationMap_.at(channel).first;
  // NOTE: A bit cheeky, maybe, but let's use the first FED channel to
  // find the offset.
  size_t const tmp0 = kBitPosTTSValFED1Lo;
  size_t const tmp1 = kBitPosTTSAlignedFED1;
  size_t const offset = tmp1 - tmp0;
  size_t const bitLoc = bitLocTmp + offset;
  bool const res = isBitSet(bitLoc);
  return res;
}

bool
tcds::pi::TTSLogEntry::isPureComment() const
{
  // NOTE: Each entry can be accompanied by a simple text
  // comment. Entries with orbit number 0 and BX number 0 are used as
  // 'pure comments.'
  return ((orbitNumber() == 0) && (bxNumber() == 0));
}

std::string
tcds::pi::TTSLogEntry::comment() const
{
  return comment_;
}

std::string
tcds::pi::TTSLogEntry::detailedComment(std::map<tcds::definitions::TTS_SOURCE, std::string> ttsChannelLabels) const
{
  // - First of all the comment from the entry itself, if any.
  std::string comment = this->comment();
  // - A note in case this is a 'baseline entry.'
  if (isBaselineEntry())
    {
      if (!comment.empty())
        {
          comment += " ";
        }
      comment += "Baseline entry.";
    }

  // - A note in case the PI TTS output value is forced.
  if (isOutputForced())
    {
      if (!comment.empty())
        {
          comment += " ";
        }
      comment += "Output is forced!";
    }

  // - A note in case any of the PI TTS inputs are forced.
  std::vector<tcds::definitions::TTS_SOURCE> const forcedInputs = this->forcedInputs(true);
  if (forcedInputs.size() != 0)
    {
      if (!comment.empty())
        {
          comment += " ";
        }
      comment += "Forced input(s): ";
      for (std::vector<tcds::definitions::TTS_SOURCE>::const_iterator i = forcedInputs.begin();
           i != forcedInputs.end();
           ++i)
        {
          if (i != forcedInputs.begin())
            {
              comment += ", ";
            }
          tcds::definitions::TTS_SOURCE const tmp =
            static_cast<tcds::definitions::TTS_SOURCE>(*i);
          comment += ttsChannelLabels.at(tmp);
        }
      comment += ".";
    }

  // - A note in case any of the inputs are unaligned (and not
  //   disabled).
  std::vector<tcds::definitions::TTS_SOURCE> const unalignedInputs = this->unalignedInputs(true);
  if (unalignedInputs.size() != 0)
    {
      if (!comment.empty())
        {
          comment += " ";
        }
      comment += "Unaligned input(s): ";
      for (std::vector<tcds::definitions::TTS_SOURCE>::const_iterator i = unalignedInputs.begin();
           i != unalignedInputs.end();
           ++i)
        {
          if (i != unalignedInputs.begin())
            {
              comment += ", ";
            }
          tcds::definitions::TTS_SOURCE const tmp =
            static_cast<tcds::definitions::TTS_SOURCE>(*i);
          comment += ttsChannelLabels.at(tmp);
        }
      comment += ".";
    }
  return comment;
}

bool
tcds::pi::TTSLogEntry::isOutputForced() const
{
  return isForced(tcds::definitions::TTS_SOURCE_OUTPUT_FROM_PI);
}

std::vector<tcds::definitions::TTS_SOURCE>
tcds::pi::TTSLogEntry::forcedInputs(bool const considerEnabledOnly) const
{
  std::vector<tcds::definitions::TTS_SOURCE> res;

  for (unsigned int channel = tcds::definitions::TTS_SOURCE_MIN;
       channel <= tcds::definitions::TTS_SOURCE_MAX;
       ++channel)
    {
      tcds::definitions::TTS_SOURCE const tmp =
        static_cast<tcds::definitions::TTS_SOURCE>(channel);
      if (isEnabled(tmp) || !considerEnabledOnly)
        {
          if (isForced(tmp))
            {
              res.push_back(tmp);
            }
        }
    }

  return res;
}

std::vector<tcds::definitions::TTS_SOURCE>
tcds::pi::TTSLogEntry::unalignedInputs(bool const considerEnabledOnly) const
{
  std::vector<tcds::definitions::TTS_SOURCE> res;

  for (unsigned int channel = tcds::definitions::TTS_SOURCE_MIN;
       channel <= tcds::definitions::TTS_SOURCE_MAX;
       ++channel)
    {
      tcds::definitions::TTS_SOURCE const tmp =
        static_cast<tcds::definitions::TTS_SOURCE>(channel);
      if (isEnabled(tmp) || !considerEnabledOnly)
        {
          if (!isAligned(tmp))
            {
              res.push_back(tmp);
            }
        }
    }

  return res;
}

bool
tcds::pi::TTSLogEntry::isBitSet(size_t const bitNum) const
{
  std::vector<bool> tmpVec = getBit(bitNum);
  uint32_t tmp = vecBoolToUInt<uint32_t>(tmpVec);
  // DEBUG DEBUG DEBUG
  assert(tmpVec.size() == 1);
  // DEBUG DEBUG DEBUG end
  return (tmp != 0);
}

std::vector<bool>
tcds::pi::TTSLogEntry::getBit(size_t const bitNum) const
{
  return getBits(bitNum, bitNum);
}

std::vector<bool>
tcds::pi::TTSLogEntry::getBits(size_t const bitNumLo,
                               size_t const bitNumHi) const
{
  // ASSERT ASSERT ASSERT
  assert (bitNumHi >= bitNumLo);
  // ASSERT ASSERT ASSERT end
  size_t tmpLo = bitNumLo;
  size_t tmpHi = bitNumHi;
  if (tmpHi == 0)
    {
      tmpHi = tmpLo;
    }
  std::vector<bool>::const_iterator bitLo = data_.begin() + tmpLo;
  std::vector<bool>::const_iterator bitHi = data_.begin() + tmpHi + 1;
  std::vector<bool> res(bitLo, bitHi);
  return res;
}
