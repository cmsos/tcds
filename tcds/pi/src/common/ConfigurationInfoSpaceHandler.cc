#include "tcds/pi/ConfigurationInfoSpaceHandler.h"

#include <stdint.h>
#include <vector>

#include "toolbox/string.h"

#include "tcds/hwutilstca/SFPMonitoringConfigHelpers.h"
#include "tcds/pi/Definitions.h"
#include "tcds/pi/Utils.h"
#include "tcds/utils/Definitions.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/Utils.h"

tcds::pi::ConfigurationInfoSpaceHandler::ConfigurationInfoSpaceHandler(xdaq::Application& xdaqApp) :
  tcds::hwutilstca::ConfigurationInfoSpaceHandlerTCA(xdaqApp)
{
  // FED-id mappings for the ten optical FED-TTS inputs.
  for (unsigned int fedNum = 1; fedNum <= tcds::definitions::kNumFEDs; ++fedNum)
    {
      createUInt32(toolbox::toString("fedIdInput%d", fedNum),
                   tcds::definitions::kImpossibleFEDId,
                   "",
                   tcds::utils::InfoSpaceItem::NOUPDATE,
                   true);
    }

  //----------

  // There are several approaches to configuring the PI, and the
  // choice is made based on this parameter.
  createUInt32("configurationMode",
               0,
               "",
               tcds::utils::InfoSpaceItem::NOUPDATE,
               true);

  //----------

  // Some of the above configuration approaches require a
  // 'fedEnableMask' (in order to receive the necessary information
  // from RunControl).
  createString("fedEnableMask",
               "",
               "",
               tcds::utils::InfoSpaceItem::NOUPDATE,
               true);

  //----------

  // The 'mixed' (i.e., FMM + AMC13) configuration modes need to know
  // which FEDs are connected to the PI via the FMM.
  createUInt32Vec("fmmFEDs",
                  std::vector<uint32_t>(),
                  "",
                  tcds::utils::InfoSpaceItem::NOUPDATE,
                  true);

  //----------

  // The maximum size of the TTC spy history to keep 'live' in the
  // application (i.e., accessible via the HyperDAQ interface).
  createUInt32("ttcSpySizeLive",
               100,
               "",
               tcds::utils::InfoSpaceItem::NOUPDATE,
               true);

  //----------

  // The maximum size of the TTS history to keep 'live' in the
  // application (i.e., accessible via the HyperDAQ interface).
  // NOTE: This does not affect the TTS history as written to disk.
  createUInt32("ttsHistSizeLive",
               100,
               "",
               tcds::utils::InfoSpaceItem::NOUPDATE,
               true);

  // The maximum size of the TTS history to keep per TTS log file.
  // NOTE: This does not affet the 'live' TTS history shown in the
  // HyperDAQ interface.
  createUInt32("ttsHistSizeFile",
               100000,
               "",
               tcds::utils::InfoSpaceItem::NOUPDATE,
               true);

  // The base path where to store the TTS log files.
  createString("ttsHistPath",
               "/var/log/tcds/pi/tts/",
               "",
               tcds::utils::InfoSpaceItem::NOUPDATE,
               true);

  //----------

  // NOTE: The following two parameters are used internally, and set
  // by parameters to the SOAP 'Configure' command. Hence they are not
  // explicitly monitored.

  // There is a choice to be made: listen to the primary, or to the
  // secondary LPM input.
  createBool("usePrimaryTCDS",
             true,
             "",
             tcds::utils::InfoSpaceItem::NOUPDATE,
             true);

  // A flag to tell is whether or not to reset the TTC PLL at the
  // start of Configure.
  createBool("skipPLLReset",
             false,
             "",
             tcds::utils::InfoSpaceItem::NOUPDATE,
             true);

  //----------

  // SFP monitoring parameters.
  tcds::hwutilstca::SFPMonitoringConfigCreateItems(this);
}

tcds::pi::ConfigurationInfoSpaceHandler::~ConfigurationInfoSpaceHandler()
{
}

void
tcds::pi::ConfigurationInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  tcds::hwutilstca::ConfigurationInfoSpaceHandlerTCA::registerItemSetsWithMonitor(monitor);

  // Application configuration.
  std::string itemSetName = "Application configuration";
  monitor.addItem(itemSetName,
                  "configurationMode",
                  "PIController configuration mode",
                  this);
  monitor.addItem(itemSetName,
                  "fmmFEDs",
                  "FEDs connected via the FMM",
                  this);
  monitor.addItem(itemSetName,
                  "ttcSpySizeLive",
                  "Max. size of the 'live' TTC spy data",
                  this);
  monitor.addItem(itemSetName,
                  "ttsHistSizeLive",
                  "Max. size of the 'live' TTS history",
                  this);
  monitor.addItem(itemSetName,
                  "ttsHistSizeFile",
                  "Max. size of a single file of TTS history",
                  this);
  monitor.addItem(itemSetName,
                  "ttsHistPath",
                  "Storage path for TTS history files",
                  this);

  // Run configuration (from RCMS).
  itemSetName = "Run configuration";
  monitor.addItem(itemSetName,
                  "fedEnableMask",
                  "FED vector",
                  this);

  //----------

  // SFP monitoring parameters.
  tcds::hwutilstca::SFPMonitoringRegisterItemsWithMonitor(this, monitor);
}

std::string
tcds::pi::ConfigurationInfoSpaceHandler::formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const
{
  // Specialized formatting for the configuration mode enum.
  std::string res = tcds::utils::escapeAsJSONString(tcds::utils::InfoSpaceHandler::kInvalidItemString);
  if (item->isValid())
    {
      std::string name = item->name();
      if (name == "usePrimaryTCDS")
        {
          bool const usePrimaryTCDS = getBool(name);
          std::string tmpStr = "primary system";
          if (!usePrimaryTCDS)
            {
              tmpStr = "secondary system";
            }
          res = tcds::utils::escapeAsJSONString(tmpStr);
        }
      else if (name == "configurationMode")
        {
          uint32_t const value = getUInt32(name);
          tcds::definitions::PICONTROLLER_CONFIG_MODE const valueEnum =
            static_cast<tcds::definitions::PICONTROLLER_CONFIG_MODE>(value);
          res = tcds::utils::escapeAsJSONString(tcds::pi::configModeToString(valueEnum));
        }
      else if (name == "fedEnableMask")
        {
          // This thing can become annoyingly long. The idea here is
          // to insert some zero-width spaces in order to make it
          // break into pieces when formatted in the web interface.
          // NOTE: Not very efficient/pretty, but it works.
          std::string const valRaw = getString(name);
          std::string const valNew = tcds::utils::chunkifyFEDVector(valRaw);
          res = tcds::utils::escapeAsJSONString(valNew);
        }
      else
        {
          // For everything else simply call the generic formatter.
          res = InfoSpaceHandler::formatItem(item);
        }
    }
  return res;
}
