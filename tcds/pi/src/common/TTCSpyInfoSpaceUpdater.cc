#include "tcds/pi/TTCSpyInfoSpaceUpdater.h"

#include <string>

#include "tcds/pi/PIDAQLoop.h"
#include "tcds/pi/TCADevicePI.h"
#include "tcds/pi/TTCSpyContents.h"
#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/InfoSpaceItem.h"

tcds::pi::TTCSpyInfoSpaceUpdater::TTCSpyInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                                         TCADevicePI const& hw,
                                                         tcds::pi::PIDAQLoop const& piDAQLoop) :
  tcds::utils::HwInfoSpaceUpdaterBase(xdaqApp, hw),
  piDAQLoop_(piDAQLoop)
{
}

tcds::pi::TTCSpyInfoSpaceUpdater::~TTCSpyInfoSpaceUpdater()
{
}

bool
tcds::pi::TTCSpyInfoSpaceUpdater::updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                                      tcds::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  bool updated = false;
  tcds::pi::TCADevicePI const& hw = getHw();
  if (hw.isHwConnected())
    {
      std::string const name = item.name();
      tcds::utils::InfoSpaceItem::UpdateType const updateType = item.updateType();
      if (updateType == tcds::utils::InfoSpaceItem::PROCESS)
        {
          // The 'PROCESS' update type means that there is something
          // special to the variable. Figure out what to do based on
          // the variable name.
          if (name == "ttcspy_log")
            {
              std::string const newVal = piDAQLoop_.ttcSnapshot().getJSONString();
              infoSpaceHandler->setString(name, newVal);
              updated = true;
            }
      }
      if (!updated)
        {
          updated = tcds::utils::HwInfoSpaceUpdaterBase::updateInfoSpaceItem(item, infoSpaceHandler);
        }
    }

  if (updated)
    {
      item.setValid();
    }
  else
    {
      item.setInvalid();
    }

  return updated;
}

tcds::pi::TCADevicePI const&
tcds::pi::TTCSpyInfoSpaceUpdater::getHw() const
{
  return dynamic_cast<tcds::pi::TCADevicePI const&>(tcds::utils::HwInfoSpaceUpdaterBase::getHw());
}
