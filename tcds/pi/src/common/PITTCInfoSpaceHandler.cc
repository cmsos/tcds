#include "tcds/pi/PITTCInfoSpaceHandler.h"

#include <cassert>
#include <stdint.h>

#include "tcds/pi/Definitions.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/Utils.h"
#include "tcds/utils/WebServer.h"

tcds::pi::PITTCInfoSpaceHandler::PITTCInfoSpaceHandler(xdaq::Application& xdaqApp,
                                                       tcds::utils::InfoSpaceUpdater* updater) :
  InfoSpaceHandler(xdaqApp, "tcds-picontroller-main", updater)
{
  // The LPM input source (primary or secondary system).
  createUInt32("lpm_input_select");

  //----------

  // Some internal synchronization settings to make the PI itself
  // aware of the details of the incoming TTC stream.
  createUInt32("sync.oc0_bcommand",
               0,
               "hex");
  createUInt32("sync.bc0_bcommand",
               0,
               "hex");
  createUInt32("sync.bc0_reset_val");
}

tcds::pi::PITTCInfoSpaceHandler::~PITTCInfoSpaceHandler()
{
}

void
tcds::pi::PITTCInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  // The LPM input source.
  monitor.newItemSet("itemset-inputselection");
  monitor.addItem("itemset-inputselection",
                  "lpm_input_select",
                  "LPM input source",
                  this);

  //----------

  // Some internal synchronization settings to make the PI itself
  // aware of the details of the incoming TTC stream.
  monitor.newItemSet("itemset-sync");
  monitor.addItem("itemset-sync",
                  "sync.oc0_bcommand",
                  "The TTC B-command to interpret as OC0",
                  this);
  monitor.addItem("itemset-sync",
                  "sync.bc0_bcommand",
                  "The TTC B-command to interpret as BC0",
                  this);
  monitor.addItem("itemset-sync",
                  "sync.bc0_reset_val",
                  "The expected BX of the BC0 B-command",
                  this);
}

void
tcds::pi::PITTCInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                               tcds::utils::Monitor& monitor,
                                                               std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "TTC" : forceTabName;

  //----------

  webServer.registerTab(tabName,
                        "TTC source selection and synchronization settings",
                        2);
  //----------

  webServer.registerTable("Source",
                          "TTC source selection",
                          monitor,
                          "itemset-inputselection",
                          tabName);
  //----------

  webServer.registerTable("Sync",
                          "Synchronization settings",
                          monitor,
                          "itemset-sync",
                          tabName);
}

std::string
tcds::pi::PITTCInfoSpaceHandler::formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const
{
  // Specialized formatting rules for our enums.
  std::string res = tcds::utils::escapeAsJSONString(tcds::utils::InfoSpaceHandler::kInvalidItemString);
  if (item->isValid())
    {
      std::string name = item->name();
      if (name == "lpm_input_select")
        {
          uint32_t val = getUInt32(name);
          switch (val)
            {
            case tcds::definitions::LPM_SOURCE_PRIMARY:
              res = "primary system";
              break;
            case tcds::definitions::LPM_SOURCE_SECONDARY:
              res = "secondary system";
              break;
            default:
              // ASSERT ASSERT ASSERT
              assert (false);
              // ASSERT ASSERT ASSERT end
              break;
            }
          res = tcds::utils::escapeAsJSONString(res);
        }
      else
        {
          // For everything else simply call the generic formatter.
          res = InfoSpaceHandler::formatItem(item);
        }
    }
  return res;
}
