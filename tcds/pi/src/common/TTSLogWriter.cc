#include "tcds/pi/TTSLogWriter.h"

#include <inttypes.h>
#include <map>

#include "toolbox/string.h"
#include "xdaq/Application.h"
#include "xdaq/ApplicationDescriptor.h"

#include "tcds/pi/TTSLogEntry.h"
#include "tcds/pi/TTSLogFile.h"
#include "tcds/pi/Utils.h"
#include "tcds/utils/ConfigurationInfoSpaceHandler.h"
#include "tcds/utils/Utils.h"

tcds::pi::TTSLogWriter::TTSLogWriter() :
  logFileSequenceNumber_(0)
{
  // Set reasonable defaults for all configuration parameters.
  setSoftwareVersion("unknown");
  setFirmwareVersion("unknown");

  setServiceName("unknown service");
  setRunNumber(0);

  std::map<tcds::definitions::TTS_SOURCE, std::string> ttsChannelLabels;
  for (unsigned int channel = tcds::definitions::TTS_CHANNEL_MIN;
       channel <= tcds::definitions::TTS_CHANNEL_MAX;
       ++channel)
    {
      tcds::definitions::TTS_SOURCE const tmp =
        static_cast<tcds::definitions::TTS_SOURCE>(channel);
      std::string const channelLabel = formatTTSChannelLabel(tmp);
      ttsChannelLabels[tmp] = channelLabel;
    }
  setTTSChannelLabels(ttsChannelLabels);

  setLogBasePath("/var/log/tcds/pi/tts/");
  setLogSize(1000);
}

tcds::pi::TTSLogWriter::~TTSLogWriter()
{
  closeLog();
}

void
tcds::pi::TTSLogWriter::updateConfiguration(std::string const& softwareVersion,
                                            std::string const& firmwareVersion,
                                            tcds::utils::ConfigurationInfoSpaceHandler const& cfgInfoSpace)
{
  setSoftwareVersion(softwareVersion);
  setFirmwareVersion(firmwareVersion);

  //----------

  setServiceName(cfgInfoSpace.getOwnerApplication().getApplicationDescriptor()->getAttribute("service"));
  setRunNumber(cfgInfoSpace.getUInt32("runNumber"));

  // Replace the default TTS channel labels with more descriptive ones
  // based on the actual configuration.
  std::map<tcds::definitions::TTS_SOURCE, std::string> ttsChannelLabels;
  for (unsigned int channel = tcds::definitions::TTS_CHANNEL_MIN;
       channel <= tcds::definitions::TTS_CHANNEL_MAX;
       ++channel)
    {
      tcds::definitions::TTS_SOURCE const tmp =
        static_cast<tcds::definitions::TTS_SOURCE>(channel);
      std::string const channelLabel = tcds::pi::formatTTSChannelLabel(tmp, cfgInfoSpace);
      ttsChannelLabels[tmp] = channelLabel;
    }
  setTTSChannelLabels(ttsChannelLabels);

  setLogBasePath(cfgInfoSpace.getString("ttsHistPath"));
  setLogSize(cfgInfoSpace.getUInt32("ttsHistSizeFile"));
}

void
tcds::pi::TTSLogWriter::setTTSChannelLabels(std::map<tcds::definitions::TTS_SOURCE,
                                            std::string> const& ttsChannelLabels)
{
  ttsChannelLabels_ = ttsChannelLabels;
}

void
tcds::pi::TTSLogWriter::setRunNumber(uint32_t const runNumber)
{
  runNumber_ = runNumber;
  // Log files are numbered sequentially within each run. So for each
  // new run we reset the sequence number.
  logFileSequenceNumber_ = 0;
}

void
tcds::pi::TTSLogWriter::setSoftwareVersion(std::string const& softwareVersion)
{
  softwareVersion_ = softwareVersion;
}

void
tcds::pi::TTSLogWriter::setFirmwareVersion(std::string const& firmwareVersion)
{
  firmwareVersion_ = firmwareVersion;
}

void
tcds::pi::TTSLogWriter::setServiceName(std::string const& serviceName)
{
  serviceName_ = serviceName;
}

void
tcds::pi::TTSLogWriter::setLogBasePath(std::string const& basePath)
{
  logFilePath_ = basePath;
  if (!toolbox::endsWith(logFilePath_, "/"))
    {
      logFilePath_ += "/";
    }
}

void
tcds::pi::TTSLogWriter::setLogSize(uint32_t const& ttsLogSize)
{
  ttsLogSize_ = ttsLogSize;
}

void
tcds::pi::TTSLogWriter::openLog(bool const isContinuation)
{
 std::string const logFileName = toolbox::toString("pi_tts_log_%s_run%" PRIu32 "_file%u_%s.txt",
                                                    serviceName_.c_str(),
                                                    runNumber_,
                                                    logFileSequenceNumber_,
                                                    tcds::utils::getTimestamp(true, true).c_str());
  logFileP_ = std::unique_ptr<tcds::pi::TTSLogFile>(new tcds::pi::TTSLogFile(softwareVersion_,
                                                                             firmwareVersion_,
                                                                             serviceName_,
                                                                             runNumber_,
                                                                             ttsChannelLabels_,
                                                                             logFilePath_,
                                                                             logFileName,
                                                                             isContinuation));
}

void
tcds::pi::TTSLogWriter::closeLog(bool const isToBeContinued)
{
  if (logFileP_.get())
    {
      logFileP_->closeLog(isToBeContinued);
      logFileP_.reset();
    }
}

void
tcds::pi::TTSLogWriter::addEntry(tcds::pi::TTSLogEntry const& logEntry)
{
  if (logFileP_.get())
    {
      logFileP_->addEntry(logEntry);
      if (shouldOpenNewFile())
        {
          closeLog(true);
          ++logFileSequenceNumber_;
          openLog(true);
        }
    }
  // BUG BUG BUG
  // Would be useful to throw an exception here.
  // BUG BUG BUG end
}

bool
tcds::pi::TTSLogWriter::shouldOpenNewFile() const
{
  int const maxNumEntries = ttsLogSize_;
  return (logFileP_->numEntries() >= maxNumEntries);
}
