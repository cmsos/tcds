#include "tcds/pi/TCADevicePI.h"

#include <algorithm>
#include <cassert>
#include <memory>
#include <unistd.h>

#include "toolbox/string.h"

#include "tcds/hwlayertca/BootstrapHelper.h"
#include "tcds/hwlayertca/TCACarrierBase.h"
#include "tcds/hwlayertca/TCACarrierFC7.h"
#include "tcds/pi/Definitions.h"
#include "tcds/pi/TTSLogEntry.h"
#include "tcds/pi/TTCSpyEntryRaw.h"

tcds::pi::TCADevicePI::TCADevicePI() :
  TCADeviceBase(std::unique_ptr<tcds::hwlayertca::TCACarrierBase>(new tcds::hwlayertca::TCACarrierFC7(hwDevice_))),
  ttcLogger_(*this, "ttc_spy", TTCSpyEntryRaw::kNumWordsPerLogEntry),
  ttsLogger_(*this, "tts_log", TTSLogEntry::kNumWordsPerLogEntry)
{
}

tcds::pi::TCADevicePI::~TCADevicePI()
{
}

std::string
tcds::pi::TCADevicePI::regNamePrefixImpl() const
{
  // All the PI registers start with 'pi.'.
  return "pi.";
}

bool
tcds::pi::TCADevicePI::bootstrapDoneImpl() const
{
  tcds::hwlayertca::BootstrapHelper h(*this);
  return h.bootstrapDone();
}

void
tcds::pi::TCADevicePI::runBootstrapImpl() const
{
  tcds::hwlayertca::BootstrapHelper h(*this);
  return h.runBootstrap();
}

tcds::hwlayer::DeviceBase::RegContentsVec
tcds::pi::TCADevicePI::readHardwareConfigurationImpl(tcds::hwlayer::RegisterInfo::RegInfoVec const& regInfos) const
{
  // This simply filters out the TTC spy and TTS log memories.
  std::string const regName("log_mem");
  tcds::hwlayer::RegisterInfo::RegInfoVec tmp(regInfos);
  tmp.erase(std::remove_if(tmp.begin(),
                           tmp.end(),
                           tcds::hwlayer::RegisterInfo::RegInfoNameMatches(regName)),
            tmp.end());

  tcds::hwlayer::DeviceBase::RegContentsVec res =
    tcds::hwlayertca::TCADeviceBase::readHardwareConfigurationImpl(tmp);

  return res;
}

void
tcds::pi::TCADevicePI::selectLPMSource(tcds::definitions::LPM_SOURCE const lpmSource) const
{
  uint32_t val = 0x0;
  switch (lpmSource)
    {
    case tcds::definitions::LPM_SOURCE_PRIMARY:
      val = 0x0;
      break;
    case tcds::definitions::LPM_SOURCE_SECONDARY:
      val = 0x1;
      break;
    default:
      // ASSERT ASSERT ASSERT
      assert (false);
      // ASSERT ASSERT ASSERT end
      break;
    }
  writeRegister("lpm_input_select", val);
}

void
tcds::pi::TCADevicePI::enableTTSInput(tcds::definitions::TTS_SOURCE const ttsSource) const
{
  std::string sourceName = "unknown";
  switch (ttsSource)
    {
    case tcds::definitions::TTS_SOURCE_FED1:
    case tcds::definitions::TTS_SOURCE_FED2:
    case tcds::definitions::TTS_SOURCE_FED3:
    case tcds::definitions::TTS_SOURCE_FED4:
    case tcds::definitions::TTS_SOURCE_FED5:
    case tcds::definitions::TTS_SOURCE_FED6:
    case tcds::definitions::TTS_SOURCE_FED7:
    case tcds::definitions::TTS_SOURCE_FED8:
    case tcds::definitions::TTS_SOURCE_FED9:
    case tcds::definitions::TTS_SOURCE_FED10:
      sourceName = toolbox::toString("fed%d", ttsSource);
      break;
    case tcds::definitions::TTS_SOURCE_RJ45:
      sourceName = "rj45";
      break;
    default:
      // ASSERT ASSERT ASSERT
      assert (false);
      // ASSERT ASSERT ASSERT end
      break;
    }
  writeRegister(toolbox::toString("tts_source_enable.%s", sourceName.c_str()), 0x1);
}

void
tcds::pi::TCADevicePI::disableTTSInput(tcds::definitions::TTS_SOURCE const ttsSource) const
{
  std::string sourceName = "unknown";
  switch (ttsSource)
    {
    case tcds::definitions::TTS_SOURCE_FED1:
    case tcds::definitions::TTS_SOURCE_FED2:
    case tcds::definitions::TTS_SOURCE_FED3:
    case tcds::definitions::TTS_SOURCE_FED4:
    case tcds::definitions::TTS_SOURCE_FED5:
    case tcds::definitions::TTS_SOURCE_FED6:
    case tcds::definitions::TTS_SOURCE_FED7:
    case tcds::definitions::TTS_SOURCE_FED8:
    case tcds::definitions::TTS_SOURCE_FED9:
    case tcds::definitions::TTS_SOURCE_FED10:
      sourceName = toolbox::toString("fed%d", ttsSource);
      break;
    case tcds::definitions::TTS_SOURCE_RJ45:
      sourceName = "rj45";
      break;
    default:
      // ASSERT ASSERT ASSERT
      assert (false);
      // ASSERT ASSERT ASSERT end
      break;
    }
  writeRegister(toolbox::toString("tts_source_enable.%s", sourceName.c_str()), 0x0);
}

void
tcds::pi::TCADevicePI::disableTTSInputs() const
{
  // Disable all TTS inputs.
  for (unsigned int i = tcds::definitions::TTS_SOURCE_MIN;
       i <= tcds::definitions::TTS_SOURCE_MAX;
       ++i)
    {
      disableTTSInput(static_cast<tcds::definitions::TTS_SOURCE>(i));
    }
  disableTTSInput(tcds::definitions::TTS_SOURCE_RJ45);
}

void
tcds::pi::TCADevicePI::resetOrbitCounter() const
{
  // A software reset of the orbit counter. Mimics an OC0.
  writeRegister("sync.sw_oc0", 0x0);
  writeRegister("sync.sw_oc0", 0x1);
  writeRegister("sync.sw_oc0", 0x0);
}

void
tcds::pi::TCADevicePI::resetOrbitChecker() const
{
  // Reset the orbit length checker.
  // NOTE: This stops the checker. The checker will start
  // automatically upon reception of the first BC0 after the reset has
  // been release.
  writeRegister("orbit_checker.control.reset", 0x0);
  writeRegister("orbit_checker.control.reset", 0x1);
  writeRegister("orbit_checker.control.reset", 0x0);
}

bool
tcds::pi::TCADevicePI::isTTCStreamAligned() const
{
  // Check if the incoming TTC stream from the iCI (which comes in
  // over the optical link from the LPM) is properly aligned.
  uint32_t const tmp = readRegister("ttc_decoder.status.ttc_stream_aligned");
  bool isAligned = (tmp != 0x0);

  if (isAligned)
    {
      // Now double-check that it is not actually briefly un-aligning
      // every now and then.
      uint32_t const tmp0 = readRegister("ttc_decoder.status.ttc_stream_aligned");
      ::sleep(1);
      uint32_t const tmp1 = readRegister("ttc_decoder.status.ttc_stream_aligned");
      isAligned = (tmp1 == tmp0);
    }

  return isAligned;
}

void
tcds::pi::TCADevicePI::enableTTCDecoder() const
{
  writeRegister("ttc_decoder.control.enable", 0x1);
}

void
tcds::pi::TCADevicePI::disableTTCDecoder() const
{
  writeRegister("ttc_decoder.control.enable", 0x0);
}

bool
tcds::pi::TCADevicePI::isTTCDecoderEnabled() const
{
  uint32_t const tmp = readRegister("ttc_decoder.control.enable");
  return (tmp != 0x0);
}

void
tcds::pi::TCADevicePI::enableTTCSpy() const
{
  ttcLogger_.enableLogging();
}

void
tcds::pi::TCADevicePI::disableTTCSpy() const
{
  ttcLogger_.disableLogging();
}

void
tcds::pi::TCADevicePI::resetTTCSpy() const
{
  ttcLogger_.resetLogging();
}

bool
tcds::pi::TCADevicePI::isTTCSpyEnabled() const
{
  return ttcLogger_.isLoggingEnabled();
}

bool
tcds::pi::TCADevicePI::isTTCSpyFull() const
{
  return ttcLogger_.isLogFull();
}

uint32_t
tcds::pi::TCADevicePI::maxNumTTCSpyEntries() const
{
  return ttcLogger_.maxNumLogEntries();
}

uint32_t
tcds::pi::TCADevicePI::numTTCSpyEntries() const
{
  return ttcLogger_.numLogEntries();
}

uint32_t
tcds::pi::TCADevicePI::readTTCSpyAddressPointer() const
{
  uint32_t const res = ttcLogger_.readLogAddressPointer();
  return res;
}

std::vector<uint32_t>
tcds::pi::TCADevicePI::readTTCSpyRaw() const
{
  return ttcLogger_.readLogRaw();
}

std::vector<uint32_t>
tcds::pi::TCADevicePI::readTTCSpyRaw(uint32_t const from) const
{
  return ttcLogger_.readLogRaw(from);
}

std::vector<uint32_t>
tcds::pi::TCADevicePI::readTTCSpyRaw(uint32_t const first, uint32_t const last) const
{
  return ttcLogger_.readLogRaw(first, last);
}

void
tcds::pi::TCADevicePI::setTTCSpyBufferMode(tcds::pi::TTXLogHelper::LOGGING_BUFFER_MODE const val) const
{
  ttcLogger_.setLogBufferMode(val);
}

void
tcds::pi::TCADevicePI::setTTCSpyBufferSizeToMax() const
{
  ttcLogger_.setLogBufferSizeToMax();
}

void
tcds::pi::TCADevicePI::setTTCSpyLoggingLogic(tcds::pi::TCADevicePI::TTC_LOGGING_LOGIC const val) const
{
  writeRegister("ttc_spy.control.trigger_combination_operator", val);
}

void
tcds::pi::TCADevicePI::setTTCSpyLoggingMode(tcds::pi::TCADevicePI::TTC_LOGGING_MODE const val) const
{
  writeRegister("ttc_spy.control.logging_mode", val);
}

void
tcds::pi::TCADevicePI::setTTCSpyTriggerMask(bool const brcBC0,
                                            bool const brcEC0,
                                            uint8_t const brcVal0,
                                            uint8_t const brcVal1,
                                            uint8_t const brcVal2,
                                            uint8_t const brcVal3,
                                            uint8_t const brcVal4,
                                            uint8_t const brcVal5,
                                            uint8_t const brcDDDD,
                                            uint8_t brcTT,
                                            bool const brcDDDDAll,
                                            bool const brcTTAll,
                                            bool const brcAll,
                                            bool const addAll,
                                            bool const l1a,
                                            bool const brcZeroData,
                                            bool const adrZeroData,
                                            bool const errCom) const
{
  // NOTE: Since there may be (and is, in most firmware versions) more
  // in this register than is configured via this method, first zero
  // the whole thing out.
  writeRegister("ttc_spy.trigger_mask", 0x0);

  // Then actually fill in the desired values.
  writeRegister("ttc_spy.trigger_mask.brc_bc0", brcBC0);
  writeRegister("ttc_spy.trigger_mask.brc_ec0", brcEC0);
  writeRegister("ttc_spy.trigger_mask.brc_val0", brcVal0);
  writeRegister("ttc_spy.trigger_mask.brc_val1", brcVal1);
  writeRegister("ttc_spy.trigger_mask.brc_val2", brcVal2);
  writeRegister("ttc_spy.trigger_mask.brc_val3", brcVal3);
  writeRegister("ttc_spy.trigger_mask.brc_val4", brcVal4);
  writeRegister("ttc_spy.trigger_mask.brc_val5", brcVal5);
  writeRegister("ttc_spy.trigger_mask.brc_dddd", brcDDDD);
  writeRegister("ttc_spy.trigger_mask.brc_tt", brcTT);
  writeRegister("ttc_spy.trigger_mask.brc_dddd_all", brcDDDDAll);
  writeRegister("ttc_spy.trigger_mask.brc_tt_all", brcTTAll);
  writeRegister("ttc_spy.trigger_mask.brc_all", brcAll);
  writeRegister("ttc_spy.trigger_mask.add_all", addAll);
  writeRegister("ttc_spy.trigger_mask.l1a", l1a);
  writeRegister("ttc_spy.trigger_mask.brc_zero_data", brcZeroData);
  writeRegister("ttc_spy.trigger_mask.adr_zero_data", adrZeroData);
  writeRegister("ttc_spy.trigger_mask.err_com", errCom);
}

void
tcds::pi::TCADevicePI::setTTSLogBufferMode(tcds::pi::TTXLogHelper::LOGGING_BUFFER_MODE const val) const
{
  ttsLogger_.setLogBufferMode(val);
}

void
tcds::pi::TCADevicePI::setTTSLogBufferSizeToMax() const
{
  ttsLogger_.setLogBufferSizeToMax();
}

void
tcds::pi::TCADevicePI::enableTTSLog() const
{
  ttsLogger_.enableLogging();
}

void
tcds::pi::TCADevicePI::disableTTSLog() const
{
  ttsLogger_.disableLogging();
}

uint32_t
tcds::pi::TCADevicePI::resetTTSLog() const
{
  return ttsLogger_.resetLogging();
}

bool
tcds::pi::TCADevicePI::isTTSLogEnabled() const
{
  return ttsLogger_.isLoggingEnabled();
}

bool
tcds::pi::TCADevicePI::isTTSLogFull() const
{
  return ttsLogger_.isLogFull();
}

uint32_t
tcds::pi::TCADevicePI::maxNumTTSLogEntries() const
{
  return ttsLogger_.maxNumLogEntries();
}

uint32_t
tcds::pi::TCADevicePI::numTTSLogEntries() const
{
  return ttsLogger_.numLogEntries();
}

uint32_t
tcds::pi::TCADevicePI::readTTSLogAddressPointer() const
{
  return ttsLogger_.readLogAddressPointer();
}

std::vector<uint32_t>
tcds::pi::TCADevicePI::readTTSLogRaw() const
{
  return ttsLogger_.readLogRaw();
}

std::vector<uint32_t>
tcds::pi::TCADevicePI::readTTSLogRaw(uint32_t const from) const
{
  return ttsLogger_.readLogRaw(from);
}

std::vector<uint32_t>
tcds::pi::TCADevicePI::readTTSLogRaw(uint32_t const first, uint32_t const last) const
{
  return ttsLogger_.readLogRaw(first, last);
}
