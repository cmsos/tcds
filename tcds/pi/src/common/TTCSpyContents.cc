#include "tcds/pi/TTCSpyContents.h"

#include <iomanip>
#include <sstream>
#include <utility>

#include "toolbox/string.h"

#include "tcds/utils/ConfigurationInfoSpaceHandler.h"
#include "tcds/utils/Definitions.h"
#include "tcds/utils/Utils.h"

// NOTE: The following assumes that rhs does not occur before lhs.
std::pair<uint64_t, uint16_t>
tcds::pi::deltaEntries(tcds::pi::TTCSpyEntry const& lhs, tcds::pi::TTCSpyEntry const& rhs)
{
  uint64_t const orbitLo = lhs.orbitNumber();
  uint64_t const orbitHi = rhs.orbitNumber();
  uint16_t const bxLo = lhs.bxNumber();
  uint16_t const bxHi = rhs.bxNumber();

  uint16_t deltaOrbit;
  uint16_t deltaBX;
  if (bxHi < bxLo)
    {
      deltaOrbit = orbitHi - orbitLo - 1;
      deltaBX = bxHi + (3564 - bxLo);
    }
  else
    {
      deltaOrbit = orbitHi - orbitLo;
      deltaBX = bxHi - bxLo;
    }

  std::pair<uint64_t, uint16_t> const res = std::make_pair(deltaOrbit, deltaBX);
  return res;
}

tcds::pi::TTCSpyContents::TTCSpyContents()
{
  // Set reasonable defaults for all configuration parameters.
  setHistSize(1000);
}

tcds::pi::TTCSpyContents::~TTCSpyContents()
{
}

void
tcds::pi::TTCSpyContents::updateConfiguration(tcds::utils::ConfigurationInfoSpaceHandler const& cfgInfoSpace)
{
  setHistSize(cfgInfoSpace.getUInt32("ttcSpySizeLive"));
}

void
tcds::pi::TTCSpyContents::setHistSize(size_t const histSize)
{
  histSize_ = histSize;
  trim();
}

void
tcds::pi::TTCSpyContents::addEntry(tcds::pi::TTCSpyEntry const& logLine)
{
  entries_.push_back(logLine);
  trim();
}

void
tcds::pi::TTCSpyContents::clear()
{
  entries_.clear();
}

std::string
tcds::pi::TTCSpyContents::getJSONString() const
{
  std::string const colIdOrbit = "orbit";
  std::string const colIdBX = "bx";
  std::string const colIdDelta = "delta";
  std::string const colIdType = "type";
  std::string const colIdFlags = "flags";
  std::string const colIdBCommandData = "data";
  std::string const colIdBCommandAddress = "address";

  //----------

  // Column names/definitions.
  std::vector<std::pair<std::string, std::string> > colNames;

  // The orbit number of this TTC entry.
  colNames.push_back(std::make_pair(colIdOrbit, "Orbit"));

  // The BX number of the this TTC entry.
  colNames.push_back(std::make_pair(colIdBX, "BX"));

  // The difference (in orbits and BXs) with respect to the previous
  // entry.
  colNames.push_back(std::make_pair(colIdDelta, "Delta"));

  // A string describing the type of TTC signal for this entry.
  colNames.push_back(std::make_pair(colIdType, "Type"));

  // Any flags that may be set.
  colNames.push_back(std::make_pair(colIdFlags, "Flags"));

  // The B-command data (for B-commands).
  colNames.push_back(std::make_pair(colIdBCommandData, "Data"));

  // The address, sub-address and internal/external addressing flag
  // (for long B-commands).
  colNames.push_back(std::make_pair(colIdBCommandAddress, "Address"));

  //----------

  std::stringstream res;

  // Start.
  res << "{";

  //----------

  // Column names.
  res << tcds::utils::escapeAsJSONString("columns") << ": {";

  for (std::vector<std::pair<std::string, std::string> >::const_iterator it = colNames.begin();
       it != colNames.end();
       ++it)
    {
      res << tcds::utils::escapeAsJSONString(it->first)
          << ": "
          << tcds::utils::escapeAsJSONString(it->second);
      if (it != (colNames.end() - 1))
        {
          res << ", ";
        }
    }

  res << "}";

  //----------

  res << ", ";

  //----------

  // Column data.
  res << tcds::utils::escapeAsJSONString("data") << ": [";

  //----------

  for (std::deque<TTCSpyEntry>::const_iterator it = entries_.begin();
       it != entries_.end();
       ++it)
    {
      res << "{";

      // The orbit number of this entry.
      res << tcds::utils::escapeAsJSONString(colIdOrbit)
          << ": "
          << tcds::utils::escapeAsJSONString(toolbox::toString("%lu", it->orbitNumber()));
      res << ", ";

      // The BX number of the this entry.
      res << tcds::utils::escapeAsJSONString(colIdBX)
          << ": "
          << tcds::utils::escapeAsJSONString(toolbox::toString("%u", it->bxNumber()));
      res << ", ";

      // The difference (in orbits and BXs) since the previous entry.
      std::string deltaStr = "-";
      if (it != entries_.begin())
        {
          std::pair<uint64_t, uint16_t> const delta = deltaEntries(*(it - 1), *it);
          deltaStr = toolbox::toString("%u:%u", delta.first, delta.second);
        }
      res << tcds::utils::escapeAsJSONString(colIdDelta)
          << ": "
          << tcds::utils::escapeAsJSONString(deltaStr);
      res << ", ";

      // A string describing the type of TTC signal for this entry.
      res << tcds::utils::escapeAsJSONString(colIdType)
          << ": "
          << tcds::utils::escapeAsJSONString(it->type());
      res << ", ";

      // Any flags that may be set.
      res << tcds::utils::escapeAsJSONString(colIdFlags)
          << ": "
          << tcds::utils::escapeAsJSONString(it->flags());
      res << ", ";

      // The B-command data (for B-commands).
      std::string dataStr = "";
      if (!it->isL1A())
        {
          uint8_t data = it->data();
          std::stringstream tmp;
          tmp << std::hex
              << std::setfill('0')
              << "0x"
              << std::setw(sizeof(data) * 2)
              << int(data);
          dataStr = tmp.str();
        }
      else
        {
          dataStr = "n/a";
        }
      res << tcds::utils::escapeAsJSONString(colIdBCommandData)
          << ": "
          << tcds::utils::escapeAsJSONString(dataStr);
      res << ", ";

      // The address, sub-address and internal/external addressing
      // flag (for long B-commands).
      std::stringstream addressStr;
      if (it->isBCommandLong())
        {
          uint16_t address = it->address();
          uint8_t subAddress = it->subAddress();
          addressStr << std::hex
                     << std::setfill('0')
                     << "0x"
                     << std::setw(sizeof(address) * 2)
                     << address
                     << "/"
                     << std::hex
                     << std::setfill('0')
                     << "0x"
                     << std::setw(sizeof(subAddress) * 2)
                     << int(subAddress);
          if (it->isAddressExternal())
            {
              addressStr << " external";
            }
          else
            {
              addressStr << " internal";
            }
        }
      else
        {
          addressStr << "n/a";
        }
      res << tcds::utils::escapeAsJSONString(colIdBCommandAddress)
          << ": "
          << tcds::utils::escapeAsJSONString(addressStr.str());

      res << "}";
      if (it != (entries_.end() - 1))
        {
          res << ", ";
        }
    }

  res << "]";

  //----------

  // Done.
  res << "}";

  return res.str();
}

size_t
tcds::pi::TTCSpyContents::numEntries() const
{
  return entries_.size();
}

void
tcds::pi::TTCSpyContents::trim()
{
  while (numEntries() > histSize_)
    {
      entries_.pop_front();
    }
}

std::string
tcds::pi::TTCSpyContents::formatTimestamp(uint64_t const timestamp) const
{
  unsigned int const numBXPerOrbit = tcds::definitions::kNumBXPerOrbit;
  unsigned int numOrbits = timestamp / numBXPerOrbit;
  unsigned int numBX = timestamp % numBXPerOrbit;
  std::stringstream res("");
  if (numOrbits > 0)
    {
      res << numOrbits << " orbit";
      if (numOrbits > 1)
        {
          res << "s";
        }
    }
  if (numBX > 0)
    {
      if (!res.str().empty())
        {
          res << " + ";
        }
      res << numBX << " BX";
    }
  if (res.str().empty())
    {
      res << "-";
    }
  return res.str();
}
