#include "tcds/pi/HwStatusInfoSpaceUpdater.h"

#include "tcds/pi/TCADevicePI.h"

tcds::pi::HwStatusInfoSpaceUpdater::HwStatusInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                                             tcds::pi::TCADevicePI const& hw) :
  tcds::hwutilstca::HwStatusInfoSpaceUpdaterTCA(xdaqApp, hw),
  hw_(hw)
{
}

tcds::pi::HwStatusInfoSpaceUpdater::~HwStatusInfoSpaceUpdater()
{
}

tcds::pi::TCADevicePI const&
tcds::pi::HwStatusInfoSpaceUpdater::getHw() const
{
  return hw_;
}
