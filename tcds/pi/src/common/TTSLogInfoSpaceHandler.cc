#include "tcds/pi/TTSLogInfoSpaceHandler.h"

#include <cassert>

#include "tcds/pi/WebTableTTSLog.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/Utils.h"
#include "tcds/utils/WebServer.h"

tcds::pi::TTSLogInfoSpaceHandler::TTSLogInfoSpaceHandler(xdaq::Application& xdaqApp,
                                                         tcds::utils::InfoSpaceUpdater* updater) :
  InfoSpaceHandler(xdaqApp, "tcds-ttslog", updater)
{
  // The TTS log data.
  createString("tts_log");
}

void
tcds::pi::TTSLogInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  // The TTS log data.
  monitor.newItemSet("itemset-ttslog");
  monitor.addItem("itemset-ttslog",
                  "tts_log",
                  "TTS log contents",
                  this);
}

void
tcds::pi::TTSLogInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                tcds::utils::Monitor& monitor,
                                                                std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "TTSLog" : forceTabName;

  webServer.registerTab(tabName,
                        "The recent TTS history.",
                        1);

  webServer.registerWebObject<WebTableTTSLog>("TTS log",
                                              "TTS log contents."
                                              " Please note that the TTS log files on disk"
                                              " contain additional information"
                                              " about the individual channel states.",
                                              monitor,
                                              "itemset-ttslog",
                                              tabName);
}

std::string
tcds::pi::TTSLogInfoSpaceHandler::formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const
{
  // Specialized formatting rules for the TTS logging settings enums.
  std::string res = tcds::utils::escapeAsJSONString(tcds::utils::InfoSpaceHandler::kInvalidItemString);
  if (item->isValid())
    {
      std::string name = item->name();
      if (name == "tts_log")
        {
          // Special in the sense that this is something that needs to
          // be interpreted as a proper JavaScript object, so let's
          // not add any more double quotes.
          res = getString(name);
        }
      else
        {
          // For everything else simply call the generic formatter.
          res = InfoSpaceHandler::formatItem(item);
        }
    }
  return res;
}
