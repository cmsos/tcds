#include "tcds/pi/PITTSAuxInfoSpaceHandler.h"

#include <stdint.h>

#include "toolbox/string.h"

#include "tcds/utils/Definitions.h"
// #include "tcds/utils/Monitor.h"
#include "tcds/utils/Utils.h"
// #include "tcds/utils/WebServer.h"
// #include "tcds/utils/WebTableTTS.h"
// #include "tcds/utils/XDAQAppBase.h"
#include "tcds/pi/Definitions.h"
#include "tcds/pi/Utils.h"

// namespace xdaq {
//   class Application;
// }

// namespace tcds {
//   namespace utils {
//     class ConfigurationInfoSpaceHandler;
//   }
// }

tcds::pi::PITTSAuxInfoSpaceHandler::PITTSAuxInfoSpaceHandler(xdaq::Application& xdaqApp,
                                                             std::string const& name,
                                                             tcds::utils::InfoSpaceUpdater* updater) :
  tcds::utils::InfoSpaceHandler(xdaqApp, name, updater)
{
}

tcds::pi::PITTSAuxInfoSpaceHandler::~PITTSAuxInfoSpaceHandler()
{
}

// tcds::utils::XDAQAppBase&
// tcds::pi::PITTSAuxInfoSpaceHandler::getOwnerApplication() const
// {
//   return static_cast<tcds::utils::XDAQAppBase&>(tcds::utils::InfoSpaceHandler::getOwnerApplication());
// }

std::string
tcds::pi::PITTSAuxInfoSpaceHandler::formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const
{
  // Some specialized formatting rules for enums.
  std::string res = tcds::utils::escapeAsJSONString(tcds::utils::InfoSpaceHandler::kInvalidItemString);
  if (item->isValid())
    {
      std::string const name = item->name();
      if (toolbox::endsWith(name, "tts_out_enable"))
        {
          uint32_t const value = getUInt32(name);
          tcds::definitions::PI_TTS_OUTPUT_MODE valueEnum =
            static_cast<tcds::definitions::PI_TTS_OUTPUT_MODE>(value);
          res = tcds::utils::escapeAsJSONString(ttsOutputModeToString(valueEnum));
        }
      else if (toolbox::startsWith(name, "tts_incoming_masked."))
        {
          // NOTE: These are 'special PI' TTS values (i.e., nine bits
          // wide). So they have a dedicated formatter.
          uint32_t const value = getUInt32(name);
          res = tcds::utils::escapeAsJSONString(tcds::utils::TTSStateToString(value, true));
        }
      else
        {
          // For all non-enums simply call the generic formatter.
          res = tcds::utils::InfoSpaceHandler::formatItem(item);
        }
    }
  return res;
}

// void
// tcds::pi::PITTSAuxInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
// {
//   // Over-all TTS enable flag and final-OR output value.
//   std::string itemSetName = "Top-level TTS";
//   monitor.newItemSet(itemSetName);
//   monitor.addItem(itemSetName,
//                   "tts_output_control.tts_out_enable",
//                   "TTS output mode",
//                   this,
//                   "TTS state output mode. The output state can either come from the TTS OR, or be forced to a fixed value.");
//   monitor.addItem(itemSetName,
//                   "tts_outgoing.tts_out",
//                   "TTS output value",
//                   this,
//                   "The actual TTS output state propagated to the LPM.");

//   //----------

//   // All individual TTS input values.
//   itemSetName = "TTS input values";
//   monitor.newItemSet(itemSetName);
//   for (std::vector<std::string>::const_iterator i = inputNames_.begin();
//        i != inputNames_.end();
//        ++i)
//     {
//       monitor.addItem(itemSetName,
//                       toolbox::toString("tts_incoming_masked.%s", i->c_str()),
//                       inputLabels_[*i],
//                       this);
//     }
// }

// void
// tcds::pi::PITTSAuxInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
//                                                                tcds::utils::Monitor& monitor,
//                                                                std::string const& forceTabName)
// {
//   std::string const tabName = forceTabName.empty() ? "TTS" : forceTabName;

//   webServer.registerTab(tabName,
//                         "TTS information",
//                         1);
//   //----------

//   // Top-level TTS configuration and state.
//   webServer.registerWebObject<tcds::utils::WebTableTTS>("Top-level TTS",
//                                                         "Over-all TTS information",
//                                                         monitor,
//                                                         "Top-level TTS",
//                                                         tabName,
//                                                         1);

//   //----------

//   // All individual FED TTS input values.
//   std::string name = "TTS input values";
//   webServer.registerWebObject<tcds::utils::WebTableTTS>(name,
//                                                         "",
//                                                         monitor,
//                                                         name,
//                                                         tabName);
// }
