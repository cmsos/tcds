#include "tcds/pi/TTXLogHelper.h"

// BUG BUG BUG
#include <iostream>
#include "toolbox/stacktrace.h"
// BUG BUG BUG end

#include "tcds/hwlayertca/TCADeviceBase.h"

tcds::pi::TTXLogHelper::TTXLogHelper(tcds::hwlayertca::TCADeviceBase const& hw,
                                     std::string const& regNamePrefix,
                                     size_t const numWordsPerEntry) :
  hw_(hw),
  regNamePrefix_(regNamePrefix),
  numWordsPerEntry_(numWordsPerEntry)
{
}

tcds::pi::TTXLogHelper::~TTXLogHelper()
{
}

void
tcds::pi::TTXLogHelper::setLogBufferMode(tcds::pi::TTXLogHelper::LOGGING_BUFFER_MODE const val) const
{
  hw_.writeRegister(regNamePrefix_ + ".control.logging_buffer_mode", val);
}

void
tcds::pi::TTXLogHelper::setLogBufferSizeToMax() const
{
  uint32_t const memSize = hw_.getBlockSize(regNamePrefix_ + ".log_mem");
  uint32_t const numEntries = (memSize / numWordsPerEntry_);
  hw_.writeRegister(regNamePrefix_ + ".control.max_num_entries", numEntries);
}

void
tcds::pi::TTXLogHelper::enableLogging() const
{
  hw_.writeRegister(regNamePrefix_ + ".control.enabled", 0x1);
}

void
tcds::pi::TTXLogHelper::disableLogging() const
{
  hw_.writeRegister(regNamePrefix_ + ".control.enabled", 0x0);
}

bool
tcds::pi::TTXLogHelper::isLoggingEnabled() const
{
  uint32_t const tmp = hw_.readRegister(regNamePrefix_ + ".control.enabled");
  return (tmp != 0x0);
}

uint32_t
tcds::pi::TTXLogHelper::resetLogging() const
{
  // NOTE: The actual reset is triggered by the 1 -> 0 transition, and
  // this reset updates a register containing the number of entries
  // missed during the time that the log was full. (At least: for the
  // TTS logger. For the TTC spy it simply does nothing extra.)

  // NOTE: This procedure is (of course) not completely free of race
  // conditions...

  hw_.writeRegister(regNamePrefix_ + ".control.reset", 0x0);
  hw_.writeRegister(regNamePrefix_ + ".control.reset", 0x1);
  hw_.writeRegister(regNamePrefix_ + ".control.reset", 0x0);

  // NOTE: The following only really does anything for the TTS
  // logger. For the TTC spy this register only exists in the address
  // table and not in the firmware.
  uint32_t const entriesMissed = hw_.readRegister(regNamePrefix_ + ".status.num_entries_missed");
  return entriesMissed;
}

uint32_t
tcds::pi::TTXLogHelper::maxNumLogEntries() const
{
  // NOTE: This is the _configured_ maximum number of entries. This
  // has to be no bigger (of course) than the actual size of the
  // history logging memory.
  return hw_.readRegister(regNamePrefix_ + ".control.max_num_entries");
}

uint32_t
tcds::pi::TTXLogHelper::numLogEntries() const
{
  // This returns the number of entries currently in the logger
  // memory.

  // NOTE: This is a little tricky. As long as the memory is not full,
  // the address pointer indicates the next entry to be written. Since
  // the first entry has address zero, the address pointer indicates
  // the number of entries currently in the register. Once the memory
  // is full, though, the address pointer will not move forward (and
  // roll over) but still stick to max_entries. And of course this
  // becomes a bit more complicated when 'manually' reducing the
  // maximum number of log entries.

  // The above of course assumes the logger is in single-shot mode...

  uint32_t numEntries;
  if (isLogFull())
    {
      numEntries = maxNumLogEntries();
    }
  else
    {
      numEntries = readLogAddressPointer();
    }
  return numEntries;
}

bool
tcds::pi::TTXLogHelper::isLogFull() const
{
  uint32_t const tmp = hw_.readRegister(regNamePrefix_ + ".status.log_full");
  return (tmp != 0x0);
}

uint32_t
tcds::pi::TTXLogHelper::readLogAddressPointer() const
{
  return hw_.readRegister(regNamePrefix_ + ".status.log_address_pointer");
}

std::vector<uint32_t>
tcds::pi::TTXLogHelper::readLogRaw() const
{
  std::vector<uint32_t> res;
  uint32_t const numEntries = numLogEntries();
  if (numEntries != 0)
    {
      uint32_t const from = 0;
      uint32_t const to = numEntries;
      res = readLogRaw(from, to);
    }
  return res;
}

std::vector<uint32_t>
tcds::pi::TTXLogHelper::readLogRaw(uint32_t const from) const
{
  // NOTE: The 'from' address is inclusive.
  std::vector<uint32_t> res;
  uint32_t const numEntries = numLogEntries();
  if (numEntries != 0)
    {
      uint32_t const to = numEntries;
      res = readLogRaw(from, to);
    }
  return res;
}

std::vector<uint32_t>
tcds::pi::TTXLogHelper::readLogRaw(uint32_t const first, uint32_t const last) const
{
  // NOTE: The 'first' address to read is inclusive, and the 'last' is
  // exclusive. (So it's really 'from' and 'to'.)
  std::vector<uint32_t> res;
  if (first < last)
    {
      // The 'normal' case.
      uint32_t const numEntries = last - first;
      uint32_t const blockSize = numEntries * numWordsPerEntry_;
      uint32_t const offset = first * numWordsPerEntry_;
      try {
        res = hw_.readBlockOffset(regNamePrefix_ + ".log_mem", blockSize, offset);
      }
      // BUG BUG BUG
      // This is temporary code to try and confirm the origin of a
      // rare error condition reading out the TTC spy log.
      catch (std::exception const& err) {
        std::cout << "DEBUG JGH TTC spy mem | first       = " << first << std::endl;
        std::cout << "DEBUG JGH TTC spy mem | last        = " << last << std::endl;
        std::cout << "DEBUG JGH TTC spy mem | numEntries  = " << numEntries << std::endl;
        std::cout << "DEBUG JGH TTC spy mem | blockSize   = " << blockSize << std::endl;
        std::cout << "DEBUG JGH TTC spy mem | offset      = " << offset << std::endl;
        std::cout << "DEBUG JGH TTC spy mem ------------------------------ " << std::endl;
        toolbox::stacktrace(20, std::cout);
        std::cout << "DEBUG JGH TTC spy mem ------------------------------ " << std::endl;
        throw;
      }
      // BUG BUG BUG end
    }
  else if (first > last)
    {
      // The 'rolled over' case.
      std::vector<uint32_t> const tmp0 = readLogRaw(first, maxNumLogEntries());
      std::vector<uint32_t> const tmp1 = readLogRaw(0, last);
      res = tmp0;
      res.insert(res.end(), tmp1.begin(), tmp1.end());
    }
  return res;
}
