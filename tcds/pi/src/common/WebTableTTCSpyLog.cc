#include "tcds/pi/WebTableTTCSpyLog.h"

#include <cassert>
#include <sstream>
#include <string>

#include "tcds/utils/Monitor.h"

tcds::pi::WebTableTTCSpyLog::WebTableTTCSpyLog(std::string const& name,
                                               std::string const& description,
                                               tcds::utils::Monitor const& monitor,
                                               std::string const& itemSetName,
                                               std::string const& tabName,
                                               size_t const colSpan) :
  tcds::utils::WebObject(name, description, monitor, itemSetName, tabName, colSpan)
{
}

std::string
tcds::pi::WebTableTTCSpyLog::getHTMLString() const
{
  // NOTE: This has been written to work with the new XDAQ12-style
  // HyperDAQ tabs and doT.js.

  std::stringstream res;

  res << "<div class=\"tcds-item-table-wrapper\">"
      << "\n";

  res << "<p class=\"tcds-item-table-title\">"
      << getName()
      << "</p>";
  res << "\n";

  res << "<p class=\"tcds-item-table-description\">"
      << getDescription()
      << "</p>";
  res << "\n";

  tcds::utils::Monitor::StringPairVector items =
    monitor_.getFormattedItemSet(itemSetName_);
  // ASSERT ASSERT ASSERT
  assert (items.size() == 1);
  // ASSERT ASSERT ASSERT end

  // NOTE: This one is kinda special. Mainly due to the fact that this
  // table can become incredibly long. Therefore this is handled
  // completely in JavaScript (with the help of a cool, virtual
  // scrolling grid plugin).
  res << "<div id=\"ttcspylog-placeholder\"></div>"
      << "\n";

  // std::string const itemName = items.at(0).first;
  // std::string const tmp = "[\"" + itemSetName_ + "\"][\"" + itemName + "\"]";

  // std::string const invalidVal = "-";
  // res << "<script id=\"test\" type=\"text/x-dot-template\">";
  // // Case 0: invalid item ('-').
  // res << "{{? it" << tmp << " == '" << invalidVal << "'}}"
  //     << "<span>" << invalidVal << "</span>"
  //     << "\n";
  // // Case 1: empty array.
  // res << "{{?? it" << tmp << ".length == 0}}"
  //     << "\n"
  //     << "<span>empty</span>"
  //     << "\n";
  // // Case 2: non-empty array.
  // res << "{{??}}"
  //     << "\n"
  //     << "<div id=\"ttcspylog-placeholder\">"
  //     // << "{{~it" << tmp << " :value:index}}"
  //     // << "\n"
  //     // << "<div>{{=value[\"Entry\"]}}</div>"
  //     // << "<div>{{=value[\"Delta\"]}}</div>"
  //     // << "<div>{{=value[\"BX\"]}}</div>"
  //     // << "<div>{{=value[\"Type\"]}}</div>"
  //     // << "<div>{{=value[\"Flags\"]}}</div>"
  //     // << "<div>{{=value[\"Data\"]}}</div>"
  //     // << "<div>{{=value[\"Address\"]}}</div>"
  //     // << "\n"
  //     // << "{{~}}"
  //     << "</div>"
  //     << "\n";
  // res << "{{?}}"
  //     << "\n";
  // res << "</script>"
  //     << "<div class=\"target\"></div>"
  //     << "\n";

  return res.str();
}
