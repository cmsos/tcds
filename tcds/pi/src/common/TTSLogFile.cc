#include "tcds/pi/TTSLogFile.h"

#include <algorithm>
#include <cerrno>
#include <cstdio>
#include <cstring>
#include <iomanip>
#include <map>

#include "toolbox/string.h"
#include "xcept/Exception.h"

#include "tcds/exception/Exception.h"
#include "tcds/pi/TTSLogEntry.h"
#include "tcds/pi/Utils.h"
#include "tcds/utils/Definitions.h"
#include "tcds/utils/Utils.h"

tcds::pi::TTSLogFile::TTSLogFile(std::string const& softwareVersion,
                                 std::string const& firmwareVersion,
                                 std::string const& serviceName,
                                 uint32_t const runNumber,
                                 std::map<tcds::definitions::TTS_SOURCE, std::string> const& ttsChannelLabels,
                                 std::string const& logFilePath,
                                 std::string const& logFileName,
                                 bool const isContinuation) :
  sepLine_(100, kCommentChar),
  softwareVersion_(softwareVersion),
  firmwareVersion_(firmwareVersion),
  serviceName_(serviceName),
  runNumber_(runNumber),
  ttsChannelLabels_(ttsChannelLabels),
  logFilePath_(logFilePath),
  logFileName_(logFileName),
  logFileFullName_(""),
  lockFileFullName_(""),
  isContinuation_(isContinuation),
  numEntries_(0)
{
  std::string tmp = logFilePath;
  if (!toolbox::endsWith(tmp, "/"))
    {
      tmp += "/";
    }
  logFileFullName_ = tmp + logFileName_;
  lockFileFullName_ = tmp + "." + logFileName_ + ".lock";
  openLog();
}

tcds::pi::TTSLogFile::~TTSLogFile()
{
  closeLog();
}

void
tcds::pi::TTSLogFile::openLog()
{
  // Open a new file,
  openLogFile();
  // and start with a descriptive opening.
  logBegin(isContinuation_);
}

void
tcds::pi::TTSLogFile::closeLog(bool const isToBeContinued)
{
  logEnd(isToBeContinued);
  closeLogFile();
}

void
tcds::pi::TTSLogFile::addEntry(tcds::pi::TTSLogEntry const& logEntry)
{
  // Timestamp, just because it's easy when looking at the logs. The
  // timestamp in ISO format the way XDAQ's toolbox::TimeVal does it
  // uses 26 characters.
  log_ << std::setw(26) << tcds::utils::getTimestamp(true);

  // We have a 48-bit orbit counter, so we need 15 characters to
  // represent the decimal number.
  log_ << " ";
  log_ << std::setw(15) << logEntry.orbitNumber();

  // BX counter. 12 bits, so 4 characters.
  log_ << " ";
  log_ << std::setw(4) << logEntry.bxNumber();

  // All TTS states.
  for (unsigned int channel = tcds::definitions::TTS_CHANNEL_MIN;
       channel <= tcds::definitions::TTS_CHANNEL_MAX;
       ++channel)
    {
      tcds::definitions::TTS_SOURCE const source  = static_cast<tcds::definitions::TTS_SOURCE>(channel);
      uint8_t const ttsVal = logEntry.ttsVal(source);
      tcds::definitions::TTS_STATE const ttsState = static_cast<tcds::definitions::TTS_STATE>(ttsVal);

      std::string tmpVal = "";
      std::string tmpFlags = "";

      if (logEntry.isPureComment())
        {
          tmpVal = "LM_00";
          tmpFlags = "   ";
        }
      else
        {
          // The details shown depend a bit on whether this TTS
          // channel represents an input, an output, or something
          // else, as well as on the masking state of the channel
          // (where applicable).
          // - The TTS state is always shown.
          tmpVal = tcds::utils::TTSStateToStringShort(ttsState);
          // - The mask flag is shown only for inputs. Otherwise a
          //   'don't care' is shown.
          if (isTTSInput(source))
            {
              tmpFlags += (logEntry.isEnabled(source) ? "." : "M");
            }
          else
            {
              tmpFlags += "x";
            }
          // - The 'forced' flag is shown for inputs and
          //   outputs. Again, otherwise an 'x'.
          if (isTTSInput(source) || isTTSOutput(source))
            {
              tmpFlags += (logEntry.isForced(source) ? "F" : ".");
            }
          else
            {
              tmpFlags += "x";
            }
          // - Only inputs can be aligned. So a flag for an input, and
          //   otherwise an 'x'.
          if (isTTSInput(source))
            {
              tmpFlags += (logEntry.isAligned(source) ? "." : "U");
            }
          else
            {
              tmpFlags += "x";
            }
        }
      log_ << " " << tmpVal << tmpFlags;
    }

  // The synchronisation source of the TTS logger..
  log_ << " ";
  log_ << "\"" << OC0ReasonToString(logEntry.getLatestOC0Reason()) << "\"";

  // A 'useful comment' (without fixed width, of course), enclosed
  // within double quotes for ease of parsing.
  std::string comment = logEntry.detailedComment(ttsChannelLabels_);
  std::replace(comment.begin(), comment.end(), '"', '\'');
  log_ << " ";
  log_ << "\"" << comment << "\"";

  // End line and flush so the line shows up in the log file (in case
  // someone is tailing it...).
  log_ << "\n";
  log_.flush();

  // Keep track of accounting.
  ++numEntries_;
}

int
tcds::pi::TTSLogFile::numEntries() const
{
  return numEntries_;
}

void
tcds::pi::TTSLogFile::openLogFile() const
{
  // Check if the output directory exists, or can be made.
  ensureOutputDirExists(logFilePath_);

  //----------

  // Create the lock file first.
  lock_.open(lockFileFullName_.c_str(), std::ios::out | std::ios::trunc);
  if (!lock_.is_open())
    {
      // Somehow failed to create the lock file...
      std::string const msg = toolbox::toString("Failed to create lock file for PI TTS log output file '%s': '%s'",
                                                lockFileFullName_.c_str(),
                                                std::strerror(errno));
      XCEPT_RAISE(tcds::exception::RuntimeProblem, msg.c_str());
    }
  // Close the lock file again. We just need the file to exist. It
  // does not have to stay open.
  lock_.close();

  //----------

  // Just in case: check if the file already exists. Should not
  // happen, but still.
  if (tcds::utils::pathExists(logFileFullName_))
    {
      // Don't care whether this is a file or not, just rename it so
      // it's out of the way.
      // std::string const extension = ".txt";
      // size_t const pos = logFileFullName_.rfind(extension);
      std::string const crapFileName = toolbox::toString("%s.unexpected_%s",
                                                         logFileFullName_.c_str(),
                                                         tcds::utils::getTimestamp(true, true).c_str());
      // crapFileName.insert(pos, "_" + getTimestamp(true, true));
      tcds::utils::rename(logFileFullName_, crapFileName);
    }

  //----------

  // Now let's open a new file so we can start logging.
  log_.open(logFileFullName_.c_str(), std::ios::out | std::ios::trunc);
  if (!log_.is_open())
    {
      // Somehow failed to open the output file...
      std::string const msg = toolbox::toString("Failed to open PI TTS log output file '%s': '%s'",
                                                logFileFullName_.c_str(),
                                                std::strerror(errno));
      XCEPT_RAISE(tcds::exception::RuntimeProblem, msg.c_str());
    }
}

void
tcds::pi::TTSLogFile::closeLogFile() const
{
  // First close the log.
  if (log_.is_open())
    {
      log_.flush();
      log_.close();
    }

  // Then remove the lock file.
  if (lock_.is_open())
    {
      lock_.close();
    }
  std::remove(lockFileFullName_.c_str());
}

void
tcds::pi::TTSLogFile::logBegin(bool const isContinuation) const
{
  log_ << sepLine_ << "\n";

  log_ << kCommentChar;
  if (!isContinuation)
    {
      log_ << " Start ";
    }
  else
    {
      log_ << " Continuation ";
    }
  log_ << "of PI TTS log for service '" << serviceName_ << "'"
       << ", run " << runNumber_
       << ", time " << tcds::utils::getTimestamp()
       << "."
       << "\n";

  log_ << sepLine_ << "\n";

  logHeader();

  log_ << sepLine_ << "\n";
  log_.flush();
}

void
tcds::pi::TTSLogFile::logHeader() const
{
  log_ << kCommentChar
       << " Software version: " << softwareVersion_
       << "\n";

  log_ << kCommentChar
       << " Firmware version: " << firmwareVersion_
       << "\n";

  log_ << sepLine_ << "\n";

  log_ << kCommentChar
       << " Data format: space-separated columns with columns in the following order."
       << "\n";

  log_ << kCommentChar
       << "   Timestamp (from software, provided for convenience)."
       << " NOTE: Do not used these to calculate time differences."
       << "\n";

  log_ << kCommentChar
       << "   Orbit number."
       << "\n";

  log_ << kCommentChar
       << "   BX number."
       << "\n";

  log_ << kCommentChar
       << "   TTS states and status flags for all channels:"
       << "\n";

  for (unsigned int channel = tcds::definitions::TTS_CHANNEL_MIN;
       channel <= tcds::definitions::TTS_CHANNEL_MAX;
       ++channel)
    {
      std::string const channelLabel =
        ttsChannelLabels_.at(static_cast<tcds::definitions::TTS_SOURCE>(channel));
      log_ << kCommentChar << "     - " << channelLabel << "\n";
    }

  log_ << kCommentChar
       << "   Synchronisation source of the TTS logger."
       << " E.g., firmware reset, software reset, OC0."
       << " (In double quotes for ease of parsing.)"
       << "\n";

  log_ << kCommentChar
       << "   Useful comment. (In double quotes for ease of parsing.)"
       << "\n";

  log_ << kCommentChar
       << " NOTE: Pure comments (added by the software instead of the hardware)"
       << " are marked with:"
       << "\n"
       << "        all TTS states LM_00, with orbit number 0"
       << " and (invalid) BX number 0, and without status flags."
       << "\n";

  log_ << kCommentChar
       << " NOTE: TTS states are represented as five-character strings:"
       << "\n"
       << "        a two-character 'human-readable' string, followed by an underscore"
       << " and a two-character hexadecimal value."
       << "\n";

  log_ << kCommentChar
       << " NOTE: TTS states are followed by a three-character flag field, containing:"
       << "\n"
       << "        an 'M'/'.'/'x' in case the channel is masked/enabled/unmaskable,"
       << "\n"
       << "        an 'F'/'.'/'x' in case the channel if forced/unforced/unforceable, and"
       << "\n"
       << "        a 'U'/'.'/'x' in case the channel is unaligned/aligned/not an input."
       << "\n"
       << "        This means that under normal conditions, flag fields should only contain"
       << " 'x's and '.'s."
       << "\n";
}

void
tcds::pi::TTSLogFile::logEnd(bool const isToBeContinued) const
{
  log_ << sepLine_ << "\n";

  log_ << kCommentChar;
  if (!isToBeContinued)
    {
      log_ << " End ";
    }
  else
    {
      log_ << " Break (to be continued in the next file) ";
    }
  log_ << "of PI TTS log for service '" << serviceName_ << "'"
       << ", run " << runNumber_
       << ", time " << tcds::utils::getTimestamp()
       << "."
       << "\n";

  log_ << sepLine_ << "\n";
  log_.flush();
}
