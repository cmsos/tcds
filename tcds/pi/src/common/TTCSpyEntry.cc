#include "tcds/pi/TTCSpyEntry.h"

#include <cassert>
#include <vector>

#include "tcds/utils/Definitions.h"

// Constructor for L1As.
tcds::pi::TTCSpyEntry::TTCSpyEntry(TTC_TRAFFIC_TYPE const trafficType,
                                   uint64_t const orbitNumber,
                                   uint16_t const bxNumber,
                                   bool const hasSingleBitError,
                                   bool const hasDoubleBitError,
                                   bool const hasFrameError) :
  trafficType_(trafficType),
  orbitNumber_(orbitNumber),
  bxNumber_(bxNumber),
  data_(0x0),
  address_(0x0),
  subAddress_(0x0),
  addressExternal_(false),
  hasSingleBitError_(hasSingleBitError),
  hasDoubleBitError_(hasDoubleBitError),
  hasFrameError_(hasFrameError)
{
  // ASSERT ASSERT ASSERT
  assert (trafficType == TTC_TRAFFIC_TYPE_L1A);
  // ASSERT ASSERT ASSERT end
}

// Constructor for short B-commands.
tcds::pi::TTCSpyEntry::TTCSpyEntry(TTC_TRAFFIC_TYPE const trafficType,
                                   uint64_t const orbitNumber,
                                   uint16_t const bxNumber,
                                   bool const hasSingleBitError,
                                   bool const hasDoubleBitError,
                                   bool const hasFrameError,
                                   uint8_t const data) :
  trafficType_(trafficType),
  orbitNumber_(orbitNumber),
  bxNumber_(bxNumber),
  data_(data),
  address_(0x0),
  subAddress_(0x0),
  addressExternal_(false),
  hasSingleBitError_(hasSingleBitError),
  hasDoubleBitError_(hasDoubleBitError),
  hasFrameError_(hasFrameError)
{
  // ASSERT ASSERT ASSERT
  assert (trafficType == TTC_TRAFFIC_TYPE_BCOMMAND_SHORT);
  // ASSERT ASSERT ASSERT end
}

// Constructor for long B-commands.
tcds::pi::TTCSpyEntry::TTCSpyEntry(TTC_TRAFFIC_TYPE const trafficType,
                                   uint64_t const orbitNumber,
                                   uint16_t const bxNumber,
                                   bool const hasSingleBitError,
                                   bool const hasDoubleBitError,
                                   bool const hasFrameError,
                                   uint8_t const data,
                                   uint16_t const address,
                                   uint8_t const subAddress,
                                   bool const addressExternal) :
  trafficType_(trafficType),
  orbitNumber_(orbitNumber),
  bxNumber_(bxNumber),
  data_(data),
  address_(address),
  subAddress_(subAddress),
  addressExternal_(addressExternal),
  hasSingleBitError_(hasSingleBitError),
  hasDoubleBitError_(hasDoubleBitError),
  hasFrameError_(hasFrameError)
{
  // ASSERT ASSERT ASSERT
  assert (trafficType == TTC_TRAFFIC_TYPE_BCOMMAND_LONG);
  // ASSERT ASSERT ASSERT end
}

tcds::pi::TTCSpyEntry::~TTCSpyEntry()
{
}

bool
tcds::pi::TTCSpyEntry::isL1A() const
{
  return (trafficType_ == TTC_TRAFFIC_TYPE_L1A);
}

bool
tcds::pi::TTCSpyEntry::isBCommand() const
{
  return (isBCommandShort() || isBCommandLong());
}

bool
tcds::pi::TTCSpyEntry::isBCommandShort() const
{
  return (trafficType_ == TTC_TRAFFIC_TYPE_BCOMMAND_SHORT);
}

bool
tcds::pi::TTCSpyEntry::isBCommandLong() const
{
  return (trafficType_ == TTC_TRAFFIC_TYPE_BCOMMAND_LONG);
}

bool
tcds::pi::TTCSpyEntry::isBCntRes() const
{
  return (isBCommandShort() && ((data_ & 0x1) != 0x0));
}

bool
tcds::pi::TTCSpyEntry::isEvCntRes() const
{
  return (isBCommandShort() && ((data_ & 0x2) != 0x0));
}

std::string
tcds::pi::TTCSpyEntry::type() const
{
  std::string res;
  switch (trafficType_)
    {
    case TTC_TRAFFIC_TYPE_L1A:
      res = "L1A";
      break;
    case TTC_TRAFFIC_TYPE_BCOMMAND_SHORT:
      res = "Broadcast command";
      break;
    case TTC_TRAFFIC_TYPE_BCOMMAND_LONG:
      res = "Addressed command";
      break;
    default:
      res = "Unknown";
      break;
    }
  return res;
}

uint64_t
tcds::pi::TTCSpyEntry::orbitNumber() const
{
  return orbitNumber_;
}

uint16_t
tcds::pi::TTCSpyEntry::bxNumber() const
{
  // NOTE: Always subtract 1! The TTCSpy has already processed the BC0
  // when it logs the entry, so a BC0 is logged in BX 1 instead of BX
  // 3564 (which is correct by definition).
  int res = bxNumber_;
  res -= 1;

  // NOTE: To calibrate the TTCSpy to report exactly the same as the
  // output of a TTCrx TTC decoder ASIC, we have to subtract 3 from
  // the reported BX numbers for triggers.
  if (isL1A())
    {
      res -= kTTCSpyAToBChannelOffset;
    }

  return fixBXRange(res);
}

std::string
tcds::pi::TTCSpyEntry::flags() const
{
  std::vector<std::string> flags;
  if (isBCntRes())
    {
      flags.push_back("BCntRes");
    }
  if (isEvCntRes())
    {
      flags.push_back("EvCntRes");
    }

  if (hasSingleBitError())
    {
      flags.push_back("Single-bit Hamming error");
    }
  if (hasDoubleBitError())
    {
      flags.push_back("Double-bit Hamming error");
    }
  if (hasFrameError())
    {
      flags.push_back("Communication error");
    }

  if (flags.size() == 0)
    {
      flags.push_back("None");
    }

  std::string flagsStr = "";
  for (std::vector<std::string>::const_iterator flag = flags.begin();
       flag != flags.end();
       ++flag)
    {
      if (flagsStr.size() > 0)
        {
          flagsStr += " & ";
        }
      flagsStr += *flag;
    }
  return flagsStr;
}

uint8_t
tcds::pi::TTCSpyEntry::data() const
{
  return data_;
}

uint16_t
tcds::pi::TTCSpyEntry::address() const
{
  return address_;
}

uint8_t
tcds::pi::TTCSpyEntry::subAddress() const
{
  return subAddress_;
}

bool
tcds::pi::TTCSpyEntry::isAddressExternal() const
{
  return addressExternal_;
}

int
tcds::pi::TTCSpyEntry::fixBXRange(int const bxIn) const
{
  // Bound the given BX number in a valid LHC orbit range.
  int bxOut = bxIn;

  int const bxMin = tcds::definitions::kFirstBX;
  int const bxMax = tcds::definitions::kLastBX;

  if (bxOut < bxMin)
    {
      bxOut += bxMax;
    }

  if (bxOut > bxMax)
    {
      bxOut -= bxMax;
    }

  return bxOut;
}

bool
tcds::pi::TTCSpyEntry::hasSingleBitError() const
{
  return hasSingleBitError_;
}

bool
tcds::pi::TTCSpyEntry::hasDoubleBitError() const
{
  return hasDoubleBitError_;
}

bool
tcds::pi::TTCSpyEntry::hasFrameError() const
{
  return hasFrameError_;
}
