#ifndef _tcds_pi_PITTSAuxInfoSpaceHandler_h_
#define _tcds_pi_PITTSAuxInfoSpaceHandler_h_

/* #include <map> */
#include <string>
/* #include <vector> */

#include "tcds/utils/InfoSpaceHandler.h"

namespace xdaq {
  class Application;
}

namespace tcds {
  namespace utils {
    class InfoSpaceUpdater;
    /* class Monitor; */
    /* class WebServer; */
    /* class XDAQAppBase; */
  }
}

namespace tcds {
  namespace pi {

    class PITTSAuxInfoSpaceHandler : public tcds::utils::InfoSpaceHandler
    {

    public:
      PITTSAuxInfoSpaceHandler(xdaq::Application& xdaqApp,
                               std::string const& name,
                               tcds::utils::InfoSpaceUpdater* updater);
      virtual ~PITTSAuxInfoSpaceHandler();

      /* tcds::utils::XDAQAppBase& getOwnerApplication() const; */

    protected:
      /* virtual void registerItemSetsWithMonitor(tcds::utils::Monitor& monitor); */
      /* virtual void registerItemSetsWithWebServer(tcds::utils::WebServer& webServer, */
      /*                                            tcds::utils::Monitor& monitor, */
      /*                                            std::string const& forceTabName=""); */

      virtual std::string formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const;

    /* private: */
    /*   std::vector<std::string> inputNames_; */
    /*   std::map<std::string, std::string> inputLabels_; */

    };

  } // namespace pi
} // namespace tcds

#endif // _tcds_pi_PITTSAuxInfoSpaceHandler_h_
