#ifndef _tcds_pi_PIController_h_
#define _tcds_pi_PIController_h_

#include <memory>
#include <string>
#include <vector>

#include "toolbox/Event.h"
#include "xdaq/Application.h"
#include "xoap/MessageReference.h"

#include "tcds/pi/PIDAQLoop.h"
#include "tcds/pi/TCADevicePI.h"
#include "tcds/utils/FSMSOAPParHelper.h"
#include "tcds/utils/RegCheckResult.h"
#include "tcds/utils/SOAPCmdBase.h"
#include "tcds/utils/SOAPCmdConfigureTTCSpy.h"
#include "tcds/utils/SOAPCmdDisableTTCSpy.h"
#include "tcds/utils/SOAPCmdDumpHardwareState.h"
#include "tcds/utils/SOAPCmdEnableTTCSpy.h"
#include "tcds/utils/SOAPCmdReadHardwareConfiguration.h"
#include "tcds/utils/SOAPCmdResetTTCSpyLog.h"
#include "tcds/utils/XDAQAppWithFSMBasic.h"

namespace xdaq {
  class ApplicationStub;
}

namespace tcds {
  namespace hwlayer {
    class RegisterInfo;
  }
}

namespace tcds {
  namespace hwutilstca {
    class HwIDInfoSpaceHandlerTCA;
    class HwIDInfoSpaceUpdaterTCA;
    class SFPInfoSpaceUpdater;
  }
}

namespace tcds {
  namespace pi {

    class HwStatusInfoSpaceHandler;
    class HwStatusInfoSpaceUpdater;
    class PITTCInfoSpaceHandler;
    class PITTCInfoSpaceUpdater;
    class PITTSInfoSpaceHandler;
    class PITTSInfoSpaceUpdater;
    class PITTSLinksInfoSpaceHandler;
    class PITTSLinksInfoSpaceUpdater;
    class SFPInfoSpaceHandler;
    class TTCSpyInfoSpaceHandler;
    class TTCSpyInfoSpaceUpdater;
    class TTSLogInfoSpaceHandler;
    class TTSLogInfoSpaceUpdater;

    class PIController : public tcds::utils::XDAQAppWithFSMBasic
    {

      friend class PIDAQLoop;

    public:
      XDAQ_INSTANTIATOR();

      PIController(xdaq::ApplicationStub* stub);
      virtual ~PIController();

    protected:
      virtual void setupInfoSpaces();

      /**
       * Access the hardware pointer as TCADevicePI&.
       */
      virtual TCADevicePI& getHw() const;

      virtual void hwConnectImpl();
      virtual void hwReleaseImpl();

      virtual void hwCfgInitializeImpl();
      virtual void hwCfgFinalizeImpl();

      virtual void configureActionImpl(toolbox::Event::Reference event);
      virtual void enableActionImpl(toolbox::Event::Reference event);
      virtual void haltActionImpl(toolbox::Event::Reference event);
      virtual void stopActionImpl(toolbox::Event::Reference event);
      virtual void zeroActionImpl(toolbox::Event::Reference event);

      virtual tcds::utils::RegCheckResult isRegisterAllowed(tcds::hwlayer::RegisterInfo const& regInfo) const;

      virtual std::vector<tcds::utils::FSMSOAPParHelper> expectedFSMSoapParsImpl(std::string const& commandName) const;
      virtual void loadSOAPCommandParameterImpl(xoap::MessageReference const& msg,
                                                tcds::utils::FSMSOAPParHelper const& param);

    private:
      // Various InfoSpaces and their InfoSpaceUpdaters.
      std::unique_ptr<tcds::hwutilstca::HwIDInfoSpaceUpdaterTCA> hwIDInfoSpaceUpdaterP_;
      std::unique_ptr<tcds::hwutilstca::HwIDInfoSpaceHandlerTCA> hwIDInfoSpaceP_;
      std::unique_ptr<tcds::pi::HwStatusInfoSpaceUpdater> hwStatusInfoSpaceUpdaterP_;
      std::unique_ptr<tcds::pi::HwStatusInfoSpaceHandler> hwStatusInfoSpaceP_;
      std::unique_ptr<tcds::pi::PITTCInfoSpaceUpdater> piInputInfoSpaceUpdaterP_;
      std::unique_ptr<tcds::pi::PITTCInfoSpaceHandler> piInputInfoSpaceP_;
      std::unique_ptr<tcds::pi::PITTSInfoSpaceUpdater> piTTSInfoSpaceUpdaterP_;
      std::unique_ptr<tcds::pi::PITTSInfoSpaceHandler> piTTSInfoSpaceP_;
      std::unique_ptr<tcds::pi::PITTSLinksInfoSpaceUpdater> piTTSLinksInfoSpaceUpdaterP_;
      std::unique_ptr<tcds::pi::PITTSLinksInfoSpaceHandler> piTTSLinksInfoSpaceP_;
      std::unique_ptr<tcds::pi::SFPInfoSpaceHandler> sfpInfoSpaceP_;
      std::unique_ptr<tcds::hwutilstca::SFPInfoSpaceUpdater> sfpInfoSpaceUpdaterP_;
      std::unique_ptr<tcds::pi::TTCSpyInfoSpaceUpdater> ttcspyInfoSpaceUpdaterP_;
      std::unique_ptr<tcds::pi::TTCSpyInfoSpaceHandler> ttcspyInfoSpaceP_;
      std::unique_ptr<tcds::pi::TTSLogInfoSpaceUpdater> ttslogInfoSpaceUpdaterP_;
      std::unique_ptr<tcds::pi::TTSLogInfoSpaceHandler> ttslogInfoSpaceP_;

      // The SOAP commands.
      template<typename> friend class tcds::utils::SOAPCmdBase;
      template<typename> friend class tcds::utils::SOAPCmdConfigureTTCSpy;
      template<typename> friend class tcds::utils::SOAPCmdDisableTTCSpy;
      template<typename> friend class tcds::utils::SOAPCmdDumpHardwareState;
      template<typename> friend class tcds::utils::SOAPCmdEnableTTCSpy;
      template<typename> friend class tcds::utils::SOAPCmdReadHardwareConfiguration;
      template<typename> friend class tcds::utils::SOAPCmdResetTTCSpyLog;
      tcds::utils::SOAPCmdConfigureTTCSpy<PIController> soapCmdConfigureTTCSpy_;
      tcds::utils::SOAPCmdDisableTTCSpy<PIController> soapCmdDisableTTCSpy_;
      tcds::utils::SOAPCmdDumpHardwareState<PIController> soapCmdDumpHardwareState_;
      tcds::utils::SOAPCmdEnableTTCSpy<PIController> soapCmdEnableTTCSpy_;
      tcds::utils::SOAPCmdReadHardwareConfiguration<PIController> soapCmdReadHardwareConfiguration_;
      tcds::utils::SOAPCmdResetTTCSpyLog<PIController> soapCmdResetTTCSpyLog_;

      // A dedicated acquisition loop for the TTC and TTS logs.
      tcds::pi::PIDAQLoop piDAQLoop_;

    };

  } // namespace pi
} // namespace tcds

#endif // _tcds_pi_PIController_h_
