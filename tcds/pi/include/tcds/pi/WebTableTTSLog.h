#ifndef _tcds_pi_WebTableTTSLog_h
#define _tcds_pi_WebTableTTSLog_h

#include <cstddef>
#include <string>

#include "tcds/utils/WebObject.h"

namespace tcds {
  namespace utils {
    class Monitor;
  }
}

namespace tcds {
  namespace pi {

    class WebTableTTSLog : public tcds::utils::WebObject
    {

    public:
      WebTableTTSLog(std::string const& name,
                     std::string const& description,
                     tcds::utils::Monitor const& monitor,
                     std::string const& itemSetName,
                     std::string const& tabName,
                     size_t const colSpan);

      std::string getHTMLString() const;

    };

  } // namespace pi
} // namespace tcds

#endif // _tcds_pi_WebTableTTSLog_h
