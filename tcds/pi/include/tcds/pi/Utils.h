#ifndef _tcds_pi_Utils_h_
#define _tcds_pi_Utils_h_

#include <map>
#include <string>
#include <vector>

#include "tcds/pi/Definitions.h"

namespace tcds {
  namespace utils {
    class ConfigurationInfoSpaceHandler;
  }
}

namespace tcds {
  namespace pi {

    bool isTTSInput(tcds::definitions::TTS_SOURCE const channel);
    bool isTTSOutput(tcds::definitions::TTS_SOURCE const channel);

    // Mapping of TTS output modes to strings.
    std::string ttsOutputModeToString(tcds::definitions::PI_TTS_OUTPUT_MODE const mode);

    // Mapping of configuration modes to strings.
    std::string configModeToString(tcds::definitions::PICONTROLLER_CONFIG_MODE const mode);

    // Mapping of TTS channels to register names.
    std::map<tcds::definitions::TTS_SOURCE, std::string> ttsChannelRegNameMap();
    std::string ttsChannelToRegName(tcds::definitions::TTS_SOURCE const channel);

    // Mapping of 'recent orbit-counter reset reasons' to readable strings.
    std::map<tcds::definitions::OC0_REASON, std::string> OC0ReasonMap();
    std::string OC0ReasonToString(tcds::definitions::OC0_REASON const reason);

    std::map<tcds::definitions::TTS_SOURCE, std::string> ttsChannelMap();
    std::string formatTTSSourceLabel(tcds::definitions::TTS_SOURCE const source);
    std::string formatTTSSourceLabel(tcds::definitions::TTS_SOURCE const source,
                                     tcds::utils::ConfigurationInfoSpaceHandler const& cfgInfoSpace);
    std::string formatTTSChannelLabel(tcds::definitions::TTS_SOURCE const channel);
    std::string formatTTSChannelLabel(tcds::definitions::TTS_SOURCE const channel,
                                     tcds::utils::ConfigurationInfoSpaceHandler const& cfgInfoSpace);

    // A couple of helper methods used in the unpacking of TTC/TTS logging
    // data.
    template <typename T> std::vector<bool> intToVecBool(T const input);
    template <typename T> T vecBoolToUInt(std::vector<bool> const input);

    void ensureOutputDirExists(std::string const& path);

  } // namespace pi
} // namespace tcds

#include "tcds/pi/Utils.hxx"

#endif // _tcds_pi_Utils_h_
