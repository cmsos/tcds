#ifndef _tcds_pi_TTSLogHistory_h_
#define _tcds_pi_TTSLogHistory_h_

#include <deque>
#include <map>
#include <stddef.h>
#include <string>

#include "tcds/pi/Definitions.h"
#include "tcds/pi/TTSLogEntry.h"

namespace tcds {
  namespace utils {
    class ConfigurationInfoSpaceHandler;
  }
}

namespace tcds {
  namespace pi {

    class TTSLogHistory
    {

    public:
      TTSLogHistory();
      ~TTSLogHistory();

      // Configuration manipulation.
      void updateConfiguration(tcds::utils::ConfigurationInfoSpaceHandler const& cfgInfoSpace);

      void addEntry(tcds::pi::TTSLogEntry const& logEntry);
      void clear();

      std::string getJSONString() const;

    private:
      std::map<tcds::definitions::TTS_SOURCE, std::string> ttsChannelLabels_;
      size_t histSize_;

      std::deque<TTSLogEntry> entries_;

      void setTTSChannelLabels(std::map<tcds::definitions::TTS_SOURCE,
                               std::string> const& ttsChannelLabels);
      void setHistSize(size_t const histSize);

      size_t numEntries() const;
      void trim();

    };

  } // namespace pi
} // namespace tcds

#endif // _tcds_pi_TTSLogHistory_h_
