#ifndef _tcds_pi_PITTSLinksInfoSpaceHandler_h_
#define _tcds_pi_PITTSLinksInfoSpaceHandler_h_

#include <map>
#include <string>
#include <vector>

#include "tcds/utils/InfoSpaceHandler.h"

namespace xdaq {
  class Application;
}

namespace tcds {
  namespace utils {
    class InfoSpaceUpdater;
    class Monitor;
    class WebServer;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace pi {

    class PITTSLinksInfoSpaceHandler : public tcds::utils::InfoSpaceHandler
    {

    public:
      PITTSLinksInfoSpaceHandler(xdaq::Application& xdaqApp,
                                 tcds::utils::InfoSpaceUpdater* updater);
      virtual ~PITTSLinksInfoSpaceHandler();

      tcds::utils::XDAQAppBase& getOwnerApplication() const;

    protected:
      virtual void registerItemSetsWithMonitor(tcds::utils::Monitor& monitor);
      virtual void registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                 tcds::utils::Monitor& monitor,
                                                 std::string const& forceTabName="");

    private:
      std::vector<std::string> inputNames_;
      std::map<std::string, std::string> inputLabels_;

    };

  } // namespace pi
} // namespace tcds

#endif // _tcds_pi_PITTSLinksInfoSpaceHandler_h_
