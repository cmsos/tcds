#ifndef _tcds_pi_SFPInfoSpaceHandler_h_
#define _tcds_pi_SFPInfoSpaceHandler_h_

#include <string>

#include "tcds/hwutilstca/SFPInfoSpaceHandlerBase.h"

namespace tcds {
  namespace utils {
    class Monitor;
    class WebServer;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace hwutilstca {
    class SFPInfoSpaceUpdater;
  }
}

namespace tcds {
  namespace pi {

    class SFPInfoSpaceHandler : public tcds::hwutilstca::SFPInfoSpaceHandlerBase
    {

    public:
      SFPInfoSpaceHandler(tcds::utils::XDAQAppBase& xdaqApp,
                          tcds::hwutilstca::SFPInfoSpaceUpdater* updater);
      virtual ~SFPInfoSpaceHandler();

    protected:
      virtual void registerItemSetsWithMonitor(tcds::utils::Monitor& monitor);
      virtual void registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                 tcds::utils::Monitor& monitor,
                                                 std::string const& forceTabName="");

    };

  } // namespace pi
} // namespace tcds

#endif // _tcds_pi_SFPInfoSpaceHandler_h_
