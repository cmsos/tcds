#ifndef _tcds_pi_TTSLogWriter_h_
#define _tcds_pi_TTSLogWriter_h_

#include <memory>
#include <map>
#include <stdint.h>
#include <string>

#include "tcds/pi/Definitions.h"

namespace tcds {
  namespace utils {
    class ConfigurationInfoSpaceHandler;
  }
}

namespace tcds {
  namespace pi {

    class TTSLogEntry;
    class TTSLogFile;

    class TTSLogWriter
    {

    public:
      TTSLogWriter();
      ~TTSLogWriter();

      // Configuration manipulation.
      void updateConfiguration(std::string const& softwareVersion,
                               std::string const& firmwareVersion,
                               tcds::utils::ConfigurationInfoSpaceHandler const& cfgInfoSpace);

      // Logging manipulation.
      void openLog(bool const isContinuation=false);
      void closeLog(bool const isToBeContinued=false);
      void addEntry(tcds::pi::TTSLogEntry const& logEntry);

    private:
      std::string softwareVersion_;
      std::string firmwareVersion_;

      std::string serviceName_;
      uint32_t runNumber_;
      std::map<tcds::definitions::TTS_SOURCE, std::string> ttsChannelLabels_;
      std::string logFilePath_;
      uint32_t ttsLogSize_;

      // In order to prevent the TTS log file from becoming too big,
      // we start a new one every now and then. The individual files
      // can be distinguished based on their sequence numbers.
      unsigned int logFileSequenceNumber_;

      std::unique_ptr<tcds::pi::TTSLogFile> logFileP_;

      void setSoftwareVersion(std::string const& softwareVersion);
      void setFirmwareVersion(std::string const& firmwareVersion);
      void setServiceName(std::string const& serviceName);
      void setRunNumber(uint32_t const runNumber);
      void setTTSChannelLabels(std::map<tcds::definitions::TTS_SOURCE,
                               std::string> const& ttsChannelLabels);
      void setLogBasePath(std::string const& basePath);
      void setLogSize(uint32_t const& ttsLogSize);

      // A method to decide if we should close the current TTS log
      // file and start a new one or not.
      bool shouldOpenNewFile() const;

    };

  } // namespace pi
} // namespace tcds

#endif // _tcds_pi_TTSLogWriter_h_
