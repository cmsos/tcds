#ifndef _tcds_pi_HwStatusInfoSpaceUpdater_h_
#define _tcds_pi_HwStatusInfoSpaceUpdater_h_

#include "tcds/hwutilstca/HwStatusInfoSpaceUpdaterTCA.h"

namespace tcds {
  namespace utils {
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace pi {

    class TCADevicePI;

    class HwStatusInfoSpaceUpdater : public tcds::hwutilstca::HwStatusInfoSpaceUpdaterTCA
    {

    public:
      HwStatusInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                               tcds::pi::TCADevicePI const& hw);
      virtual ~HwStatusInfoSpaceUpdater();

    private:
      tcds::pi::TCADevicePI const& getHw() const;

      tcds::pi::TCADevicePI const& hw_;

    };

  } // namespace pi
} // namespace tcds

#endif // _tcds_pi_HwStatusInfoSpaceUpdater_h_
