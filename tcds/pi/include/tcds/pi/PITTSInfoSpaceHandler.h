#ifndef _tcds_pi_PITTSInfoSpaceHandler_h_
#define _tcds_pi_PITTSInfoSpaceHandler_h_

#include <map>
#include <string>
#include <vector>

#include "tcds/pi/Definitions.h"
#include "tcds/utils/MultiInfoSpaceHandler.h"

namespace xdaq {
  class Application;
}

namespace tcds {
  namespace utils {
    class InfoSpaceHandler;
    class InfoSpaceUpdater;
    class Monitor;
    class WebServer;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace pi {

    class PITTSInfoSpaceHandler : public tcds::utils::MultiInfoSpaceHandler
    {

    public:
      PITTSInfoSpaceHandler(xdaq::Application& xdaqApp,
                            tcds::utils::InfoSpaceUpdater* updater);
      virtual ~PITTSInfoSpaceHandler();

      tcds::utils::XDAQAppBase& getOwnerApplication() const;

    protected:
      virtual void registerItemSetsWithMonitor(tcds::utils::Monitor& monitor);
      virtual void registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                 tcds::utils::Monitor& monitor,
                                                 std::string const& forceTabName="");

    private:
      std::map<tcds::definitions::TTS_SOURCE, std::string> inputNames_;
      std::map<tcds::definitions::TTS_SOURCE, std::string> inputLabels_;

      void createTTSChannel(tcds::utils::InfoSpaceHandler* const handler,
                            std::string const sourceLabel,
                            std::string const sourceType,
                            unsigned int const idNumber,
                            std::string const regName);

    };

  } // namespace pi
} // namespace tcds

#endif // _tcds_pi_PITTSInfoSpaceHandler_h_
