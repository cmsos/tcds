#ifndef _tcds_pi_TTXLogHelper_h_
#define _tcds_pi_TTXLogHelper_h_

#include <stddef.h>
#include <stdint.h>
#include <string>
#include <vector>

namespace tcds {
  namespace hwlayertca {
    class TCADeviceBase;
  }
}

namespace tcds {
  namespace pi {

    class TTXLogHelper
    {

      /* This is a helper class to handle the (pretty much common)
         firmware loggers for the PI TTC spy and the PI TTS logger. */

    public:

      enum LOGGING_BUFFER_MODE {LOGGING_BUFFER_MODE_SINGLESHOT = 0,
                                LOGGING_BUFFER_MODE_CIRCULAR = 1};

      TTXLogHelper(tcds::hwlayertca::TCADeviceBase const& hw,
                   std::string const& regNamePrefix,
                   size_t const numWordsPerEntry);
      ~TTXLogHelper();

      void setLogBufferMode(tcds::pi::TTXLogHelper::LOGGING_BUFFER_MODE const val) const;
      void setLogBufferSizeToMax() const;

      void enableLogging() const;
      void disableLogging() const;
      bool isLoggingEnabled() const;
      uint32_t resetLogging() const;

      uint32_t maxNumLogEntries() const;
      uint32_t numLogEntries() const;
      bool isLogFull() const;
      uint32_t readLogAddressPointer() const;

      std::vector<uint32_t> readLogRaw() const;
      std::vector<uint32_t> readLogRaw(uint32_t const from) const;
      std::vector<uint32_t> readLogRaw(uint32_t const first, uint32_t const last) const;

    private:

      tcds::hwlayertca::TCADeviceBase const& hw_;
      std::string const regNamePrefix_;
      size_t const numWordsPerEntry_;

    };

  } // namespace pi
} // namespace tcds

#endif // _tcds_pi_TTXLogHelper_h_
