#ifndef _tcds_pi_TTSLogEntry_h_
#define _tcds_pi_TTSLogEntry_h_

#include <cstddef>
#include <map>
#include <stdint.h>
#include <string>
#include <utility>
#include <vector>

#include "tcds/pi/Definitions.h"

namespace tcds {
  namespace pi {

    class TTSLogEntry
    {

    public:

      // The spy log is organized in6 4K entries of 32-bit words, with
      // each entry occupying 8 words mapped as follows.
      // - Memory location [8*entry+0] -> log[31:0]
      // - Memory location [8*entry+1] -> log[63:32]
      // - Memory location [8*entry+2] -> log[95:64]
      // - Memory location [8*entry+3] -> log[127:96]
      // - Memory location [8*entry+4] -> log[159:128]
      // - Memory location [8*entry+5] -> log[191:160]
      // - Memory location [8*entry+6] -> log[223:192]
      // - Memory location [8*entry+7] -> log[255:224]

      // Each log entry contains the following data:
      //   entry[254]     -> first entry in the log
      //   entry[253]     -> ocnt_last_reset_by_sw
      //   entry[252]     -> ocnt_last_reset_by_brc
      //   entry[251:240] -> bcnt
      //   entry[239:192] -> ocnt

      //   entry[191:0] as following

      //   entry[12*i+7 : 12*i] -> tts_data [i]
      //   entry[12*i+8]        -> tts_type [i]
      //   entry[12*i+9]        -> tts_en   [i]
      //   entry[12*i+10]       -> tts_force[i]
      //   entry[12*i+11]       -> aligned[i]

      // NOTE: Each entry can be accompanied by a simple text
      // comment. Entries with orbit number 0 and BX number 0 are used
      // as 'pure comments.'

      // This class was designed to handle TTS log entries. The number
      // of words per entry is based on the 32-bit definition of an
      // IPbus word.
      static size_t const kNumBitsPerLogEntry = 256;
      static size_t const kNumWordsPerLogEntry = kNumBitsPerLogEntry / 32;

      TTSLogEntry(std::string const& comment);
      TTSLogEntry(std::vector<uint32_t> const dataIn,
                  std::string const& comment="");

      bool isBaselineEntry() const;

      bool latestOC0WasSW() const;
      bool latestOC0WasBgo() const;
      tcds::definitions::OC0_REASON getLatestOC0Reason() const;

      uint64_t orbitNumber() const;
      uint16_t bxNumber() const;

      // NOTE: TTS values (in the PI) are in principle 9 bits wide:
      // - Eight bits for the received value.
      // - One bit to signal if the TTS RX control had to change the
      //   value (e.g., in case the link was not aligned).
      uint16_t ttsVal(tcds::definitions::TTS_SOURCE const channel) const;

      bool isEnabled(tcds::definitions::TTS_SOURCE const channel) const;
      bool isForced(tcds::definitions::TTS_SOURCE const channel) const;
      bool isAligned(tcds::definitions::TTS_SOURCE const channel) const;

      bool isPureComment() const;
      std::string comment() const;
      // A helper method to build/format a comprehensible comment
      // string based on the forced/aligned flags.
      // NOTE: This may not be very pretty. On the other hand: it
      // needs application-level information to be able to accurately
      // describe the FED inputs.
      std::string detailedComment(std::map<tcds::definitions::TTS_SOURCE, std::string> ttsChannelLabels) const;


      // A few utility methods derived from the above. For
      // convenience.
      bool isOutputForced() const;
      std::vector<tcds::definitions::TTS_SOURCE> forcedInputs(bool const considerEnabledOnly) const;
      std::vector<tcds::definitions::TTS_SOURCE> unalignedInputs(bool const considerEnabledOnly) const;

    private:

      // The following define the various pieces of a log entry.
      // NOTE: Not all of the aligned/forced/enabled flags make sense,
      // but this way it provides a uniform interface.
      static size_t const kBitPosBaselineEntry = 254;
      static size_t const kBitPosLatestOC0WasSw = 253;
      static size_t const kBitPosLatestOC0WasBgo = 252;

      static size_t const kBitPosBXNumHi = 251;
      static size_t const kBitPosBXNumLo = 240;
      static size_t const kBitPosOrbitHi = 239;
      static size_t const kBitPosOrbitLo = 192;

      static size_t const kBitPosTTSAlignedOut = 167;
      static size_t const kBitPosTTSForcedOut = 166;
      static size_t const kBitPosTTSEnabledOut = 165;
      static size_t const kBitPosTTSValOutHi = 164;
      static size_t const kBitPosTTSValOutLo = 156;

      static size_t const kBitPosTTSAlignedForce = 155;
      static size_t const kBitPosTTSForcedForce = 154;
      static size_t const kBitPosTTSEnabledForce = 153;
      static size_t const kBitPosTTSValForceHi = 152;
      static size_t const kBitPosTTSValForceLo = 144;

      static size_t const kBitPosTTSAlignedOR = 143;
      static size_t const kBitPosTTSForcedOR = 142;
      static size_t const kBitPosTTSEnabledOR = 141;
      static size_t const kBitPosTTSValORHi = 140;
      static size_t const kBitPosTTSValORLo = 132;

      static size_t const kBitPosTTSAlignedRJ45 = 131;
      static size_t const kBitPosTTSForcedRJ45 = 130;
      static size_t const kBitPosTTSEnabledRJ45 = 129;
      static size_t const kBitPosTTSValRJ45Hi = 128;
      static size_t const kBitPosTTSValRJ45Lo = 120;

      static size_t const kBitPosTTSAlignedFED10 = 119;
      static size_t const kBitPosTTSForcedFED10 = 118;
      static size_t const kBitPosTTSEnabledFED10 = 117;
      static size_t const kBitPosTTSValFED10Hi = 116;
      static size_t const kBitPosTTSValFED10Lo = 108;

      static size_t const kBitPosTTSAlignedFED9 = 107;
      static size_t const kBitPosTTSForcedFED9 = 106;
      static size_t const kBitPosTTSEnabledFED9 = 105;
      static size_t const kBitPosTTSValFED9Hi = 104;
      static size_t const kBitPosTTSValFED9Lo = 96;

      static size_t const kBitPosTTSAlignedFED8 = 95;
      static size_t const kBitPosTTSForcedFED8 = 94;
      static size_t const kBitPosTTSEnabledFED8 = 93;
      static size_t const kBitPosTTSValFED8Hi = 92;
      static size_t const kBitPosTTSValFED8Lo = 84;

      static size_t const kBitPosTTSAlignedFED7 = 83;
      static size_t const kBitPosTTSForcedFED7 = 82;
      static size_t const kBitPosTTSEnabledFED7 = 81;
      static size_t const kBitPosTTSValFED7Hi = 80;
      static size_t const kBitPosTTSValFED7Lo = 72;

      static size_t const kBitPosTTSAlignedFED6 = 71;
      static size_t const kBitPosTTSForcedFED6 = 70;
      static size_t const kBitPosTTSEnabledFED6 = 69;
      static size_t const kBitPosTTSValFED6Hi = 68;
      static size_t const kBitPosTTSValFED6Lo = 60;

      static size_t const kBitPosTTSAlignedFED5 = 59;
      static size_t const kBitPosTTSForcedFED5 = 58;
      static size_t const kBitPosTTSEnabledFED5 = 57;
      static size_t const kBitPosTTSValFED5Hi = 56;
      static size_t const kBitPosTTSValFED5Lo = 48;

      static size_t const kBitPosTTSAlignedFED4 = 47;
      static size_t const kBitPosTTSForcedFED4 = 46;
      static size_t const kBitPosTTSEnabledFED4 = 45;
      static size_t const kBitPosTTSValFED4Hi = 44;
      static size_t const kBitPosTTSValFED4Lo = 36;

      static size_t const kBitPosTTSAlignedFED3 = 35;
      static size_t const kBitPosTTSForcedFED3 = 34;
      static size_t const kBitPosTTSEnabledFED3 = 33;
      static size_t const kBitPosTTSValFED3Hi = 32;
      static size_t const kBitPosTTSValFED3Lo = 24;

      static size_t const kBitPosTTSAlignedFED2 = 23;
      static size_t const kBitPosTTSForcedFED2 = 22;
      static size_t const kBitPosTTSEnabledFED2 = 21;
      static size_t const kBitPosTTSValFED2Hi = 20;
      static size_t const kBitPosTTSValFED2Lo = 12;

      static size_t const kBitPosTTSAlignedFED1 = 11;
      static size_t const kBitPosTTSForcedFED1 = 10;
      static size_t const kBitPosTTSEnabledFED1 = 9;
      static size_t const kBitPosTTSValFED1Hi = 8;
      static size_t const kBitPosTTSValFED1Lo = 0;

      void addData(std::vector<uint32_t> const dataIn);

      bool isBitSet(size_t const bitNum) const;
      std::vector<bool> getBit(size_t const bitNum) const;
      std::vector<bool> getBits(size_t const bitNumLo,
                                size_t const bitNumHi) const;

      std::map<tcds::definitions::TTS_SOURCE,
        std::pair<size_t, size_t> > bitLocationMap_;
      std::vector<bool> data_;
      std::string comment_;

    };

  } // namespace pi
} // namespace tcds

#endif // _tcds_pi_TTSLogEntry_h_
