#ifndef _tcds_pi_TCADevicePI_h_
#define _tcds_pi_TCADevicePI_h_

#include <stdint.h>
#include <string>
#include <vector>

#include "tcds/hwlayer/DeviceBase.h"
#include "tcds/hwlayer/RegisterInfo.h"
#include "tcds/hwlayertca/TCADeviceBase.h"
#include "tcds/pi/Definitions.h"
#include "tcds/pi/TTXLogHelper.h"

namespace tcds {
  namespace pi {

    /**
     * Implementation of Partition Interface (PI) functionality. The
     * PI is based on the FC7 FMC carrier board.
     */
    class TCADevicePI : public tcds::hwlayertca::TCADeviceBase
    {

    public:

      enum TTC_LOGGING_LOGIC {TTC_LOGGING_LOGIC_OR = 0,
                              TTC_LOGGING_LOGIC_AND = 1};
      enum TTC_LOGGING_MODE {TTC_LOGGING_MODE_ONLY = 0,
                             TTC_LOGGING_MODE_EXCEPT = 1};

      TCADevicePI();
      virtual ~TCADevicePI();

      void selectLPMSource(tcds::definitions::LPM_SOURCE const lpmSource) const;

      void enableTTSInput(tcds::definitions::TTS_SOURCE const ttsSource) const;
      void disableTTSInput(tcds::definitions::TTS_SOURCE const ttsSource) const;
      void disableTTSInputs() const;

      void resetOrbitCounter() const;
      void resetOrbitChecker() const;

      bool isTTCStreamAligned() const;

      // Methods related to the TTC decoder (which feeds both the TTC
      // spy and the TTC stream alignment checker).
      void enableTTCDecoder() const;
      void disableTTCDecoder() const;
      bool isTTCDecoderEnabled() const;

      // Methods related to the TTC spy.
      void setTTCSpyBufferMode(tcds::pi::TTXLogHelper::LOGGING_BUFFER_MODE const val) const;
      void setTTCSpyBufferSizeToMax() const;
      void enableTTCSpy() const;
      void disableTTCSpy() const;
      void resetTTCSpy() const;
      bool isTTCSpyEnabled() const;
      bool isTTCSpyFull() const;
      uint32_t maxNumTTCSpyEntries() const;
      uint32_t numTTCSpyEntries() const;
      uint32_t readTTCSpyAddressPointer() const;
      std::vector<uint32_t> readTTCSpyRaw() const;
      std::vector<uint32_t> readTTCSpyRaw(uint32_t const from) const;
      std::vector<uint32_t> readTTCSpyRaw(uint32_t const first, uint32_t const last) const;
      void setTTCSpyLoggingLogic(tcds::pi::TCADevicePI::TTC_LOGGING_LOGIC const val) const;
      void setTTCSpyLoggingMode(tcds::pi::TCADevicePI::TTC_LOGGING_MODE const val) const;
      void setTTCSpyTriggerMask(bool const brcBC0,
                                bool const brcEC0,
                                uint8_t const brcVal0,
                                uint8_t const brcVal1,
                                uint8_t const brcVal2,
                                uint8_t const brcVal3,
                                uint8_t const brcVal4,
                                uint8_t const brcVal5,
                                uint8_t const brcDDDD,
                                uint8_t brcTT,
                                bool const brcDDDDAll,
                                bool const brcTTAll,
                                bool const brcAll,
                                bool const addAll,
                                bool const l1a,
                                bool const brcZeroData,
                                bool const adrZeroData,
                                bool const errCom) const;

      // Methods related to the TTS logger.
      void setTTSLogBufferMode(tcds::pi::TTXLogHelper::LOGGING_BUFFER_MODE const val) const;
      void setTTSLogBufferSizeToMax() const;
      void enableTTSLog() const;
      void disableTTSLog() const;
      uint32_t resetTTSLog() const;
      bool isTTSLogEnabled() const;
      bool isTTSLogFull() const;
      uint32_t maxNumTTSLogEntries() const;
      uint32_t numTTSLogEntries() const;
      uint32_t readTTSLogAddressPointer() const;
      std::vector<uint32_t> readTTSLogRaw() const;
      std::vector<uint32_t> readTTSLogRaw(uint32_t const from) const;
      std::vector<uint32_t> readTTSLogRaw(uint32_t const first, uint32_t const last) const;

      // std::string readSFPVendorName() const;

    protected:
      virtual std::string regNamePrefixImpl() const;

      virtual bool bootstrapDoneImpl() const;
      virtual void runBootstrapImpl() const;

      virtual tcds::hwlayer::DeviceBase::RegContentsVec readHardwareConfigurationImpl(tcds::hwlayer::RegisterInfo::RegInfoVec const& regInfos) const;

    private:
      TTXLogHelper ttcLogger_;
      TTXLogHelper ttsLogger_;

    };

  } // namespace pi
} // namespace tcds

#endif // _tcds_pi_TCADevicePI_h_
