#ifndef _tcds_pi_TTCSpyEntryRaw_h_
#define _tcds_pi_TTCSpyEntryRaw_h_

#include <cstddef>
#include <stdint.h>
#include <string>
#include <vector>

namespace tcds {
  namespace pi {

    class TTCSpyEntryRaw
    {

    public:

      // The spy log is organized in 4K entries of 32-bit words, with
      // each entry occupying 4 words mapped as follows.
      // - Memory location [4*entry+0] -> log[31:0]
      // - Memory location [4*entry+1] -> log[63:32]
      // - Memory location [4*entry+2] -> log[95:64]
      // - Memory location [4*entry+3] -> log[127:96]

      // ########################
      // Short, broadcast command (16 bits).
      // ########################

      // 00TTDDDDEBHHHHH1:
      // T -> test command, 2 bits.
      // D -> command/data, 4 bits.
      // E -> event counter reset, 1 bit.
      // B -> bunch counter reset, 1 bit.
      // H -> hamming code, 5 bits.

      // ########################
      // Long, addressed command (42 bits).
      // ########################

      // 01aaaaaaaaaaaaaae1ssssssssddddddddhhhhhhh1:
      // a -> ttcrx address, 14 bits.
      // e -> internal(0)/external(1), 1 bit.
      // s -> subaddress, 8 bits.
      // d -> data, 8 bits.
      // h -> hamming code, 7 bits.

      // ########################
      // LOG entry
      // ########################

      // data[123:112] <- BX number
      // data[111:64] <- orbit number

      // data[63]     <- reserved

      // data[62:49]  <- aaaaaaaaaaaaaa
      // data[48]     <- e
      // data[47:40]  <- ssssssss
      // data[39:32]  <- dddddddd

      // data[31:29]  <- reserved

      // data[28]     <- error communication
      // data[24]     <- error double bit
      // data[20]     <- error single bit
      // data[16]     <- l1accept

      // data[12]     <- long addressed command strobe (only when trigger_adr_all is set to 1)

      // data[8]      <- short broadcast strobe        (only when trigger_brc_all is set to 1)

      // data[7:6]    <- TT
      // data[5:2]    <- DDDD
      // data[1]      <- E
      // data[0]      <- B

      // This class was designed to handle TTCSpy log entries. The
      // number of words per entry is based on the 32-bit definition
      // of an IPbus word.
      static size_t const kNumBitsPerLogEntry = 128;
      static size_t const kNumWordsPerLogEntry = kNumBitsPerLogEntry / 32;

      TTCSpyEntryRaw(std::vector<uint32_t> const dataIn);

      std::string rawDataAsString() const;
      std::string type() const;

      uint64_t orbitNumber() const;
      uint16_t bxNumber() const;

      // Some type checker functions for the TTC signal in this entry.
      bool isL1A() const;
      bool isBCntRes() const;
      bool isEvCntRes() const;
      bool isLongCommand() const;
      bool isShortCommand() const;
      bool isLongExternal() const;
      bool hasLongCommandStrobeSet() const;
      bool hasShortCommandStrobeSet() const;
      bool hasSngErrStrSet() const;
      bool hasDblErrStrSet() const;
      bool hasCommErrorBitSet() const;
      uint8_t longData() const;
      uint16_t longAddress() const;
      uint8_t longSubAddress() const;
      uint8_t shortData() const;

    private:

      // The following define the various pieces of a log entry.
      static size_t const kBitPosBXNumHi = 123;
      static size_t const kBitPosBXNumLo = 112;
      static size_t const kBitPosOrbitHi = 111;
      static size_t const kBitPosOrbitLo = 64;
      static size_t const kBitPosLongAddressHi = 62;
      static size_t const kBitPosLongAddressLo = 49;
      static size_t const kBitPosLongExternalFlag = 48;
      static size_t const kBitPosLongSubAddressHi = 47;
      static size_t const kBitPosLongSubAddressLo = 40;
      static size_t const kBitPosLongDataHi = 39;
      static size_t const kBitPosLongDataLo = 32;
      static size_t const kBitPosCommError = 28;
      static size_t const kBitPosSingleBitError = 24;
      static size_t const kBitPosDoubleBitError = 20;
      static size_t const kBitPosL1Accept = 16;
      static size_t const kBitPosLongCommandStrobe = 12;
      static size_t const kBitPosShortCommandStrobe = 8;
      static size_t const kBitPosShortUserDataHi = 7;
      static size_t const kBitPosShortUserDataLo = 6;
      static size_t const kBitPosShortSystemDataHi = 5;
      static size_t const kBitPosShortSystemDataLo = 2;
      static size_t const kBitPosShortEvCntRes = 1;
      static size_t const kBitPosShortBCntRes = 0;

      bool isBitSet(size_t const bitNum) const;
      std::vector<bool> getBit(size_t const bitNum) const;
      std::vector<bool> getBits(size_t const bitNumLo,
                                size_t const bitNumHi) const;

      std::vector<uint32_t> rawData_;
      std::vector<bool> data_;

    };

  } // namespace pi
} // namespace tcds

#endif // _tcds_pi_TTCSpyEntryRaw_h_
