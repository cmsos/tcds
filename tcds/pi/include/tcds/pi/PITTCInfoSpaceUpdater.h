#ifndef _tcds_pi_PITTCInfoSpaceUpdater_h_
#define _tcds_pi_PITTCInfoSpaceUpdater_h_

#include "tcds/utils/HwInfoSpaceUpdaterBase.h"

namespace tcds {
  namespace utils {
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace pi {

    class TCADevicePI;

    class PITTCInfoSpaceUpdater : public tcds::utils::HwInfoSpaceUpdaterBase
    {

    public:
      PITTCInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                            TCADevicePI const& hw);
      virtual ~PITTCInfoSpaceUpdater();

    protected:
      tcds::pi::TCADevicePI const& getHw() const;

    };

  } // namespace pi
} // namespace tcds

#endif // _tcds_pi_PITTCInfoSpaceUpdater_h_
