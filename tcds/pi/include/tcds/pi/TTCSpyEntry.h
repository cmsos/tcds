#ifndef _tcds_pi_TTCSpyEntry_h_
#define _tcds_pi_TTCSpyEntry_h_

#include <stdint.h>
#include <string>

namespace tcds {
  namespace pi {

    /**
     * This is basically a wrapper around the TTCSpyEntryRaw class. Each
     * TTCSpyEntryRaw represents a single entry in the TTCSpy log
     * memory. But each such an entry can contain both an L1A and a
     * B-command. So they have to be split into 'lines' that can be
     * shown. After that, they have to be time-shifted etc. in order
     * to make the TTCSpy results match the workings of a TTCrx
     * decoder ASIC. That's what this class does.
     */
    class TTCSpyEntry
    {

    public:

      // An L1A and a B-go are defined to be in the same BX when the
      // two corresponding strobe pins on a TTCrx decoder chip strobe
      // at the same time. The below number is added to each logged
      // B-command timestamp (actually: to anything that is not an
      // L1A) in order to make such signals also appear in the same BX
      // in the spy log. (I.e., this calibrate the spy timing.)
      static uint64_t const kTTCSpyAToBChannelOffset = 3;

      enum TTC_TRAFFIC_TYPE {TTC_TRAFFIC_TYPE_L1A,
                             TTC_TRAFFIC_TYPE_BCOMMAND_SHORT,
                             TTC_TRAFFIC_TYPE_BCOMMAND_LONG};

      // Constructor for L1As.
      TTCSpyEntry(TTC_TRAFFIC_TYPE const trafficType,
                  uint64_t const orbitNumber,
                  uint16_t const bxNumber,
                  bool const hasSingleBitError,
                  bool const hasDoubleBitError,
                  bool const hasFrameError);

      // Constructor for short B-commands.
      TTCSpyEntry(TTC_TRAFFIC_TYPE const trafficType,
                  uint64_t const orbitNumber,
                  uint16_t const bxNumber,
                  bool const hasSingleBitError,
                  bool const hasDoubleBitError,
                  bool const hasFrameError,
                  uint8_t const data);

      // Constructor for long B-commands.
      TTCSpyEntry(TTC_TRAFFIC_TYPE const trafficType,
                  uint64_t const orbitNumber,
                  uint16_t const bxNumber,
                  bool const hasSingleBitError,
                  bool const hasDoubleBitError,
                  bool const hasFrameError,
                  uint8_t const data,
                  uint16_t const address,
                  uint8_t const subAddress,
                  bool const addressExternal);

      ~TTCSpyEntry();

      bool operator==(TTCSpyEntry const& entry) const
      {
        // NOTE: This is a rather arbitrary definition of equality,
        // mostly useful in the context of the less-than operator when
        // used for sorting. (See also below.)
        return ((orbitNumber() == entry.orbitNumber()) &&
                (bxNumber() == entry.bxNumber()) &&
                ((isL1A() && entry.isL1A()) ||
                 (isBCommandShort() && entry.isBCommandShort()) ||
                 (isBCommandLong() && entry.isBCommandLong())));
      }

      // NOTE: Sorting these entries is not always a good idea. BC0s
      // and OC0s act on the internal state of the TTCSpy, affecting
      // the BX and orbit counts. This will confuse the sorting.
      bool operator<(TTCSpyEntry const& entry) const
      {
        if (*this == entry) {
          return false;
        }
        else
          {
            // Sort by orbit number and BX number, A-channel before
            // B-channel.
            if (orbitNumber() != entry.orbitNumber())
              {
                return (orbitNumber() < entry.orbitNumber());
              }
            else
              {
                if (bxNumber() != entry.bxNumber())
                  {
                    return (bxNumber() < entry.bxNumber());
                  }
                else
                  {
                    // NOTE: Given the way the TTC works, in this case we
                    // must be dealing with an A-channel/B-channel pair.
                    return isL1A();
                  }
              }
          }
      }

      bool isL1A() const;
      bool isBCommand() const;
      bool isBCommandShort() const;
      bool isBCommandLong() const;
      bool isBCntRes() const;
      bool isEvCntRes() const;

      std::string type() const;
      uint64_t orbitNumber() const;
      uint16_t bxNumber() const;
      std::string flags() const;
      uint8_t data() const;
      uint16_t address() const;
      uint8_t subAddress() const;
      bool isAddressExternal() const;

      bool hasSingleBitError() const;
      bool hasDoubleBitError() const;
      bool hasFrameError() const;

    private:

      int fixBXRange(int const bxIn) const;

      TTC_TRAFFIC_TYPE trafficType_;

      uint64_t orbitNumber_;
      uint16_t bxNumber_;

      // The data is only used for B-commands.
      uint8_t data_;

      // The address and sub-address are only used for long
      // B-commands.
      uint16_t address_;
      uint8_t subAddress_;
      bool addressExternal_;

      bool hasSingleBitError_;
      bool hasDoubleBitError_;
      bool hasFrameError_;

    };

  } // namespace pi
} // namespace tcds

#endif // _tcds_pi_TTCSpyEntry_h_
