#ifndef _tcds_pi_ConfigurationInfoSpaceHandler_h_
#define _tcds_pi_ConfigurationInfoSpaceHandler_h_

#include <string>

#include "tcds/hwutilstca/ConfigurationInfoSpaceHandlerTCA.h"
#include "tcds/utils/InfoSpaceHandler.h"

namespace tcds {
  namespace utils {
    class Monitor;
  }
}

namespace xdaq {
  class Application;
}

namespace tcds {
  namespace pi {

    /**
     * This class really only adds the FED labels to the 'normal'
     * ConfigurationInfoSpaceHandlerTCA.
     */
    class ConfigurationInfoSpaceHandler : public tcds::hwutilstca::ConfigurationInfoSpaceHandlerTCA
    {

    public:
      ConfigurationInfoSpaceHandler(xdaq::Application& xdaqApp);
      virtual ~ConfigurationInfoSpaceHandler();

    protected:
      virtual void registerItemSetsWithMonitor(tcds::utils::Monitor& monitor);

      virtual std::string formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const;

    };

  } // namespace pi
} // namespace tcds

#endif // _tcds_pi_ConfigurationInfoSpaceHandler_h_
