#ifndef _tcds_pi_PIDAQLoop_h_
#define _tcds_pi_PIDAQLoop_h_

#include <memory>
#include <stdint.h>
#include <string>
#include <sys/types.h>
#include <vector>

#include "toolbox/exception/Listener.h"
#include "toolbox/lang/Class.h"

#include "tcds/pi/TTCSpyContents.h"
#include "tcds/pi/TTCSpyEntry.h"
#include "tcds/pi/TTSLogEntry.h"
#include "tcds/pi/TTSLogHistory.h"
#include "tcds/pi/TTSLogWriter.h"
#include "tcds/utils/Lock.h"

namespace toolbox {
  namespace task {
    class ActionSignature;
    class WorkLoop;
  }
}

namespace xcept {
  class Exception;
}

namespace tcds {
  namespace pi {
    class PIController;
  }
}

namespace tcds {
  namespace pi {

    // This is a dedicated DAQ loop that reads the contents of the
    // TTCSpy and TTSLogger memories in the PI. These tasks represent
    // more stringent requirements on the read-out than simple
    // monitoring does. Hence the 'true DAQ loop.'

    // NOTE: It is assumed that the PIDAQLoop is only started after
    // the PIController has connected to the hardware.
    class PIDAQLoop :
      public toolbox::exception::Listener,
      public toolbox::lang::Class
    {

    public:
      PIDAQLoop(tcds::pi::PIController& piController);
      virtual ~PIDAQLoop();

      // The toolbox::exception::Listener callback.
      virtual void onException(xcept::Exception& err);

      // Acquisition loop (start/stop) control methods.
      void start(bool const enableTTSHistoryLogging);
      void stop();

      // NOTE: As far as implementation is concerned the TTC spy and
      // the TTS log are very similar. In terms of usage, though, they
      // are quite different. The TTS log is supposed to take data
      // undisturbed during a whole run, and to never reset/change
      // configuration in between. The TTC spy on the other hand can
      // be reconfigured, in which case the history up to the
      // reconfiguration should be cleared.

      // Clear the history (both TTC and TTS).
      void clearHistory();
      // Clear the TTC spy history.
      void clearTTCSpyHistory();
      // Clear the TTS log history.
      void clearTTSLogHistory();

      // Snapshots help us get the data to the monitoring
      // InfoSpaceHandlers.
      tcds::pi::TTCSpyContents ttcSnapshot() const;
      tcds::pi::TTSLogHistory ttsSnapshot() const;

    private:
      static const useconds_t kLoopRelaxTime = 100000;

      std::string const piDAQAlarmName_;
      tcds::pi::PIController& piController_;
      std::unique_ptr<toolbox::task::ActionSignature> daqWorkLoopActionP_;
      std::unique_ptr<toolbox::task::WorkLoop> daqWorkLoopP_;
      std::string workLoopName_;

      // A little helper for enabling/disabling the TTS history
      // logging in/out of DAQ runs.
      bool isTTSHistoryLoggingEnabled_;

      // The most recent entries in the TTC spy, to be shown in the
      // monitoring.
      tcds::pi::TTCSpyContents ttcSpyHistory_;

      // The most recent entries in the TTS log, to be shown in the
      // monitoring.
      tcds::pi::TTSLogHistory ttsLogHistory_;

      // A helper to persist the TTS log to file.
      tcds::pi::TTSLogWriter ttsLogWriter_;

      mutable tcds::utils::Lock lock_;

      uint32_t ttcSpyAddressPointer_;
      uint32_t ttsLogAddressPointer_;
      bool isFirstRound_;

      bool isHwConnected() const;

      // Main update method. This handles both TTC and TTS.
      bool update(toolbox::task::WorkLoop* workLoop);

      // Update the contents of the TTCSpy memory from the hardware.
      void updateTTCInfo();

      // Update the contents of the TTSLogger memory from the
      // hardware.
      void updateTTSInfo();

      // Process (a) data line(s) (i.e., (a) true log entr(y)(ies)).
      void addTTCData(std::vector<uint32_t> const& rawData);

      // Process (a) data line(s) (i.e., (a) true log entr(y)(ies)).
      void addTTSData(std::vector<uint32_t> const& rawData);

      // Process a comment line (i.e., a 'derived' entry)
      void addTTSComment(std::string const& comment);

      // Add a single TTC spy line.
      void addTTCEntry(tcds::pi::TTCSpyEntry const& logEntry);

      // Add a single TTS log line.
      void addTTSEntry(tcds::pi::TTSLogEntry const& logEntry);

      std::vector<tcds::pi::TTCSpyEntry> convertRawTTCSpyData(std::vector<uint32_t> const& rawData) const;
      std::vector<tcds::pi::TTSLogEntry> convertRawTTSLogData(std::vector<uint32_t> const& rawData) const;

    };

  } // namespace pi
} // namespace tcds

#endif // _tcds_pi_PIDAQLoop_h_
