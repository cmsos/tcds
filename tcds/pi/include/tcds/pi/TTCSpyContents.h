#ifndef _tcds_pi_TTCSpyContents_h_
#define _tcds_pi_TTCSpyContents_h_

#include <deque>
#include <stddef.h>
#include <stdint.h>
#include <string>
#include <utility>
#include <vector>

#include "tcds/pi/TTCSpyEntryRaw.h"
#include "tcds/pi/TTCSpyEntry.h"

namespace tcds {
  namespace utils {
    class ConfigurationInfoSpaceHandler;
  }
}

namespace tcds {
  namespace pi {

    std::pair<uint64_t, uint16_t> deltaEntries(tcds::pi::TTCSpyEntry const& lhs,
                                               tcds::pi::TTCSpyEntry const& rhs);

    class TTCSpyContents
    {

    public:
      TTCSpyContents();
      ~TTCSpyContents();

      // Configuration manipulation.
      void updateConfiguration(tcds::utils::ConfigurationInfoSpaceHandler const& cfgInfoSpace);

      void addEntry(tcds::pi::TTCSpyEntry const& logLine);
      void clear();

      std::string getJSONString() const;

    private:
      size_t histSize_;

      // These are the raw entries from the hardware.
      std::vector<TTCSpyEntryRaw> entriesRaw_;
      // These are the 'unpacked' entries, corrected for timing etc.
      std::deque<TTCSpyEntry> entries_;

      void setHistSize(size_t const histSize);

      size_t numEntries() const;
      void trim();

      std::string formatTimestamp(uint64_t const timestamp) const;

    };

  } // namespace pi
} // namespace tcds

#endif // _tcds_pi_TTCSpyContents_h_
