#ifndef _tcds_pi_Definitions_h_
#define _tcds_pi_Definitions_h_

#include <stdint.h>
#include <string>

namespace tcds {
  namespace definitions {

    enum LPM_SOURCE {LPM_SOURCE_PRIMARY=0,
                     LPM_SOURCE_SECONDARY=1};
    enum TTS_SOURCE {TTS_SOURCE_RJ45=0,
                     TTS_SOURCE_FED1=1,
                     TTS_SOURCE_FED2=2,
                     TTS_SOURCE_FED3=3,
                     TTS_SOURCE_FED4=4,
                     TTS_SOURCE_FED5=5,
                     TTS_SOURCE_FED6=6,
                     TTS_SOURCE_FED7=7,
                     TTS_SOURCE_FED8=8,
                     TTS_SOURCE_FED9=9,
                     TTS_SOURCE_FED10=10,
                     TTS_SOURCE_OUTPUT_FROM_OR=11,
                     TTS_SOURCE_OUTPUT_FROM_FORCE_REGISTER=12,
                     TTS_SOURCE_OUTPUT_FROM_PI=13};
    TTS_SOURCE const TTS_SOURCE_MIN = TTS_SOURCE_RJ45;
    TTS_SOURCE const TTS_SOURCE_MAX = TTS_SOURCE_FED10;
    TTS_SOURCE const TTS_CHANNEL_MIN = TTS_SOURCE_RJ45;
    TTS_SOURCE const TTS_CHANNEL_MAX = TTS_SOURCE_OUTPUT_FROM_PI;

    // The PI TTS output mode. The output either propagates the output
    // of the TTS OR, or it can be forced to a fixed value.
    enum PI_TTS_OUTPUT_MODE {
      PI_TTS_OUTPUT_MODE_FORCED = 0,
      PI_TTS_OUTPUT_MODE_OR = 1
    };

    // The number of new-style, AMC13-based FEDs with connected to the
    // PI.
    unsigned int const kNumFEDs = 10;
    unsigned int const kFEDNumMin = 1;
    unsigned int const kFEDNumMax = kNumFEDs;

    // The 'configuration mode' of the PIController.
    enum PICONTROLLER_CONFIG_MODE {
      // For legacy systems connected via an FMM to the RJ45 TTS input
      // on the PI.
      PICONTROLLER_CONFIG_MODE_LEGACY_FMM = 0,
      // For new-style systems using AMC13 optical TTS links to the
      // PI.
      PICONTROLLER_CONFIG_MODE_AMC13 = 1,
      // Mixed mode. Combines the best of both worlds.
      PICONTROLLER_CONFIG_MODE_MIXED = 2,
      // Special mode for PIs that do not contribute to TTS. For
      // example: BRIL partitions, secondary PIs in partitions that
      // require more than ten TTC fibers, etc.
      PICONTROLLER_CONFIG_MODE_NOTTS = 3
    };

    // A way to describe what triggered the most recent orbit-counter
    // reset in the TTS log.
    enum OC0_REASON {
      OC0_REASON_UNKNOWN = 0,
      // It can be a general reset of the firmware block.
      OC0_REASON_RESET = 1,
      // It can be triggered by the programmed TTC broadcast command.
      OC0_REASON_BGO = 2,
      // It can be triggered by the software.
      OC0_REASON_SW = 3,
      // For pure comments there is no (applicable) OC0 reason.
      OC0_REASON_NA = 4
    };

  } // namespace definitions
} // namespace tcds

#endif // _tcds_pi_Definitions_h_
