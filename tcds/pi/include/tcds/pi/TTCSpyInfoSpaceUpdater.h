#ifndef _tcds_pi_TTCSpyInfoSpaceUpdater_h_
#define _tcds_pi_TTCSpyInfoSpaceUpdater_h_

#include "tcds/utils/HwInfoSpaceUpdaterBase.h"

namespace tcds {
  namespace utils {
    class InfoSpaceHandler;
    class InfoSpaceItem;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace pi {

    class PIDAQLoop;
    class TCADevicePI;

    class TTCSpyInfoSpaceUpdater : public tcds::utils::HwInfoSpaceUpdaterBase
    {

    public:
      TTCSpyInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                             TCADevicePI const& hw,
                             tcds::pi::PIDAQLoop const& piDAQLoop);
      virtual ~TTCSpyInfoSpaceUpdater();

    protected:
      virtual bool updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                       tcds::utils::InfoSpaceHandler* const infoSpaceHandler);

      tcds::pi::TCADevicePI const& getHw() const;

    private:
      tcds::pi::PIDAQLoop const& piDAQLoop_;

    };

  } // namespace pi
} // namespace tcds

#endif // _tcds_pi_TTCSpyInfoSpaceUpdater_h_
