#include "tcds/lpm/HwStatusInfoSpaceHandler.h"

#include <stdint.h>

#include "tcds/pm/Definitions.h"
#include "tcds/pm/Utils.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/Utils.h"
#include "tcds/utils/WebServer.h"

tcds::lpm::HwStatusInfoSpaceHandler::HwStatusInfoSpaceHandler(xdaq::Application& xdaqApp,
                                                              tcds::utils::InfoSpaceUpdater* updater) :
  InfoSpaceHandler(xdaqApp, "tcds-hw-status-lpm", updater)
{
  // Hardware status.
  createBool("ttc_clock_stable", false, "true/false", tcds::utils::InfoSpaceItem::PROCESS);
  createBool("ttc_clock_up", false, "true/false", tcds::utils::InfoSpaceItem::PROCESS);
  createUInt32("main.bc_alignment_status.orbit_marker_absent_count");
  createUInt32("main.bc_alignment_status.bc_counter_realignment_count");
  createUInt32("main.bc_alignment_status.system_bc_counter_realignment_count");

  // The overall partition controller state (in the firmware).
  createUInt32("pm_state", 0, "", tcds::utils::InfoSpaceItem::PROCESS);
}

tcds::lpm::HwStatusInfoSpaceHandler::~HwStatusInfoSpaceHandler()
{
}

void
tcds::lpm::HwStatusInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  // Hardware status.
  std::string itemSetName = "itemset-hw-status";
  monitor.newItemSet(itemSetName);

  // Clock monitoring.
  monitor.addItem(itemSetName,
                  "ttc_clock_stable",
                  "TTC clock stable",
                  this);
  monitor.addItem(itemSetName,
                  "ttc_clock_up",
                  "TTC clock PLL locked",
                  this);

  // Orbit monitoring.
  monitor.addItem(itemSetName,
                  "main.bc_alignment_status.orbit_marker_absent_count",
                  "Orbit (main and system) realignment count - orbit marker absent",
                  this);
  monitor.addItem(itemSetName,
                  "main.bc_alignment_status.bc_counter_realignment_count",
                  "Main orbit realignment count - orbit marker too early/late",
                  this);
  monitor.addItem(itemSetName,
                  "main.bc_alignment_status.system_bc_counter_realignment_count",
                  "System orbit realignment count - orbit marker too early/late",
                  this);

  // Partition manager (firmware) status.
  itemSetName = "itemset-pm-state";
  monitor.newItemSet(itemSetName);
  monitor.addItem(itemSetName,
                  "pm_state",
                  "Running state",
                  this);
}

void
tcds::lpm::HwStatusInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                   tcds::utils::Monitor& monitor,
                                                                   std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "Hardware status" : forceTabName;

  webServer.registerTab(tabName,
                        "Hardware information",
                        3);
  webServer.registerTable("Hardware status",
                          "Hardware status info",
                          monitor,
                          "itemset-hw-status",
                          tabName);
  webServer.registerTable("Running state",
                          "Firmware PM running state",
                          monitor,
                          "itemset-pm-state",
                          tabName);
}

std::string
tcds::lpm::HwStatusInfoSpaceHandler::formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const
{
  std::string res = tcds::utils::escapeAsJSONString(tcds::utils::InfoSpaceHandler::kInvalidItemString);
  if (item->isValid())
    {
      std::string name = item->name();
      if (name == "pm_state")
        {
          uint32_t const value = getUInt32(name);
          tcds::definitions::PM_STATE const valueEnum =
            static_cast<tcds::definitions::PM_STATE>(value);
          res = tcds::utils::escapeAsJSONString(tcds::pm::pmStateToString(valueEnum));
        }
      else
        {
          // For everything else simply call the generic formatter.
          res = InfoSpaceHandler::formatItem(item);
        }
    }
  return res;
}
