#include "tcds/lpm/TCADeviceLPM.h"

#include <cassert>
#include <cinttypes>
#include <memory>
#include <ostream>

#include "toolbox/string.h"

#include "tcds/hwlayertca/BootstrapHelper.h"
#include "tcds/hwlayertca/TCACarrierBase.h"
#include "tcds/hwlayertca/TCACarrierFC7.h"
#include "tcds/lpm/Definitions.h"

tcds::lpm::TCADeviceLPM::TCADeviceLPM() :
  tcds::pm::TCADevicePMCommonBase(std::unique_ptr<tcds::hwlayertca::TCACarrierBase>(new tcds::hwlayertca::TCACarrierFC7(hwDevice_))),
  ipmNumber_(0)
{
}

tcds::lpm::TCADeviceLPM::~TCADeviceLPM()
{
}

unsigned short
tcds::lpm::TCADeviceLPM::ipmNumber() const
{
  return ipmNumber_;
}

void
tcds::lpm::TCADeviceLPM::setIPMNumber(unsigned short const ipmNumber)
{
  ipmNumber_ = ipmNumber;
}

std::string
tcds::lpm::TCADeviceLPM::regNamePrefixImpl() const
{
  // All the iPM registers start with 'ipmX.', where 'X' is the iPM
  // number in the LPM (i.e., [1, 2]).
  std::stringstream regNamePrefix;
  regNamePrefix << "ipm" << ipmNumber() << ".";
  return regNamePrefix.str();
}

bool
tcds::lpm::TCADeviceLPM::bootstrapDoneImpl() const
{
  tcds::hwlayertca::BootstrapHelper h(*this);
  return h.bootstrapDone();
}

void
tcds::lpm::TCADeviceLPM::runBootstrapImpl() const
{
  tcds::hwlayertca::BootstrapHelper h(*this);
  h.runBootstrap();
  // Reset the backplane link to the CPM.
  hwDevice_.writeRegister("lpm_main.backplane_tts_control.gtx_reset", 0x0);
  hwDevice_.writeRegister("lpm_main.backplane_tts_control.gtx_reset", 0x1);
  hwDevice_.writeRegister("lpm_main.backplane_tts_control.gtx_reset", 0x0);
}

void
tcds::lpm::TCADeviceLPM::configureTriggerAlignment() const
{
  // This one is not really trigger alignment, but it aligns the BX
  // number reported for each trigger in the DAQ event record.
  std::string regName = "main.orbit_alignment_config.daq_record_adjust";
  writeRegister(regName, 0xd70);

  //----------

  // BX-alignment for cyclic triggers.
  regName = "main.int_fw_alignment_settings.cyclic_trigger_adjust";
  writeRegister(regName, 0x7a);

  // BX-alignment for the bunch-mask.
  regName = "main.int_fw_alignment_settings.bunch_mask_adjust";
  writeRegister(regName, 0x7b);

  // BX-alignment for the sequence triggers.
  regName = "bgo_trains.bgo_train_config.calib_trigger_adjust";
  writeRegister(regName, 0x7b);
}

void
tcds::lpm::TCADeviceLPM::configureExternalTriggerSettings(uint32_t const accessibleTriggerInput) const
{
  // NOTE: This is a bit of a gimmick. Since the software does not
  // really have access to any registers above ipm.*, and the external
  // trigger input/polarity selection is done in lpm_main.control, the
  // software writes several flags in ipm.main. This method reads
  // these flags and propagates these settings to lpm_main.control.

  std::string const trigNum = toolbox::toString("%" PRIu32, accessibleTriggerInput);
  std::string const otherTrigNum = (accessibleTriggerInput == 0) ? "1" : "0";

  // Trigger source.
  uint32_t const tmpSrc =
    readRegister("main.lpm_external_trigger" + trigNum + "_source_mode");
  if (tmpSrc == 0)
    {
      hwDevice_.writeRegister("lpm_main.control.external_l1a_" + trigNum + "_source", 0x0);
      hwDevice_.writeRegister("lpm_main.control.invert_external_l1a_" + trigNum, 0x1);
    }
  else
    {
      hwDevice_.writeRegister("lpm_main.control.external_l1a_" + trigNum + "_source", 0x1);
      hwDevice_.writeRegister("lpm_main.control.invert_external_l1a_" + trigNum, 0x0);
    }

  // Trigger polarity inversion flag.
  // NOTE: This is with respect to the default applied above.
  uint32_t const tmpPol =
    readRegister("main.lpm_external_trigger" + trigNum + "_inversion_mode");
  if (tmpPol != 0)
    {
      uint32_t const tmp =
        hwDevice_.readRegister("lpm_main.control.invert_external_l1a_" + trigNum);
      bool const invOri = (tmp == 1);
      hwDevice_.writeRegister("lpm_main.control.invert_external_l1a_" + trigNum, !invOri);
    }

  // Trigger 'copy' flag.
  std::string const regNameFrom =
    "main.lpm_external_trigger" + trigNum + "_follows_trigger" + otherTrigNum;
  uint32_t const tmpFollow = readRegister(regNameFrom);
  std::string const regNameTo =
    "lpm_main.control.external_l1a_" + trigNum + "_follows_external_l1a_" + otherTrigNum;
  hwDevice_.writeRegister(regNameTo, tmpFollow);
}

void
tcds::lpm::TCADeviceLPM::disableExternalTrigger(unsigned const inaccessibleTriggerInput) const
{
  std::string const trigNum = toolbox::toString("%" PRIu32, inaccessibleTriggerInput);
  std::string const regName = "main.inselect.external_trigger" + trigNum + "_enable";
  writeRegister(regName, 0x0);
}

tcds::definitions::LPM_EXTERNAL_TRIGGER_SOURCE_MODE
tcds::lpm::TCADeviceLPM::readExternalTriggerSourceMode(uint32_t const trigNum) const
{
  tcds::definitions::LPM_EXTERNAL_TRIGGER_SOURCE_MODE res =
    tcds::definitions::LPM_EXTERNAL_TRIGGER_SOURCE_UNKNOWN;

  uint32_t const otherTrigNum = (trigNum == 0) ? 1 : 0;

  std::string const regName =
    toolbox::toString("lpm_main.control.external_l1a_%" PRId32"_follows_external_l1a_%" PRId32, trigNum, otherTrigNum);
  uint32_t const tmp = hwDevice_.readRegister(regName);
  if (tmp == 1)
    {
      if (trigNum == 0)
        {
          res = tcds::definitions::LPM_EXTERNAL_TRIGGER_SOURCE_COPIED_FROM_1;
        }
      else
        {
          res = tcds::definitions::LPM_EXTERNAL_TRIGGER_SOURCE_COPIED_FROM_0;
        }
    }
  else
    {
      std::string const regName =
        toolbox::toString("lpm_main.control.external_l1a_%" PRId32 "_source", trigNum);
      uint32_t const tmp = hwDevice_.readRegister(regName);
      if (tmp == 0)
        {
          res = tcds::definitions::LPM_EXTERNAL_TRIGGER_SOURCE_NIM;
        }
      else
        {
          res = tcds::definitions::LPM_EXTERNAL_TRIGGER_SOURCE_TTL;
        }
    }

  return res;
}

tcds::definitions::LPM_EXTERNAL_TRIGGER_INVERSION_MODE
tcds::lpm::TCADeviceLPM::readExternalTriggerInversionMode(uint32_t const trigNum) const
{
  std::string const regName =
    toolbox::toString("lpm_main.control.invert_external_l1a_%" PRId32, trigNum);
  uint32_t const tmp =  hwDevice_.readRegister(regName);
  return static_cast<tcds::definitions::LPM_EXTERNAL_TRIGGER_INVERSION_MODE>(tmp);
}

tcds::definitions::LPM_EXTERNAL_TRIGGER_LOGIC_MODE
tcds::lpm::TCADeviceLPM::readExternalTriggerLogicMode(uint32_t const trigNum) const
{
  tcds::definitions::LPM_EXTERNAL_TRIGGER_LOGIC_MODE res;

  tcds::definitions::LPM_EXTERNAL_TRIGGER_SOURCE_MODE srcMode =
    readExternalTriggerSourceMode(trigNum);
  tcds::definitions::LPM_EXTERNAL_TRIGGER_INVERSION_MODE invMode =
    readExternalTriggerInversionMode(trigNum);

  if (srcMode == tcds::definitions::LPM_EXTERNAL_TRIGGER_SOURCE_NIM)
    {
      if (invMode == tcds::definitions::LPM_EXTERNAL_TRIGGER_INVERSION_ON)
        {
          res = tcds::definitions::LPM_EXTERNAL_TRIGGER_LOGIC_ACTIVE_HI;
        }
      else
        {
          res = tcds::definitions::LPM_EXTERNAL_TRIGGER_LOGIC_ACTIVE_LO;
        }
    }
  else
    {
      if (invMode == tcds::definitions::LPM_EXTERNAL_TRIGGER_INVERSION_OFF)
        {
          res = tcds::definitions::LPM_EXTERNAL_TRIGGER_LOGIC_ACTIVE_HI;
        }
      else
        {
          res = tcds::definitions::LPM_EXTERNAL_TRIGGER_LOGIC_ACTIVE_LO;
        }
    }

  return res;
}
