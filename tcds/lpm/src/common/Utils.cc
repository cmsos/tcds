#include "tcds/lpm/Utils.h"

#include "toolbox/string.h"
#include "xcept/Exception.h"

#include "tcds/exception/Exception.h"

std::string
tcds::lpm::externalTriggerSrcModeToString(tcds::definitions::LPM_EXTERNAL_TRIGGER_SOURCE_MODE const srcMode)
{
  std::string res = "unknown";

  switch (srcMode)
    {
    case tcds::definitions::LPM_EXTERNAL_TRIGGER_SOURCE_UNKNOWN:
      res = "unknown";
      break;
    case tcds::definitions::LPM_EXTERNAL_TRIGGER_SOURCE_NIM:
      res = "'tr' input (NIM)";
      break;
    case tcds::definitions::LPM_EXTERNAL_TRIGGER_SOURCE_TTL:
      res = "'aux' input (LVTTL)";
      break;
    case tcds::definitions::LPM_EXTERNAL_TRIGGER_SOURCE_COPIED_FROM_0:
      res = "copied from external trigger 0";
      break;
    case tcds::definitions::LPM_EXTERNAL_TRIGGER_SOURCE_COPIED_FROM_1:
      res = "copied from external trigger 1";
      break;
    default:
      XCEPT_RAISE(tcds::exception::ValueError,
                  toolbox::toString("%d is not a valid LPM external-trigger source mode.",
                                    srcMode));
      break;
    }
  return res;
}

std::string
tcds::lpm::externalTriggerInvModeToString(tcds::definitions::LPM_EXTERNAL_TRIGGER_INVERSION_MODE const invMode)
{
  std::string res = "unknown";

  switch (invMode)
    {
    case tcds::definitions::LPM_EXTERNAL_TRIGGER_INVERSION_OFF:
      res = "off";
      break;
    case tcds::definitions::LPM_EXTERNAL_TRIGGER_INVERSION_ON:
      res = "on";
      break;
    default:
      XCEPT_RAISE(tcds::exception::ValueError,
                  toolbox::toString("%d is not a valid LPM external-trigger inversion mode.",
                                    invMode));
      break;
    }
  return res;
}

std::string
tcds::lpm::externalTriggerLogicModeToString(tcds::definitions::LPM_EXTERNAL_TRIGGER_LOGIC_MODE const logicMode)
{
  std::string res = "unknown";

  switch (logicMode)
    {
    case tcds::definitions::LPM_EXTERNAL_TRIGGER_LOGIC_ACTIVE_HI:
      res = "active-high";
      break;
    case tcds::definitions::LPM_EXTERNAL_TRIGGER_LOGIC_ACTIVE_LO:
      res = "active-low";
      break;
    default:
      XCEPT_RAISE(tcds::exception::ValueError,
                  toolbox::toString("%d is not a valid LPM external-trigger logic mode.",
                                    logicMode));
      break;
    }
  return res;
}
