#include "tcds/lpm/HwStatusInfoSpaceUpdater.h"

#include <stdint.h>
#include <string>

#include "tcds/lpm/TCADeviceLPM.h"
#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/InfoSpaceItem.h"

tcds::lpm::HwStatusInfoSpaceUpdater::HwStatusInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                                              tcds::lpm::TCADeviceLPM const& hw) :
  tcds::utils::HwInfoSpaceUpdaterBase(xdaqApp, hw),
  hw_(hw)
{
}

tcds::lpm::HwStatusInfoSpaceUpdater::~HwStatusInfoSpaceUpdater()
{
}

bool
tcds::lpm::HwStatusInfoSpaceUpdater::updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                                         tcds::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  bool updated = false;
  tcds::lpm::TCADeviceLPM const& hw = getHw();
  if (hw.isReadyForUse())
    {
      std::string name = item.name();
      tcds::utils::InfoSpaceItem::UpdateType updateType = item.updateType();
      if (updateType == tcds::utils::InfoSpaceItem::PROCESS)
        {
          // The 'PROCESS' update type means that there is something
          // special to the variable. Figure out what to do based on the
          // variable name.
          if (name == "ttc_clock_up")
            {
              bool const newVal = hw.isTTCClockUp();
              infoSpaceHandler->setBool(name, newVal);
              updated = true;
            }
          else if (name == "ttc_clock_stable")
            {
              bool const newVal = hw.isTTCClockStable();
              infoSpaceHandler->setBool(name, newVal);
              updated = true;
            }
          else if (name == "pm_state")
            {
              uint32_t newVal = hw.getPMState();
              infoSpaceHandler->setUInt32(name, newVal);
              updated = true;
            }
        }
      if (!updated)
        {
          updated = tcds::utils::HwInfoSpaceUpdaterBase::updateInfoSpaceItem(item, infoSpaceHandler);
        }
    }

  if (updated)
    {
      item.setValid();
    }
  else
    {
      item.setInvalid();
    }

  return updated;
}

tcds::lpm::TCADeviceLPM const&
tcds::lpm::HwStatusInfoSpaceUpdater::getHw() const
{
  return hw_;
}
