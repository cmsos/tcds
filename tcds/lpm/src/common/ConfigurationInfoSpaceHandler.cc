#include "tcds/lpm/ConfigurationInfoSpaceHandler.h"

#include <cstddef>
#include <string>

#include "toolbox/string.h"

#include "tcds/hwutilstca/SFPMonitoringConfigHelpers.h"
#include "tcds/pm/Definitions.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/Utils.h"

tcds::lpm::ConfigurationInfoSpaceHandler::ConfigurationInfoSpaceHandler(xdaq::Application& xdaqApp) :
  tcds::hwutilstca::ConfigurationInfoSpaceHandlerTCA(xdaqApp)
{
  createUInt32("ipmNumber", 1, "", tcds::utils::InfoSpaceItem::NOUPDATE, true);

  //----------

  // The FED id number (twelve bits, strictly speaking) to be used in
  // the DAQ link.
  createUInt32("fedId",
               0,
               "",
               tcds::utils::InfoSpaceItem::NOUPDATE,
               true);

  // The FED vector.
  createString("fedEnableMask",
               "",
               "",
               tcds::utils::InfoSpaceItem::NOUPDATE,
               true);

  // The TTC partition map.
  createString("ttcPartitionMap",
               "",
               "",
               tcds::utils::InfoSpaceItem::NOUPDATE,
               true);

  //----------

  // Configurable access to/control over the two LPM external trigger
  // inputs.
  createBool("controlsExternalTrigger0",
             false,
             "enabled/disabled",
             tcds::utils::InfoSpaceItem::NOUPDATE,
             true);
  createBool("controlsExternalTrigger1",
             false,
             "enabled/disabled",
             tcds::utils::InfoSpaceItem::NOUPDATE,
             true);

  //----------

  // Text labels for each of the TTS inputs.
  // For the iCIs.
  for (size_t iciNum = 1; iciNum <= tcds::definitions::kNumICIsPerLPM; ++iciNum)
    {
      createString(toolbox::toString("partitionLabelICI%d", iciNum),
                   "",
                   "",
                   tcds::utils::InfoSpaceItem::NOUPDATE,
                   true);
    }
  // For the APVEs.
  for (size_t apveNum = 1; apveNum <= tcds::definitions::kNumAPVEsPerLPM; ++apveNum)
    {
      createString(toolbox::toString("partitionLabelAPVE%d", apveNum),
                   "",
                   "",
                   tcds::utils::InfoSpaceItem::NOUPDATE,
                   true);
    }

  //----------

  // SFP monitoring parameters.
  tcds::hwutilstca::SFPMonitoringConfigCreateItems(this);
}

void
tcds::lpm::ConfigurationInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  tcds::hwutilstca::ConfigurationInfoSpaceHandlerTCA::registerItemSetsWithMonitor(monitor);

  std::string itemSetName = "Application configuration";

  // The internal iPM number.
  monitor.addItem("Application configuration",
                  "ipmNumber",
                  "iPM number",
                  this);

  // The FED id.
  monitor.addItem(itemSetName,
                  "fedId",
                  "FED id",
                  this);

  //----------

  // Configurable access to/control over the two LPM external trigger
  // inputs.
  monitor.addItem(itemSetName,
                  "controlsExternalTrigger0",
                  "Access to external trigger 0",
                  this,
                  "This refers to the TR0/AUXA input pair");
  monitor.addItem(itemSetName,
                  "controlsExternalTrigger1",
                  "Access to external trigger 1",
                  this,
                  "This refers to the TR1/AUXB input pair");

  //----------

  // Run configuration parameters (from RCMS).
  itemSetName = "Run configuration";

  // The FED vector.
  monitor.addItem(itemSetName,
                  "fedEnableMask",
                  "FED vector",
                  this);

  // The TTC partition map.
  monitor.addItem(itemSetName,
                  "ttcPartitionMap",
                  "TTC partition map",
                  this);

  // SFP monitoring parameters.
  tcds::hwutilstca::SFPMonitoringRegisterItemsWithMonitor(this, monitor);
}

std::string
tcds::lpm::ConfigurationInfoSpaceHandler::formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const
{
  std::string res = tcds::utils::escapeAsJSONString(tcds::utils::InfoSpaceHandler::kInvalidItemString);
  if (item->isValid())
    {
      std::string name = item->name();
      if (name == "fedEnableMask" || name == "ttcPartitionMap")
        {
          // These things can become annoyingly long. The idea here is
          // to insert some zero-width spaces in order to make it
          // break into pieces when formatted in the web interface.
          // NOTE: Not very efficient/pretty, but it works.
          std::string const valRaw = getString(name);
          std::string const valNew = tcds::utils::chunkifyFEDVector(valRaw);
          res = tcds::utils::escapeAsJSONString(valNew);
        }
      else
        {
          // For everything else simply call the generic formatter.
          res = InfoSpaceHandler::formatItem(item);
        }
    }
  return res;
}
