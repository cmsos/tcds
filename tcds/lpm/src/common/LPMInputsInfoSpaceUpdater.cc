#include "tcds/lpm/LPMInputsInfoSpaceUpdater.h"

#include <stdint.h>
#include <string>

#include "tcds/lpm/TCADeviceLPM.h"
#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Utils.h"

tcds::lpm::LPMInputsInfoSpaceUpdater::LPMInputsInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                                                TCADeviceLPM const& hw) :
  tcds::utils::HwInfoSpaceUpdaterBase(xdaqApp, hw),
  hw_(hw)
{
}

bool
tcds::lpm::LPMInputsInfoSpaceUpdater::updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                                          tcds::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  TCADeviceLPM const& hw = getHw();
  bool updated = false;
  if (hw.isHwConnected())
    {
      std::string name = item.name();
      tcds::utils::InfoSpaceItem::UpdateType updateType = item.updateType();
      if (updateType == tcds::utils::InfoSpaceItem::PROCESS)
        {
          // The 'PROCESS' update type means that there is something
          // special to the variable. Figure out what to do based on the
          // variable name.
          if (name == "random_trigger_rate")
            {
              uint32_t const newVal = hw.readRandomTriggerRate();
              infoSpaceHandler->setUInt32(name, newVal);
              updated = true;
            }
          else if (name == "bunch_mask_trigger_pattern_size")
            {
              std::vector<uint16_t> tmp = hw.readBunchMaskTriggerBXs();
              uint32_t const newVal = tmp.size();
              infoSpaceHandler->setUInt32(name, newVal);
              updated = true;
            }
          else if (name == "bunch_mask_trigger_pattern")
            {
              std::vector<uint16_t> bxList = hw.readBunchMaskTriggerBXs();
              std::vector<std::pair<uint16_t, uint16_t> > bxRanges =
                tcds::utils::groupBXListIntoRanges(bxList);
              std::string const newVal =  tcds::utils::formatBXRangeList(bxRanges);
              infoSpaceHandler->setString(name, newVal);
              updated = true;
            }
          else if ((name == "main.lpm_external_trigger0_source_mode") ||
                   (name == "main.lpm_external_trigger1_source_mode"))
            {
              // Special registers with special data sources.
              uint32_t newVal = 0;
              if (name == "main.lpm_external_trigger0_source_mode")
                {
                  newVal = hw.readExternalTriggerSourceMode(0);
                }
              else
                {
                  newVal = hw.readExternalTriggerSourceMode(1);
                }
              infoSpaceHandler->setUInt32(name, newVal);
              updated = true;
            }
          else if ((name == "main.lpm_external_trigger0_logic_mode") ||
                   (name == "main.lpm_external_trigger1_logic_mode"))
            {
              // Special registers with special data sources.
              uint32_t newVal = 0;
              if (name == "main.lpm_external_trigger0_logic_mode")
                {
                  newVal = hw.readExternalTriggerLogicMode(0);
                }
              else
                {
                  newVal = hw.readExternalTriggerLogicMode(1);
                }
              infoSpaceHandler->setUInt32(name, newVal);
              updated = true;
            }
        }
      if (!updated)
        {
          updated = tcds::utils::HwInfoSpaceUpdaterBase::updateInfoSpaceItem(item, infoSpaceHandler);
        }
    }

  if (updated)
    {
      item.setValid();
    }
  else
    {
      item.setInvalid();
    }

  return updated;
}

tcds::lpm::TCADeviceLPM const&
tcds::lpm::LPMInputsInfoSpaceUpdater::getHw() const
{
  return hw_;
}
