#include "tcds/lpm/LPMController.h"

#include <cinttypes>
#include <stdint.h>
#include <string>

#include "toolbox/string.h"
#include "xcept/Exception.h"
#include "xdaq/exception/ApplicationInstantiationFailed.h"
#include "xgi/Method.h"

#include "tcds/exception/Exception.h"
#include "tcds/hwlayer/DeviceBase.h"
#include "tcds/hwlayer/RegisterInfo.h"
#include "tcds/hwutilstca/CyclicGensInfoSpaceHandler.h"
#include "tcds/hwutilstca/CyclicGensInfoSpaceUpdater.h"
#include "tcds/hwutilstca/HwIDInfoSpaceHandlerTCA.h"
#include "tcds/hwutilstca/HwIDInfoSpaceUpdaterTCA.h"
#include "tcds/hwutilstca/SFPInfoSpaceUpdater.h"
#include "tcds/hwutilstca/Utils.h"
#include "tcds/lpm/ConfigurationInfoSpaceHandler.h"
#include "tcds/lpm/HwStatusInfoSpaceHandler.h"
#include "tcds/lpm/HwStatusInfoSpaceUpdater.h"
#include "tcds/lpm/LPMInputsInfoSpaceHandler.h"
#include "tcds/lpm/LPMInputsInfoSpaceUpdater.h"
#include "tcds/lpm/SFPInfoSpaceHandler.h"
#include "tcds/lpm/TCADeviceLPM.h"
#include "tcds/lpm/version.h"
#include "tcds/pm/ActionsInfoSpaceHandler.h"
#include "tcds/pm/ActionsInfoSpaceUpdater.h"
#include "tcds/pm/CountersInfoSpaceHandler.h"
#include "tcds/pm/CountersInfoSpaceUpdater.h"
#include "tcds/pm/DAQInfoSpaceHandler.h"
#include "tcds/pm/DAQInfoSpaceUpdater.h"
#include "tcds/pm/Definitions.h"
#include "tcds/pm/ReTriInfoSpaceHandler.h"
#include "tcds/pm/ReTriInfoSpaceUpdater.h"
#include "tcds/pm/SchedulingInfoSpaceHandler.h"
#include "tcds/pm/SchedulingInfoSpaceUpdater.h"
#include "tcds/pm/SequencesInfoSpaceHandler.h"
#include "tcds/pm/SequencesInfoSpaceUpdater.h"
#include "tcds/pm/TTSCountersInfoSpaceHandler.h"
#include "tcds/pm/TTSCountersInfoSpaceUpdater.h"
#include "tcds/pm/TTSInfoSpaceHandler.h"
#include "tcds/pm/TTSInfoSpaceUpdater.h"
#include "tcds/utils/LogMacros.h"
#include "tcds/utils/SOAPUtils.h"
#include "tcds/utils/Utils.h"
#include "tcds/utils/XDAQAppBase.h"

XDAQ_INSTANTIATOR_IMPL(tcds::lpm::LPMController)

tcds::lpm::LPMController::LPMController(xdaq::ApplicationStub* stub)
try
  :
  tcds::utils::XDAQAppWithFSMForPMs(stub, std::unique_ptr<tcds::hwlayer::DeviceBase>(new tcds::lpm::TCADeviceLPM())),
    soapCmdDisableCyclicGenerator_(*this),
    soapCmdDisableRandomTriggers_(*this),
    soapCmdDumpHardwareState_(*this),
    soapCmdEnableCyclicGenerator_(*this),
    soapCmdEnableRandomTriggers_(*this),
    soapCmdInitCyclicGenerator_(*this),
    soapCmdInitCyclicGenerators_(*this),
    soapCmdReadHardwareConfiguration_(*this),
    soapCmdSendBgo_(*this),
    soapCmdSendBgoTrain_(*this),
    soapCmdSendL1A_(*this),
    soapCmdSendL1APattern_(*this),
    soapCmdStopL1APattern_(*this),
    controllerHelper_(*this, getHw())
    {
      // Create the InfoSpace holding all configuration information.
      cfgInfoSpaceP_ =
        std::unique_ptr<tcds::lpm::ConfigurationInfoSpaceHandler>(new tcds::lpm::ConfigurationInfoSpaceHandler(*this));

      // Make sure the correct default hardware configuration file is found.
      cfgInfoSpaceP_->setString("defaultHwConfigurationFilePath",
                                "${XDAQ_ROOT}/etc/tcds/lpm/hw_cfg_default_lpm.txt");
    }
catch (tcds::exception::Exception const& err)
  {
    std::string msgBase = "Something went wrong instantiating the LPMController application";
    std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.what());
    XCEPT_RAISE(xdaq::exception::ApplicationInstantiationFailed, msg.c_str());
  }

tcds::lpm::LPMController::~LPMController()
{
  hwRelease();
}

void
tcds::lpm::LPMController::setupInfoSpaces()
{
  // Make sure the configuration settings are up-to-date.
  cfgInfoSpaceP_->readInfoSpace();

  // Instantiate all hardware-dependent InfoSpaceHandlers and InfoSpaceUpdaters.
  actionsInfoSpaceUpdaterP_ = std::unique_ptr<tcds::pm::ActionsInfoSpaceUpdater>(new tcds::pm::ActionsInfoSpaceUpdater(*this, getHw()));
  actionsInfoSpaceP_ =
    std::unique_ptr<tcds::pm::ActionsInfoSpaceHandler>(new tcds::pm::ActionsInfoSpaceHandler(*this, actionsInfoSpaceUpdaterP_.get(), false));
  countersInfoSpaceUpdaterP_ = std::unique_ptr<tcds::pm::CountersInfoSpaceUpdater>(new tcds::pm::CountersInfoSpaceUpdater(*this, getHw()));
  countersInfoSpaceP_ =
    std::unique_ptr<tcds::pm::CountersInfoSpaceHandler>(new tcds::pm::CountersInfoSpaceHandler(*this, countersInfoSpaceUpdaterP_.get()));
  cyclicGensInfoSpaceUpdaterP_ = std::unique_ptr<tcds::hwutilstca::CyclicGensInfoSpaceUpdater>(new tcds::hwutilstca::CyclicGensInfoSpaceUpdater(*this, getHw()));
  cyclicGensInfoSpaceP_ =
    std::unique_ptr<tcds::hwutilstca::CyclicGensInfoSpaceHandler>(new tcds::hwutilstca::CyclicGensInfoSpaceHandler(*this, cyclicGensInfoSpaceUpdaterP_.get(), tcds::definitions::kNumCyclicGensPerPM));
  daqInfoSpaceUpdaterP_ = std::unique_ptr<tcds::pm::DAQInfoSpaceUpdater>(new tcds::pm::DAQInfoSpaceUpdater(*this, getHw()));
  daqInfoSpaceP_ =
    std::unique_ptr<tcds::pm::DAQInfoSpaceHandler>(new tcds::pm::DAQInfoSpaceHandler(*this, daqInfoSpaceUpdaterP_.get()));
  hwIDInfoSpaceUpdaterP_ =
    std::unique_ptr<tcds::hwutilstca::HwIDInfoSpaceUpdaterTCA>(new tcds::hwutilstca::HwIDInfoSpaceUpdaterTCA(*this, getHw()));
  hwIDInfoSpaceP_ =
    std::unique_ptr<tcds::hwutilstca::HwIDInfoSpaceHandlerTCA>(new tcds::hwutilstca::HwIDInfoSpaceHandlerTCA(*this, hwIDInfoSpaceUpdaterP_.get()));
  hwStatusInfoSpaceUpdaterP_ =
    std::unique_ptr<tcds::lpm::HwStatusInfoSpaceUpdater>(new tcds::lpm::HwStatusInfoSpaceUpdater(*this, getHw()));
  hwStatusInfoSpaceP_ =
    std::unique_ptr<tcds::lpm::HwStatusInfoSpaceHandler>(new tcds::lpm::HwStatusInfoSpaceHandler(*this, hwStatusInfoSpaceUpdaterP_.get()));
  inputsInfoSpaceUpdaterP_ = std::unique_ptr<tcds::lpm::LPMInputsInfoSpaceUpdater>(new tcds::lpm::LPMInputsInfoSpaceUpdater(*this, getHw()));
  inputsInfoSpaceP_ =
    std::unique_ptr<tcds::lpm::LPMInputsInfoSpaceHandler>(new tcds::lpm::LPMInputsInfoSpaceHandler(*this, inputsInfoSpaceUpdaterP_.get()));
  retriInfoSpaceUpdaterP_ = std::unique_ptr<tcds::pm::ReTriInfoSpaceUpdater>(new tcds::pm::ReTriInfoSpaceUpdater(*this, getHw()));
  retriInfoSpaceP_ =
    std::unique_ptr<tcds::pm::ReTriInfoSpaceHandler>(new tcds::pm::ReTriInfoSpaceHandler(*this, retriInfoSpaceUpdaterP_.get()));
  schedulingInfoSpaceUpdaterP_ = std::unique_ptr<tcds::pm::SchedulingInfoSpaceUpdater>(new tcds::pm::SchedulingInfoSpaceUpdater(*this, getHw()));
  schedulingInfoSpaceP_ =
    std::unique_ptr<tcds::pm::SchedulingInfoSpaceHandler>(new tcds::pm::SchedulingInfoSpaceHandler(*this, schedulingInfoSpaceUpdaterP_.get()));
  sequencesInfoSpaceUpdaterP_ = std::unique_ptr<tcds::pm::SequencesInfoSpaceUpdater>(new tcds::pm::SequencesInfoSpaceUpdater(*this, getHw()));
  sequencesInfoSpaceP_ =
    std::unique_ptr<tcds::pm::SequencesInfoSpaceHandler>(new tcds::pm::SequencesInfoSpaceHandler(*this, sequencesInfoSpaceUpdaterP_.get()));
  sfpInfoSpaceUpdaterP_ =
    std::unique_ptr<tcds::hwutilstca::SFPInfoSpaceUpdater>(new tcds::hwutilstca::SFPInfoSpaceUpdater(*this, getHw()));
  sfpInfoSpaceP_ =
    std::unique_ptr<SFPInfoSpaceHandler>(new SFPInfoSpaceHandler(*this, sfpInfoSpaceUpdaterP_.get()));
  ttsCountersInfoSpaceUpdaterP_ = std::unique_ptr<tcds::pm::TTSCountersInfoSpaceUpdater>(new tcds::pm::TTSCountersInfoSpaceUpdater(*this, getHw()));
  ttsCountersInfoSpaceP_ =
    std::unique_ptr<tcds::pm::TTSCountersInfoSpaceHandler>(new tcds::pm::TTSCountersInfoSpaceHandler(*this, ttsCountersInfoSpaceUpdaterP_.get(), false));
  ttsInfoSpaceUpdaterP_ = std::unique_ptr<tcds::pm::TTSInfoSpaceUpdater>(new tcds::pm::TTSInfoSpaceUpdater(*this, getHw()));
  ttsInfoSpaceP_ =
    std::unique_ptr<tcds::pm::TTSInfoSpaceHandler>(new tcds::pm::TTSInfoSpaceHandler(*this, ttsInfoSpaceUpdaterP_.get(), false));

  // Register all InfoSpaceItems with the Monitor.
  cfgInfoSpaceP_->registerItemSets(monitor_, webServer_);
  appStateInfoSpace_.registerItemSets(monitor_, webServer_);
  hwIDInfoSpaceP_->registerItemSets(monitor_, webServer_);
  hwStatusInfoSpaceP_->registerItemSets(monitor_, webServer_);
  sfpInfoSpaceP_->registerItemSets(monitor_, webServer_);
  inputsInfoSpaceP_->registerItemSets(monitor_, webServer_);
  schedulingInfoSpaceP_->registerItemSets(monitor_, webServer_);
  sequencesInfoSpaceP_->registerItemSets(monitor_, webServer_);
  actionsInfoSpaceP_->registerItemSets(monitor_, webServer_);
  cyclicGensInfoSpaceP_->registerItemSets(monitor_, webServer_);
  retriInfoSpaceP_->registerItemSets(monitor_, webServer_);
  ttsInfoSpaceP_->registerItemSets(monitor_, webServer_);
  ttsCountersInfoSpaceP_->registerItemSets(monitor_, webServer_);
  countersInfoSpaceP_->registerItemSets(monitor_, webServer_);
  daqInfoSpaceP_->registerItemSets(monitor_, webServer_);

  // Force a write of all TTS InfoSpaces. This triggers an XMAS update
  // in order to populate the flashlists such that other applications
  // can pick them up.
  ttsInfoSpaceP_->writeInfoSpace(true);
}

tcds::lpm::TCADeviceLPM&
tcds::lpm::LPMController::getHw() const
{
  return static_cast<tcds::lpm::TCADeviceLPM&>(*hwP_.get());
}

void
tcds::lpm::LPMController::hwConnectImpl()
{
  tcds::hwutilstca::tcaDeviceHwConnectImpl(*cfgInfoSpaceP_, getHw());
}

void
tcds::lpm::LPMController::hwReleaseImpl()
{
  getHw().hwRelease();
}

void
tcds::lpm::LPMController::hwCfgInitializeImpl()
{
  // Check for the presence of the TTC clock. Without TTC clock it is
  // no use continuing (and apart from that, some registers will not
  // be accessible).
  if (!(getHw().isTTCClockUp() && getHw().isTTCClockStable()))
    {
      std::string const msg = "Could not configure the hardware: no TTC clock present, or clock not stable.";
      ERROR(msg);
      XCEPT_RAISE(tcds::exception::TTCClockProblem, msg.c_str());
    }

  controllerHelper_.hwCfgInitialize();
}

void
tcds::lpm::LPMController::hwCfgFinalizeImpl()
{
  // Check for a valid orbit signal. Without that, it's no use
  // continuing.
  if (!getHw().isOrbitSignalOk())
    {
      std::string const msg = "Could not configure the hardware: no orbit signal present, or orbit signal unstable/incorrect length.";
      ERROR(msg);
      XCEPT_RAISE(tcds::exception::TTCOrbitProblem, msg.c_str());
    }

  //----------

  controllerHelper_.hwCfgFinalize();

  //----------

  // The LPM does not have an APVE, so let's disable the PM APVE TTS
  // input.
  getHw().writeRegister("main.inselect.apve_enable", 0);

  //----------

  // Configure the trigger BX-alignment registers with the right
  // values.
  getHw().configureTriggerAlignment();

  // Configure the software version, in case anyone is interested.
  getHw().setSoftwareVersion(getSwVersion());

  //----------

  // And, special for the LPM, configure the external-trigger sources.
  // NOTE: This only applies the settings, but does not handle
  // enabling/disabling.
  // NOTE: This is a post-processing (!) step, really. Internally this
  // method uses values written in a 'semaphores' register from the
  // hardware configuration file.
  for (uint32_t i = 0; i != 2; ++i)
    {
      bool const controlsExternalTrigger =
        cfgInfoSpaceP_->getBool(toolbox::toString("controlsExternalTrigger%" PRIu32, i));
      if (controlsExternalTrigger)
        {
          getHw().configureExternalTriggerSettings(i);
        }
      else
        {
          getHw().disableExternalTrigger(i);
        }
    }

  //----------

  // Enable/disable DAQ backpressure based on whether we're going to
  // be read out or not.
  std::string const fedEnableMask = cfgInfoSpaceP_->getString("fedEnableMask");
  uint32_t const fedId = cfgInfoSpaceP_->getUInt32("fedId");
  bool const useDAQLink = tcds::utils::doesFedDoDAQ(fedId, fedEnableMask);
  if (useDAQLink)
    {
      getHw().enableDAQBackpressure();
    }
  else
    {
      getHw().disableDAQBackpressure();
    }

  //----------

  // Enable/disable TTS inputs based on whether or not each of the
  // partitions is included in the run.
  std::string const ttcPartitionMap = cfgInfoSpaceP_->getString("ttcPartitionMap");

  // For all iCIs.
  for (unsigned int iciNum = 1; iciNum <= tcds::definitions::kNumICIsPerLPM; ++iciNum)
    {
      std::string const tmp = toolbox::toString("partitionLabelICI%d", iciNum);
      std::string const partitionName = cfgInfoSpaceP_->getString(tmp);
      bool const partitionDoesTTS = tcds::utils::doesPartitionDoTTS(partitionName, ttcPartitionMap);
      if (partitionDoesTTS)
        {
          getHw().enableTTSFromICI(1, iciNum);
        }
      else
        {
          getHw().disableTTSFromICI(1, iciNum);
        }
    }

  // For all APVEs.
  for (unsigned int apveNum = 1; apveNum <= tcds::definitions::kNumAPVEsPerLPM; ++apveNum)
    {
      std::string const tmp = toolbox::toString("partitionLabelAPVE%d", apveNum);
      std::string const partitionName = cfgInfoSpaceP_->getString(tmp);
      bool const partitionDoesTTS = tcds::utils::doesPartitionDoTTS(partitionName, ttcPartitionMap);
      if (partitionDoesTTS)
        {
          getHw().enableTTSFromAPVE(1, apveNum);
        }
      else
        {
          getHw().disableTTSFromAPVE(1, apveNum);
        }
    }
}

void
tcds::lpm::LPMController::coldResetActionImpl(toolbox::Event::Reference event)
{
  // Explicitly do nothing. The LPM is a shared piece of
  // hardware. Cannot just coldReset that.
}

void
tcds::lpm::LPMController::configureActionImpl(toolbox::Event::Reference event)
{
  // Extract the iPM number from the configuration and propagate that
  // into our hardware device instance.
  unsigned short const ipmNumber = cfgInfoSpaceP_->getUInt32("ipmNumber");
  getHw().setIPMNumber(ipmNumber);

  // Then do what we normally do.
  tcds::utils::XDAQAppWithFSMForPMs::configureActionImpl(event);
}

void
tcds::lpm::LPMController::enableActionImpl(toolbox::Event::Reference event)
{
  controllerHelper_.enableAction(event);
}

void
tcds::lpm::LPMController::pauseActionImpl(toolbox::Event::Reference event)
{
  controllerHelper_.pauseAction(event);
}

void
tcds::lpm::LPMController::resumeActionImpl(toolbox::Event::Reference event)
{
  controllerHelper_.resumeAction(event);
}

void
tcds::lpm::LPMController::stopActionImpl(toolbox::Event::Reference event)
{
  controllerHelper_.stopAction(event);
}

void
tcds::lpm::LPMController::ttcHardResetActionImpl(toolbox::Event::Reference event)
{
  controllerHelper_.ttcHardResetAction(event);
}

void
tcds::lpm::LPMController::ttcResyncActionImpl(toolbox::Event::Reference event)
{
  controllerHelper_.ttcResyncAction(event);
}

void
tcds::lpm::LPMController::zeroActionImpl(toolbox::Event::Reference event)
{
  controllerHelper_.zeroAction(event);

  // BUG BUG BUG
  // Force a write of all TTS InfoSpaces. This triggers an XMAS update
  // in order to populate the flashlists such that other applications
  // can pick them up.
  ttsInfoSpaceP_->writeInfoSpace(true);
  // BUG BUG BUG end
}

tcds::utils::RegCheckResult
tcds::lpm::LPMController::isRegisterAllowed(tcds::hwlayer::RegisterInfo const& regInfo) const
{
  // Start by applying the default selection.
  tcds::utils::RegCheckResult res = tcds::utils::XDAQAppBase::isRegisterAllowed(regInfo);

  // Then the XPM-common selection.
  res = (res && controllerHelper_.isRegisterAllowed(regInfo))
    ? res : tcds::utils::kRegCheckResultDisallowed;

  // There is no APVE in the LPM, so we should not fiddle with this
  // register.
  res = (res && (regInfo.name().find("inselect.apve_enable") == std::string::npos))
    ? res : tcds::utils::kRegCheckResultDisallowed;

  // Now for the fun part. There are some registers related to the
  // external triggers that, depending on the application
  // configuration, may or may not be available to us.
  for (unsigned trigNum = 0; trigNum <= 1; ++trigNum)
    {
      bool const controlsExternalTrigger =
        cfgInfoSpaceP_->getBool(toolbox::toString("controlsExternalTrigger%d", trigNum));
      if (!controlsExternalTrigger)
        {
          res = (res && (regInfo.name().find(toolbox::toString("external_trigger%d", trigNum)) ==
                         std::string::npos))
            ? res : tcds::utils::kRegCheckResultUnavailable;
        }
    }

  return res;
}

std::vector<tcds::utils::FSMSOAPParHelper>
tcds::lpm::LPMController::expectedFSMSoapParsImpl(std::string const& commandName) const
{
  // Define what we expect in terms of parameters for each SOAP FSM
  // command.
  std::vector<tcds::utils::FSMSOAPParHelper> params =
    tcds::utils::XDAQAppWithFSMForPMs::expectedFSMSoapParsImpl(commandName);

  // Configure for the takes the usual parameters as well as
  // 'xdaq:fedEnableMask' and 'xdaq:ttcPartitionMap'.
  if ((commandName == "Configure") ||
      (commandName == "Reconfigure"))
    {
      params.push_back(tcds::utils::FSMSOAPParHelper(commandName, "fedEnableMask", true));
      params.push_back(tcds::utils::FSMSOAPParHelper(commandName, "ttcPartitionMap", true));
    }
  return params;
}

void
tcds::lpm::LPMController::loadSOAPCommandParameterImpl(xoap::MessageReference const& msg,
                                                       tcds::utils::FSMSOAPParHelper const& param)
{
  // NOTE: The assumption is that when entering this method, the
  // command-to-parameter mapping, required parameter presence,
  // etc. have all been checked already. This method only concerns the
  // actual loading of the parameters.

  std::string const parName = param.parameterName();

  if (parName == "fedEnableMask")
    {
      // Import 'xdaq:fedEnableMask'.
      std::string const fedEnableMask =
        tcds::utils::soap::extractSOAPCommandParameterString(msg, parName);
      cfgInfoSpaceP_->setString(parName, fedEnableMask);
    }
  else if (parName == "ttcPartitionMap")
    {
      // Import 'xdaq:ttcPartitionMap'.
      std::string const ttcPartitionMap =
        tcds::utils::soap::extractSOAPCommandParameterString(msg, parName);
      cfgInfoSpaceP_->setString(parName, ttcPartitionMap);
    }
  else
    {
      // For other commands: use the usual approach.
      tcds::utils::XDAQAppWithFSMForPMs::loadSOAPCommandParameterImpl(msg, param);
    }
}

uint32_t
tcds::lpm::LPMController::getSwVersion() const
{
  // NOTE: No checks on ranges are made before masking. Gonna break at
  // some point...
  uint32_t const swVersion =
    ((TCDS_LPM_VERSION_MAJOR & 0xff) << 24) +
    ((TCDS_LPM_VERSION_MINOR & 0xfff) << 12) +
    (TCDS_LPM_VERSION_PATCH & 0xfff);
  return swVersion;
}
