#include "tcds/lpm/SFPInfoSpaceHandler.h"

#include <cstddef>

#include "toolbox/string.h"

#include "tcds/hwlayertca/Definitions.h"
#include "tcds/hwutilstca/SFPInfoSpaceUpdater.h"
#include "tcds/hwlayertca/Utils.h"
#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/WebServer.h"
#include "tcds/utils/XDAQAppBase.h"

tcds::lpm::SFPInfoSpaceHandler::SFPInfoSpaceHandler(tcds::utils::XDAQAppBase& xdaqApp,
                                                    tcds::hwutilstca::SFPInfoSpaceUpdater* updater) :
  tcds::hwutilstca::SFPInfoSpaceHandlerBase(xdaqApp, updater)
{
  // The SFPs connecting to the DAQ.
  for (unsigned int source = tcds::definitions::SFP_LPM_DAQ_MIN;
       source <= tcds::definitions::SFP_LPM_DAQ_MAX;
       ++source)
    {
      tcds::definitions::SFP_LPM const sourceEnum =
        static_cast<tcds::definitions::SFP_LPM>(source);
      std::string const name = tcds::hwlayertca::sfpToRegName(sourceEnum);
      std::string const infoSpaceHandlerName = toolbox::toString("tcds-sfp-info-lpm-%s",
                                                                 name.c_str());

      tcds::utils::InfoSpaceHandler* handler =
        new tcds::utils::InfoSpaceHandler(xdaqApp, infoSpaceHandlerName, updater);
      infoSpaceHandlers_[name] = handler;
      createSFPChannel(handler,
                       tcds::definitions::kSFP_TYPE_LPM_DAQ,
                       source,
                       name);
    }
}

tcds::lpm::SFPInfoSpaceHandler::~SFPInfoSpaceHandler()
{
  // Cleanup of InfoSpaceHandlers happens in the MultiInfoSpaceHandler
  // base class.
}

void
tcds::lpm::SFPInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  // The SFPs connecting to the DAQ.
  for (unsigned int source = tcds::definitions::SFP_LPM_DAQ_MIN;
       source <= tcds::definitions::SFP_LPM_DAQ_MAX;
       ++source)
    {
      tcds::definitions::SFP_LPM const sourceEnum =
        static_cast<tcds::definitions::SFP_LPM>(source);
      std::string const name = tcds::hwlayertca::sfpToShortName(sourceEnum);
      std::string const itemSetName = toolbox::toString("itemset-sfp-status-%s",
                                                        name.c_str());
      registerSFPChannel(tcds::hwlayertca::sfpToRegName(sourceEnum), itemSetName, monitor);
    }
}

void
tcds::lpm::SFPInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                              tcds::utils::Monitor& monitor,
                                                              std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "SFPs" : forceTabName;
  size_t const numSFPs = (tcds::definitions::SFP_LPM_DAQ_MAX -
                          tcds::definitions::SFP_LPM_DAQ_MIN) + 1;
  size_t const numColumns = numSFPs;

  webServer.registerTab(tabName, "SFP monitoring", numColumns);

  //----------

  // The SFPs connecting to the DAQ.
  for (unsigned int source = tcds::definitions::SFP_LPM_DAQ_MIN;
       source <= tcds::definitions::SFP_LPM_DAQ_MAX;
       ++source)
    {
      tcds::definitions::SFP_LPM const sourceEnum =
        static_cast<tcds::definitions::SFP_LPM>(source);
      std::string const name = tcds::hwlayertca::sfpToShortName(sourceEnum);
      std::string const itemSetName = toolbox::toString("itemset-sfp-status-%s",
                                                        name.c_str());
      webServer.registerTable(tcds::hwlayertca::sfpToLongName(sourceEnum),
                              "",
                              monitor,
                              itemSetName,
                              tabName);
    }
}
