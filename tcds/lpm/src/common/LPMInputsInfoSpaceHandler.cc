#include "tcds/lpm/LPMInputsInfoSpaceHandler.h"

#include <stdint.h>
#include <vector>

#include "tcds/lpm/Definitions.h"
#include "tcds/lpm/Utils.h"
#include "tcds/pm/PatternTriggerRAMContents.h"
#include "tcds/pm/WebTablePatternTriggerRAM.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/Utils.h"
#include "tcds/utils/WebServer.h"

tcds::lpm::LPMInputsInfoSpaceHandler::LPMInputsInfoSpaceHandler(xdaq::Application& xdaqApp,
                                                                tcds::utils::InfoSpaceUpdater* updater) :
  InfoSpaceHandler(xdaqApp, "tcds-lpm-inputs", updater)
{
  //----------

  // The enable/disable flags for the two front-panel trigger inputs.
  createBool("main.inselect.external_trigger0_enable", false, "enabled/disabled");
  createBool("main.inselect.external_trigger1_enable", false, "enabled/disabled");

  // Software-triggered triggers and B-gos.
  createBool("main.inselect.combined_software_trigger_and_bgo_enable", false, "enabled/disabled");

  // TTS state-triggered triggers and B-gos.
  createBool("main.inselect.combined_tts_action_trigger_and_bgo_enable", false, "enabled/disabled");

  // Cyclic triggers (and not B-gos!).
  createBool("main.inselect.cyclic_trigger_enable", false, "enabled/disabled");

  // The bunch-mask trigger generator.
  createBool("main.inselect.bunch_mask_trigger_enable", false, "enabled/disabled");

  // The random-trigger generator.
  createBool("main.inselect.random_trigger_enable", false, "enabled/disabled");

  // The pattern trigger generator.
  createBool("main.inselect.pattern_trigger_enable", false, "enabled/disabled");

  //----------

  // The source mode (i.e., NIM, LVTTL, or
  // copied-from-the-other-input) of the two external trigger inputs.
  createUInt32("main.lpm_external_trigger0_source_mode", 0, "", tcds::utils::InfoSpaceItem::PROCESS);
  createUInt32("main.lpm_external_trigger1_source_mode", 0, "", tcds::utils::InfoSpaceItem::PROCESS);

  //----------

  // The logic mode (active-low/active-high) of the two external trigger inputs.
  createUInt32("main.lpm_external_trigger0_logic_mode", 0, "", tcds::utils::InfoSpaceItem::PROCESS);
  createUInt32("main.lpm_external_trigger1_logic_mode", 0, "", tcds::utils::InfoSpaceItem::PROCESS);

  //----------

  // The delays (in BX) applied to the external trigger sources.
  createUInt32("main.trigger_delay.external_trigger0_delay");
  createUInt32("main.trigger_delay.external_trigger1_delay");

  //----------

  // L1As from the CPM (via the backplane to the LPM) as external
  // trigger source.
  createBool("main.inselect.external_trigger_from_cpm_l1a_enable", false, "enabled/disabled");

  //----------

  // Orbit source settings.
  createBool("main.inselect.cpm_orbit_select", false, "enabled/disabled");
  createBool("main.inselect.ttcmi_orbit_select", false, "enabled/disabled");
  createUInt32("main.orbit_alignment_config.orbit_offset");

  //----------

  // The requested random-trigger rate.
  createUInt32("random_trigger_rate", 0, "", tcds::utils::InfoSpaceItem::PROCESS);

  //----------

  // The trigger bunch mask.
  createUInt32("main.bunch_mask_trigger_config.prescale");
  createUInt32("main.bunch_mask_trigger_config.initial_prescale");
  createUInt32("bunch_mask_trigger_pattern_size", 0, "", tcds::utils::InfoSpaceItem::PROCESS);
  createString("bunch_mask_trigger_pattern", "", "", tcds::utils::InfoSpaceItem::PROCESS);

  //----------

  // The RAM-based pattern trigger generator (pattern).
  createBool("main.pattern_trigger_config.loop", false, "true/false");
  createBool("main.pattern_trigger_config.sync_loop", false, "true/false");
  createUInt32Vec("pattern_trigger_ram");
}

tcds::lpm::LPMInputsInfoSpaceHandler::~LPMInputsInfoSpaceHandler()
{
}

void
tcds::lpm::LPMInputsInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  // Trigger and B-go sources.
  monitor.newItemSet("itemset-inputs");
  monitor.addItem("itemset-inputs",
                  "main.inselect.external_trigger0_enable",
                  "Front-panel trigger input 0",
                  this);
  monitor.addItem("itemset-inputs",
                  "main.lpm_external_trigger0_source_mode",
                  "Front-panel trigger input 0 source",
                  this,
                  "The source of the external trigger source:"
                  " the front-panel TR0 lemo input"
                  ", the front-panel AUXA lemo input"
                  ", or a copy of front-panel trigger 1."
                  " NOTE: The 'copy' option is almost always a bad idea.");
  monitor.addItem("itemset-inputs",
                  "main.lpm_external_trigger0_logic_mode",
                  "Front-panel trigger input 0 polarity",
                  this);
  monitor.addItem("itemset-inputs",
                  "main.trigger_delay.external_trigger0_delay",
                  "Front-panel trigger input 0 delay (BX)",
                  this);
  monitor.addItem("itemset-inputs",
                  "main.inselect.external_trigger1_enable",
                  "Front-panel trigger input 1",
                  this);
  monitor.addItem("itemset-inputs",
                  "main.lpm_external_trigger1_source_mode",
                  "Front-panel trigger input 1 source",
                  this,
                  "The source of the external trigger source:"
                  " the front-panel TR1 lemo input"
                  ", the front-panel AUXB lemo input"
                  ", or a copy of front-panel trigger 0."
                  " NOTE: The 'copy' option is almost always a bad idea.");
  monitor.addItem("itemset-inputs",
                  "main.lpm_external_trigger1_logic_mode",
                  "Front-panel trigger input 1 polarity",
                  this);
  monitor.addItem("itemset-inputs",
                  "main.trigger_delay.external_trigger1_delay",
                  "Front-panel trigger input 1 delay (BX)",
                  this);
  monitor.addItem("itemset-inputs",
                  "main.inselect.external_trigger_from_cpm_l1a_enable",
                  "CPM L1As as external LPM trigger",
                  this);
  monitor.addItem("itemset-inputs",
                  "main.inselect.combined_software_trigger_and_bgo_enable",
                  "IPbus/SOAP-based triggers and B-gos",
                  this);
  monitor.addItem("itemset-inputs",
                  "main.inselect.combined_tts_action_trigger_and_bgo_enable",
                  "TTS state-driven triggers and B-gos",
                  this);
  monitor.addItem("itemset-inputs",
                  "main.inselect.cyclic_trigger_enable",
                  "Cyclic triggers",
                  this);
  monitor.addItem("itemset-inputs",
                  "main.inselect.bunch_mask_trigger_enable",
                  "BX-mask trigger generator",
                  this);
  monitor.addItem("itemset-inputs",
                  "main.inselect.random_trigger_enable",
                  "Random-trigger generator",
                  this);
  monitor.addItem("itemset-inputs",
                  "main.inselect.pattern_trigger_enable",
                  "Pattern trigger generator",
                  this);

  // Orbit sources and settings.
  monitor.newItemSet("itemset-orbitsource");
  monitor.addItem("itemset-orbitsource",
                  "main.inselect.cpm_orbit_select",
                  "Orbit source: CPM",
                  this,
                  "If the CPM is selected as orbit source,"
                  " the CPM BC0 command is used orbit as marker."
                  " Otherwise, if the TTCmi is selected as orbit source,"
                  " the the front-panel orbit input is used as orbit marker."
                  " If neither is selected, an orbit marker is generated internally.");
  monitor.addItem("itemset-orbitsource",
                  "main.inselect.ttcmi_orbit_select",
                  "Orbit source: TTCmi",
                  this,
                  "If the CPM is selected as orbit source,"
                  " the CPM BC0 command is used orbit as marker."
                  " Otherwise, if the TTCmi is selected as orbit source,"
                  " the the front-panel orbit input is used as orbit marker."
                  " If neither is selected, an orbit marker is generated internally.");
  monitor.addItem("itemset-orbitsource",
                  "main.orbit_alignment_config.orbit_offset",
                  "Orbit offset (BX)",
                  this);

  // The trigger bunch mask.
  monitor.newItemSet("itemset-trigger-mask");
  monitor.addItem("itemset-trigger-mask",
                  "main.bunch_mask_trigger_config.prescale",
                  "Prescale",
                  this);
  monitor.addItem("itemset-trigger-mask",
                  "main.bunch_mask_trigger_config.initial_prescale",
                  "Initial prescale value",
                  this);
  monitor.addItem("itemset-trigger-mask",
                  "bunch_mask_trigger_pattern_size",
                  "Number of forced-trigger BXs per orbit",
                  this);
  monitor.addItem("itemset-trigger-mask",
                  "bunch_mask_trigger_pattern",
                  "Forced-trigger BXs",
                  this);

  // The random-trigger.
  monitor.newItemSet("itemset-random-trigger");
  monitor.addItem("itemset-random-trigger",
                  "random_trigger_rate",
                  "Requested random-trigger rate (Hz)",
                  this);

  // The RAM-based pattern trigger.
  monitor.newItemSet("itemset-pattern-trigger");
  monitor.addItem("itemset-pattern-trigger",
                  "main.pattern_trigger_config.loop",
                  "Loop mode",
                  this);
  monitor.addItem("itemset-pattern-trigger",
                  "main.pattern_trigger_config.sync_loop",
                  "Synchronize loop to BC0",
                  this);
  monitor.addItem("itemset-pattern-trigger",
                  "pattern_trigger_ram",
                  "Trigger/delay pattern",
                  this);
}

void
tcds::lpm::LPMInputsInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                    tcds::utils::Monitor& monitor,
                                                                    std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "Input sources" : forceTabName;

  webServer.registerTab(tabName,
                        "General LPM information",
                        4);

  webServer.registerTable("Orbit source",
                          "Orbit source selection and configuration",
                          monitor,
                          "itemset-orbitsource",
                          tabName);

  // A spacer to enforce the layout of the HyperDAQ page.
  webServer.registerSpacer("Spacer",
                           "",
                           monitor,
                           "",
                           tabName,
                           3);

  webServer.registerTable("Trigger/B-go inputs",
                          "Trigger and B-go source selection and configuration",
                          monitor,
                          "itemset-inputs",
                          tabName);

  webServer.registerTable("BX-mask trigger",
                          "Bunch-mask trigger configuration",
                          monitor,
                          "itemset-trigger-mask",
                          tabName);

  webServer.registerTable("Random trigger",
                          "Random-trigger configuration",
                          monitor,
                          "itemset-random-trigger",
                          tabName);

  webServer.registerWebObject<tcds::pm::WebTablePatternTriggerRAM>("Pattern trigger",
                                                                   "Pattern trigger configuration",
                                                                   monitor,
                                                                   "itemset-pattern-trigger",
                                                                   tabName);
}

std::string
tcds::lpm::LPMInputsInfoSpaceHandler::formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const
{
  // Specialized formatting for the external-trigger source/inversion
  // mode enums.
  std::string res = tcds::utils::escapeAsJSONString(tcds::utils::InfoSpaceHandler::kInvalidItemString);
  if (item->isValid())
    {
      std::string name = item->name();

      if ((name == "main.lpm_external_trigger0_source_mode") ||
          (name == "main.lpm_external_trigger1_source_mode"))
        {
          // External-trigger source modes.
          uint32_t const value = getUInt32(name);
          tcds::definitions::LPM_EXTERNAL_TRIGGER_SOURCE_MODE const valueEnum =
            static_cast<tcds::definitions::LPM_EXTERNAL_TRIGGER_SOURCE_MODE>(value);
          res = tcds::utils::escapeAsJSONString(tcds::lpm::externalTriggerSrcModeToString(valueEnum));
        }
      else if ((name == "main.lpm_external_trigger0_logic_mode") ||
               (name == "main.lpm_external_trigger1_logic_mode"))
        {
          // External-trigger logic modes.
          uint32_t const value = getUInt32(name);
          tcds::definitions::LPM_EXTERNAL_TRIGGER_LOGIC_MODE const valueEnum =
            static_cast<tcds::definitions::LPM_EXTERNAL_TRIGGER_LOGIC_MODE>(value);
          res = tcds::utils::escapeAsJSONString(tcds::lpm::externalTriggerLogicModeToString(valueEnum));
        }
      else if (name == "pattern_trigger_ram")
        {
          std::vector<uint32_t> const ramContentsRaw = getUInt32Vec(name);
          tcds::pm::PatternTriggerRAMContents const ramContents(ramContentsRaw);
          res = ramContents.getJSONString();
        }
      else
        {
          // For everything else simply call the generic formatter.
          res = InfoSpaceHandler::formatItem(item);
        }
    }
  return res;
}
