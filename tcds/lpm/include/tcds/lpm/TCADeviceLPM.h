#ifndef _tcds_ici_TCADeviceLPM_h_
#define _tcds_ici_TCADeviceLPM_h_

#include <stdint.h>
#include <string>

#include "tcds/lpm/Definitions.h"
#include "tcds/pm/TCADevicePMCommonBase.h"

namespace tcds {
  namespace lpm {

    /**
     * Implementation of Local Partition Manager (LPM)
     * functionality. The LPM is based on the FC7 FMC carrier board.
     */
    class TCADeviceLPM : public tcds::pm::TCADevicePMCommonBase
    {

    public:

      /**
       * @note
       * The TCADeviceLPM need an iPM number. The TCADeviceLPM
       * connects to the physical LPM hardware, but only acts upon the
       * iPM (firmware) instance with the given number. The caveat is
       * that at the time the TCADeviceLPM object is instantiated in
       * the LPMController, the XDAQ configuration has not yet been
       * loaded, so the iPM number is not yet known. Hence the (ugly)
       * setIPMNumber() method.
       */

      TCADeviceLPM();
      virtual ~TCADeviceLPM();

      unsigned short ipmNumber() const;
      void setIPMNumber(unsigned short const iciNumber);

      void configureTriggerAlignment() const;
      void configureExternalTriggerSettings(unsigned const accessibleTriggerInput) const;
      void disableExternalTrigger(unsigned const inaccessibleTriggerInput) const;
      tcds::definitions::LPM_EXTERNAL_TRIGGER_SOURCE_MODE readExternalTriggerSourceMode(uint32_t const trigNum) const;
      tcds::definitions::LPM_EXTERNAL_TRIGGER_INVERSION_MODE readExternalTriggerInversionMode(uint32_t const trigNum) const;
      tcds::definitions::LPM_EXTERNAL_TRIGGER_LOGIC_MODE readExternalTriggerLogicMode(uint32_t const trigNum) const;

    protected:
      virtual std::string regNamePrefixImpl() const;

      virtual bool bootstrapDoneImpl() const;
      virtual void runBootstrapImpl() const;

    private:
      unsigned short ipmNumber_;

    };

  } // namespace lpm
} // namespace tcds

#endif // _tcds_ici_TCADeviceLPM_h_
