#ifndef _tcds_lpm_Definitions_h_
#define _tcds_lpm_Definitions_h_

namespace tcds {
  namespace definitions {

    enum LPM_EXTERNAL_TRIGGER_SOURCE_MODE {
      LPM_EXTERNAL_TRIGGER_SOURCE_UNKNOWN = 0,
      LPM_EXTERNAL_TRIGGER_SOURCE_NIM,
      LPM_EXTERNAL_TRIGGER_SOURCE_TTL,
      LPM_EXTERNAL_TRIGGER_SOURCE_COPIED_FROM_0,
      LPM_EXTERNAL_TRIGGER_SOURCE_COPIED_FROM_1
    };

    enum LPM_EXTERNAL_TRIGGER_INVERSION_MODE {
      LPM_EXTERNAL_TRIGGER_INVERSION_OFF = 0,
      LPM_EXTERNAL_TRIGGER_INVERSION_ON = 1
    };

    enum LPM_EXTERNAL_TRIGGER_LOGIC_MODE {
      LPM_EXTERNAL_TRIGGER_LOGIC_ACTIVE_HI = 0,
      LPM_EXTERNAL_TRIGGER_LOGIC_ACTIVE_LO = 1
    };

  } // namespace definitions
} // namespace tcds

#endif // _tcds_lpm_Definitions_h_
