#ifndef _tcds_lpm_Utils_h_
#define _tcds_lpm_Utils_h_

#include <string>

#include "tcds/lpm/Definitions.h"

namespace tcds {
  namespace lpm {

    // Mapping of external trigger source/inversion modes to strings.
    std::string externalTriggerSrcModeToString(tcds::definitions::LPM_EXTERNAL_TRIGGER_SOURCE_MODE const srcMode);
    std::string externalTriggerInvModeToString(tcds::definitions::LPM_EXTERNAL_TRIGGER_INVERSION_MODE const invMode);

    // Mapping of external trigger logic modes to strings.
    std::string externalTriggerLogicModeToString(tcds::definitions::LPM_EXTERNAL_TRIGGER_LOGIC_MODE const logicMode);

  } // namespace lpm
} // namespace tcds

#endif // _tcds_lpm_Utils_h_
