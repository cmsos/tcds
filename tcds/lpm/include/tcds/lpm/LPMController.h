#ifndef _tcds_lpm_LPMController_h_
#define _tcds_lpm_LPMController_h_

#include <memory>
#include <stdint.h>
#include <string>
#include <vector>

#include "toolbox/Event.h"
#include "xdaq/Application.h"
#include "xoap/MessageReference.h"

#include "tcds/lpm/TCADeviceLPM.h"
#include "tcds/pm/PMControllerHelper.h"
#include "tcds/utils/FSMSOAPParHelper.h"
#include "tcds/utils/RegCheckResult.h"
#include "tcds/utils/SOAPCmdBase.h"
#include "tcds/utils/SOAPCmdDisableCyclicGenerator.h"
#include "tcds/utils/SOAPCmdDisableRandomTriggers.h"
#include "tcds/utils/SOAPCmdDumpHardwareState.h"
#include "tcds/utils/SOAPCmdEnableCyclicGenerator.h"
#include "tcds/utils/SOAPCmdEnableRandomTriggers.h"
#include "tcds/utils/SOAPCmdInitCyclicGenerator.h"
#include "tcds/utils/SOAPCmdInitCyclicGenerators.h"
#include "tcds/utils/SOAPCmdReadHardwareConfiguration.h"
#include "tcds/utils/SOAPCmdSendBgo.h"
#include "tcds/utils/SOAPCmdSendBgoTrain.h"
#include "tcds/utils/SOAPCmdSendL1A.h"
#include "tcds/utils/SOAPCmdSendL1APattern.h"
#include "tcds/utils/SOAPCmdStopL1APattern.h"
#include "tcds/utils/XDAQAppWithFSMForPMs.h"

namespace xdaq {
  class ApplicationStub;
}

namespace xgi {
  class Input;
  class Output;
}

namespace tcds {
  namespace hwlayer {
    class RegisterInfo;
    }
}

namespace tcds {
  namespace hwutilstca {
    class CyclicGensInfoSpaceHandler;
    class CyclicGensInfoSpaceUpdater;
    class HwIDInfoSpaceHandlerTCA;
    class HwIDInfoSpaceUpdaterTCA;
    class SFPInfoSpaceUpdater;
  }
}

namespace tcds {
  namespace pm {
    class ActionsInfoSpaceHandler;
    class ActionsInfoSpaceUpdater;
    class CountersInfoSpaceHandler;
    class CountersInfoSpaceUpdater;
    class DAQInfoSpaceHandler;
    class DAQInfoSpaceUpdater;
    class ReTriInfoSpaceHandler;
    class ReTriInfoSpaceUpdater;
    class SchedulingInfoSpaceHandler;
    class SchedulingInfoSpaceUpdater;
    class SequencesInfoSpaceHandler;
    class SequencesInfoSpaceUpdater;
    class TTSCountersInfoSpaceHandler;
    class TTSCountersInfoSpaceUpdater;
    class TTSInfoSpaceHandler;
    class TTSInfoSpaceUpdater;
  }
}

namespace tcds {
  namespace lpm {

    class HwStatusInfoSpaceHandler;
    class HwStatusInfoSpaceUpdater;
    class LPMInputsInfoSpaceHandler;
    class LPMInputsInfoSpaceUpdater;
    class SFPInfoSpaceHandler;

    class LPMController : public tcds::utils::XDAQAppWithFSMForPMs
    {

    public:
      XDAQ_INSTANTIATOR();

      LPMController(xdaq::ApplicationStub* stub);
      virtual ~LPMController();

    protected:
      virtual void setupInfoSpaces();

      /**
       * Access the hardware pointer as TCADeviceLPM&.
       */
      virtual tcds::lpm::TCADeviceLPM& getHw() const;

      virtual void hwConnectImpl();
      virtual void hwReleaseImpl();

      virtual void hwCfgInitializeImpl();
      virtual void hwCfgFinalizeImpl();

      virtual void coldResetActionImpl(toolbox::Event::Reference event);
      virtual void configureActionImpl(toolbox::Event::Reference event);
      virtual void enableActionImpl(toolbox::Event::Reference event);
      virtual void pauseActionImpl(toolbox::Event::Reference event);
      virtual void resumeActionImpl(toolbox::Event::Reference event);
      virtual void stopActionImpl(toolbox::Event::Reference event);
      virtual void ttcHardResetActionImpl(toolbox::Event::Reference event);
      virtual void ttcResyncActionImpl(toolbox::Event::Reference event);
      virtual void zeroActionImpl(toolbox::Event::Reference event);

      virtual tcds::utils::RegCheckResult isRegisterAllowed(tcds::hwlayer::RegisterInfo const& regInfo) const;

      virtual std::vector<tcds::utils::FSMSOAPParHelper> expectedFSMSoapParsImpl(std::string const& commandName) const;
      virtual void loadSOAPCommandParameterImpl(xoap::MessageReference const& msg,
                                                tcds::utils::FSMSOAPParHelper const& param);

    private:
      uint32_t getSwVersion() const;

      // Various InfoSpaces and their InfoSpaceUpdaters.
      std::unique_ptr<tcds::pm::ActionsInfoSpaceUpdater> actionsInfoSpaceUpdaterP_;
      std::unique_ptr<tcds::pm::ActionsInfoSpaceHandler> actionsInfoSpaceP_;
      std::unique_ptr<tcds::pm::CountersInfoSpaceUpdater> countersInfoSpaceUpdaterP_;
      std::unique_ptr<tcds::pm::CountersInfoSpaceHandler> countersInfoSpaceP_;
      std::unique_ptr<tcds::hwutilstca::CyclicGensInfoSpaceUpdater> cyclicGensInfoSpaceUpdaterP_;
      std::unique_ptr<tcds::hwutilstca::CyclicGensInfoSpaceHandler> cyclicGensInfoSpaceP_;
      std::unique_ptr<tcds::pm::DAQInfoSpaceUpdater> daqInfoSpaceUpdaterP_;
      std::unique_ptr<tcds::pm::DAQInfoSpaceHandler> daqInfoSpaceP_;
      std::unique_ptr<tcds::hwutilstca::HwIDInfoSpaceUpdaterTCA> hwIDInfoSpaceUpdaterP_;
      std::unique_ptr<tcds::hwutilstca::HwIDInfoSpaceHandlerTCA> hwIDInfoSpaceP_;
      std::unique_ptr<tcds::lpm::HwStatusInfoSpaceUpdater> hwStatusInfoSpaceUpdaterP_;
      std::unique_ptr<tcds::lpm::HwStatusInfoSpaceHandler> hwStatusInfoSpaceP_;
      std::unique_ptr<tcds::lpm::LPMInputsInfoSpaceUpdater> inputsInfoSpaceUpdaterP_;
      std::unique_ptr<tcds::lpm::LPMInputsInfoSpaceHandler> inputsInfoSpaceP_;
      std::unique_ptr<tcds::pm::ReTriInfoSpaceUpdater> retriInfoSpaceUpdaterP_;
      std::unique_ptr<tcds::pm::ReTriInfoSpaceHandler> retriInfoSpaceP_;
      std::unique_ptr<tcds::pm::SchedulingInfoSpaceUpdater> schedulingInfoSpaceUpdaterP_;
      std::unique_ptr<tcds::pm::SchedulingInfoSpaceHandler> schedulingInfoSpaceP_;
      std::unique_ptr<tcds::pm::SequencesInfoSpaceUpdater> sequencesInfoSpaceUpdaterP_;
      std::unique_ptr<tcds::pm::SequencesInfoSpaceHandler> sequencesInfoSpaceP_;
      std::unique_ptr<tcds::lpm::SFPInfoSpaceHandler> sfpInfoSpaceP_;
      std::unique_ptr<tcds::hwutilstca::SFPInfoSpaceUpdater> sfpInfoSpaceUpdaterP_;
      std::unique_ptr<tcds::pm::TTSCountersInfoSpaceUpdater> ttsCountersInfoSpaceUpdaterP_;
      std::unique_ptr<tcds::pm::TTSCountersInfoSpaceHandler> ttsCountersInfoSpaceP_;
      std::unique_ptr<tcds::pm::TTSInfoSpaceUpdater> ttsInfoSpaceUpdaterP_;
      std::unique_ptr<tcds::pm::TTSInfoSpaceHandler> ttsInfoSpaceP_;

      // The SOAP commands.
      template<typename> friend class tcds::utils::SOAPCmdBase;
      template<typename> friend class tcds::utils::SOAPCmdDisableCyclicGenerator;
      template<typename> friend class tcds::utils::SOAPCmdDisableRandomTriggers;
      template<typename> friend class tcds::utils::SOAPCmdDumpHardwareState;
      template<typename> friend class tcds::utils::SOAPCmdEnableCyclicGenerator;
      template<typename> friend class tcds::utils::SOAPCmdEnableRandomTriggers;
      template<typename> friend class tcds::utils::SOAPCmdInitCyclicGenerator;
      template<typename> friend class tcds::utils::SOAPCmdInitCyclicGenerators;
      template<typename> friend class tcds::utils::SOAPCmdReadHardwareConfiguration;
      template<typename> friend class tcds::utils::SOAPCmdSendBgo;
      template<typename> friend class tcds::utils::SOAPCmdSendBgoTrain;
      template<typename> friend class tcds::utils::SOAPCmdSendL1A;
      template<typename> friend class tcds::utils::SOAPCmdSendL1APattern;
      template<typename> friend class tcds::utils::SOAPCmdStopL1APattern;
      tcds::utils::SOAPCmdDisableCyclicGenerator<LPMController> soapCmdDisableCyclicGenerator_;
      tcds::utils::SOAPCmdDisableRandomTriggers<LPMController> soapCmdDisableRandomTriggers_;
      tcds::utils::SOAPCmdDumpHardwareState<LPMController> soapCmdDumpHardwareState_;
      tcds::utils::SOAPCmdEnableCyclicGenerator<LPMController> soapCmdEnableCyclicGenerator_;
      tcds::utils::SOAPCmdEnableRandomTriggers<LPMController> soapCmdEnableRandomTriggers_;
      tcds::utils::SOAPCmdInitCyclicGenerator<LPMController> soapCmdInitCyclicGenerator_;
      tcds::utils::SOAPCmdInitCyclicGenerators<LPMController> soapCmdInitCyclicGenerators_;
      tcds::utils::SOAPCmdReadHardwareConfiguration<LPMController> soapCmdReadHardwareConfiguration_;
      tcds::utils::SOAPCmdSendBgo<LPMController> soapCmdSendBgo_;
      tcds::utils::SOAPCmdSendBgoTrain<LPMController> soapCmdSendBgoTrain_;
      tcds::utils::SOAPCmdSendL1A<LPMController> soapCmdSendL1A_;
      tcds::utils::SOAPCmdSendL1APattern<LPMController> soapCmdSendL1APattern_;
      tcds::utils::SOAPCmdStopL1APattern<LPMController> soapCmdStopL1APattern_;

      tcds::pm::PMControllerHelper controllerHelper_;

    };

  } // namespace lpm
} // namespace tcds

#endif // _tcds_lpm_LPMController_h_
