#ifndef _tcds_lpm_ConfigurationInfoSpaceHandler_h_
#define _tcds_lpm_ConfigurationInfoSpaceHandler_h_

#include <string>

#include "tcds/hwutilstca/ConfigurationInfoSpaceHandlerTCA.h"
#include "tcds/utils/InfoSpaceHandler.h"

namespace xdaq {
  class Application;
}

namespace tcds {
  namespace utils {
    class Monitor;
  }
}

namespace tcds {
  namespace lpm {

    class ConfigurationInfoSpaceHandler :
      public tcds::hwutilstca::ConfigurationInfoSpaceHandlerTCA
    {

    public:
      ConfigurationInfoSpaceHandler(xdaq::Application& xdaqApp);

    protected:
      virtual void registerItemSetsWithMonitor(tcds::utils::Monitor& monitor);

      virtual std::string formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const;

    };

  } // namespace lpm
} // namespace tcds

#endif // _tcds_lpm_ConfigurationInfoSpaceHandler_h_
